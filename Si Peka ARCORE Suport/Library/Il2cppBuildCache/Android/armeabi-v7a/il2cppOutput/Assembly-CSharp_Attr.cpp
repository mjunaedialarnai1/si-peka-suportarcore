﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// AOT.MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E;
// easyar.RecordAttribute
struct RecordAttribute_t17C0FA0895CCC82A1AFBBC3137FC024365F7175C;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// easyar.TagAttribute
struct TagAttribute_t25835110934E81FFF0F475C25B2C0B55A0110E64;
// easyar.TaggedUnionAttribute
struct TaggedUnionAttribute_t22E9E03B365A9255EF9A384C44B86E887817A199;
// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute
struct UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DestroyDelegate_t06913E7F91825A39D16B0B5B858A0D4E3E0DD965_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DestroyDelegate_t19DF030524ECBB3C6FB30B7F6D7B9F9CB870C123_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DestroyDelegate_t35091CFD1722C90710B8A7AB97593EC98A26658E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DestroyDelegate_t58A7AB2A5A8928369341E9DC03EBCC39EF972AE0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DestroyDelegate_t612DD6C205DBB515CF1C60F9AAB92E6562522825_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DestroyDelegate_t6699F482F515AD5ED68C6F1590FD89DECDE994F4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DestroyDelegate_t7B362C1A77F9F2395866FF28E52395CC02B4BCED_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DestroyDelegate_t7BAAB0E127D21FA78DAE82B863AEC49CD1D10CA9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DestroyDelegate_t8735587E2933C9D4CDC7FFFCA3C0A1A4563462C6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DestroyDelegate_t97E100E285A7D476FD58380B4563992D58B202F4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DestroyDelegate_tAF8DAAFE464D9A29DD1C6C760EA48D8DB7BCE582_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DestroyDelegate_tB4D508A0446FC216D77D45C9FFA2EE5DA87FC6CD_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DestroyDelegate_tBAB522B953680A703B4A6FBCA481D14ED5BEE88A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DestroyDelegate_tC0C638CDDCB62AE0D763CD16CF6592D3B70F558C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DestroyDelegate_tEBB0571F7C72252FB3680AC0380E6743BC1B0F2F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DestroyDelegate_tFD3FCCCA178A24F53B77AE9C5467B6EF9BA787A7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FunctionDelegate_t01460FEEEA633A8E3096203BCF2F73FB25645CAA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FunctionDelegate_t02275F114513D195DF579898FEEE0208C2EA7DD5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FunctionDelegate_t1ED7EDF57444A462927251285002860AAE764D88_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FunctionDelegate_t24D4FC28606EA11582C19A49D74BA7AC23199BF6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FunctionDelegate_t3E3C9EA91F1A83BEBF5DAFFFE8DB794AD9D49F57_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FunctionDelegate_t40991407952C25E28FEEC90D4A273559B53725E6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FunctionDelegate_t4AD0FE4EF289F75F64E16B339F3029749646F481_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FunctionDelegate_t549DD47FEB60A8418F9189A33662FB345FED7864_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FunctionDelegate_t55A2EA6C5FCF83659D0A477CC147B72C749606D8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FunctionDelegate_t7155C73D6C89E8CE69E1BE682BA84C2B1267A94A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FunctionDelegate_t74FC9648A5A202EE203FF605C38B0FC8ADF69D2C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FunctionDelegate_t8D3DB5D310F9BA1F98B76E94E057A4C1FA3B29D8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FunctionDelegate_t8D63B831E7322B3E23D1E7DBA36D51AA8B821B46_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FunctionDelegate_t965BDC18AA6758A35803FD04576AB475C9E15BAE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FunctionDelegate_tDA46A592EE9410CC083E2ACCFC2486641B718660_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FunctionDelegate_tE37CC43FCC99C300F646A1E52C2CB062F4E37860_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* RenderCameraController_t8DCAC8D2FE426132342CB3E9517C741CC5B73D1E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadFileU3Ed__2_t66473629DE12D0A0305ED769CBC746239B46CB51_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadImageBufferU3Ed__37_t208A68FB14AD7CFAB1F73742F2CC2043D0958615_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadObjFileFromSourceU3Ed__35_tD2FA01F73EFBF81483C3B2F7723FB1FB87B09C3F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadTargetDataBufferU3Ed__38_tD425E9564475563A48477E1A063BB693827A6844_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShareScreenshotU3Ed__7_tF87639DF68B8B53FBE67F96F08499BE172EA9023_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowMessageU3Ed__8_tFBE033A689E35901144A3D3D4F1915C69163FCB2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__2_t3C6C41677E5E07C125793AA6F2216A9C847EBA6C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct  CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct  CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.CreateAssetMenuAttribute
struct  CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;
	// System.Int32 UnityEngine.CreateAssetMenuAttribute::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CorderU3Ek__BackingField_2)); }
	inline int32_t get_U3CorderU3Ek__BackingField_2() const { return ___U3CorderU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_2() { return &___U3CorderU3Ek__BackingField_2; }
	inline void set_U3CorderU3Ek__BackingField_2(int32_t value)
	{
		___U3CorderU3Ek__BackingField_2 = value;
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct  DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct  ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HideInInspector
struct  HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// AOT.MonoPInvokeCallbackAttribute
struct  MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Scripting.PreserveAttribute
struct  PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// easyar.RecordAttribute
struct  RecordAttribute_t17C0FA0895CCC82A1AFBBC3137FC024365F7175C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct  RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct  RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct  SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct  StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// easyar.TagAttribute
struct  TagAttribute_t25835110934E81FFF0F475C25B2C0B55A0110E64  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// easyar.TaggedUnionAttribute
struct  TaggedUnionAttribute_t22E9E03B365A9255EF9A384C44B86E887817A199  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.InteropServices.CallingConvention
struct  CallingConvention_tCD05DC1A211D9713286784F4DDDE1BA18B839924 
{
public:
	// System.Int32 System.Runtime.InteropServices.CallingConvention::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CallingConvention_tCD05DC1A211D9713286784F4DDDE1BA18B839924, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.InteropServices.CharSet
struct  CharSet_tF37E3433B83409C49A52A325333BFBC08ACD6E4B 
{
public:
	// System.Int32 System.Runtime.InteropServices.CharSet::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CharSet_tF37E3433B83409C49A52A325333BFBC08ACD6E4B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct  IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RuntimeInitializeLoadType
struct  RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5 
{
public:
	// System.Int32 UnityEngine.RuntimeInitializeLoadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TextAreaAttribute
struct  TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Int32 UnityEngine.TextAreaAttribute::minLines
	int32_t ___minLines_0;
	// System.Int32 UnityEngine.TextAreaAttribute::maxLines
	int32_t ___maxLines_1;

public:
	inline static int32_t get_offset_of_minLines_0() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B, ___minLines_0)); }
	inline int32_t get_minLines_0() const { return ___minLines_0; }
	inline int32_t* get_address_of_minLines_0() { return &___minLines_0; }
	inline void set_minLines_0(int32_t value)
	{
		___minLines_0 = value;
	}

	inline static int32_t get_offset_of_maxLines_1() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B, ___maxLines_1)); }
	inline int32_t get_maxLines_1() const { return ___maxLines_1; }
	inline int32_t* get_address_of_maxLines_1() { return &___maxLines_1; }
	inline void set_maxLines_1(int32_t value)
	{
		___maxLines_1 = value;
	}
};


// UnityEngine.TooltipAttribute
struct  TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct  DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct  DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct  RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D  : public PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948
{
public:
	// UnityEngine.RuntimeInitializeLoadType UnityEngine.RuntimeInitializeOnLoadMethodAttribute::m_LoadType
	int32_t ___m_LoadType_0;

public:
	inline static int32_t get_offset_of_m_LoadType_0() { return static_cast<int32_t>(offsetof(RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D, ___m_LoadType_0)); }
	inline int32_t get_m_LoadType_0() const { return ___m_LoadType_0; }
	inline int32_t* get_address_of_m_LoadType_0() { return &___m_LoadType_0; }
	inline void set_m_LoadType_0(int32_t value)
	{
		___m_LoadType_0 = value;
	}
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute
struct  UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Runtime.InteropServices.CallingConvention System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::m_callingConvention
	int32_t ___m_callingConvention_0;
	// System.Runtime.InteropServices.CharSet System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::CharSet
	int32_t ___CharSet_1;
	// System.Boolean System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::BestFitMapping
	bool ___BestFitMapping_2;
	// System.Boolean System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::ThrowOnUnmappableChar
	bool ___ThrowOnUnmappableChar_3;
	// System.Boolean System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::SetLastError
	bool ___SetLastError_4;

public:
	inline static int32_t get_offset_of_m_callingConvention_0() { return static_cast<int32_t>(offsetof(UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F, ___m_callingConvention_0)); }
	inline int32_t get_m_callingConvention_0() const { return ___m_callingConvention_0; }
	inline int32_t* get_address_of_m_callingConvention_0() { return &___m_callingConvention_0; }
	inline void set_m_callingConvention_0(int32_t value)
	{
		___m_callingConvention_0 = value;
	}

	inline static int32_t get_offset_of_CharSet_1() { return static_cast<int32_t>(offsetof(UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F, ___CharSet_1)); }
	inline int32_t get_CharSet_1() const { return ___CharSet_1; }
	inline int32_t* get_address_of_CharSet_1() { return &___CharSet_1; }
	inline void set_CharSet_1(int32_t value)
	{
		___CharSet_1 = value;
	}

	inline static int32_t get_offset_of_BestFitMapping_2() { return static_cast<int32_t>(offsetof(UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F, ___BestFitMapping_2)); }
	inline bool get_BestFitMapping_2() const { return ___BestFitMapping_2; }
	inline bool* get_address_of_BestFitMapping_2() { return &___BestFitMapping_2; }
	inline void set_BestFitMapping_2(bool value)
	{
		___BestFitMapping_2 = value;
	}

	inline static int32_t get_offset_of_ThrowOnUnmappableChar_3() { return static_cast<int32_t>(offsetof(UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F, ___ThrowOnUnmappableChar_3)); }
	inline bool get_ThrowOnUnmappableChar_3() const { return ___ThrowOnUnmappableChar_3; }
	inline bool* get_address_of_ThrowOnUnmappableChar_3() { return &___ThrowOnUnmappableChar_3; }
	inline void set_ThrowOnUnmappableChar_3(bool value)
	{
		___ThrowOnUnmappableChar_3 = value;
	}

	inline static int32_t get_offset_of_SetLastError_4() { return static_cast<int32_t>(offsetof(UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F, ___SetLastError_4)); }
	inline bool get_SetLastError_4() const { return ___SetLastError_4; }
	inline bool* get_address_of_SetLastError_4() { return &___SetLastError_4; }
	inline void set_SetLastError_4(bool value)
	{
		___SetLastError_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void AOT.MonoPInvokeCallbackAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344 (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * __this, Type_t * ___type0, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::.ctor(System.Runtime.InteropServices.CallingConvention)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7 (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * __this, int32_t ___callingConvention0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor(UnityEngine.RuntimeInitializeLoadType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516 (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * __this, int32_t ___loadType0, const RuntimeMethod* method);
// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042 (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * __this, int32_t ___minLines0, int32_t ___maxLines1, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void easyar.RecordAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RecordAttribute__ctor_mB1E4F65426EE78E761977E7DF8232A49CC6AAC55 (RecordAttribute_t17C0FA0895CCC82A1AFBBC3137FC024365F7175C * __this, const RuntimeMethod* method);
// System.Void easyar.TaggedUnionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TaggedUnionAttribute__ctor_m50D336CC0A29803F16677978D8227E974BBF480E (TaggedUnionAttribute_t22E9E03B365A9255EF9A384C44B86E887817A199 * __this, const RuntimeMethod* method);
// System.Void easyar.TagAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TagAttribute__ctor_m44A1127B7D01D36A479963B3C559993271CDF1A4 (TagAttribute_t25835110934E81FFF0F475C25B2C0B55A0110E64 * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[2];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[3];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void ARCoreSuport_tDE359F9977610D84AE1201360BA17A9A646F11C4_CustomAttributesCacheGenerator_m_Session(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ARCoreSuport_tDE359F9977610D84AE1201360BA17A9A646F11C4_CustomAttributesCacheGenerator_ARCoreSuport_Start_mBFD408374D7E30C82694DBE2D3C2896217CE1006(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__2_t3C6C41677E5E07C125793AA6F2216A9C847EBA6C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__2_t3C6C41677E5E07C125793AA6F2216A9C847EBA6C_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__2_t3C6C41677E5E07C125793AA6F2216A9C847EBA6C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t3C6C41677E5E07C125793AA6F2216A9C847EBA6C_CustomAttributesCacheGenerator_U3CStartU3Ed__2__ctor_m76493853135E7B3E98D72CAAFD17980374605DE0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t3C6C41677E5E07C125793AA6F2216A9C847EBA6C_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_IDisposable_Dispose_mF2D6ACAB9BD8472C08C90F50438A0657E8412A70(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t3C6C41677E5E07C125793AA6F2216A9C847EBA6C_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m42BEA4DE7F2E3D42FAC40F1BD820B34ECA178777(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t3C6C41677E5E07C125793AA6F2216A9C847EBA6C_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m4E31F02CE2F73E30AA546D239DA3D2DFCF2DDED3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t3C6C41677E5E07C125793AA6F2216A9C847EBA6C_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_mD08A543706D0F525E2B14CA73AD25DDB7336F8B9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Anim_tF3468CFBF078A3F63800C8B0BEA4A5119743F8D4_CustomAttributesCacheGenerator_Anim_delay_m49B92C3EC4783A88BB915F7B9E5828E35676DFE3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_0_0_0_var), NULL);
	}
}
static void U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3__ctor_mB41968B0DEEA004DDB90B2E10A791EB96E48D2CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_IDisposable_Dispose_m1F06D74D5874EB191FBFA69957B9C26470ADAC4A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96FF82B4AFBECC14FD6D4D385BD51F77F6B96E26(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_Collections_IEnumerator_Reset_mDDF68C47CED558B3AB02F86D903391C0BED82521(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_Collections_IEnumerator_get_Current_m9E064515BE7020B0F9EF09EA13C41705F94DE64D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void btnClickOnPortal_tF8B964A7A30C56B888E43B69B2A758EFCCE1F1C8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_0_0_0_var), NULL);
	}
}
static void bukaSpawner_t05FD50273D5CA2CDABF9472BBED6DCA6523E73CC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_0_0_0_var), NULL);
	}
}
static void ScreenShotShare_t085704E55DC8A01DD9C51F156EC17710AA273BE7_CustomAttributesCacheGenerator_ScreenShotShare_ShareScreenshot_m246A5054CA729C909BFCC37EFD7826BDCD8CE5A1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShareScreenshotU3Ed__7_tF87639DF68B8B53FBE67F96F08499BE172EA9023_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CShareScreenshotU3Ed__7_tF87639DF68B8B53FBE67F96F08499BE172EA9023_0_0_0_var), NULL);
	}
}
static void U3CShareScreenshotU3Ed__7_tF87639DF68B8B53FBE67F96F08499BE172EA9023_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShareScreenshotU3Ed__7_tF87639DF68B8B53FBE67F96F08499BE172EA9023_CustomAttributesCacheGenerator_U3CShareScreenshotU3Ed__7__ctor_m272767E0AF9F078E6DCED77A092EBC7439D54C70(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShareScreenshotU3Ed__7_tF87639DF68B8B53FBE67F96F08499BE172EA9023_CustomAttributesCacheGenerator_U3CShareScreenshotU3Ed__7_System_IDisposable_Dispose_mBD94F610B2DE5685FB673119CDF7237905DD2B27(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShareScreenshotU3Ed__7_tF87639DF68B8B53FBE67F96F08499BE172EA9023_CustomAttributesCacheGenerator_U3CShareScreenshotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFCABB45518780891A94828CA071C36CF1A36E5C4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShareScreenshotU3Ed__7_tF87639DF68B8B53FBE67F96F08499BE172EA9023_CustomAttributesCacheGenerator_U3CShareScreenshotU3Ed__7_System_Collections_IEnumerator_Reset_mBFE3415BA35E908E556C1CB1A61028BE711AB897(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShareScreenshotU3Ed__7_tF87639DF68B8B53FBE67F96F08499BE172EA9023_CustomAttributesCacheGenerator_U3CShareScreenshotU3Ed__7_System_Collections_IEnumerator_get_Current_mB20376FE895FF4BAF3C18F02580F4DE96ABBE28D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_map_mF38403A543E4A656CDD76F759FCB6E3A17A69BAE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoid_func_m8233FE906FAE6473609404B722CF66412E5EFB48(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FunctionDelegate_t01460FEEEA633A8E3096203BCF2F73FB25645CAA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(FunctionDelegate_t01460FEEEA633A8E3096203BCF2F73FB25645CAA_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoid_destroy_m9A583576B55465A17C01F9B74E416B531702AB80(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DestroyDelegate_t7B362C1A77F9F2395866FF28E52395CC02B4BCED_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(DestroyDelegate_t7B362C1A77F9F2395866FF28E52395CC02B4BCED_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromOutputFrame_func_m142DA320BDD782F0665F95929A1FAF518AAB0B80(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FunctionDelegate_t24D4FC28606EA11582C19A49D74BA7AC23199BF6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(FunctionDelegate_t24D4FC28606EA11582C19A49D74BA7AC23199BF6_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromOutputFrame_destroy_m39E6790C6700E68EEB6EF662B32E84E8150EE773(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DestroyDelegate_t8735587E2933C9D4CDC7FFFCA3C0A1A4563462C6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(DestroyDelegate_t8735587E2933C9D4CDC7FFFCA3C0A1A4563462C6_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromTargetAndBool_func_m4F4E191A4A3E8C8B1C93A9A83F773843E947D3E0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FunctionDelegate_t40991407952C25E28FEEC90D4A273559B53725E6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(FunctionDelegate_t40991407952C25E28FEEC90D4A273559B53725E6_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromTargetAndBool_destroy_m407203B6D0165B9CBB93CDC394623D25D1394C3E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DestroyDelegate_tFD3FCCCA178A24F53B77AE9C5467B6EF9BA787A7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(DestroyDelegate_tFD3FCCCA178A24F53B77AE9C5467B6EF9BA787A7_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_func_m47D3C6B1C3FA746A6D370FBC7B1D517706FB3EFE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FunctionDelegate_t965BDC18AA6758A35803FD04576AB475C9E15BAE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(FunctionDelegate_t965BDC18AA6758A35803FD04576AB475C9E15BAE_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_destroy_m44D24357597F4A3861061A606C66CAC57CBDE0D8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DestroyDelegate_tC0C638CDDCB62AE0D763CD16CF6592D3B70F558C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(DestroyDelegate_tC0C638CDDCB62AE0D763CD16CF6592D3B70F558C_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromCloudRecognizationResult_func_m631246C2873CA0786043DC8226B7A1F69BAF973D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FunctionDelegate_t4AD0FE4EF289F75F64E16B339F3029749646F481_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(FunctionDelegate_t4AD0FE4EF289F75F64E16B339F3029749646F481_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromCloudRecognizationResult_destroy_mBF60D4AFDA72F76BFBE41B77BBAACB66D22FB251(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DestroyDelegate_t6699F482F515AD5ED68C6F1590FD89DECDE994F4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(DestroyDelegate_t6699F482F515AD5ED68C6F1590FD89DECDE994F4_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromInputFrame_func_mA15F0CAA1EFCDBBE96BA6D3A17F1AE49FFB6443F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FunctionDelegate_t3E3C9EA91F1A83BEBF5DAFFFE8DB794AD9D49F57_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(FunctionDelegate_t3E3C9EA91F1A83BEBF5DAFFFE8DB794AD9D49F57_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromInputFrame_destroy_mBC40B788EC5FA01C081A8B8F29DD83BD7C04BBE0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DestroyDelegate_tAF8DAAFE464D9A29DD1C6C760EA48D8DB7BCE582_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(DestroyDelegate_tAF8DAAFE464D9A29DD1C6C760EA48D8DB7BCE582_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromCameraState_func_mC7E9F14253350E76D81DD852C31479AE6FA97541(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FunctionDelegate_t8D3DB5D310F9BA1F98B76E94E057A4C1FA3B29D8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(FunctionDelegate_t8D3DB5D310F9BA1F98B76E94E057A4C1FA3B29D8_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromCameraState_destroy_mFF1D0D74D6DDC00A03B9F68ABD23B93BBCB212B2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DestroyDelegate_tBAB522B953680A703B4A6FBCA481D14ED5BEE88A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(DestroyDelegate_tBAB522B953680A703B4A6FBCA481D14ED5BEE88A_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromPermissionStatusAndString_func_m8D9D32B6E616DCFD443DE289BEE2A1A9E10D63B3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FunctionDelegate_t8D63B831E7322B3E23D1E7DBA36D51AA8B821B46_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(FunctionDelegate_t8D63B831E7322B3E23D1E7DBA36D51AA8B821B46_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromPermissionStatusAndString_destroy_m781B495D25D988C89184294BD62865D9E801969D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DestroyDelegate_t19DF030524ECBB3C6FB30B7F6D7B9F9CB870C123_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(DestroyDelegate_t19DF030524ECBB3C6FB30B7F6D7B9F9CB870C123_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromLogLevelAndString_func_m9A78157C61E95CA37E54667EDB501049BE406D6A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FunctionDelegate_t74FC9648A5A202EE203FF605C38B0FC8ADF69D2C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(FunctionDelegate_t74FC9648A5A202EE203FF605C38B0FC8ADF69D2C_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromLogLevelAndString_destroy_mF22BD5934E396FB1042F8B0E27B54DBE318FCBFE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DestroyDelegate_t7BAAB0E127D21FA78DAE82B863AEC49CD1D10CA9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(DestroyDelegate_t7BAAB0E127D21FA78DAE82B863AEC49CD1D10CA9_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromRecordStatusAndString_func_mA45DA83CDE8ACEB8F46865D1B86A518290177EBA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FunctionDelegate_t55A2EA6C5FCF83659D0A477CC147B72C749606D8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(FunctionDelegate_t55A2EA6C5FCF83659D0A477CC147B72C749606D8_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromRecordStatusAndString_destroy_mA47A5359B41B4FD8321FA5CC075E76AD7ABBBCD1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DestroyDelegate_t612DD6C205DBB515CF1C60F9AAB92E6562522825_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(DestroyDelegate_t612DD6C205DBB515CF1C60F9AAB92E6562522825_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromBool_func_m18B5057D02371A6A12534E44D51E79565365602A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FunctionDelegate_t549DD47FEB60A8418F9189A33662FB345FED7864_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(FunctionDelegate_t549DD47FEB60A8418F9189A33662FB345FED7864_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromBool_destroy_m3D313F4C383413AAD2FCB52140036EA67F40B985(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DestroyDelegate_t06913E7F91825A39D16B0B5B858A0D4E3E0DD965_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(DestroyDelegate_t06913E7F91825A39D16B0B5B858A0D4E3E0DD965_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromBoolAndStringAndString_func_m32AF4156D52F9B1AEA2A81D03E596F56609DF977(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FunctionDelegate_tDA46A592EE9410CC083E2ACCFC2486641B718660_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(FunctionDelegate_tDA46A592EE9410CC083E2ACCFC2486641B718660_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromBoolAndStringAndString_destroy_mA6C753EE55047102FBA63F1ABC45CE9D2B516010(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DestroyDelegate_t58A7AB2A5A8928369341E9DC03EBCC39EF972AE0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(DestroyDelegate_t58A7AB2A5A8928369341E9DC03EBCC39EF972AE0_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromBoolAndString_func_m4786C9B24241EBD28AAF555E9CFCB97344EFB8CB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FunctionDelegate_t1ED7EDF57444A462927251285002860AAE764D88_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(FunctionDelegate_t1ED7EDF57444A462927251285002860AAE764D88_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromBoolAndString_destroy_mDC79451BFE0F528760772F0A015FBC035E6E023E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DestroyDelegate_tB4D508A0446FC216D77D45C9FFA2EE5DA87FC6CD_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(DestroyDelegate_tB4D508A0446FC216D77D45C9FFA2EE5DA87FC6CD_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromVideoStatus_func_mB7B7FD5B5858F436F3466A1919AF72453E7BE34F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FunctionDelegate_tE37CC43FCC99C300F646A1E52C2CB062F4E37860_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(FunctionDelegate_tE37CC43FCC99C300F646A1E52C2CB062F4E37860_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromVideoStatus_destroy_mC318C3913E76C1CF3DC21561E52A312033C580E4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DestroyDelegate_tEBB0571F7C72252FB3680AC0380E6743BC1B0F2F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(DestroyDelegate_tEBB0571F7C72252FB3680AC0380E6743BC1B0F2F_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromFeedbackFrame_func_mF3A0BCA98C72331241FAC3A693309EBC3570DAD7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FunctionDelegate_t7155C73D6C89E8CE69E1BE682BA84C2B1267A94A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(FunctionDelegate_t7155C73D6C89E8CE69E1BE682BA84C2B1267A94A_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromFeedbackFrame_destroy_mE0489B5AF6DB4645A89B1AA2C5E728AA2085DA06(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DestroyDelegate_t97E100E285A7D476FD58380B4563992D58B202F4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(DestroyDelegate_t97E100E285A7D476FD58380B4563992D58B202F4_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfOutputFrameFromListOfOutputFrame_func_m728DEA484FEF9A32AF0EA5C985684C3A923B6E93(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FunctionDelegate_t02275F114513D195DF579898FEEE0208C2EA7DD5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(FunctionDelegate_t02275F114513D195DF579898FEEE0208C2EA7DD5_0_0_0_var), NULL);
	}
}
static void Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfOutputFrameFromListOfOutputFrame_destroy_m3213152A87BBAB55CB406BB9D08FB45301A33151(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DestroyDelegate_t35091CFD1722C90710B8A7AB97593EC98A26658E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(DestroyDelegate_t35091CFD1722C90710B8A7AB97593EC98A26658E_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_1_t05664A17ABAF0037F70BE8F9D6579D1682A7856D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FunctionDelegate_t01460FEEEA633A8E3096203BCF2F73FB25645CAA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void DestroyDelegate_t7B362C1A77F9F2395866FF28E52395CC02B4BCED_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void FunctionDelegate_t24D4FC28606EA11582C19A49D74BA7AC23199BF6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void DestroyDelegate_t8735587E2933C9D4CDC7FFFCA3C0A1A4563462C6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void FunctionDelegate_t40991407952C25E28FEEC90D4A273559B53725E6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void DestroyDelegate_tFD3FCCCA178A24F53B77AE9C5467B6EF9BA787A7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void FunctionDelegate_t965BDC18AA6758A35803FD04576AB475C9E15BAE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void DestroyDelegate_tC0C638CDDCB62AE0D763CD16CF6592D3B70F558C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void FunctionDelegate_t4AD0FE4EF289F75F64E16B339F3029749646F481_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void DestroyDelegate_t6699F482F515AD5ED68C6F1590FD89DECDE994F4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void FunctionDelegate_t3E3C9EA91F1A83BEBF5DAFFFE8DB794AD9D49F57_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void DestroyDelegate_tAF8DAAFE464D9A29DD1C6C760EA48D8DB7BCE582_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void FunctionDelegate_t8D3DB5D310F9BA1F98B76E94E057A4C1FA3B29D8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void DestroyDelegate_tBAB522B953680A703B4A6FBCA481D14ED5BEE88A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void FunctionDelegate_t8D63B831E7322B3E23D1E7DBA36D51AA8B821B46_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void DestroyDelegate_t19DF030524ECBB3C6FB30B7F6D7B9F9CB870C123_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void FunctionDelegate_t74FC9648A5A202EE203FF605C38B0FC8ADF69D2C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void DestroyDelegate_t7BAAB0E127D21FA78DAE82B863AEC49CD1D10CA9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void FunctionDelegate_t55A2EA6C5FCF83659D0A477CC147B72C749606D8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void DestroyDelegate_t612DD6C205DBB515CF1C60F9AAB92E6562522825_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void FunctionDelegate_t549DD47FEB60A8418F9189A33662FB345FED7864_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void DestroyDelegate_t06913E7F91825A39D16B0B5B858A0D4E3E0DD965_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void FunctionDelegate_tDA46A592EE9410CC083E2ACCFC2486641B718660_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void DestroyDelegate_t58A7AB2A5A8928369341E9DC03EBCC39EF972AE0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void FunctionDelegate_t1ED7EDF57444A462927251285002860AAE764D88_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void DestroyDelegate_tB4D508A0446FC216D77D45C9FFA2EE5DA87FC6CD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void FunctionDelegate_tE37CC43FCC99C300F646A1E52C2CB062F4E37860_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void DestroyDelegate_tEBB0571F7C72252FB3680AC0380E6743BC1B0F2F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void FunctionDelegate_t7155C73D6C89E8CE69E1BE682BA84C2B1267A94A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void DestroyDelegate_t97E100E285A7D476FD58380B4563992D58B202F4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void FunctionDelegate_t02275F114513D195DF579898FEEE0208C2EA7DD5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void DestroyDelegate_t35091CFD1722C90710B8A7AB97593EC98A26658E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void U3CU3Ec_tEDB673ED8B65778F336690A2A0AA5386B668ECC8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass700_0_t59F494BF1FBC058340016CF34743309195CC1E9E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass704_0_t039F65D6042104CF570D7BED8C3BF3AB54C79BC1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass710_0_tFAA78D177D34D2B7D975D937DD6EC2BC2E756C3E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass718_0_tDBAC0B1BB6B7B3CB5E703B936A89B7C54268DFB4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass725_0_t4B72D935E5EECDCD95E101187447016A051CDC50_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass772_0_tB5DAF5FDEF381352FD27A3AB971E59B2304043B8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass776_0_tBE9688521F7E5DEB66C5171BAAAD30D371755839_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass776_1_tA391E4CEBB20623507BA3464F1B594DC9A69CD10_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tDC62FE176348A86F585341DD48D7974C5990D5BE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t90F17302BC8B68FE8528B6B4EDAF8DD6F7B7A81C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_tA8D80133534EC54E2EA403531AEAC85F19DFF8AF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass11_0_tE35CA0DD333BAEB9D4E5ED18A0BEC2C937951BA2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t12C8F22F107E6BFB7EA198C7E964102765DF53D4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass13_0_t1D61D291F31901231A2A64B576DD940FE6F57503_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tB916E7FCA4E75964D43C825EB76F5ADC7B4899FD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t5E28DFCC318EB46EFB6176C90E37766F03FB766B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t34BE33C4114026239A47715CDF46A806AED54DF8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tC47A12109307561E460D3203B1864227DD3D24CA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t350B36A6C4B064F104F582FB51E06339B4E7B070_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tA15E0AC8C76B09D9DA95EFD3A6CC49EE7B859D8F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t5CAAE528C1523EBDA9073C908EA4377D084F658E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tB39217463B45900DDFF1BAAF6EB87C122D240E42_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tC91F828345600208925D5BFAAB28298C9E3A97C4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tF480100DB1BD44F5C62F3E594B4A9467147F0C6A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t8FCC5C06D013EBB4C0B38A42A4447298806E4AE3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t7C3EEDABA6FB0F15D283794ABAA1EEAE9BBD65FC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tB18C8EB72D4C6D8A0CCEEF83F35E9CAE55BCE868_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t79F9D7C2C20E8E125BE8DB940D3C0E74D4FB275C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t87B55B25E460DF303FFA4A365897BCCDCF555F24_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tCC755E78F3528EB7A93593324E833C6297657D8F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tF8B4104CD1063CAD5FCD0F90AB467C8FCCD6460D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CameraImageRenderer_tA2755D03A0C4CF10F91C9346A6AF8C8BE13F5B24_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RenderCameraController_t8DCAC8D2FE426132342CB3E9517C741CC5B73D1E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(RenderCameraController_t8DCAC8D2FE426132342CB3E9517C741CC5B73D1E_0_0_0_var), NULL);
	}
}
static void CameraImageRenderer_tA2755D03A0C4CF10F91C9346A6AF8C8BE13F5B24_CustomAttributesCacheGenerator_OnFrameRenderUpdate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CameraImageRenderer_tA2755D03A0C4CF10F91C9346A6AF8C8BE13F5B24_CustomAttributesCacheGenerator_TargetTextureChange(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CameraImageRenderer_tA2755D03A0C4CF10F91C9346A6AF8C8BE13F5B24_CustomAttributesCacheGenerator_CameraImageRenderer_add_OnFrameRenderUpdate_m51A0C232CFF90B955C67688FDF8BB86EA34997D7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CameraImageRenderer_tA2755D03A0C4CF10F91C9346A6AF8C8BE13F5B24_CustomAttributesCacheGenerator_CameraImageRenderer_remove_OnFrameRenderUpdate_mC9D3AE9A0D3839A54144AF9F19A5CD119EB3DB34(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CameraImageRenderer_tA2755D03A0C4CF10F91C9346A6AF8C8BE13F5B24_CustomAttributesCacheGenerator_CameraImageRenderer_add_TargetTextureChange_mAF7C7E5E3E3C695B331C152F9B904C8C3B7FD14F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CameraImageRenderer_tA2755D03A0C4CF10F91C9346A6AF8C8BE13F5B24_CustomAttributesCacheGenerator_CameraImageRenderer_remove_TargetTextureChange_m78E2A24B9CA80FDBE52FA01D1B9A6A551013A8A3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CameraImageShaders_tA84FA43E9494BB9FD3B7178682907FB646632540_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x45\x61\x73\x79\x41\x52\x2F\x53\x68\x61\x64\x65\x72\x73"), NULL);
	}
}
static void DisplayEmulator_tD487D2897CEC3D0F45AD4B4791AD6BEC28798EE7_CustomAttributesCacheGenerator_U3CRotationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisplayEmulator_tD487D2897CEC3D0F45AD4B4791AD6BEC28798EE7_CustomAttributesCacheGenerator_DisplayEmulator_get_Rotation_mB9E15EBA1BFAE08CB03D0509C103E91E1BED84C0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisplayEmulator_tD487D2897CEC3D0F45AD4B4791AD6BEC28798EE7_CustomAttributesCacheGenerator_DisplayEmulator_set_Rotation_m5626FF085412A9BF4F86CDC79D842FD29A94F305(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RenderCameraController_t8DCAC8D2FE426132342CB3E9517C741CC5B73D1E_CustomAttributesCacheGenerator_RenderCameraController_U3COnFrameUpdateU3Eb__15_0_mB648F9A4567EEEE592C4E8366B39BB4C11C24152(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RenderCameraController_t8DCAC8D2FE426132342CB3E9517C741CC5B73D1E_CustomAttributesCacheGenerator_RenderCameraController_U3COnFrameUpdateU3Eb__15_1_m2E3D602A08DF91717909CC18E505A4AB28907002(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RenderCameraEventHandler_t7702C29BFBB9E141AD5D1D89B44020E9ABC3EF90_CustomAttributesCacheGenerator_PreRender(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RenderCameraEventHandler_t7702C29BFBB9E141AD5D1D89B44020E9ABC3EF90_CustomAttributesCacheGenerator_PostRender(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RenderCameraEventHandler_t7702C29BFBB9E141AD5D1D89B44020E9ABC3EF90_CustomAttributesCacheGenerator_RenderCameraEventHandler_add_PreRender_m2B5CB28BCD7FD08991DA0C37A3FD8572B5B50FE8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RenderCameraEventHandler_t7702C29BFBB9E141AD5D1D89B44020E9ABC3EF90_CustomAttributesCacheGenerator_RenderCameraEventHandler_remove_PreRender_m170313DE9016B8BE301148361DB4385E5B7102F1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RenderCameraEventHandler_t7702C29BFBB9E141AD5D1D89B44020E9ABC3EF90_CustomAttributesCacheGenerator_RenderCameraEventHandler_add_PostRender_mAE8817443F0267F4799FF8FD04DC718B7799C2EC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RenderCameraEventHandler_t7702C29BFBB9E141AD5D1D89B44020E9ABC3EF90_CustomAttributesCacheGenerator_RenderCameraEventHandler_remove_PostRender_mB83C13F707EB6CBA4786BD8C6AD3D580680004D3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RenderCameraParameters_t50DFBE92E83834EDB3ABFA78FF9D7B03A0E5F316_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x45\x61\x73\x79\x41\x52\x2F\x52\x65\x6E\x64\x65\x72\x20\x43\x61\x6D\x65\x72\x61\x20\x50\x61\x72\x61\x6D\x65\x74\x65\x72\x73"), NULL);
	}
}
static void RenderCameraParameters_t50DFBE92E83834EDB3ABFA78FF9D7B03A0E5F316_CustomAttributesCacheGenerator_U3CTransformU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RenderCameraParameters_t50DFBE92E83834EDB3ABFA78FF9D7B03A0E5F316_CustomAttributesCacheGenerator_U3CParametersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RenderCameraParameters_t50DFBE92E83834EDB3ABFA78FF9D7B03A0E5F316_CustomAttributesCacheGenerator_RenderCameraParameters_get_Transform_m3E7E68A87D3D8F5EFCC7A209DCD42545BF62B0DD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RenderCameraParameters_t50DFBE92E83834EDB3ABFA78FF9D7B03A0E5F316_CustomAttributesCacheGenerator_RenderCameraParameters_set_Transform_m72CB53FC12350A7F3F15769ACE79F46F9F481708(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RenderCameraParameters_t50DFBE92E83834EDB3ABFA78FF9D7B03A0E5F316_CustomAttributesCacheGenerator_RenderCameraParameters_get_Parameters_m0EDC1415649355013C64BBAABEEE13E84178A618(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RenderCameraParameters_t50DFBE92E83834EDB3ABFA78FF9D7B03A0E5F316_CustomAttributesCacheGenerator_RenderCameraParameters_set_Parameters_m573A60BFD02F1DAF1316611AEAC9520681530AE7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_U3CDeviceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_CameraType(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_CameraIndex(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_cameraPreference(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_DeviceCreated(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_DeviceOpened(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_DeviceClosed(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_VideoCameraDevice_get_Device_m926B3C984C4B5D61119460CA890229146FDE6355(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_VideoCameraDevice_set_Device_m79AD8296E6911D67EE9443AF8002911C250CD6AE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_VideoCameraDevice_add_DeviceCreated_m3E212D2F198B64A65897F769F074B15BEBFFAA36(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_VideoCameraDevice_remove_DeviceCreated_m515E14F76CBAA5895B5E25B38A206DD27F0A5903(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_VideoCameraDevice_add_DeviceOpened_m77F110C7EC10229FA0081A405A144D24F9403DE1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_VideoCameraDevice_remove_DeviceOpened_mB4CD73BD43608CCAE16B64BCC970E50C3F2C1B50(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_VideoCameraDevice_add_DeviceClosed_m3859CE363FA4A25C778697DF921AF7FEEA05BD33(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_VideoCameraDevice_remove_DeviceClosed_m65127FC471D2D556004BC3FCF95979A663D9FAC6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_VideoCameraDevice_U3COpenU3Eb__36_0_m190320BF0CC8352DE99FFCD853BC7D15C35AAC9D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoRecorder_t5D94DFB2824E84CE27FC6412F494C4C0F8D695F2_CustomAttributesCacheGenerator_StatusUpdate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoRecorder_t5D94DFB2824E84CE27FC6412F494C4C0F8D695F2_CustomAttributesCacheGenerator_U3CIsReadyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoRecorder_t5D94DFB2824E84CE27FC6412F494C4C0F8D695F2_CustomAttributesCacheGenerator_VideoRecorder_add_StatusUpdate_m063C7113C310771CDE2D387DC7F8BB834660BB86(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoRecorder_t5D94DFB2824E84CE27FC6412F494C4C0F8D695F2_CustomAttributesCacheGenerator_VideoRecorder_remove_StatusUpdate_m2A7DD8BCF5AEAC7DD6065798C822E87B2FF420B1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoRecorder_t5D94DFB2824E84CE27FC6412F494C4C0F8D695F2_CustomAttributesCacheGenerator_VideoRecorder_get_IsReady_mEC576F4B82E4DDA3400B8EF8593EAC7429F2639C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoRecorder_t5D94DFB2824E84CE27FC6412F494C4C0F8D695F2_CustomAttributesCacheGenerator_VideoRecorder_set_IsReady_m45B7BD7D9955CF1B069513E7E670DED48E63FE5E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoRecorder_t5D94DFB2824E84CE27FC6412F494C4C0F8D695F2_CustomAttributesCacheGenerator_VideoRecorder_U3CStartU3Eb__14_0_m19D1B3C3E07883B5B2F14E30A2B681AAB83DBAB6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VideoRecorder_t5D94DFB2824E84CE27FC6412F494C4C0F8D695F2_CustomAttributesCacheGenerator_VideoRecorder_U3CStartRecordingU3Eb__17_0_m2BA06D3E64FB113F664EBC97423BCCA9FA5BDE1A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_U3CInitializedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_U3CSchedulerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_U3CWorkerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_U3CDisplayU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_get_Instance_m4D9BEEAA229476C1639FC24DA46A94A5620A0D30(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_set_Instance_m0644DDA7B23B4DF33F6CA10E1EBBAF23EC11AAC5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_get_Initialized_m0FCA276095BC052B4B970D3D1354FB1E6AA98C41(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_set_Initialized_mFD745ACB6C6E691DABA81F36CBC5525734185717(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_get_Scheduler_mC7D3A3E19D863258A8AEF0905F37C16AE54760E6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_set_Scheduler_m5DB7C2DB70602A75751F494AB8DB5B86ABBFB15F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_get_Worker_m7EC07F0D41BF1A762F1B21D68779BA48B00B3E9E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_set_Worker_mFB4F26C6EA7D7CA0AA5D741339D8E34538D1EC91(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_get_Display_mD3A3DC439FCED5221F7A096AF46AB2EB43D4A32E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_set_Display_m8172B0A0B2853E66E36B9EEA28F929E6D08EC73D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_GlobalInitialization_mB63A67532D901C55F5B3A086D12E443A395FB87F(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516(tmp, 1LL, NULL);
	}
}
static void U3CU3Ec_t8953A7CF2F433039D224627C019B1D53D76D965C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EasyARSettings_t3C0403A57B51DEA05B5E546CCBE92E1366725D13_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x45\x61\x73\x79\x41\x52\x2F\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void EasyARSettings_t3C0403A57B51DEA05B5E546CCBE92E1366725D13_CustomAttributesCacheGenerator_LicenseKey(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[2];
		TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042(tmp, 1LL, 10LL, NULL);
	}
}
static void EasyARSettings_t3C0403A57B51DEA05B5E546CCBE92E1366725D13_CustomAttributesCacheGenerator_IOSRecordingSupport(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x63\x72\x6F\x70\x68\x6F\x6E\x65\x20\x70\x65\x72\x6D\x69\x73\x73\x69\x6F\x6E\x20\x69\x73\x20\x72\x65\x71\x75\x69\x72\x65\x64\x20\x66\x6F\x72\x20\x65\x61\x73\x79\x61\x72\x2E\x56\x69\x64\x65\x6F\x52\x65\x63\x6F\x72\x64\x65\x72\x2E\x20\x41\x63\x63\x6F\x72\x64\x69\x6E\x67\x20\x74\x6F\x20\x41\x70\x70\x6C\x65\x27\x73\x20\x70\x6F\x6C\x69\x63\x79\x2C\x20\x79\x6F\x75\x20\x6E\x65\x65\x64\x20\x74\x6F\x20\x61\x64\x64\x20\x4E\x53\x4D\x69\x63\x72\x6F\x70\x68\x6F\x6E\x65\x55\x73\x61\x67\x65\x44\x65\x73\x63\x72\x69\x70\x74\x69\x6F\x6E\x20\x6B\x65\x79\x20\x69\x6E\x20\x74\x68\x65\x20\x70\x6C\x69\x73\x74\x20\x69\x66\x20\x74\x68\x69\x73\x20\x69\x73\x20\x74\x75\x72\x6E\x65\x64\x20\x6F\x6E\x2E"), NULL);
	}
}
static void EasyARSettings_t3C0403A57B51DEA05B5E546CCBE92E1366725D13_CustomAttributesCacheGenerator_ARCoreSDK(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x52\x43\x6F\x72\x65\x20\x53\x44\x4B\x20\x63\x6F\x6E\x66\x69\x67\x75\x72\x61\x74\x69\x6F\x6E\x2E\x20\x49\x66\x20\x79\x6F\x75\x20\x61\x72\x65\x20\x75\x73\x69\x6E\x67\x20\x41\x52\x20\x46\x6F\x75\x6E\x64\x61\x74\x69\x6F\x6E\x20\x6F\x72\x20\x6F\x74\x68\x65\x72\x20\x41\x52\x43\x6F\x72\x65\x20\x53\x44\x4B\x20\x64\x69\x73\x74\x72\x69\x62\x75\x74\x69\x6F\x6E\x73\x2C\x20\x73\x65\x74\x20\x69\x74\x20\x74\x6F\x20\x45\x78\x74\x65\x72\x6E\x61\x6C\x2E"), NULL);
	}
}
static void AndroidManifestPermission_t673225EC0A11452F8E0ED674F66F0ADC1DFE4AD8_CustomAttributesCacheGenerator_CameraDevice(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x65\x72\x6D\x69\x73\x73\x69\x6F\x6E\x20\x72\x65\x71\x75\x69\x72\x65\x64\x20\x66\x6F\x72\x20\x65\x61\x73\x79\x61\x72\x2E\x43\x61\x6D\x65\x72\x61\x44\x65\x76\x69\x63\x65"), NULL);
	}
}
static void AndroidManifestPermission_t673225EC0A11452F8E0ED674F66F0ADC1DFE4AD8_CustomAttributesCacheGenerator_Recording(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x65\x72\x6D\x69\x73\x73\x69\x6F\x6E\x20\x72\x65\x71\x75\x69\x72\x65\x64\x20\x66\x6F\x72\x20\x65\x61\x73\x79\x61\x72\x2E\x56\x69\x64\x65\x6F\x52\x65\x63\x6F\x72\x64\x65\x72"), NULL);
	}
}
static void CloudRecognizerFrameFilter_t5A42E33A5B6E6D7C2B5A208DC1C171CAEC54F35F_CustomAttributesCacheGenerator_U3CCloudRecognizerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CloudRecognizerFrameFilter_t5A42E33A5B6E6D7C2B5A208DC1C171CAEC54F35F_CustomAttributesCacheGenerator_ServerKeyType(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CloudRecognizerFrameFilter_t5A42E33A5B6E6D7C2B5A208DC1C171CAEC54F35F_CustomAttributesCacheGenerator_ServiceConfig(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CloudRecognizerFrameFilter_t5A42E33A5B6E6D7C2B5A208DC1C171CAEC54F35F_CustomAttributesCacheGenerator_PrivateServiceConfig(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CloudRecognizerFrameFilter_t5A42E33A5B6E6D7C2B5A208DC1C171CAEC54F35F_CustomAttributesCacheGenerator_CloudRecognizerFrameFilter_get_CloudRecognizer_m354F93CE6C51A14562194DE2BCEA51F27EE49D5F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CloudRecognizerFrameFilter_t5A42E33A5B6E6D7C2B5A208DC1C171CAEC54F35F_CustomAttributesCacheGenerator_CloudRecognizerFrameFilter_set_CloudRecognizer_m63F102BA1221D4CB3E75A63FB2E693A3B4B054F0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_U3CTargetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageFileSource(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_TargetDataFileSource(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_trackerHasSet(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_tracker(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_TargetAvailable(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_TargetLoad(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_TargetUnload(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_get_Target_m0F1C3C12D9FBF9ADC0AEB5320D86F30D19C0D811(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_set_Target_m691E8B52130CF50D8A749C600CF8E1A9DFC5442B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_add_TargetAvailable_m9467BEDC472B4D21DEEC28741A0CEAFB29DFA820(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_remove_TargetAvailable_m8329089F509E9519EFF04A625CC1666E5A635B50(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_add_TargetLoad_m0D7180BCFF4AEAEB052DD4EA929097BE08EF188C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_remove_TargetLoad_m02E72F6F67A3C78E8AFB012F713611088C6AF932(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_add_TargetUnload_mF3BD3D47A1BA72C2A2A9A5FBAEDD875AA78AC24B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_remove_TargetUnload_mFC4F4CC13F66062F92A926CB7B71B135F11D9B4D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_LoadImageBuffer_mB52CC13907B585A80CD0E74F48F4AC4D04F9CF86(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadImageBufferU3Ed__37_t208A68FB14AD7CFAB1F73742F2CC2043D0958615_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadImageBufferU3Ed__37_t208A68FB14AD7CFAB1F73742F2CC2043D0958615_0_0_0_var), NULL);
	}
}
static void ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_LoadTargetDataBuffer_mDCC3A373F4D20BBB75237CAA55538F03F15E976A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadTargetDataBufferU3Ed__38_tD425E9564475563A48477E1A063BB693827A6844_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadTargetDataBufferU3Ed__38_tD425E9564475563A48477E1A063BB693827A6844_0_0_0_var), NULL);
	}
}
static void ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_U3CLoadTargetDataFileU3Eb__35_0_m376DC236B4B665D74CB1642A180D1DF362DB8FA5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_U3CUpdateTargetInTrackerU3Eb__39_0_mC5711419892B651ACFD0BF21371CB426D3C5C10A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass34_0_t3F566A9A0E3CFCB7CE61C7B0AAB8E6679BFA29D5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass37_0_tD4B7550AA95EEF22872B15A661A8EBD6BAE0F1F8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadImageBufferU3Ed__37_t208A68FB14AD7CFAB1F73742F2CC2043D0958615_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadImageBufferU3Ed__37_t208A68FB14AD7CFAB1F73742F2CC2043D0958615_CustomAttributesCacheGenerator_U3CLoadImageBufferU3Ed__37__ctor_m316EB6054CB5AECB04721EA523876CB3B8F7CFF4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadImageBufferU3Ed__37_t208A68FB14AD7CFAB1F73742F2CC2043D0958615_CustomAttributesCacheGenerator_U3CLoadImageBufferU3Ed__37_System_IDisposable_Dispose_m9549C468CA4876B3B335D784700FB6C36890511D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadImageBufferU3Ed__37_t208A68FB14AD7CFAB1F73742F2CC2043D0958615_CustomAttributesCacheGenerator_U3CLoadImageBufferU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA37C5AAEACB0AA727307F7BE6217A3E2B3EDD78B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadImageBufferU3Ed__37_t208A68FB14AD7CFAB1F73742F2CC2043D0958615_CustomAttributesCacheGenerator_U3CLoadImageBufferU3Ed__37_System_Collections_IEnumerator_Reset_mE9E76167788A27BAF0B470EBF78DE2E3BD92946F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadImageBufferU3Ed__37_t208A68FB14AD7CFAB1F73742F2CC2043D0958615_CustomAttributesCacheGenerator_U3CLoadImageBufferU3Ed__37_System_Collections_IEnumerator_get_Current_m0A268429535E1FFCB1091F7B5CE73BBB1AC6FD49(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass38_0_t4A1B890A5232D6732CAB99FF1724C41F4C5482F8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadTargetDataBufferU3Ed__38_tD425E9564475563A48477E1A063BB693827A6844_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadTargetDataBufferU3Ed__38_tD425E9564475563A48477E1A063BB693827A6844_CustomAttributesCacheGenerator_U3CLoadTargetDataBufferU3Ed__38__ctor_m77E8BEB2D46B0FC1D095FFEB0CA65C72FC0C3C3C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadTargetDataBufferU3Ed__38_tD425E9564475563A48477E1A063BB693827A6844_CustomAttributesCacheGenerator_U3CLoadTargetDataBufferU3Ed__38_System_IDisposable_Dispose_mED0D6ADF10935E5905B204001A364D6B3267C858(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadTargetDataBufferU3Ed__38_tD425E9564475563A48477E1A063BB693827A6844_CustomAttributesCacheGenerator_U3CLoadTargetDataBufferU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m965E6D287A33DAA0DA332F7023DC1E356F17F0F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadTargetDataBufferU3Ed__38_tD425E9564475563A48477E1A063BB693827A6844_CustomAttributesCacheGenerator_U3CLoadTargetDataBufferU3Ed__38_System_Collections_IEnumerator_Reset_m191C863FD554A6ABFCCBDDA70C1209F28B0B32D8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadTargetDataBufferU3Ed__38_tD425E9564475563A48477E1A063BB693827A6844_CustomAttributesCacheGenerator_U3CLoadTargetDataBufferU3Ed__38_System_Collections_IEnumerator_get_Current_mC4E576DFA43B13AFBD8812DF07ECD41A9EC5B13A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass39_0_tA4BA8705F2785624FD2A924D3E14861837A476E3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_U3CTrackerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_simultaneousNum(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_TargetLoad(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_TargetUnload(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_SimultaneousNumChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_ImageTrackerFrameFilter_get_Tracker_m921C5DE4FA7D22775D7A0DBF9E39BB6D283E765F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_ImageTrackerFrameFilter_set_Tracker_m0C3EA99FAF9A0B03278CE3C9127E3FEFC7CDBDB5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_ImageTrackerFrameFilter_add_TargetLoad_mAC6919010D498254A50B080CD9E7D06BA4263FBD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_ImageTrackerFrameFilter_remove_TargetLoad_m3578D6764A9429D55F53A2702D352A6157507745(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_ImageTrackerFrameFilter_add_TargetUnload_m9784EAE4580C2EA58AF655BBC2CDF523A78EA59F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_ImageTrackerFrameFilter_remove_TargetUnload_m55826DC7A7DD08B92535FE77F01F5A9D03343DC2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_ImageTrackerFrameFilter_add_SimultaneousNumChanged_mE16608F77817AA1C35B60C4C55F8CB86B88F1480(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_ImageTrackerFrameFilter_remove_SimultaneousNumChanged_m96B16C9C0818E2E2EB5916212E096CAE78BF02CA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass37_0_tBEAC9E6555412C737837AB6175E88BC0F753FF26_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass38_0_t30CFFB26276DF47336FA634E483ABF6C4E19FD93_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_U3CTargetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjFileSource(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_trackerHasSet(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_tracker(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_TargetAvailable(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_TargetLoad(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_TargetUnload(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjectTargetController_get_Target_m6A2FD3B44FC41DA411C2E3944988452E2733321B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjectTargetController_set_Target_mCE5C95E720E26090653430B9C1FBF667388A4BDC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjectTargetController_add_TargetAvailable_mCAC4FBCFA1A94B4CF3C6B5733E78246AE971B0AD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjectTargetController_remove_TargetAvailable_mB4F3B4DA1CE06FC264792F198ACDF17893EDB3F7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjectTargetController_add_TargetLoad_m8BC5700373C419E7FD833DFE691298443DE9BBE6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjectTargetController_remove_TargetLoad_mED573C2E2B49FBAE05701F23B2DFE9D8D884C54F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjectTargetController_add_TargetUnload_m754EF6ECA76B0D74D7FFABAE46697A4F4C9BA7B7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjectTargetController_remove_TargetUnload_m636650C9CA43F4DBA220C8DFE0134C1229C16907(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjectTargetController_LoadObjFileFromSource_mD45D611FC2B2F282424481E62F8CD8A9852F9DCB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadObjFileFromSourceU3Ed__35_tD2FA01F73EFBF81483C3B2F7723FB1FB87B09C3F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadObjFileFromSourceU3Ed__35_tD2FA01F73EFBF81483C3B2F7723FB1FB87B09C3F_0_0_0_var), NULL);
	}
}
static void ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjectTargetController_U3CUpdateTargetInTrackerU3Eb__36_0_m908DC12A830F0908B6ADFC0F462C6661446B896D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass35_0_tEA75EB810B4C89D89626C37A604F9AAE0B09938B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass35_1_tDF46C8832978C2F0D2AC4BE094B2E0E47A3F8537_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadObjFileFromSourceU3Ed__35_tD2FA01F73EFBF81483C3B2F7723FB1FB87B09C3F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadObjFileFromSourceU3Ed__35_tD2FA01F73EFBF81483C3B2F7723FB1FB87B09C3F_CustomAttributesCacheGenerator_U3CLoadObjFileFromSourceU3Ed__35__ctor_m7A6A05D9063B632855EC5CA6BA48A96F054B8BFE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadObjFileFromSourceU3Ed__35_tD2FA01F73EFBF81483C3B2F7723FB1FB87B09C3F_CustomAttributesCacheGenerator_U3CLoadObjFileFromSourceU3Ed__35_System_IDisposable_Dispose_m5A04B92ED61DC373ACAB6C6D5078C3FCFB50E888(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadObjFileFromSourceU3Ed__35_tD2FA01F73EFBF81483C3B2F7723FB1FB87B09C3F_CustomAttributesCacheGenerator_U3CLoadObjFileFromSourceU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m88255FE97B48643ACC9CE2B55EDD4FBCC117AABC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadObjFileFromSourceU3Ed__35_tD2FA01F73EFBF81483C3B2F7723FB1FB87B09C3F_CustomAttributesCacheGenerator_U3CLoadObjFileFromSourceU3Ed__35_System_Collections_IEnumerator_Reset_mC27E618A38368C2677E9B791ADDF7563C7000A66(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadObjFileFromSourceU3Ed__35_tD2FA01F73EFBF81483C3B2F7723FB1FB87B09C3F_CustomAttributesCacheGenerator_U3CLoadObjFileFromSourceU3Ed__35_System_Collections_IEnumerator_get_Current_mA8A08EB1A756E18E934D954AA573F97B8EE915C3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass36_0_t77A3629A64A992F9143C0CF2B4FC15CA04FCEC0E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_U3CTrackerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_simultaneousNum(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_TargetLoad(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_TargetUnload(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_SimultaneousNumChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_ObjectTrackerFrameFilter_get_Tracker_mA297A04811AA9A4D809E939C83CFB45579E0B960(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_ObjectTrackerFrameFilter_set_Tracker_m3B1E1011F4FCBE741BA781359354DB7308564A2A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_ObjectTrackerFrameFilter_add_TargetLoad_m873A78DB962501BC4DD7523EA6949F455BE30A24(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_ObjectTrackerFrameFilter_remove_TargetLoad_m82D9803297B69CF5AFEAB7AA0F12CF44AE7F52C4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_ObjectTrackerFrameFilter_add_TargetUnload_mA1C4BB8E00F52698BE5A5A21B5103BC342D4EE17(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_ObjectTrackerFrameFilter_remove_TargetUnload_m091E7E675C61AC4B2D7E4785726A5F25BA57E33D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_ObjectTrackerFrameFilter_add_SimultaneousNumChanged_m365A36A03D58A673DF10564813B2DB35C9306846(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_ObjectTrackerFrameFilter_remove_SimultaneousNumChanged_m4571D7E0F96DCD389C730718FDDEA39C507F7971(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass36_0_tE0090E211D0ADCF60FF2E0F4C9551825C8679AE7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass37_0_t535E84F0B4CCE42912B3A9D355AF196CF7E55483_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Unit_tDDDE486307D1CA3361B4E3144CB51C412BCB2007_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RecordAttribute_t17C0FA0895CCC82A1AFBBC3137FC024365F7175C * tmp = (RecordAttribute_t17C0FA0895CCC82A1AFBBC3137FC024365F7175C *)cache->attributes[0];
		RecordAttribute__ctor_mB1E4F65426EE78E761977E7DF8232A49CC6AAC55(tmp, NULL);
	}
}
static void Optional_1_t4453CF5BCF85A5A466C7852DF1130CF811D81BE2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		TaggedUnionAttribute_t22E9E03B365A9255EF9A384C44B86E887817A199 * tmp = (TaggedUnionAttribute_t22E9E03B365A9255EF9A384C44B86E887817A199 *)cache->attributes[0];
		TaggedUnionAttribute__ctor_m50D336CC0A29803F16677978D8227E974BBF480E(tmp, NULL);
	}
}
static void Optional_1_t4453CF5BCF85A5A466C7852DF1130CF811D81BE2_CustomAttributesCacheGenerator__Tag(CustomAttributesCache* cache)
{
	{
		TagAttribute_t25835110934E81FFF0F475C25B2C0B55A0110E64 * tmp = (TagAttribute_t25835110934E81FFF0F475C25B2C0B55A0110E64 *)cache->attributes[0];
		TagAttribute__ctor_m44A1127B7D01D36A479963B3C559993271CDF1A4(tmp, NULL);
	}
}
static void APIExtend_t576D4C83D728565EDDB31F57223C146FB3C99531_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void APIExtend_t576D4C83D728565EDDB31F57223C146FB3C99531_CustomAttributesCacheGenerator_APIExtend_ToUnityMatrix_m206A3ACA63D8BB98326D5DFB039F3FF9DED64550(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void APIExtend_t576D4C83D728565EDDB31F57223C146FB3C99531_CustomAttributesCacheGenerator_APIExtend_ToEasyARVector_mE2724A0CB62CD9CDEAE1046A8B79894F648985F1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void APIExtend_t576D4C83D728565EDDB31F57223C146FB3C99531_CustomAttributesCacheGenerator_APIExtend_ToEasyARVector_m9600E8DE018615DF428475EA76401EFC81B2A242(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void APIExtend_t576D4C83D728565EDDB31F57223C146FB3C99531_CustomAttributesCacheGenerator_APIExtend_ToUnityVector_mF8D534B98643DC1F8A61AD769F2246627141661F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void APIExtend_t576D4C83D728565EDDB31F57223C146FB3C99531_CustomAttributesCacheGenerator_APIExtend_ToUnityVector_m091751C847170124879708574534A1AF98CE92CD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void FileUtil_t577E197EC4ADE41E2545260BA9531D460E966A1F_CustomAttributesCacheGenerator_FileUtil_LoadFile_m4AC9BF927CBA7B25789521DD69A1D0BC5CFC5919(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadFileU3Ed__2_t66473629DE12D0A0305ED769CBC746239B46CB51_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadFileU3Ed__2_t66473629DE12D0A0305ED769CBC746239B46CB51_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_t413430F61E610C5262FE0D2F73F31AECEC085AF9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadFileU3Ed__2_t66473629DE12D0A0305ED769CBC746239B46CB51_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadFileU3Ed__2_t66473629DE12D0A0305ED769CBC746239B46CB51_CustomAttributesCacheGenerator_U3CLoadFileU3Ed__2__ctor_m7DAF075882D1195526359B33F1A62ED93E14812D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadFileU3Ed__2_t66473629DE12D0A0305ED769CBC746239B46CB51_CustomAttributesCacheGenerator_U3CLoadFileU3Ed__2_System_IDisposable_Dispose_mCC55578A4367C5E8FD2DB846C963A4C38CABBD79(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadFileU3Ed__2_t66473629DE12D0A0305ED769CBC746239B46CB51_CustomAttributesCacheGenerator_U3CLoadFileU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4177F95AB6FB19CBE473039096963BB7E9635A57(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadFileU3Ed__2_t66473629DE12D0A0305ED769CBC746239B46CB51_CustomAttributesCacheGenerator_U3CLoadFileU3Ed__2_System_Collections_IEnumerator_Reset_mA8A4FE670BD8AFE9610995CDDB1CA59D0A3C447A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadFileU3Ed__2_t66473629DE12D0A0305ED769CBC746239B46CB51_CustomAttributesCacheGenerator_U3CLoadFileU3Ed__2_System_Collections_IEnumerator_get_Current_mCBCAEC97C03DA3D9F1F58642A8A252E93C8FE366(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GUIPopup_t8A4FEBFFF6F240BEF4708E89CA472852144D97C1_CustomAttributesCacheGenerator_GUIPopup_ShowMessage_mD81F2280B13C2F0CA60575456D4707531137D1F6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowMessageU3Ed__8_tFBE033A689E35901144A3D3D4F1915C69163FCB2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CShowMessageU3Ed__8_tFBE033A689E35901144A3D3D4F1915C69163FCB2_0_0_0_var), NULL);
	}
}
static void U3CShowMessageU3Ed__8_tFBE033A689E35901144A3D3D4F1915C69163FCB2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowMessageU3Ed__8_tFBE033A689E35901144A3D3D4F1915C69163FCB2_CustomAttributesCacheGenerator_U3CShowMessageU3Ed__8__ctor_mAB6F5B533E4E6F9E8A40EBAF7760CE490A96C8C9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowMessageU3Ed__8_tFBE033A689E35901144A3D3D4F1915C69163FCB2_CustomAttributesCacheGenerator_U3CShowMessageU3Ed__8_System_IDisposable_Dispose_mE9E1ED495359DED79D0F13CD157EBDE403127D68(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowMessageU3Ed__8_tFBE033A689E35901144A3D3D4F1915C69163FCB2_CustomAttributesCacheGenerator_U3CShowMessageU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4ACF3C5DA26FB7206A9E7D9F1716CF3DCF4C5B3B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowMessageU3Ed__8_tFBE033A689E35901144A3D3D4F1915C69163FCB2_CustomAttributesCacheGenerator_U3CShowMessageU3Ed__8_System_Collections_IEnumerator_Reset_mD67A86850AFFE89CC4F85E8D08C43BDEDE9DA34F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowMessageU3Ed__8_tFBE033A689E35901144A3D3D4F1915C69163FCB2_CustomAttributesCacheGenerator_U3CShowMessageU3Ed__8_System_Collections_IEnumerator_get_Current_mCA7C149643CD2D0E0987E92DD490202826DA5818(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ThreadWorker_tD00F16D21225F93A49F2019B5FB7EE85C1EFEAE2_CustomAttributesCacheGenerator_ThreadWorker_U3CCreateThreadU3Eb__6_0_m80AF9A8C9FD34024464AB15ADD3C185E5600DD6B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARAssembly_tD524E1F11F3744DCAA6614284A1EA98A842196F7_CustomAttributesCacheGenerator_U3CReadyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARAssembly_tD524E1F11F3744DCAA6614284A1EA98A842196F7_CustomAttributesCacheGenerator_U3CRequireWorldCenterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARAssembly_tD524E1F11F3744DCAA6614284A1EA98A842196F7_CustomAttributesCacheGenerator_ARAssembly_get_Ready_mE0D7D9A363183508328A72FA7C587F88859F5A95(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARAssembly_tD524E1F11F3744DCAA6614284A1EA98A842196F7_CustomAttributesCacheGenerator_ARAssembly_set_Ready_m4628605213B7465AA5147702C0AAA558D806DADF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARAssembly_tD524E1F11F3744DCAA6614284A1EA98A842196F7_CustomAttributesCacheGenerator_ARAssembly_get_RequireWorldCenter_m0C74C6CC8937D31FFE68E706A2F279E258C4DA2F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARAssembly_tD524E1F11F3744DCAA6614284A1EA98A842196F7_CustomAttributesCacheGenerator_ARAssembly_set_RequireWorldCenter_m7C8C2237D1D06E9E9FEDCE171B6F758FEE8094D9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_Assembly(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_FrameChange(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_FrameUpdate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_WorldRootChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_U3CFrameCameraParametersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_ARSession_add_FrameChange_m77548873BBFE5CEE577894326237892B05F413E0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_ARSession_remove_FrameChange_mC3AEE88DD4EF22F919C173CFA09EC594ECF29F2E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_ARSession_add_FrameUpdate_m3CFB06B201EC6F834CA4ABF4F200695E4FACB08A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_ARSession_remove_FrameUpdate_mDB1136F48109CC9DA0B48657022D986E0AC516D9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_ARSession_add_WorldRootChanged_m7FD7A4B2571EB42293F054A5C23127CB960D6FDE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_ARSession_remove_WorldRootChanged_m5AFE86652B914D379381B33B0C64DB303D8DE049(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_ARSession_get_FrameCameraParameters_m894DA7ABA5970C3B7EEE05BEA3FE8598C3D276B5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_ARSession_set_FrameCameraParameters_mEA8C1C870A72E8AD0C7C1B6845FE332624923178(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TargetController_t6224059002B5A227C857B3F069CF648EB9CC317D_CustomAttributesCacheGenerator_U3CIsTrackedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TargetController_t6224059002B5A227C857B3F069CF648EB9CC317D_CustomAttributesCacheGenerator_U3CIsLoadedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TargetController_t6224059002B5A227C857B3F069CF648EB9CC317D_CustomAttributesCacheGenerator_TargetController_get_IsTracked_mCDCD168D5AE7E4FBB89191F60A086BB2159C02B9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TargetController_t6224059002B5A227C857B3F069CF648EB9CC317D_CustomAttributesCacheGenerator_TargetController_set_IsTracked_mEB0AC36E8C682FA8B2D00B02E65CAE450F40741F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TargetController_t6224059002B5A227C857B3F069CF648EB9CC317D_CustomAttributesCacheGenerator_TargetController_get_IsLoaded_m74BD4BCDAC8BDA2A60482DB96BEC34F944E38D51(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TargetController_t6224059002B5A227C857B3F069CF648EB9CC317D_CustomAttributesCacheGenerator_TargetController_set_IsLoaded_m7C2707C8D99CAAFC51A250614A4790FD43D99472(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldRootController_t3C4EEDEC8FBD39F957F99B437AF2859DC6B222B1_CustomAttributesCacheGenerator_TrackingStatusChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldRootController_t3C4EEDEC8FBD39F957F99B437AF2859DC6B222B1_CustomAttributesCacheGenerator_U3CTrackingStatusU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldRootController_t3C4EEDEC8FBD39F957F99B437AF2859DC6B222B1_CustomAttributesCacheGenerator_WorldRootController_add_TrackingStatusChanged_mD2CE170147C8A715C42BBD4EF9951AB7C9977A51(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldRootController_t3C4EEDEC8FBD39F957F99B437AF2859DC6B222B1_CustomAttributesCacheGenerator_WorldRootController_remove_TrackingStatusChanged_m0FF0A489070271B1B9B65CB749D396FE388D7508(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldRootController_t3C4EEDEC8FBD39F957F99B437AF2859DC6B222B1_CustomAttributesCacheGenerator_WorldRootController_get_TrackingStatus_m93D0D79F1E994459E25C2692A96D498D1599EB5E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldRootController_t3C4EEDEC8FBD39F957F99B437AF2859DC6B222B1_CustomAttributesCacheGenerator_WorldRootController_set_TrackingStatus_m1573ED629C5CAA28C2907D85CE636576BBF05968(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapBlockController_t2782D8A2996368BF3D9AD88814219C88D3A23C35_CustomAttributesCacheGenerator_U3CInfoU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapBlockController_t2782D8A2996368BF3D9AD88814219C88D3A23C35_CustomAttributesCacheGenerator_DenseSpatialMapBlockController_get_Info_m9B5FAAE3691BEA4AAA516AF8BC75A971D265267F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapBlockController_t2782D8A2996368BF3D9AD88814219C88D3A23C35_CustomAttributesCacheGenerator_DenseSpatialMapBlockController_set_Info_m13F0699BCA49F39EB168F49AE2199DD7CC7ABFFC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_U3CBuilderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_MapCreate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_MapUpdate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_U3CTrackingStatusU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_DenseSpatialMapBuilderFrameFilter_get_Builder_m758140881A3D5EB2FDD98AEF2DBE5412150F6ABD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_DenseSpatialMapBuilderFrameFilter_set_Builder_m737FCF073162DE6634880B16969D488F26D0A7C6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_DenseSpatialMapBuilderFrameFilter_add_MapCreate_m50792A04FFC1A0E331C87EBAD4429A612F55392D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_DenseSpatialMapBuilderFrameFilter_remove_MapCreate_m672731B5112CA728EAC9F8C238D9B1BEEB4C13FB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_DenseSpatialMapBuilderFrameFilter_add_MapUpdate_m2DED4A7DD8A17E497F480B12B1F438CDE6099BBC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_DenseSpatialMapBuilderFrameFilter_remove_MapUpdate_m210F0D63D0E6A2AC2A3B88A55296E30729EEDA18(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_DenseSpatialMapBuilderFrameFilter_get_TrackingStatus_m3E17B76E936462D507DD2C5DDC6F6EA56BF48F03(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_DenseSpatialMapBuilderFrameFilter_set_TrackingStatus_m6FB6FEC5F485ECC8E81867E643274C679E6FB8E1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_DenseSpatialMapBuilderFrameFilter_U3COnAssembleU3Eb__41_0_mB7091E6C9A2299026A8DBA9B6790D69D225AEB91(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapDepthRenderer_tEE0270C713C47AFCC2D26ADF47E35B8119896817_CustomAttributesCacheGenerator_U3CRenderDepthCameraU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapDepthRenderer_tEE0270C713C47AFCC2D26ADF47E35B8119896817_CustomAttributesCacheGenerator_U3CMapMeshMaterialU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapDepthRenderer_tEE0270C713C47AFCC2D26ADF47E35B8119896817_CustomAttributesCacheGenerator_DenseSpatialMapDepthRenderer_get_RenderDepthCamera_m681A99424571E53FC47EE5FA06DA4662882FC1F2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapDepthRenderer_tEE0270C713C47AFCC2D26ADF47E35B8119896817_CustomAttributesCacheGenerator_DenseSpatialMapDepthRenderer_set_RenderDepthCamera_m492EF4A696EF204AD15D3E94CF170B8E9ABD6F45(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapDepthRenderer_tEE0270C713C47AFCC2D26ADF47E35B8119896817_CustomAttributesCacheGenerator_DenseSpatialMapDepthRenderer_get_MapMeshMaterial_m0BBD1D7CD30168722CCD1758403766320276DF87(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DenseSpatialMapDepthRenderer_tEE0270C713C47AFCC2D26ADF47E35B8119896817_CustomAttributesCacheGenerator_DenseSpatialMapDepthRenderer_set_MapMeshMaterial_mC72C0357A3454D90FE3AD3493D0ADE32C158EF36(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_U3CMapInfoU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_MapManagerSource(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_showPointCloud(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_pointCloudParticleParameter(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_mapWorkerHasSet(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_mapWorker(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_MapInfoAvailable(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_MapLocalized(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_MapStopLocalize(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_MapLoad(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_MapUnload(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_MapHost(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_U3CPointCloudU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_U3CIsLocalizingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_get_MapInfo_m9F7973AF582AB3D534F5A78C60A5B9EA4FF77A09(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_set_MapInfo_m9A2E132F01CDA34B585D1393BDB53914392E6148(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_add_MapInfoAvailable_m2862A31F3E802A743EB2EFD089685CAAEE2ABD56(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_remove_MapInfoAvailable_mE56EBAB6010C146C210663CDF819CD10602D897F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_add_MapLocalized_m1E916CEF68A96614F5550D3ED1E13B5BAE9F64B3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_remove_MapLocalized_m9B100A0D840C4240F4EE3D07078C36B28E4CC18C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_add_MapStopLocalize_m47AD420BFD0385FEEFC65E3FE60316C2A3BA65F6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_remove_MapStopLocalize_mA18DA2D96E3D564B641366E2D1878197A572F88C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_add_MapLoad_m1EAD1D28F05893F5F24E830D895C07650119676E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_remove_MapLoad_m948B3F5628A733F6ED2E96668F4FFCFACD494333(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_add_MapUnload_m2FB811A896D21DDF1CE0365F4672BF245B99F69F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_remove_MapUnload_m662469C6CE6B9C1ADCE099947BF8FB0AAAE3883B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_add_MapHost_mDD77B7155B17BBB8F9A4737B6C8F03CE408208BE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_remove_MapHost_m053C92797E2D6D610C67E10ECB87A00ADC7459B2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_get_PointCloud_m9E47A3C47C14727138FFB5EF22CFB19EED8DDC83(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_set_PointCloud_m5741878569506D3FF8A9541478C658577DA831EF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_get_IsLocalizing_m18ECA11E492F62E020E84D70D19E52FAA5418A14(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_set_IsLocalizing_m3953314E7CC78205D195598D224B1FA8C7F0885E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_U3CHostU3Eb__56_0_m3CAEB4EC3D350CCFA68C581013A003178E786564(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_U3CUpdatePointCloudU3Eb__59_0_m02E8F1ACE41A94F38155BAB09A9EC88EED9B1202(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_U3CUpdateMapInLocalizerU3Eb__63_0_mF3859885855B1D4DAE5DE4A90F9F337DB9E26733(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass58_0_t095E9C7A78F28F20E514E2C748AAE2B48A16C43A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass63_0_t6E388F6323FB2851236A4B4A4A332ABBBF2C3F10_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_U3CBuilderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_U3CLocalizerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_U3CManagerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_ServiceConfig(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_MapLoad(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_MapUnload(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_MapHost(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_U3CTrackingStatusU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_U3CWorkingModeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_U3CLocalizedMapU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_U3CBuilderMapControllerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_get_Builder_mBDD7FD5C1D5874ECEA16C70E90EBB24B60A156D2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_set_Builder_m8FB3708CF2035B1B9F3143EF73679478F41D533B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_get_Localizer_m481D0F8E9147E444A72E4E5314E274A149015FFD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_set_Localizer_mF300368DA1D996D55EE36DCDB612A2A19A1205FF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_get_Manager_m3FEB082165C0C8373D273049674055AF80FEA82C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_set_Manager_mD110FC2EDB5EF821F0FE1CB19AAACD3EB92927CD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_add_MapLoad_m9761FF716AD4AB2E7B31B8B95F167D55A743EEDF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_remove_MapLoad_mF1EA708E880E9F28E711B6189275A1549F55E704(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_add_MapUnload_mACB33B7581BAAAE760DDC69C6C91034978F21414(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_remove_MapUnload_m0AD3F4D12187B774221F0895C7DDC7B60831EC7E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_add_MapHost_mB906F4AF336AFAB07D645A0EEAEFCE1516CE5727(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_remove_MapHost_mF4B8E4EEC4D44230DAD3148CB92E1A7744B279B7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_get_TrackingStatus_mF01A0D9EADE683AA43A9A09567B9C29CA231F300(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_set_TrackingStatus_mFEA33C5312DB9C0A699EE011CF98CF4955465D9A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_get_WorkingMode_mF8E9D3D97D5F1BC30860D7CAD2733F96EC97DE84(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_set_WorkingMode_m149AC1EE7A6BAEF8771908A6BD493F27AD0BF5CE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_get_LocalizedMap_m58350A5B898D5E2D5437F76AD6B670CDC0BFC55F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_set_LocalizedMap_m2006E253D0EE4E6D526491D2287EC70DBE4D6353(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_get_BuilderMapController_m3B422BADE125C970974568C6AEA40AA76B33988A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_set_BuilderMapController_mF5A767EE02F7F72607BC17A0A188478C3BF3AF88(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_U3COnAssembleU3Eb__62_0_m1C6E2C8CF69B82D1DF8CE6FBECC0F00C9EA2AA15(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass64_0_t077F69999BC79DA6D4A7AEFF58907A27E52F7902_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass65_0_tECCE9C536DE9B2EEE07CEB5F5C3E04A2812AA948_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass66_0_t9C21DCE46449BD59D109191BDBB6045D5FB9A1D3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SurfaceTrackerFrameFilter_t2BEA58EA4D95BD41028F6206C9E7990C2394382E_CustomAttributesCacheGenerator_U3CTrackerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SurfaceTrackerFrameFilter_t2BEA58EA4D95BD41028F6206C9E7990C2394382E_CustomAttributesCacheGenerator_SurfaceTrackerFrameFilter_get_Tracker_m3515EE44BB36EDE69EA10E7DFB43725F308B37E8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SurfaceTrackerFrameFilter_t2BEA58EA4D95BD41028F6206C9E7990C2394382E_CustomAttributesCacheGenerator_SurfaceTrackerFrameFilter_set_Tracker_mE4E2C8642D3A95893D082229CB198D89059ACCB8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_U3CDeviceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_DeviceCreated(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_DeviceOpened(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_DeviceClosed(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_VIOCameraDeviceUnion_get_Device_mFE46D8F19218FECD9FF165FE6E9759992BD707AA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_VIOCameraDeviceUnion_set_Device_m9B7BA8186E8CA67F5674BBA422FC088F10786232(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_VIOCameraDeviceUnion_add_DeviceCreated_m74AD89492D2711324561C8E330BADB762414D47C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_VIOCameraDeviceUnion_remove_DeviceCreated_m814DE270549834E858D6F5DA0DA2998774673B23(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_VIOCameraDeviceUnion_add_DeviceOpened_m19356CB413BC5584A315200A6C88CCDD82736F90(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_VIOCameraDeviceUnion_remove_DeviceOpened_m23CAF95355804DDDF8E0FABA8BC358D9638813E6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_VIOCameraDeviceUnion_add_DeviceClosed_m5B573063D9E27DA34B996A6C0220A4F4579AE97B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_VIOCameraDeviceUnion_remove_DeviceClosed_m29E68A81FFAEB479EE86B4F072F416A8CE65B1EA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_VIOCameraDeviceUnion_U3COpenU3Eb__35_0_m6EF254CCD5BFA74F72BCE71EA2E0C494780258A0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DeviceUnion_t28AB5438E81AA07F093A6D1A114616FDCB540FC9_CustomAttributesCacheGenerator_U3CDeviceTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DeviceUnion_t28AB5438E81AA07F093A6D1A114616FDCB540FC9_CustomAttributesCacheGenerator_DeviceUnion_get_DeviceType_m3C4A65E1E009312D2B43DA6C9A89ADBDFC457536(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DeviceUnion_t28AB5438E81AA07F093A6D1A114616FDCB540FC9_CustomAttributesCacheGenerator_DeviceUnion_set_DeviceType_m07F63903240ECF385A4C8E518FC8A7733BFF528C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass31_0_tFC0DE9EF235A32B0CDD56FBD46A4F96156CB04FE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass38_0_t69E545D6C9ABDAF5197C97948736476FCA9F56EC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass39_0_t4CD5AD199A2A20ABD7EDA455F921EAAC020E1CEE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass40_0_t6189F2CFDF63149C3B6E82D3DAB2286E14D3CE5A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[469] = 
{
	U3CStartU3Ed__2_t3C6C41677E5E07C125793AA6F2216A9C847EBA6C_CustomAttributesCacheGenerator,
	U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator,
	btnClickOnPortal_tF8B964A7A30C56B888E43B69B2A758EFCCE1F1C8_CustomAttributesCacheGenerator,
	bukaSpawner_t05FD50273D5CA2CDABF9472BBED6DCA6523E73CC_CustomAttributesCacheGenerator,
	U3CShareScreenshotU3Ed__7_tF87639DF68B8B53FBE67F96F08499BE172EA9023_CustomAttributesCacheGenerator,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_1_t05664A17ABAF0037F70BE8F9D6579D1682A7856D_CustomAttributesCacheGenerator,
	FunctionDelegate_t01460FEEEA633A8E3096203BCF2F73FB25645CAA_CustomAttributesCacheGenerator,
	DestroyDelegate_t7B362C1A77F9F2395866FF28E52395CC02B4BCED_CustomAttributesCacheGenerator,
	FunctionDelegate_t24D4FC28606EA11582C19A49D74BA7AC23199BF6_CustomAttributesCacheGenerator,
	DestroyDelegate_t8735587E2933C9D4CDC7FFFCA3C0A1A4563462C6_CustomAttributesCacheGenerator,
	FunctionDelegate_t40991407952C25E28FEEC90D4A273559B53725E6_CustomAttributesCacheGenerator,
	DestroyDelegate_tFD3FCCCA178A24F53B77AE9C5467B6EF9BA787A7_CustomAttributesCacheGenerator,
	FunctionDelegate_t965BDC18AA6758A35803FD04576AB475C9E15BAE_CustomAttributesCacheGenerator,
	DestroyDelegate_tC0C638CDDCB62AE0D763CD16CF6592D3B70F558C_CustomAttributesCacheGenerator,
	FunctionDelegate_t4AD0FE4EF289F75F64E16B339F3029749646F481_CustomAttributesCacheGenerator,
	DestroyDelegate_t6699F482F515AD5ED68C6F1590FD89DECDE994F4_CustomAttributesCacheGenerator,
	FunctionDelegate_t3E3C9EA91F1A83BEBF5DAFFFE8DB794AD9D49F57_CustomAttributesCacheGenerator,
	DestroyDelegate_tAF8DAAFE464D9A29DD1C6C760EA48D8DB7BCE582_CustomAttributesCacheGenerator,
	FunctionDelegate_t8D3DB5D310F9BA1F98B76E94E057A4C1FA3B29D8_CustomAttributesCacheGenerator,
	DestroyDelegate_tBAB522B953680A703B4A6FBCA481D14ED5BEE88A_CustomAttributesCacheGenerator,
	FunctionDelegate_t8D63B831E7322B3E23D1E7DBA36D51AA8B821B46_CustomAttributesCacheGenerator,
	DestroyDelegate_t19DF030524ECBB3C6FB30B7F6D7B9F9CB870C123_CustomAttributesCacheGenerator,
	FunctionDelegate_t74FC9648A5A202EE203FF605C38B0FC8ADF69D2C_CustomAttributesCacheGenerator,
	DestroyDelegate_t7BAAB0E127D21FA78DAE82B863AEC49CD1D10CA9_CustomAttributesCacheGenerator,
	FunctionDelegate_t55A2EA6C5FCF83659D0A477CC147B72C749606D8_CustomAttributesCacheGenerator,
	DestroyDelegate_t612DD6C205DBB515CF1C60F9AAB92E6562522825_CustomAttributesCacheGenerator,
	FunctionDelegate_t549DD47FEB60A8418F9189A33662FB345FED7864_CustomAttributesCacheGenerator,
	DestroyDelegate_t06913E7F91825A39D16B0B5B858A0D4E3E0DD965_CustomAttributesCacheGenerator,
	FunctionDelegate_tDA46A592EE9410CC083E2ACCFC2486641B718660_CustomAttributesCacheGenerator,
	DestroyDelegate_t58A7AB2A5A8928369341E9DC03EBCC39EF972AE0_CustomAttributesCacheGenerator,
	FunctionDelegate_t1ED7EDF57444A462927251285002860AAE764D88_CustomAttributesCacheGenerator,
	DestroyDelegate_tB4D508A0446FC216D77D45C9FFA2EE5DA87FC6CD_CustomAttributesCacheGenerator,
	FunctionDelegate_tE37CC43FCC99C300F646A1E52C2CB062F4E37860_CustomAttributesCacheGenerator,
	DestroyDelegate_tEBB0571F7C72252FB3680AC0380E6743BC1B0F2F_CustomAttributesCacheGenerator,
	FunctionDelegate_t7155C73D6C89E8CE69E1BE682BA84C2B1267A94A_CustomAttributesCacheGenerator,
	DestroyDelegate_t97E100E285A7D476FD58380B4563992D58B202F4_CustomAttributesCacheGenerator,
	FunctionDelegate_t02275F114513D195DF579898FEEE0208C2EA7DD5_CustomAttributesCacheGenerator,
	DestroyDelegate_t35091CFD1722C90710B8A7AB97593EC98A26658E_CustomAttributesCacheGenerator,
	U3CU3Ec_tEDB673ED8B65778F336690A2A0AA5386B668ECC8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass700_0_t59F494BF1FBC058340016CF34743309195CC1E9E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass704_0_t039F65D6042104CF570D7BED8C3BF3AB54C79BC1_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass710_0_tFAA78D177D34D2B7D975D937DD6EC2BC2E756C3E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass718_0_tDBAC0B1BB6B7B3CB5E703B936A89B7C54268DFB4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass725_0_t4B72D935E5EECDCD95E101187447016A051CDC50_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass772_0_tB5DAF5FDEF381352FD27A3AB971E59B2304043B8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass776_0_tBE9688521F7E5DEB66C5171BAAAD30D371755839_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass776_1_tA391E4CEBB20623507BA3464F1B594DC9A69CD10_CustomAttributesCacheGenerator,
	U3CU3Ec_tDC62FE176348A86F585341DD48D7974C5990D5BE_CustomAttributesCacheGenerator,
	U3CU3Ec_t90F17302BC8B68FE8528B6B4EDAF8DD6F7B7A81C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_tA8D80133534EC54E2EA403531AEAC85F19DFF8AF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_0_tE35CA0DD333BAEB9D4E5ED18A0BEC2C937951BA2_CustomAttributesCacheGenerator,
	U3CU3Ec_t12C8F22F107E6BFB7EA198C7E964102765DF53D4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass13_0_t1D61D291F31901231A2A64B576DD940FE6F57503_CustomAttributesCacheGenerator,
	U3CU3Ec_tB916E7FCA4E75964D43C825EB76F5ADC7B4899FD_CustomAttributesCacheGenerator,
	U3CU3Ec_t5E28DFCC318EB46EFB6176C90E37766F03FB766B_CustomAttributesCacheGenerator,
	U3CU3Ec_t34BE33C4114026239A47715CDF46A806AED54DF8_CustomAttributesCacheGenerator,
	U3CU3Ec_tC47A12109307561E460D3203B1864227DD3D24CA_CustomAttributesCacheGenerator,
	U3CU3Ec_t350B36A6C4B064F104F582FB51E06339B4E7B070_CustomAttributesCacheGenerator,
	U3CU3Ec_tA15E0AC8C76B09D9DA95EFD3A6CC49EE7B859D8F_CustomAttributesCacheGenerator,
	U3CU3Ec_t5CAAE528C1523EBDA9073C908EA4377D084F658E_CustomAttributesCacheGenerator,
	U3CU3Ec_tB39217463B45900DDFF1BAAF6EB87C122D240E42_CustomAttributesCacheGenerator,
	U3CU3Ec_tC91F828345600208925D5BFAAB28298C9E3A97C4_CustomAttributesCacheGenerator,
	U3CU3Ec_tF480100DB1BD44F5C62F3E594B4A9467147F0C6A_CustomAttributesCacheGenerator,
	U3CU3Ec_t8FCC5C06D013EBB4C0B38A42A4447298806E4AE3_CustomAttributesCacheGenerator,
	U3CU3Ec_t7C3EEDABA6FB0F15D283794ABAA1EEAE9BBD65FC_CustomAttributesCacheGenerator,
	U3CU3Ec_tB18C8EB72D4C6D8A0CCEEF83F35E9CAE55BCE868_CustomAttributesCacheGenerator,
	U3CU3Ec_t79F9D7C2C20E8E125BE8DB940D3C0E74D4FB275C_CustomAttributesCacheGenerator,
	U3CU3Ec_t87B55B25E460DF303FFA4A365897BCCDCF555F24_CustomAttributesCacheGenerator,
	U3CU3Ec_tCC755E78F3528EB7A93593324E833C6297657D8F_CustomAttributesCacheGenerator,
	U3CU3Ec_tF8B4104CD1063CAD5FCD0F90AB467C8FCCD6460D_CustomAttributesCacheGenerator,
	CameraImageRenderer_tA2755D03A0C4CF10F91C9346A6AF8C8BE13F5B24_CustomAttributesCacheGenerator,
	CameraImageShaders_tA84FA43E9494BB9FD3B7178682907FB646632540_CustomAttributesCacheGenerator,
	RenderCameraParameters_t50DFBE92E83834EDB3ABFA78FF9D7B03A0E5F316_CustomAttributesCacheGenerator,
	U3CU3Ec_t8953A7CF2F433039D224627C019B1D53D76D965C_CustomAttributesCacheGenerator,
	EasyARSettings_t3C0403A57B51DEA05B5E546CCBE92E1366725D13_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass34_0_t3F566A9A0E3CFCB7CE61C7B0AAB8E6679BFA29D5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass37_0_tD4B7550AA95EEF22872B15A661A8EBD6BAE0F1F8_CustomAttributesCacheGenerator,
	U3CLoadImageBufferU3Ed__37_t208A68FB14AD7CFAB1F73742F2CC2043D0958615_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass38_0_t4A1B890A5232D6732CAB99FF1724C41F4C5482F8_CustomAttributesCacheGenerator,
	U3CLoadTargetDataBufferU3Ed__38_tD425E9564475563A48477E1A063BB693827A6844_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass39_0_tA4BA8705F2785624FD2A924D3E14861837A476E3_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass37_0_tBEAC9E6555412C737837AB6175E88BC0F753FF26_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass38_0_t30CFFB26276DF47336FA634E483ABF6C4E19FD93_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass35_0_tEA75EB810B4C89D89626C37A604F9AAE0B09938B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass35_1_tDF46C8832978C2F0D2AC4BE094B2E0E47A3F8537_CustomAttributesCacheGenerator,
	U3CLoadObjFileFromSourceU3Ed__35_tD2FA01F73EFBF81483C3B2F7723FB1FB87B09C3F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass36_0_t77A3629A64A992F9143C0CF2B4FC15CA04FCEC0E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass36_0_tE0090E211D0ADCF60FF2E0F4C9551825C8679AE7_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass37_0_t535E84F0B4CCE42912B3A9D355AF196CF7E55483_CustomAttributesCacheGenerator,
	Unit_tDDDE486307D1CA3361B4E3144CB51C412BCB2007_CustomAttributesCacheGenerator,
	Optional_1_t4453CF5BCF85A5A466C7852DF1130CF811D81BE2_CustomAttributesCacheGenerator,
	APIExtend_t576D4C83D728565EDDB31F57223C146FB3C99531_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_t413430F61E610C5262FE0D2F73F31AECEC085AF9_CustomAttributesCacheGenerator,
	U3CLoadFileU3Ed__2_t66473629DE12D0A0305ED769CBC746239B46CB51_CustomAttributesCacheGenerator,
	U3CShowMessageU3Ed__8_tFBE033A689E35901144A3D3D4F1915C69163FCB2_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass58_0_t095E9C7A78F28F20E514E2C748AAE2B48A16C43A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass63_0_t6E388F6323FB2851236A4B4A4A332ABBBF2C3F10_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass64_0_t077F69999BC79DA6D4A7AEFF58907A27E52F7902_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass65_0_tECCE9C536DE9B2EEE07CEB5F5C3E04A2812AA948_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass66_0_t9C21DCE46449BD59D109191BDBB6045D5FB9A1D3_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass31_0_tFC0DE9EF235A32B0CDD56FBD46A4F96156CB04FE_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass38_0_t69E545D6C9ABDAF5197C97948736476FCA9F56EC_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass39_0_t4CD5AD199A2A20ABD7EDA455F921EAAC020E1CEE_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass40_0_t6189F2CFDF63149C3B6E82D3DAB2286E14D3CE5A_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator,
	ARCoreSuport_tDE359F9977610D84AE1201360BA17A9A646F11C4_CustomAttributesCacheGenerator_m_Session,
	CameraImageRenderer_tA2755D03A0C4CF10F91C9346A6AF8C8BE13F5B24_CustomAttributesCacheGenerator_OnFrameRenderUpdate,
	CameraImageRenderer_tA2755D03A0C4CF10F91C9346A6AF8C8BE13F5B24_CustomAttributesCacheGenerator_TargetTextureChange,
	DisplayEmulator_tD487D2897CEC3D0F45AD4B4791AD6BEC28798EE7_CustomAttributesCacheGenerator_U3CRotationU3Ek__BackingField,
	RenderCameraEventHandler_t7702C29BFBB9E141AD5D1D89B44020E9ABC3EF90_CustomAttributesCacheGenerator_PreRender,
	RenderCameraEventHandler_t7702C29BFBB9E141AD5D1D89B44020E9ABC3EF90_CustomAttributesCacheGenerator_PostRender,
	RenderCameraParameters_t50DFBE92E83834EDB3ABFA78FF9D7B03A0E5F316_CustomAttributesCacheGenerator_U3CTransformU3Ek__BackingField,
	RenderCameraParameters_t50DFBE92E83834EDB3ABFA78FF9D7B03A0E5F316_CustomAttributesCacheGenerator_U3CParametersU3Ek__BackingField,
	VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_U3CDeviceU3Ek__BackingField,
	VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_CameraType,
	VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_CameraIndex,
	VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_cameraPreference,
	VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_DeviceCreated,
	VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_DeviceOpened,
	VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_DeviceClosed,
	VideoRecorder_t5D94DFB2824E84CE27FC6412F494C4C0F8D695F2_CustomAttributesCacheGenerator_StatusUpdate,
	VideoRecorder_t5D94DFB2824E84CE27FC6412F494C4C0F8D695F2_CustomAttributesCacheGenerator_U3CIsReadyU3Ek__BackingField,
	EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_U3CInitializedU3Ek__BackingField,
	EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_U3CSchedulerU3Ek__BackingField,
	EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_U3CWorkerU3Ek__BackingField,
	EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_U3CDisplayU3Ek__BackingField,
	EasyARSettings_t3C0403A57B51DEA05B5E546CCBE92E1366725D13_CustomAttributesCacheGenerator_LicenseKey,
	EasyARSettings_t3C0403A57B51DEA05B5E546CCBE92E1366725D13_CustomAttributesCacheGenerator_IOSRecordingSupport,
	EasyARSettings_t3C0403A57B51DEA05B5E546CCBE92E1366725D13_CustomAttributesCacheGenerator_ARCoreSDK,
	AndroidManifestPermission_t673225EC0A11452F8E0ED674F66F0ADC1DFE4AD8_CustomAttributesCacheGenerator_CameraDevice,
	AndroidManifestPermission_t673225EC0A11452F8E0ED674F66F0ADC1DFE4AD8_CustomAttributesCacheGenerator_Recording,
	CloudRecognizerFrameFilter_t5A42E33A5B6E6D7C2B5A208DC1C171CAEC54F35F_CustomAttributesCacheGenerator_U3CCloudRecognizerU3Ek__BackingField,
	CloudRecognizerFrameFilter_t5A42E33A5B6E6D7C2B5A208DC1C171CAEC54F35F_CustomAttributesCacheGenerator_ServerKeyType,
	CloudRecognizerFrameFilter_t5A42E33A5B6E6D7C2B5A208DC1C171CAEC54F35F_CustomAttributesCacheGenerator_ServiceConfig,
	CloudRecognizerFrameFilter_t5A42E33A5B6E6D7C2B5A208DC1C171CAEC54F35F_CustomAttributesCacheGenerator_PrivateServiceConfig,
	ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_U3CTargetU3Ek__BackingField,
	ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageFileSource,
	ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_TargetDataFileSource,
	ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_trackerHasSet,
	ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_tracker,
	ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_TargetAvailable,
	ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_TargetLoad,
	ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_TargetUnload,
	ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_U3CTrackerU3Ek__BackingField,
	ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_simultaneousNum,
	ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_TargetLoad,
	ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_TargetUnload,
	ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_SimultaneousNumChanged,
	ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_U3CTargetU3Ek__BackingField,
	ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjFileSource,
	ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_trackerHasSet,
	ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_tracker,
	ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_TargetAvailable,
	ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_TargetLoad,
	ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_TargetUnload,
	ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_U3CTrackerU3Ek__BackingField,
	ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_simultaneousNum,
	ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_TargetLoad,
	ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_TargetUnload,
	ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_SimultaneousNumChanged,
	Optional_1_t4453CF5BCF85A5A466C7852DF1130CF811D81BE2_CustomAttributesCacheGenerator__Tag,
	ARAssembly_tD524E1F11F3744DCAA6614284A1EA98A842196F7_CustomAttributesCacheGenerator_U3CReadyU3Ek__BackingField,
	ARAssembly_tD524E1F11F3744DCAA6614284A1EA98A842196F7_CustomAttributesCacheGenerator_U3CRequireWorldCenterU3Ek__BackingField,
	ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_Assembly,
	ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_FrameChange,
	ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_FrameUpdate,
	ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_WorldRootChanged,
	ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_U3CFrameCameraParametersU3Ek__BackingField,
	TargetController_t6224059002B5A227C857B3F069CF648EB9CC317D_CustomAttributesCacheGenerator_U3CIsTrackedU3Ek__BackingField,
	TargetController_t6224059002B5A227C857B3F069CF648EB9CC317D_CustomAttributesCacheGenerator_U3CIsLoadedU3Ek__BackingField,
	WorldRootController_t3C4EEDEC8FBD39F957F99B437AF2859DC6B222B1_CustomAttributesCacheGenerator_TrackingStatusChanged,
	WorldRootController_t3C4EEDEC8FBD39F957F99B437AF2859DC6B222B1_CustomAttributesCacheGenerator_U3CTrackingStatusU3Ek__BackingField,
	DenseSpatialMapBlockController_t2782D8A2996368BF3D9AD88814219C88D3A23C35_CustomAttributesCacheGenerator_U3CInfoU3Ek__BackingField,
	DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_U3CBuilderU3Ek__BackingField,
	DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_MapCreate,
	DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_MapUpdate,
	DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_U3CTrackingStatusU3Ek__BackingField,
	DenseSpatialMapDepthRenderer_tEE0270C713C47AFCC2D26ADF47E35B8119896817_CustomAttributesCacheGenerator_U3CRenderDepthCameraU3Ek__BackingField,
	DenseSpatialMapDepthRenderer_tEE0270C713C47AFCC2D26ADF47E35B8119896817_CustomAttributesCacheGenerator_U3CMapMeshMaterialU3Ek__BackingField,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_U3CMapInfoU3Ek__BackingField,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_MapManagerSource,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_showPointCloud,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_pointCloudParticleParameter,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_mapWorkerHasSet,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_mapWorker,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_MapInfoAvailable,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_MapLocalized,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_MapStopLocalize,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_MapLoad,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_MapUnload,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_MapHost,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_U3CPointCloudU3Ek__BackingField,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_U3CIsLocalizingU3Ek__BackingField,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_U3CBuilderU3Ek__BackingField,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_U3CLocalizerU3Ek__BackingField,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_U3CManagerU3Ek__BackingField,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_ServiceConfig,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_MapLoad,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_MapUnload,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_MapHost,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_U3CTrackingStatusU3Ek__BackingField,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_U3CWorkingModeU3Ek__BackingField,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_U3CLocalizedMapU3Ek__BackingField,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_U3CBuilderMapControllerU3Ek__BackingField,
	SurfaceTrackerFrameFilter_t2BEA58EA4D95BD41028F6206C9E7990C2394382E_CustomAttributesCacheGenerator_U3CTrackerU3Ek__BackingField,
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_U3CDeviceU3Ek__BackingField,
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_DeviceCreated,
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_DeviceOpened,
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_DeviceClosed,
	DeviceUnion_t28AB5438E81AA07F093A6D1A114616FDCB540FC9_CustomAttributesCacheGenerator_U3CDeviceTypeU3Ek__BackingField,
	ARCoreSuport_tDE359F9977610D84AE1201360BA17A9A646F11C4_CustomAttributesCacheGenerator_ARCoreSuport_Start_mBFD408374D7E30C82694DBE2D3C2896217CE1006,
	U3CStartU3Ed__2_t3C6C41677E5E07C125793AA6F2216A9C847EBA6C_CustomAttributesCacheGenerator_U3CStartU3Ed__2__ctor_m76493853135E7B3E98D72CAAFD17980374605DE0,
	U3CStartU3Ed__2_t3C6C41677E5E07C125793AA6F2216A9C847EBA6C_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_IDisposable_Dispose_mF2D6ACAB9BD8472C08C90F50438A0657E8412A70,
	U3CStartU3Ed__2_t3C6C41677E5E07C125793AA6F2216A9C847EBA6C_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m42BEA4DE7F2E3D42FAC40F1BD820B34ECA178777,
	U3CStartU3Ed__2_t3C6C41677E5E07C125793AA6F2216A9C847EBA6C_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m4E31F02CE2F73E30AA546D239DA3D2DFCF2DDED3,
	U3CStartU3Ed__2_t3C6C41677E5E07C125793AA6F2216A9C847EBA6C_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_mD08A543706D0F525E2B14CA73AD25DDB7336F8B9,
	Anim_tF3468CFBF078A3F63800C8B0BEA4A5119743F8D4_CustomAttributesCacheGenerator_Anim_delay_m49B92C3EC4783A88BB915F7B9E5828E35676DFE3,
	U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3__ctor_mB41968B0DEEA004DDB90B2E10A791EB96E48D2CD,
	U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_IDisposable_Dispose_m1F06D74D5874EB191FBFA69957B9C26470ADAC4A,
	U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96FF82B4AFBECC14FD6D4D385BD51F77F6B96E26,
	U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_Collections_IEnumerator_Reset_mDDF68C47CED558B3AB02F86D903391C0BED82521,
	U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_Collections_IEnumerator_get_Current_m9E064515BE7020B0F9EF09EA13C41705F94DE64D,
	ScreenShotShare_t085704E55DC8A01DD9C51F156EC17710AA273BE7_CustomAttributesCacheGenerator_ScreenShotShare_ShareScreenshot_m246A5054CA729C909BFCC37EFD7826BDCD8CE5A1,
	U3CShareScreenshotU3Ed__7_tF87639DF68B8B53FBE67F96F08499BE172EA9023_CustomAttributesCacheGenerator_U3CShareScreenshotU3Ed__7__ctor_m272767E0AF9F078E6DCED77A092EBC7439D54C70,
	U3CShareScreenshotU3Ed__7_tF87639DF68B8B53FBE67F96F08499BE172EA9023_CustomAttributesCacheGenerator_U3CShareScreenshotU3Ed__7_System_IDisposable_Dispose_mBD94F610B2DE5685FB673119CDF7237905DD2B27,
	U3CShareScreenshotU3Ed__7_tF87639DF68B8B53FBE67F96F08499BE172EA9023_CustomAttributesCacheGenerator_U3CShareScreenshotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFCABB45518780891A94828CA071C36CF1A36E5C4,
	U3CShareScreenshotU3Ed__7_tF87639DF68B8B53FBE67F96F08499BE172EA9023_CustomAttributesCacheGenerator_U3CShareScreenshotU3Ed__7_System_Collections_IEnumerator_Reset_mBFE3415BA35E908E556C1CB1A61028BE711AB897,
	U3CShareScreenshotU3Ed__7_tF87639DF68B8B53FBE67F96F08499BE172EA9023_CustomAttributesCacheGenerator_U3CShareScreenshotU3Ed__7_System_Collections_IEnumerator_get_Current_mB20376FE895FF4BAF3C18F02580F4DE96ABBE28D,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_map_mF38403A543E4A656CDD76F759FCB6E3A17A69BAE,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoid_func_m8233FE906FAE6473609404B722CF66412E5EFB48,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoid_destroy_m9A583576B55465A17C01F9B74E416B531702AB80,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromOutputFrame_func_m142DA320BDD782F0665F95929A1FAF518AAB0B80,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromOutputFrame_destroy_m39E6790C6700E68EEB6EF662B32E84E8150EE773,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromTargetAndBool_func_m4F4E191A4A3E8C8B1C93A9A83F773843E947D3E0,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromTargetAndBool_destroy_m407203B6D0165B9CBB93CDC394623D25D1394C3E,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_func_m47D3C6B1C3FA746A6D370FBC7B1D517706FB3EFE,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_destroy_m44D24357597F4A3861061A606C66CAC57CBDE0D8,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromCloudRecognizationResult_func_m631246C2873CA0786043DC8226B7A1F69BAF973D,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromCloudRecognizationResult_destroy_mBF60D4AFDA72F76BFBE41B77BBAACB66D22FB251,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromInputFrame_func_mA15F0CAA1EFCDBBE96BA6D3A17F1AE49FFB6443F,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromInputFrame_destroy_mBC40B788EC5FA01C081A8B8F29DD83BD7C04BBE0,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromCameraState_func_mC7E9F14253350E76D81DD852C31479AE6FA97541,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromCameraState_destroy_mFF1D0D74D6DDC00A03B9F68ABD23B93BBCB212B2,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromPermissionStatusAndString_func_m8D9D32B6E616DCFD443DE289BEE2A1A9E10D63B3,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromPermissionStatusAndString_destroy_m781B495D25D988C89184294BD62865D9E801969D,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromLogLevelAndString_func_m9A78157C61E95CA37E54667EDB501049BE406D6A,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromLogLevelAndString_destroy_mF22BD5934E396FB1042F8B0E27B54DBE318FCBFE,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromRecordStatusAndString_func_mA45DA83CDE8ACEB8F46865D1B86A518290177EBA,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromRecordStatusAndString_destroy_mA47A5359B41B4FD8321FA5CC075E76AD7ABBBCD1,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromBool_func_m18B5057D02371A6A12534E44D51E79565365602A,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromBool_destroy_m3D313F4C383413AAD2FCB52140036EA67F40B985,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromBoolAndStringAndString_func_m32AF4156D52F9B1AEA2A81D03E596F56609DF977,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromBoolAndStringAndString_destroy_mA6C753EE55047102FBA63F1ABC45CE9D2B516010,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromBoolAndString_func_m4786C9B24241EBD28AAF555E9CFCB97344EFB8CB,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromBoolAndString_destroy_mDC79451BFE0F528760772F0A015FBC035E6E023E,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromVideoStatus_func_mB7B7FD5B5858F436F3466A1919AF72453E7BE34F,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromVideoStatus_destroy_mC318C3913E76C1CF3DC21561E52A312033C580E4,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromFeedbackFrame_func_mF3A0BCA98C72331241FAC3A693309EBC3570DAD7,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfVoidFromFeedbackFrame_destroy_mE0489B5AF6DB4645A89B1AA2C5E728AA2085DA06,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfOutputFrameFromListOfOutputFrame_func_m728DEA484FEF9A32AF0EA5C985684C3A923B6E93,
	Detail_tA2BACAC7FC7A866CE9E07BD94FE19F3958F72368_CustomAttributesCacheGenerator_Detail_FunctorOfOutputFrameFromListOfOutputFrame_destroy_m3213152A87BBAB55CB406BB9D08FB45301A33151,
	CameraImageRenderer_tA2755D03A0C4CF10F91C9346A6AF8C8BE13F5B24_CustomAttributesCacheGenerator_CameraImageRenderer_add_OnFrameRenderUpdate_m51A0C232CFF90B955C67688FDF8BB86EA34997D7,
	CameraImageRenderer_tA2755D03A0C4CF10F91C9346A6AF8C8BE13F5B24_CustomAttributesCacheGenerator_CameraImageRenderer_remove_OnFrameRenderUpdate_mC9D3AE9A0D3839A54144AF9F19A5CD119EB3DB34,
	CameraImageRenderer_tA2755D03A0C4CF10F91C9346A6AF8C8BE13F5B24_CustomAttributesCacheGenerator_CameraImageRenderer_add_TargetTextureChange_mAF7C7E5E3E3C695B331C152F9B904C8C3B7FD14F,
	CameraImageRenderer_tA2755D03A0C4CF10F91C9346A6AF8C8BE13F5B24_CustomAttributesCacheGenerator_CameraImageRenderer_remove_TargetTextureChange_m78E2A24B9CA80FDBE52FA01D1B9A6A551013A8A3,
	DisplayEmulator_tD487D2897CEC3D0F45AD4B4791AD6BEC28798EE7_CustomAttributesCacheGenerator_DisplayEmulator_get_Rotation_mB9E15EBA1BFAE08CB03D0509C103E91E1BED84C0,
	DisplayEmulator_tD487D2897CEC3D0F45AD4B4791AD6BEC28798EE7_CustomAttributesCacheGenerator_DisplayEmulator_set_Rotation_m5626FF085412A9BF4F86CDC79D842FD29A94F305,
	RenderCameraController_t8DCAC8D2FE426132342CB3E9517C741CC5B73D1E_CustomAttributesCacheGenerator_RenderCameraController_U3COnFrameUpdateU3Eb__15_0_mB648F9A4567EEEE592C4E8366B39BB4C11C24152,
	RenderCameraController_t8DCAC8D2FE426132342CB3E9517C741CC5B73D1E_CustomAttributesCacheGenerator_RenderCameraController_U3COnFrameUpdateU3Eb__15_1_m2E3D602A08DF91717909CC18E505A4AB28907002,
	RenderCameraEventHandler_t7702C29BFBB9E141AD5D1D89B44020E9ABC3EF90_CustomAttributesCacheGenerator_RenderCameraEventHandler_add_PreRender_m2B5CB28BCD7FD08991DA0C37A3FD8572B5B50FE8,
	RenderCameraEventHandler_t7702C29BFBB9E141AD5D1D89B44020E9ABC3EF90_CustomAttributesCacheGenerator_RenderCameraEventHandler_remove_PreRender_m170313DE9016B8BE301148361DB4385E5B7102F1,
	RenderCameraEventHandler_t7702C29BFBB9E141AD5D1D89B44020E9ABC3EF90_CustomAttributesCacheGenerator_RenderCameraEventHandler_add_PostRender_mAE8817443F0267F4799FF8FD04DC718B7799C2EC,
	RenderCameraEventHandler_t7702C29BFBB9E141AD5D1D89B44020E9ABC3EF90_CustomAttributesCacheGenerator_RenderCameraEventHandler_remove_PostRender_mB83C13F707EB6CBA4786BD8C6AD3D580680004D3,
	RenderCameraParameters_t50DFBE92E83834EDB3ABFA78FF9D7B03A0E5F316_CustomAttributesCacheGenerator_RenderCameraParameters_get_Transform_m3E7E68A87D3D8F5EFCC7A209DCD42545BF62B0DD,
	RenderCameraParameters_t50DFBE92E83834EDB3ABFA78FF9D7B03A0E5F316_CustomAttributesCacheGenerator_RenderCameraParameters_set_Transform_m72CB53FC12350A7F3F15769ACE79F46F9F481708,
	RenderCameraParameters_t50DFBE92E83834EDB3ABFA78FF9D7B03A0E5F316_CustomAttributesCacheGenerator_RenderCameraParameters_get_Parameters_m0EDC1415649355013C64BBAABEEE13E84178A618,
	RenderCameraParameters_t50DFBE92E83834EDB3ABFA78FF9D7B03A0E5F316_CustomAttributesCacheGenerator_RenderCameraParameters_set_Parameters_m573A60BFD02F1DAF1316611AEAC9520681530AE7,
	VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_VideoCameraDevice_get_Device_m926B3C984C4B5D61119460CA890229146FDE6355,
	VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_VideoCameraDevice_set_Device_m79AD8296E6911D67EE9443AF8002911C250CD6AE,
	VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_VideoCameraDevice_add_DeviceCreated_m3E212D2F198B64A65897F769F074B15BEBFFAA36,
	VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_VideoCameraDevice_remove_DeviceCreated_m515E14F76CBAA5895B5E25B38A206DD27F0A5903,
	VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_VideoCameraDevice_add_DeviceOpened_m77F110C7EC10229FA0081A405A144D24F9403DE1,
	VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_VideoCameraDevice_remove_DeviceOpened_mB4CD73BD43608CCAE16B64BCC970E50C3F2C1B50,
	VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_VideoCameraDevice_add_DeviceClosed_m3859CE363FA4A25C778697DF921AF7FEEA05BD33,
	VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_VideoCameraDevice_remove_DeviceClosed_m65127FC471D2D556004BC3FCF95979A663D9FAC6,
	VideoCameraDevice_t9DF2B7A7DCF0994B6F33107AB5783A099C835B51_CustomAttributesCacheGenerator_VideoCameraDevice_U3COpenU3Eb__36_0_m190320BF0CC8352DE99FFCD853BC7D15C35AAC9D,
	VideoRecorder_t5D94DFB2824E84CE27FC6412F494C4C0F8D695F2_CustomAttributesCacheGenerator_VideoRecorder_add_StatusUpdate_m063C7113C310771CDE2D387DC7F8BB834660BB86,
	VideoRecorder_t5D94DFB2824E84CE27FC6412F494C4C0F8D695F2_CustomAttributesCacheGenerator_VideoRecorder_remove_StatusUpdate_m2A7DD8BCF5AEAC7DD6065798C822E87B2FF420B1,
	VideoRecorder_t5D94DFB2824E84CE27FC6412F494C4C0F8D695F2_CustomAttributesCacheGenerator_VideoRecorder_get_IsReady_mEC576F4B82E4DDA3400B8EF8593EAC7429F2639C,
	VideoRecorder_t5D94DFB2824E84CE27FC6412F494C4C0F8D695F2_CustomAttributesCacheGenerator_VideoRecorder_set_IsReady_m45B7BD7D9955CF1B069513E7E670DED48E63FE5E,
	VideoRecorder_t5D94DFB2824E84CE27FC6412F494C4C0F8D695F2_CustomAttributesCacheGenerator_VideoRecorder_U3CStartU3Eb__14_0_m19D1B3C3E07883B5B2F14E30A2B681AAB83DBAB6,
	VideoRecorder_t5D94DFB2824E84CE27FC6412F494C4C0F8D695F2_CustomAttributesCacheGenerator_VideoRecorder_U3CStartRecordingU3Eb__17_0_m2BA06D3E64FB113F664EBC97423BCCA9FA5BDE1A,
	EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_get_Instance_m4D9BEEAA229476C1639FC24DA46A94A5620A0D30,
	EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_set_Instance_m0644DDA7B23B4DF33F6CA10E1EBBAF23EC11AAC5,
	EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_get_Initialized_m0FCA276095BC052B4B970D3D1354FB1E6AA98C41,
	EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_set_Initialized_mFD745ACB6C6E691DABA81F36CBC5525734185717,
	EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_get_Scheduler_mC7D3A3E19D863258A8AEF0905F37C16AE54760E6,
	EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_set_Scheduler_m5DB7C2DB70602A75751F494AB8DB5B86ABBFB15F,
	EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_get_Worker_m7EC07F0D41BF1A762F1B21D68779BA48B00B3E9E,
	EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_set_Worker_mFB4F26C6EA7D7CA0AA5D741339D8E34538D1EC91,
	EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_get_Display_mD3A3DC439FCED5221F7A096AF46AB2EB43D4A32E,
	EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_set_Display_m8172B0A0B2853E66E36B9EEA28F929E6D08EC73D,
	EasyARController_t86F454DEC906B71A982CD3050032B0E4836EFEE1_CustomAttributesCacheGenerator_EasyARController_GlobalInitialization_mB63A67532D901C55F5B3A086D12E443A395FB87F,
	CloudRecognizerFrameFilter_t5A42E33A5B6E6D7C2B5A208DC1C171CAEC54F35F_CustomAttributesCacheGenerator_CloudRecognizerFrameFilter_get_CloudRecognizer_m354F93CE6C51A14562194DE2BCEA51F27EE49D5F,
	CloudRecognizerFrameFilter_t5A42E33A5B6E6D7C2B5A208DC1C171CAEC54F35F_CustomAttributesCacheGenerator_CloudRecognizerFrameFilter_set_CloudRecognizer_m63F102BA1221D4CB3E75A63FB2E693A3B4B054F0,
	ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_get_Target_m0F1C3C12D9FBF9ADC0AEB5320D86F30D19C0D811,
	ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_set_Target_m691E8B52130CF50D8A749C600CF8E1A9DFC5442B,
	ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_add_TargetAvailable_m9467BEDC472B4D21DEEC28741A0CEAFB29DFA820,
	ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_remove_TargetAvailable_m8329089F509E9519EFF04A625CC1666E5A635B50,
	ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_add_TargetLoad_m0D7180BCFF4AEAEB052DD4EA929097BE08EF188C,
	ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_remove_TargetLoad_m02E72F6F67A3C78E8AFB012F713611088C6AF932,
	ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_add_TargetUnload_mF3BD3D47A1BA72C2A2A9A5FBAEDD875AA78AC24B,
	ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_remove_TargetUnload_mFC4F4CC13F66062F92A926CB7B71B135F11D9B4D,
	ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_LoadImageBuffer_mB52CC13907B585A80CD0E74F48F4AC4D04F9CF86,
	ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_LoadTargetDataBuffer_mDCC3A373F4D20BBB75237CAA55538F03F15E976A,
	ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_U3CLoadTargetDataFileU3Eb__35_0_m376DC236B4B665D74CB1642A180D1DF362DB8FA5,
	ImageTargetController_tB47E118F8DF8B20256D4956349E7622200538A22_CustomAttributesCacheGenerator_ImageTargetController_U3CUpdateTargetInTrackerU3Eb__39_0_mC5711419892B651ACFD0BF21371CB426D3C5C10A,
	U3CLoadImageBufferU3Ed__37_t208A68FB14AD7CFAB1F73742F2CC2043D0958615_CustomAttributesCacheGenerator_U3CLoadImageBufferU3Ed__37__ctor_m316EB6054CB5AECB04721EA523876CB3B8F7CFF4,
	U3CLoadImageBufferU3Ed__37_t208A68FB14AD7CFAB1F73742F2CC2043D0958615_CustomAttributesCacheGenerator_U3CLoadImageBufferU3Ed__37_System_IDisposable_Dispose_m9549C468CA4876B3B335D784700FB6C36890511D,
	U3CLoadImageBufferU3Ed__37_t208A68FB14AD7CFAB1F73742F2CC2043D0958615_CustomAttributesCacheGenerator_U3CLoadImageBufferU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA37C5AAEACB0AA727307F7BE6217A3E2B3EDD78B,
	U3CLoadImageBufferU3Ed__37_t208A68FB14AD7CFAB1F73742F2CC2043D0958615_CustomAttributesCacheGenerator_U3CLoadImageBufferU3Ed__37_System_Collections_IEnumerator_Reset_mE9E76167788A27BAF0B470EBF78DE2E3BD92946F,
	U3CLoadImageBufferU3Ed__37_t208A68FB14AD7CFAB1F73742F2CC2043D0958615_CustomAttributesCacheGenerator_U3CLoadImageBufferU3Ed__37_System_Collections_IEnumerator_get_Current_m0A268429535E1FFCB1091F7B5CE73BBB1AC6FD49,
	U3CLoadTargetDataBufferU3Ed__38_tD425E9564475563A48477E1A063BB693827A6844_CustomAttributesCacheGenerator_U3CLoadTargetDataBufferU3Ed__38__ctor_m77E8BEB2D46B0FC1D095FFEB0CA65C72FC0C3C3C,
	U3CLoadTargetDataBufferU3Ed__38_tD425E9564475563A48477E1A063BB693827A6844_CustomAttributesCacheGenerator_U3CLoadTargetDataBufferU3Ed__38_System_IDisposable_Dispose_mED0D6ADF10935E5905B204001A364D6B3267C858,
	U3CLoadTargetDataBufferU3Ed__38_tD425E9564475563A48477E1A063BB693827A6844_CustomAttributesCacheGenerator_U3CLoadTargetDataBufferU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m965E6D287A33DAA0DA332F7023DC1E356F17F0F0,
	U3CLoadTargetDataBufferU3Ed__38_tD425E9564475563A48477E1A063BB693827A6844_CustomAttributesCacheGenerator_U3CLoadTargetDataBufferU3Ed__38_System_Collections_IEnumerator_Reset_m191C863FD554A6ABFCCBDDA70C1209F28B0B32D8,
	U3CLoadTargetDataBufferU3Ed__38_tD425E9564475563A48477E1A063BB693827A6844_CustomAttributesCacheGenerator_U3CLoadTargetDataBufferU3Ed__38_System_Collections_IEnumerator_get_Current_mC4E576DFA43B13AFBD8812DF07ECD41A9EC5B13A,
	ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_ImageTrackerFrameFilter_get_Tracker_m921C5DE4FA7D22775D7A0DBF9E39BB6D283E765F,
	ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_ImageTrackerFrameFilter_set_Tracker_m0C3EA99FAF9A0B03278CE3C9127E3FEFC7CDBDB5,
	ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_ImageTrackerFrameFilter_add_TargetLoad_mAC6919010D498254A50B080CD9E7D06BA4263FBD,
	ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_ImageTrackerFrameFilter_remove_TargetLoad_m3578D6764A9429D55F53A2702D352A6157507745,
	ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_ImageTrackerFrameFilter_add_TargetUnload_m9784EAE4580C2EA58AF655BBC2CDF523A78EA59F,
	ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_ImageTrackerFrameFilter_remove_TargetUnload_m55826DC7A7DD08B92535FE77F01F5A9D03343DC2,
	ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_ImageTrackerFrameFilter_add_SimultaneousNumChanged_mE16608F77817AA1C35B60C4C55F8CB86B88F1480,
	ImageTrackerFrameFilter_t95F6A6C65014484B1E046A37C539C86AA40211A1_CustomAttributesCacheGenerator_ImageTrackerFrameFilter_remove_SimultaneousNumChanged_m96B16C9C0818E2E2EB5916212E096CAE78BF02CA,
	ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjectTargetController_get_Target_m6A2FD3B44FC41DA411C2E3944988452E2733321B,
	ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjectTargetController_set_Target_mCE5C95E720E26090653430B9C1FBF667388A4BDC,
	ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjectTargetController_add_TargetAvailable_mCAC4FBCFA1A94B4CF3C6B5733E78246AE971B0AD,
	ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjectTargetController_remove_TargetAvailable_mB4F3B4DA1CE06FC264792F198ACDF17893EDB3F7,
	ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjectTargetController_add_TargetLoad_m8BC5700373C419E7FD833DFE691298443DE9BBE6,
	ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjectTargetController_remove_TargetLoad_mED573C2E2B49FBAE05701F23B2DFE9D8D884C54F,
	ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjectTargetController_add_TargetUnload_m754EF6ECA76B0D74D7FFABAE46697A4F4C9BA7B7,
	ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjectTargetController_remove_TargetUnload_m636650C9CA43F4DBA220C8DFE0134C1229C16907,
	ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjectTargetController_LoadObjFileFromSource_mD45D611FC2B2F282424481E62F8CD8A9852F9DCB,
	ObjectTargetController_t30CF1B02CAA4D6F44D09485112C1796139D566E3_CustomAttributesCacheGenerator_ObjectTargetController_U3CUpdateTargetInTrackerU3Eb__36_0_m908DC12A830F0908B6ADFC0F462C6661446B896D,
	U3CLoadObjFileFromSourceU3Ed__35_tD2FA01F73EFBF81483C3B2F7723FB1FB87B09C3F_CustomAttributesCacheGenerator_U3CLoadObjFileFromSourceU3Ed__35__ctor_m7A6A05D9063B632855EC5CA6BA48A96F054B8BFE,
	U3CLoadObjFileFromSourceU3Ed__35_tD2FA01F73EFBF81483C3B2F7723FB1FB87B09C3F_CustomAttributesCacheGenerator_U3CLoadObjFileFromSourceU3Ed__35_System_IDisposable_Dispose_m5A04B92ED61DC373ACAB6C6D5078C3FCFB50E888,
	U3CLoadObjFileFromSourceU3Ed__35_tD2FA01F73EFBF81483C3B2F7723FB1FB87B09C3F_CustomAttributesCacheGenerator_U3CLoadObjFileFromSourceU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m88255FE97B48643ACC9CE2B55EDD4FBCC117AABC,
	U3CLoadObjFileFromSourceU3Ed__35_tD2FA01F73EFBF81483C3B2F7723FB1FB87B09C3F_CustomAttributesCacheGenerator_U3CLoadObjFileFromSourceU3Ed__35_System_Collections_IEnumerator_Reset_mC27E618A38368C2677E9B791ADDF7563C7000A66,
	U3CLoadObjFileFromSourceU3Ed__35_tD2FA01F73EFBF81483C3B2F7723FB1FB87B09C3F_CustomAttributesCacheGenerator_U3CLoadObjFileFromSourceU3Ed__35_System_Collections_IEnumerator_get_Current_mA8A08EB1A756E18E934D954AA573F97B8EE915C3,
	ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_ObjectTrackerFrameFilter_get_Tracker_mA297A04811AA9A4D809E939C83CFB45579E0B960,
	ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_ObjectTrackerFrameFilter_set_Tracker_m3B1E1011F4FCBE741BA781359354DB7308564A2A,
	ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_ObjectTrackerFrameFilter_add_TargetLoad_m873A78DB962501BC4DD7523EA6949F455BE30A24,
	ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_ObjectTrackerFrameFilter_remove_TargetLoad_m82D9803297B69CF5AFEAB7AA0F12CF44AE7F52C4,
	ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_ObjectTrackerFrameFilter_add_TargetUnload_mA1C4BB8E00F52698BE5A5A21B5103BC342D4EE17,
	ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_ObjectTrackerFrameFilter_remove_TargetUnload_m091E7E675C61AC4B2D7E4785726A5F25BA57E33D,
	ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_ObjectTrackerFrameFilter_add_SimultaneousNumChanged_m365A36A03D58A673DF10564813B2DB35C9306846,
	ObjectTrackerFrameFilter_t163B60215623FC001D06E6806179AC2499F12F30_CustomAttributesCacheGenerator_ObjectTrackerFrameFilter_remove_SimultaneousNumChanged_m4571D7E0F96DCD389C730718FDDEA39C507F7971,
	APIExtend_t576D4C83D728565EDDB31F57223C146FB3C99531_CustomAttributesCacheGenerator_APIExtend_ToUnityMatrix_m206A3ACA63D8BB98326D5DFB039F3FF9DED64550,
	APIExtend_t576D4C83D728565EDDB31F57223C146FB3C99531_CustomAttributesCacheGenerator_APIExtend_ToEasyARVector_mE2724A0CB62CD9CDEAE1046A8B79894F648985F1,
	APIExtend_t576D4C83D728565EDDB31F57223C146FB3C99531_CustomAttributesCacheGenerator_APIExtend_ToEasyARVector_m9600E8DE018615DF428475EA76401EFC81B2A242,
	APIExtend_t576D4C83D728565EDDB31F57223C146FB3C99531_CustomAttributesCacheGenerator_APIExtend_ToUnityVector_mF8D534B98643DC1F8A61AD769F2246627141661F,
	APIExtend_t576D4C83D728565EDDB31F57223C146FB3C99531_CustomAttributesCacheGenerator_APIExtend_ToUnityVector_m091751C847170124879708574534A1AF98CE92CD,
	FileUtil_t577E197EC4ADE41E2545260BA9531D460E966A1F_CustomAttributesCacheGenerator_FileUtil_LoadFile_m4AC9BF927CBA7B25789521DD69A1D0BC5CFC5919,
	U3CLoadFileU3Ed__2_t66473629DE12D0A0305ED769CBC746239B46CB51_CustomAttributesCacheGenerator_U3CLoadFileU3Ed__2__ctor_m7DAF075882D1195526359B33F1A62ED93E14812D,
	U3CLoadFileU3Ed__2_t66473629DE12D0A0305ED769CBC746239B46CB51_CustomAttributesCacheGenerator_U3CLoadFileU3Ed__2_System_IDisposable_Dispose_mCC55578A4367C5E8FD2DB846C963A4C38CABBD79,
	U3CLoadFileU3Ed__2_t66473629DE12D0A0305ED769CBC746239B46CB51_CustomAttributesCacheGenerator_U3CLoadFileU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4177F95AB6FB19CBE473039096963BB7E9635A57,
	U3CLoadFileU3Ed__2_t66473629DE12D0A0305ED769CBC746239B46CB51_CustomAttributesCacheGenerator_U3CLoadFileU3Ed__2_System_Collections_IEnumerator_Reset_mA8A4FE670BD8AFE9610995CDDB1CA59D0A3C447A,
	U3CLoadFileU3Ed__2_t66473629DE12D0A0305ED769CBC746239B46CB51_CustomAttributesCacheGenerator_U3CLoadFileU3Ed__2_System_Collections_IEnumerator_get_Current_mCBCAEC97C03DA3D9F1F58642A8A252E93C8FE366,
	GUIPopup_t8A4FEBFFF6F240BEF4708E89CA472852144D97C1_CustomAttributesCacheGenerator_GUIPopup_ShowMessage_mD81F2280B13C2F0CA60575456D4707531137D1F6,
	U3CShowMessageU3Ed__8_tFBE033A689E35901144A3D3D4F1915C69163FCB2_CustomAttributesCacheGenerator_U3CShowMessageU3Ed__8__ctor_mAB6F5B533E4E6F9E8A40EBAF7760CE490A96C8C9,
	U3CShowMessageU3Ed__8_tFBE033A689E35901144A3D3D4F1915C69163FCB2_CustomAttributesCacheGenerator_U3CShowMessageU3Ed__8_System_IDisposable_Dispose_mE9E1ED495359DED79D0F13CD157EBDE403127D68,
	U3CShowMessageU3Ed__8_tFBE033A689E35901144A3D3D4F1915C69163FCB2_CustomAttributesCacheGenerator_U3CShowMessageU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4ACF3C5DA26FB7206A9E7D9F1716CF3DCF4C5B3B,
	U3CShowMessageU3Ed__8_tFBE033A689E35901144A3D3D4F1915C69163FCB2_CustomAttributesCacheGenerator_U3CShowMessageU3Ed__8_System_Collections_IEnumerator_Reset_mD67A86850AFFE89CC4F85E8D08C43BDEDE9DA34F,
	U3CShowMessageU3Ed__8_tFBE033A689E35901144A3D3D4F1915C69163FCB2_CustomAttributesCacheGenerator_U3CShowMessageU3Ed__8_System_Collections_IEnumerator_get_Current_mCA7C149643CD2D0E0987E92DD490202826DA5818,
	ThreadWorker_tD00F16D21225F93A49F2019B5FB7EE85C1EFEAE2_CustomAttributesCacheGenerator_ThreadWorker_U3CCreateThreadU3Eb__6_0_m80AF9A8C9FD34024464AB15ADD3C185E5600DD6B,
	ARAssembly_tD524E1F11F3744DCAA6614284A1EA98A842196F7_CustomAttributesCacheGenerator_ARAssembly_get_Ready_mE0D7D9A363183508328A72FA7C587F88859F5A95,
	ARAssembly_tD524E1F11F3744DCAA6614284A1EA98A842196F7_CustomAttributesCacheGenerator_ARAssembly_set_Ready_m4628605213B7465AA5147702C0AAA558D806DADF,
	ARAssembly_tD524E1F11F3744DCAA6614284A1EA98A842196F7_CustomAttributesCacheGenerator_ARAssembly_get_RequireWorldCenter_m0C74C6CC8937D31FFE68E706A2F279E258C4DA2F,
	ARAssembly_tD524E1F11F3744DCAA6614284A1EA98A842196F7_CustomAttributesCacheGenerator_ARAssembly_set_RequireWorldCenter_m7C8C2237D1D06E9E9FEDCE171B6F758FEE8094D9,
	ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_ARSession_add_FrameChange_m77548873BBFE5CEE577894326237892B05F413E0,
	ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_ARSession_remove_FrameChange_mC3AEE88DD4EF22F919C173CFA09EC594ECF29F2E,
	ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_ARSession_add_FrameUpdate_m3CFB06B201EC6F834CA4ABF4F200695E4FACB08A,
	ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_ARSession_remove_FrameUpdate_mDB1136F48109CC9DA0B48657022D986E0AC516D9,
	ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_ARSession_add_WorldRootChanged_m7FD7A4B2571EB42293F054A5C23127CB960D6FDE,
	ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_ARSession_remove_WorldRootChanged_m5AFE86652B914D379381B33B0C64DB303D8DE049,
	ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_ARSession_get_FrameCameraParameters_m894DA7ABA5970C3B7EEE05BEA3FE8598C3D276B5,
	ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0_CustomAttributesCacheGenerator_ARSession_set_FrameCameraParameters_mEA8C1C870A72E8AD0C7C1B6845FE332624923178,
	TargetController_t6224059002B5A227C857B3F069CF648EB9CC317D_CustomAttributesCacheGenerator_TargetController_get_IsTracked_mCDCD168D5AE7E4FBB89191F60A086BB2159C02B9,
	TargetController_t6224059002B5A227C857B3F069CF648EB9CC317D_CustomAttributesCacheGenerator_TargetController_set_IsTracked_mEB0AC36E8C682FA8B2D00B02E65CAE450F40741F,
	TargetController_t6224059002B5A227C857B3F069CF648EB9CC317D_CustomAttributesCacheGenerator_TargetController_get_IsLoaded_m74BD4BCDAC8BDA2A60482DB96BEC34F944E38D51,
	TargetController_t6224059002B5A227C857B3F069CF648EB9CC317D_CustomAttributesCacheGenerator_TargetController_set_IsLoaded_m7C2707C8D99CAAFC51A250614A4790FD43D99472,
	WorldRootController_t3C4EEDEC8FBD39F957F99B437AF2859DC6B222B1_CustomAttributesCacheGenerator_WorldRootController_add_TrackingStatusChanged_mD2CE170147C8A715C42BBD4EF9951AB7C9977A51,
	WorldRootController_t3C4EEDEC8FBD39F957F99B437AF2859DC6B222B1_CustomAttributesCacheGenerator_WorldRootController_remove_TrackingStatusChanged_m0FF0A489070271B1B9B65CB749D396FE388D7508,
	WorldRootController_t3C4EEDEC8FBD39F957F99B437AF2859DC6B222B1_CustomAttributesCacheGenerator_WorldRootController_get_TrackingStatus_m93D0D79F1E994459E25C2692A96D498D1599EB5E,
	WorldRootController_t3C4EEDEC8FBD39F957F99B437AF2859DC6B222B1_CustomAttributesCacheGenerator_WorldRootController_set_TrackingStatus_m1573ED629C5CAA28C2907D85CE636576BBF05968,
	DenseSpatialMapBlockController_t2782D8A2996368BF3D9AD88814219C88D3A23C35_CustomAttributesCacheGenerator_DenseSpatialMapBlockController_get_Info_m9B5FAAE3691BEA4AAA516AF8BC75A971D265267F,
	DenseSpatialMapBlockController_t2782D8A2996368BF3D9AD88814219C88D3A23C35_CustomAttributesCacheGenerator_DenseSpatialMapBlockController_set_Info_m13F0699BCA49F39EB168F49AE2199DD7CC7ABFFC,
	DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_DenseSpatialMapBuilderFrameFilter_get_Builder_m758140881A3D5EB2FDD98AEF2DBE5412150F6ABD,
	DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_DenseSpatialMapBuilderFrameFilter_set_Builder_m737FCF073162DE6634880B16969D488F26D0A7C6,
	DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_DenseSpatialMapBuilderFrameFilter_add_MapCreate_m50792A04FFC1A0E331C87EBAD4429A612F55392D,
	DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_DenseSpatialMapBuilderFrameFilter_remove_MapCreate_m672731B5112CA728EAC9F8C238D9B1BEEB4C13FB,
	DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_DenseSpatialMapBuilderFrameFilter_add_MapUpdate_m2DED4A7DD8A17E497F480B12B1F438CDE6099BBC,
	DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_DenseSpatialMapBuilderFrameFilter_remove_MapUpdate_m210F0D63D0E6A2AC2A3B88A55296E30729EEDA18,
	DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_DenseSpatialMapBuilderFrameFilter_get_TrackingStatus_m3E17B76E936462D507DD2C5DDC6F6EA56BF48F03,
	DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_DenseSpatialMapBuilderFrameFilter_set_TrackingStatus_m6FB6FEC5F485ECC8E81867E643274C679E6FB8E1,
	DenseSpatialMapBuilderFrameFilter_t5619F10497A36E16247E5AB156D543C0DFCFC1AC_CustomAttributesCacheGenerator_DenseSpatialMapBuilderFrameFilter_U3COnAssembleU3Eb__41_0_mB7091E6C9A2299026A8DBA9B6790D69D225AEB91,
	DenseSpatialMapDepthRenderer_tEE0270C713C47AFCC2D26ADF47E35B8119896817_CustomAttributesCacheGenerator_DenseSpatialMapDepthRenderer_get_RenderDepthCamera_m681A99424571E53FC47EE5FA06DA4662882FC1F2,
	DenseSpatialMapDepthRenderer_tEE0270C713C47AFCC2D26ADF47E35B8119896817_CustomAttributesCacheGenerator_DenseSpatialMapDepthRenderer_set_RenderDepthCamera_m492EF4A696EF204AD15D3E94CF170B8E9ABD6F45,
	DenseSpatialMapDepthRenderer_tEE0270C713C47AFCC2D26ADF47E35B8119896817_CustomAttributesCacheGenerator_DenseSpatialMapDepthRenderer_get_MapMeshMaterial_m0BBD1D7CD30168722CCD1758403766320276DF87,
	DenseSpatialMapDepthRenderer_tEE0270C713C47AFCC2D26ADF47E35B8119896817_CustomAttributesCacheGenerator_DenseSpatialMapDepthRenderer_set_MapMeshMaterial_mC72C0357A3454D90FE3AD3493D0ADE32C158EF36,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_get_MapInfo_m9F7973AF582AB3D534F5A78C60A5B9EA4FF77A09,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_set_MapInfo_m9A2E132F01CDA34B585D1393BDB53914392E6148,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_add_MapInfoAvailable_m2862A31F3E802A743EB2EFD089685CAAEE2ABD56,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_remove_MapInfoAvailable_mE56EBAB6010C146C210663CDF819CD10602D897F,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_add_MapLocalized_m1E916CEF68A96614F5550D3ED1E13B5BAE9F64B3,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_remove_MapLocalized_m9B100A0D840C4240F4EE3D07078C36B28E4CC18C,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_add_MapStopLocalize_m47AD420BFD0385FEEFC65E3FE60316C2A3BA65F6,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_remove_MapStopLocalize_mA18DA2D96E3D564B641366E2D1878197A572F88C,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_add_MapLoad_m1EAD1D28F05893F5F24E830D895C07650119676E,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_remove_MapLoad_m948B3F5628A733F6ED2E96668F4FFCFACD494333,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_add_MapUnload_m2FB811A896D21DDF1CE0365F4672BF245B99F69F,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_remove_MapUnload_m662469C6CE6B9C1ADCE099947BF8FB0AAAE3883B,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_add_MapHost_mDD77B7155B17BBB8F9A4737B6C8F03CE408208BE,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_remove_MapHost_m053C92797E2D6D610C67E10ECB87A00ADC7459B2,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_get_PointCloud_m9E47A3C47C14727138FFB5EF22CFB19EED8DDC83,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_set_PointCloud_m5741878569506D3FF8A9541478C658577DA831EF,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_get_IsLocalizing_m18ECA11E492F62E020E84D70D19E52FAA5418A14,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_set_IsLocalizing_m3953314E7CC78205D195598D224B1FA8C7F0885E,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_U3CHostU3Eb__56_0_m3CAEB4EC3D350CCFA68C581013A003178E786564,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_U3CUpdatePointCloudU3Eb__59_0_m02E8F1ACE41A94F38155BAB09A9EC88EED9B1202,
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758_CustomAttributesCacheGenerator_SparseSpatialMapController_U3CUpdateMapInLocalizerU3Eb__63_0_mF3859885855B1D4DAE5DE4A90F9F337DB9E26733,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_get_Builder_mBDD7FD5C1D5874ECEA16C70E90EBB24B60A156D2,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_set_Builder_m8FB3708CF2035B1B9F3143EF73679478F41D533B,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_get_Localizer_m481D0F8E9147E444A72E4E5314E274A149015FFD,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_set_Localizer_mF300368DA1D996D55EE36DCDB612A2A19A1205FF,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_get_Manager_m3FEB082165C0C8373D273049674055AF80FEA82C,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_set_Manager_mD110FC2EDB5EF821F0FE1CB19AAACD3EB92927CD,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_add_MapLoad_m9761FF716AD4AB2E7B31B8B95F167D55A743EEDF,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_remove_MapLoad_mF1EA708E880E9F28E711B6189275A1549F55E704,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_add_MapUnload_mACB33B7581BAAAE760DDC69C6C91034978F21414,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_remove_MapUnload_m0AD3F4D12187B774221F0895C7DDC7B60831EC7E,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_add_MapHost_mB906F4AF336AFAB07D645A0EEAEFCE1516CE5727,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_remove_MapHost_mF4B8E4EEC4D44230DAD3148CB92E1A7744B279B7,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_get_TrackingStatus_mF01A0D9EADE683AA43A9A09567B9C29CA231F300,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_set_TrackingStatus_mFEA33C5312DB9C0A699EE011CF98CF4955465D9A,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_get_WorkingMode_mF8E9D3D97D5F1BC30860D7CAD2733F96EC97DE84,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_set_WorkingMode_m149AC1EE7A6BAEF8771908A6BD493F27AD0BF5CE,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_get_LocalizedMap_m58350A5B898D5E2D5437F76AD6B670CDC0BFC55F,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_set_LocalizedMap_m2006E253D0EE4E6D526491D2287EC70DBE4D6353,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_get_BuilderMapController_m3B422BADE125C970974568C6AEA40AA76B33988A,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_set_BuilderMapController_mF5A767EE02F7F72607BC17A0A188478C3BF3AF88,
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2_CustomAttributesCacheGenerator_SparseSpatialMapWorkerFrameFilter_U3COnAssembleU3Eb__62_0_m1C6E2C8CF69B82D1DF8CE6FBECC0F00C9EA2AA15,
	SurfaceTrackerFrameFilter_t2BEA58EA4D95BD41028F6206C9E7990C2394382E_CustomAttributesCacheGenerator_SurfaceTrackerFrameFilter_get_Tracker_m3515EE44BB36EDE69EA10E7DFB43725F308B37E8,
	SurfaceTrackerFrameFilter_t2BEA58EA4D95BD41028F6206C9E7990C2394382E_CustomAttributesCacheGenerator_SurfaceTrackerFrameFilter_set_Tracker_mE4E2C8642D3A95893D082229CB198D89059ACCB8,
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_VIOCameraDeviceUnion_get_Device_mFE46D8F19218FECD9FF165FE6E9759992BD707AA,
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_VIOCameraDeviceUnion_set_Device_m9B7BA8186E8CA67F5674BBA422FC088F10786232,
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_VIOCameraDeviceUnion_add_DeviceCreated_m74AD89492D2711324561C8E330BADB762414D47C,
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_VIOCameraDeviceUnion_remove_DeviceCreated_m814DE270549834E858D6F5DA0DA2998774673B23,
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_VIOCameraDeviceUnion_add_DeviceOpened_m19356CB413BC5584A315200A6C88CCDD82736F90,
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_VIOCameraDeviceUnion_remove_DeviceOpened_m23CAF95355804DDDF8E0FABA8BC358D9638813E6,
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_VIOCameraDeviceUnion_add_DeviceClosed_m5B573063D9E27DA34B996A6C0220A4F4579AE97B,
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_VIOCameraDeviceUnion_remove_DeviceClosed_m29E68A81FFAEB479EE86B4F072F416A8CE65B1EA,
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E_CustomAttributesCacheGenerator_VIOCameraDeviceUnion_U3COpenU3Eb__35_0_m6EF254CCD5BFA74F72BCE71EA2E0C494780258A0,
	DeviceUnion_t28AB5438E81AA07F093A6D1A114616FDCB540FC9_CustomAttributesCacheGenerator_DeviceUnion_get_DeviceType_m3C4A65E1E009312D2B43DA6C9A89ADBDFC457536,
	DeviceUnion_t28AB5438E81AA07F093A6D1A114616FDCB540FC9_CustomAttributesCacheGenerator_DeviceUnion_set_DeviceType_m07F63903240ECF385A4C8E518FC8A7733BFF528C,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
