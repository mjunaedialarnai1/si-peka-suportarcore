﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50;
// System.String
struct String_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rngAccess_12), (void*)value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rng_13), (void*)value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____fastRng_14), (void*)value);
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.RectInt
struct  RectInt_tE7B8105A280C1AC73A4157ED41F9B86C9BD91E49 
{
public:
	// System.Int32 UnityEngine.RectInt::m_XMin
	int32_t ___m_XMin_0;
	// System.Int32 UnityEngine.RectInt::m_YMin
	int32_t ___m_YMin_1;
	// System.Int32 UnityEngine.RectInt::m_Width
	int32_t ___m_Width_2;
	// System.Int32 UnityEngine.RectInt::m_Height
	int32_t ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(RectInt_tE7B8105A280C1AC73A4157ED41F9B86C9BD91E49, ___m_XMin_0)); }
	inline int32_t get_m_XMin_0() const { return ___m_XMin_0; }
	inline int32_t* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(int32_t value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(RectInt_tE7B8105A280C1AC73A4157ED41F9B86C9BD91E49, ___m_YMin_1)); }
	inline int32_t get_m_YMin_1() const { return ___m_YMin_1; }
	inline int32_t* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(int32_t value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(RectInt_tE7B8105A280C1AC73A4157ED41F9B86C9BD91E49, ___m_Width_2)); }
	inline int32_t get_m_Width_2() const { return ___m_Width_2; }
	inline int32_t* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(int32_t value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(RectInt_tE7B8105A280C1AC73A4157ED41F9B86C9BD91E49, ___m_Height_3)); }
	inline int32_t get_m_Height_3() const { return ___m_Height_3; }
	inline int32_t* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(int32_t value)
	{
		___m_Height_3 = value;
	}
};


// UnityEngine.Vector2Int
struct  Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 
{
public:
	// System.Int32 UnityEngine.Vector2Int::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.Vector2Int::m_Y
	int32_t ___m_Y_1;

public:
	inline static int32_t get_offset_of_m_X_0() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9, ___m_X_0)); }
	inline int32_t get_m_X_0() const { return ___m_X_0; }
	inline int32_t* get_address_of_m_X_0() { return &___m_X_0; }
	inline void set_m_X_0(int32_t value)
	{
		___m_X_0 = value;
	}

	inline static int32_t get_offset_of_m_Y_1() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9, ___m_Y_1)); }
	inline int32_t get_m_Y_1() const { return ___m_Y_1; }
	inline int32_t* get_address_of_m_Y_1() { return &___m_Y_1; }
	inline void set_m_Y_1(int32_t value)
	{
		___m_Y_1 = value;
	}
};

struct Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields
{
public:
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Zero
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___s_Zero_2;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_One
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___s_One_3;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Up
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___s_Up_4;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Down
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___s_Down_5;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Left
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___s_Left_6;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Right
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___s_Right_7;

public:
	inline static int32_t get_offset_of_s_Zero_2() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields, ___s_Zero_2)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_s_Zero_2() const { return ___s_Zero_2; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_s_Zero_2() { return &___s_Zero_2; }
	inline void set_s_Zero_2(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___s_Zero_2 = value;
	}

	inline static int32_t get_offset_of_s_One_3() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields, ___s_One_3)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_s_One_3() const { return ___s_One_3; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_s_One_3() { return &___s_One_3; }
	inline void set_s_One_3(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___s_One_3 = value;
	}

	inline static int32_t get_offset_of_s_Up_4() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields, ___s_Up_4)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_s_Up_4() const { return ___s_Up_4; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_s_Up_4() { return &___s_Up_4; }
	inline void set_s_Up_4(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___s_Up_4 = value;
	}

	inline static int32_t get_offset_of_s_Down_5() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields, ___s_Down_5)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_s_Down_5() const { return ___s_Down_5; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_s_Down_5() { return &___s_Down_5; }
	inline void set_s_Down_5(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___s_Down_5 = value;
	}

	inline static int32_t get_offset_of_s_Left_6() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields, ___s_Left_6)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_s_Left_6() const { return ___s_Left_6; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_s_Left_6() { return &___s_Left_6; }
	inline void set_s_Left_6(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___s_Left_6 = value;
	}

	inline static int32_t get_offset_of_s_Right_7() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields, ___s_Right_7)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_s_Right_7() const { return ___s_Right_7; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_s_Right_7() { return &___s_Right_7; }
	inline void set_s_Right_7(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___s_Right_7 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.XR.ARCore.ArCameraConfigFilter
struct  ArCameraConfigFilter_t221B7F0FF74822E577F6C1577DCD66F0F1123F18 
{
public:
	// System.IntPtr UnityEngine.XR.ARCore.ArCameraConfigFilter::m_Self
	intptr_t ___m_Self_0;

public:
	inline static int32_t get_offset_of_m_Self_0() { return static_cast<int32_t>(offsetof(ArCameraConfigFilter_t221B7F0FF74822E577F6C1577DCD66F0F1123F18, ___m_Self_0)); }
	inline intptr_t get_m_Self_0() const { return ___m_Self_0; }
	inline intptr_t* get_address_of_m_Self_0() { return &___m_Self_0; }
	inline void set_m_Self_0(intptr_t value)
	{
		___m_Self_0 = value;
	}
};


// UnityEngine.XR.ARCore.ArConfig
struct  ArConfig_t2E38AE690FF4FF95EE8FDF4E03AD0C640484B40E 
{
public:
	// System.IntPtr UnityEngine.XR.ARCore.ArConfig::m_Self
	intptr_t ___m_Self_0;

public:
	inline static int32_t get_offset_of_m_Self_0() { return static_cast<int32_t>(offsetof(ArConfig_t2E38AE690FF4FF95EE8FDF4E03AD0C640484B40E, ___m_Self_0)); }
	inline intptr_t get_m_Self_0() const { return ___m_Self_0; }
	inline intptr_t* get_address_of_m_Self_0() { return &___m_Self_0; }
	inline void set_m_Self_0(intptr_t value)
	{
		___m_Self_0 = value;
	}
};


// UnityEngine.XR.ARCore.ArSession
struct  ArSession_tC5D0D839684E5D0A103AC9B421B51BE4E661668E 
{
public:
	// System.IntPtr UnityEngine.XR.ARCore.ArSession::m_Self
	intptr_t ___m_Self_0;

public:
	inline static int32_t get_offset_of_m_Self_0() { return static_cast<int32_t>(offsetof(ArSession_tC5D0D839684E5D0A103AC9B421B51BE4E661668E, ___m_Self_0)); }
	inline intptr_t get_m_Self_0() const { return ___m_Self_0; }
	inline intptr_t* get_address_of_m_Self_0() { return &___m_Self_0; }
	inline void set_m_Self_0(intptr_t value)
	{
		___m_Self_0 = value;
	}
};


// easyar.CalibrationDownloadStatus
struct  CalibrationDownloadStatus_t0A1F62F0BC32F95FC341DEEA0F4B88744D66D6F7 
{
public:
	// System.Int32 easyar.CalibrationDownloadStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CalibrationDownloadStatus_t0A1F62F0BC32F95FC341DEEA0F4B88744D66D6F7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.CameraState
struct  CameraState_tAB341C916D6547B1F4A3B49ECAA0D946CCD93EDF 
{
public:
	// System.Int32 easyar.CameraState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraState_tAB341C916D6547B1F4A3B49ECAA0D946CCD93EDF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.LogLevel
struct  LogLevel_t49A681A37470FE2977163BB445D5D7DB3D2A73DC 
{
public:
	// System.Int32 easyar.LogLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogLevel_t49A681A37470FE2977163BB445D5D7DB3D2A73DC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.PermissionStatus
struct  PermissionStatus_tD5B038F7D373F41ECDBD1FB8C1525E264DCC8FED 
{
public:
	// System.Int32 easyar.PermissionStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PermissionStatus_tD5B038F7D373F41ECDBD1FB8C1525E264DCC8FED, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.RecordStatus
struct  RecordStatus_tC330D61721F3937EDEDF1A9DAA3DB2C81C9999A9 
{
public:
	// System.Int32 easyar.RecordStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RecordStatus_tC330D61721F3937EDEDF1A9DAA3DB2C81C9999A9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TextureFormat
struct  TextureFormat_tBED5388A0445FE978F97B41D247275B036407932 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_tBED5388A0445FE978F97B41D247275B036407932, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.VideoStatus
struct  VideoStatus_t1C5597E81378DA8FBEF085ACA14639ABC5D07955 
{
public:
	// System.Int32 easyar.VideoStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoStatus_t1C5597E81378DA8FBEF085ACA14639ABC5D07955, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.Detail/OptionalOfString
struct  OptionalOfString_t39EBFD29D5959A3DBCFB2C6018AA22DABBD63360 
{
public:
	// System.Byte easyar.Detail/OptionalOfString::has_value_
	uint8_t ___has_value__0;
	// System.IntPtr easyar.Detail/OptionalOfString::value
	intptr_t ___value_1;

public:
	inline static int32_t get_offset_of_has_value__0() { return static_cast<int32_t>(offsetof(OptionalOfString_t39EBFD29D5959A3DBCFB2C6018AA22DABBD63360, ___has_value__0)); }
	inline uint8_t get_has_value__0() const { return ___has_value__0; }
	inline uint8_t* get_address_of_has_value__0() { return &___has_value__0; }
	inline void set_has_value__0(uint8_t value)
	{
		___has_value__0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(OptionalOfString_t39EBFD29D5959A3DBCFB2C6018AA22DABBD63360, ___value_1)); }
	inline intptr_t get_value_1() const { return ___value_1; }
	inline intptr_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(intptr_t value)
	{
		___value_1 = value;
	}
};


// UnityEngine.XR.ARSubsystems.XRCpuImage/AsyncConversionStatus
struct  AsyncConversionStatus_t94171EDB7E6E25979DFCEF01F7B6EA6B8A5DAD42 
{
public:
	// System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage/AsyncConversionStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AsyncConversionStatus_t94171EDB7E6E25979DFCEF01F7B6EA6B8A5DAD42, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARSubsystems.XRCpuImage/Transformation
struct  Transformation_t5812B66180F359977F76AB67CC9E923CF0B55938 
{
public:
	// System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage/Transformation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transformation_t5812B66180F359977F76AB67CC9E923CF0B55938, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARCore.ARCoreSessionSubsystem/NativeApi/ArAvailability
struct  ArAvailability_tE538B006129766E37E893353B71F627B8040A880 
{
public:
	// System.Int32 UnityEngine.XR.ARCore.ARCoreSessionSubsystem/NativeApi/ArAvailability::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ArAvailability_tE538B006129766E37E893353B71F627B8040A880, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARCore.ARCoreSessionSubsystem/NativeApi/ArPrestoApkInstallStatus
struct  ArPrestoApkInstallStatus_t51D537681702853840D138A766B1D6BA7674062C 
{
public:
	// System.Int32 UnityEngine.XR.ARCore.ARCoreSessionSubsystem/NativeApi/ArPrestoApkInstallStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ArPrestoApkInstallStatus_t51D537681702853840D138A766B1D6BA7674062C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARSubsystems.XRCpuImage/ConversionParams
struct  ConversionParams_t3DDB9752BA823641A302D0783C14048D9B09B74A 
{
public:
	// UnityEngine.RectInt UnityEngine.XR.ARSubsystems.XRCpuImage/ConversionParams::m_InputRect
	RectInt_tE7B8105A280C1AC73A4157ED41F9B86C9BD91E49  ___m_InputRect_0;
	// UnityEngine.Vector2Int UnityEngine.XR.ARSubsystems.XRCpuImage/ConversionParams::m_OutputDimensions
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___m_OutputDimensions_1;
	// UnityEngine.TextureFormat UnityEngine.XR.ARSubsystems.XRCpuImage/ConversionParams::m_Format
	int32_t ___m_Format_2;
	// UnityEngine.XR.ARSubsystems.XRCpuImage/Transformation UnityEngine.XR.ARSubsystems.XRCpuImage/ConversionParams::m_Transformation
	int32_t ___m_Transformation_3;

public:
	inline static int32_t get_offset_of_m_InputRect_0() { return static_cast<int32_t>(offsetof(ConversionParams_t3DDB9752BA823641A302D0783C14048D9B09B74A, ___m_InputRect_0)); }
	inline RectInt_tE7B8105A280C1AC73A4157ED41F9B86C9BD91E49  get_m_InputRect_0() const { return ___m_InputRect_0; }
	inline RectInt_tE7B8105A280C1AC73A4157ED41F9B86C9BD91E49 * get_address_of_m_InputRect_0() { return &___m_InputRect_0; }
	inline void set_m_InputRect_0(RectInt_tE7B8105A280C1AC73A4157ED41F9B86C9BD91E49  value)
	{
		___m_InputRect_0 = value;
	}

	inline static int32_t get_offset_of_m_OutputDimensions_1() { return static_cast<int32_t>(offsetof(ConversionParams_t3DDB9752BA823641A302D0783C14048D9B09B74A, ___m_OutputDimensions_1)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_m_OutputDimensions_1() const { return ___m_OutputDimensions_1; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_m_OutputDimensions_1() { return &___m_OutputDimensions_1; }
	inline void set_m_OutputDimensions_1(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___m_OutputDimensions_1 = value;
	}

	inline static int32_t get_offset_of_m_Format_2() { return static_cast<int32_t>(offsetof(ConversionParams_t3DDB9752BA823641A302D0783C14048D9B09B74A, ___m_Format_2)); }
	inline int32_t get_m_Format_2() const { return ___m_Format_2; }
	inline int32_t* get_address_of_m_Format_2() { return &___m_Format_2; }
	inline void set_m_Format_2(int32_t value)
	{
		___m_Format_2 = value;
	}

	inline static int32_t get_offset_of_m_Transformation_3() { return static_cast<int32_t>(offsetof(ConversionParams_t3DDB9752BA823641A302D0783C14048D9B09B74A, ___m_Transformation_3)); }
	inline int32_t get_m_Transformation_3() const { return ___m_Transformation_3; }
	inline int32_t* get_address_of_m_Transformation_3() { return &___m_Transformation_3; }
	inline void set_m_Transformation_3(int32_t value)
	{
		___m_Transformation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif

extern "C" void DEFAULT_CALL ReversePInvokeWrapper_ARCameraBackground_BeforeBackgroundRenderHandler_m1A225B853F7A95788FB799D22868A946C99C2975(int32_t ___eventId0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfOutputFrameFromListOfOutputFrame_destroy_m3213152A87BBAB55CB406BB9D08FB45301A33151(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfOutputFrameFromListOfOutputFrame_func_m728DEA484FEF9A32AF0EA5C985684C3A923B6E93(intptr_t ___state0, intptr_t ___arg01, intptr_t* ___Return2, intptr_t* ___exception3);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromBoolAndStringAndString_destroy_mA6C753EE55047102FBA63F1ABC45CE9D2B516010(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromBoolAndStringAndString_func_m32AF4156D52F9B1AEA2A81D03E596F56609DF977(intptr_t ___state0, int8_t ___arg01, intptr_t ___arg12, intptr_t ___arg23, intptr_t* ___exception4);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromBoolAndString_destroy_mDC79451BFE0F528760772F0A015FBC035E6E023E(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromBoolAndString_func_m4786C9B24241EBD28AAF555E9CFCB97344EFB8CB(intptr_t ___state0, int8_t ___arg01, intptr_t ___arg12, intptr_t* ___exception3);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromBool_destroy_m3D313F4C383413AAD2FCB52140036EA67F40B985(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromBool_func_m18B5057D02371A6A12534E44D51E79565365602A(intptr_t ___state0, int8_t ___arg01, intptr_t* ___exception2);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_destroy_m44D24357597F4A3861061A606C66CAC57CBDE0D8(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_func_m47D3C6B1C3FA746A6D370FBC7B1D517706FB3EFE(intptr_t ___state0, int32_t ___arg01, OptionalOfString_t39EBFD29D5959A3DBCFB2C6018AA22DABBD63360  ___arg12, intptr_t* ___exception3);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromCameraState_destroy_mFF1D0D74D6DDC00A03B9F68ABD23B93BBCB212B2(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromCameraState_func_mC7E9F14253350E76D81DD852C31479AE6FA97541(intptr_t ___state0, int32_t ___arg01, intptr_t* ___exception2);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromCloudRecognizationResult_destroy_mBF60D4AFDA72F76BFBE41B77BBAACB66D22FB251(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromCloudRecognizationResult_func_m631246C2873CA0786043DC8226B7A1F69BAF973D(intptr_t ___state0, intptr_t ___arg01, intptr_t* ___exception2);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromFeedbackFrame_destroy_mE0489B5AF6DB4645A89B1AA2C5E728AA2085DA06(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromFeedbackFrame_func_mF3A0BCA98C72331241FAC3A693309EBC3570DAD7(intptr_t ___state0, intptr_t ___arg01, intptr_t* ___exception2);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromInputFrame_destroy_mBC40B788EC5FA01C081A8B8F29DD83BD7C04BBE0(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromInputFrame_func_mA15F0CAA1EFCDBBE96BA6D3A17F1AE49FFB6443F(intptr_t ___state0, intptr_t ___arg01, intptr_t* ___exception2);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromLogLevelAndString_destroy_mF22BD5934E396FB1042F8B0E27B54DBE318FCBFE(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromLogLevelAndString_func_m9A78157C61E95CA37E54667EDB501049BE406D6A(intptr_t ___state0, int32_t ___arg01, intptr_t ___arg12, intptr_t* ___exception3);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromOutputFrame_destroy_m39E6790C6700E68EEB6EF662B32E84E8150EE773(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromOutputFrame_func_m142DA320BDD782F0665F95929A1FAF518AAB0B80(intptr_t ___state0, intptr_t ___arg01, intptr_t* ___exception2);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromPermissionStatusAndString_destroy_m781B495D25D988C89184294BD62865D9E801969D(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromPermissionStatusAndString_func_m8D9D32B6E616DCFD443DE289BEE2A1A9E10D63B3(intptr_t ___state0, int32_t ___arg01, intptr_t ___arg12, intptr_t* ___exception3);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromRecordStatusAndString_destroy_mA47A5359B41B4FD8321FA5CC075E76AD7ABBBCD1(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromRecordStatusAndString_func_mA45DA83CDE8ACEB8F46865D1B86A518290177EBA(intptr_t ___state0, int32_t ___arg01, intptr_t ___arg12, intptr_t* ___exception3);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromTargetAndBool_destroy_m407203B6D0165B9CBB93CDC394623D25D1394C3E(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromTargetAndBool_func_m4F4E191A4A3E8C8B1C93A9A83F773843E947D3E0(intptr_t ___state0, intptr_t ___arg01, int8_t ___arg12, intptr_t* ___exception3);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromVideoStatus_destroy_mC318C3913E76C1CF3DC21561E52A312033C580E4(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromVideoStatus_func_mB7B7FD5B5858F436F3466A1919AF72453E7BE34F(intptr_t ___state0, int32_t ___arg01, intptr_t* ___exception2);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoid_destroy_m9A583576B55465A17C01F9B74E416B531702AB80(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoid_func_m8233FE906FAE6473609404B722CF66412E5EFB48(intptr_t ___state0, intptr_t* ___exception1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_OSSpecificSynchronizationContext_InvocationEntry_m0045E44F7E960D6B4A864D5206D4116249C09BB0(intptr_t ___arg0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_XRCpuImage_OnAsyncConversionComplete_m60DC813F1A2B8A59F993D245683847D933D292A9(int32_t ___status0, ConversionParams_t3DDB9752BA823641A302D0783C14048D9B09B74A  ___conversionParams1, intptr_t ___dataPtr2, int32_t ___dataLength3, intptr_t ___context4);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_ARCoreProvider_OnBeforeGetCameraConfiguration_m38BB627D1EB76A533FC804BB76815EFF9F8F52A4(intptr_t ___providerHandle0, ArSession_tC5D0D839684E5D0A103AC9B421B51BE4E661668E  ___session1, ArCameraConfigFilter_t221B7F0FF74822E577F6C1577DCD66F0F1123F18  ___filter2);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_ARCoreProvider_CameraPermissionRequestProvider_m0E9468644682F3042A5368601E230B46C7315C59(Il2CppMethodPointer ___callback0, intptr_t ___context1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_ARCoreProvider_OnApkInstallation_mB4577B455FE8DAC994BD4BCC3ADF2C239C9A0499(int32_t ___status0, intptr_t ___context1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_ARCoreProvider_OnCheckApkAvailability_mF296B6B889C9E96E19074265AEBABC32A9056751(int32_t ___availability0, intptr_t ___context1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_ARCoreProvider_SetConfigurationCallback_mC1501045CC4A96222724006C1000E810B0783443(ArSession_tC5D0D839684E5D0A103AC9B421B51BE4E661668E  ___session0, ArConfig_t2E38AE690FF4FF95EE8FDF4E03AD0C640484B40E  ___config1, intptr_t ___context2);
extern "C" Guid_t  DEFAULT_CALL ReversePInvokeWrapper_ARCoreProvider_GenerateGuid_mA52E9502CED2C91DD0681FFB36AA2C9AC876DD0B();


IL2CPP_EXTERN_C const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[];
const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[41] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_ARCameraBackground_BeforeBackgroundRenderHandler_m1A225B853F7A95788FB799D22868A946C99C2975),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfOutputFrameFromListOfOutputFrame_destroy_m3213152A87BBAB55CB406BB9D08FB45301A33151),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfOutputFrameFromListOfOutputFrame_func_m728DEA484FEF9A32AF0EA5C985684C3A923B6E93),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromBoolAndStringAndString_destroy_mA6C753EE55047102FBA63F1ABC45CE9D2B516010),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromBoolAndStringAndString_func_m32AF4156D52F9B1AEA2A81D03E596F56609DF977),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromBoolAndString_destroy_mDC79451BFE0F528760772F0A015FBC035E6E023E),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromBoolAndString_func_m4786C9B24241EBD28AAF555E9CFCB97344EFB8CB),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromBool_destroy_m3D313F4C383413AAD2FCB52140036EA67F40B985),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromBool_func_m18B5057D02371A6A12534E44D51E79565365602A),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_destroy_m44D24357597F4A3861061A606C66CAC57CBDE0D8),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_func_m47D3C6B1C3FA746A6D370FBC7B1D517706FB3EFE),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromCameraState_destroy_mFF1D0D74D6DDC00A03B9F68ABD23B93BBCB212B2),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromCameraState_func_mC7E9F14253350E76D81DD852C31479AE6FA97541),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromCloudRecognizationResult_destroy_mBF60D4AFDA72F76BFBE41B77BBAACB66D22FB251),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromCloudRecognizationResult_func_m631246C2873CA0786043DC8226B7A1F69BAF973D),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromFeedbackFrame_destroy_mE0489B5AF6DB4645A89B1AA2C5E728AA2085DA06),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromFeedbackFrame_func_mF3A0BCA98C72331241FAC3A693309EBC3570DAD7),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromInputFrame_destroy_mBC40B788EC5FA01C081A8B8F29DD83BD7C04BBE0),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromInputFrame_func_mA15F0CAA1EFCDBBE96BA6D3A17F1AE49FFB6443F),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromLogLevelAndString_destroy_mF22BD5934E396FB1042F8B0E27B54DBE318FCBFE),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromLogLevelAndString_func_m9A78157C61E95CA37E54667EDB501049BE406D6A),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromOutputFrame_destroy_m39E6790C6700E68EEB6EF662B32E84E8150EE773),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromOutputFrame_func_m142DA320BDD782F0665F95929A1FAF518AAB0B80),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromPermissionStatusAndString_destroy_m781B495D25D988C89184294BD62865D9E801969D),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromPermissionStatusAndString_func_m8D9D32B6E616DCFD443DE289BEE2A1A9E10D63B3),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromRecordStatusAndString_destroy_mA47A5359B41B4FD8321FA5CC075E76AD7ABBBCD1),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromRecordStatusAndString_func_mA45DA83CDE8ACEB8F46865D1B86A518290177EBA),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromTargetAndBool_destroy_m407203B6D0165B9CBB93CDC394623D25D1394C3E),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromTargetAndBool_func_m4F4E191A4A3E8C8B1C93A9A83F773843E947D3E0),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromVideoStatus_destroy_mC318C3913E76C1CF3DC21561E52A312033C580E4),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromVideoStatus_func_mB7B7FD5B5858F436F3466A1919AF72453E7BE34F),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoid_destroy_m9A583576B55465A17C01F9B74E416B531702AB80),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoid_func_m8233FE906FAE6473609404B722CF66412E5EFB48),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_OSSpecificSynchronizationContext_InvocationEntry_m0045E44F7E960D6B4A864D5206D4116249C09BB0),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_XRCpuImage_OnAsyncConversionComplete_m60DC813F1A2B8A59F993D245683847D933D292A9),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_ARCoreProvider_OnBeforeGetCameraConfiguration_m38BB627D1EB76A533FC804BB76815EFF9F8F52A4),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_ARCoreProvider_CameraPermissionRequestProvider_m0E9468644682F3042A5368601E230B46C7315C59),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_ARCoreProvider_OnApkInstallation_mB4577B455FE8DAC994BD4BCC3ADF2C239C9A0499),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_ARCoreProvider_OnCheckApkAvailability_mF296B6B889C9E96E19074265AEBABC32A9056751),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_ARCoreProvider_SetConfigurationCallback_mC1501045CC4A96222724006C1000E810B0783443),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_ARCoreProvider_GenerateGuid_mA52E9502CED2C91DD0681FFB36AA2C9AC876DD0B),
};
