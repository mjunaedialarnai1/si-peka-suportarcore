﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>



// System.Action`1<easyar.InputFrameSink>
struct Action_1_t2D3D45E97EC048B5CBBB0C12D657C57D2546998E;
// System.Action`1<System.Int32>
struct Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B;
// System.Action`3<easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String>
struct Action_3_t19842C82DF16F364B16C28046B2B24E1C96EC218;
// System.Action`4<easyar.SparseSpatialMapController,easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String>
struct Action_4_t0575E8501FB3174D965BAAA6F41155442409F864;
// System.Collections.Generic.Dictionary`2<System.String,easyar.SparseSpatialMapController>
struct Dictionary_2_t6308C9F5D80DC7F5363BEB6AB25A46187D6CDDB7;
// System.Func`1<System.Int32>
struct Func_1_tCB4CC73D86ED9FF6219A185C0C591F956E5DD1BA;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// easyar.ARCoreCameraDevice
struct ARCoreCameraDevice_t06EFABA3C9845DB59BAC4190900CC40C2FDA2F09;
// easyar.ARKitCameraDevice
struct ARKitCameraDevice_t22455ED863FCA429EFD434AC922A824BE773E0C9;
// easyar.ARSession
struct ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0;
// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6;
// easyar.CalibrationDownloader
struct CalibrationDownloader_tA658B7F2F0ADEA39D63B542AE2CBB3D46489CD97;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// easyar.InputFrameSink
struct InputFrameSink_tA1C8E0C1E35C46892BD6001FB52448E371CF094A;
// easyar.MotionTrackerCameraDevice
struct MotionTrackerCameraDevice_t14DA140FBE6AF7DB67C2B4B48ACABFCF4A6C9D89;
// easyar.SparseSpatialMap
struct SparseSpatialMap_t26B9E1A5A6835F3F39613A1EE395EEEEF9329315;
// easyar.SparseSpatialMapController
struct SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758;
// easyar.SparseSpatialMapManager
struct SparseSpatialMapManager_tB7AA7A4A3CDEA90D406562E6DAD84D34F683A6D1;
// easyar.SparseSpatialMapWorkerFrameFilter
struct SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2;
// System.String
struct String_t;
// easyar.SurfaceTracker
struct SurfaceTracker_t51B55B8398B2F4537CA25FB289A36F39B8B6CC79;
// easyar.VIOCameraDeviceUnion
struct VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// easyar.SparseSpatialMapWorkerFrameFilter/MapLocalizerConfig
struct MapLocalizerConfig_t72428BF25E41CECF8892A78B4A55A6A82A6C130D;
// easyar.SparseSpatialMapWorkerFrameFilter/SpatialMapServiceConfig
struct SpatialMapServiceConfig_t086F77E62E9A3A9F71A3170AC5E3D6B75E92BA6C;
// easyar.VIOCameraDeviceUnion/DeviceUnion
struct DeviceUnion_t28AB5438E81AA07F093A6D1A114616FDCB540FC9;
// easyar.VIOCameraDeviceUnion/MotionTrackerCameraDeviceParameters
struct MotionTrackerCameraDeviceParameters_tFCE91F25E86732B2628FCB523A65253DF8930931;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// easyar.SparseSpatialMapWorkerFrameFilter/<>c__DisplayClass65_0
struct  U3CU3Ec__DisplayClass65_0_tECCE9C536DE9B2EEE07CEB5F5C3E04A2812AA948  : public RuntimeObject
{
public:
	// easyar.SparseSpatialMapWorkerFrameFilter easyar.SparseSpatialMapWorkerFrameFilter/<>c__DisplayClass65_0::<>4__this
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2 * ___U3CU3E4__this_0;
	// easyar.SparseSpatialMapController easyar.SparseSpatialMapWorkerFrameFilter/<>c__DisplayClass65_0::controller
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758 * ___controller_1;
	// System.Action`3<easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String> easyar.SparseSpatialMapWorkerFrameFilter/<>c__DisplayClass65_0::callback
	Action_3_t19842C82DF16F364B16C28046B2B24E1C96EC218 * ___callback_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass65_0_tECCE9C536DE9B2EEE07CEB5F5C3E04A2812AA948, ___U3CU3E4__this_0)); }
	inline SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_controller_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass65_0_tECCE9C536DE9B2EEE07CEB5F5C3E04A2812AA948, ___controller_1)); }
	inline SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758 * get_controller_1() const { return ___controller_1; }
	inline SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758 ** get_address_of_controller_1() { return &___controller_1; }
	inline void set_controller_1(SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758 * value)
	{
		___controller_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___controller_1), (void*)value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass65_0_tECCE9C536DE9B2EEE07CEB5F5C3E04A2812AA948, ___callback_2)); }
	inline Action_3_t19842C82DF16F364B16C28046B2B24E1C96EC218 * get_callback_2() const { return ___callback_2; }
	inline Action_3_t19842C82DF16F364B16C28046B2B24E1C96EC218 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_3_t19842C82DF16F364B16C28046B2B24E1C96EC218 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callback_2), (void*)value);
	}
};


// easyar.SparseSpatialMapWorkerFrameFilter/<>c__DisplayClass66_0
struct  U3CU3Ec__DisplayClass66_0_t9C21DCE46449BD59D109191BDBB6045D5FB9A1D3  : public RuntimeObject
{
public:
	// System.String easyar.SparseSpatialMapWorkerFrameFilter/<>c__DisplayClass66_0::name
	String_t* ___name_0;
	// easyar.SparseSpatialMapWorkerFrameFilter easyar.SparseSpatialMapWorkerFrameFilter/<>c__DisplayClass66_0::<>4__this
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2 * ___U3CU3E4__this_1;
	// easyar.SparseSpatialMapController easyar.SparseSpatialMapWorkerFrameFilter/<>c__DisplayClass66_0::controller
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758 * ___controller_2;
	// System.Action`3<easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String> easyar.SparseSpatialMapWorkerFrameFilter/<>c__DisplayClass66_0::callback
	Action_3_t19842C82DF16F364B16C28046B2B24E1C96EC218 * ___callback_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass66_0_t9C21DCE46449BD59D109191BDBB6045D5FB9A1D3, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass66_0_t9C21DCE46449BD59D109191BDBB6045D5FB9A1D3, ___U3CU3E4__this_1)); }
	inline SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}

	inline static int32_t get_offset_of_controller_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass66_0_t9C21DCE46449BD59D109191BDBB6045D5FB9A1D3, ___controller_2)); }
	inline SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758 * get_controller_2() const { return ___controller_2; }
	inline SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758 ** get_address_of_controller_2() { return &___controller_2; }
	inline void set_controller_2(SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758 * value)
	{
		___controller_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___controller_2), (void*)value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass66_0_t9C21DCE46449BD59D109191BDBB6045D5FB9A1D3, ___callback_3)); }
	inline Action_3_t19842C82DF16F364B16C28046B2B24E1C96EC218 * get_callback_3() const { return ___callback_3; }
	inline Action_3_t19842C82DF16F364B16C28046B2B24E1C96EC218 ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(Action_3_t19842C82DF16F364B16C28046B2B24E1C96EC218 * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callback_3), (void*)value);
	}
};


// easyar.VIOCameraDeviceUnion/<>c__DisplayClass31_0
struct  U3CU3Ec__DisplayClass31_0_tFC0DE9EF235A32B0CDD56FBD46A4F96156CB04FE  : public RuntimeObject
{
public:
	// easyar.CalibrationDownloader easyar.VIOCameraDeviceUnion/<>c__DisplayClass31_0::downloader
	CalibrationDownloader_tA658B7F2F0ADEA39D63B542AE2CBB3D46489CD97 * ___downloader_0;

public:
	inline static int32_t get_offset_of_downloader_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_tFC0DE9EF235A32B0CDD56FBD46A4F96156CB04FE, ___downloader_0)); }
	inline CalibrationDownloader_tA658B7F2F0ADEA39D63B542AE2CBB3D46489CD97 * get_downloader_0() const { return ___downloader_0; }
	inline CalibrationDownloader_tA658B7F2F0ADEA39D63B542AE2CBB3D46489CD97 ** get_address_of_downloader_0() { return &___downloader_0; }
	inline void set_downloader_0(CalibrationDownloader_tA658B7F2F0ADEA39D63B542AE2CBB3D46489CD97 * value)
	{
		___downloader_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___downloader_0), (void*)value);
	}
};


// easyar.VIOCameraDeviceUnion/<>c__DisplayClass38_0
struct  U3CU3Ec__DisplayClass38_0_t69E545D6C9ABDAF5197C97948736476FCA9F56EC  : public RuntimeObject
{
public:
	// easyar.VIOCameraDeviceUnion easyar.VIOCameraDeviceUnion/<>c__DisplayClass38_0::<>4__this
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E * ___U3CU3E4__this_0;
	// easyar.MotionTrackerCameraDevice easyar.VIOCameraDeviceUnion/<>c__DisplayClass38_0::device
	MotionTrackerCameraDevice_t14DA140FBE6AF7DB67C2B4B48ACABFCF4A6C9D89 * ___device_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t69E545D6C9ABDAF5197C97948736476FCA9F56EC, ___U3CU3E4__this_0)); }
	inline VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_device_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t69E545D6C9ABDAF5197C97948736476FCA9F56EC, ___device_1)); }
	inline MotionTrackerCameraDevice_t14DA140FBE6AF7DB67C2B4B48ACABFCF4A6C9D89 * get_device_1() const { return ___device_1; }
	inline MotionTrackerCameraDevice_t14DA140FBE6AF7DB67C2B4B48ACABFCF4A6C9D89 ** get_address_of_device_1() { return &___device_1; }
	inline void set_device_1(MotionTrackerCameraDevice_t14DA140FBE6AF7DB67C2B4B48ACABFCF4A6C9D89 * value)
	{
		___device_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___device_1), (void*)value);
	}
};


// easyar.VIOCameraDeviceUnion/<>c__DisplayClass39_0
struct  U3CU3Ec__DisplayClass39_0_t4CD5AD199A2A20ABD7EDA455F921EAAC020E1CEE  : public RuntimeObject
{
public:
	// easyar.ARKitCameraDevice easyar.VIOCameraDeviceUnion/<>c__DisplayClass39_0::device
	ARKitCameraDevice_t22455ED863FCA429EFD434AC922A824BE773E0C9 * ___device_0;

public:
	inline static int32_t get_offset_of_device_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_t4CD5AD199A2A20ABD7EDA455F921EAAC020E1CEE, ___device_0)); }
	inline ARKitCameraDevice_t22455ED863FCA429EFD434AC922A824BE773E0C9 * get_device_0() const { return ___device_0; }
	inline ARKitCameraDevice_t22455ED863FCA429EFD434AC922A824BE773E0C9 ** get_address_of_device_0() { return &___device_0; }
	inline void set_device_0(ARKitCameraDevice_t22455ED863FCA429EFD434AC922A824BE773E0C9 * value)
	{
		___device_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___device_0), (void*)value);
	}
};


// easyar.VIOCameraDeviceUnion/<>c__DisplayClass40_0
struct  U3CU3Ec__DisplayClass40_0_t6189F2CFDF63149C3B6E82D3DAB2286E14D3CE5A  : public RuntimeObject
{
public:
	// easyar.ARCoreCameraDevice easyar.VIOCameraDeviceUnion/<>c__DisplayClass40_0::device
	ARCoreCameraDevice_t06EFABA3C9845DB59BAC4190900CC40C2FDA2F09 * ___device_0;

public:
	inline static int32_t get_offset_of_device_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_t6189F2CFDF63149C3B6E82D3DAB2286E14D3CE5A, ___device_0)); }
	inline ARCoreCameraDevice_t06EFABA3C9845DB59BAC4190900CC40C2FDA2F09 * get_device_0() const { return ___device_0; }
	inline ARCoreCameraDevice_t06EFABA3C9845DB59BAC4190900CC40C2FDA2F09 ** get_address_of_device_0() { return &___device_0; }
	inline void set_device_0(ARCoreCameraDevice_t06EFABA3C9845DB59BAC4190900CC40C2FDA2F09 * value)
	{
		___device_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___device_0), (void*)value);
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24
struct  __StaticArrayInitTypeSizeU3D24_t2F23740D8943FC7C06AD3DD80B71D65744F140F2 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t2F23740D8943FC7C06AD3DD80B71D65744F140F2__padding[24];
	};

public:
};


// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1
	__StaticArrayInitTypeSizeU3D24_t2F23740D8943FC7C06AD3DD80B71D65744F140F2  ___753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1_0;

public:
	inline static int32_t get_offset_of_U3753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_StaticFields, ___753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1_0)); }
	inline __StaticArrayInitTypeSizeU3D24_t2F23740D8943FC7C06AD3DD80B71D65744F140F2  get_U3753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1_0() const { return ___753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1_0; }
	inline __StaticArrayInitTypeSizeU3D24_t2F23740D8943FC7C06AD3DD80B71D65744F140F2 * get_address_of_U3753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1_0() { return &___753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1_0; }
	inline void set_U3753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1_0(__StaticArrayInitTypeSizeU3D24_t2F23740D8943FC7C06AD3DD80B71D65744F140F2  value)
	{
		___753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1_0 = value;
	}
};


// easyar.MotionTrackerCameraDeviceFPS
struct  MotionTrackerCameraDeviceFPS_t483C0CEC16C5C7F2CFF0EF5675B694E1B0EA1C58 
{
public:
	// System.Int32 easyar.MotionTrackerCameraDeviceFPS::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MotionTrackerCameraDeviceFPS_t483C0CEC16C5C7F2CFF0EF5675B694E1B0EA1C58, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.MotionTrackerCameraDeviceFocusMode
struct  MotionTrackerCameraDeviceFocusMode_tF6727042884057BA65BFC256865A8A1406377E53 
{
public:
	// System.Int32 easyar.MotionTrackerCameraDeviceFocusMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MotionTrackerCameraDeviceFocusMode_tF6727042884057BA65BFC256865A8A1406377E53, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.MotionTrackerCameraDeviceQualityLevel
struct  MotionTrackerCameraDeviceQualityLevel_t47967FD0EDD960B87B80705B8673EB831A5C8D30 
{
public:
	// System.Int32 easyar.MotionTrackerCameraDeviceQualityLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MotionTrackerCameraDeviceQualityLevel_t47967FD0EDD960B87B80705B8673EB831A5C8D30, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.MotionTrackerCameraDeviceResolution
struct  MotionTrackerCameraDeviceResolution_tAEC9A75620582FEB8EC1C1B015A5DB259382B45A 
{
public:
	// System.Int32 easyar.MotionTrackerCameraDeviceResolution::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MotionTrackerCameraDeviceResolution_tAEC9A75620582FEB8EC1C1B015A5DB259382B45A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.MotionTrackerCameraDeviceTrackingMode
struct  MotionTrackerCameraDeviceTrackingMode_tE95D9BACD31D69B36B27BB6B56FABEE8C8D7E2F6 
{
public:
	// System.Int32 easyar.MotionTrackerCameraDeviceTrackingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MotionTrackerCameraDeviceTrackingMode_tE95D9BACD31D69B36B27BB6B56FABEE8C8D7E2F6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.MotionTrackingStatus
struct  MotionTrackingStatus_t4796CB2010F06ECF3605228464744EF01E6E9B32 
{
public:
	// System.Int32 easyar.MotionTrackingStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MotionTrackingStatus_t4796CB2010F06ECF3605228464744EF01E6E9B32, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// easyar.SparseSpatialMapWorkerFrameFilter/Mode
struct  Mode_t99C5DCC5283A52EAEB329FD7CBEECC07C60E9BC1 
{
public:
	// System.Int32 easyar.SparseSpatialMapWorkerFrameFilter/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t99C5DCC5283A52EAEB329FD7CBEECC07C60E9BC1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.VIOCameraDeviceUnion/DeviceChooseStrategy
struct  DeviceChooseStrategy_t504208EF88006B06CD99E579F32C4B03506DA79E 
{
public:
	// System.Int32 easyar.VIOCameraDeviceUnion/DeviceChooseStrategy::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DeviceChooseStrategy_t504208EF88006B06CD99E579F32C4B03506DA79E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.VIOCameraDeviceUnion/DeviceUnion/VIODeviceType
struct  VIODeviceType_tD0CB378CF2E6EAED5AA5F70AF25A233652B37AD1 
{
public:
	// System.Int32 easyar.VIOCameraDeviceUnion/DeviceUnion/VIODeviceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VIODeviceType_tD0CB378CF2E6EAED5AA5F70AF25A233652B37AD1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// easyar.VIOCameraDeviceUnion/DeviceUnion
struct  DeviceUnion_t28AB5438E81AA07F093A6D1A114616FDCB540FC9  : public RuntimeObject
{
public:
	// easyar.MotionTrackerCameraDevice easyar.VIOCameraDeviceUnion/DeviceUnion::motionTrackerCameraDevice
	MotionTrackerCameraDevice_t14DA140FBE6AF7DB67C2B4B48ACABFCF4A6C9D89 * ___motionTrackerCameraDevice_0;
	// easyar.ARKitCameraDevice easyar.VIOCameraDeviceUnion/DeviceUnion::arKitCameraDevice
	ARKitCameraDevice_t22455ED863FCA429EFD434AC922A824BE773E0C9 * ___arKitCameraDevice_1;
	// easyar.ARCoreCameraDevice easyar.VIOCameraDeviceUnion/DeviceUnion::arCoreCameraDevice
	ARCoreCameraDevice_t06EFABA3C9845DB59BAC4190900CC40C2FDA2F09 * ___arCoreCameraDevice_2;
	// easyar.VIOCameraDeviceUnion/DeviceUnion/VIODeviceType easyar.VIOCameraDeviceUnion/DeviceUnion::<DeviceType>k__BackingField
	int32_t ___U3CDeviceTypeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_motionTrackerCameraDevice_0() { return static_cast<int32_t>(offsetof(DeviceUnion_t28AB5438E81AA07F093A6D1A114616FDCB540FC9, ___motionTrackerCameraDevice_0)); }
	inline MotionTrackerCameraDevice_t14DA140FBE6AF7DB67C2B4B48ACABFCF4A6C9D89 * get_motionTrackerCameraDevice_0() const { return ___motionTrackerCameraDevice_0; }
	inline MotionTrackerCameraDevice_t14DA140FBE6AF7DB67C2B4B48ACABFCF4A6C9D89 ** get_address_of_motionTrackerCameraDevice_0() { return &___motionTrackerCameraDevice_0; }
	inline void set_motionTrackerCameraDevice_0(MotionTrackerCameraDevice_t14DA140FBE6AF7DB67C2B4B48ACABFCF4A6C9D89 * value)
	{
		___motionTrackerCameraDevice_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___motionTrackerCameraDevice_0), (void*)value);
	}

	inline static int32_t get_offset_of_arKitCameraDevice_1() { return static_cast<int32_t>(offsetof(DeviceUnion_t28AB5438E81AA07F093A6D1A114616FDCB540FC9, ___arKitCameraDevice_1)); }
	inline ARKitCameraDevice_t22455ED863FCA429EFD434AC922A824BE773E0C9 * get_arKitCameraDevice_1() const { return ___arKitCameraDevice_1; }
	inline ARKitCameraDevice_t22455ED863FCA429EFD434AC922A824BE773E0C9 ** get_address_of_arKitCameraDevice_1() { return &___arKitCameraDevice_1; }
	inline void set_arKitCameraDevice_1(ARKitCameraDevice_t22455ED863FCA429EFD434AC922A824BE773E0C9 * value)
	{
		___arKitCameraDevice_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arKitCameraDevice_1), (void*)value);
	}

	inline static int32_t get_offset_of_arCoreCameraDevice_2() { return static_cast<int32_t>(offsetof(DeviceUnion_t28AB5438E81AA07F093A6D1A114616FDCB540FC9, ___arCoreCameraDevice_2)); }
	inline ARCoreCameraDevice_t06EFABA3C9845DB59BAC4190900CC40C2FDA2F09 * get_arCoreCameraDevice_2() const { return ___arCoreCameraDevice_2; }
	inline ARCoreCameraDevice_t06EFABA3C9845DB59BAC4190900CC40C2FDA2F09 ** get_address_of_arCoreCameraDevice_2() { return &___arCoreCameraDevice_2; }
	inline void set_arCoreCameraDevice_2(ARCoreCameraDevice_t06EFABA3C9845DB59BAC4190900CC40C2FDA2F09 * value)
	{
		___arCoreCameraDevice_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arCoreCameraDevice_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CDeviceTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DeviceUnion_t28AB5438E81AA07F093A6D1A114616FDCB540FC9, ___U3CDeviceTypeU3Ek__BackingField_3)); }
	inline int32_t get_U3CDeviceTypeU3Ek__BackingField_3() const { return ___U3CDeviceTypeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CDeviceTypeU3Ek__BackingField_3() { return &___U3CDeviceTypeU3Ek__BackingField_3; }
	inline void set_U3CDeviceTypeU3Ek__BackingField_3(int32_t value)
	{
		___U3CDeviceTypeU3Ek__BackingField_3 = value;
	}
};


// easyar.VIOCameraDeviceUnion/MotionTrackerCameraDeviceParameters
struct  MotionTrackerCameraDeviceParameters_tFCE91F25E86732B2628FCB523A65253DF8930931  : public RuntimeObject
{
public:
	// easyar.MotionTrackerCameraDeviceFPS easyar.VIOCameraDeviceUnion/MotionTrackerCameraDeviceParameters::FPS
	int32_t ___FPS_0;
	// easyar.MotionTrackerCameraDeviceFocusMode easyar.VIOCameraDeviceUnion/MotionTrackerCameraDeviceParameters::FocusMode
	int32_t ___FocusMode_1;
	// easyar.MotionTrackerCameraDeviceResolution easyar.VIOCameraDeviceUnion/MotionTrackerCameraDeviceParameters::Resolution
	int32_t ___Resolution_2;
	// easyar.MotionTrackerCameraDeviceTrackingMode easyar.VIOCameraDeviceUnion/MotionTrackerCameraDeviceParameters::TrackingMode
	int32_t ___TrackingMode_3;
	// easyar.MotionTrackerCameraDeviceQualityLevel easyar.VIOCameraDeviceUnion/MotionTrackerCameraDeviceParameters::MinQualityLevel
	int32_t ___MinQualityLevel_4;

public:
	inline static int32_t get_offset_of_FPS_0() { return static_cast<int32_t>(offsetof(MotionTrackerCameraDeviceParameters_tFCE91F25E86732B2628FCB523A65253DF8930931, ___FPS_0)); }
	inline int32_t get_FPS_0() const { return ___FPS_0; }
	inline int32_t* get_address_of_FPS_0() { return &___FPS_0; }
	inline void set_FPS_0(int32_t value)
	{
		___FPS_0 = value;
	}

	inline static int32_t get_offset_of_FocusMode_1() { return static_cast<int32_t>(offsetof(MotionTrackerCameraDeviceParameters_tFCE91F25E86732B2628FCB523A65253DF8930931, ___FocusMode_1)); }
	inline int32_t get_FocusMode_1() const { return ___FocusMode_1; }
	inline int32_t* get_address_of_FocusMode_1() { return &___FocusMode_1; }
	inline void set_FocusMode_1(int32_t value)
	{
		___FocusMode_1 = value;
	}

	inline static int32_t get_offset_of_Resolution_2() { return static_cast<int32_t>(offsetof(MotionTrackerCameraDeviceParameters_tFCE91F25E86732B2628FCB523A65253DF8930931, ___Resolution_2)); }
	inline int32_t get_Resolution_2() const { return ___Resolution_2; }
	inline int32_t* get_address_of_Resolution_2() { return &___Resolution_2; }
	inline void set_Resolution_2(int32_t value)
	{
		___Resolution_2 = value;
	}

	inline static int32_t get_offset_of_TrackingMode_3() { return static_cast<int32_t>(offsetof(MotionTrackerCameraDeviceParameters_tFCE91F25E86732B2628FCB523A65253DF8930931, ___TrackingMode_3)); }
	inline int32_t get_TrackingMode_3() const { return ___TrackingMode_3; }
	inline int32_t* get_address_of_TrackingMode_3() { return &___TrackingMode_3; }
	inline void set_TrackingMode_3(int32_t value)
	{
		___TrackingMode_3 = value;
	}

	inline static int32_t get_offset_of_MinQualityLevel_4() { return static_cast<int32_t>(offsetof(MotionTrackerCameraDeviceParameters_tFCE91F25E86732B2628FCB523A65253DF8930931, ___MinQualityLevel_4)); }
	inline int32_t get_MinQualityLevel_4() const { return ___MinQualityLevel_4; }
	inline int32_t* get_address_of_MinQualityLevel_4() { return &___MinQualityLevel_4; }
	inline void set_MinQualityLevel_4(int32_t value)
	{
		___MinQualityLevel_4 = value;
	}
};


// UnityEngine.Behaviour
struct  Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// easyar.FrameFilter
struct  FrameFilter_tBF572586253A2067B971C65739AA82AB121662D9  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean easyar.FrameFilter::horizontalFlip
	bool ___horizontalFlip_4;

public:
	inline static int32_t get_offset_of_horizontalFlip_4() { return static_cast<int32_t>(offsetof(FrameFilter_tBF572586253A2067B971C65739AA82AB121662D9, ___horizontalFlip_4)); }
	inline bool get_horizontalFlip_4() const { return ___horizontalFlip_4; }
	inline bool* get_address_of_horizontalFlip_4() { return &___horizontalFlip_4; }
	inline void set_horizontalFlip_4(bool value)
	{
		___horizontalFlip_4 = value;
	}
};


// easyar.FrameSource
struct  FrameSource_t02E6ED15D84ACA37F80D358EC31B06A21E07632C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// easyar.InputFrameSink easyar.FrameSource::sink
	InputFrameSink_tA1C8E0C1E35C46892BD6001FB52448E371CF094A * ___sink_4;
	// easyar.ARSession easyar.FrameSource::arSession
	ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0 * ___arSession_5;

public:
	inline static int32_t get_offset_of_sink_4() { return static_cast<int32_t>(offsetof(FrameSource_t02E6ED15D84ACA37F80D358EC31B06A21E07632C, ___sink_4)); }
	inline InputFrameSink_tA1C8E0C1E35C46892BD6001FB52448E371CF094A * get_sink_4() const { return ___sink_4; }
	inline InputFrameSink_tA1C8E0C1E35C46892BD6001FB52448E371CF094A ** get_address_of_sink_4() { return &___sink_4; }
	inline void set_sink_4(InputFrameSink_tA1C8E0C1E35C46892BD6001FB52448E371CF094A * value)
	{
		___sink_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sink_4), (void*)value);
	}

	inline static int32_t get_offset_of_arSession_5() { return static_cast<int32_t>(offsetof(FrameSource_t02E6ED15D84ACA37F80D358EC31B06A21E07632C, ___arSession_5)); }
	inline ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0 * get_arSession_5() const { return ___arSession_5; }
	inline ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0 ** get_address_of_arSession_5() { return &___arSession_5; }
	inline void set_arSession_5(ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0 * value)
	{
		___arSession_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arSession_5), (void*)value);
	}
};


// easyar.CameraSource
struct  CameraSource_t50290249836EC0D582E97BCD80F7803EB3D66D75  : public FrameSource_t02E6ED15D84ACA37F80D358EC31B06A21E07632C
{
public:
	// System.Int32 easyar.CameraSource::bufferCapacity
	int32_t ___bufferCapacity_6;

public:
	inline static int32_t get_offset_of_bufferCapacity_6() { return static_cast<int32_t>(offsetof(CameraSource_t50290249836EC0D582E97BCD80F7803EB3D66D75, ___bufferCapacity_6)); }
	inline int32_t get_bufferCapacity_6() const { return ___bufferCapacity_6; }
	inline int32_t* get_address_of_bufferCapacity_6() { return &___bufferCapacity_6; }
	inline void set_bufferCapacity_6(int32_t value)
	{
		___bufferCapacity_6 = value;
	}
};


// easyar.SparseSpatialMapWorkerFrameFilter
struct  SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2  : public FrameFilter_tBF572586253A2067B971C65739AA82AB121662D9
{
public:
	// easyar.SparseSpatialMap easyar.SparseSpatialMapWorkerFrameFilter::<Builder>k__BackingField
	SparseSpatialMap_t26B9E1A5A6835F3F39613A1EE395EEEEF9329315 * ___U3CBuilderU3Ek__BackingField_5;
	// easyar.SparseSpatialMap easyar.SparseSpatialMapWorkerFrameFilter::<Localizer>k__BackingField
	SparseSpatialMap_t26B9E1A5A6835F3F39613A1EE395EEEEF9329315 * ___U3CLocalizerU3Ek__BackingField_6;
	// easyar.SparseSpatialMapManager easyar.SparseSpatialMapWorkerFrameFilter::<Manager>k__BackingField
	SparseSpatialMapManager_tB7AA7A4A3CDEA90D406562E6DAD84D34F683A6D1 * ___U3CManagerU3Ek__BackingField_7;
	// easyar.SparseSpatialMapWorkerFrameFilter/MapLocalizerConfig easyar.SparseSpatialMapWorkerFrameFilter::LocalizerConfig
	MapLocalizerConfig_t72428BF25E41CECF8892A78B4A55A6A82A6C130D * ___LocalizerConfig_8;
	// System.Boolean easyar.SparseSpatialMapWorkerFrameFilter::UseGlobalServiceConfig
	bool ___UseGlobalServiceConfig_9;
	// easyar.SparseSpatialMapWorkerFrameFilter/SpatialMapServiceConfig easyar.SparseSpatialMapWorkerFrameFilter::ServiceConfig
	SpatialMapServiceConfig_t086F77E62E9A3A9F71A3170AC5E3D6B75E92BA6C * ___ServiceConfig_10;
	// easyar.ARSession easyar.SparseSpatialMapWorkerFrameFilter::Session
	ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0 * ___Session_11;
	// UnityEngine.GameObject easyar.SparseSpatialMapWorkerFrameFilter::mapRoot
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___mapRoot_12;
	// easyar.SparseSpatialMap easyar.SparseSpatialMapWorkerFrameFilter::sparseSpatialMapWorker
	SparseSpatialMap_t26B9E1A5A6835F3F39613A1EE395EEEEF9329315 * ___sparseSpatialMapWorker_13;
	// System.String easyar.SparseSpatialMapWorkerFrameFilter::localizedMapID
	String_t* ___localizedMapID_14;
	// System.Collections.Generic.Dictionary`2<System.String,easyar.SparseSpatialMapController> easyar.SparseSpatialMapWorkerFrameFilter::mapControllers
	Dictionary_2_t6308C9F5D80DC7F5363BEB6AB25A46187D6CDDB7 * ___mapControllers_15;
	// System.Boolean easyar.SparseSpatialMapWorkerFrameFilter::isStarted
	bool ___isStarted_16;
	// System.Action`4<easyar.SparseSpatialMapController,easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String> easyar.SparseSpatialMapWorkerFrameFilter::MapLoad
	Action_4_t0575E8501FB3174D965BAAA6F41155442409F864 * ___MapLoad_17;
	// System.Action`4<easyar.SparseSpatialMapController,easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String> easyar.SparseSpatialMapWorkerFrameFilter::MapUnload
	Action_4_t0575E8501FB3174D965BAAA6F41155442409F864 * ___MapUnload_18;
	// System.Action`4<easyar.SparseSpatialMapController,easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String> easyar.SparseSpatialMapWorkerFrameFilter::MapHost
	Action_4_t0575E8501FB3174D965BAAA6F41155442409F864 * ___MapHost_19;
	// easyar.MotionTrackingStatus easyar.SparseSpatialMapWorkerFrameFilter::<TrackingStatus>k__BackingField
	int32_t ___U3CTrackingStatusU3Ek__BackingField_20;
	// easyar.SparseSpatialMapWorkerFrameFilter/Mode easyar.SparseSpatialMapWorkerFrameFilter::<WorkingMode>k__BackingField
	int32_t ___U3CWorkingModeU3Ek__BackingField_21;
	// easyar.SparseSpatialMapController easyar.SparseSpatialMapWorkerFrameFilter::<LocalizedMap>k__BackingField
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758 * ___U3CLocalizedMapU3Ek__BackingField_22;
	// easyar.SparseSpatialMapController easyar.SparseSpatialMapWorkerFrameFilter::<BuilderMapController>k__BackingField
	SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758 * ___U3CBuilderMapControllerU3Ek__BackingField_23;

public:
	inline static int32_t get_offset_of_U3CBuilderU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2, ___U3CBuilderU3Ek__BackingField_5)); }
	inline SparseSpatialMap_t26B9E1A5A6835F3F39613A1EE395EEEEF9329315 * get_U3CBuilderU3Ek__BackingField_5() const { return ___U3CBuilderU3Ek__BackingField_5; }
	inline SparseSpatialMap_t26B9E1A5A6835F3F39613A1EE395EEEEF9329315 ** get_address_of_U3CBuilderU3Ek__BackingField_5() { return &___U3CBuilderU3Ek__BackingField_5; }
	inline void set_U3CBuilderU3Ek__BackingField_5(SparseSpatialMap_t26B9E1A5A6835F3F39613A1EE395EEEEF9329315 * value)
	{
		___U3CBuilderU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CBuilderU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CLocalizerU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2, ___U3CLocalizerU3Ek__BackingField_6)); }
	inline SparseSpatialMap_t26B9E1A5A6835F3F39613A1EE395EEEEF9329315 * get_U3CLocalizerU3Ek__BackingField_6() const { return ___U3CLocalizerU3Ek__BackingField_6; }
	inline SparseSpatialMap_t26B9E1A5A6835F3F39613A1EE395EEEEF9329315 ** get_address_of_U3CLocalizerU3Ek__BackingField_6() { return &___U3CLocalizerU3Ek__BackingField_6; }
	inline void set_U3CLocalizerU3Ek__BackingField_6(SparseSpatialMap_t26B9E1A5A6835F3F39613A1EE395EEEEF9329315 * value)
	{
		___U3CLocalizerU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CLocalizerU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CManagerU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2, ___U3CManagerU3Ek__BackingField_7)); }
	inline SparseSpatialMapManager_tB7AA7A4A3CDEA90D406562E6DAD84D34F683A6D1 * get_U3CManagerU3Ek__BackingField_7() const { return ___U3CManagerU3Ek__BackingField_7; }
	inline SparseSpatialMapManager_tB7AA7A4A3CDEA90D406562E6DAD84D34F683A6D1 ** get_address_of_U3CManagerU3Ek__BackingField_7() { return &___U3CManagerU3Ek__BackingField_7; }
	inline void set_U3CManagerU3Ek__BackingField_7(SparseSpatialMapManager_tB7AA7A4A3CDEA90D406562E6DAD84D34F683A6D1 * value)
	{
		___U3CManagerU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CManagerU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_LocalizerConfig_8() { return static_cast<int32_t>(offsetof(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2, ___LocalizerConfig_8)); }
	inline MapLocalizerConfig_t72428BF25E41CECF8892A78B4A55A6A82A6C130D * get_LocalizerConfig_8() const { return ___LocalizerConfig_8; }
	inline MapLocalizerConfig_t72428BF25E41CECF8892A78B4A55A6A82A6C130D ** get_address_of_LocalizerConfig_8() { return &___LocalizerConfig_8; }
	inline void set_LocalizerConfig_8(MapLocalizerConfig_t72428BF25E41CECF8892A78B4A55A6A82A6C130D * value)
	{
		___LocalizerConfig_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LocalizerConfig_8), (void*)value);
	}

	inline static int32_t get_offset_of_UseGlobalServiceConfig_9() { return static_cast<int32_t>(offsetof(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2, ___UseGlobalServiceConfig_9)); }
	inline bool get_UseGlobalServiceConfig_9() const { return ___UseGlobalServiceConfig_9; }
	inline bool* get_address_of_UseGlobalServiceConfig_9() { return &___UseGlobalServiceConfig_9; }
	inline void set_UseGlobalServiceConfig_9(bool value)
	{
		___UseGlobalServiceConfig_9 = value;
	}

	inline static int32_t get_offset_of_ServiceConfig_10() { return static_cast<int32_t>(offsetof(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2, ___ServiceConfig_10)); }
	inline SpatialMapServiceConfig_t086F77E62E9A3A9F71A3170AC5E3D6B75E92BA6C * get_ServiceConfig_10() const { return ___ServiceConfig_10; }
	inline SpatialMapServiceConfig_t086F77E62E9A3A9F71A3170AC5E3D6B75E92BA6C ** get_address_of_ServiceConfig_10() { return &___ServiceConfig_10; }
	inline void set_ServiceConfig_10(SpatialMapServiceConfig_t086F77E62E9A3A9F71A3170AC5E3D6B75E92BA6C * value)
	{
		___ServiceConfig_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ServiceConfig_10), (void*)value);
	}

	inline static int32_t get_offset_of_Session_11() { return static_cast<int32_t>(offsetof(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2, ___Session_11)); }
	inline ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0 * get_Session_11() const { return ___Session_11; }
	inline ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0 ** get_address_of_Session_11() { return &___Session_11; }
	inline void set_Session_11(ARSession_tB632CBE2FA7F75F583A0BC59AE043E494067BDD0 * value)
	{
		___Session_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Session_11), (void*)value);
	}

	inline static int32_t get_offset_of_mapRoot_12() { return static_cast<int32_t>(offsetof(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2, ___mapRoot_12)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_mapRoot_12() const { return ___mapRoot_12; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_mapRoot_12() { return &___mapRoot_12; }
	inline void set_mapRoot_12(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___mapRoot_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mapRoot_12), (void*)value);
	}

	inline static int32_t get_offset_of_sparseSpatialMapWorker_13() { return static_cast<int32_t>(offsetof(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2, ___sparseSpatialMapWorker_13)); }
	inline SparseSpatialMap_t26B9E1A5A6835F3F39613A1EE395EEEEF9329315 * get_sparseSpatialMapWorker_13() const { return ___sparseSpatialMapWorker_13; }
	inline SparseSpatialMap_t26B9E1A5A6835F3F39613A1EE395EEEEF9329315 ** get_address_of_sparseSpatialMapWorker_13() { return &___sparseSpatialMapWorker_13; }
	inline void set_sparseSpatialMapWorker_13(SparseSpatialMap_t26B9E1A5A6835F3F39613A1EE395EEEEF9329315 * value)
	{
		___sparseSpatialMapWorker_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sparseSpatialMapWorker_13), (void*)value);
	}

	inline static int32_t get_offset_of_localizedMapID_14() { return static_cast<int32_t>(offsetof(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2, ___localizedMapID_14)); }
	inline String_t* get_localizedMapID_14() const { return ___localizedMapID_14; }
	inline String_t** get_address_of_localizedMapID_14() { return &___localizedMapID_14; }
	inline void set_localizedMapID_14(String_t* value)
	{
		___localizedMapID_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___localizedMapID_14), (void*)value);
	}

	inline static int32_t get_offset_of_mapControllers_15() { return static_cast<int32_t>(offsetof(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2, ___mapControllers_15)); }
	inline Dictionary_2_t6308C9F5D80DC7F5363BEB6AB25A46187D6CDDB7 * get_mapControllers_15() const { return ___mapControllers_15; }
	inline Dictionary_2_t6308C9F5D80DC7F5363BEB6AB25A46187D6CDDB7 ** get_address_of_mapControllers_15() { return &___mapControllers_15; }
	inline void set_mapControllers_15(Dictionary_2_t6308C9F5D80DC7F5363BEB6AB25A46187D6CDDB7 * value)
	{
		___mapControllers_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mapControllers_15), (void*)value);
	}

	inline static int32_t get_offset_of_isStarted_16() { return static_cast<int32_t>(offsetof(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2, ___isStarted_16)); }
	inline bool get_isStarted_16() const { return ___isStarted_16; }
	inline bool* get_address_of_isStarted_16() { return &___isStarted_16; }
	inline void set_isStarted_16(bool value)
	{
		___isStarted_16 = value;
	}

	inline static int32_t get_offset_of_MapLoad_17() { return static_cast<int32_t>(offsetof(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2, ___MapLoad_17)); }
	inline Action_4_t0575E8501FB3174D965BAAA6F41155442409F864 * get_MapLoad_17() const { return ___MapLoad_17; }
	inline Action_4_t0575E8501FB3174D965BAAA6F41155442409F864 ** get_address_of_MapLoad_17() { return &___MapLoad_17; }
	inline void set_MapLoad_17(Action_4_t0575E8501FB3174D965BAAA6F41155442409F864 * value)
	{
		___MapLoad_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MapLoad_17), (void*)value);
	}

	inline static int32_t get_offset_of_MapUnload_18() { return static_cast<int32_t>(offsetof(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2, ___MapUnload_18)); }
	inline Action_4_t0575E8501FB3174D965BAAA6F41155442409F864 * get_MapUnload_18() const { return ___MapUnload_18; }
	inline Action_4_t0575E8501FB3174D965BAAA6F41155442409F864 ** get_address_of_MapUnload_18() { return &___MapUnload_18; }
	inline void set_MapUnload_18(Action_4_t0575E8501FB3174D965BAAA6F41155442409F864 * value)
	{
		___MapUnload_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MapUnload_18), (void*)value);
	}

	inline static int32_t get_offset_of_MapHost_19() { return static_cast<int32_t>(offsetof(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2, ___MapHost_19)); }
	inline Action_4_t0575E8501FB3174D965BAAA6F41155442409F864 * get_MapHost_19() const { return ___MapHost_19; }
	inline Action_4_t0575E8501FB3174D965BAAA6F41155442409F864 ** get_address_of_MapHost_19() { return &___MapHost_19; }
	inline void set_MapHost_19(Action_4_t0575E8501FB3174D965BAAA6F41155442409F864 * value)
	{
		___MapHost_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MapHost_19), (void*)value);
	}

	inline static int32_t get_offset_of_U3CTrackingStatusU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2, ___U3CTrackingStatusU3Ek__BackingField_20)); }
	inline int32_t get_U3CTrackingStatusU3Ek__BackingField_20() const { return ___U3CTrackingStatusU3Ek__BackingField_20; }
	inline int32_t* get_address_of_U3CTrackingStatusU3Ek__BackingField_20() { return &___U3CTrackingStatusU3Ek__BackingField_20; }
	inline void set_U3CTrackingStatusU3Ek__BackingField_20(int32_t value)
	{
		___U3CTrackingStatusU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CWorkingModeU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2, ___U3CWorkingModeU3Ek__BackingField_21)); }
	inline int32_t get_U3CWorkingModeU3Ek__BackingField_21() const { return ___U3CWorkingModeU3Ek__BackingField_21; }
	inline int32_t* get_address_of_U3CWorkingModeU3Ek__BackingField_21() { return &___U3CWorkingModeU3Ek__BackingField_21; }
	inline void set_U3CWorkingModeU3Ek__BackingField_21(int32_t value)
	{
		___U3CWorkingModeU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CLocalizedMapU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2, ___U3CLocalizedMapU3Ek__BackingField_22)); }
	inline SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758 * get_U3CLocalizedMapU3Ek__BackingField_22() const { return ___U3CLocalizedMapU3Ek__BackingField_22; }
	inline SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758 ** get_address_of_U3CLocalizedMapU3Ek__BackingField_22() { return &___U3CLocalizedMapU3Ek__BackingField_22; }
	inline void set_U3CLocalizedMapU3Ek__BackingField_22(SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758 * value)
	{
		___U3CLocalizedMapU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CLocalizedMapU3Ek__BackingField_22), (void*)value);
	}

	inline static int32_t get_offset_of_U3CBuilderMapControllerU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2, ___U3CBuilderMapControllerU3Ek__BackingField_23)); }
	inline SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758 * get_U3CBuilderMapControllerU3Ek__BackingField_23() const { return ___U3CBuilderMapControllerU3Ek__BackingField_23; }
	inline SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758 ** get_address_of_U3CBuilderMapControllerU3Ek__BackingField_23() { return &___U3CBuilderMapControllerU3Ek__BackingField_23; }
	inline void set_U3CBuilderMapControllerU3Ek__BackingField_23(SparseSpatialMapController_tD7C2769633C800BCF0D715C2E9A36F919FE33758 * value)
	{
		___U3CBuilderMapControllerU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CBuilderMapControllerU3Ek__BackingField_23), (void*)value);
	}
};


// easyar.SurfaceTrackerFrameFilter
struct  SurfaceTrackerFrameFilter_t2BEA58EA4D95BD41028F6206C9E7990C2394382E  : public FrameFilter_tBF572586253A2067B971C65739AA82AB121662D9
{
public:
	// easyar.SurfaceTracker easyar.SurfaceTrackerFrameFilter::<Tracker>k__BackingField
	SurfaceTracker_t51B55B8398B2F4537CA25FB289A36F39B8B6CC79 * ___U3CTrackerU3Ek__BackingField_5;
	// System.Boolean easyar.SurfaceTrackerFrameFilter::isStarted
	bool ___isStarted_6;

public:
	inline static int32_t get_offset_of_U3CTrackerU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SurfaceTrackerFrameFilter_t2BEA58EA4D95BD41028F6206C9E7990C2394382E, ___U3CTrackerU3Ek__BackingField_5)); }
	inline SurfaceTracker_t51B55B8398B2F4537CA25FB289A36F39B8B6CC79 * get_U3CTrackerU3Ek__BackingField_5() const { return ___U3CTrackerU3Ek__BackingField_5; }
	inline SurfaceTracker_t51B55B8398B2F4537CA25FB289A36F39B8B6CC79 ** get_address_of_U3CTrackerU3Ek__BackingField_5() { return &___U3CTrackerU3Ek__BackingField_5; }
	inline void set_U3CTrackerU3Ek__BackingField_5(SurfaceTracker_t51B55B8398B2F4537CA25FB289A36F39B8B6CC79 * value)
	{
		___U3CTrackerU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTrackerU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_isStarted_6() { return static_cast<int32_t>(offsetof(SurfaceTrackerFrameFilter_t2BEA58EA4D95BD41028F6206C9E7990C2394382E, ___isStarted_6)); }
	inline bool get_isStarted_6() const { return ___isStarted_6; }
	inline bool* get_address_of_isStarted_6() { return &___isStarted_6; }
	inline void set_isStarted_6(bool value)
	{
		___isStarted_6 = value;
	}
};


// easyar.VIOCameraDeviceUnion
struct  VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E  : public CameraSource_t50290249836EC0D582E97BCD80F7803EB3D66D75
{
public:
	// easyar.VIOCameraDeviceUnion/DeviceUnion easyar.VIOCameraDeviceUnion::<Device>k__BackingField
	DeviceUnion_t28AB5438E81AA07F093A6D1A114616FDCB540FC9 * ___U3CDeviceU3Ek__BackingField_7;
	// easyar.VIOCameraDeviceUnion/DeviceChooseStrategy easyar.VIOCameraDeviceUnion::DeviceStrategy
	int32_t ___DeviceStrategy_8;
	// easyar.VIOCameraDeviceUnion/MotionTrackerCameraDeviceParameters easyar.VIOCameraDeviceUnion::DesiredMotionTrackerParameters
	MotionTrackerCameraDeviceParameters_tFCE91F25E86732B2628FCB523A65253DF8930931 * ___DesiredMotionTrackerParameters_9;
	// System.Boolean easyar.VIOCameraDeviceUnion::UpdateCalibrationOnStart
	bool ___UpdateCalibrationOnStart_10;
	// System.Action easyar.VIOCameraDeviceUnion::deviceStart
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___deviceStart_11;
	// System.Action easyar.VIOCameraDeviceUnion::deviceStop
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___deviceStop_12;
	// System.Action easyar.VIOCameraDeviceUnion::deviceClose
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___deviceClose_13;
	// System.Action`1<System.Int32> easyar.VIOCameraDeviceUnion::deviceSetBufferCapacity
	Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * ___deviceSetBufferCapacity_14;
	// System.Func`1<System.Int32> easyar.VIOCameraDeviceUnion::deviceGetBufferCapacity
	Func_1_tCB4CC73D86ED9FF6219A185C0C591F956E5DD1BA * ___deviceGetBufferCapacity_15;
	// System.Action`1<easyar.InputFrameSink> easyar.VIOCameraDeviceUnion::deviceConnect
	Action_1_t2D3D45E97EC048B5CBBB0C12D657C57D2546998E * ___deviceConnect_16;
	// System.Boolean easyar.VIOCameraDeviceUnion::willOpen
	bool ___willOpen_17;
	// System.Boolean easyar.VIOCameraDeviceUnion::arcoreLoaded
	bool ___arcoreLoaded_18;
	// System.Action easyar.VIOCameraDeviceUnion::DeviceCreated
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___DeviceCreated_19;
	// System.Action easyar.VIOCameraDeviceUnion::DeviceOpened
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___DeviceOpened_20;
	// System.Action easyar.VIOCameraDeviceUnion::DeviceClosed
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___DeviceClosed_21;

public:
	inline static int32_t get_offset_of_U3CDeviceU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E, ___U3CDeviceU3Ek__BackingField_7)); }
	inline DeviceUnion_t28AB5438E81AA07F093A6D1A114616FDCB540FC9 * get_U3CDeviceU3Ek__BackingField_7() const { return ___U3CDeviceU3Ek__BackingField_7; }
	inline DeviceUnion_t28AB5438E81AA07F093A6D1A114616FDCB540FC9 ** get_address_of_U3CDeviceU3Ek__BackingField_7() { return &___U3CDeviceU3Ek__BackingField_7; }
	inline void set_U3CDeviceU3Ek__BackingField_7(DeviceUnion_t28AB5438E81AA07F093A6D1A114616FDCB540FC9 * value)
	{
		___U3CDeviceU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDeviceU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_DeviceStrategy_8() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E, ___DeviceStrategy_8)); }
	inline int32_t get_DeviceStrategy_8() const { return ___DeviceStrategy_8; }
	inline int32_t* get_address_of_DeviceStrategy_8() { return &___DeviceStrategy_8; }
	inline void set_DeviceStrategy_8(int32_t value)
	{
		___DeviceStrategy_8 = value;
	}

	inline static int32_t get_offset_of_DesiredMotionTrackerParameters_9() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E, ___DesiredMotionTrackerParameters_9)); }
	inline MotionTrackerCameraDeviceParameters_tFCE91F25E86732B2628FCB523A65253DF8930931 * get_DesiredMotionTrackerParameters_9() const { return ___DesiredMotionTrackerParameters_9; }
	inline MotionTrackerCameraDeviceParameters_tFCE91F25E86732B2628FCB523A65253DF8930931 ** get_address_of_DesiredMotionTrackerParameters_9() { return &___DesiredMotionTrackerParameters_9; }
	inline void set_DesiredMotionTrackerParameters_9(MotionTrackerCameraDeviceParameters_tFCE91F25E86732B2628FCB523A65253DF8930931 * value)
	{
		___DesiredMotionTrackerParameters_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DesiredMotionTrackerParameters_9), (void*)value);
	}

	inline static int32_t get_offset_of_UpdateCalibrationOnStart_10() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E, ___UpdateCalibrationOnStart_10)); }
	inline bool get_UpdateCalibrationOnStart_10() const { return ___UpdateCalibrationOnStart_10; }
	inline bool* get_address_of_UpdateCalibrationOnStart_10() { return &___UpdateCalibrationOnStart_10; }
	inline void set_UpdateCalibrationOnStart_10(bool value)
	{
		___UpdateCalibrationOnStart_10 = value;
	}

	inline static int32_t get_offset_of_deviceStart_11() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E, ___deviceStart_11)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_deviceStart_11() const { return ___deviceStart_11; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_deviceStart_11() { return &___deviceStart_11; }
	inline void set_deviceStart_11(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___deviceStart_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deviceStart_11), (void*)value);
	}

	inline static int32_t get_offset_of_deviceStop_12() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E, ___deviceStop_12)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_deviceStop_12() const { return ___deviceStop_12; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_deviceStop_12() { return &___deviceStop_12; }
	inline void set_deviceStop_12(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___deviceStop_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deviceStop_12), (void*)value);
	}

	inline static int32_t get_offset_of_deviceClose_13() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E, ___deviceClose_13)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_deviceClose_13() const { return ___deviceClose_13; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_deviceClose_13() { return &___deviceClose_13; }
	inline void set_deviceClose_13(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___deviceClose_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deviceClose_13), (void*)value);
	}

	inline static int32_t get_offset_of_deviceSetBufferCapacity_14() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E, ___deviceSetBufferCapacity_14)); }
	inline Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * get_deviceSetBufferCapacity_14() const { return ___deviceSetBufferCapacity_14; }
	inline Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B ** get_address_of_deviceSetBufferCapacity_14() { return &___deviceSetBufferCapacity_14; }
	inline void set_deviceSetBufferCapacity_14(Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * value)
	{
		___deviceSetBufferCapacity_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deviceSetBufferCapacity_14), (void*)value);
	}

	inline static int32_t get_offset_of_deviceGetBufferCapacity_15() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E, ___deviceGetBufferCapacity_15)); }
	inline Func_1_tCB4CC73D86ED9FF6219A185C0C591F956E5DD1BA * get_deviceGetBufferCapacity_15() const { return ___deviceGetBufferCapacity_15; }
	inline Func_1_tCB4CC73D86ED9FF6219A185C0C591F956E5DD1BA ** get_address_of_deviceGetBufferCapacity_15() { return &___deviceGetBufferCapacity_15; }
	inline void set_deviceGetBufferCapacity_15(Func_1_tCB4CC73D86ED9FF6219A185C0C591F956E5DD1BA * value)
	{
		___deviceGetBufferCapacity_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deviceGetBufferCapacity_15), (void*)value);
	}

	inline static int32_t get_offset_of_deviceConnect_16() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E, ___deviceConnect_16)); }
	inline Action_1_t2D3D45E97EC048B5CBBB0C12D657C57D2546998E * get_deviceConnect_16() const { return ___deviceConnect_16; }
	inline Action_1_t2D3D45E97EC048B5CBBB0C12D657C57D2546998E ** get_address_of_deviceConnect_16() { return &___deviceConnect_16; }
	inline void set_deviceConnect_16(Action_1_t2D3D45E97EC048B5CBBB0C12D657C57D2546998E * value)
	{
		___deviceConnect_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deviceConnect_16), (void*)value);
	}

	inline static int32_t get_offset_of_willOpen_17() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E, ___willOpen_17)); }
	inline bool get_willOpen_17() const { return ___willOpen_17; }
	inline bool* get_address_of_willOpen_17() { return &___willOpen_17; }
	inline void set_willOpen_17(bool value)
	{
		___willOpen_17 = value;
	}

	inline static int32_t get_offset_of_arcoreLoaded_18() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E, ___arcoreLoaded_18)); }
	inline bool get_arcoreLoaded_18() const { return ___arcoreLoaded_18; }
	inline bool* get_address_of_arcoreLoaded_18() { return &___arcoreLoaded_18; }
	inline void set_arcoreLoaded_18(bool value)
	{
		___arcoreLoaded_18 = value;
	}

	inline static int32_t get_offset_of_DeviceCreated_19() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E, ___DeviceCreated_19)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_DeviceCreated_19() const { return ___DeviceCreated_19; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_DeviceCreated_19() { return &___DeviceCreated_19; }
	inline void set_DeviceCreated_19(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___DeviceCreated_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DeviceCreated_19), (void*)value);
	}

	inline static int32_t get_offset_of_DeviceOpened_20() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E, ___DeviceOpened_20)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_DeviceOpened_20() const { return ___DeviceOpened_20; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_DeviceOpened_20() { return &___DeviceOpened_20; }
	inline void set_DeviceOpened_20(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___DeviceOpened_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DeviceOpened_20), (void*)value);
	}

	inline static int32_t get_offset_of_DeviceClosed_21() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E, ___DeviceClosed_21)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_DeviceClosed_21() const { return ___DeviceClosed_21; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_DeviceClosed_21() { return &___DeviceClosed_21; }
	inline void set_DeviceClosed_21(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___DeviceClosed_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DeviceClosed_21), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3362[3] = 
{
	U3CU3Ec__DisplayClass65_0_tECCE9C536DE9B2EEE07CEB5F5C3E04A2812AA948::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass65_0_tECCE9C536DE9B2EEE07CEB5F5C3E04A2812AA948::get_offset_of_controller_1(),
	U3CU3Ec__DisplayClass65_0_tECCE9C536DE9B2EEE07CEB5F5C3E04A2812AA948::get_offset_of_callback_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3363[4] = 
{
	U3CU3Ec__DisplayClass66_0_t9C21DCE46449BD59D109191BDBB6045D5FB9A1D3::get_offset_of_name_0(),
	U3CU3Ec__DisplayClass66_0_t9C21DCE46449BD59D109191BDBB6045D5FB9A1D3::get_offset_of_U3CU3E4__this_1(),
	U3CU3Ec__DisplayClass66_0_t9C21DCE46449BD59D109191BDBB6045D5FB9A1D3::get_offset_of_controller_2(),
	U3CU3Ec__DisplayClass66_0_t9C21DCE46449BD59D109191BDBB6045D5FB9A1D3::get_offset_of_callback_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3364[19] = 
{
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2::get_offset_of_U3CBuilderU3Ek__BackingField_5(),
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2::get_offset_of_U3CLocalizerU3Ek__BackingField_6(),
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2::get_offset_of_U3CManagerU3Ek__BackingField_7(),
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2::get_offset_of_LocalizerConfig_8(),
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2::get_offset_of_UseGlobalServiceConfig_9(),
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2::get_offset_of_ServiceConfig_10(),
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2::get_offset_of_Session_11(),
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2::get_offset_of_mapRoot_12(),
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2::get_offset_of_sparseSpatialMapWorker_13(),
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2::get_offset_of_localizedMapID_14(),
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2::get_offset_of_mapControllers_15(),
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2::get_offset_of_isStarted_16(),
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2::get_offset_of_MapLoad_17(),
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2::get_offset_of_MapUnload_18(),
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2::get_offset_of_MapHost_19(),
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2::get_offset_of_U3CTrackingStatusU3Ek__BackingField_20(),
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2::get_offset_of_U3CWorkingModeU3Ek__BackingField_21(),
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2::get_offset_of_U3CLocalizedMapU3Ek__BackingField_22(),
	SparseSpatialMapWorkerFrameFilter_tC4BA8039B502B51C65D90F599746EFAD0F52A5C2::get_offset_of_U3CBuilderMapControllerU3Ek__BackingField_23(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3365[2] = 
{
	SurfaceTrackerFrameFilter_t2BEA58EA4D95BD41028F6206C9E7990C2394382E::get_offset_of_U3CTrackerU3Ek__BackingField_5(),
	SurfaceTrackerFrameFilter_t2BEA58EA4D95BD41028F6206C9E7990C2394382E::get_offset_of_isStarted_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3366[5] = 
{
	DeviceChooseStrategy_t504208EF88006B06CD99E579F32C4B03506DA79E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3367[4] = 
{
	VIODeviceType_tD0CB378CF2E6EAED5AA5F70AF25A233652B37AD1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3368[4] = 
{
	DeviceUnion_t28AB5438E81AA07F093A6D1A114616FDCB540FC9::get_offset_of_motionTrackerCameraDevice_0(),
	DeviceUnion_t28AB5438E81AA07F093A6D1A114616FDCB540FC9::get_offset_of_arKitCameraDevice_1(),
	DeviceUnion_t28AB5438E81AA07F093A6D1A114616FDCB540FC9::get_offset_of_arCoreCameraDevice_2(),
	DeviceUnion_t28AB5438E81AA07F093A6D1A114616FDCB540FC9::get_offset_of_U3CDeviceTypeU3Ek__BackingField_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3369[5] = 
{
	MotionTrackerCameraDeviceParameters_tFCE91F25E86732B2628FCB523A65253DF8930931::get_offset_of_FPS_0(),
	MotionTrackerCameraDeviceParameters_tFCE91F25E86732B2628FCB523A65253DF8930931::get_offset_of_FocusMode_1(),
	MotionTrackerCameraDeviceParameters_tFCE91F25E86732B2628FCB523A65253DF8930931::get_offset_of_Resolution_2(),
	MotionTrackerCameraDeviceParameters_tFCE91F25E86732B2628FCB523A65253DF8930931::get_offset_of_TrackingMode_3(),
	MotionTrackerCameraDeviceParameters_tFCE91F25E86732B2628FCB523A65253DF8930931::get_offset_of_MinQualityLevel_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3370[1] = 
{
	U3CU3Ec__DisplayClass31_0_tFC0DE9EF235A32B0CDD56FBD46A4F96156CB04FE::get_offset_of_downloader_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3371[2] = 
{
	U3CU3Ec__DisplayClass38_0_t69E545D6C9ABDAF5197C97948736476FCA9F56EC::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass38_0_t69E545D6C9ABDAF5197C97948736476FCA9F56EC::get_offset_of_device_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3372[1] = 
{
	U3CU3Ec__DisplayClass39_0_t4CD5AD199A2A20ABD7EDA455F921EAAC020E1CEE::get_offset_of_device_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3373[1] = 
{
	U3CU3Ec__DisplayClass40_0_t6189F2CFDF63149C3B6E82D3DAB2286E14D3CE5A::get_offset_of_device_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3374[15] = 
{
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E::get_offset_of_U3CDeviceU3Ek__BackingField_7(),
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E::get_offset_of_DeviceStrategy_8(),
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E::get_offset_of_DesiredMotionTrackerParameters_9(),
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E::get_offset_of_UpdateCalibrationOnStart_10(),
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E::get_offset_of_deviceStart_11(),
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E::get_offset_of_deviceStop_12(),
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E::get_offset_of_deviceClose_13(),
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E::get_offset_of_deviceSetBufferCapacity_14(),
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E::get_offset_of_deviceGetBufferCapacity_15(),
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E::get_offset_of_deviceConnect_16(),
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E::get_offset_of_willOpen_17(),
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E::get_offset_of_arcoreLoaded_18(),
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E::get_offset_of_DeviceCreated_19(),
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E::get_offset_of_DeviceOpened_20(),
	VIOCameraDeviceUnion_t38725787C73A8DE4196C2D642AD5E43DAA189F2E::get_offset_of_DeviceClosed_21(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3376[1] = 
{
	U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_StaticFields::get_offset_of_U3753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
