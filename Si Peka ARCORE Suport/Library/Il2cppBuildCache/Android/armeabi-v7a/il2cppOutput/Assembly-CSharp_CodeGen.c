﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"


extern const RuntimeMethod* Detail_FunctorOfOutputFrameFromListOfOutputFrame_destroy_m3213152A87BBAB55CB406BB9D08FB45301A33151_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfOutputFrameFromListOfOutputFrame_func_m728DEA484FEF9A32AF0EA5C985684C3A923B6E93_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromBoolAndStringAndString_destroy_mA6C753EE55047102FBA63F1ABC45CE9D2B516010_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromBoolAndStringAndString_func_m32AF4156D52F9B1AEA2A81D03E596F56609DF977_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromBoolAndString_destroy_mDC79451BFE0F528760772F0A015FBC035E6E023E_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromBoolAndString_func_m4786C9B24241EBD28AAF555E9CFCB97344EFB8CB_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromBool_destroy_m3D313F4C383413AAD2FCB52140036EA67F40B985_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromBool_func_m18B5057D02371A6A12534E44D51E79565365602A_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_destroy_m44D24357597F4A3861061A606C66CAC57CBDE0D8_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_func_m47D3C6B1C3FA746A6D370FBC7B1D517706FB3EFE_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromCameraState_destroy_mFF1D0D74D6DDC00A03B9F68ABD23B93BBCB212B2_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromCameraState_func_mC7E9F14253350E76D81DD852C31479AE6FA97541_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromCloudRecognizationResult_destroy_mBF60D4AFDA72F76BFBE41B77BBAACB66D22FB251_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromCloudRecognizationResult_func_m631246C2873CA0786043DC8226B7A1F69BAF973D_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromFeedbackFrame_destroy_mE0489B5AF6DB4645A89B1AA2C5E728AA2085DA06_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromFeedbackFrame_func_mF3A0BCA98C72331241FAC3A693309EBC3570DAD7_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromInputFrame_destroy_mBC40B788EC5FA01C081A8B8F29DD83BD7C04BBE0_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromInputFrame_func_mA15F0CAA1EFCDBBE96BA6D3A17F1AE49FFB6443F_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromLogLevelAndString_destroy_mF22BD5934E396FB1042F8B0E27B54DBE318FCBFE_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromLogLevelAndString_func_m9A78157C61E95CA37E54667EDB501049BE406D6A_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromOutputFrame_destroy_m39E6790C6700E68EEB6EF662B32E84E8150EE773_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromOutputFrame_func_m142DA320BDD782F0665F95929A1FAF518AAB0B80_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromPermissionStatusAndString_destroy_m781B495D25D988C89184294BD62865D9E801969D_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromPermissionStatusAndString_func_m8D9D32B6E616DCFD443DE289BEE2A1A9E10D63B3_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromRecordStatusAndString_destroy_mA47A5359B41B4FD8321FA5CC075E76AD7ABBBCD1_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromRecordStatusAndString_func_mA45DA83CDE8ACEB8F46865D1B86A518290177EBA_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromTargetAndBool_destroy_m407203B6D0165B9CBB93CDC394623D25D1394C3E_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromTargetAndBool_func_m4F4E191A4A3E8C8B1C93A9A83F773843E947D3E0_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromVideoStatus_destroy_mC318C3913E76C1CF3DC21561E52A312033C580E4_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromVideoStatus_func_mB7B7FD5B5858F436F3466A1919AF72453E7BE34F_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoid_destroy_m9A583576B55465A17C01F9B74E416B531702AB80_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoid_func_m8233FE906FAE6473609404B722CF66412E5EFB48_RuntimeMethod_var;



// 0x00000001 System.Void KembaliDariEasyAR::kembaliDariEasyAR()
extern void KembaliDariEasyAR_kembaliDariEasyAR_m5A4148BD3FAABBEF05148EEDD274C2A84E161ADD (void);
// 0x00000002 System.Void KembaliDariEasyAR::aktifkanAR()
extern void KembaliDariEasyAR_aktifkanAR_m18662C8381CB5E87A4A409CE5B9CA0798C87200A (void);
// 0x00000003 System.Void KembaliDariEasyAR::.ctor()
extern void KembaliDariEasyAR__ctor_m232E4884579EC832A4944E2310C9A4D7539D087E (void);
// 0x00000004 System.Void ObjectSpawner::Start()
extern void ObjectSpawner_Start_mB5C9193D6076E2D9586CA1F5862D4C7D9AF008DE (void);
// 0x00000005 System.Void ObjectSpawner::Update()
extern void ObjectSpawner_Update_mBCDB36C4FF21AC8AF713E11BDB1FE504B0EC8D19 (void);
// 0x00000006 System.Void ObjectSpawner::.ctor()
extern void ObjectSpawner__ctor_mD103BAD86ABBCFC5726513C071E31158C1A150D3 (void);
// 0x00000007 System.Void PlacementIndikator::Start()
extern void PlacementIndikator_Start_m5B6CB5353735CDE6E03933108AB81D1491F5B137 (void);
// 0x00000008 System.Void PlacementIndikator::Update()
extern void PlacementIndikator_Update_m135BFAE5D934DB8B9F8D7040A82A782786ECFF4F (void);
// 0x00000009 System.Void PlacementIndikator::.ctor()
extern void PlacementIndikator__ctor_mA4B09B95B69DF6D34E510FE47750015A98BDFD71 (void);
// 0x0000000A System.Void PlaneControl::Awake()
extern void PlaneControl_Awake_m5C2AE092D75FA1EB4A2188B5B540A1DB5007E368 (void);
// 0x0000000B System.Void PlaneControl::Start()
extern void PlaneControl_Start_mBC9449251D6CA209730FB231E4129015EE1B33C3 (void);
// 0x0000000C System.Void PlaneControl::Update()
extern void PlaneControl_Update_mAC2F27CBEAE431DB346B68F325188598F5291C6F (void);
// 0x0000000D System.Void PlaneControl::TogglePlaneDetectionAndVisibility()
extern void PlaneControl_TogglePlaneDetectionAndVisibility_mC69B8FD2F19FD4F2404219864C9C1FEF3FD6228F (void);
// 0x0000000E System.Void PlaneControl::setAllPlaneActiveOrDeactive(System.Boolean)
extern void PlaneControl_setAllPlaneActiveOrDeactive_mA4EB838468BA4B4D9A3153467360F9FCC7E11748 (void);
// 0x0000000F System.Void PlaneControl::.ctor()
extern void PlaneControl__ctor_m45312DC01E68365151FBB48E1774961CCE5FEFC8 (void);
// 0x00000010 System.Void PlaneControlerr::Awake()
extern void PlaneControlerr_Awake_m6596122CFE455A44468FFCF71C5E9020DC158C94 (void);
// 0x00000011 System.Void PlaneControlerr::Start()
extern void PlaneControlerr_Start_m7E6958B9FB8C3AEC852A10D5B46995090DBB7592 (void);
// 0x00000012 System.Void PlaneControlerr::Update()
extern void PlaneControlerr_Update_mA612E45B5188888557F8F059EC6EF1C6C853566C (void);
// 0x00000013 System.Void PlaneControlerr::TogglePlaneDetectionAndVisibility()
extern void PlaneControlerr_TogglePlaneDetectionAndVisibility_mDEEF2B5D0E7CF4EFF0263F04448AF15E2584261E (void);
// 0x00000014 System.Void PlaneControlerr::setAllPlaneActiveOrDeactive(System.Boolean)
extern void PlaneControlerr_setAllPlaneActiveOrDeactive_m6FEFED64E94EE4F94D78B8682D1285FAE5C2D5DE (void);
// 0x00000015 System.Void PlaneControlerr::.ctor()
extern void PlaneControlerr__ctor_mA4B4EC97C45FDB39A861D6ADCDB82D33AD1B4E0C (void);
// 0x00000016 System.Void PortalPlacer::Awake()
extern void PortalPlacer_Awake_mF75A0F743CD792E50FAE8F13E07E6CECEE35E80F (void);
// 0x00000017 System.Void PortalPlacer::Start()
extern void PortalPlacer_Start_mAB9088429140C18FFA055513A151CB7CE1701C7D (void);
// 0x00000018 System.Void PortalPlacer::Update()
extern void PortalPlacer_Update_m878E0F76F8807C08AB9F41A32C681A0777E97F47 (void);
// 0x00000019 System.Void PortalPlacer::.ctor()
extern void PortalPlacer__ctor_m266527257FCC26F3F1DDF63B696465F391018D85 (void);
// 0x0000001A System.Void Potal::Start()
extern void Potal_Start_mC78472AC4C18A70A25252379398AB4394743C104 (void);
// 0x0000001B System.Void Potal::Update()
extern void Potal_Update_m0FB35F0BCE2504F4B8C053F4C351532400671214 (void);
// 0x0000001C System.Void Potal::OnTriggerStay(UnityEngine.Collider)
extern void Potal_OnTriggerStay_m8254040F35D9335D2CFF2E7AF80E68C117A514DB (void);
// 0x0000001D System.Void Potal::TogglePlaneDetectionAndVisibility()
extern void Potal_TogglePlaneDetectionAndVisibility_m10FA42F6907B6BB5866F87BA44F831EB73646543 (void);
// 0x0000001E System.Void Potal::setAllPlaneActiveOrDeactive(System.Boolean)
extern void Potal_setAllPlaneActiveOrDeactive_mE9AD31C5C45F690E4511CE832E803999512142FC (void);
// 0x0000001F System.Void Potal::.ctor()
extern void Potal__ctor_m5B6826C2EA366992F2633BF6FD2CAF7DC5B0540E (void);
// 0x00000020 System.Collections.IEnumerator ARCoreSuport::Start()
extern void ARCoreSuport_Start_mBFD408374D7E30C82694DBE2D3C2896217CE1006 (void);
// 0x00000021 System.Void ARCoreSuport::.ctor()
extern void ARCoreSuport__ctor_mCF22F9C1DB756B05465502964E4B71E732F01C04 (void);
// 0x00000022 System.Void ARCoreSuport/<Start>d__2::.ctor(System.Int32)
extern void U3CStartU3Ed__2__ctor_m76493853135E7B3E98D72CAAFD17980374605DE0 (void);
// 0x00000023 System.Void ARCoreSuport/<Start>d__2::System.IDisposable.Dispose()
extern void U3CStartU3Ed__2_System_IDisposable_Dispose_mF2D6ACAB9BD8472C08C90F50438A0657E8412A70 (void);
// 0x00000024 System.Boolean ARCoreSuport/<Start>d__2::MoveNext()
extern void U3CStartU3Ed__2_MoveNext_mC0911D70346CA9891F9A0FAE1D05E44E7D8DBA86 (void);
// 0x00000025 System.Object ARCoreSuport/<Start>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m42BEA4DE7F2E3D42FAC40F1BD820B34ECA178777 (void);
// 0x00000026 System.Void ARCoreSuport/<Start>d__2::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m4E31F02CE2F73E30AA546D239DA3D2DFCF2DDED3 (void);
// 0x00000027 System.Object ARCoreSuport/<Start>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_mD08A543706D0F525E2B14CA73AD25DDB7336F8B9 (void);
// 0x00000028 System.Void Activate::Start()
extern void Activate_Start_m08C8888DB0894B521E7A391FE19387E224C5C10A (void);
// 0x00000029 System.Void Activate::Update()
extern void Activate_Update_mA3BB4F4A0B7BBFE9B6F62B2FB0086021D43760D4 (void);
// 0x0000002A System.Void Activate::aktifkanGameObject()
extern void Activate_aktifkanGameObject_m23283F6F3095D28077566382E1F926F1BC985A09 (void);
// 0x0000002B System.Void Activate::nonaktifkanGameObject()
extern void Activate_nonaktifkanGameObject_m8E2CEDFD2FA7912D361D44E528696247C7D7D84A (void);
// 0x0000002C System.Void Activate::assss()
extern void Activate_assss_mA1AC697A6E9A852727107D362EEEF8DA3DCCE5FD (void);
// 0x0000002D System.Void Activate::.ctor()
extern void Activate__ctor_mFDE6E64A8AE313D7BCEC986811153A8443E2306C (void);
// 0x0000002E System.Void Anim::Update()
extern void Anim_Update_m246B9B6F3A5A51531603888DD2E0C56FDB496B46 (void);
// 0x0000002F System.Void Anim::playanimation()
extern void Anim_playanimation_m68C20969E30DCA86C059A75C74B6A62FFDF60A1B (void);
// 0x00000030 System.Void Anim::stand()
extern void Anim_stand_m9B3427C6CB70E7B4326709D1318868D5C66E9B21 (void);
// 0x00000031 System.Collections.IEnumerator Anim::delay()
extern void Anim_delay_m49B92C3EC4783A88BB915F7B9E5828E35676DFE3 (void);
// 0x00000032 System.Void Anim::.ctor()
extern void Anim__ctor_mD06BEBFB08FB944CC36A2F70EDCD029A8B0DD0D8 (void);
// 0x00000033 System.Void Anim/<delay>d__3::.ctor(System.Int32)
extern void U3CdelayU3Ed__3__ctor_mB41968B0DEEA004DDB90B2E10A791EB96E48D2CD (void);
// 0x00000034 System.Void Anim/<delay>d__3::System.IDisposable.Dispose()
extern void U3CdelayU3Ed__3_System_IDisposable_Dispose_m1F06D74D5874EB191FBFA69957B9C26470ADAC4A (void);
// 0x00000035 System.Boolean Anim/<delay>d__3::MoveNext()
extern void U3CdelayU3Ed__3_MoveNext_mFDE630DACFE2D69CE5A5DE03FE60482B185AFECF (void);
// 0x00000036 System.Object Anim/<delay>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdelayU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96FF82B4AFBECC14FD6D4D385BD51F77F6B96E26 (void);
// 0x00000037 System.Void Anim/<delay>d__3::System.Collections.IEnumerator.Reset()
extern void U3CdelayU3Ed__3_System_Collections_IEnumerator_Reset_mDDF68C47CED558B3AB02F86D903391C0BED82521 (void);
// 0x00000038 System.Object Anim/<delay>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CdelayU3Ed__3_System_Collections_IEnumerator_get_Current_m9E064515BE7020B0F9EF09EA13C41705F94DE64D (void);
// 0x00000039 System.Void EasyARControler::Start()
extern void EasyARControler_Start_m1092DCC1644878FF6D86EA66E34662C6AA7B6BF7 (void);
// 0x0000003A System.Void EasyARControler::Next()
extern void EasyARControler_Next_m0866CDC4A065A9536088EE8FA586983F22AA60B2 (void);
// 0x0000003B System.Void EasyARControler::prev()
extern void EasyARControler_prev_m274893F4512C3232546C36C9DA28FCF582C59ECC (void);
// 0x0000003C System.Void EasyARControler::PlayAnimasi()
extern void EasyARControler_PlayAnimasi_m8AEF21F340DBC4904B1C88966865EC3CB938E16A (void);
// 0x0000003D System.Void EasyARControler::animCOntrol()
extern void EasyARControler_animCOntrol_mEB0904D4FC397AE091770585D7468F2507E3A02C (void);
// 0x0000003E System.Void EasyARControler::.ctor()
extern void EasyARControler__ctor_mA8B53629B1A3D1766F29B278CE5C7A0D49415FB4 (void);
// 0x0000003F System.Void SelesaiMembacaMateri::selesaiMembacaMateri()
extern void SelesaiMembacaMateri_selesaiMembacaMateri_m0082ABE8F9F8EC2765B51433B8FFE5BBDB7AAC2A (void);
// 0x00000040 System.Void SelesaiMembacaMateri::.ctor()
extern void SelesaiMembacaMateri__ctor_m5E2BAEE86B86E242F69F53043FC61DAFE93CD996 (void);
// 0x00000041 System.Void btnManagement::Start()
extern void btnManagement_Start_mF48E0EBD090E2DDED5807A39183C9FE90099FA4B (void);
// 0x00000042 System.Void btnManagement::cek()
extern void btnManagement_cek_mCCD1DC838CC0B0F1702ED6B5CC3C65E71A8130F4 (void);
// 0x00000043 System.Void btnManagement::Update()
extern void btnManagement_Update_m3E3EB97A2B03CC47E0FCF8BE621A3A64CA2044F6 (void);
// 0x00000044 System.Void btnManagement::.ctor()
extern void btnManagement__ctor_mC22F4E3DF7589275A0EBD5D94A035135B5FCA7DC (void);
// 0x00000045 System.Void Navigasi::OpenScene()
extern void Navigasi_OpenScene_m573932EBABBE9DFB8F7762AE9EB1D9C8C222A7C4 (void);
// 0x00000046 System.Void Navigasi::MenuActivation()
extern void Navigasi_MenuActivation_mA700273D1FA27CD1491E1491E01598885CB0418D (void);
// 0x00000047 System.Void Navigasi::exit()
extern void Navigasi_exit_m674CE2DB540A966B2042A550EE129D75AF2C875B (void);
// 0x00000048 System.Void Navigasi::Aktifkan()
extern void Navigasi_Aktifkan_m613C59BC8DE196A9AF3DFCB4A58BA8212D43AA49 (void);
// 0x00000049 System.Void Navigasi::.ctor()
extern void Navigasi__ctor_m2DA66A4CFA2ABA1173FE29EAEB2AE8FAE319E132 (void);
// 0x0000004A System.Void ManagemntPetunjukFlow::Start()
extern void ManagemntPetunjukFlow_Start_mC80FD3F6FAE704EA1246FC88A22914DFE4C677D3 (void);
// 0x0000004B System.Void ManagemntPetunjukFlow::openInformasi()
extern void ManagemntPetunjukFlow_openInformasi_mFDA0C10D88BF0C146482B68D48E25BF2E41A4FE6 (void);
// 0x0000004C System.Void ManagemntPetunjukFlow::Update()
extern void ManagemntPetunjukFlow_Update_mA4924B875F7103D09EC2A9E47843375DEAB4BB44 (void);
// 0x0000004D System.Void ManagemntPetunjukFlow::.ctor()
extern void ManagemntPetunjukFlow__ctor_mDF42BBBF5A762596FF7D9AE9B054A5096DB1C2CE (void);
// 0x0000004E System.Void ObjectsSpawner::Start()
extern void ObjectsSpawner_Start_mBF52F9451978F4D58543DED1B8E22CC25F2A63A3 (void);
// 0x0000004F System.Void ObjectsSpawner::Activate()
extern void ObjectsSpawner_Activate_mC0A44C68979AA05085759BB56B42E157DB2DC50E (void);
// 0x00000050 System.Void ObjectsSpawner::.ctor()
extern void ObjectsSpawner__ctor_m9BC18DBC0299BC5FD412959A0FEE190ADD31EC01 (void);
// 0x00000051 System.Void PlacementIndicator::Start()
extern void PlacementIndicator_Start_mC0F1E7B8F84514D2EDEA712785CDC9399A376200 (void);
// 0x00000052 System.Void PlacementIndicator::Update()
extern void PlacementIndicator_Update_m04B65B8903A034EA4C717E34BE6B8EF861382FE4 (void);
// 0x00000053 System.Void PlacementIndicator::.ctor()
extern void PlacementIndicator__ctor_mC5EF49C57C7F6519CA9E925EC1A0B13C7D3A08BE (void);
// 0x00000054 System.Void aktifkanARFoundation::aktifkan()
extern void aktifkanARFoundation_aktifkan_m41637420386F2152FD39F4E6FC6597E46C4B87D1 (void);
// 0x00000055 System.Void aktifkanARFoundation::.ctor()
extern void aktifkanARFoundation__ctor_m4BB032E4BE8924FE10675A4D8CA61191C424E469 (void);
// 0x00000056 System.Void btnClickOnPortal::Start()
extern void btnClickOnPortal_Start_m6FBE4A93973B9C19E5011B86BB48F01B8F2FFF02 (void);
// 0x00000057 System.Void btnClickOnPortal::OnMouseDown()
extern void btnClickOnPortal_OnMouseDown_m929B6F6F5A4FA7C0388A44178F76918A9B4E76CD (void);
// 0x00000058 System.Void btnClickOnPortal::.ctor()
extern void btnClickOnPortal__ctor_m2997580A817FAA16CCC54E7B9FC6D25AA6B0B02C (void);
// 0x00000059 System.Void bukaSpawner::karakterAnimPlay()
extern void bukaSpawner_karakterAnimPlay_mBC5542E23F774DA358CE324B5D2E068CC3370C6C (void);
// 0x0000005A System.Void bukaSpawner::Start()
extern void bukaSpawner_Start_m43ADC1E1A6F16021C060577B717B613512E6ADA0 (void);
// 0x0000005B System.Void bukaSpawner::OnMouseDown()
extern void bukaSpawner_OnMouseDown_mFE274A6A878276CA61A520AAB129EB28313F0727 (void);
// 0x0000005C System.Void bukaSpawner::.ctor()
extern void bukaSpawner__ctor_m27A97EA406DD50760D700008FA38099EDF9C8F48 (void);
// 0x0000005D System.Void managementTextInSimulasi::Start()
extern void managementTextInSimulasi_Start_mEE66FDC6C39BF44E18A4A3438C49EEBB2FB98620 (void);
// 0x0000005E System.Void managementTextInSimulasi::Update()
extern void managementTextInSimulasi_Update_m59357B8EEFA71F874FF3984DC01B28709EE7939C (void);
// 0x0000005F System.Void managementTextInSimulasi::.ctor()
extern void managementTextInSimulasi__ctor_m88D48C28281F88CA536D6B09203C69A8E51C4D5D (void);
// 0x00000060 System.Void simulasiManagement::Start()
extern void simulasiManagement_Start_mAD4BD2C81906E79DC15DDBABB29B8091F879BC05 (void);
// 0x00000061 System.Void simulasiManagement::btnSimulasiOnCLick()
extern void simulasiManagement_btnSimulasiOnCLick_m157E9802A2356279E116807AD25B1DB19ECF7960 (void);
// 0x00000062 System.Void simulasiManagement::btnBackToPortalClicked()
extern void simulasiManagement_btnBackToPortalClicked_mBFD598E7B1B4716069DF00A24230D467311845DC (void);
// 0x00000063 System.Void simulasiManagement::.ctor()
extern void simulasiManagement__ctor_m80F20B2919404EFAE2B21839EB41ACB1883B50D8 (void);
// 0x00000064 System.Void managementAnimasiSimulasi::Start()
extern void managementAnimasiSimulasi_Start_mA01F9567FD11A206538A28450925DD056CA2B294 (void);
// 0x00000065 System.Void managementAnimasiSimulasi::Update()
extern void managementAnimasiSimulasi_Update_m4BFF1AD5945BDEE79A68F8AB26A663E55CD5CF86 (void);
// 0x00000066 System.Void managementAnimasiSimulasi::aktifkanAnimasiIdle()
extern void managementAnimasiSimulasi_aktifkanAnimasiIdle_m8A4192A4CBA87EA14D9209CE18F26E278EBEF652 (void);
// 0x00000067 System.Void managementAnimasiSimulasi::nextKlik()
extern void managementAnimasiSimulasi_nextKlik_mA30AF6B07DB9CE154F8B5EBFEF0CE91E0E801518 (void);
// 0x00000068 System.Void managementAnimasiSimulasi::PrevKlik()
extern void managementAnimasiSimulasi_PrevKlik_m9AFA86B69756DD9C9C7B102280281A5E888B632E (void);
// 0x00000069 System.Void managementAnimasiSimulasi::PlayVideo()
extern void managementAnimasiSimulasi_PlayVideo_m7A1EEF33C8BEF572DE34C490782889100FBE35AE (void);
// 0x0000006A System.Void managementAnimasiSimulasi::PauseVideo()
extern void managementAnimasiSimulasi_PauseVideo_m45429CE849824859C0A38295DC8453452A3DBE76 (void);
// 0x0000006B System.Void managementAnimasiSimulasi::PlaykumpulanAnimasi()
extern void managementAnimasiSimulasi_PlaykumpulanAnimasi_m857A5B1E4D2606440522722F7D63F23924AA9FFB (void);
// 0x0000006C System.Void managementAnimasiSimulasi::.ctor()
extern void managementAnimasiSimulasi__ctor_m475216434EA1295B759C9730B82E22998F14CE17 (void);
// 0x0000006D System.Void Puzel::Start()
extern void Puzel_Start_m14B74B4823B527582FBF9A8255771F246DF6F4C5 (void);
// 0x0000006E System.Void Puzel::DragA()
extern void Puzel_DragA_m28A63CD501FB6FB3D2A200CABE8697918E53098F (void);
// 0x0000006F System.Void Puzel::DragB()
extern void Puzel_DragB_mA27B72B871E348D12BA6A64BA3A4F52B165DE8A6 (void);
// 0x00000070 System.Void Puzel::DragC()
extern void Puzel_DragC_m769A8662D4347E4EAE884AA7F818F1A3E2595E99 (void);
// 0x00000071 System.Void Puzel::DragD()
extern void Puzel_DragD_mB614451940FD020131BC13F3E54927D19E431783 (void);
// 0x00000072 System.Void Puzel::DragE()
extern void Puzel_DragE_m80B0D1EE5325BE8D7BA6C5054604655E153662CA (void);
// 0x00000073 System.Void Puzel::DragF()
extern void Puzel_DragF_m456BC4D4D9237A228EAB68744D8B9448109C98E0 (void);
// 0x00000074 System.Void Puzel::DragG()
extern void Puzel_DragG_m274EC2D0157B68AF06C9B03BEAC872BB88C8029E (void);
// 0x00000075 System.Void Puzel::DragH()
extern void Puzel_DragH_m80310CC37453D25E45D951155959286216EB6613 (void);
// 0x00000076 System.Void Puzel::DragI()
extern void Puzel_DragI_m45C978FF9078AD152566445A38E682955B7C2EDD (void);
// 0x00000077 System.Void Puzel::DropA()
extern void Puzel_DropA_m44A99CFE681F22AA7E19D5C77C552F7C3A673B39 (void);
// 0x00000078 System.Void Puzel::DropB()
extern void Puzel_DropB_m19D38314C70F9EDBFEC5257E91131F8D222F808C (void);
// 0x00000079 System.Void Puzel::DropC()
extern void Puzel_DropC_m15C6DF045568FB2058A7FFD40AB5495F25598C4C (void);
// 0x0000007A System.Void Puzel::DropD()
extern void Puzel_DropD_mEBDC6A624F90DD312C8E889E6041B27C97580117 (void);
// 0x0000007B System.Void Puzel::DropE()
extern void Puzel_DropE_mB0AECA6DD370BBFC16E941C72622E6A4BEF93F05 (void);
// 0x0000007C System.Void Puzel::DropF()
extern void Puzel_DropF_m68DDF56F06320278ABE5C69BC8CB0B3081195ACB (void);
// 0x0000007D System.Void Puzel::DropG()
extern void Puzel_DropG_mB4CA6A0237FD6CD6F2C941801CFD5E14F597203E (void);
// 0x0000007E System.Void Puzel::DropH()
extern void Puzel_DropH_m70A03B096B0627EA0B78CB17ED671F04463161EC (void);
// 0x0000007F System.Void Puzel::DropI()
extern void Puzel_DropI_mD8B728217AE9413B1A14770FBF38F1C6427748B4 (void);
// 0x00000080 System.Void Puzel::ResetNilai()
extern void Puzel_ResetNilai_m6F8B5397C06BB920DF97CB3A2EB33A2ABDE3E70F (void);
// 0x00000081 System.Void Puzel::Update()
extern void Puzel_Update_mF6891C58AB28A5104F307C217E4FDEB89E735B68 (void);
// 0x00000082 System.Void Puzel::.ctor()
extern void Puzel__ctor_m67EADCAA4061E27AE3FBA845818C869207A04739 (void);
// 0x00000083 System.Void hasilPuzel::Start()
extern void hasilPuzel_Start_mF82B330C7C647D55647C0B4904B99BB2F26D8650 (void);
// 0x00000084 System.Void hasilPuzel::Update()
extern void hasilPuzel_Update_m5CCC3D5A4D4484E75724646A5D4D0427E6C14514 (void);
// 0x00000085 System.Void hasilPuzel::.ctor()
extern void hasilPuzel__ctor_m0C60ED81BDFCF89B3728FB611ECDE9D71BDD93F7 (void);
// 0x00000086 System.Void ScreenShotShare::ShareScreenshotWithText()
extern void ScreenShotShare_ShareScreenshotWithText_m64E941F7BA653D37750F8EE9EAFF406C6ADB2E22 (void);
// 0x00000087 System.Void ScreenShotShare::Share()
extern void ScreenShotShare_Share_m66BF412525C3CCD017FD8E999F8794E731852E89 (void);
// 0x00000088 System.Collections.IEnumerator ScreenShotShare::ShareScreenshot()
extern void ScreenShotShare_ShareScreenshot_m246A5054CA729C909BFCC37EFD7826BDCD8CE5A1 (void);
// 0x00000089 System.Void ScreenShotShare::.ctor()
extern void ScreenShotShare__ctor_m76FF50308F01467D18A49DA80ADBD099F6BBBEEA (void);
// 0x0000008A System.Void ScreenShotShare/<ShareScreenshot>d__7::.ctor(System.Int32)
extern void U3CShareScreenshotU3Ed__7__ctor_m272767E0AF9F078E6DCED77A092EBC7439D54C70 (void);
// 0x0000008B System.Void ScreenShotShare/<ShareScreenshot>d__7::System.IDisposable.Dispose()
extern void U3CShareScreenshotU3Ed__7_System_IDisposable_Dispose_mBD94F610B2DE5685FB673119CDF7237905DD2B27 (void);
// 0x0000008C System.Boolean ScreenShotShare/<ShareScreenshot>d__7::MoveNext()
extern void U3CShareScreenshotU3Ed__7_MoveNext_mE87D776E207C595C06B0A9CDA7F6C9087911D70B (void);
// 0x0000008D System.Object ScreenShotShare/<ShareScreenshot>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShareScreenshotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFCABB45518780891A94828CA071C36CF1A36E5C4 (void);
// 0x0000008E System.Void ScreenShotShare/<ShareScreenshot>d__7::System.Collections.IEnumerator.Reset()
extern void U3CShareScreenshotU3Ed__7_System_Collections_IEnumerator_Reset_mBFE3415BA35E908E556C1CB1A61028BE711AB897 (void);
// 0x0000008F System.Object ScreenShotShare/<ShareScreenshot>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CShareScreenshotU3Ed__7_System_Collections_IEnumerator_get_Current_mB20376FE895FF4BAF3C18F02580F4DE96ABBE28D (void);
// 0x00000090 System.Void MusicPlayer::Start()
extern void MusicPlayer_Start_m196809CDCF89356E622F9AAA17375AC2A0F4DAB5 (void);
// 0x00000091 System.Void MusicPlayer::UpdateVolume(System.Single)
extern void MusicPlayer_UpdateVolume_mC4DE5B277CD5443466F505D98A14754FA816B6DC (void);
// 0x00000092 System.Void MusicPlayer::.ctor()
extern void MusicPlayer__ctor_mA83600A144210E8586F4618ED75C614BFE6450FC (void);
// 0x00000093 System.Void PlayerNameSetting::Start()
extern void PlayerNameSetting_Start_m74AF951D982F204EE018879F5135FC9F01F6CC79 (void);
// 0x00000094 System.Void PlayerNameSetting::Update()
extern void PlayerNameSetting_Update_mCDFF975992B9E4C2548CB09D098816D7BAEC626E (void);
// 0x00000095 System.Void PlayerNameSetting::setUserName()
extern void PlayerNameSetting_setUserName_mB529CC03F5ECC3167FAF427DA4802308765990DD (void);
// 0x00000096 System.Void PlayerNameSetting::ResetUserName()
extern void PlayerNameSetting_ResetUserName_mF265BD82F5F582A894B8F422695A779350985404 (void);
// 0x00000097 System.Void PlayerNameSetting::.ctor()
extern void PlayerNameSetting__ctor_m46602F82A3957B8CAD73C02E30D3C71BE7E0B440 (void);
// 0x00000098 System.Void ShowUserName::Start()
extern void ShowUserName_Start_m473417E40D053C0DA3804D5F6B576D8A085D6C5C (void);
// 0x00000099 System.Void ShowUserName::Update()
extern void ShowUserName_Update_m576972F642F519E619BB9722317ADC543FEF1B90 (void);
// 0x0000009A System.Void ShowUserName::.ctor()
extern void ShowUserName__ctor_m42C72C60BFBF2853C045D29CD02162976303C087 (void);
// 0x0000009B System.Void DialogText3::Start()
extern void DialogText3_Start_mFCD1B78FFC3320229DFF74FBA38494231277D759 (void);
// 0x0000009C System.Void DialogText3::sangatBaikFunction()
extern void DialogText3_sangatBaikFunction_m720E11482673EEFC207C4A4EE4173017391A006D (void);
// 0x0000009D System.Void DialogText3::kurangBaikFunction()
extern void DialogText3_kurangBaikFunction_m14095A431941002F103A8D046C0D756EBA9D63ED (void);
// 0x0000009E System.Void DialogText3::.ctor()
extern void DialogText3__ctor_m2586B91AF3A964E5A8F49B466FF009A7F1EE9AA6 (void);
// 0x0000009F System.Void ShowUserNameInStory::Start()
extern void ShowUserNameInStory_Start_mD469348304F28D75A8F946A37B752CFF5244DED4 (void);
// 0x000000A0 System.Void ShowUserNameInStory::Update()
extern void ShowUserNameInStory_Update_m17F8E6D341122CC319EF219FB27FB55E1F0D81FC (void);
// 0x000000A1 System.Void ShowUserNameInStory::.ctor()
extern void ShowUserNameInStory__ctor_m56263559375C3354928A6562E3C5C6646FB29F3A (void);
// 0x000000A2 System.Void StorymanagementScript::Start()
extern void StorymanagementScript_Start_m54ACBB1633BAA46C22E3F7D03AE3D620E09C7263 (void);
// 0x000000A3 System.Void StorymanagementScript::setUserName()
extern void StorymanagementScript_setUserName_mB1BDC2C5481FB40F0731BE31ADEFB56E19AB5427 (void);
// 0x000000A4 System.Void StorymanagementScript::ResetUserName()
extern void StorymanagementScript_ResetUserName_mA7884C10679D41D623BFCE781A2434327D95CB6A (void);
// 0x000000A5 System.Void StorymanagementScript::.ctor()
extern void StorymanagementScript__ctor_mB5EBE5FDFC3670914FAB4DE6109F7164D677224A (void);
// 0x000000A6 System.Void loadStart::Start()
extern void loadStart_Start_m5E430157AF3A4B38EE1E04DA7B97AC027867BAAF (void);
// 0x000000A7 System.Void loadStart::Update()
extern void loadStart_Update_m6BBD2E3F2DFA59386BF81AB1D2ABBF4DBA937939 (void);
// 0x000000A8 System.Void loadStart::.ctor()
extern void loadStart__ctor_m24BB1A8FCEA23ABF831E194250FE3B31BFE4F8FD (void);
// 0x000000A9 System.Void tombolStoryLewati::Start()
extern void tombolStoryLewati_Start_m5921EF680B1593EE1EDE534871A69BD97D332174 (void);
// 0x000000AA System.Void tombolStoryLewati::Update()
extern void tombolStoryLewati_Update_m608B4116A06F7E1EDF4F042EEDEF528370692D5F (void);
// 0x000000AB System.Void tombolStoryLewati::.ctor()
extern void tombolStoryLewati__ctor_mC51FCD1001F316CDCF03E2040530484760D2C79B (void);
// 0x000000AC System.Void btnTest::Test()
extern void btnTest_Test_mCF7869A83B7AE27B6C7A878A855327CB7840871D (void);
// 0x000000AD System.Void btnTest::.ctor()
extern void btnTest__ctor_m5EE8B3A0F712B8F17AE1DC62D71C57BC56AC2D0E (void);
// 0x000000AE System.Void CustomSoal::Start()
extern void CustomSoal_Start_mBC47880AF29DC211386632CBCAF32532F4026F28 (void);
// 0x000000AF System.Void CustomSoal::jawaban(System.Boolean)
extern void CustomSoal_jawaban_m22C4AA0F946C65C9FF46C27AE751B8C7ABE9B740 (void);
// 0x000000B0 System.Void CustomSoal::Update()
extern void CustomSoal_Update_m0073C4D0BA1FF40D00D159D6A71066837CD8599E (void);
// 0x000000B1 System.Void CustomSoal::.ctor()
extern void CustomSoal__ctor_mCCB0804E9F516BDA0890D6FE4ADB89C4950F6645 (void);
// 0x000000B2 System.Void HasilQuis::Start()
extern void HasilQuis_Start_m8D932902D418AB6DEEF2CF057BC1645848AF186C (void);
// 0x000000B3 System.Void HasilQuis::Update()
extern void HasilQuis_Update_m605F585D1E35CF1FF5BA8CF09D6359DFA4E24E53 (void);
// 0x000000B4 System.Void HasilQuis::.ctor()
extern void HasilQuis__ctor_m35E837EBA8347446331B6CBCA1F01481E5C53BED (void);
// 0x000000B5 System.Void Randomm::Start()
extern void Randomm_Start_m114460E631D0F6BE85193526DDF4EA659532F1E5 (void);
// 0x000000B6 System.Void Randomm::Update()
extern void Randomm_Update_m5918D3E8DFCF928167FD00444399A3CB1877FB5E (void);
// 0x000000B7 System.Void Randomm::RandomSoal()
extern void Randomm_RandomSoal_m0A5678454724EFCE70A04BD2E4AA44C06C2799E6 (void);
// 0x000000B8 System.Void Randomm::.ctor()
extern void Randomm__ctor_m576FF3A9151ED34AAADC1B20ADBB99C099972CA6 (void);
// 0x000000B9 System.Void openingQuis::Start()
extern void openingQuis_Start_m60390CED62CE4D3F3C132C6BAA141A449FCA130F (void);
// 0x000000BA System.Void openingQuis::SceneQuisOpen()
extern void openingQuis_SceneQuisOpen_mD104430B324D3BC1DEEE86ABB8A5167D644767CC (void);
// 0x000000BB System.Void openingQuis::.ctor()
extern void openingQuis__ctor_mB36E233662D1A595341AAE5D61F849155DD1124E (void);
// 0x000000BC System.Void Loading::LoadScene()
extern void Loading_LoadScene_m1E31C730D00720B81EFBC578D3FD0C11A4D79B56 (void);
// 0x000000BD System.Void Loading::sceneStorySudahDibuka()
extern void Loading_sceneStorySudahDibuka_m6D7B39264D158CC50CD84587D1188378301EE031 (void);
// 0x000000BE System.Void Loading::.ctor()
extern void Loading__ctor_mE163CCF6BD5A7EDA90F61A352261F32C2DE99FFB (void);
// 0x000000BF System.Void aktifkanAnimasiPlayer::PlayAnimasi()
extern void aktifkanAnimasiPlayer_PlayAnimasi_mE72F4442D762ABB8559171C3B0D1508ED82813AF (void);
// 0x000000C0 System.Void aktifkanAnimasiPlayer::.ctor()
extern void aktifkanAnimasiPlayer__ctor_m6FA86E01FDE0035C4A342545457F048AB544ED8D (void);
// 0x000000C1 System.Void arCameraOpenPremission::Start()
extern void arCameraOpenPremission_Start_m1126791C268C532291FE4BC5A39E4D3016319548 (void);
// 0x000000C2 System.Void arCameraOpenPremission::Update()
extern void arCameraOpenPremission_Update_m14BB67B3AD2DA273F30E2AE2D0AB77918B1FA092 (void);
// 0x000000C3 System.Void arCameraOpenPremission::.ctor()
extern void arCameraOpenPremission__ctor_m2B6CF41AAA6926494DF46384317089A03A709DCC (void);
// 0x000000C4 System.Void bacaTutupMateri::Start()
extern void bacaTutupMateri_Start_m0502E114A0C09BBCAE995358548EA108D1DA5AA1 (void);
// 0x000000C5 System.Void bacaTutupMateri::Update()
extern void bacaTutupMateri_Update_m39718EF691286B08B92DB339B0265658EFB59571 (void);
// 0x000000C6 System.Void bacaTutupMateri::.ctor()
extern void bacaTutupMateri__ctor_m5CC18EC7E0F48156D11D6142A4E4935CF98C21DD (void);
// 0x000000C7 System.Void cekLulus::Start()
extern void cekLulus_Start_m8DDB15CD052E446410A7D7B52BCCFBFC61CF158C (void);
// 0x000000C8 System.Void cekLulus::Update()
extern void cekLulus_Update_m998ACFA6C2F27219C26E7F753313B9B40CC78162 (void);
// 0x000000C9 System.Void cekLulus::.ctor()
extern void cekLulus__ctor_m959BC18ED7F73C262E28261F06F86217B871084C (void);
// 0x000000CA System.Void videoPlayerControler::Start()
extern void videoPlayerControler_Start_m5F789D894C6E086C351B000691D0FA5A4912E90D (void);
// 0x000000CB System.Void videoPlayerControler::Update()
extern void videoPlayerControler_Update_m3F1AF2AC8196B0BCC2B9F9E6F65A2935B94DE0E3 (void);
// 0x000000CC System.Void videoPlayerControler::.ctor()
extern void videoPlayerControler__ctor_m0D00727283C93BC954117C4FCF8DB40FC7507F89 (void);
// 0x000000CD System.Void easyar.Detail::easyar_String_from_utf8(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_String_from_utf8_m2DA7CAF4C393BD0D25A8B97C5CD294A4A25006B3 (void);
// 0x000000CE System.Void easyar.Detail::easyar_String_from_utf8_begin(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_String_from_utf8_begin_mDB4D71C0566596CB79CDC60A78E9308A78040D6F (void);
// 0x000000CF System.IntPtr easyar.Detail::easyar_String_begin(System.IntPtr)
extern void Detail_easyar_String_begin_m80F1E6E27F082E51C13AF2FB7E2747B7821F9D73 (void);
// 0x000000D0 System.IntPtr easyar.Detail::easyar_String_end(System.IntPtr)
extern void Detail_easyar_String_end_mA908EC1799B9F8F4F974B8F08107EF2C5E70A621 (void);
// 0x000000D1 System.Void easyar.Detail::easyar_String_copy(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_String_copy_m9F7AF2499A7DA63113578874CBB0F5A91F983F40 (void);
// 0x000000D2 System.Void easyar.Detail::easyar_String__dtor(System.IntPtr)
extern void Detail_easyar_String__dtor_m2DED6C3DD6968034E40AFAA0066CFC5BD7A048FE (void);
// 0x000000D3 System.Void easyar.Detail::easyar_ObjectTargetParameters__ctor(System.IntPtr&)
extern void Detail_easyar_ObjectTargetParameters__ctor_m637003D10781D34D80C3CB7D5DB203882D0094D4 (void);
// 0x000000D4 System.Void easyar.Detail::easyar_ObjectTargetParameters_bufferDictionary(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTargetParameters_bufferDictionary_m2D457379C80C16558B9AA9032852C7299B010EEB (void);
// 0x000000D5 System.Void easyar.Detail::easyar_ObjectTargetParameters_setBufferDictionary(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ObjectTargetParameters_setBufferDictionary_m361CE7143BF579234CB0FFF2C12420AE2CD856FB (void);
// 0x000000D6 System.Void easyar.Detail::easyar_ObjectTargetParameters_objPath(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTargetParameters_objPath_mB65B60D291C1E534A30F6E18A603EBA13B666ABD (void);
// 0x000000D7 System.Void easyar.Detail::easyar_ObjectTargetParameters_setObjPath(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ObjectTargetParameters_setObjPath_m8A4FECE44F4D097042CC63C0D5C9176BFE7D8924 (void);
// 0x000000D8 System.Void easyar.Detail::easyar_ObjectTargetParameters_name(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTargetParameters_name_mEBC3980323799362B9221DCF32CE1D757AC2C08A (void);
// 0x000000D9 System.Void easyar.Detail::easyar_ObjectTargetParameters_setName(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ObjectTargetParameters_setName_m3A7BBB751A11E47B3F45433C09095C05415E9EB4 (void);
// 0x000000DA System.Void easyar.Detail::easyar_ObjectTargetParameters_uid(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTargetParameters_uid_m3F691FCAD5ED21152FB40412CC6F4D464B085135 (void);
// 0x000000DB System.Void easyar.Detail::easyar_ObjectTargetParameters_setUid(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ObjectTargetParameters_setUid_m69D87019EC1BF25A92B8119B8AF5FD1571E931B1 (void);
// 0x000000DC System.Void easyar.Detail::easyar_ObjectTargetParameters_meta(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTargetParameters_meta_m78D60ADBF5FDEE14A7A8746025CC8E738D66377E (void);
// 0x000000DD System.Void easyar.Detail::easyar_ObjectTargetParameters_setMeta(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ObjectTargetParameters_setMeta_m9E19EFFBD10EE50CFEB0E94B2F0DDC6CFD6C0253 (void);
// 0x000000DE System.Single easyar.Detail::easyar_ObjectTargetParameters_scale(System.IntPtr)
extern void Detail_easyar_ObjectTargetParameters_scale_m5591D3289429860EF877BED3D550512B933209FA (void);
// 0x000000DF System.Void easyar.Detail::easyar_ObjectTargetParameters_setScale(System.IntPtr,System.Single)
extern void Detail_easyar_ObjectTargetParameters_setScale_m826B60C7D3AA452FE4371B4F700FFCCFDC0EAE2A (void);
// 0x000000E0 System.Void easyar.Detail::easyar_ObjectTargetParameters__dtor(System.IntPtr)
extern void Detail_easyar_ObjectTargetParameters__dtor_m60AB2D26961BDC225AD482947647717E214E780E (void);
// 0x000000E1 System.Void easyar.Detail::easyar_ObjectTargetParameters__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTargetParameters__retain_m8AF0772D8450F8AF6C5D0B35103A5F0C30F71D38 (void);
// 0x000000E2 System.IntPtr easyar.Detail::easyar_ObjectTargetParameters__typeName(System.IntPtr)
extern void Detail_easyar_ObjectTargetParameters__typeName_m0A4DABD98CF0388AF11B16F25D9E999A685F3B04 (void);
// 0x000000E3 System.Void easyar.Detail::easyar_ObjectTarget__ctor(System.IntPtr&)
extern void Detail_easyar_ObjectTarget__ctor_m2EDDAA4703683E11267B44737BEB26D3E5646C41 (void);
// 0x000000E4 System.Void easyar.Detail::easyar_ObjectTarget_createFromParameters(System.IntPtr,easyar.Detail/OptionalOfObjectTarget&)
extern void Detail_easyar_ObjectTarget_createFromParameters_m470466EB6BF9AAD4921F47D996C62AC3A61C772A (void);
// 0x000000E5 System.Void easyar.Detail::easyar_ObjectTarget_createFromObjectFile(System.IntPtr,easyar.StorageType,System.IntPtr,System.IntPtr,System.IntPtr,System.Single,easyar.Detail/OptionalOfObjectTarget&)
extern void Detail_easyar_ObjectTarget_createFromObjectFile_m76BBA0202F75E7C9F5F39361E143353D21F32D16 (void);
// 0x000000E6 System.Single easyar.Detail::easyar_ObjectTarget_scale(System.IntPtr)
extern void Detail_easyar_ObjectTarget_scale_m7E0D3C5AA15A14221D5427ABE6D960AC4B3BCE6C (void);
// 0x000000E7 System.Void easyar.Detail::easyar_ObjectTarget_boundingBox(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTarget_boundingBox_mDA96465330FD5B9EA92FB95F1F1B2F71B6A419FE (void);
// 0x000000E8 System.Boolean easyar.Detail::easyar_ObjectTarget_setScale(System.IntPtr,System.Single)
extern void Detail_easyar_ObjectTarget_setScale_m50F08EAC87B5EE26C9D1051819222ECEACF52F57 (void);
// 0x000000E9 System.Int32 easyar.Detail::easyar_ObjectTarget_runtimeID(System.IntPtr)
extern void Detail_easyar_ObjectTarget_runtimeID_mF9FA8E9A2A0B0E16096B38C1A6BE54A09A0F26B0 (void);
// 0x000000EA System.Void easyar.Detail::easyar_ObjectTarget_uid(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTarget_uid_m6297C6C66FAB9FC7622E5A06E2E63AD7272E591C (void);
// 0x000000EB System.Void easyar.Detail::easyar_ObjectTarget_name(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTarget_name_m5A22B3B603982084D94C52A92461360DA8FEEECC (void);
// 0x000000EC System.Void easyar.Detail::easyar_ObjectTarget_setName(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ObjectTarget_setName_mFE0BF726DB2173ABD89B6CEA583402A00DC386C9 (void);
// 0x000000ED System.Void easyar.Detail::easyar_ObjectTarget_meta(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTarget_meta_m1A7E9B2D79F1B9734827F6AC286485C3F280F0F2 (void);
// 0x000000EE System.Void easyar.Detail::easyar_ObjectTarget_setMeta(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ObjectTarget_setMeta_mD487FE98D13C41DFCDF8B183DAE2997F0FC0636B (void);
// 0x000000EF System.Void easyar.Detail::easyar_ObjectTarget__dtor(System.IntPtr)
extern void Detail_easyar_ObjectTarget__dtor_m04F8976E44AB38C1A3B16CA5979D4B850238B779 (void);
// 0x000000F0 System.Void easyar.Detail::easyar_ObjectTarget__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTarget__retain_m47A9EE0292C69A5585A2BDCEB10C81A0F62AE074 (void);
// 0x000000F1 System.IntPtr easyar.Detail::easyar_ObjectTarget__typeName(System.IntPtr)
extern void Detail_easyar_ObjectTarget__typeName_mEBBEE04D4886FB8C2612D31B9C924150975A8FF9 (void);
// 0x000000F2 System.Void easyar.Detail::easyar_castObjectTargetToTarget(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castObjectTargetToTarget_m8FF61E65D8F79C363BCD438E12C0D702F1E42ADF (void);
// 0x000000F3 System.Void easyar.Detail::easyar_tryCastTargetToObjectTarget(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastTargetToObjectTarget_m5B7770E0A6BD5AFF24FD45577EBCF113DB792039 (void);
// 0x000000F4 System.Void easyar.Detail::easyar_ObjectTrackerResult_targetInstances(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTrackerResult_targetInstances_m8F8F0B23D489B9B33EB43457983BC56910991991 (void);
// 0x000000F5 System.Void easyar.Detail::easyar_ObjectTrackerResult_setTargetInstances(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ObjectTrackerResult_setTargetInstances_m8C761F266081908892FD9260FD97AF90B6CC4DA1 (void);
// 0x000000F6 System.Void easyar.Detail::easyar_ObjectTrackerResult__dtor(System.IntPtr)
extern void Detail_easyar_ObjectTrackerResult__dtor_m3D400E997315FAF2A6C5CEECF6A3A650A329C472 (void);
// 0x000000F7 System.Void easyar.Detail::easyar_ObjectTrackerResult__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTrackerResult__retain_m871040417045170E03972F49F22F62A6BBC67297 (void);
// 0x000000F8 System.IntPtr easyar.Detail::easyar_ObjectTrackerResult__typeName(System.IntPtr)
extern void Detail_easyar_ObjectTrackerResult__typeName_m6432181585669CF6F6C34213F3D537B982A049FA (void);
// 0x000000F9 System.Void easyar.Detail::easyar_castObjectTrackerResultToFrameFilterResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castObjectTrackerResultToFrameFilterResult_m1C3BFC7F8EBEB1DE6DD156CB3556FE6B8C82CD8C (void);
// 0x000000FA System.Void easyar.Detail::easyar_tryCastFrameFilterResultToObjectTrackerResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastFrameFilterResultToObjectTrackerResult_m436EB30B1F1C620F8BEAEE5377290D56A01D28AF (void);
// 0x000000FB System.Void easyar.Detail::easyar_castObjectTrackerResultToTargetTrackerResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castObjectTrackerResultToTargetTrackerResult_m0C350F7F99536F8A36FEDF200C55D66D0CBFDB58 (void);
// 0x000000FC System.Void easyar.Detail::easyar_tryCastTargetTrackerResultToObjectTrackerResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastTargetTrackerResultToObjectTrackerResult_m1551343F131EF56787B3A79A25C2F8DE08DF53B8 (void);
// 0x000000FD System.Boolean easyar.Detail::easyar_ObjectTracker_isAvailable()
extern void Detail_easyar_ObjectTracker_isAvailable_m1E3C403BE9935EA51C818555DD28A19DD3BE5FC6 (void);
// 0x000000FE System.Void easyar.Detail::easyar_ObjectTracker_feedbackFrameSink(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTracker_feedbackFrameSink_mCEC4367BD2973A9FB803539EB989FE294817292B (void);
// 0x000000FF System.Int32 easyar.Detail::easyar_ObjectTracker_bufferRequirement(System.IntPtr)
extern void Detail_easyar_ObjectTracker_bufferRequirement_mC9316E49B4E5F5CEC579BD77B9A20AB8D7B3C04C (void);
// 0x00000100 System.Void easyar.Detail::easyar_ObjectTracker_outputFrameSource(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTracker_outputFrameSource_m62BDAA2EE46D43F570C9FCDBD1E2B35ED2A49544 (void);
// 0x00000101 System.Void easyar.Detail::easyar_ObjectTracker_create(System.IntPtr&)
extern void Detail_easyar_ObjectTracker_create_mC42354BC3FA2A959BC320DD203F89B5FE6903B38 (void);
// 0x00000102 System.Boolean easyar.Detail::easyar_ObjectTracker_start(System.IntPtr)
extern void Detail_easyar_ObjectTracker_start_m39CFED7004F8A9AF7A4CDA674180C6C46B18D52A (void);
// 0x00000103 System.Void easyar.Detail::easyar_ObjectTracker_stop(System.IntPtr)
extern void Detail_easyar_ObjectTracker_stop_mC37B66991A81F581622F634C5633C1E678A5206F (void);
// 0x00000104 System.Void easyar.Detail::easyar_ObjectTracker_close(System.IntPtr)
extern void Detail_easyar_ObjectTracker_close_mCF7E201075A213E8521D1A6C3B85F360BF4EA90C (void);
// 0x00000105 System.Void easyar.Detail::easyar_ObjectTracker_loadTarget(System.IntPtr,System.IntPtr,System.IntPtr,easyar.Detail/FunctorOfVoidFromTargetAndBool)
extern void Detail_easyar_ObjectTracker_loadTarget_mC74F26A7098B27B752B383481877FE646ABBF462 (void);
// 0x00000106 System.Void easyar.Detail::easyar_ObjectTracker_unloadTarget(System.IntPtr,System.IntPtr,System.IntPtr,easyar.Detail/FunctorOfVoidFromTargetAndBool)
extern void Detail_easyar_ObjectTracker_unloadTarget_m0BA497EE6C241FB007D56723C983EC393D30DA95 (void);
// 0x00000107 System.Void easyar.Detail::easyar_ObjectTracker_targets(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTracker_targets_m391902AAD98CF580B37B371D6ACBBC8ED370BDD1 (void);
// 0x00000108 System.Boolean easyar.Detail::easyar_ObjectTracker_setSimultaneousNum(System.IntPtr,System.Int32)
extern void Detail_easyar_ObjectTracker_setSimultaneousNum_mCA264512CCC0248D3AF26BF911126975DCF24D64 (void);
// 0x00000109 System.Int32 easyar.Detail::easyar_ObjectTracker_simultaneousNum(System.IntPtr)
extern void Detail_easyar_ObjectTracker_simultaneousNum_m7197D4C5E9CFE0F59865EE8B43E5C450C4025997 (void);
// 0x0000010A System.Void easyar.Detail::easyar_ObjectTracker__dtor(System.IntPtr)
extern void Detail_easyar_ObjectTracker__dtor_m75F8E305F1EAA92CAE90E5F686153808DAA6EF6E (void);
// 0x0000010B System.Void easyar.Detail::easyar_ObjectTracker__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTracker__retain_m3782C67AEECDD44345451C8B341C9072ADDB84DF (void);
// 0x0000010C System.IntPtr easyar.Detail::easyar_ObjectTracker__typeName(System.IntPtr)
extern void Detail_easyar_ObjectTracker__typeName_m9F8CD32CDC1DE76B5DE9887DB1A418B23C4D405C (void);
// 0x0000010D System.Void easyar.Detail::easyar_CalibrationDownloader__ctor(System.IntPtr&)
extern void Detail_easyar_CalibrationDownloader__ctor_m5653A935C9FEA7A8A95FAC89D17D398178A17590 (void);
// 0x0000010E System.Void easyar.Detail::easyar_CalibrationDownloader_download(System.IntPtr,System.IntPtr,easyar.Detail/FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString)
extern void Detail_easyar_CalibrationDownloader_download_mA2C9E0C64A35D16752A73C6BA8F6C2B4B230ADBB (void);
// 0x0000010F System.Void easyar.Detail::easyar_CalibrationDownloader__dtor(System.IntPtr)
extern void Detail_easyar_CalibrationDownloader__dtor_m8F40F3EB22D93CC867E2426E792D9F3C8D7C3F01 (void);
// 0x00000110 System.Void easyar.Detail::easyar_CalibrationDownloader__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_CalibrationDownloader__retain_m8FB19D3C25874C3C96F35B259C494207D4CCB6C2 (void);
// 0x00000111 System.IntPtr easyar.Detail::easyar_CalibrationDownloader__typeName(System.IntPtr)
extern void Detail_easyar_CalibrationDownloader__typeName_m4E4D8F4FE9B3DC49D9B7211C9DFEB6EF27E487B1 (void);
// 0x00000112 easyar.CloudRecognizationStatus easyar.Detail::easyar_CloudRecognizationResult_getStatus(System.IntPtr)
extern void Detail_easyar_CloudRecognizationResult_getStatus_mA0CBB446D4AE77F95BB4F55A94960556B3E80D63 (void);
// 0x00000113 System.Void easyar.Detail::easyar_CloudRecognizationResult_getTarget(System.IntPtr,easyar.Detail/OptionalOfImageTarget&)
extern void Detail_easyar_CloudRecognizationResult_getTarget_m96AD1AD13C9D87B517CEC285A031D6F6245D264B (void);
// 0x00000114 System.Void easyar.Detail::easyar_CloudRecognizationResult_getUnknownErrorMessage(System.IntPtr,easyar.Detail/OptionalOfString&)
extern void Detail_easyar_CloudRecognizationResult_getUnknownErrorMessage_m6340BC91259268F35627F5FF5DDEBD691EE0B229 (void);
// 0x00000115 System.Void easyar.Detail::easyar_CloudRecognizationResult__dtor(System.IntPtr)
extern void Detail_easyar_CloudRecognizationResult__dtor_mE0D3D9C33F87C8B14C22E282ADC056EB8B2F750D (void);
// 0x00000116 System.Void easyar.Detail::easyar_CloudRecognizationResult__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_CloudRecognizationResult__retain_mFE4630327ED2EFAA9806DAE1019C71C96916E891 (void);
// 0x00000117 System.IntPtr easyar.Detail::easyar_CloudRecognizationResult__typeName(System.IntPtr)
extern void Detail_easyar_CloudRecognizationResult__typeName_m0ABD21542E0F97A741116D500C821978092BC9D2 (void);
// 0x00000118 System.Boolean easyar.Detail::easyar_CloudRecognizer_isAvailable()
extern void Detail_easyar_CloudRecognizer_isAvailable_mC76EE2FEE6C102B7E277B89899DBAF35E28D684B (void);
// 0x00000119 System.Void easyar.Detail::easyar_CloudRecognizer_create(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_CloudRecognizer_create_m922E0ABC6961AE8893661BFDB14965112045AD17 (void);
// 0x0000011A System.Void easyar.Detail::easyar_CloudRecognizer_createByCloudSecret(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_CloudRecognizer_createByCloudSecret_m15002FB5AFC5010A913C5A6A1D948923FB0377D0 (void);
// 0x0000011B System.Void easyar.Detail::easyar_CloudRecognizer_resolve(System.IntPtr,System.IntPtr,System.IntPtr,easyar.Detail/FunctorOfVoidFromCloudRecognizationResult)
extern void Detail_easyar_CloudRecognizer_resolve_mD60F910D2A1BE5DD6966EFC87C9E4A9B10877AA4 (void);
// 0x0000011C System.Void easyar.Detail::easyar_CloudRecognizer_close(System.IntPtr)
extern void Detail_easyar_CloudRecognizer_close_m13330A20A7CB643D3921225299FB255C42D8E7AC (void);
// 0x0000011D System.Void easyar.Detail::easyar_CloudRecognizer__dtor(System.IntPtr)
extern void Detail_easyar_CloudRecognizer__dtor_m1E9F71693E60211683D4AE6FA33ADB3DF837D73E (void);
// 0x0000011E System.Void easyar.Detail::easyar_CloudRecognizer__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_CloudRecognizer__retain_m6B3C33C272A25093D0F8FBB586DE31AE5EEE44B9 (void);
// 0x0000011F System.IntPtr easyar.Detail::easyar_CloudRecognizer__typeName(System.IntPtr)
extern void Detail_easyar_CloudRecognizer__typeName_mD4EE220D5D4170E93964BA1C253F57BF26B4DDFF (void);
// 0x00000120 System.Void easyar.Detail::easyar_Buffer_wrap(System.IntPtr,System.Int32,easyar.Detail/FunctorOfVoid,System.IntPtr&)
extern void Detail_easyar_Buffer_wrap_m03192D7725A4F933C8E9594CCEF280F03F96043A (void);
// 0x00000121 System.Void easyar.Detail::easyar_Buffer_create(System.Int32,System.IntPtr&)
extern void Detail_easyar_Buffer_create_mA0EC37C69901901623726466D0BD9938A6047E70 (void);
// 0x00000122 System.IntPtr easyar.Detail::easyar_Buffer_data(System.IntPtr)
extern void Detail_easyar_Buffer_data_m4A521871FF7E44394845F01D265D7D32175AFA33 (void);
// 0x00000123 System.Int32 easyar.Detail::easyar_Buffer_size(System.IntPtr)
extern void Detail_easyar_Buffer_size_mA46A2EF329426A444A825206697FC2E984AAC3C4 (void);
// 0x00000124 System.Void easyar.Detail::easyar_Buffer_memoryCopy(System.IntPtr,System.IntPtr,System.Int32)
extern void Detail_easyar_Buffer_memoryCopy_m7D60428F5761D0FFB8BBDABAB0098C69C352BDFF (void);
// 0x00000125 System.Boolean easyar.Detail::easyar_Buffer_tryCopyFrom(System.IntPtr,System.IntPtr,System.Int32,System.Int32,System.Int32)
extern void Detail_easyar_Buffer_tryCopyFrom_m01DABB0E2A53906DC6C51C583727B5DFBB1CC1EA (void);
// 0x00000126 System.Boolean easyar.Detail::easyar_Buffer_tryCopyTo(System.IntPtr,System.Int32,System.IntPtr,System.Int32,System.Int32)
extern void Detail_easyar_Buffer_tryCopyTo_mCFCA78D42FE02C740B48AF275BD9A0C0571B605D (void);
// 0x00000127 System.Void easyar.Detail::easyar_Buffer_partition(System.IntPtr,System.Int32,System.Int32,System.IntPtr&)
extern void Detail_easyar_Buffer_partition_m73B23B19B4264F1EDDE1957DA691A07337F48619 (void);
// 0x00000128 System.Void easyar.Detail::easyar_Buffer__dtor(System.IntPtr)
extern void Detail_easyar_Buffer__dtor_m7D775BC77DC4D413AF93AB0D3AC9372733A54D23 (void);
// 0x00000129 System.Void easyar.Detail::easyar_Buffer__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_Buffer__retain_mBCCFAFC1EBDDAE734CDC1E0FAB247DFFCE9D7350 (void);
// 0x0000012A System.IntPtr easyar.Detail::easyar_Buffer__typeName(System.IntPtr)
extern void Detail_easyar_Buffer__typeName_mE58A1E5A96223257916EEE5542EB539543DDE073 (void);
// 0x0000012B System.Void easyar.Detail::easyar_BufferDictionary__ctor(System.IntPtr&)
extern void Detail_easyar_BufferDictionary__ctor_m76333B59F69D1621B74A6DF7D2D721EAB9D6B5F1 (void);
// 0x0000012C System.Int32 easyar.Detail::easyar_BufferDictionary_count(System.IntPtr)
extern void Detail_easyar_BufferDictionary_count_m100A7DE259532BD8FE29070E3E666ACC903966C7 (void);
// 0x0000012D System.Boolean easyar.Detail::easyar_BufferDictionary_contains(System.IntPtr,System.IntPtr)
extern void Detail_easyar_BufferDictionary_contains_m00C0D752D50D27180AA976364FFD52574B6DFFF4 (void);
// 0x0000012E System.Void easyar.Detail::easyar_BufferDictionary_tryGet(System.IntPtr,System.IntPtr,easyar.Detail/OptionalOfBuffer&)
extern void Detail_easyar_BufferDictionary_tryGet_mD9621CFE66A384EADCBBB9728AAE4113C26017B6 (void);
// 0x0000012F System.Void easyar.Detail::easyar_BufferDictionary_set(System.IntPtr,System.IntPtr,System.IntPtr)
extern void Detail_easyar_BufferDictionary_set_mE0D4CADCCABD4AEBFB5427489321284B74514CB9 (void);
// 0x00000130 System.Boolean easyar.Detail::easyar_BufferDictionary_remove(System.IntPtr,System.IntPtr)
extern void Detail_easyar_BufferDictionary_remove_m3AD4EDDEA3F3F433F4F426DD32442788433F5728 (void);
// 0x00000131 System.Void easyar.Detail::easyar_BufferDictionary_clear(System.IntPtr)
extern void Detail_easyar_BufferDictionary_clear_mA4F8DF88321B5B77EE6FEE71FD73E8F0734963F5 (void);
// 0x00000132 System.Void easyar.Detail::easyar_BufferDictionary__dtor(System.IntPtr)
extern void Detail_easyar_BufferDictionary__dtor_m197AC42D329F36167C679A6A1BC3E58E20A8E2FD (void);
// 0x00000133 System.Void easyar.Detail::easyar_BufferDictionary__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_BufferDictionary__retain_m868AB31532852DCC7E2BBF3EACB793F361E4848E (void);
// 0x00000134 System.IntPtr easyar.Detail::easyar_BufferDictionary__typeName(System.IntPtr)
extern void Detail_easyar_BufferDictionary__typeName_mE10EFBAAA048062AE980ACE6CF1F870BA5B27902 (void);
// 0x00000135 System.Void easyar.Detail::easyar_BufferPool__ctor(System.Int32,System.Int32,System.IntPtr&)
extern void Detail_easyar_BufferPool__ctor_mBAD8F4AF31F24BA677A3F5E807F988ABF4C625FF (void);
// 0x00000136 System.Int32 easyar.Detail::easyar_BufferPool_block_size(System.IntPtr)
extern void Detail_easyar_BufferPool_block_size_m6CD55C63BEFB20B6399B21343B455A38B3E02884 (void);
// 0x00000137 System.Int32 easyar.Detail::easyar_BufferPool_capacity(System.IntPtr)
extern void Detail_easyar_BufferPool_capacity_mD2F803DCBEDACD30CA57969DFBBBDCB988235101 (void);
// 0x00000138 System.Int32 easyar.Detail::easyar_BufferPool_size(System.IntPtr)
extern void Detail_easyar_BufferPool_size_m1FE3A5C417590CDE94C8525A84170921A94049C1 (void);
// 0x00000139 System.Void easyar.Detail::easyar_BufferPool_tryAcquire(System.IntPtr,easyar.Detail/OptionalOfBuffer&)
extern void Detail_easyar_BufferPool_tryAcquire_m6165F642E008317A90C412FA04B51FE72E656E37 (void);
// 0x0000013A System.Void easyar.Detail::easyar_BufferPool__dtor(System.IntPtr)
extern void Detail_easyar_BufferPool__dtor_mE2344B8EC0FD2FEC666B499843993F2006177509 (void);
// 0x0000013B System.Void easyar.Detail::easyar_BufferPool__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_BufferPool__retain_mBC255825EC10D247F276F9815E6EA4B8599234B6 (void);
// 0x0000013C System.IntPtr easyar.Detail::easyar_BufferPool__typeName(System.IntPtr)
extern void Detail_easyar_BufferPool__typeName_m7509C755A1553F321D9CD8AC6F0D5C110FC7F5DF (void);
// 0x0000013D System.Void easyar.Detail::easyar_CameraParameters__ctor(easyar.Vec2I,easyar.Vec2F,easyar.Vec2F,easyar.CameraDeviceType,System.Int32,System.IntPtr&)
extern void Detail_easyar_CameraParameters__ctor_mAA215A450F4B77054881B0DAACA03D19A8B126E1 (void);
// 0x0000013E easyar.Vec2I easyar.Detail::easyar_CameraParameters_size(System.IntPtr)
extern void Detail_easyar_CameraParameters_size_m7A534D7E1A49FCFB8491958C1F72724AC41A4A66 (void);
// 0x0000013F easyar.Vec2F easyar.Detail::easyar_CameraParameters_focalLength(System.IntPtr)
extern void Detail_easyar_CameraParameters_focalLength_m01C45DBB4D545AB63EFD2652BF0FC25A10C70198 (void);
// 0x00000140 easyar.Vec2F easyar.Detail::easyar_CameraParameters_principalPoint(System.IntPtr)
extern void Detail_easyar_CameraParameters_principalPoint_m82E6599234ADDDF64BBCDE27F5AB832E640440C8 (void);
// 0x00000141 easyar.CameraDeviceType easyar.Detail::easyar_CameraParameters_cameraDeviceType(System.IntPtr)
extern void Detail_easyar_CameraParameters_cameraDeviceType_m60FF26D9F0242785DB82081357FC73AD0CD27C33 (void);
// 0x00000142 System.Int32 easyar.Detail::easyar_CameraParameters_cameraOrientation(System.IntPtr)
extern void Detail_easyar_CameraParameters_cameraOrientation_mAAE737042FB7C3AABD05172ED5A8A79A104E5D1C (void);
// 0x00000143 System.Void easyar.Detail::easyar_CameraParameters_createWithDefaultIntrinsics(easyar.Vec2I,easyar.CameraDeviceType,System.Int32,System.IntPtr&)
extern void Detail_easyar_CameraParameters_createWithDefaultIntrinsics_m40AD6FD2340AAB6B383B14E7BC48D203AA208A4D (void);
// 0x00000144 System.Void easyar.Detail::easyar_CameraParameters_getResized(System.IntPtr,easyar.Vec2I,System.IntPtr&)
extern void Detail_easyar_CameraParameters_getResized_mA56516800D9AB0C95C154869392A1D94A69EF4D4 (void);
// 0x00000145 System.Int32 easyar.Detail::easyar_CameraParameters_imageOrientation(System.IntPtr,System.Int32)
extern void Detail_easyar_CameraParameters_imageOrientation_m3FDB4F8C3E30165F41F1DB4EDA397060B163E6C7 (void);
// 0x00000146 System.Boolean easyar.Detail::easyar_CameraParameters_imageHorizontalFlip(System.IntPtr,System.Boolean)
extern void Detail_easyar_CameraParameters_imageHorizontalFlip_m879D20F6F9036BA2FDAA449F53DAFE1385160C5B (void);
// 0x00000147 easyar.Matrix44F easyar.Detail::easyar_CameraParameters_projection(System.IntPtr,System.Single,System.Single,System.Single,System.Int32,System.Boolean,System.Boolean)
extern void Detail_easyar_CameraParameters_projection_m046E0733F146345E09B4A0A451F3F5436DABBFA9 (void);
// 0x00000148 easyar.Matrix44F easyar.Detail::easyar_CameraParameters_imageProjection(System.IntPtr,System.Single,System.Int32,System.Boolean,System.Boolean)
extern void Detail_easyar_CameraParameters_imageProjection_m2CD5CB900916AE81CCEF3031F331B70FBAF9F7CC (void);
// 0x00000149 easyar.Vec2F easyar.Detail::easyar_CameraParameters_screenCoordinatesFromImageCoordinates(System.IntPtr,System.Single,System.Int32,System.Boolean,System.Boolean,easyar.Vec2F)
extern void Detail_easyar_CameraParameters_screenCoordinatesFromImageCoordinates_mB2F49481601C3B9D9A443D6F84AAEB8DA285F39E (void);
// 0x0000014A easyar.Vec2F easyar.Detail::easyar_CameraParameters_imageCoordinatesFromScreenCoordinates(System.IntPtr,System.Single,System.Int32,System.Boolean,System.Boolean,easyar.Vec2F)
extern void Detail_easyar_CameraParameters_imageCoordinatesFromScreenCoordinates_m52004AE3E22B85CD437640501415DD8B877CD549 (void);
// 0x0000014B System.Boolean easyar.Detail::easyar_CameraParameters_equalsTo(System.IntPtr,System.IntPtr)
extern void Detail_easyar_CameraParameters_equalsTo_m8494B08EDC74086C83F72C7737BCDF12FA6724D9 (void);
// 0x0000014C System.Void easyar.Detail::easyar_CameraParameters__dtor(System.IntPtr)
extern void Detail_easyar_CameraParameters__dtor_mC5C64EE4AFC57ED49946AB3AEF42D6F1955BFE39 (void);
// 0x0000014D System.Void easyar.Detail::easyar_CameraParameters__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_CameraParameters__retain_mBD8E6C8FC84ECB81491554FB7FA1140DBA7207E9 (void);
// 0x0000014E System.IntPtr easyar.Detail::easyar_CameraParameters__typeName(System.IntPtr)
extern void Detail_easyar_CameraParameters__typeName_m0A9B931AA2A9651D42CCDD6715C84A835D84EF48 (void);
// 0x0000014F System.Void easyar.Detail::easyar_Image__ctor(System.IntPtr,easyar.PixelFormat,System.Int32,System.Int32,System.IntPtr&)
extern void Detail_easyar_Image__ctor_m44A471BE21CE2248820B2B20C0EBBECA50FE4997 (void);
// 0x00000150 System.Void easyar.Detail::easyar_Image_buffer(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_Image_buffer_m97EC18D4DFB7DF285534889A34BF774ED98CA3A4 (void);
// 0x00000151 easyar.PixelFormat easyar.Detail::easyar_Image_format(System.IntPtr)
extern void Detail_easyar_Image_format_m2D7C24910CC6D6691165389D0B43B165F5B1FC15 (void);
// 0x00000152 System.Int32 easyar.Detail::easyar_Image_width(System.IntPtr)
extern void Detail_easyar_Image_width_m45359AFEB1DF248FB74D9E263BCF8418F7D54118 (void);
// 0x00000153 System.Int32 easyar.Detail::easyar_Image_height(System.IntPtr)
extern void Detail_easyar_Image_height_m5B065D782DCF8E48ED1CD366B4E7378B90232D26 (void);
// 0x00000154 System.Void easyar.Detail::easyar_Image__dtor(System.IntPtr)
extern void Detail_easyar_Image__dtor_m88CA1CF1706F83BFC0057F118A23C88D6AFD58A4 (void);
// 0x00000155 System.Void easyar.Detail::easyar_Image__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_Image__retain_m7C0AB58E963C7E993DD48EEBAD78D3AAB27FEDC0 (void);
// 0x00000156 System.IntPtr easyar.Detail::easyar_Image__typeName(System.IntPtr)
extern void Detail_easyar_Image__typeName_m1A1A436ACAB1DEC5E45E6D981E00E42EEF8882DE (void);
// 0x00000157 System.Boolean easyar.Detail::easyar_DenseSpatialMap_isAvailable()
extern void Detail_easyar_DenseSpatialMap_isAvailable_mAABE73FA136DCEA3EBBEBABC8DE51DB878D64727 (void);
// 0x00000158 System.Void easyar.Detail::easyar_DenseSpatialMap_inputFrameSink(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_DenseSpatialMap_inputFrameSink_mD5564EE366F474301A0C21BA35DE2608B54F113A (void);
// 0x00000159 System.Int32 easyar.Detail::easyar_DenseSpatialMap_bufferRequirement(System.IntPtr)
extern void Detail_easyar_DenseSpatialMap_bufferRequirement_m1441A26D30934DD772A37776B499B4F87690709C (void);
// 0x0000015A System.Void easyar.Detail::easyar_DenseSpatialMap_create(System.IntPtr&)
extern void Detail_easyar_DenseSpatialMap_create_m7C71437D6B834C9DC752F5980460010BBC927FBE (void);
// 0x0000015B System.Boolean easyar.Detail::easyar_DenseSpatialMap_start(System.IntPtr)
extern void Detail_easyar_DenseSpatialMap_start_mC6B54765AB2E19FC31A9289D3694122F0285A29D (void);
// 0x0000015C System.Void easyar.Detail::easyar_DenseSpatialMap_stop(System.IntPtr)
extern void Detail_easyar_DenseSpatialMap_stop_m11E9E6D55028812A5091261386693761B875467A (void);
// 0x0000015D System.Void easyar.Detail::easyar_DenseSpatialMap_close(System.IntPtr)
extern void Detail_easyar_DenseSpatialMap_close_mB81A7395F1C8EFB1707375C05B37D17632F27C59 (void);
// 0x0000015E System.Void easyar.Detail::easyar_DenseSpatialMap_getMesh(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_DenseSpatialMap_getMesh_mBEDAA0D457D9AD2723D42FCC7BECE3B0BE701BF0 (void);
// 0x0000015F System.Boolean easyar.Detail::easyar_DenseSpatialMap_updateSceneMesh(System.IntPtr,System.Boolean)
extern void Detail_easyar_DenseSpatialMap_updateSceneMesh_m6191CD770946FD9275F3160429F376D06558677F (void);
// 0x00000160 System.Void easyar.Detail::easyar_DenseSpatialMap__dtor(System.IntPtr)
extern void Detail_easyar_DenseSpatialMap__dtor_mA76C8E31C78960C65135230A18C2BB89CD9F03A9 (void);
// 0x00000161 System.Void easyar.Detail::easyar_DenseSpatialMap__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_DenseSpatialMap__retain_mED37119E5C69B6172D2757E7B0CEE58E6571306B (void);
// 0x00000162 System.IntPtr easyar.Detail::easyar_DenseSpatialMap__typeName(System.IntPtr)
extern void Detail_easyar_DenseSpatialMap__typeName_mE2C64E053664FFBBD425FAC93E675922CB8A6D1C (void);
// 0x00000163 System.Int32 easyar.Detail::easyar_SceneMesh_getNumOfVertexAll(System.IntPtr)
extern void Detail_easyar_SceneMesh_getNumOfVertexAll_m7146D06DE77DDC6B0BFBAE578407AA060B3E02A3 (void);
// 0x00000164 System.Int32 easyar.Detail::easyar_SceneMesh_getNumOfIndexAll(System.IntPtr)
extern void Detail_easyar_SceneMesh_getNumOfIndexAll_m9D6F8F84E7C6809D1CAC11A342F6F2057B9880BF (void);
// 0x00000165 System.Void easyar.Detail::easyar_SceneMesh_getVerticesAll(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SceneMesh_getVerticesAll_m96D1BB0BA1B8DABBE84569AEB2774D43E66CFF7A (void);
// 0x00000166 System.Void easyar.Detail::easyar_SceneMesh_getNormalsAll(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SceneMesh_getNormalsAll_m1F185562AF0769074DBB586AA69C45A448AA4F7E (void);
// 0x00000167 System.Void easyar.Detail::easyar_SceneMesh_getIndicesAll(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SceneMesh_getIndicesAll_mCEE0580B9064316E28BAC1A1D91E78FB213345AF (void);
// 0x00000168 System.Int32 easyar.Detail::easyar_SceneMesh_getNumOfVertexIncremental(System.IntPtr)
extern void Detail_easyar_SceneMesh_getNumOfVertexIncremental_m3DAC2EFBD191DB08C49F028C8E9155C438F82CC0 (void);
// 0x00000169 System.Int32 easyar.Detail::easyar_SceneMesh_getNumOfIndexIncremental(System.IntPtr)
extern void Detail_easyar_SceneMesh_getNumOfIndexIncremental_mFCE7C74CD9E206D767055C916EFCBD330D77FEDE (void);
// 0x0000016A System.Void easyar.Detail::easyar_SceneMesh_getVerticesIncremental(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SceneMesh_getVerticesIncremental_mC18389F6F4702A20AF583F91AD8CF766BE64EB8B (void);
// 0x0000016B System.Void easyar.Detail::easyar_SceneMesh_getNormalsIncremental(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SceneMesh_getNormalsIncremental_mB15201BFE2D01D7C4489F2C4A27E0A543CE8D9C1 (void);
// 0x0000016C System.Void easyar.Detail::easyar_SceneMesh_getIndicesIncremental(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SceneMesh_getIndicesIncremental_m56ABECD0C7D5277BFFBD75DB9E66AC6FA1B013CD (void);
// 0x0000016D System.Void easyar.Detail::easyar_SceneMesh_getBlocksInfoIncremental(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SceneMesh_getBlocksInfoIncremental_m201C45354BE5D6E3CEC5E75F5E31671C6BD720B0 (void);
// 0x0000016E System.Single easyar.Detail::easyar_SceneMesh_getBlockDimensionInMeters(System.IntPtr)
extern void Detail_easyar_SceneMesh_getBlockDimensionInMeters_mA2781961B708806E3B60B99B24A461CA94A2EE31 (void);
// 0x0000016F System.Void easyar.Detail::easyar_SceneMesh__dtor(System.IntPtr)
extern void Detail_easyar_SceneMesh__dtor_mC025311DE470C0FD1110593EECEB01028A9D9FE0 (void);
// 0x00000170 System.Void easyar.Detail::easyar_SceneMesh__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SceneMesh__retain_m7BAAD5C1F051F018819C9570C491A633C4C03E56 (void);
// 0x00000171 System.IntPtr easyar.Detail::easyar_SceneMesh__typeName(System.IntPtr)
extern void Detail_easyar_SceneMesh__typeName_mD385DE632E36FA0BBAADDB6A7787B83F876E7C25 (void);
// 0x00000172 System.Void easyar.Detail::easyar_ARCoreCameraDevice__ctor(System.IntPtr&)
extern void Detail_easyar_ARCoreCameraDevice__ctor_mD0E25F1340C204C238EBD7CA7115EF4D2A19A859 (void);
// 0x00000173 System.Boolean easyar.Detail::easyar_ARCoreCameraDevice_isAvailable()
extern void Detail_easyar_ARCoreCameraDevice_isAvailable_m7A20554F3DD889B92D88B709547ED0D40A8B209F (void);
// 0x00000174 System.Int32 easyar.Detail::easyar_ARCoreCameraDevice_bufferCapacity(System.IntPtr)
extern void Detail_easyar_ARCoreCameraDevice_bufferCapacity_mC17684BDBC3C2CD405371597170FA7B58980CD1B (void);
// 0x00000175 System.Void easyar.Detail::easyar_ARCoreCameraDevice_setBufferCapacity(System.IntPtr,System.Int32)
extern void Detail_easyar_ARCoreCameraDevice_setBufferCapacity_mD3EA381C904F693B4654243972AD4996BAA6A778 (void);
// 0x00000176 System.Void easyar.Detail::easyar_ARCoreCameraDevice_inputFrameSource(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ARCoreCameraDevice_inputFrameSource_m4D2A49B60FA5FDE7B51E37D2F939C72FB152FFD8 (void);
// 0x00000177 System.Boolean easyar.Detail::easyar_ARCoreCameraDevice_start(System.IntPtr)
extern void Detail_easyar_ARCoreCameraDevice_start_m204A0AB38BD80B0E705FDC8596FB99074425B06E (void);
// 0x00000178 System.Void easyar.Detail::easyar_ARCoreCameraDevice_stop(System.IntPtr)
extern void Detail_easyar_ARCoreCameraDevice_stop_m5B00A1744E508D416060699BD66A43DF32F4F89D (void);
// 0x00000179 System.Void easyar.Detail::easyar_ARCoreCameraDevice_close(System.IntPtr)
extern void Detail_easyar_ARCoreCameraDevice_close_m4EE6ABB1636153CF2A1CF137A4FE2132975AF10A (void);
// 0x0000017A System.Void easyar.Detail::easyar_ARCoreCameraDevice__dtor(System.IntPtr)
extern void Detail_easyar_ARCoreCameraDevice__dtor_mF59A3D8CB30459215F7FDDE8D9E385C98B0DF20A (void);
// 0x0000017B System.Void easyar.Detail::easyar_ARCoreCameraDevice__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ARCoreCameraDevice__retain_mD63517CDDD0020B7F7822495086C6768A6F6327A (void);
// 0x0000017C System.IntPtr easyar.Detail::easyar_ARCoreCameraDevice__typeName(System.IntPtr)
extern void Detail_easyar_ARCoreCameraDevice__typeName_m36C150485151F73F3CDBD12A3189F4966165E37D (void);
// 0x0000017D System.Void easyar.Detail::easyar_ARKitCameraDevice__ctor(System.IntPtr&)
extern void Detail_easyar_ARKitCameraDevice__ctor_m86DB727BBD35A50964F030CC28A182C7DFA04BC0 (void);
// 0x0000017E System.Boolean easyar.Detail::easyar_ARKitCameraDevice_isAvailable()
extern void Detail_easyar_ARKitCameraDevice_isAvailable_mB9C209C79F87E01D9A16E5238BB3876B9E737CA9 (void);
// 0x0000017F System.Int32 easyar.Detail::easyar_ARKitCameraDevice_bufferCapacity(System.IntPtr)
extern void Detail_easyar_ARKitCameraDevice_bufferCapacity_m183DA6C9B9DBDA9A1C738B4C1293162254FEA5B8 (void);
// 0x00000180 System.Void easyar.Detail::easyar_ARKitCameraDevice_setBufferCapacity(System.IntPtr,System.Int32)
extern void Detail_easyar_ARKitCameraDevice_setBufferCapacity_m5D28F4726A39A1E2EF4DA002C82C08C884B14185 (void);
// 0x00000181 System.Void easyar.Detail::easyar_ARKitCameraDevice_inputFrameSource(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ARKitCameraDevice_inputFrameSource_m9E16719FA7CB888509E55417C971ACE74346C043 (void);
// 0x00000182 System.Boolean easyar.Detail::easyar_ARKitCameraDevice_start(System.IntPtr)
extern void Detail_easyar_ARKitCameraDevice_start_m2041B2CD32F3919AB0C1DAD5438C821C9D14FE25 (void);
// 0x00000183 System.Void easyar.Detail::easyar_ARKitCameraDevice_stop(System.IntPtr)
extern void Detail_easyar_ARKitCameraDevice_stop_m6829EC000DD6BD23E69C4593303CBAAD4613836F (void);
// 0x00000184 System.Void easyar.Detail::easyar_ARKitCameraDevice_close(System.IntPtr)
extern void Detail_easyar_ARKitCameraDevice_close_mDEDE6B8F7C9B4DFC2E481944F6C4D3D394261929 (void);
// 0x00000185 System.Void easyar.Detail::easyar_ARKitCameraDevice__dtor(System.IntPtr)
extern void Detail_easyar_ARKitCameraDevice__dtor_m9D8243E2282CC26CDE8409C926F654C30A482635 (void);
// 0x00000186 System.Void easyar.Detail::easyar_ARKitCameraDevice__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ARKitCameraDevice__retain_m99542F0C3CBA00CB4119EB9A24E0B36EEABFE80B (void);
// 0x00000187 System.IntPtr easyar.Detail::easyar_ARKitCameraDevice__typeName(System.IntPtr)
extern void Detail_easyar_ARKitCameraDevice__typeName_m870F68D8D2468DAF2382071A87CDDF5222C1DA1D (void);
// 0x00000188 System.Void easyar.Detail::easyar_CameraDevice__ctor(System.IntPtr&)
extern void Detail_easyar_CameraDevice__ctor_mB405C541CF4727B7107B6447A0DDE0ED4519D42F (void);
// 0x00000189 System.Boolean easyar.Detail::easyar_CameraDevice_isAvailable()
extern void Detail_easyar_CameraDevice_isAvailable_mA981320BD0FCAD60E0B61E9AE0F89160991F197C (void);
// 0x0000018A easyar.AndroidCameraApiType easyar.Detail::easyar_CameraDevice_androidCameraApiType(System.IntPtr)
extern void Detail_easyar_CameraDevice_androidCameraApiType_m3D69DFED8EC373B9262281D0AB3CE3D754E792C7 (void);
// 0x0000018B System.Void easyar.Detail::easyar_CameraDevice_setAndroidCameraApiType(System.IntPtr,easyar.AndroidCameraApiType)
extern void Detail_easyar_CameraDevice_setAndroidCameraApiType_m1B47F2F719EAD5FCE16F6AD1D8117DE982BC7995 (void);
// 0x0000018C System.Int32 easyar.Detail::easyar_CameraDevice_bufferCapacity(System.IntPtr)
extern void Detail_easyar_CameraDevice_bufferCapacity_m07237BC25FB91FB5967418048E5D8E1145DF231D (void);
// 0x0000018D System.Void easyar.Detail::easyar_CameraDevice_setBufferCapacity(System.IntPtr,System.Int32)
extern void Detail_easyar_CameraDevice_setBufferCapacity_m038DC508F2EE5D8ADC18903DB3FD2C545F148BB7 (void);
// 0x0000018E System.Void easyar.Detail::easyar_CameraDevice_inputFrameSource(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_CameraDevice_inputFrameSource_mFDF8E18D2290A71D0AC87647848F0E939CB0474F (void);
// 0x0000018F System.Void easyar.Detail::easyar_CameraDevice_setStateChangedCallback(System.IntPtr,System.IntPtr,easyar.Detail/OptionalOfFunctorOfVoidFromCameraState)
extern void Detail_easyar_CameraDevice_setStateChangedCallback_mFEAFB69C164BF4773F28CA25AEE348846AE37A4F (void);
// 0x00000190 System.Void easyar.Detail::easyar_CameraDevice_requestPermissions(System.IntPtr,easyar.Detail/OptionalOfFunctorOfVoidFromPermissionStatusAndString)
extern void Detail_easyar_CameraDevice_requestPermissions_m0B0085346DAC92BFB7AF8A9F85713157F91CDA21 (void);
// 0x00000191 System.Int32 easyar.Detail::easyar_CameraDevice_cameraCount()
extern void Detail_easyar_CameraDevice_cameraCount_mD57207D600584A13C3D9C1B07E75F6811D251624 (void);
// 0x00000192 System.Boolean easyar.Detail::easyar_CameraDevice_openWithIndex(System.IntPtr,System.Int32)
extern void Detail_easyar_CameraDevice_openWithIndex_m4073550678E1FCEB99D97E2397C914798B821133 (void);
// 0x00000193 System.Boolean easyar.Detail::easyar_CameraDevice_openWithSpecificType(System.IntPtr,easyar.CameraDeviceType)
extern void Detail_easyar_CameraDevice_openWithSpecificType_mF3B2320DDCAC228770AEA01BDF5478DA8CC556B5 (void);
// 0x00000194 System.Boolean easyar.Detail::easyar_CameraDevice_openWithPreferredType(System.IntPtr,easyar.CameraDeviceType)
extern void Detail_easyar_CameraDevice_openWithPreferredType_mB956E4CB4A931E84FC6ABDD69661DE54449CD948 (void);
// 0x00000195 System.Boolean easyar.Detail::easyar_CameraDevice_start(System.IntPtr)
extern void Detail_easyar_CameraDevice_start_m960EA265AF0BE266722D3BD009C14E7D8225CEF3 (void);
// 0x00000196 System.Void easyar.Detail::easyar_CameraDevice_stop(System.IntPtr)
extern void Detail_easyar_CameraDevice_stop_m366C4F59C27B36EA4396F079E9303A294010F4AA (void);
// 0x00000197 System.Void easyar.Detail::easyar_CameraDevice_close(System.IntPtr)
extern void Detail_easyar_CameraDevice_close_m9836A272E51099812771C1283AFC7B91FD7CD7C9 (void);
// 0x00000198 System.Int32 easyar.Detail::easyar_CameraDevice_index(System.IntPtr)
extern void Detail_easyar_CameraDevice_index_m8F498A0B1FAEA6C122A35F9D8240881FC1632999 (void);
// 0x00000199 easyar.CameraDeviceType easyar.Detail::easyar_CameraDevice_type(System.IntPtr)
extern void Detail_easyar_CameraDevice_type_m9E6DC493DB342DEA32EB947273FAF2C5E5475E01 (void);
// 0x0000019A System.Void easyar.Detail::easyar_CameraDevice_cameraParameters(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_CameraDevice_cameraParameters_m1387A96109B6BBFBD990DE6D6C3132A5EECBF2CD (void);
// 0x0000019B System.Void easyar.Detail::easyar_CameraDevice_setCameraParameters(System.IntPtr,System.IntPtr)
extern void Detail_easyar_CameraDevice_setCameraParameters_m96FB515B58EE378AFFC70D9A102FAA3890495A91 (void);
// 0x0000019C easyar.Vec2I easyar.Detail::easyar_CameraDevice_size(System.IntPtr)
extern void Detail_easyar_CameraDevice_size_mD134A37CAD76E1636E69FDA95CDE5E0F39B37499 (void);
// 0x0000019D System.Int32 easyar.Detail::easyar_CameraDevice_supportedSizeCount(System.IntPtr)
extern void Detail_easyar_CameraDevice_supportedSizeCount_mEDFA70CE8ADB04288E8637A45950CB4A71642CBF (void);
// 0x0000019E easyar.Vec2I easyar.Detail::easyar_CameraDevice_supportedSize(System.IntPtr,System.Int32)
extern void Detail_easyar_CameraDevice_supportedSize_m201FC7AF4AD527208CB4B601C66F97F4E8E2D679 (void);
// 0x0000019F System.Boolean easyar.Detail::easyar_CameraDevice_setSize(System.IntPtr,easyar.Vec2I)
extern void Detail_easyar_CameraDevice_setSize_mEF8C1866B2536CEB2F3CBF37BC212736760A93BE (void);
// 0x000001A0 System.Int32 easyar.Detail::easyar_CameraDevice_supportedFrameRateRangeCount(System.IntPtr)
extern void Detail_easyar_CameraDevice_supportedFrameRateRangeCount_mF01B0EB632E1FCB58EB820347A7580F952164364 (void);
// 0x000001A1 System.Single easyar.Detail::easyar_CameraDevice_supportedFrameRateRangeLower(System.IntPtr,System.Int32)
extern void Detail_easyar_CameraDevice_supportedFrameRateRangeLower_mE7B65520F6C6A078086320F1E37DD656FE227791 (void);
// 0x000001A2 System.Single easyar.Detail::easyar_CameraDevice_supportedFrameRateRangeUpper(System.IntPtr,System.Int32)
extern void Detail_easyar_CameraDevice_supportedFrameRateRangeUpper_m4DAD75C7158067F7671BCD5F60A0D0C573DC95DD (void);
// 0x000001A3 System.Int32 easyar.Detail::easyar_CameraDevice_frameRateRange(System.IntPtr)
extern void Detail_easyar_CameraDevice_frameRateRange_m51DF0304024EE5E4300BA565E93E15FA96995EF4 (void);
// 0x000001A4 System.Boolean easyar.Detail::easyar_CameraDevice_setFrameRateRange(System.IntPtr,System.Int32)
extern void Detail_easyar_CameraDevice_setFrameRateRange_m9344A9BA4713FFB9F28B15BE534D37F9C7EDC5A3 (void);
// 0x000001A5 System.Boolean easyar.Detail::easyar_CameraDevice_setFlashTorchMode(System.IntPtr,System.Boolean)
extern void Detail_easyar_CameraDevice_setFlashTorchMode_m3527FF29F6EB7DC46BBE6116F734123DEBE4D675 (void);
// 0x000001A6 System.Boolean easyar.Detail::easyar_CameraDevice_setFocusMode(System.IntPtr,easyar.CameraDeviceFocusMode)
extern void Detail_easyar_CameraDevice_setFocusMode_mFE26FB8E92420728952E476D40E413B75A571328 (void);
// 0x000001A7 System.Boolean easyar.Detail::easyar_CameraDevice_autoFocus(System.IntPtr)
extern void Detail_easyar_CameraDevice_autoFocus_m5A1A762266717912EBF4864AAFA9802F199F1839 (void);
// 0x000001A8 System.Void easyar.Detail::easyar_CameraDevice__dtor(System.IntPtr)
extern void Detail_easyar_CameraDevice__dtor_mFD2E20EFA823BCD671E9D617D03223341D8917A0 (void);
// 0x000001A9 System.Void easyar.Detail::easyar_CameraDevice__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_CameraDevice__retain_mF5CE38212B060044A5EC6DBCD98ACB7E39A9D90D (void);
// 0x000001AA System.IntPtr easyar.Detail::easyar_CameraDevice__typeName(System.IntPtr)
extern void Detail_easyar_CameraDevice__typeName_mF55756C09B1228F8E189673858CF7668CB480494 (void);
// 0x000001AB easyar.AndroidCameraApiType easyar.Detail::easyar_CameraDeviceSelector_getAndroidCameraApiType(easyar.CameraDevicePreference)
extern void Detail_easyar_CameraDeviceSelector_getAndroidCameraApiType_mA486C42BB7E399900705043EC168923BD2EDD836 (void);
// 0x000001AC System.Void easyar.Detail::easyar_CameraDeviceSelector_createCameraDevice(easyar.CameraDevicePreference,System.IntPtr&)
extern void Detail_easyar_CameraDeviceSelector_createCameraDevice_m6C66003EE767694DB312A06455FA8A8E14C3AEE2 (void);
// 0x000001AD easyar.CameraDeviceFocusMode easyar.Detail::easyar_CameraDeviceSelector_getFocusMode(easyar.CameraDevicePreference)
extern void Detail_easyar_CameraDeviceSelector_getFocusMode_mFC2F7BFB64FEE71FE7555F68039ED142E9BE433D (void);
// 0x000001AE easyar.Matrix44F easyar.Detail::easyar_SurfaceTrackerResult_transform(System.IntPtr)
extern void Detail_easyar_SurfaceTrackerResult_transform_m6D4768FCC861984CECD1F5C642406DDE22FC4680 (void);
// 0x000001AF System.Void easyar.Detail::easyar_SurfaceTrackerResult__dtor(System.IntPtr)
extern void Detail_easyar_SurfaceTrackerResult__dtor_mEE66403D4D611D169E7CD292219E485780D08200 (void);
// 0x000001B0 System.Void easyar.Detail::easyar_SurfaceTrackerResult__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SurfaceTrackerResult__retain_mB7EE5CA1F170B0B98CB0D40594E81D38D9B71EAE (void);
// 0x000001B1 System.IntPtr easyar.Detail::easyar_SurfaceTrackerResult__typeName(System.IntPtr)
extern void Detail_easyar_SurfaceTrackerResult__typeName_m58D260C01BD13BF53E190200CE9AD10AED5C99D8 (void);
// 0x000001B2 System.Void easyar.Detail::easyar_castSurfaceTrackerResultToFrameFilterResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castSurfaceTrackerResultToFrameFilterResult_mA006B3D8F339F9E51DC2B1B7C1E77F54B63AAFC7 (void);
// 0x000001B3 System.Void easyar.Detail::easyar_tryCastFrameFilterResultToSurfaceTrackerResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastFrameFilterResultToSurfaceTrackerResult_m72C70FFCA274B1792A53F4B6C47F741F3521EB2E (void);
// 0x000001B4 System.Boolean easyar.Detail::easyar_SurfaceTracker_isAvailable()
extern void Detail_easyar_SurfaceTracker_isAvailable_mBA249973FA782ACBB1ABC5CCE5C7D1AC9F032073 (void);
// 0x000001B5 System.Void easyar.Detail::easyar_SurfaceTracker_inputFrameSink(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SurfaceTracker_inputFrameSink_m67B09A786EEB1C0393C169053807E5645DC06970 (void);
// 0x000001B6 System.Int32 easyar.Detail::easyar_SurfaceTracker_bufferRequirement(System.IntPtr)
extern void Detail_easyar_SurfaceTracker_bufferRequirement_mC37903CCF5F9C80996833585F116699783BEABF6 (void);
// 0x000001B7 System.Void easyar.Detail::easyar_SurfaceTracker_outputFrameSource(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SurfaceTracker_outputFrameSource_mE18F6A531B109BC80128815B72BF402C44BCD2BE (void);
// 0x000001B8 System.Void easyar.Detail::easyar_SurfaceTracker_create(System.IntPtr&)
extern void Detail_easyar_SurfaceTracker_create_mBFF3374FD2EFAF562D40022121828F90D5F007DB (void);
// 0x000001B9 System.Boolean easyar.Detail::easyar_SurfaceTracker_start(System.IntPtr)
extern void Detail_easyar_SurfaceTracker_start_m730AE242ACDF7B8B5CDEF6827C8431AC43659475 (void);
// 0x000001BA System.Void easyar.Detail::easyar_SurfaceTracker_stop(System.IntPtr)
extern void Detail_easyar_SurfaceTracker_stop_m51E2D6D19F28986AB22610F4AAC119FF1D9BE715 (void);
// 0x000001BB System.Void easyar.Detail::easyar_SurfaceTracker_close(System.IntPtr)
extern void Detail_easyar_SurfaceTracker_close_mED0BB4FD96C4079C9C92454FAFD5DD3F1C9A0D4E (void);
// 0x000001BC System.Void easyar.Detail::easyar_SurfaceTracker_alignTargetToCameraImagePoint(System.IntPtr,easyar.Vec2F)
extern void Detail_easyar_SurfaceTracker_alignTargetToCameraImagePoint_m9A24550BD5984AA4497390F1CEAD4E4B08FA52E1 (void);
// 0x000001BD System.Void easyar.Detail::easyar_SurfaceTracker__dtor(System.IntPtr)
extern void Detail_easyar_SurfaceTracker__dtor_m982521FEB1B05367B07A569632640FE80E485799 (void);
// 0x000001BE System.Void easyar.Detail::easyar_SurfaceTracker__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SurfaceTracker__retain_m989AA65BBBE31C3FFC751F6B5040F54ADC02D34E (void);
// 0x000001BF System.IntPtr easyar.Detail::easyar_SurfaceTracker__typeName(System.IntPtr)
extern void Detail_easyar_SurfaceTracker__typeName_mC41CC41D9CE19BA9FBAF6D3661FB289680D3D303 (void);
// 0x000001C0 System.Void easyar.Detail::easyar_MotionTrackerCameraDevice__ctor(System.IntPtr&)
extern void Detail_easyar_MotionTrackerCameraDevice__ctor_m6CAC90B6251117DE20D92F11F84745C36563F31D (void);
// 0x000001C1 System.Boolean easyar.Detail::easyar_MotionTrackerCameraDevice_isAvailable()
extern void Detail_easyar_MotionTrackerCameraDevice_isAvailable_m55C63DE5CC04307E314BC84319CD6FBA797C6943 (void);
// 0x000001C2 easyar.MotionTrackerCameraDeviceQualityLevel easyar.Detail::easyar_MotionTrackerCameraDevice_getQualityLevel()
extern void Detail_easyar_MotionTrackerCameraDevice_getQualityLevel_m519ECCF3D076A17F090BF9ED2985C3157EE51E04 (void);
// 0x000001C3 System.Boolean easyar.Detail::easyar_MotionTrackerCameraDevice_setFrameRateType(System.IntPtr,easyar.MotionTrackerCameraDeviceFPS)
extern void Detail_easyar_MotionTrackerCameraDevice_setFrameRateType_mCE673716577DE0B7D8327F4E59AEA2A83A541B86 (void);
// 0x000001C4 System.Boolean easyar.Detail::easyar_MotionTrackerCameraDevice_setFocusMode(System.IntPtr,easyar.MotionTrackerCameraDeviceFocusMode)
extern void Detail_easyar_MotionTrackerCameraDevice_setFocusMode_mC68112FCEF401780EDEBC886A5D4B713C27D2988 (void);
// 0x000001C5 System.Boolean easyar.Detail::easyar_MotionTrackerCameraDevice_setFrameResolutionType(System.IntPtr,easyar.MotionTrackerCameraDeviceResolution)
extern void Detail_easyar_MotionTrackerCameraDevice_setFrameResolutionType_m96EC8E6E4F95F351D557B7F961740DE140A1676A (void);
// 0x000001C6 System.Boolean easyar.Detail::easyar_MotionTrackerCameraDevice_setTrackingMode(System.IntPtr,easyar.MotionTrackerCameraDeviceTrackingMode)
extern void Detail_easyar_MotionTrackerCameraDevice_setTrackingMode_m23DCB25ADC3BA479129AEF84EFC05C164B4AE555 (void);
// 0x000001C7 System.Void easyar.Detail::easyar_MotionTrackerCameraDevice_setBufferCapacity(System.IntPtr,System.Int32)
extern void Detail_easyar_MotionTrackerCameraDevice_setBufferCapacity_m728C01BC05C3B8A03F5599864DDC1EAC205BDF18 (void);
// 0x000001C8 System.Int32 easyar.Detail::easyar_MotionTrackerCameraDevice_bufferCapacity(System.IntPtr)
extern void Detail_easyar_MotionTrackerCameraDevice_bufferCapacity_m2D2F0AC88FA2C5B2AC19AAF4663B2D9AAB19373F (void);
// 0x000001C9 System.Void easyar.Detail::easyar_MotionTrackerCameraDevice_inputFrameSource(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_MotionTrackerCameraDevice_inputFrameSource_m9C835F6E423C0899A0FEBAE196D5DDBEE0DB5A26 (void);
// 0x000001CA System.Boolean easyar.Detail::easyar_MotionTrackerCameraDevice_start(System.IntPtr)
extern void Detail_easyar_MotionTrackerCameraDevice_start_m19C6583624F012F051B994A60C2F5A35DAF9D814 (void);
// 0x000001CB System.Void easyar.Detail::easyar_MotionTrackerCameraDevice_stop(System.IntPtr)
extern void Detail_easyar_MotionTrackerCameraDevice_stop_mDBA64624E2BB2B48D3D82CDFF58761428430DCB1 (void);
// 0x000001CC System.Void easyar.Detail::easyar_MotionTrackerCameraDevice_close(System.IntPtr)
extern void Detail_easyar_MotionTrackerCameraDevice_close_m983F8EAFFEB084FC2DEFCEC5E139A2D3E0EBCB39 (void);
// 0x000001CD System.Void easyar.Detail::easyar_MotionTrackerCameraDevice_hitTestAgainstPointCloud(System.IntPtr,easyar.Vec2F,System.IntPtr&)
extern void Detail_easyar_MotionTrackerCameraDevice_hitTestAgainstPointCloud_mADE44C2D8A6585D2332FCB7757CBCF256C7EDAA9 (void);
// 0x000001CE System.Void easyar.Detail::easyar_MotionTrackerCameraDevice_hitTestAgainstHorizontalPlane(System.IntPtr,easyar.Vec2F,System.IntPtr&)
extern void Detail_easyar_MotionTrackerCameraDevice_hitTestAgainstHorizontalPlane_m39258207EC4E55FB29DD892E0914173B596788D8 (void);
// 0x000001CF System.Void easyar.Detail::easyar_MotionTrackerCameraDevice_getLocalPointsCloud(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_MotionTrackerCameraDevice_getLocalPointsCloud_mC68EA7E86F505C042CA1ED1EA8D02DF328644685 (void);
// 0x000001D0 System.Void easyar.Detail::easyar_MotionTrackerCameraDevice__dtor(System.IntPtr)
extern void Detail_easyar_MotionTrackerCameraDevice__dtor_mC7FCCB20D8571C4305260D2A320A71DBD91772C0 (void);
// 0x000001D1 System.Void easyar.Detail::easyar_MotionTrackerCameraDevice__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_MotionTrackerCameraDevice__retain_m0FA38323C0918FB6CB7C263E2FD0FFE3FBCC37C0 (void);
// 0x000001D2 System.IntPtr easyar.Detail::easyar_MotionTrackerCameraDevice__typeName(System.IntPtr)
extern void Detail_easyar_MotionTrackerCameraDevice__typeName_m1C756B0684C70B1B5A00B68F2A54700F4F18D94A (void);
// 0x000001D3 System.Void easyar.Detail::easyar_InputFrameRecorder_input(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameRecorder_input_mBEB513D8F9314E43C1282F90A8FABD982CF7F076 (void);
// 0x000001D4 System.Int32 easyar.Detail::easyar_InputFrameRecorder_bufferRequirement(System.IntPtr)
extern void Detail_easyar_InputFrameRecorder_bufferRequirement_mA086537AD808ED3532744AECF24CC1E0FC6271AC (void);
// 0x000001D5 System.Void easyar.Detail::easyar_InputFrameRecorder_output(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameRecorder_output_mC44495B86E433BB1ECFA61A8E5B5DA604B8CA3D7 (void);
// 0x000001D6 System.Void easyar.Detail::easyar_InputFrameRecorder_create(System.IntPtr&)
extern void Detail_easyar_InputFrameRecorder_create_m686864C821DC3EC89D8A32C424EB5ECE8313C212 (void);
// 0x000001D7 System.Boolean easyar.Detail::easyar_InputFrameRecorder_start(System.IntPtr,System.IntPtr,System.Int32)
extern void Detail_easyar_InputFrameRecorder_start_m04BBFDC50C6F94806DD23031CFA15E73E00016CC (void);
// 0x000001D8 System.Void easyar.Detail::easyar_InputFrameRecorder_stop(System.IntPtr)
extern void Detail_easyar_InputFrameRecorder_stop_m028E67230F996DA136BDF8353ABFC4905DDF3970 (void);
// 0x000001D9 System.Void easyar.Detail::easyar_InputFrameRecorder__dtor(System.IntPtr)
extern void Detail_easyar_InputFrameRecorder__dtor_m7130C3D430866640ED4F23387B9B5EC34966A363 (void);
// 0x000001DA System.Void easyar.Detail::easyar_InputFrameRecorder__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameRecorder__retain_m600C7B382D3DE54821EDBE53FD2A1B266E653BF1 (void);
// 0x000001DB System.IntPtr easyar.Detail::easyar_InputFrameRecorder__typeName(System.IntPtr)
extern void Detail_easyar_InputFrameRecorder__typeName_m444B7BAE02641E4D13249CD5DD58723CDFD40A2B (void);
// 0x000001DC System.Void easyar.Detail::easyar_InputFramePlayer_output(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFramePlayer_output_mA0677C2D76C833F66489C16A558BFE8EB0095639 (void);
// 0x000001DD System.Void easyar.Detail::easyar_InputFramePlayer_create(System.IntPtr&)
extern void Detail_easyar_InputFramePlayer_create_mB67CC5EA5ECB16A8CC2791ABEA3CDE91EE01D4D5 (void);
// 0x000001DE System.Boolean easyar.Detail::easyar_InputFramePlayer_start(System.IntPtr,System.IntPtr)
extern void Detail_easyar_InputFramePlayer_start_mA67B099F2B54F4D8C00C2F28588D72A32879CB21 (void);
// 0x000001DF System.Void easyar.Detail::easyar_InputFramePlayer_stop(System.IntPtr)
extern void Detail_easyar_InputFramePlayer_stop_mF90457A2CD97CA08993F0CD3CADC2DEB94729F8B (void);
// 0x000001E0 System.Void easyar.Detail::easyar_InputFramePlayer_pause(System.IntPtr)
extern void Detail_easyar_InputFramePlayer_pause_m13D23B49180F2953E002BD04832EAB662EC3F673 (void);
// 0x000001E1 System.Boolean easyar.Detail::easyar_InputFramePlayer_resume(System.IntPtr)
extern void Detail_easyar_InputFramePlayer_resume_mDB57C847FA7BDF8602EDD49D2BC6EDB4F93B6A5B (void);
// 0x000001E2 System.Double easyar.Detail::easyar_InputFramePlayer_totalTime(System.IntPtr)
extern void Detail_easyar_InputFramePlayer_totalTime_m4435576D974E4528C367374F60A33587D57AAAFC (void);
// 0x000001E3 System.Double easyar.Detail::easyar_InputFramePlayer_currentTime(System.IntPtr)
extern void Detail_easyar_InputFramePlayer_currentTime_m1534A0F0EC4A6FA5F45E5CBE6BA52DEE9386F3C4 (void);
// 0x000001E4 System.Int32 easyar.Detail::easyar_InputFramePlayer_initalScreenRotation(System.IntPtr)
extern void Detail_easyar_InputFramePlayer_initalScreenRotation_m9486226AC2D6594392615FB44CF3A8590D55A19C (void);
// 0x000001E5 System.Boolean easyar.Detail::easyar_InputFramePlayer_isCompleted(System.IntPtr)
extern void Detail_easyar_InputFramePlayer_isCompleted_m4A73C2541210A781BA857EC88988A2D41B5407E1 (void);
// 0x000001E6 System.Void easyar.Detail::easyar_InputFramePlayer__dtor(System.IntPtr)
extern void Detail_easyar_InputFramePlayer__dtor_m1ADD1CA37CD9C05F797878CBDF32BF0BCDFE3CC3 (void);
// 0x000001E7 System.Void easyar.Detail::easyar_InputFramePlayer__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFramePlayer__retain_mEFA522C2E75E4B658001122549E2BA3DFA26780D (void);
// 0x000001E8 System.IntPtr easyar.Detail::easyar_InputFramePlayer__typeName(System.IntPtr)
extern void Detail_easyar_InputFramePlayer__typeName_m4FD18FD1009B83A0E8645A95D1062C99B98214D5 (void);
// 0x000001E9 System.Void easyar.Detail::easyar_CallbackScheduler__dtor(System.IntPtr)
extern void Detail_easyar_CallbackScheduler__dtor_m684352FC0B0167FC5B85E91CF5B4BBBA44CCA1A1 (void);
// 0x000001EA System.Void easyar.Detail::easyar_CallbackScheduler__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_CallbackScheduler__retain_m0EC514E939119FA3047090AA8545DF35547F06F8 (void);
// 0x000001EB System.IntPtr easyar.Detail::easyar_CallbackScheduler__typeName(System.IntPtr)
extern void Detail_easyar_CallbackScheduler__typeName_mBF2A950D049126E3EFD1000BB9B0ADD7E3DD4FB1 (void);
// 0x000001EC System.Void easyar.Detail::easyar_DelayedCallbackScheduler__ctor(System.IntPtr&)
extern void Detail_easyar_DelayedCallbackScheduler__ctor_mBE6B4CC0B10CA546A0DADC47C0A696CF4A3D94E1 (void);
// 0x000001ED System.Boolean easyar.Detail::easyar_DelayedCallbackScheduler_runOne(System.IntPtr)
extern void Detail_easyar_DelayedCallbackScheduler_runOne_mBCDE38C512F70FC3FB9FBC89873B4603EBDA1FC0 (void);
// 0x000001EE System.Void easyar.Detail::easyar_DelayedCallbackScheduler__dtor(System.IntPtr)
extern void Detail_easyar_DelayedCallbackScheduler__dtor_m07B96B20830AAC9882F8B8F47EC2100CF3150FA8 (void);
// 0x000001EF System.Void easyar.Detail::easyar_DelayedCallbackScheduler__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_DelayedCallbackScheduler__retain_m0B6DA117F0F5A4795BDE11E49FA98FF112067A4B (void);
// 0x000001F0 System.IntPtr easyar.Detail::easyar_DelayedCallbackScheduler__typeName(System.IntPtr)
extern void Detail_easyar_DelayedCallbackScheduler__typeName_m236DE9591BEF48F199261D9B8FB6E1CCC8572233 (void);
// 0x000001F1 System.Void easyar.Detail::easyar_castDelayedCallbackSchedulerToCallbackScheduler(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castDelayedCallbackSchedulerToCallbackScheduler_m0060229DEC0711FFD8732005E154D946D2A6DE2D (void);
// 0x000001F2 System.Void easyar.Detail::easyar_tryCastCallbackSchedulerToDelayedCallbackScheduler(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastCallbackSchedulerToDelayedCallbackScheduler_mE6AB2B617A2A07F90EFA8EDDC002CAE65CABC4AD (void);
// 0x000001F3 System.Void easyar.Detail::easyar_ImmediateCallbackScheduler_getDefault(System.IntPtr&)
extern void Detail_easyar_ImmediateCallbackScheduler_getDefault_mE6D1E97D7947162FE757124F0D479753DBF30A5F (void);
// 0x000001F4 System.Void easyar.Detail::easyar_ImmediateCallbackScheduler__dtor(System.IntPtr)
extern void Detail_easyar_ImmediateCallbackScheduler__dtor_mCCD9D47AAC9BEE9C92AE1251060B6F126588DBB6 (void);
// 0x000001F5 System.Void easyar.Detail::easyar_ImmediateCallbackScheduler__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImmediateCallbackScheduler__retain_m3F9F8F0AD545EBC2C64D7B492DB6179D92D466CD (void);
// 0x000001F6 System.IntPtr easyar.Detail::easyar_ImmediateCallbackScheduler__typeName(System.IntPtr)
extern void Detail_easyar_ImmediateCallbackScheduler__typeName_m64622B1248D85297D2EF633DBE301B32326DD3B3 (void);
// 0x000001F7 System.Void easyar.Detail::easyar_castImmediateCallbackSchedulerToCallbackScheduler(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castImmediateCallbackSchedulerToCallbackScheduler_m97C104D04CBA8660F7D844185F90F60F57786B24 (void);
// 0x000001F8 System.Void easyar.Detail::easyar_tryCastCallbackSchedulerToImmediateCallbackScheduler(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastCallbackSchedulerToImmediateCallbackScheduler_mC1C86E6289AE80F2649853564E676790EF450F5D (void);
// 0x000001F9 System.Void easyar.Detail::easyar_JniUtility_wrapByteArray(System.IntPtr,System.Boolean,easyar.Detail/FunctorOfVoid,System.IntPtr&)
extern void Detail_easyar_JniUtility_wrapByteArray_mEE60CE941623DDB8C72367874AFF01CCEF11CC55 (void);
// 0x000001FA System.Void easyar.Detail::easyar_JniUtility_wrapBuffer(System.IntPtr,easyar.Detail/FunctorOfVoid,System.IntPtr&)
extern void Detail_easyar_JniUtility_wrapBuffer_m84CF87BE545230D4334EF7B818AE9A049F5B30B8 (void);
// 0x000001FB System.IntPtr easyar.Detail::easyar_JniUtility_getDirectBufferAddress(System.IntPtr)
extern void Detail_easyar_JniUtility_getDirectBufferAddress_m93D341139E763CB641476BB55E8E7AC48F477F6C (void);
// 0x000001FC System.Void easyar.Detail::easyar_Log_setLogFunc(easyar.Detail/FunctorOfVoidFromLogLevelAndString)
extern void Detail_easyar_Log_setLogFunc_m569769815FBF7CC1F961D7E8E8D03131119D5E63 (void);
// 0x000001FD System.Void easyar.Detail::easyar_Log_setLogFuncWithScheduler(System.IntPtr,easyar.Detail/FunctorOfVoidFromLogLevelAndString)
extern void Detail_easyar_Log_setLogFuncWithScheduler_m4069E258D639C2F41D29C74A35C451BD26980FCE (void);
// 0x000001FE System.Void easyar.Detail::easyar_Log_resetLogFunc()
extern void Detail_easyar_Log_resetLogFunc_m677D66DDFADBAC6B4CCB3CAB54538C547EA2C64C (void);
// 0x000001FF System.Void easyar.Detail::easyar_Storage_setAssetDirPath(System.IntPtr)
extern void Detail_easyar_Storage_setAssetDirPath_m5B18609ABCBDE6DCCC8551CEFE33A52E9D429D79 (void);
// 0x00000200 System.Void easyar.Detail::easyar_ImageTargetParameters__ctor(System.IntPtr&)
extern void Detail_easyar_ImageTargetParameters__ctor_m0FD27677B99B1027C9C6B9CB06637C04FE9D2054 (void);
// 0x00000201 System.Void easyar.Detail::easyar_ImageTargetParameters_image(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTargetParameters_image_mFC22D31662AD505C78D620AC85E0230E56B735DF (void);
// 0x00000202 System.Void easyar.Detail::easyar_ImageTargetParameters_setImage(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ImageTargetParameters_setImage_m65C905D9310FF108FB23E34587016F532F420EDD (void);
// 0x00000203 System.Void easyar.Detail::easyar_ImageTargetParameters_name(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTargetParameters_name_mB52F62216DE6573A86B558910B2A85E1BF618DD6 (void);
// 0x00000204 System.Void easyar.Detail::easyar_ImageTargetParameters_setName(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ImageTargetParameters_setName_m6DCBA29AEC9301D3E75FF1F1D698EE82C102093D (void);
// 0x00000205 System.Void easyar.Detail::easyar_ImageTargetParameters_uid(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTargetParameters_uid_m332228BD543BD3DA50BA960135D67F9DC9A373C2 (void);
// 0x00000206 System.Void easyar.Detail::easyar_ImageTargetParameters_setUid(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ImageTargetParameters_setUid_mBB755087E4E95CDEF7A13296BF47D4B4E857F9DA (void);
// 0x00000207 System.Void easyar.Detail::easyar_ImageTargetParameters_meta(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTargetParameters_meta_mA2EAC3A3CA689D3F892F7493657E481EC1EB9B7F (void);
// 0x00000208 System.Void easyar.Detail::easyar_ImageTargetParameters_setMeta(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ImageTargetParameters_setMeta_m4C106F0F3F51175DA9B1315D9CF1750895AA9D0C (void);
// 0x00000209 System.Single easyar.Detail::easyar_ImageTargetParameters_scale(System.IntPtr)
extern void Detail_easyar_ImageTargetParameters_scale_mFCBFE00863217B61B1BC2298F3590BC616AB40A5 (void);
// 0x0000020A System.Void easyar.Detail::easyar_ImageTargetParameters_setScale(System.IntPtr,System.Single)
extern void Detail_easyar_ImageTargetParameters_setScale_m0D22E88804FA5BAB387824E894668191BCFF1EDC (void);
// 0x0000020B System.Void easyar.Detail::easyar_ImageTargetParameters__dtor(System.IntPtr)
extern void Detail_easyar_ImageTargetParameters__dtor_m3EDFAD17DAE6D2AD58DC6077E47FE9DD043A697C (void);
// 0x0000020C System.Void easyar.Detail::easyar_ImageTargetParameters__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTargetParameters__retain_m11576149B985B35F02B2F470166A7E18CED12685 (void);
// 0x0000020D System.IntPtr easyar.Detail::easyar_ImageTargetParameters__typeName(System.IntPtr)
extern void Detail_easyar_ImageTargetParameters__typeName_mB60D034F827A9EC0F5F68AF39C57C67AD0342ACC (void);
// 0x0000020E System.Void easyar.Detail::easyar_ImageTarget__ctor(System.IntPtr&)
extern void Detail_easyar_ImageTarget__ctor_m0A6D9681AC02EFF28EE0496ECE5654DAF6B54E00 (void);
// 0x0000020F System.Void easyar.Detail::easyar_ImageTarget_createFromParameters(System.IntPtr,easyar.Detail/OptionalOfImageTarget&)
extern void Detail_easyar_ImageTarget_createFromParameters_m5CB26DBCD0CF96F2FB0E65A991BFEAEF323827ED (void);
// 0x00000210 System.Void easyar.Detail::easyar_ImageTarget_createFromTargetFile(System.IntPtr,easyar.StorageType,easyar.Detail/OptionalOfImageTarget&)
extern void Detail_easyar_ImageTarget_createFromTargetFile_mD4078BCCEDC80E69BB0B19B8E1D1F2A8EB9BF3C5 (void);
// 0x00000211 System.Void easyar.Detail::easyar_ImageTarget_createFromTargetData(System.IntPtr,easyar.Detail/OptionalOfImageTarget&)
extern void Detail_easyar_ImageTarget_createFromTargetData_mFC343E6A14B73670FDF90B58876310E7BDB1C80E (void);
// 0x00000212 System.Boolean easyar.Detail::easyar_ImageTarget_save(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ImageTarget_save_m479F63B3F9A3FF9939693CF2324179444E92F8CF (void);
// 0x00000213 System.Void easyar.Detail::easyar_ImageTarget_createFromImageFile(System.IntPtr,easyar.StorageType,System.IntPtr,System.IntPtr,System.IntPtr,System.Single,easyar.Detail/OptionalOfImageTarget&)
extern void Detail_easyar_ImageTarget_createFromImageFile_mBE2239162C93CDCD6F99EFFA249A02FBA104FB44 (void);
// 0x00000214 System.Single easyar.Detail::easyar_ImageTarget_scale(System.IntPtr)
extern void Detail_easyar_ImageTarget_scale_m409DEBB5D1D16F02CC66B531F164EBF652E16BC0 (void);
// 0x00000215 System.Single easyar.Detail::easyar_ImageTarget_aspectRatio(System.IntPtr)
extern void Detail_easyar_ImageTarget_aspectRatio_mA433065636D99050C732D168F505253D12E2F4C4 (void);
// 0x00000216 System.Boolean easyar.Detail::easyar_ImageTarget_setScale(System.IntPtr,System.Single)
extern void Detail_easyar_ImageTarget_setScale_mBF3F603817936A58699F859A0882A4D40C5D7DB0 (void);
// 0x00000217 System.Void easyar.Detail::easyar_ImageTarget_images(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTarget_images_m09CE7654B246725DD07EC1F3AA92DEF50D5A612A (void);
// 0x00000218 System.Int32 easyar.Detail::easyar_ImageTarget_runtimeID(System.IntPtr)
extern void Detail_easyar_ImageTarget_runtimeID_mAB4E845C7795CCF7ED85E746CDC9065175AA1D98 (void);
// 0x00000219 System.Void easyar.Detail::easyar_ImageTarget_uid(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTarget_uid_mA8173C56ECBC44D2E93BEC608F3E3B9A56F6C480 (void);
// 0x0000021A System.Void easyar.Detail::easyar_ImageTarget_name(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTarget_name_m0C1E5D8FEECCEF4E3B0F3729E094D26614B69776 (void);
// 0x0000021B System.Void easyar.Detail::easyar_ImageTarget_setName(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ImageTarget_setName_mF8DCDCBB0684ADE2FE3FC02F8C575AE95CAAAAD8 (void);
// 0x0000021C System.Void easyar.Detail::easyar_ImageTarget_meta(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTarget_meta_m09437F1A16D9DCF26AAE3FC5456CE4B1DDFA2A39 (void);
// 0x0000021D System.Void easyar.Detail::easyar_ImageTarget_setMeta(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ImageTarget_setMeta_m89E3CA09FA56941DEC74C5E15089039F85933AF4 (void);
// 0x0000021E System.Void easyar.Detail::easyar_ImageTarget__dtor(System.IntPtr)
extern void Detail_easyar_ImageTarget__dtor_mDBA6E7F38A80CD7B65C1F45F3836BB6AC3DCBB02 (void);
// 0x0000021F System.Void easyar.Detail::easyar_ImageTarget__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTarget__retain_m46AF17AF6C17F39ACB58E24484208D5143BA6718 (void);
// 0x00000220 System.IntPtr easyar.Detail::easyar_ImageTarget__typeName(System.IntPtr)
extern void Detail_easyar_ImageTarget__typeName_m0F5B48F3598E4A49CF3D4115A837B2EFB69C8C9A (void);
// 0x00000221 System.Void easyar.Detail::easyar_castImageTargetToTarget(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castImageTargetToTarget_m7F4128D5FA318D391B3D23C3E661E0ABA6A205DF (void);
// 0x00000222 System.Void easyar.Detail::easyar_tryCastTargetToImageTarget(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastTargetToImageTarget_m3049137DA10F63138107F6CF004A0B08698CD6F0 (void);
// 0x00000223 System.Void easyar.Detail::easyar_ImageTrackerResult_targetInstances(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTrackerResult_targetInstances_m63558BB4F19A4EDD4AF92B0F87BD34934195C44A (void);
// 0x00000224 System.Void easyar.Detail::easyar_ImageTrackerResult_setTargetInstances(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ImageTrackerResult_setTargetInstances_mCB517A260A368F68E46FCBEC7925592F265FE8AA (void);
// 0x00000225 System.Void easyar.Detail::easyar_ImageTrackerResult__dtor(System.IntPtr)
extern void Detail_easyar_ImageTrackerResult__dtor_mD002F891E531F62C9D76910B6140AA85D233C011 (void);
// 0x00000226 System.Void easyar.Detail::easyar_ImageTrackerResult__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTrackerResult__retain_m6E2FEA1B58E5C73F20812DEC7901E4F2860F4C15 (void);
// 0x00000227 System.IntPtr easyar.Detail::easyar_ImageTrackerResult__typeName(System.IntPtr)
extern void Detail_easyar_ImageTrackerResult__typeName_m176C4305D7121D8F0EF63AFD372859E1B95448A3 (void);
// 0x00000228 System.Void easyar.Detail::easyar_castImageTrackerResultToFrameFilterResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castImageTrackerResultToFrameFilterResult_m334E3E97A78E3B16C329243193EB5E7D25281876 (void);
// 0x00000229 System.Void easyar.Detail::easyar_tryCastFrameFilterResultToImageTrackerResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastFrameFilterResultToImageTrackerResult_m213E1A5C01318166BC6E6170177F51F9CC95F739 (void);
// 0x0000022A System.Void easyar.Detail::easyar_castImageTrackerResultToTargetTrackerResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castImageTrackerResultToTargetTrackerResult_mA431BA21BF4A2AD5E392D9780814E76904663283 (void);
// 0x0000022B System.Void easyar.Detail::easyar_tryCastTargetTrackerResultToImageTrackerResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastTargetTrackerResultToImageTrackerResult_m1AC368B01616FAE615F782779D66484B2D58C3D7 (void);
// 0x0000022C System.Boolean easyar.Detail::easyar_ImageTracker_isAvailable()
extern void Detail_easyar_ImageTracker_isAvailable_mE80A18D690EF0A78E1EB23A14CF503273C5925A7 (void);
// 0x0000022D System.Void easyar.Detail::easyar_ImageTracker_feedbackFrameSink(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTracker_feedbackFrameSink_m63B655DDCC8B577E5B8E505E93AE05284497738E (void);
// 0x0000022E System.Int32 easyar.Detail::easyar_ImageTracker_bufferRequirement(System.IntPtr)
extern void Detail_easyar_ImageTracker_bufferRequirement_m7AFE49C1D96DC67B11A4CF7C3615C74C6BDBAABF (void);
// 0x0000022F System.Void easyar.Detail::easyar_ImageTracker_outputFrameSource(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTracker_outputFrameSource_m30B4CDABDA48DC4DC270177C6B0260F8FC7FF1EA (void);
// 0x00000230 System.Void easyar.Detail::easyar_ImageTracker_create(System.IntPtr&)
extern void Detail_easyar_ImageTracker_create_m4C3D0862BFA18DD95221488B84D3E4C7F26CA596 (void);
// 0x00000231 System.Void easyar.Detail::easyar_ImageTracker_createWithMode(easyar.ImageTrackerMode,System.IntPtr&)
extern void Detail_easyar_ImageTracker_createWithMode_mC082919C46FB3B330FF3D79817692B3945A007E9 (void);
// 0x00000232 System.Boolean easyar.Detail::easyar_ImageTracker_start(System.IntPtr)
extern void Detail_easyar_ImageTracker_start_m2D69BB4E7851277B995D4CA049703995FC3EFBF5 (void);
// 0x00000233 System.Void easyar.Detail::easyar_ImageTracker_stop(System.IntPtr)
extern void Detail_easyar_ImageTracker_stop_m460D9E4FCD73D32E978372C3C54A5F32C54405D7 (void);
// 0x00000234 System.Void easyar.Detail::easyar_ImageTracker_close(System.IntPtr)
extern void Detail_easyar_ImageTracker_close_m90E992BE9A139911D536566643F057B6B5DF1197 (void);
// 0x00000235 System.Void easyar.Detail::easyar_ImageTracker_loadTarget(System.IntPtr,System.IntPtr,System.IntPtr,easyar.Detail/FunctorOfVoidFromTargetAndBool)
extern void Detail_easyar_ImageTracker_loadTarget_m738ED9839F9F3C0A6E5C124E2712BE181014D045 (void);
// 0x00000236 System.Void easyar.Detail::easyar_ImageTracker_unloadTarget(System.IntPtr,System.IntPtr,System.IntPtr,easyar.Detail/FunctorOfVoidFromTargetAndBool)
extern void Detail_easyar_ImageTracker_unloadTarget_m8D384CEDC2BDA5B6647729D354560430A3BA70BB (void);
// 0x00000237 System.Void easyar.Detail::easyar_ImageTracker_targets(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTracker_targets_m3CB2F552B0A2223541AAE00A14745C499A5DB9A6 (void);
// 0x00000238 System.Boolean easyar.Detail::easyar_ImageTracker_setSimultaneousNum(System.IntPtr,System.Int32)
extern void Detail_easyar_ImageTracker_setSimultaneousNum_m8772322A8E75DC4ECF01A3000515A4AD7DB77742 (void);
// 0x00000239 System.Int32 easyar.Detail::easyar_ImageTracker_simultaneousNum(System.IntPtr)
extern void Detail_easyar_ImageTracker_simultaneousNum_mD5B45A136B234CDD7F9BA096E51DFECD219A423F (void);
// 0x0000023A System.Void easyar.Detail::easyar_ImageTracker__dtor(System.IntPtr)
extern void Detail_easyar_ImageTracker__dtor_mA0E4CB6DD08894DD88D0CA6AF236F15F3ED0F9AD (void);
// 0x0000023B System.Void easyar.Detail::easyar_ImageTracker__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTracker__retain_mB779C69D6AF8E6280B867877E88BCB0D71B41BF4 (void);
// 0x0000023C System.IntPtr easyar.Detail::easyar_ImageTracker__typeName(System.IntPtr)
extern void Detail_easyar_ImageTracker__typeName_m50D975B4B6295076955DE693D7F5B211C31922D8 (void);
// 0x0000023D System.Boolean easyar.Detail::easyar_Recorder_isAvailable()
extern void Detail_easyar_Recorder_isAvailable_m664DE466561E56522DDF21AD391A07AEDF79F81E (void);
// 0x0000023E System.Void easyar.Detail::easyar_Recorder_requestPermissions(System.IntPtr,easyar.Detail/OptionalOfFunctorOfVoidFromPermissionStatusAndString)
extern void Detail_easyar_Recorder_requestPermissions_mA51414AF0CB31D8DFDF0BC6C24826AA9A8B38B94 (void);
// 0x0000023F System.Void easyar.Detail::easyar_Recorder_create(System.IntPtr,System.IntPtr,easyar.Detail/OptionalOfFunctorOfVoidFromRecordStatusAndString,System.IntPtr&)
extern void Detail_easyar_Recorder_create_m9A1A191031C52AA367BB0FE4A32408953D24A336 (void);
// 0x00000240 System.Void easyar.Detail::easyar_Recorder_start(System.IntPtr)
extern void Detail_easyar_Recorder_start_m4B750E1F4DFE6739C9CC3790DF520F2007426BB0 (void);
// 0x00000241 System.Void easyar.Detail::easyar_Recorder_updateFrame(System.IntPtr,System.IntPtr,System.Int32,System.Int32)
extern void Detail_easyar_Recorder_updateFrame_m2FE475EF987E691FD62BC5D8EA5578E2B91F73E0 (void);
// 0x00000242 System.Boolean easyar.Detail::easyar_Recorder_stop(System.IntPtr)
extern void Detail_easyar_Recorder_stop_m88B9CCF0F3A833BD15CDC83DC11244CF29E3B6A8 (void);
// 0x00000243 System.Void easyar.Detail::easyar_Recorder__dtor(System.IntPtr)
extern void Detail_easyar_Recorder__dtor_m3DE91B8C335D3DDB51C1B0ACC23FB22BD0C1082D (void);
// 0x00000244 System.Void easyar.Detail::easyar_Recorder__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_Recorder__retain_mD4C9BB52F95124213CBF97CCC404E0A4F04008CE (void);
// 0x00000245 System.IntPtr easyar.Detail::easyar_Recorder__typeName(System.IntPtr)
extern void Detail_easyar_Recorder__typeName_m646AC8DF6B53BB671F43015026DC8CC1F889434F (void);
// 0x00000246 System.Void easyar.Detail::easyar_RecorderConfiguration__ctor(System.IntPtr&)
extern void Detail_easyar_RecorderConfiguration__ctor_m2D1D7531379DAEA2A65F56A86E38B1B75EFC766D (void);
// 0x00000247 System.Void easyar.Detail::easyar_RecorderConfiguration_setOutputFile(System.IntPtr,System.IntPtr)
extern void Detail_easyar_RecorderConfiguration_setOutputFile_mA522D96C033D7394EEB822C26E14A5A5F45ADF2C (void);
// 0x00000248 System.Boolean easyar.Detail::easyar_RecorderConfiguration_setProfile(System.IntPtr,easyar.RecordProfile)
extern void Detail_easyar_RecorderConfiguration_setProfile_m217D189F131577EF27A05646124F94276DDA9E2F (void);
// 0x00000249 System.Void easyar.Detail::easyar_RecorderConfiguration_setVideoSize(System.IntPtr,easyar.RecordVideoSize)
extern void Detail_easyar_RecorderConfiguration_setVideoSize_mFD2835BF0AC73927758FD9EF089D77554ED96058 (void);
// 0x0000024A System.Void easyar.Detail::easyar_RecorderConfiguration_setVideoBitrate(System.IntPtr,System.Int32)
extern void Detail_easyar_RecorderConfiguration_setVideoBitrate_m91E055692D96998FAB1E637168160817383727BB (void);
// 0x0000024B System.Void easyar.Detail::easyar_RecorderConfiguration_setChannelCount(System.IntPtr,System.Int32)
extern void Detail_easyar_RecorderConfiguration_setChannelCount_m3B80E50D176A151028299D9BC40FEFF531DCAE55 (void);
// 0x0000024C System.Void easyar.Detail::easyar_RecorderConfiguration_setAudioSampleRate(System.IntPtr,System.Int32)
extern void Detail_easyar_RecorderConfiguration_setAudioSampleRate_m4CD99E54D4133D1BD5628F8A2074FC69126B9FCB (void);
// 0x0000024D System.Void easyar.Detail::easyar_RecorderConfiguration_setAudioBitrate(System.IntPtr,System.Int32)
extern void Detail_easyar_RecorderConfiguration_setAudioBitrate_m889B95DB08E6C109D17F3EEA66589C0E24536C03 (void);
// 0x0000024E System.Void easyar.Detail::easyar_RecorderConfiguration_setVideoOrientation(System.IntPtr,easyar.RecordVideoOrientation)
extern void Detail_easyar_RecorderConfiguration_setVideoOrientation_mCE238E2C49AC7BAB4E4B18855D2F95573DAB219D (void);
// 0x0000024F System.Void easyar.Detail::easyar_RecorderConfiguration_setZoomMode(System.IntPtr,easyar.RecordZoomMode)
extern void Detail_easyar_RecorderConfiguration_setZoomMode_m7349296E56F4F14EB28A5964C57BF2D14D3F830E (void);
// 0x00000250 System.Void easyar.Detail::easyar_RecorderConfiguration__dtor(System.IntPtr)
extern void Detail_easyar_RecorderConfiguration__dtor_mA9BBE3ED4BBDF40CB61A0E1594C4674F81FE4CD7 (void);
// 0x00000251 System.Void easyar.Detail::easyar_RecorderConfiguration__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_RecorderConfiguration__retain_mC3E60F419C9C5F9F024FCB0B2ED09F477F14ACC3 (void);
// 0x00000252 System.IntPtr easyar.Detail::easyar_RecorderConfiguration__typeName(System.IntPtr)
extern void Detail_easyar_RecorderConfiguration__typeName_m09451E3F5FE2BEED1F0324F56A53DA65261A8A16 (void);
// 0x00000253 easyar.MotionTrackingStatus easyar.Detail::easyar_SparseSpatialMapResult_getMotionTrackingStatus(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapResult_getMotionTrackingStatus_m813F2076B9E681A6E9D51493592D1D4C48EFA3D3 (void);
// 0x00000254 easyar.Detail/OptionalOfMatrix44F easyar.Detail::easyar_SparseSpatialMapResult_getVioPose(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapResult_getVioPose_mDAFF6109B2FF8EF9934E0E85B1215CEC791CD181 (void);
// 0x00000255 easyar.Detail/OptionalOfMatrix44F easyar.Detail::easyar_SparseSpatialMapResult_getMapPose(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapResult_getMapPose_m6311CEF9E66020A46570452D6A30695D3AEF96AC (void);
// 0x00000256 System.Boolean easyar.Detail::easyar_SparseSpatialMapResult_getLocalizationStatus(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapResult_getLocalizationStatus_mDB4EABA1E1A0AE880401408F51903ED155ACEB2A (void);
// 0x00000257 System.Void easyar.Detail::easyar_SparseSpatialMapResult_getLocalizationMapID(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMapResult_getLocalizationMapID_m5950109E0BCF79E53B81BB5F30D8106B19F913AC (void);
// 0x00000258 System.Void easyar.Detail::easyar_SparseSpatialMapResult__dtor(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapResult__dtor_m3FB4DD378D7AAECEB06B1AB03ED64E7A1CA1C63E (void);
// 0x00000259 System.Void easyar.Detail::easyar_SparseSpatialMapResult__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMapResult__retain_m6902EB74CB83188E98FDA0900B1F65A6FADBCABA (void);
// 0x0000025A System.IntPtr easyar.Detail::easyar_SparseSpatialMapResult__typeName(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapResult__typeName_mF5394558BEDFC5935F72B0EB075C50C7DACE9DFA (void);
// 0x0000025B System.Void easyar.Detail::easyar_castSparseSpatialMapResultToFrameFilterResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castSparseSpatialMapResultToFrameFilterResult_m4C9FC0AECAD1F5E5F846D3A14913C965AFD04BF2 (void);
// 0x0000025C System.Void easyar.Detail::easyar_tryCastFrameFilterResultToSparseSpatialMapResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastFrameFilterResultToSparseSpatialMapResult_mA8B38112B54B2131A5B425E706D7A73AD97D9C15 (void);
// 0x0000025D System.Void easyar.Detail::easyar_PlaneData__ctor(System.IntPtr&)
extern void Detail_easyar_PlaneData__ctor_m325596FF63EBA7F8E7B62BA3D607B773BF285F81 (void);
// 0x0000025E easyar.PlaneType easyar.Detail::easyar_PlaneData_getType(System.IntPtr)
extern void Detail_easyar_PlaneData_getType_m3CD683FEFC482911E1067232CDBE514DCF20BA16 (void);
// 0x0000025F easyar.Matrix44F easyar.Detail::easyar_PlaneData_getPose(System.IntPtr)
extern void Detail_easyar_PlaneData_getPose_m543779FB85FD6B84822E4044943CB1707A405C1D (void);
// 0x00000260 System.Single easyar.Detail::easyar_PlaneData_getExtentX(System.IntPtr)
extern void Detail_easyar_PlaneData_getExtentX_mD053831371BACDC93864C6625954A7B65FB0788F (void);
// 0x00000261 System.Single easyar.Detail::easyar_PlaneData_getExtentZ(System.IntPtr)
extern void Detail_easyar_PlaneData_getExtentZ_m1828227B7F77C462417F6A755295781E4C5D9E58 (void);
// 0x00000262 System.Void easyar.Detail::easyar_PlaneData__dtor(System.IntPtr)
extern void Detail_easyar_PlaneData__dtor_mD777D5B7C77646F4B8F87ED1537E04631EF402E7 (void);
// 0x00000263 System.Void easyar.Detail::easyar_PlaneData__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_PlaneData__retain_mE6F9CB15BE162EA8E7B052712B36359E9B55F0EA (void);
// 0x00000264 System.IntPtr easyar.Detail::easyar_PlaneData__typeName(System.IntPtr)
extern void Detail_easyar_PlaneData__typeName_mE0A5C4AE29D9BD2DC9445742C9C8F8F33065927F (void);
// 0x00000265 System.Void easyar.Detail::easyar_SparseSpatialMapConfig__ctor(System.IntPtr&)
extern void Detail_easyar_SparseSpatialMapConfig__ctor_m8FE2EA52B927EA866348F8E455A7BD73287DE87C (void);
// 0x00000266 System.Void easyar.Detail::easyar_SparseSpatialMapConfig_setLocalizationMode(System.IntPtr,easyar.LocalizationMode)
extern void Detail_easyar_SparseSpatialMapConfig_setLocalizationMode_m5ECCA9A568F3BCDF58045B3355B25EEC790184B6 (void);
// 0x00000267 easyar.LocalizationMode easyar.Detail::easyar_SparseSpatialMapConfig_getLocalizationMode(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapConfig_getLocalizationMode_m96582C5B09288BDC0BA847EEAD5485A85026B50F (void);
// 0x00000268 System.Void easyar.Detail::easyar_SparseSpatialMapConfig__dtor(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapConfig__dtor_m4B6C9C47592C691DE16186CDC65D50F1BAC24A09 (void);
// 0x00000269 System.Void easyar.Detail::easyar_SparseSpatialMapConfig__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMapConfig__retain_mE0E7C67A1B16ABE21C7FAEEC07421FC2C1904B7D (void);
// 0x0000026A System.IntPtr easyar.Detail::easyar_SparseSpatialMapConfig__typeName(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapConfig__typeName_mAB0AFB14DC85D65E8B0659D740CC714E55169A34 (void);
// 0x0000026B System.Boolean easyar.Detail::easyar_SparseSpatialMap_isAvailable()
extern void Detail_easyar_SparseSpatialMap_isAvailable_mBE88A9C77F56DAA56E0EBB0DC36108280B721BCA (void);
// 0x0000026C System.Void easyar.Detail::easyar_SparseSpatialMap_inputFrameSink(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMap_inputFrameSink_m6123671A597EE737A8D60853D2D6AF1F9806D273 (void);
// 0x0000026D System.Int32 easyar.Detail::easyar_SparseSpatialMap_bufferRequirement(System.IntPtr)
extern void Detail_easyar_SparseSpatialMap_bufferRequirement_mF95D4C7CD1541ADE6C56005E9CEAA3DAC42EEAE7 (void);
// 0x0000026E System.Void easyar.Detail::easyar_SparseSpatialMap_outputFrameSource(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMap_outputFrameSource_mD95B0385931DA3FC32BA1CA433AAB872BD409714 (void);
// 0x0000026F System.Void easyar.Detail::easyar_SparseSpatialMap_create(System.IntPtr&)
extern void Detail_easyar_SparseSpatialMap_create_m91EF53FA93EF300F21F9E7A342AB947B100B6EE9 (void);
// 0x00000270 System.Boolean easyar.Detail::easyar_SparseSpatialMap_start(System.IntPtr)
extern void Detail_easyar_SparseSpatialMap_start_mC6C209FC143F1D229E089C81573A5808C2EAD490 (void);
// 0x00000271 System.Void easyar.Detail::easyar_SparseSpatialMap_stop(System.IntPtr)
extern void Detail_easyar_SparseSpatialMap_stop_m0D68D0162C4F58C1D9CEFD1238F5E5424DF6E44A (void);
// 0x00000272 System.Void easyar.Detail::easyar_SparseSpatialMap_close(System.IntPtr)
extern void Detail_easyar_SparseSpatialMap_close_mA546298717C23B7869071093470324B26ED415F7 (void);
// 0x00000273 System.Void easyar.Detail::easyar_SparseSpatialMap_getPointCloudBuffer(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMap_getPointCloudBuffer_m43AAB8D38806F7456A258C12D66EB39CD280B088 (void);
// 0x00000274 System.Void easyar.Detail::easyar_SparseSpatialMap_getMapPlanes(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMap_getMapPlanes_mF40833922861EE9E5FB294F28CCBE735D11875A7 (void);
// 0x00000275 System.Void easyar.Detail::easyar_SparseSpatialMap_hitTestAgainstPointCloud(System.IntPtr,easyar.Vec2F,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMap_hitTestAgainstPointCloud_mA92FA96AA8B906477E52836B66CC4E479D067045 (void);
// 0x00000276 System.Void easyar.Detail::easyar_SparseSpatialMap_hitTestAgainstPlanes(System.IntPtr,easyar.Vec2F,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMap_hitTestAgainstPlanes_m4047B195AB754260E4E7D1374DE431AC684C2299 (void);
// 0x00000277 System.Void easyar.Detail::easyar_SparseSpatialMap_getMapVersion(System.IntPtr&)
extern void Detail_easyar_SparseSpatialMap_getMapVersion_mF4D1A3FBD50F7A5F54336C690C1D997F8D13833E (void);
// 0x00000278 System.Void easyar.Detail::easyar_SparseSpatialMap_unloadMap(System.IntPtr,System.IntPtr,System.IntPtr,easyar.Detail/OptionalOfFunctorOfVoidFromBool)
extern void Detail_easyar_SparseSpatialMap_unloadMap_mB867BDDA2B2107FEE76E1E06415A375DB7AAFB96 (void);
// 0x00000279 System.Void easyar.Detail::easyar_SparseSpatialMap_setConfig(System.IntPtr,System.IntPtr)
extern void Detail_easyar_SparseSpatialMap_setConfig_m690BDC6EFA97833958B4504331BDC61AEED086C9 (void);
// 0x0000027A System.Void easyar.Detail::easyar_SparseSpatialMap_getConfig(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMap_getConfig_mBACFEEF54D61FA68039B813D5DC9C962D77C0704 (void);
// 0x0000027B System.Boolean easyar.Detail::easyar_SparseSpatialMap_startLocalization(System.IntPtr)
extern void Detail_easyar_SparseSpatialMap_startLocalization_mBF0DF641E51184A7883DCEC1B48358A1AE4B11AB (void);
// 0x0000027C System.Void easyar.Detail::easyar_SparseSpatialMap_stopLocalization(System.IntPtr)
extern void Detail_easyar_SparseSpatialMap_stopLocalization_mF7A0C967CA59B8D5B370B9BAE85C55DF87A4FBD4 (void);
// 0x0000027D System.Void easyar.Detail::easyar_SparseSpatialMap__dtor(System.IntPtr)
extern void Detail_easyar_SparseSpatialMap__dtor_mE8BA0180D02F0CBA277811E4885DDD0FB9930AAF (void);
// 0x0000027E System.Void easyar.Detail::easyar_SparseSpatialMap__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMap__retain_m4AA707A843149A40F03DA6576268FB41D58702C1 (void);
// 0x0000027F System.IntPtr easyar.Detail::easyar_SparseSpatialMap__typeName(System.IntPtr)
extern void Detail_easyar_SparseSpatialMap__typeName_m5AB42FBA95109D309FFC6F0A6156012BBF640BF1 (void);
// 0x00000280 System.Boolean easyar.Detail::easyar_SparseSpatialMapManager_isAvailable()
extern void Detail_easyar_SparseSpatialMapManager_isAvailable_mC81BE288E35E33F622EC716781CD0B7664F075C1 (void);
// 0x00000281 System.Void easyar.Detail::easyar_SparseSpatialMapManager_create(System.IntPtr&)
extern void Detail_easyar_SparseSpatialMapManager_create_m37320AA1B24D95A35EE33F2A4286EF9AF0A61714 (void);
// 0x00000282 System.Void easyar.Detail::easyar_SparseSpatialMapManager_host(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,easyar.Detail/OptionalOfImage,System.IntPtr,easyar.Detail/FunctorOfVoidFromBoolAndStringAndString)
extern void Detail_easyar_SparseSpatialMapManager_host_m05E49EB4E36BEFE9B4CCFD77E81A3219C5AFEEA3 (void);
// 0x00000283 System.Void easyar.Detail::easyar_SparseSpatialMapManager_load(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,easyar.Detail/FunctorOfVoidFromBoolAndString)
extern void Detail_easyar_SparseSpatialMapManager_load_m792234D212DB39489A6948AF849D378D64C7E455 (void);
// 0x00000284 System.Void easyar.Detail::easyar_SparseSpatialMapManager_clear(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapManager_clear_m867396C59E30B6AB407B928C591A677D2F11B051 (void);
// 0x00000285 System.Void easyar.Detail::easyar_SparseSpatialMapManager__dtor(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapManager__dtor_mCE7EE8CECA084FD20604ED9DE960916B8CF3EB91 (void);
// 0x00000286 System.Void easyar.Detail::easyar_SparseSpatialMapManager__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMapManager__retain_m707EBC3ECC912051E6D2A4D60312B61959901468 (void);
// 0x00000287 System.IntPtr easyar.Detail::easyar_SparseSpatialMapManager__typeName(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapManager__typeName_m401C66DEFE58495C037B0985D2C1042C4FE504DC (void);
// 0x00000288 System.Int32 easyar.Detail::easyar_Engine_schemaHash()
extern void Detail_easyar_Engine_schemaHash_m6F578A82F8311649E6F8CA600B3298928FCCC125 (void);
// 0x00000289 System.Boolean easyar.Detail::easyar_Engine_initialize(System.IntPtr)
extern void Detail_easyar_Engine_initialize_m2808284C3CCD0E5F10C71280241A8F8131EFF9E8 (void);
// 0x0000028A System.Void easyar.Detail::easyar_Engine_onPause()
extern void Detail_easyar_Engine_onPause_mFE24A1A0C398659CE6E89C79AE5E8140150E8B22 (void);
// 0x0000028B System.Void easyar.Detail::easyar_Engine_onResume()
extern void Detail_easyar_Engine_onResume_mB6F7F9B41F6B71CF8468A05222D01C36E66291E4 (void);
// 0x0000028C System.Void easyar.Detail::easyar_Engine_errorMessage(System.IntPtr&)
extern void Detail_easyar_Engine_errorMessage_mC7F773F6F9EC6E986C59001EF96DB017E08F07B2 (void);
// 0x0000028D System.Void easyar.Detail::easyar_Engine_versionString(System.IntPtr&)
extern void Detail_easyar_Engine_versionString_m9AFAB4AC12DB90CCEA9AC11F63CDBFAC6507AACC (void);
// 0x0000028E System.Void easyar.Detail::easyar_Engine_name(System.IntPtr&)
extern void Detail_easyar_Engine_name_m4FCAB81F099D97EF10AFF5D9CC100CA7246BF910 (void);
// 0x0000028F System.Void easyar.Detail::easyar_VideoPlayer__ctor(System.IntPtr&)
extern void Detail_easyar_VideoPlayer__ctor_m7DEEF39BCDF82BFA1EBFD2C4947281C4487AA0AF (void);
// 0x00000290 System.Boolean easyar.Detail::easyar_VideoPlayer_isAvailable()
extern void Detail_easyar_VideoPlayer_isAvailable_m247E99F08701BF16125C14E26BABBAADD68D40A3 (void);
// 0x00000291 System.Void easyar.Detail::easyar_VideoPlayer_setVideoType(System.IntPtr,easyar.VideoType)
extern void Detail_easyar_VideoPlayer_setVideoType_m660F8600937071ED11FB5346ECFB78F6A37D839D (void);
// 0x00000292 System.Void easyar.Detail::easyar_VideoPlayer_setRenderTexture(System.IntPtr,System.IntPtr)
extern void Detail_easyar_VideoPlayer_setRenderTexture_m916212678B69459C08516D2657FD603ED20CA5BB (void);
// 0x00000293 System.Void easyar.Detail::easyar_VideoPlayer_open(System.IntPtr,System.IntPtr,easyar.StorageType,System.IntPtr,easyar.Detail/OptionalOfFunctorOfVoidFromVideoStatus)
extern void Detail_easyar_VideoPlayer_open_m8BAA9EA44B7186C1915D2A528FFCB0FC1AAB5563 (void);
// 0x00000294 System.Void easyar.Detail::easyar_VideoPlayer_close(System.IntPtr)
extern void Detail_easyar_VideoPlayer_close_m5919A001327D2CBF0145D937C00D44F03810D1A8 (void);
// 0x00000295 System.Boolean easyar.Detail::easyar_VideoPlayer_play(System.IntPtr)
extern void Detail_easyar_VideoPlayer_play_m0CB1CCF485DFE61B7FC664CD0CF319AAA9B86413 (void);
// 0x00000296 System.Void easyar.Detail::easyar_VideoPlayer_stop(System.IntPtr)
extern void Detail_easyar_VideoPlayer_stop_m0C39C35D78CF0DC2F7110E4099A1E1DF601FB00C (void);
// 0x00000297 System.Void easyar.Detail::easyar_VideoPlayer_pause(System.IntPtr)
extern void Detail_easyar_VideoPlayer_pause_m38F54B89E7A8E23C3FF4D871D94A69D06F5C0B08 (void);
// 0x00000298 System.Boolean easyar.Detail::easyar_VideoPlayer_isRenderTextureAvailable(System.IntPtr)
extern void Detail_easyar_VideoPlayer_isRenderTextureAvailable_m7C8682F7D18949CC4A07411654FAE41E32C53B86 (void);
// 0x00000299 System.Void easyar.Detail::easyar_VideoPlayer_updateFrame(System.IntPtr)
extern void Detail_easyar_VideoPlayer_updateFrame_mE170AB115613CD74D0E6A02FEDA45241E3AB181A (void);
// 0x0000029A System.Int32 easyar.Detail::easyar_VideoPlayer_duration(System.IntPtr)
extern void Detail_easyar_VideoPlayer_duration_m3B9168D0CA8DF29ECBECEC54AE46B671382A5233 (void);
// 0x0000029B System.Int32 easyar.Detail::easyar_VideoPlayer_currentPosition(System.IntPtr)
extern void Detail_easyar_VideoPlayer_currentPosition_mCD83B3C1F52EAD0E6BBC0197F6D687B30A287E2A (void);
// 0x0000029C System.Boolean easyar.Detail::easyar_VideoPlayer_seek(System.IntPtr,System.Int32)
extern void Detail_easyar_VideoPlayer_seek_mCBA37307527BF6BC029E24AEDE42199C9E739282 (void);
// 0x0000029D easyar.Vec2I easyar.Detail::easyar_VideoPlayer_size(System.IntPtr)
extern void Detail_easyar_VideoPlayer_size_m54C1CF41F978BAEA1982376B75C1D5FA0CF4D11E (void);
// 0x0000029E System.Single easyar.Detail::easyar_VideoPlayer_volume(System.IntPtr)
extern void Detail_easyar_VideoPlayer_volume_m2843DFA0ADB258918A00816B6841C46FDB3D0739 (void);
// 0x0000029F System.Boolean easyar.Detail::easyar_VideoPlayer_setVolume(System.IntPtr,System.Single)
extern void Detail_easyar_VideoPlayer_setVolume_mB788E58E23D4A29AA23F04E3A1BA9BAA18E18587 (void);
// 0x000002A0 System.Void easyar.Detail::easyar_VideoPlayer__dtor(System.IntPtr)
extern void Detail_easyar_VideoPlayer__dtor_m5E05634EBF902BA73867793D38626D286B57E3B2 (void);
// 0x000002A1 System.Void easyar.Detail::easyar_VideoPlayer__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_VideoPlayer__retain_m142D6CAE8EDE88226A1DF0334C55DF6DDADEB839 (void);
// 0x000002A2 System.IntPtr easyar.Detail::easyar_VideoPlayer__typeName(System.IntPtr)
extern void Detail_easyar_VideoPlayer__typeName_mB82D9A777EBBDCFC0C53B086D3DF6023949B9B25 (void);
// 0x000002A3 System.Void easyar.Detail::easyar_ImageHelper_decode(System.IntPtr,easyar.Detail/OptionalOfImage&)
extern void Detail_easyar_ImageHelper_decode_m54E1A1473FBE0A4EC3041C4AC90BBC85E94A124C (void);
// 0x000002A4 System.Void easyar.Detail::easyar_SignalSink_handle(System.IntPtr)
extern void Detail_easyar_SignalSink_handle_mF8D4929F26413ABCF873201C154874B4F555EDED (void);
// 0x000002A5 System.Void easyar.Detail::easyar_SignalSink__dtor(System.IntPtr)
extern void Detail_easyar_SignalSink__dtor_m020A6AF4EF86B69E5F72A148DFC8ACB1938A6ECF (void);
// 0x000002A6 System.Void easyar.Detail::easyar_SignalSink__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SignalSink__retain_mE20AE59D901078E06BF50CD565C86EAA869675D6 (void);
// 0x000002A7 System.IntPtr easyar.Detail::easyar_SignalSink__typeName(System.IntPtr)
extern void Detail_easyar_SignalSink__typeName_m93E13AF5901FC5258959BF9214B0E0BDD21F2C4C (void);
// 0x000002A8 System.Void easyar.Detail::easyar_SignalSource_setHandler(System.IntPtr,easyar.Detail/OptionalOfFunctorOfVoid)
extern void Detail_easyar_SignalSource_setHandler_mC8EB8BAD3FB3B26B9CEFFF57CD880575E83AC9C6 (void);
// 0x000002A9 System.Void easyar.Detail::easyar_SignalSource_connect(System.IntPtr,System.IntPtr)
extern void Detail_easyar_SignalSource_connect_m39160F3C2489BE79586B0D129612B8867F43FF99 (void);
// 0x000002AA System.Void easyar.Detail::easyar_SignalSource_disconnect(System.IntPtr)
extern void Detail_easyar_SignalSource_disconnect_m4062413F99631943BAB09A710DB5C501027C1306 (void);
// 0x000002AB System.Void easyar.Detail::easyar_SignalSource__dtor(System.IntPtr)
extern void Detail_easyar_SignalSource__dtor_m047A72AD7F71771DC751C576AEE2DD9A8BADD858 (void);
// 0x000002AC System.Void easyar.Detail::easyar_SignalSource__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SignalSource__retain_mC2241D163FC6A0F84F35655B90F6F5176F4E75D6 (void);
// 0x000002AD System.IntPtr easyar.Detail::easyar_SignalSource__typeName(System.IntPtr)
extern void Detail_easyar_SignalSource__typeName_m95F2FE9FD51259458BCCB4F3BA1CDCA9BBE517CC (void);
// 0x000002AE System.Void easyar.Detail::easyar_InputFrameSink_handle(System.IntPtr,System.IntPtr)
extern void Detail_easyar_InputFrameSink_handle_mB5540ACC221D25F534202E6F6AD094F33EC33F01 (void);
// 0x000002AF System.Void easyar.Detail::easyar_InputFrameSink__dtor(System.IntPtr)
extern void Detail_easyar_InputFrameSink__dtor_m66461159B78670B7CE8C115FAE87117B618E9540 (void);
// 0x000002B0 System.Void easyar.Detail::easyar_InputFrameSink__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameSink__retain_m0B598173F3D208579263B5FCFFC4350B88094744 (void);
// 0x000002B1 System.IntPtr easyar.Detail::easyar_InputFrameSink__typeName(System.IntPtr)
extern void Detail_easyar_InputFrameSink__typeName_m8D06AFC57143DBC4C855F355268B40EAC952B197 (void);
// 0x000002B2 System.Void easyar.Detail::easyar_InputFrameSource_setHandler(System.IntPtr,easyar.Detail/OptionalOfFunctorOfVoidFromInputFrame)
extern void Detail_easyar_InputFrameSource_setHandler_m8BD6868FAF52B5DBC5D419E88CE3A750C305CDA0 (void);
// 0x000002B3 System.Void easyar.Detail::easyar_InputFrameSource_connect(System.IntPtr,System.IntPtr)
extern void Detail_easyar_InputFrameSource_connect_mB61B076815A78262766067931A7EEF5CED7B7DAD (void);
// 0x000002B4 System.Void easyar.Detail::easyar_InputFrameSource_disconnect(System.IntPtr)
extern void Detail_easyar_InputFrameSource_disconnect_mB1F0E055E8D7165536ABFDDC428A6E74A7BD6D43 (void);
// 0x000002B5 System.Void easyar.Detail::easyar_InputFrameSource__dtor(System.IntPtr)
extern void Detail_easyar_InputFrameSource__dtor_m99C011B4C4067C8041BAD206DD7AF0993AB98AD6 (void);
// 0x000002B6 System.Void easyar.Detail::easyar_InputFrameSource__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameSource__retain_m3C7F0D1558AC4C8386A092E822806158A98C4A52 (void);
// 0x000002B7 System.IntPtr easyar.Detail::easyar_InputFrameSource__typeName(System.IntPtr)
extern void Detail_easyar_InputFrameSource__typeName_m932250B2B9E1093F2136E872F9219DB88865767D (void);
// 0x000002B8 System.Void easyar.Detail::easyar_OutputFrameSink_handle(System.IntPtr,System.IntPtr)
extern void Detail_easyar_OutputFrameSink_handle_m005AF3CC6B5EE21AF3CB2B7BD4CF584B92C2426B (void);
// 0x000002B9 System.Void easyar.Detail::easyar_OutputFrameSink__dtor(System.IntPtr)
extern void Detail_easyar_OutputFrameSink__dtor_mF9A5126192F570D39EF035665CCB31333DED5861 (void);
// 0x000002BA System.Void easyar.Detail::easyar_OutputFrameSink__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrameSink__retain_mE259890E77414BBE2B7E893AB9A00874524B48F1 (void);
// 0x000002BB System.IntPtr easyar.Detail::easyar_OutputFrameSink__typeName(System.IntPtr)
extern void Detail_easyar_OutputFrameSink__typeName_mE0B3F0F3A607684030AA07F0F0EA772BE1703801 (void);
// 0x000002BC System.Void easyar.Detail::easyar_OutputFrameSource_setHandler(System.IntPtr,easyar.Detail/OptionalOfFunctorOfVoidFromOutputFrame)
extern void Detail_easyar_OutputFrameSource_setHandler_mAE9C0083CB087AA6DCD29AEE2C8C611CD73315AD (void);
// 0x000002BD System.Void easyar.Detail::easyar_OutputFrameSource_connect(System.IntPtr,System.IntPtr)
extern void Detail_easyar_OutputFrameSource_connect_mBB280A8EF6C0AC595B4D97FFFDFEA9E78C109279 (void);
// 0x000002BE System.Void easyar.Detail::easyar_OutputFrameSource_disconnect(System.IntPtr)
extern void Detail_easyar_OutputFrameSource_disconnect_m0A2096BB11941B1D91FBA9BD2A971CDB89775AA2 (void);
// 0x000002BF System.Void easyar.Detail::easyar_OutputFrameSource__dtor(System.IntPtr)
extern void Detail_easyar_OutputFrameSource__dtor_mB5DA4B03955B3520667F5124B1D24C6CAE568600 (void);
// 0x000002C0 System.Void easyar.Detail::easyar_OutputFrameSource__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrameSource__retain_m1A7E3E551ABAB77CC100C663C82055E0F31B18CE (void);
// 0x000002C1 System.IntPtr easyar.Detail::easyar_OutputFrameSource__typeName(System.IntPtr)
extern void Detail_easyar_OutputFrameSource__typeName_mD81C08884E8BFEB84A305779188878A9553DF1F5 (void);
// 0x000002C2 System.Void easyar.Detail::easyar_FeedbackFrameSink_handle(System.IntPtr,System.IntPtr)
extern void Detail_easyar_FeedbackFrameSink_handle_m81FC5687C0994262F0BB630B6BEF372CC126BEBB (void);
// 0x000002C3 System.Void easyar.Detail::easyar_FeedbackFrameSink__dtor(System.IntPtr)
extern void Detail_easyar_FeedbackFrameSink__dtor_m6DB49F4E319A89071E02BEED1E4F830434E83C92 (void);
// 0x000002C4 System.Void easyar.Detail::easyar_FeedbackFrameSink__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_FeedbackFrameSink__retain_m44875276040A45B9FB190DF42F7BEDEE1691846B (void);
// 0x000002C5 System.IntPtr easyar.Detail::easyar_FeedbackFrameSink__typeName(System.IntPtr)
extern void Detail_easyar_FeedbackFrameSink__typeName_m8C23AE43282A8C560924D93DEE50BBAF81D1DAF2 (void);
// 0x000002C6 System.Void easyar.Detail::easyar_FeedbackFrameSource_setHandler(System.IntPtr,easyar.Detail/OptionalOfFunctorOfVoidFromFeedbackFrame)
extern void Detail_easyar_FeedbackFrameSource_setHandler_m22EE616EAF4B46CF8343612E37FB371CAAF638F6 (void);
// 0x000002C7 System.Void easyar.Detail::easyar_FeedbackFrameSource_connect(System.IntPtr,System.IntPtr)
extern void Detail_easyar_FeedbackFrameSource_connect_m51709B65AFE9470F9B1D7BA5864A3F66C0F4EBF4 (void);
// 0x000002C8 System.Void easyar.Detail::easyar_FeedbackFrameSource_disconnect(System.IntPtr)
extern void Detail_easyar_FeedbackFrameSource_disconnect_mF932D449C519B9A82972CED0C7080F745806ABA0 (void);
// 0x000002C9 System.Void easyar.Detail::easyar_FeedbackFrameSource__dtor(System.IntPtr)
extern void Detail_easyar_FeedbackFrameSource__dtor_m1072D69C201ECB0FF0E0A5B782B52D4C3C993B68 (void);
// 0x000002CA System.Void easyar.Detail::easyar_FeedbackFrameSource__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_FeedbackFrameSource__retain_m677CA87D360C67BC97FFDC1BB5338D0347C4EE12 (void);
// 0x000002CB System.IntPtr easyar.Detail::easyar_FeedbackFrameSource__typeName(System.IntPtr)
extern void Detail_easyar_FeedbackFrameSource__typeName_m37E8388B4ECB95B903CD4689A96E6639207F2DF9 (void);
// 0x000002CC System.Void easyar.Detail::easyar_InputFrameFork_input(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameFork_input_mB275D38AE4E9E874EEABFFE738826C7FEEEA3D0B (void);
// 0x000002CD System.Void easyar.Detail::easyar_InputFrameFork_output(System.IntPtr,System.Int32,System.IntPtr&)
extern void Detail_easyar_InputFrameFork_output_m7186A23030C142637B5CDBD28C14D4556D4F12AE (void);
// 0x000002CE System.Int32 easyar.Detail::easyar_InputFrameFork_outputCount(System.IntPtr)
extern void Detail_easyar_InputFrameFork_outputCount_mA8AC4E9200789FA962EB416DC41A31DFD8D7B554 (void);
// 0x000002CF System.Void easyar.Detail::easyar_InputFrameFork_create(System.Int32,System.IntPtr&)
extern void Detail_easyar_InputFrameFork_create_mBFE6FE8B154267C078B2D20D7871BCAE801F43D3 (void);
// 0x000002D0 System.Void easyar.Detail::easyar_InputFrameFork__dtor(System.IntPtr)
extern void Detail_easyar_InputFrameFork__dtor_mA54172D89163226A9B96EE4FF5D4715F90C7D2D4 (void);
// 0x000002D1 System.Void easyar.Detail::easyar_InputFrameFork__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameFork__retain_m78142EBFED0AF893F23A9E9D15E662FADE000C2D (void);
// 0x000002D2 System.IntPtr easyar.Detail::easyar_InputFrameFork__typeName(System.IntPtr)
extern void Detail_easyar_InputFrameFork__typeName_mD0EA3EDE8D8AEA165DAFE3F26E12439191CCBE25 (void);
// 0x000002D3 System.Void easyar.Detail::easyar_OutputFrameFork_input(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrameFork_input_mE2886D9845537FE7F158F19BB432BC8B37C3C4F2 (void);
// 0x000002D4 System.Void easyar.Detail::easyar_OutputFrameFork_output(System.IntPtr,System.Int32,System.IntPtr&)
extern void Detail_easyar_OutputFrameFork_output_mA9E6E6FB1AE2F79457EBA2D19A9B21C39D144854 (void);
// 0x000002D5 System.Int32 easyar.Detail::easyar_OutputFrameFork_outputCount(System.IntPtr)
extern void Detail_easyar_OutputFrameFork_outputCount_m6AD280BABA4F7D1F837467A14683931E018B3F26 (void);
// 0x000002D6 System.Void easyar.Detail::easyar_OutputFrameFork_create(System.Int32,System.IntPtr&)
extern void Detail_easyar_OutputFrameFork_create_m589A7D9B8A8571B76A46A8EE31B7227CB09BD4C3 (void);
// 0x000002D7 System.Void easyar.Detail::easyar_OutputFrameFork__dtor(System.IntPtr)
extern void Detail_easyar_OutputFrameFork__dtor_m9F594F28100D68CAAFD9C22898B973517A246C23 (void);
// 0x000002D8 System.Void easyar.Detail::easyar_OutputFrameFork__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrameFork__retain_m611B6B48C25064E8BFE1951023D4302FF080CCA2 (void);
// 0x000002D9 System.IntPtr easyar.Detail::easyar_OutputFrameFork__typeName(System.IntPtr)
extern void Detail_easyar_OutputFrameFork__typeName_mAE7671463C5E8FD795CCCA7F989DE2C4BB29DCB3 (void);
// 0x000002DA System.Void easyar.Detail::easyar_OutputFrameJoin_input(System.IntPtr,System.Int32,System.IntPtr&)
extern void Detail_easyar_OutputFrameJoin_input_mD6A852B1EFF87A699611A98E24F1C79DEF8CB16E (void);
// 0x000002DB System.Void easyar.Detail::easyar_OutputFrameJoin_output(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrameJoin_output_m83C4B14052CB595530A149985B91C53DE1B51D07 (void);
// 0x000002DC System.Int32 easyar.Detail::easyar_OutputFrameJoin_inputCount(System.IntPtr)
extern void Detail_easyar_OutputFrameJoin_inputCount_mBDEAED819FE0AC103EC11A0D684F134C3A345C5A (void);
// 0x000002DD System.Void easyar.Detail::easyar_OutputFrameJoin_create(System.Int32,System.IntPtr&)
extern void Detail_easyar_OutputFrameJoin_create_mD364B93567339FBCE28ED01DE049E533DF296D52 (void);
// 0x000002DE System.Void easyar.Detail::easyar_OutputFrameJoin_createWithJoiner(System.Int32,easyar.Detail/FunctorOfOutputFrameFromListOfOutputFrame,System.IntPtr&)
extern void Detail_easyar_OutputFrameJoin_createWithJoiner_m694281D772425C857E61A918557494BF6F2433F5 (void);
// 0x000002DF System.Void easyar.Detail::easyar_OutputFrameJoin__dtor(System.IntPtr)
extern void Detail_easyar_OutputFrameJoin__dtor_m23D06DE2B88604D03CAE6CB32F4D4091D59D146D (void);
// 0x000002E0 System.Void easyar.Detail::easyar_OutputFrameJoin__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrameJoin__retain_m3C3B941E4F9CBB9F00B9F25F0FE9782ED943E02E (void);
// 0x000002E1 System.IntPtr easyar.Detail::easyar_OutputFrameJoin__typeName(System.IntPtr)
extern void Detail_easyar_OutputFrameJoin__typeName_mDB1DB4C091499DD99B8EAD48B0FC5FBD0428EA12 (void);
// 0x000002E2 System.Void easyar.Detail::easyar_FeedbackFrameFork_input(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_FeedbackFrameFork_input_m57800AE3BC9398FFBDA0985A8470ED92D592D434 (void);
// 0x000002E3 System.Void easyar.Detail::easyar_FeedbackFrameFork_output(System.IntPtr,System.Int32,System.IntPtr&)
extern void Detail_easyar_FeedbackFrameFork_output_m384CBE75EC3FF7369C42C78EE7536B530CFE3ABE (void);
// 0x000002E4 System.Int32 easyar.Detail::easyar_FeedbackFrameFork_outputCount(System.IntPtr)
extern void Detail_easyar_FeedbackFrameFork_outputCount_m6CE611293059CAB6FD246B6079876D64264F919A (void);
// 0x000002E5 System.Void easyar.Detail::easyar_FeedbackFrameFork_create(System.Int32,System.IntPtr&)
extern void Detail_easyar_FeedbackFrameFork_create_mCFC66E5F05A7216880F124FF9288D6915294A458 (void);
// 0x000002E6 System.Void easyar.Detail::easyar_FeedbackFrameFork__dtor(System.IntPtr)
extern void Detail_easyar_FeedbackFrameFork__dtor_mBDD3DFD7324105447F763F200716DF75DF9D3FB5 (void);
// 0x000002E7 System.Void easyar.Detail::easyar_FeedbackFrameFork__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_FeedbackFrameFork__retain_mC0209A5E29DE1EAA0AB8B3B8FF490B70B2BA576F (void);
// 0x000002E8 System.IntPtr easyar.Detail::easyar_FeedbackFrameFork__typeName(System.IntPtr)
extern void Detail_easyar_FeedbackFrameFork__typeName_m217B953453A42A68D3B61507DD1FF90D531A092A (void);
// 0x000002E9 System.Void easyar.Detail::easyar_InputFrameThrottler_input(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameThrottler_input_m8D300B50DEE77924E128439ABCE022C6138502BB (void);
// 0x000002EA System.Int32 easyar.Detail::easyar_InputFrameThrottler_bufferRequirement(System.IntPtr)
extern void Detail_easyar_InputFrameThrottler_bufferRequirement_mA0101746C556B8C97FB20C15AAA37935BD42AFA1 (void);
// 0x000002EB System.Void easyar.Detail::easyar_InputFrameThrottler_output(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameThrottler_output_m0BD6AD005A874C8DF5C886D24C64800BB1E33C10 (void);
// 0x000002EC System.Void easyar.Detail::easyar_InputFrameThrottler_signalInput(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameThrottler_signalInput_m8C246FA720BEC088B27F552194D8C781B3465B6A (void);
// 0x000002ED System.Void easyar.Detail::easyar_InputFrameThrottler_create(System.IntPtr&)
extern void Detail_easyar_InputFrameThrottler_create_m695E13A5FE7F7FDCB3979E90A8A920D6340FF21D (void);
// 0x000002EE System.Void easyar.Detail::easyar_InputFrameThrottler__dtor(System.IntPtr)
extern void Detail_easyar_InputFrameThrottler__dtor_m90E7A4D2DAD0A7DCED7A811AF805CA7659A6C048 (void);
// 0x000002EF System.Void easyar.Detail::easyar_InputFrameThrottler__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameThrottler__retain_mF6C22242C3DC46ACB9AF59EE781346A76BC0A951 (void);
// 0x000002F0 System.IntPtr easyar.Detail::easyar_InputFrameThrottler__typeName(System.IntPtr)
extern void Detail_easyar_InputFrameThrottler__typeName_mA7436BD865EAD57DDF15F43BB100C7C21F18AE66 (void);
// 0x000002F1 System.Void easyar.Detail::easyar_OutputFrameBuffer_input(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrameBuffer_input_m8C6FFF6312CB4607D96EB361E30B3A8980ED53F6 (void);
// 0x000002F2 System.Int32 easyar.Detail::easyar_OutputFrameBuffer_bufferRequirement(System.IntPtr)
extern void Detail_easyar_OutputFrameBuffer_bufferRequirement_m19C61150AFB4866A2C09975990F420F6ADD4E347 (void);
// 0x000002F3 System.Void easyar.Detail::easyar_OutputFrameBuffer_signalOutput(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrameBuffer_signalOutput_m41F1607F7A9C8C84517252D68B7E144E7952E2B7 (void);
// 0x000002F4 System.Void easyar.Detail::easyar_OutputFrameBuffer_peek(System.IntPtr,easyar.Detail/OptionalOfOutputFrame&)
extern void Detail_easyar_OutputFrameBuffer_peek_mC923F2ED92AB6D7F6D0BE5D7A146676667E451F0 (void);
// 0x000002F5 System.Void easyar.Detail::easyar_OutputFrameBuffer_create(System.IntPtr&)
extern void Detail_easyar_OutputFrameBuffer_create_mC6D7699ACCCC386E9D1B34490B072252DA9B83DB (void);
// 0x000002F6 System.Void easyar.Detail::easyar_OutputFrameBuffer_pause(System.IntPtr)
extern void Detail_easyar_OutputFrameBuffer_pause_mCB67726A2498413D5502ED22428CACD9E150EEBA (void);
// 0x000002F7 System.Void easyar.Detail::easyar_OutputFrameBuffer_resume(System.IntPtr)
extern void Detail_easyar_OutputFrameBuffer_resume_mCDFFC6F51CFBDB27D38A4CC5CE96102A843C2569 (void);
// 0x000002F8 System.Void easyar.Detail::easyar_OutputFrameBuffer__dtor(System.IntPtr)
extern void Detail_easyar_OutputFrameBuffer__dtor_m9629F9FC4768C465EE0803331F581DC052E61B06 (void);
// 0x000002F9 System.Void easyar.Detail::easyar_OutputFrameBuffer__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrameBuffer__retain_mA24BD34C823DF71D7B56D98348CD7A692EFA12B5 (void);
// 0x000002FA System.IntPtr easyar.Detail::easyar_OutputFrameBuffer__typeName(System.IntPtr)
extern void Detail_easyar_OutputFrameBuffer__typeName_m29257668A9C6A3AEA190163C467579E64241B60E (void);
// 0x000002FB System.Void easyar.Detail::easyar_InputFrameToOutputFrameAdapter_input(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameToOutputFrameAdapter_input_mEC8DBCBC19E2B6554576AF226836D6E795D6A36A (void);
// 0x000002FC System.Void easyar.Detail::easyar_InputFrameToOutputFrameAdapter_output(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameToOutputFrameAdapter_output_m43000B084DE6527B0819D1FF2C03D896F287D710 (void);
// 0x000002FD System.Void easyar.Detail::easyar_InputFrameToOutputFrameAdapter_create(System.IntPtr&)
extern void Detail_easyar_InputFrameToOutputFrameAdapter_create_mBF2C272141770A9D9BA8E6371A8598D9B927D55D (void);
// 0x000002FE System.Void easyar.Detail::easyar_InputFrameToOutputFrameAdapter__dtor(System.IntPtr)
extern void Detail_easyar_InputFrameToOutputFrameAdapter__dtor_mB47E5E792163583CFEF26FFFEDF268F393863806 (void);
// 0x000002FF System.Void easyar.Detail::easyar_InputFrameToOutputFrameAdapter__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameToOutputFrameAdapter__retain_m27F00C1EC372C61AFAD5F989253A1F49C77678C7 (void);
// 0x00000300 System.IntPtr easyar.Detail::easyar_InputFrameToOutputFrameAdapter__typeName(System.IntPtr)
extern void Detail_easyar_InputFrameToOutputFrameAdapter__typeName_mBD44555301188DC4EEA8C825B3380510831D6781 (void);
// 0x00000301 System.Void easyar.Detail::easyar_InputFrameToFeedbackFrameAdapter_input(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameToFeedbackFrameAdapter_input_m066255D89E5C2727E518BBD30E028E24523591E8 (void);
// 0x00000302 System.Int32 easyar.Detail::easyar_InputFrameToFeedbackFrameAdapter_bufferRequirement(System.IntPtr)
extern void Detail_easyar_InputFrameToFeedbackFrameAdapter_bufferRequirement_mC96C762A823766EC31595EA401DD7EE8FBA25F3F (void);
// 0x00000303 System.Void easyar.Detail::easyar_InputFrameToFeedbackFrameAdapter_sideInput(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameToFeedbackFrameAdapter_sideInput_m533577CC9CCA1535D18BBD1648D356A89CDACD0E (void);
// 0x00000304 System.Void easyar.Detail::easyar_InputFrameToFeedbackFrameAdapter_output(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameToFeedbackFrameAdapter_output_mF57204BBCA48F3CF1E47578EABAE94D285C3F60D (void);
// 0x00000305 System.Void easyar.Detail::easyar_InputFrameToFeedbackFrameAdapter_create(System.IntPtr&)
extern void Detail_easyar_InputFrameToFeedbackFrameAdapter_create_mA9CF29FEF03B4BAD7B44421A96FBF1126458D048 (void);
// 0x00000306 System.Void easyar.Detail::easyar_InputFrameToFeedbackFrameAdapter__dtor(System.IntPtr)
extern void Detail_easyar_InputFrameToFeedbackFrameAdapter__dtor_m1A6C640B2D12C1B2EC2E896C1A41BC65A818BE86 (void);
// 0x00000307 System.Void easyar.Detail::easyar_InputFrameToFeedbackFrameAdapter__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameToFeedbackFrameAdapter__retain_mA4723A50EA44182D2B7A0886BEA6E5BF2E1533E8 (void);
// 0x00000308 System.IntPtr easyar.Detail::easyar_InputFrameToFeedbackFrameAdapter__typeName(System.IntPtr)
extern void Detail_easyar_InputFrameToFeedbackFrameAdapter__typeName_mE6959B1246DD06AB0C7C69B4CC7101EB24689E6F (void);
// 0x00000309 System.Int32 easyar.Detail::easyar_InputFrame_index(System.IntPtr)
extern void Detail_easyar_InputFrame_index_mEDB2856C9FF9A88E9920ECBFA50950087AC48969 (void);
// 0x0000030A System.Void easyar.Detail::easyar_InputFrame_image(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrame_image_m140EFB6262F340B9F0F3916E72141833D74160D0 (void);
// 0x0000030B System.Boolean easyar.Detail::easyar_InputFrame_hasCameraParameters(System.IntPtr)
extern void Detail_easyar_InputFrame_hasCameraParameters_mD931E132D851F4DA7F88CB8EE5BCFC5C9C145BBA (void);
// 0x0000030C System.Void easyar.Detail::easyar_InputFrame_cameraParameters(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrame_cameraParameters_mA62697433558044B6FF1BD53681AC6AA9B25B328 (void);
// 0x0000030D System.Boolean easyar.Detail::easyar_InputFrame_hasTemporalInformation(System.IntPtr)
extern void Detail_easyar_InputFrame_hasTemporalInformation_m36F5B3CF5A939305D81930E233F1089EB521119E (void);
// 0x0000030E System.Double easyar.Detail::easyar_InputFrame_timestamp(System.IntPtr)
extern void Detail_easyar_InputFrame_timestamp_mF7C39F86224842E30C4B3173E090B9721E3DD552 (void);
// 0x0000030F System.Boolean easyar.Detail::easyar_InputFrame_hasSpatialInformation(System.IntPtr)
extern void Detail_easyar_InputFrame_hasSpatialInformation_mD3FB17E31F570987126A639EA8FF612D4C2A0244 (void);
// 0x00000310 easyar.Matrix44F easyar.Detail::easyar_InputFrame_cameraTransform(System.IntPtr)
extern void Detail_easyar_InputFrame_cameraTransform_m6F3F30E489C5897C7ABDBF871120564FA6B330C9 (void);
// 0x00000311 easyar.MotionTrackingStatus easyar.Detail::easyar_InputFrame_trackingStatus(System.IntPtr)
extern void Detail_easyar_InputFrame_trackingStatus_m09ECEACD869F2FA44DD3DAFC5059107BFEF23A02 (void);
// 0x00000312 System.Void easyar.Detail::easyar_InputFrame_create(System.IntPtr,System.IntPtr,System.Double,easyar.Matrix44F,easyar.MotionTrackingStatus,System.IntPtr&)
extern void Detail_easyar_InputFrame_create_m754F0F74A62561C8EFCD40841C7EC2F4FF8627DE (void);
// 0x00000313 System.Void easyar.Detail::easyar_InputFrame_createWithImageAndCameraParametersAndTemporal(System.IntPtr,System.IntPtr,System.Double,System.IntPtr&)
extern void Detail_easyar_InputFrame_createWithImageAndCameraParametersAndTemporal_m5260B805F182EB356D16A83371827A68EB7A75BE (void);
// 0x00000314 System.Void easyar.Detail::easyar_InputFrame_createWithImageAndCameraParameters(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrame_createWithImageAndCameraParameters_m141AF32142981C8D0B33190C555D3715482C377B (void);
// 0x00000315 System.Void easyar.Detail::easyar_InputFrame_createWithImage(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrame_createWithImage_mDDEE39E781193DA245C0F447E5937E201653E875 (void);
// 0x00000316 System.Void easyar.Detail::easyar_InputFrame__dtor(System.IntPtr)
extern void Detail_easyar_InputFrame__dtor_mA41B0663599C7850BE0670167D8A51083454F19E (void);
// 0x00000317 System.Void easyar.Detail::easyar_InputFrame__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrame__retain_m941C00204E986077F77F1B49AA1C6535DEF0F0EE (void);
// 0x00000318 System.IntPtr easyar.Detail::easyar_InputFrame__typeName(System.IntPtr)
extern void Detail_easyar_InputFrame__typeName_mD9811522E0609F824D15428806FE8B2E5A4573DA (void);
// 0x00000319 System.Void easyar.Detail::easyar_FrameFilterResult__dtor(System.IntPtr)
extern void Detail_easyar_FrameFilterResult__dtor_m9E3E4A0BCF1622F04F0C71BFB8C1EC3EAD31BBA1 (void);
// 0x0000031A System.Void easyar.Detail::easyar_FrameFilterResult__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_FrameFilterResult__retain_mBFF0B4D846C559A0C1A40A107F17426482076967 (void);
// 0x0000031B System.IntPtr easyar.Detail::easyar_FrameFilterResult__typeName(System.IntPtr)
extern void Detail_easyar_FrameFilterResult__typeName_mF7F3C046209720735927948B7B26A27D83B8F6D0 (void);
// 0x0000031C System.Void easyar.Detail::easyar_OutputFrame__ctor(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrame__ctor_m658A62084DD2F33A00BDDC27E91E3B446565A7D7 (void);
// 0x0000031D System.Int32 easyar.Detail::easyar_OutputFrame_index(System.IntPtr)
extern void Detail_easyar_OutputFrame_index_mE738B10BE5849CA075F81D71709BEAD6D99E5FD2 (void);
// 0x0000031E System.Void easyar.Detail::easyar_OutputFrame_inputFrame(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrame_inputFrame_m0E1A48E600F3D95869E56BC6E83FD954636C6E8D (void);
// 0x0000031F System.Void easyar.Detail::easyar_OutputFrame_results(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrame_results_m241A08A345B5878CB51D282B81A6390E20C31024 (void);
// 0x00000320 System.Void easyar.Detail::easyar_OutputFrame__dtor(System.IntPtr)
extern void Detail_easyar_OutputFrame__dtor_m9DB1463D81EE0A7E8865889925B8EE3AA5759630 (void);
// 0x00000321 System.Void easyar.Detail::easyar_OutputFrame__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrame__retain_m3F25965774DB1FD083E9BAECF5AD271E3E7EEB73 (void);
// 0x00000322 System.IntPtr easyar.Detail::easyar_OutputFrame__typeName(System.IntPtr)
extern void Detail_easyar_OutputFrame__typeName_mB17A156F808FAB28F3B8116BE77F24EC1A8F1D78 (void);
// 0x00000323 System.Void easyar.Detail::easyar_FeedbackFrame__ctor(System.IntPtr,easyar.Detail/OptionalOfOutputFrame,System.IntPtr&)
extern void Detail_easyar_FeedbackFrame__ctor_mE8130EAE0CF0E3888A7885B0E7E6C642383D65CD (void);
// 0x00000324 System.Void easyar.Detail::easyar_FeedbackFrame_inputFrame(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_FeedbackFrame_inputFrame_mF682B084F7560A4B26AA6571FBB0214A07207645 (void);
// 0x00000325 System.Void easyar.Detail::easyar_FeedbackFrame_previousOutputFrame(System.IntPtr,easyar.Detail/OptionalOfOutputFrame&)
extern void Detail_easyar_FeedbackFrame_previousOutputFrame_m8DC07F8A8228C5B660D3B340004F8208F3568468 (void);
// 0x00000326 System.Void easyar.Detail::easyar_FeedbackFrame__dtor(System.IntPtr)
extern void Detail_easyar_FeedbackFrame__dtor_mB30F6BF9135F890A874E5EAB99FCC3AB6D8DF5A2 (void);
// 0x00000327 System.Void easyar.Detail::easyar_FeedbackFrame__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_FeedbackFrame__retain_m5CE500F436BFC3D2021CE70C61F1E9CCF18EC8C1 (void);
// 0x00000328 System.IntPtr easyar.Detail::easyar_FeedbackFrame__typeName(System.IntPtr)
extern void Detail_easyar_FeedbackFrame__typeName_m6D201590A998FCEF6F10222740DA6CD67A2AC9C7 (void);
// 0x00000329 System.Int32 easyar.Detail::easyar_Target_runtimeID(System.IntPtr)
extern void Detail_easyar_Target_runtimeID_m8133CBAB8D8BC24930D60997CD9B363141388090 (void);
// 0x0000032A System.Void easyar.Detail::easyar_Target_uid(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_Target_uid_m64B63E65751B41F3C942BAE4BA0D435B69B7AFDE (void);
// 0x0000032B System.Void easyar.Detail::easyar_Target_name(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_Target_name_mBDE3F498CCF6ECAF3487C69FE68FA89B6150AB16 (void);
// 0x0000032C System.Void easyar.Detail::easyar_Target_setName(System.IntPtr,System.IntPtr)
extern void Detail_easyar_Target_setName_m2654EBF078FE6C6C0F83D963D8E33A8C25D07D39 (void);
// 0x0000032D System.Void easyar.Detail::easyar_Target_meta(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_Target_meta_m326FDC00B0E1D1C3F6D9F69EFCC66202664A584C (void);
// 0x0000032E System.Void easyar.Detail::easyar_Target_setMeta(System.IntPtr,System.IntPtr)
extern void Detail_easyar_Target_setMeta_m5F31D654E462FAC79AA3D065C08763A9AB110066 (void);
// 0x0000032F System.Void easyar.Detail::easyar_Target__dtor(System.IntPtr)
extern void Detail_easyar_Target__dtor_m7AB63DE8A08DB4D9C65F35719CC085877EFD3F19 (void);
// 0x00000330 System.Void easyar.Detail::easyar_Target__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_Target__retain_m7237D732A5DABC4791C37561F6B275D63B68F9F4 (void);
// 0x00000331 System.IntPtr easyar.Detail::easyar_Target__typeName(System.IntPtr)
extern void Detail_easyar_Target__typeName_m395D719B2522F8518226EA93C1D2819652370C9B (void);
// 0x00000332 System.Void easyar.Detail::easyar_TargetInstance__ctor(System.IntPtr&)
extern void Detail_easyar_TargetInstance__ctor_m0DD4ADBAF3D0252867A147A7CD4DE6F3738CE3CB (void);
// 0x00000333 easyar.TargetStatus easyar.Detail::easyar_TargetInstance_status(System.IntPtr)
extern void Detail_easyar_TargetInstance_status_m31F421A09DED7AF3990AC857B3D5CA98714C76ED (void);
// 0x00000334 System.Void easyar.Detail::easyar_TargetInstance_target(System.IntPtr,easyar.Detail/OptionalOfTarget&)
extern void Detail_easyar_TargetInstance_target_m136FB8DFEE24063CCA22233F2E235F67C78FD9BA (void);
// 0x00000335 easyar.Matrix44F easyar.Detail::easyar_TargetInstance_pose(System.IntPtr)
extern void Detail_easyar_TargetInstance_pose_m7686948E36D6B5F981FEA41DAB373AA97791BD7E (void);
// 0x00000336 System.Void easyar.Detail::easyar_TargetInstance__dtor(System.IntPtr)
extern void Detail_easyar_TargetInstance__dtor_mE4E998FD33C32B2BBE0F2261FC1A21E26BF2F052 (void);
// 0x00000337 System.Void easyar.Detail::easyar_TargetInstance__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_TargetInstance__retain_m55AEDEADBCC847B0817482F5149AC885D6947D86 (void);
// 0x00000338 System.IntPtr easyar.Detail::easyar_TargetInstance__typeName(System.IntPtr)
extern void Detail_easyar_TargetInstance__typeName_m6029D2120AACBEB48D17BCE40BD28A881C04D05F (void);
// 0x00000339 System.Void easyar.Detail::easyar_TargetTrackerResult_targetInstances(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_TargetTrackerResult_targetInstances_m3504052671989AE87C0FE4644B6988DF0DD0525B (void);
// 0x0000033A System.Void easyar.Detail::easyar_TargetTrackerResult_setTargetInstances(System.IntPtr,System.IntPtr)
extern void Detail_easyar_TargetTrackerResult_setTargetInstances_m6DB34AB5A1E8DEE2C138994F722C0E4D84B5C036 (void);
// 0x0000033B System.Void easyar.Detail::easyar_TargetTrackerResult__dtor(System.IntPtr)
extern void Detail_easyar_TargetTrackerResult__dtor_m908866548574FE5DEE920577ED6A2062053834DF (void);
// 0x0000033C System.Void easyar.Detail::easyar_TargetTrackerResult__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_TargetTrackerResult__retain_m670DE6D7F57400063547E12E3EF3C8790C9B543D (void);
// 0x0000033D System.IntPtr easyar.Detail::easyar_TargetTrackerResult__typeName(System.IntPtr)
extern void Detail_easyar_TargetTrackerResult__typeName_m6FBA8B4058BCFF5FD27F22A4A604809AB0253536 (void);
// 0x0000033E System.Void easyar.Detail::easyar_castTargetTrackerResultToFrameFilterResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castTargetTrackerResultToFrameFilterResult_mCA3AB27FF8A530C05456D13A7B88147285B09328 (void);
// 0x0000033F System.Void easyar.Detail::easyar_tryCastFrameFilterResultToTargetTrackerResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastFrameFilterResultToTargetTrackerResult_m456D60E618967C9C48D9F92FCA77CD0896CEAC9F (void);
// 0x00000340 System.Int32 easyar.Detail::easyar_TextureId_getInt(System.IntPtr)
extern void Detail_easyar_TextureId_getInt_m842A142D7F8201DED1A3E9DFE1B65A52AA30EF9D (void);
// 0x00000341 System.IntPtr easyar.Detail::easyar_TextureId_getPointer(System.IntPtr)
extern void Detail_easyar_TextureId_getPointer_mEBDBE484C534F23FB2678577D30384B58469B4B0 (void);
// 0x00000342 System.Void easyar.Detail::easyar_TextureId_fromInt(System.Int32,System.IntPtr&)
extern void Detail_easyar_TextureId_fromInt_m48F5A7434E24DF8D0706FAF5A5D8804F2460DF45 (void);
// 0x00000343 System.Void easyar.Detail::easyar_TextureId_fromPointer(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_TextureId_fromPointer_mA4A591B382CFCEFECF7A4CA8CBD219A969FD9E42 (void);
// 0x00000344 System.Void easyar.Detail::easyar_TextureId__dtor(System.IntPtr)
extern void Detail_easyar_TextureId__dtor_m09C583139324ED5C363164493E952E6E48C53730 (void);
// 0x00000345 System.Void easyar.Detail::easyar_TextureId__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_TextureId__retain_mD84406FFD9C8C110F33ABAD1C601301B8D224B10 (void);
// 0x00000346 System.IntPtr easyar.Detail::easyar_TextureId__typeName(System.IntPtr)
extern void Detail_easyar_TextureId__typeName_mCB452F6133A09F14F50FDFD0054A3A69145497F9 (void);
// 0x00000347 System.Void easyar.Detail::easyar_ListOfVec3F__ctor(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfVec3F__ctor_m208AA39916D6306C7A1D13EFE918FF983F0017C4 (void);
// 0x00000348 System.Void easyar.Detail::easyar_ListOfVec3F__dtor(System.IntPtr)
extern void Detail_easyar_ListOfVec3F__dtor_mD96C74E4AADDDC9C8546A071980D587A79AFCEB1 (void);
// 0x00000349 System.Void easyar.Detail::easyar_ListOfVec3F_copy(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfVec3F_copy_m0C5220942D553A6DA0B65B19EBE664BFB35475FB (void);
// 0x0000034A System.Int32 easyar.Detail::easyar_ListOfVec3F_size(System.IntPtr)
extern void Detail_easyar_ListOfVec3F_size_m5140DB61BD2C43887EAE7E336B573ADD335F9D5F (void);
// 0x0000034B easyar.Vec3F easyar.Detail::easyar_ListOfVec3F_at(System.IntPtr,System.Int32)
extern void Detail_easyar_ListOfVec3F_at_m45853C0A7829D9B210287C182976A701FBDFB96F (void);
// 0x0000034C System.Void easyar.Detail::easyar_ListOfTargetInstance__ctor(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfTargetInstance__ctor_mE802AD303EC8450099963AA439FE2A186DFF679D (void);
// 0x0000034D System.Void easyar.Detail::easyar_ListOfTargetInstance__dtor(System.IntPtr)
extern void Detail_easyar_ListOfTargetInstance__dtor_m4E09F18CC8763D8439AE3033C73F722535B2864C (void);
// 0x0000034E System.Void easyar.Detail::easyar_ListOfTargetInstance_copy(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfTargetInstance_copy_m1E804B6A7B63C091A09ED70548C3380CB36DD59A (void);
// 0x0000034F System.Int32 easyar.Detail::easyar_ListOfTargetInstance_size(System.IntPtr)
extern void Detail_easyar_ListOfTargetInstance_size_m455367BA9B4EA9033FE49BC85A06AD6F804C039C (void);
// 0x00000350 System.IntPtr easyar.Detail::easyar_ListOfTargetInstance_at(System.IntPtr,System.Int32)
extern void Detail_easyar_ListOfTargetInstance_at_mA3F2F3CD0DC4A32B67AB28258580179BA4A31EDE (void);
// 0x00000351 System.Void easyar.Detail::easyar_ListOfOptionalOfFrameFilterResult__ctor(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfOptionalOfFrameFilterResult__ctor_mBF5DAD81C63BC8A1905F515A0A08F13D72949466 (void);
// 0x00000352 System.Void easyar.Detail::easyar_ListOfOptionalOfFrameFilterResult__dtor(System.IntPtr)
extern void Detail_easyar_ListOfOptionalOfFrameFilterResult__dtor_m4060C70E10D3F52C23C1AB5165227CCD3E077CB9 (void);
// 0x00000353 System.Void easyar.Detail::easyar_ListOfOptionalOfFrameFilterResult_copy(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfOptionalOfFrameFilterResult_copy_mA399AE55ADFF1C3BA45E0903861E25D1FF1C1600 (void);
// 0x00000354 System.Int32 easyar.Detail::easyar_ListOfOptionalOfFrameFilterResult_size(System.IntPtr)
extern void Detail_easyar_ListOfOptionalOfFrameFilterResult_size_m2608B475B5302D0DEEC0E172A45BC6F70D5E52BF (void);
// 0x00000355 easyar.Detail/OptionalOfFrameFilterResult easyar.Detail::easyar_ListOfOptionalOfFrameFilterResult_at(System.IntPtr,System.Int32)
extern void Detail_easyar_ListOfOptionalOfFrameFilterResult_at_m9FA024208C78175EA09194FADEC6B8D9F69FACC3 (void);
// 0x00000356 System.Void easyar.Detail::easyar_ListOfTarget__ctor(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfTarget__ctor_mADD03B4FB4AA5A1A3C54A9C3DD5B0D0F4F4BB7A2 (void);
// 0x00000357 System.Void easyar.Detail::easyar_ListOfTarget__dtor(System.IntPtr)
extern void Detail_easyar_ListOfTarget__dtor_mEC0C99C5EB317C5C55A29AB90DE3AE52545E7B5A (void);
// 0x00000358 System.Void easyar.Detail::easyar_ListOfTarget_copy(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfTarget_copy_mF20A09B8BC01C621246AFDCAD29B637699B1F3D4 (void);
// 0x00000359 System.Int32 easyar.Detail::easyar_ListOfTarget_size(System.IntPtr)
extern void Detail_easyar_ListOfTarget_size_m0A293FCD1184DC9A700EB9B47D8AD00F562B55C8 (void);
// 0x0000035A System.IntPtr easyar.Detail::easyar_ListOfTarget_at(System.IntPtr,System.Int32)
extern void Detail_easyar_ListOfTarget_at_m5E034C9665D389CE13DC8D784470CA62FFBE9803 (void);
// 0x0000035B System.Void easyar.Detail::easyar_ListOfImage__ctor(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfImage__ctor_m18F81AD3F3E783983E1CE031417B9055BAC17794 (void);
// 0x0000035C System.Void easyar.Detail::easyar_ListOfImage__dtor(System.IntPtr)
extern void Detail_easyar_ListOfImage__dtor_m1325BED170B1F85FEEF02452FDCCA854E7131BAE (void);
// 0x0000035D System.Void easyar.Detail::easyar_ListOfImage_copy(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfImage_copy_m4758F2547D592FA865733107E6CB4B2C84935115 (void);
// 0x0000035E System.Int32 easyar.Detail::easyar_ListOfImage_size(System.IntPtr)
extern void Detail_easyar_ListOfImage_size_mA8BC0B77623E68B647E3D607A6E2ECC9ABB2CA40 (void);
// 0x0000035F System.IntPtr easyar.Detail::easyar_ListOfImage_at(System.IntPtr,System.Int32)
extern void Detail_easyar_ListOfImage_at_m9C02A5A9B5FFF80701D658034A9427F94AAD099D (void);
// 0x00000360 System.Void easyar.Detail::easyar_ListOfBlockInfo__ctor(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfBlockInfo__ctor_m7102CA58A465879D2C65993202F35D854DA6BDFC (void);
// 0x00000361 System.Void easyar.Detail::easyar_ListOfBlockInfo__dtor(System.IntPtr)
extern void Detail_easyar_ListOfBlockInfo__dtor_m9EFADB57FBA215F7AA169E160D9764BB3E041CD0 (void);
// 0x00000362 System.Void easyar.Detail::easyar_ListOfBlockInfo_copy(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfBlockInfo_copy_m7BEA45087689C73457B99CDEC423A8E263AFB751 (void);
// 0x00000363 System.Int32 easyar.Detail::easyar_ListOfBlockInfo_size(System.IntPtr)
extern void Detail_easyar_ListOfBlockInfo_size_m0401FBBD16C69BB6855778FE317EFD5204F0B5BB (void);
// 0x00000364 easyar.BlockInfo easyar.Detail::easyar_ListOfBlockInfo_at(System.IntPtr,System.Int32)
extern void Detail_easyar_ListOfBlockInfo_at_m35F302F45F102C40FAC0E6843CDF07F1FAA90F51 (void);
// 0x00000365 System.Void easyar.Detail::easyar_ListOfPlaneData__ctor(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfPlaneData__ctor_m8D38E177592B910002CC0C8F663334E38530EFF7 (void);
// 0x00000366 System.Void easyar.Detail::easyar_ListOfPlaneData__dtor(System.IntPtr)
extern void Detail_easyar_ListOfPlaneData__dtor_mD085D3967B86ED76F284856BBA64CCC2BB22C790 (void);
// 0x00000367 System.Void easyar.Detail::easyar_ListOfPlaneData_copy(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfPlaneData_copy_m24D45BD45728185CE0E2D05B88CF2E18FCDE2C81 (void);
// 0x00000368 System.Int32 easyar.Detail::easyar_ListOfPlaneData_size(System.IntPtr)
extern void Detail_easyar_ListOfPlaneData_size_m71083CD736D629DBD7F2B346C77948E64341E1B4 (void);
// 0x00000369 System.IntPtr easyar.Detail::easyar_ListOfPlaneData_at(System.IntPtr,System.Int32)
extern void Detail_easyar_ListOfPlaneData_at_m668ADDB5A436FB8D02320150EF81680C501F4AC9 (void);
// 0x0000036A System.Void easyar.Detail::easyar_ListOfOutputFrame__ctor(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfOutputFrame__ctor_mDB981943B148F4903059E8744E4B633AE972C2B3 (void);
// 0x0000036B System.Void easyar.Detail::easyar_ListOfOutputFrame__dtor(System.IntPtr)
extern void Detail_easyar_ListOfOutputFrame__dtor_mC530268C5AC9EB0551810F7E0828C4AFCBF5D1BF (void);
// 0x0000036C System.Void easyar.Detail::easyar_ListOfOutputFrame_copy(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfOutputFrame_copy_mA2045B036A943E3E174AD085C50E93930B165F87 (void);
// 0x0000036D System.Int32 easyar.Detail::easyar_ListOfOutputFrame_size(System.IntPtr)
extern void Detail_easyar_ListOfOutputFrame_size_mFF9B7D1B62CBF8F0DF6BA856C2AA6A3D0E66B06E (void);
// 0x0000036E System.IntPtr easyar.Detail::easyar_ListOfOutputFrame_at(System.IntPtr,System.Int32)
extern void Detail_easyar_ListOfOutputFrame_at_mADC5037975B76D149A95A42CE4AE249E23B2749A (void);
// 0x0000036F System.IntPtr easyar.Detail::String_to_c(easyar.Detail/AutoRelease,System.String)
extern void Detail_String_to_c_m1E6A781AFCBC919C5A6A7FB3430C60DCE08FA04B (void);
// 0x00000370 System.IntPtr easyar.Detail::String_to_c_inner(System.String)
extern void Detail_String_to_c_inner_m338944A05970542DD747E0DE3E99D53EB3C7CF83 (void);
// 0x00000371 System.String easyar.Detail::String_from_c(easyar.Detail/AutoRelease,System.IntPtr)
extern void Detail_String_from_c_m598C945D4B0987313DEB9CC0810324968E252552 (void);
// 0x00000372 System.String easyar.Detail::String_from_cstring(System.IntPtr)
extern void Detail_String_from_cstring_m41461029B66E7B609106EEB30475D59BEE46DA75 (void);
// 0x00000373 T easyar.Detail::Object_from_c(System.IntPtr,System.Func`2<System.IntPtr,System.IntPtr>)
// 0x00000374 TValue easyar.Detail::map(TKey,System.Func`2<TKey,TValue>)
// 0x00000375 System.Void easyar.Detail::FunctorOfVoid_func(System.IntPtr,System.IntPtr&)
extern void Detail_FunctorOfVoid_func_m8233FE906FAE6473609404B722CF66412E5EFB48 (void);
// 0x00000376 System.Void easyar.Detail::FunctorOfVoid_destroy(System.IntPtr)
extern void Detail_FunctorOfVoid_destroy_m9A583576B55465A17C01F9B74E416B531702AB80 (void);
// 0x00000377 easyar.Detail/FunctorOfVoid easyar.Detail::FunctorOfVoid_to_c(System.Action)
extern void Detail_FunctorOfVoid_to_c_mC5613CA270E64E7C57C51C13A7CB36FB52996427 (void);
// 0x00000378 System.IntPtr easyar.Detail::ListOfVec3F_to_c(easyar.Detail/AutoRelease,System.Collections.Generic.List`1<easyar.Vec3F>)
extern void Detail_ListOfVec3F_to_c_mF8E4DB1661D8CE1ED5BC20DDED1FCD2B4D50FAA8 (void);
// 0x00000379 System.Collections.Generic.List`1<easyar.Vec3F> easyar.Detail::ListOfVec3F_from_c(easyar.Detail/AutoRelease,System.IntPtr)
extern void Detail_ListOfVec3F_from_c_m2D780A2EDE5838EA5755F1D6187AB78F420DED5B (void);
// 0x0000037A System.IntPtr easyar.Detail::ListOfTargetInstance_to_c(easyar.Detail/AutoRelease,System.Collections.Generic.List`1<easyar.TargetInstance>)
extern void Detail_ListOfTargetInstance_to_c_m7CC734436F98C3A7F02BC0E8EE232F0E015EF58B (void);
// 0x0000037B System.Collections.Generic.List`1<easyar.TargetInstance> easyar.Detail::ListOfTargetInstance_from_c(easyar.Detail/AutoRelease,System.IntPtr)
extern void Detail_ListOfTargetInstance_from_c_m2AF1CCF8D8F1A17C759520947A0FF7A7E5BE43A1 (void);
// 0x0000037C System.IntPtr easyar.Detail::ListOfOptionalOfFrameFilterResult_to_c(easyar.Detail/AutoRelease,System.Collections.Generic.List`1<easyar.Optional`1<easyar.FrameFilterResult>>)
extern void Detail_ListOfOptionalOfFrameFilterResult_to_c_mD353FDBE04412B0F818612BF3E95C8239CDF0F67 (void);
// 0x0000037D System.Collections.Generic.List`1<easyar.Optional`1<easyar.FrameFilterResult>> easyar.Detail::ListOfOptionalOfFrameFilterResult_from_c(easyar.Detail/AutoRelease,System.IntPtr)
extern void Detail_ListOfOptionalOfFrameFilterResult_from_c_mA2DA762569EFBE14680F8CB7ACAAEDDDE2C15AB4 (void);
// 0x0000037E System.Void easyar.Detail::FunctorOfVoidFromOutputFrame_func(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_FunctorOfVoidFromOutputFrame_func_m142DA320BDD782F0665F95929A1FAF518AAB0B80 (void);
// 0x0000037F System.Void easyar.Detail::FunctorOfVoidFromOutputFrame_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromOutputFrame_destroy_m39E6790C6700E68EEB6EF662B32E84E8150EE773 (void);
// 0x00000380 easyar.Detail/FunctorOfVoidFromOutputFrame easyar.Detail::FunctorOfVoidFromOutputFrame_to_c(System.Action`1<easyar.OutputFrame>)
extern void Detail_FunctorOfVoidFromOutputFrame_to_c_m1FA7F0AE62C3404FDF4D9B79CB4A076A09F691FC (void);
// 0x00000381 System.Void easyar.Detail::FunctorOfVoidFromTargetAndBool_func(System.IntPtr,System.IntPtr,System.Boolean,System.IntPtr&)
extern void Detail_FunctorOfVoidFromTargetAndBool_func_m4F4E191A4A3E8C8B1C93A9A83F773843E947D3E0 (void);
// 0x00000382 System.Void easyar.Detail::FunctorOfVoidFromTargetAndBool_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromTargetAndBool_destroy_m407203B6D0165B9CBB93CDC394623D25D1394C3E (void);
// 0x00000383 easyar.Detail/FunctorOfVoidFromTargetAndBool easyar.Detail::FunctorOfVoidFromTargetAndBool_to_c(System.Action`2<easyar.Target,System.Boolean>)
extern void Detail_FunctorOfVoidFromTargetAndBool_to_c_mA09F2DBB0D1DA2A1B825161B8FD24FF34548DEAE (void);
// 0x00000384 System.IntPtr easyar.Detail::ListOfTarget_to_c(easyar.Detail/AutoRelease,System.Collections.Generic.List`1<easyar.Target>)
extern void Detail_ListOfTarget_to_c_m0D5BFEE2B4FF3C1589D7F02C660542A9F41D75E3 (void);
// 0x00000385 System.Collections.Generic.List`1<easyar.Target> easyar.Detail::ListOfTarget_from_c(easyar.Detail/AutoRelease,System.IntPtr)
extern void Detail_ListOfTarget_from_c_m84398A60270F2855D753A0B2D48FFDE9E0E1F621 (void);
// 0x00000386 System.Void easyar.Detail::FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_func(System.IntPtr,easyar.CalibrationDownloadStatus,easyar.Detail/OptionalOfString,System.IntPtr&)
extern void Detail_FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_func_m47D3C6B1C3FA746A6D370FBC7B1D517706FB3EFE (void);
// 0x00000387 System.Void easyar.Detail::FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_destroy_m44D24357597F4A3861061A606C66CAC57CBDE0D8 (void);
// 0x00000388 easyar.Detail/FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString easyar.Detail::FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_to_c(System.Action`2<easyar.CalibrationDownloadStatus,easyar.Optional`1<System.String>>)
extern void Detail_FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_to_c_m37E8441A163CA939A0656B244F46D4874B5270DA (void);
// 0x00000389 System.IntPtr easyar.Detail::ListOfImage_to_c(easyar.Detail/AutoRelease,System.Collections.Generic.List`1<easyar.Image>)
extern void Detail_ListOfImage_to_c_m7DFB79A1964C7797E749B4D88CD57BBBA02D01DC (void);
// 0x0000038A System.Collections.Generic.List`1<easyar.Image> easyar.Detail::ListOfImage_from_c(easyar.Detail/AutoRelease,System.IntPtr)
extern void Detail_ListOfImage_from_c_m4AD4BB1192546842FF2B370A37D7A6EB77FBB60A (void);
// 0x0000038B System.Void easyar.Detail::FunctorOfVoidFromCloudRecognizationResult_func(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_FunctorOfVoidFromCloudRecognizationResult_func_m631246C2873CA0786043DC8226B7A1F69BAF973D (void);
// 0x0000038C System.Void easyar.Detail::FunctorOfVoidFromCloudRecognizationResult_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromCloudRecognizationResult_destroy_mBF60D4AFDA72F76BFBE41B77BBAACB66D22FB251 (void);
// 0x0000038D easyar.Detail/FunctorOfVoidFromCloudRecognizationResult easyar.Detail::FunctorOfVoidFromCloudRecognizationResult_to_c(System.Action`1<easyar.CloudRecognizationResult>)
extern void Detail_FunctorOfVoidFromCloudRecognizationResult_to_c_mF394000F6512A843638766A3E872E063C9EABA8A (void);
// 0x0000038E System.IntPtr easyar.Detail::ListOfBlockInfo_to_c(easyar.Detail/AutoRelease,System.Collections.Generic.List`1<easyar.BlockInfo>)
extern void Detail_ListOfBlockInfo_to_c_m8AE703276F8D6E490385821E9F12A5C127EFD4C6 (void);
// 0x0000038F System.Collections.Generic.List`1<easyar.BlockInfo> easyar.Detail::ListOfBlockInfo_from_c(easyar.Detail/AutoRelease,System.IntPtr)
extern void Detail_ListOfBlockInfo_from_c_mC33AB4B618886BC4852E2FDB4ED326F215CA7553 (void);
// 0x00000390 System.Void easyar.Detail::FunctorOfVoidFromInputFrame_func(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_FunctorOfVoidFromInputFrame_func_mA15F0CAA1EFCDBBE96BA6D3A17F1AE49FFB6443F (void);
// 0x00000391 System.Void easyar.Detail::FunctorOfVoidFromInputFrame_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromInputFrame_destroy_mBC40B788EC5FA01C081A8B8F29DD83BD7C04BBE0 (void);
// 0x00000392 easyar.Detail/FunctorOfVoidFromInputFrame easyar.Detail::FunctorOfVoidFromInputFrame_to_c(System.Action`1<easyar.InputFrame>)
extern void Detail_FunctorOfVoidFromInputFrame_to_c_mE09D592C78FB2450897DBE82D3F71C797045EC57 (void);
// 0x00000393 System.Void easyar.Detail::FunctorOfVoidFromCameraState_func(System.IntPtr,easyar.CameraState,System.IntPtr&)
extern void Detail_FunctorOfVoidFromCameraState_func_mC7E9F14253350E76D81DD852C31479AE6FA97541 (void);
// 0x00000394 System.Void easyar.Detail::FunctorOfVoidFromCameraState_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromCameraState_destroy_mFF1D0D74D6DDC00A03B9F68ABD23B93BBCB212B2 (void);
// 0x00000395 easyar.Detail/FunctorOfVoidFromCameraState easyar.Detail::FunctorOfVoidFromCameraState_to_c(System.Action`1<easyar.CameraState>)
extern void Detail_FunctorOfVoidFromCameraState_to_c_m48184C444F8707ED832ABB4A5958E4DBAEA2DE52 (void);
// 0x00000396 System.Void easyar.Detail::FunctorOfVoidFromPermissionStatusAndString_func(System.IntPtr,easyar.PermissionStatus,System.IntPtr,System.IntPtr&)
extern void Detail_FunctorOfVoidFromPermissionStatusAndString_func_m8D9D32B6E616DCFD443DE289BEE2A1A9E10D63B3 (void);
// 0x00000397 System.Void easyar.Detail::FunctorOfVoidFromPermissionStatusAndString_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromPermissionStatusAndString_destroy_m781B495D25D988C89184294BD62865D9E801969D (void);
// 0x00000398 easyar.Detail/FunctorOfVoidFromPermissionStatusAndString easyar.Detail::FunctorOfVoidFromPermissionStatusAndString_to_c(System.Action`2<easyar.PermissionStatus,System.String>)
extern void Detail_FunctorOfVoidFromPermissionStatusAndString_to_c_m090CF72FE3106914DE4F79C2B4406956AE9A680A (void);
// 0x00000399 System.Void easyar.Detail::FunctorOfVoidFromLogLevelAndString_func(System.IntPtr,easyar.LogLevel,System.IntPtr,System.IntPtr&)
extern void Detail_FunctorOfVoidFromLogLevelAndString_func_m9A78157C61E95CA37E54667EDB501049BE406D6A (void);
// 0x0000039A System.Void easyar.Detail::FunctorOfVoidFromLogLevelAndString_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromLogLevelAndString_destroy_mF22BD5934E396FB1042F8B0E27B54DBE318FCBFE (void);
// 0x0000039B easyar.Detail/FunctorOfVoidFromLogLevelAndString easyar.Detail::FunctorOfVoidFromLogLevelAndString_to_c(System.Action`2<easyar.LogLevel,System.String>)
extern void Detail_FunctorOfVoidFromLogLevelAndString_to_c_m69294E9D33E5CE6B6527652B4DBE3F5F697A1010 (void);
// 0x0000039C System.Void easyar.Detail::FunctorOfVoidFromRecordStatusAndString_func(System.IntPtr,easyar.RecordStatus,System.IntPtr,System.IntPtr&)
extern void Detail_FunctorOfVoidFromRecordStatusAndString_func_mA45DA83CDE8ACEB8F46865D1B86A518290177EBA (void);
// 0x0000039D System.Void easyar.Detail::FunctorOfVoidFromRecordStatusAndString_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromRecordStatusAndString_destroy_mA47A5359B41B4FD8321FA5CC075E76AD7ABBBCD1 (void);
// 0x0000039E easyar.Detail/FunctorOfVoidFromRecordStatusAndString easyar.Detail::FunctorOfVoidFromRecordStatusAndString_to_c(System.Action`2<easyar.RecordStatus,System.String>)
extern void Detail_FunctorOfVoidFromRecordStatusAndString_to_c_mFEEEB691A291F28EA0A514233B9C1878A35353AA (void);
// 0x0000039F System.IntPtr easyar.Detail::ListOfPlaneData_to_c(easyar.Detail/AutoRelease,System.Collections.Generic.List`1<easyar.PlaneData>)
extern void Detail_ListOfPlaneData_to_c_m956FFABFAC8E0F822137F46242637F3FA0BC161A (void);
// 0x000003A0 System.Collections.Generic.List`1<easyar.PlaneData> easyar.Detail::ListOfPlaneData_from_c(easyar.Detail/AutoRelease,System.IntPtr)
extern void Detail_ListOfPlaneData_from_c_mABE69D1DF9912A0F5F41C2EBAE6E632C4E7FD1E1 (void);
// 0x000003A1 System.Void easyar.Detail::FunctorOfVoidFromBool_func(System.IntPtr,System.Boolean,System.IntPtr&)
extern void Detail_FunctorOfVoidFromBool_func_m18B5057D02371A6A12534E44D51E79565365602A (void);
// 0x000003A2 System.Void easyar.Detail::FunctorOfVoidFromBool_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromBool_destroy_m3D313F4C383413AAD2FCB52140036EA67F40B985 (void);
// 0x000003A3 easyar.Detail/FunctorOfVoidFromBool easyar.Detail::FunctorOfVoidFromBool_to_c(System.Action`1<System.Boolean>)
extern void Detail_FunctorOfVoidFromBool_to_c_m3F8E9FD5F5A57F76366EF25F9995C785216EC7FA (void);
// 0x000003A4 System.Void easyar.Detail::FunctorOfVoidFromBoolAndStringAndString_func(System.IntPtr,System.Boolean,System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_FunctorOfVoidFromBoolAndStringAndString_func_m32AF4156D52F9B1AEA2A81D03E596F56609DF977 (void);
// 0x000003A5 System.Void easyar.Detail::FunctorOfVoidFromBoolAndStringAndString_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromBoolAndStringAndString_destroy_mA6C753EE55047102FBA63F1ABC45CE9D2B516010 (void);
// 0x000003A6 easyar.Detail/FunctorOfVoidFromBoolAndStringAndString easyar.Detail::FunctorOfVoidFromBoolAndStringAndString_to_c(System.Action`3<System.Boolean,System.String,System.String>)
extern void Detail_FunctorOfVoidFromBoolAndStringAndString_to_c_mA32E423FE6EC406784F0CF931C765DD7316E0E91 (void);
// 0x000003A7 System.Void easyar.Detail::FunctorOfVoidFromBoolAndString_func(System.IntPtr,System.Boolean,System.IntPtr,System.IntPtr&)
extern void Detail_FunctorOfVoidFromBoolAndString_func_m4786C9B24241EBD28AAF555E9CFCB97344EFB8CB (void);
// 0x000003A8 System.Void easyar.Detail::FunctorOfVoidFromBoolAndString_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromBoolAndString_destroy_mDC79451BFE0F528760772F0A015FBC035E6E023E (void);
// 0x000003A9 easyar.Detail/FunctorOfVoidFromBoolAndString easyar.Detail::FunctorOfVoidFromBoolAndString_to_c(System.Action`2<System.Boolean,System.String>)
extern void Detail_FunctorOfVoidFromBoolAndString_to_c_mF617AB1474F656AD940D507F3D60A75B80D9C47E (void);
// 0x000003AA System.Void easyar.Detail::FunctorOfVoidFromVideoStatus_func(System.IntPtr,easyar.VideoStatus,System.IntPtr&)
extern void Detail_FunctorOfVoidFromVideoStatus_func_mB7B7FD5B5858F436F3466A1919AF72453E7BE34F (void);
// 0x000003AB System.Void easyar.Detail::FunctorOfVoidFromVideoStatus_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromVideoStatus_destroy_mC318C3913E76C1CF3DC21561E52A312033C580E4 (void);
// 0x000003AC easyar.Detail/FunctorOfVoidFromVideoStatus easyar.Detail::FunctorOfVoidFromVideoStatus_to_c(System.Action`1<easyar.VideoStatus>)
extern void Detail_FunctorOfVoidFromVideoStatus_to_c_mDC754888D7AD6794FDDC572C48E824417E33329E (void);
// 0x000003AD System.Void easyar.Detail::FunctorOfVoidFromFeedbackFrame_func(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_FunctorOfVoidFromFeedbackFrame_func_mF3A0BCA98C72331241FAC3A693309EBC3570DAD7 (void);
// 0x000003AE System.Void easyar.Detail::FunctorOfVoidFromFeedbackFrame_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromFeedbackFrame_destroy_mE0489B5AF6DB4645A89B1AA2C5E728AA2085DA06 (void);
// 0x000003AF easyar.Detail/FunctorOfVoidFromFeedbackFrame easyar.Detail::FunctorOfVoidFromFeedbackFrame_to_c(System.Action`1<easyar.FeedbackFrame>)
extern void Detail_FunctorOfVoidFromFeedbackFrame_to_c_m4654D6C9873EA88DC10913ECE80F99077EEF36E7 (void);
// 0x000003B0 System.Void easyar.Detail::FunctorOfOutputFrameFromListOfOutputFrame_func(System.IntPtr,System.IntPtr,System.IntPtr&,System.IntPtr&)
extern void Detail_FunctorOfOutputFrameFromListOfOutputFrame_func_m728DEA484FEF9A32AF0EA5C985684C3A923B6E93 (void);
// 0x000003B1 System.Void easyar.Detail::FunctorOfOutputFrameFromListOfOutputFrame_destroy(System.IntPtr)
extern void Detail_FunctorOfOutputFrameFromListOfOutputFrame_destroy_m3213152A87BBAB55CB406BB9D08FB45301A33151 (void);
// 0x000003B2 easyar.Detail/FunctorOfOutputFrameFromListOfOutputFrame easyar.Detail::FunctorOfOutputFrameFromListOfOutputFrame_to_c(System.Func`2<System.Collections.Generic.List`1<easyar.OutputFrame>,easyar.OutputFrame>)
extern void Detail_FunctorOfOutputFrameFromListOfOutputFrame_to_c_mCBD0AE3E1527F9689322688D72E736FFE864F19F (void);
// 0x000003B3 System.IntPtr easyar.Detail::ListOfOutputFrame_to_c(easyar.Detail/AutoRelease,System.Collections.Generic.List`1<easyar.OutputFrame>)
extern void Detail_ListOfOutputFrame_to_c_m628E834221C655F32D06A3BBF65F958B1539C06C (void);
// 0x000003B4 System.Collections.Generic.List`1<easyar.OutputFrame> easyar.Detail::ListOfOutputFrame_from_c(easyar.Detail/AutoRelease,System.IntPtr)
extern void Detail_ListOfOutputFrame_from_c_m3D19C2738649FBCCF73E2BDB8F4BA683E2199C22 (void);
// 0x000003B5 System.Void easyar.Detail::.cctor()
extern void Detail__cctor_m0D326FDA27F767F6166422FE775C92BACE73668B (void);
// 0x000003B6 System.Void easyar.Detail/AutoRelease::Add(System.Action)
extern void AutoRelease_Add_m7A212059DFF41BE1EF6D05BEB95B52EF57C7B9F2 (void);
// 0x000003B7 T easyar.Detail/AutoRelease::Add(T,System.Action`1<T>)
// 0x000003B8 System.Void easyar.Detail/AutoRelease::Dispose()
extern void AutoRelease_Dispose_mE65C3AD323041685F52968097252346E245B058B (void);
// 0x000003B9 System.Void easyar.Detail/AutoRelease::.ctor()
extern void AutoRelease__ctor_m980DEE2E4F4111B44F7DB7EAD221C3F2F7CBB6B7 (void);
// 0x000003BA System.Void easyar.Detail/AutoRelease/<>c__DisplayClass2_0`1::.ctor()
// 0x000003BB System.Void easyar.Detail/AutoRelease/<>c__DisplayClass2_0`1::<Add>b__0()
// 0x000003BC System.Boolean easyar.Detail/OptionalOfBuffer::get_has_value()
extern void OptionalOfBuffer_get_has_value_mD6F26A29092013167861EE3E660476683A64529C (void);
// 0x000003BD System.Void easyar.Detail/OptionalOfBuffer::set_has_value(System.Boolean)
extern void OptionalOfBuffer_set_has_value_mDC4547AE9C5D5CB894F23AB1AC16BB2B758D3CC4 (void);
// 0x000003BE System.Void easyar.Detail/FunctorOfVoid/FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m1D45E2663E4A37D350606702178E972C22504488 (void);
// 0x000003BF System.Void easyar.Detail/FunctorOfVoid/FunctionDelegate::Invoke(System.IntPtr,System.IntPtr&)
extern void FunctionDelegate_Invoke_m312678BEED2079103BF1FA550DCC3F140C93ABA2 (void);
// 0x000003C0 System.IAsyncResult easyar.Detail/FunctorOfVoid/FunctionDelegate::BeginInvoke(System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_mA05E0CF1A0E17FFE7F7C261F84C87DF741268287 (void);
// 0x000003C1 System.Void easyar.Detail/FunctorOfVoid/FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_mC4D65D2A67724FCCEA48222E86707B798BDA5A86 (void);
// 0x000003C2 System.Void easyar.Detail/FunctorOfVoid/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_mEB511B64443B787AA6736EF3BEFC1FD6EE475DBE (void);
// 0x000003C3 System.Void easyar.Detail/FunctorOfVoid/DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m2D13CC8DFB145CE47D614842D9F878ED87F563CC (void);
// 0x000003C4 System.IAsyncResult easyar.Detail/FunctorOfVoid/DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_m5020302360D3815B930AE1204E78072E0DE5F5AB (void);
// 0x000003C5 System.Void easyar.Detail/FunctorOfVoid/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_m700981CCA5D5A2D881CDF1885D9A55F8B3C3E716 (void);
// 0x000003C6 System.Boolean easyar.Detail/OptionalOfObjectTarget::get_has_value()
extern void OptionalOfObjectTarget_get_has_value_mF804B145E14437EA95BD0A5664AA16887425D034 (void);
// 0x000003C7 System.Void easyar.Detail/OptionalOfObjectTarget::set_has_value(System.Boolean)
extern void OptionalOfObjectTarget_set_has_value_mF9205E39066CE8464514E8A917412EE576AAE862 (void);
// 0x000003C8 System.Boolean easyar.Detail/OptionalOfTarget::get_has_value()
extern void OptionalOfTarget_get_has_value_m54BFBDD1FA4C03B32E0254203C95AB89471226C8 (void);
// 0x000003C9 System.Void easyar.Detail/OptionalOfTarget::set_has_value(System.Boolean)
extern void OptionalOfTarget_set_has_value_mD53D8270DE95BE1FE4DB151129C9654A6DAD4BCB (void);
// 0x000003CA System.Boolean easyar.Detail/OptionalOfOutputFrame::get_has_value()
extern void OptionalOfOutputFrame_get_has_value_m16750E6F8AC1CDD2105035942148EC3F17A2731C (void);
// 0x000003CB System.Void easyar.Detail/OptionalOfOutputFrame::set_has_value(System.Boolean)
extern void OptionalOfOutputFrame_set_has_value_m0ED852A17431EA3157B7D32E3579B90F9DB11AA8 (void);
// 0x000003CC System.Boolean easyar.Detail/OptionalOfFrameFilterResult::get_has_value()
extern void OptionalOfFrameFilterResult_get_has_value_m0B5B694A1819769F52A8B8B676DD8E7A504C4A1B (void);
// 0x000003CD System.Void easyar.Detail/OptionalOfFrameFilterResult::set_has_value(System.Boolean)
extern void OptionalOfFrameFilterResult_set_has_value_mA6CDF1D438A978182E3D705DABCFB18A46835EC8 (void);
// 0x000003CE System.Boolean easyar.Detail/OptionalOfFunctorOfVoidFromOutputFrame::get_has_value()
extern void OptionalOfFunctorOfVoidFromOutputFrame_get_has_value_mAFC7BA70B0524FC0FFE85A533C0BD64F6C07E41A (void);
// 0x000003CF System.Void easyar.Detail/OptionalOfFunctorOfVoidFromOutputFrame::set_has_value(System.Boolean)
extern void OptionalOfFunctorOfVoidFromOutputFrame_set_has_value_mDAB3D368167065A00CA62736E0677FBC5A4D8540 (void);
// 0x000003D0 System.Void easyar.Detail/FunctorOfVoidFromOutputFrame/FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m66F61E3190D4E36FD0C3C0707BFD957529213AF8 (void);
// 0x000003D1 System.Void easyar.Detail/FunctorOfVoidFromOutputFrame/FunctionDelegate::Invoke(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void FunctionDelegate_Invoke_mB7D8367BEC0AFE1316E79DC15A680A36FB82940B (void);
// 0x000003D2 System.IAsyncResult easyar.Detail/FunctorOfVoidFromOutputFrame/FunctionDelegate::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_m569BE18FCE5C0B0AE5CB72A1062100AC6108E43E (void);
// 0x000003D3 System.Void easyar.Detail/FunctorOfVoidFromOutputFrame/FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_m7CC0900AD87A0341B02A3482FEEF055973DA18C7 (void);
// 0x000003D4 System.Void easyar.Detail/FunctorOfVoidFromOutputFrame/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_mC13B534CE440DEE47C2E37906F8317A6B10343D4 (void);
// 0x000003D5 System.Void easyar.Detail/FunctorOfVoidFromOutputFrame/DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m832126404B59A946EB12979D657AE9DC3BB1A833 (void);
// 0x000003D6 System.IAsyncResult easyar.Detail/FunctorOfVoidFromOutputFrame/DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_mAB9C3F695EB5221E67C04035619A538EBE8306A8 (void);
// 0x000003D7 System.Void easyar.Detail/FunctorOfVoidFromOutputFrame/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_mFA27FD6FB8129F4FDD1C55E0845D13FAA66D7C0C (void);
// 0x000003D8 System.Void easyar.Detail/FunctorOfVoidFromTargetAndBool/FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_mAB19B3CEB58FF908E87C397A326F2697E6726AD5 (void);
// 0x000003D9 System.Void easyar.Detail/FunctorOfVoidFromTargetAndBool/FunctionDelegate::Invoke(System.IntPtr,System.IntPtr,System.Boolean,System.IntPtr&)
extern void FunctionDelegate_Invoke_m5BD1865D5105B750654908BB965C68ACD6A6AFA0 (void);
// 0x000003DA System.IAsyncResult easyar.Detail/FunctorOfVoidFromTargetAndBool/FunctionDelegate::BeginInvoke(System.IntPtr,System.IntPtr,System.Boolean,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_m1F486631168CC5C442EB408540E7D983BCBBC782 (void);
// 0x000003DB System.Void easyar.Detail/FunctorOfVoidFromTargetAndBool/FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_m0EA23CDF2296BAAC4D92D04E0BEC259851A48BDB (void);
// 0x000003DC System.Void easyar.Detail/FunctorOfVoidFromTargetAndBool/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m0C265E4B43D43EF11071BB6E13F5CB7D4C7FABFB (void);
// 0x000003DD System.Void easyar.Detail/FunctorOfVoidFromTargetAndBool/DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m36AED1864B294C2113E7CEEB15F7424A75EC67A3 (void);
// 0x000003DE System.IAsyncResult easyar.Detail/FunctorOfVoidFromTargetAndBool/DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_m98D795559000E47969925DBB8650B430A3BFBAFC (void);
// 0x000003DF System.Void easyar.Detail/FunctorOfVoidFromTargetAndBool/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_mB2DF1D58ADF47846B4D2886FC01F4610ED86AF28 (void);
// 0x000003E0 System.Void easyar.Detail/FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString/FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m4A872BF58089EA72EAF3DE53DAFDAC0BCD82FFA5 (void);
// 0x000003E1 System.Void easyar.Detail/FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString/FunctionDelegate::Invoke(System.IntPtr,easyar.CalibrationDownloadStatus,easyar.Detail/OptionalOfString,System.IntPtr&)
extern void FunctionDelegate_Invoke_m166C49F173919D60AD282CBC1AF82956F9740473 (void);
// 0x000003E2 System.IAsyncResult easyar.Detail/FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString/FunctionDelegate::BeginInvoke(System.IntPtr,easyar.CalibrationDownloadStatus,easyar.Detail/OptionalOfString,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_mEFE11253325F7453FDE38F52967CD32CAE5B2D21 (void);
// 0x000003E3 System.Void easyar.Detail/FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString/FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_m49F69DAB78FAB5E677C7D57D0B7EAA35C8378330 (void);
// 0x000003E4 System.Void easyar.Detail/FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m6ED8161195640DFFB8E46F5FA6289B3A05509D9A (void);
// 0x000003E5 System.Void easyar.Detail/FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString/DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m36D93B6620D6BED4DDFFEE8C6507C1368AAB8FC4 (void);
// 0x000003E6 System.IAsyncResult easyar.Detail/FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString/DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_m3A1D66B8E42C64B5085DC416A4EC45A5FFD46AD1 (void);
// 0x000003E7 System.Void easyar.Detail/FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_m1A8DA08D56D7A319184E7EDA702A3D7D9DBD9DFB (void);
// 0x000003E8 System.Boolean easyar.Detail/OptionalOfString::get_has_value()
extern void OptionalOfString_get_has_value_m9CD5A6C104367A1ED5B9E44785BA721056E0D4AE (void);
// 0x000003E9 System.Void easyar.Detail/OptionalOfString::set_has_value(System.Boolean)
extern void OptionalOfString_set_has_value_m264C0ED811DA93486B2DE04A53B25EE094DE2C6A (void);
// 0x000003EA System.Boolean easyar.Detail/OptionalOfImageTarget::get_has_value()
extern void OptionalOfImageTarget_get_has_value_m1EF7147FD931E8F917DEB6C46BD51A7563022D33 (void);
// 0x000003EB System.Void easyar.Detail/OptionalOfImageTarget::set_has_value(System.Boolean)
extern void OptionalOfImageTarget_set_has_value_m8644B289D2E7A1195C693BB4E9843FE22F44C7A2 (void);
// 0x000003EC System.Void easyar.Detail/FunctorOfVoidFromCloudRecognizationResult/FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m77BEF147432A3DB2F47F6A89266AF68957951242 (void);
// 0x000003ED System.Void easyar.Detail/FunctorOfVoidFromCloudRecognizationResult/FunctionDelegate::Invoke(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void FunctionDelegate_Invoke_m76DA49C11AB299733BD600054318972AA08A818A (void);
// 0x000003EE System.IAsyncResult easyar.Detail/FunctorOfVoidFromCloudRecognizationResult/FunctionDelegate::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_mA12817782D70B1FFBF1D42DF0292BD974131947D (void);
// 0x000003EF System.Void easyar.Detail/FunctorOfVoidFromCloudRecognizationResult/FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_m2B0EE98024E41E40D8B27CCFF1DFE3FDFD45EDA5 (void);
// 0x000003F0 System.Void easyar.Detail/FunctorOfVoidFromCloudRecognizationResult/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m130EFA1D6131D28FEDE27440AF8FE8323ECD71A0 (void);
// 0x000003F1 System.Void easyar.Detail/FunctorOfVoidFromCloudRecognizationResult/DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m1AFC4A439D8AD3B7975FE4853FBD353B35F0BB1C (void);
// 0x000003F2 System.IAsyncResult easyar.Detail/FunctorOfVoidFromCloudRecognizationResult/DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_mCE7E52992BADEE8719A0F99C894AD9A2DFB0BD43 (void);
// 0x000003F3 System.Void easyar.Detail/FunctorOfVoidFromCloudRecognizationResult/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_mED953CB4B4E72974B422853854EA3AA378C48AD8 (void);
// 0x000003F4 System.Boolean easyar.Detail/OptionalOfFunctorOfVoidFromInputFrame::get_has_value()
extern void OptionalOfFunctorOfVoidFromInputFrame_get_has_value_m1D155C98CBF075D904FF4031CAA79FA57AC1324C (void);
// 0x000003F5 System.Void easyar.Detail/OptionalOfFunctorOfVoidFromInputFrame::set_has_value(System.Boolean)
extern void OptionalOfFunctorOfVoidFromInputFrame_set_has_value_m9E8B4E0E2EDD811B56D788AA37AC28C6CDFC2D92 (void);
// 0x000003F6 System.Void easyar.Detail/FunctorOfVoidFromInputFrame/FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m22228959FDD2D76D0494DB8BDE40DE8450114A74 (void);
// 0x000003F7 System.Void easyar.Detail/FunctorOfVoidFromInputFrame/FunctionDelegate::Invoke(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void FunctionDelegate_Invoke_mB73E30BD7031DC272CAEECFF9FD1DBA1229FACF8 (void);
// 0x000003F8 System.IAsyncResult easyar.Detail/FunctorOfVoidFromInputFrame/FunctionDelegate::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_mAA32B7FEBDB4EDBBEE8845351F35B80D5B70BBB7 (void);
// 0x000003F9 System.Void easyar.Detail/FunctorOfVoidFromInputFrame/FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_m0ED48B3290658AFC0C429CB25D098A501DD17533 (void);
// 0x000003FA System.Void easyar.Detail/FunctorOfVoidFromInputFrame/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m637A5714DFD493412A57351034DF201AAD3CBA50 (void);
// 0x000003FB System.Void easyar.Detail/FunctorOfVoidFromInputFrame/DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m79FE9F5D3B70D8BA4C970A2400EF2F512EDA1D81 (void);
// 0x000003FC System.IAsyncResult easyar.Detail/FunctorOfVoidFromInputFrame/DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_m142C7A0F1ED3FE2B0560D9640A131B4044D0A796 (void);
// 0x000003FD System.Void easyar.Detail/FunctorOfVoidFromInputFrame/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_mC382376EED82A56614E777BA8963755CB008D049 (void);
// 0x000003FE System.Boolean easyar.Detail/OptionalOfFunctorOfVoidFromCameraState::get_has_value()
extern void OptionalOfFunctorOfVoidFromCameraState_get_has_value_m8B4EBF20C2AAD048E9D671047BEE42BF9D07308B (void);
// 0x000003FF System.Void easyar.Detail/OptionalOfFunctorOfVoidFromCameraState::set_has_value(System.Boolean)
extern void OptionalOfFunctorOfVoidFromCameraState_set_has_value_m1ED45DFE473C86A2641CF8823E93D759BBBFF7D1 (void);
// 0x00000400 System.Void easyar.Detail/FunctorOfVoidFromCameraState/FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_mEC46C2A6C3C08807C1562646AB98B8A199AB03D8 (void);
// 0x00000401 System.Void easyar.Detail/FunctorOfVoidFromCameraState/FunctionDelegate::Invoke(System.IntPtr,easyar.CameraState,System.IntPtr&)
extern void FunctionDelegate_Invoke_mF4E3F4769FEA4E1DD1B4E8B82D10C4913E05B451 (void);
// 0x00000402 System.IAsyncResult easyar.Detail/FunctorOfVoidFromCameraState/FunctionDelegate::BeginInvoke(System.IntPtr,easyar.CameraState,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_m281A3202DBF9A613647EB838A8FE26BFB7AB2F38 (void);
// 0x00000403 System.Void easyar.Detail/FunctorOfVoidFromCameraState/FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_m3527CDAC3F38C1F78E6F8A01DFC6E07B02611072 (void);
// 0x00000404 System.Void easyar.Detail/FunctorOfVoidFromCameraState/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_mC86175595B8866AECB3E95633F4024081810E8AB (void);
// 0x00000405 System.Void easyar.Detail/FunctorOfVoidFromCameraState/DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_mDFFBFC132610A0FD48CF58FAA6EA185C8B7FADE2 (void);
// 0x00000406 System.IAsyncResult easyar.Detail/FunctorOfVoidFromCameraState/DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_m15447F2B57605F71E3D39ABC4CBF7E8E683B6E07 (void);
// 0x00000407 System.Void easyar.Detail/FunctorOfVoidFromCameraState/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_m11E070CD98E249582C1FAF058031B3914728EAE4 (void);
// 0x00000408 System.Boolean easyar.Detail/OptionalOfFunctorOfVoidFromPermissionStatusAndString::get_has_value()
extern void OptionalOfFunctorOfVoidFromPermissionStatusAndString_get_has_value_m81D80E41F5A8E24E6396A58596E192C4CF7DE650 (void);
// 0x00000409 System.Void easyar.Detail/OptionalOfFunctorOfVoidFromPermissionStatusAndString::set_has_value(System.Boolean)
extern void OptionalOfFunctorOfVoidFromPermissionStatusAndString_set_has_value_m92FE8F26A4611C4D1D925EF3DB553A81F482D896 (void);
// 0x0000040A System.Void easyar.Detail/FunctorOfVoidFromPermissionStatusAndString/FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m7CB4B98FD27C695EF880ED3DEDCCD68EC81B96D7 (void);
// 0x0000040B System.Void easyar.Detail/FunctorOfVoidFromPermissionStatusAndString/FunctionDelegate::Invoke(System.IntPtr,easyar.PermissionStatus,System.IntPtr,System.IntPtr&)
extern void FunctionDelegate_Invoke_m331255F6103E2E85AD26F6F4AA18C4C7179D5B01 (void);
// 0x0000040C System.IAsyncResult easyar.Detail/FunctorOfVoidFromPermissionStatusAndString/FunctionDelegate::BeginInvoke(System.IntPtr,easyar.PermissionStatus,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_mBA591F260EB29850D7FF2A05F84D4403002FBC0C (void);
// 0x0000040D System.Void easyar.Detail/FunctorOfVoidFromPermissionStatusAndString/FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_mF5C064004142278F5DF052DD64D81DE5B01001AE (void);
// 0x0000040E System.Void easyar.Detail/FunctorOfVoidFromPermissionStatusAndString/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m9FFB97C3E475AE9C2B59D5CC3BBFCFF4A0A5D5E6 (void);
// 0x0000040F System.Void easyar.Detail/FunctorOfVoidFromPermissionStatusAndString/DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m67058F84DC237C252F0BA5045B4D6D8546507C5A (void);
// 0x00000410 System.IAsyncResult easyar.Detail/FunctorOfVoidFromPermissionStatusAndString/DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_mEB4027E06F88F4D9B381FB2C65756ED937134C48 (void);
// 0x00000411 System.Void easyar.Detail/FunctorOfVoidFromPermissionStatusAndString/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_m327A1294381F14BB2D8DE3CEFA64B6B634003D27 (void);
// 0x00000412 System.Void easyar.Detail/FunctorOfVoidFromLogLevelAndString/FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m1ED621058775EBEDE6295EF2D208146A255975CC (void);
// 0x00000413 System.Void easyar.Detail/FunctorOfVoidFromLogLevelAndString/FunctionDelegate::Invoke(System.IntPtr,easyar.LogLevel,System.IntPtr,System.IntPtr&)
extern void FunctionDelegate_Invoke_m92881F9A36AF47BDFF0D94E1E97195AA6EE04EB2 (void);
// 0x00000414 System.IAsyncResult easyar.Detail/FunctorOfVoidFromLogLevelAndString/FunctionDelegate::BeginInvoke(System.IntPtr,easyar.LogLevel,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_mCFCA59D7162BBB7E44AD21D453A2C78534B3F656 (void);
// 0x00000415 System.Void easyar.Detail/FunctorOfVoidFromLogLevelAndString/FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_mF9FD7D3E095552529DE87711CA1C24C7C1BA0EA1 (void);
// 0x00000416 System.Void easyar.Detail/FunctorOfVoidFromLogLevelAndString/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_mCF6A2241214BA897AC9500FF8FEBF386FAB4CB38 (void);
// 0x00000417 System.Void easyar.Detail/FunctorOfVoidFromLogLevelAndString/DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m7BE9BA22B468AABBA23017B98EA905B8B3562FA0 (void);
// 0x00000418 System.IAsyncResult easyar.Detail/FunctorOfVoidFromLogLevelAndString/DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_m13D7228FB7BB78CBABD7D840A7A613F263E18A11 (void);
// 0x00000419 System.Void easyar.Detail/FunctorOfVoidFromLogLevelAndString/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_m7A2EF640AA2A19F9272D58FEDA1CFDD9CB1F32DE (void);
// 0x0000041A System.Boolean easyar.Detail/OptionalOfFunctorOfVoidFromRecordStatusAndString::get_has_value()
extern void OptionalOfFunctorOfVoidFromRecordStatusAndString_get_has_value_m40F62F2C9F50F336DB495B577C99D4AC9849FFD5 (void);
// 0x0000041B System.Void easyar.Detail/OptionalOfFunctorOfVoidFromRecordStatusAndString::set_has_value(System.Boolean)
extern void OptionalOfFunctorOfVoidFromRecordStatusAndString_set_has_value_mCC230A3F9E9860FC8BD386C044F043F177F7E852 (void);
// 0x0000041C System.Void easyar.Detail/FunctorOfVoidFromRecordStatusAndString/FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m7A83805BB9EA5670DC0648F8779AAB53AA8E88FC (void);
// 0x0000041D System.Void easyar.Detail/FunctorOfVoidFromRecordStatusAndString/FunctionDelegate::Invoke(System.IntPtr,easyar.RecordStatus,System.IntPtr,System.IntPtr&)
extern void FunctionDelegate_Invoke_m0640327DB6AF15A5E7453C71732763EDE32CB96B (void);
// 0x0000041E System.IAsyncResult easyar.Detail/FunctorOfVoidFromRecordStatusAndString/FunctionDelegate::BeginInvoke(System.IntPtr,easyar.RecordStatus,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_mDA2EBB0C5A782EE4ECB65F17E5C7C4C192DBEB30 (void);
// 0x0000041F System.Void easyar.Detail/FunctorOfVoidFromRecordStatusAndString/FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_m61EDA4A6FE2F6542395C6D0518ABE5C1A0F5A3E2 (void);
// 0x00000420 System.Void easyar.Detail/FunctorOfVoidFromRecordStatusAndString/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m06D7090996066E6297C71939AFD1DE1B7660A740 (void);
// 0x00000421 System.Void easyar.Detail/FunctorOfVoidFromRecordStatusAndString/DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m626B00C6EE55BAF2B51AF6D3440FAA26AF378D0B (void);
// 0x00000422 System.IAsyncResult easyar.Detail/FunctorOfVoidFromRecordStatusAndString/DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_m07955E9D590DBD3CC52F0169EB9E91F711F91C5F (void);
// 0x00000423 System.Void easyar.Detail/FunctorOfVoidFromRecordStatusAndString/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_mC57A971B42AD810362C6C1AA5473EF7E050161E8 (void);
// 0x00000424 System.Boolean easyar.Detail/OptionalOfMatrix44F::get_has_value()
extern void OptionalOfMatrix44F_get_has_value_m9C30778B7D9E982A11356624459B1D0A08A30AD0 (void);
// 0x00000425 System.Void easyar.Detail/OptionalOfMatrix44F::set_has_value(System.Boolean)
extern void OptionalOfMatrix44F_set_has_value_m68B97FAF4E20F2D0EE93221D7C51FE60F5EE9B55 (void);
// 0x00000426 System.Boolean easyar.Detail/OptionalOfFunctorOfVoidFromBool::get_has_value()
extern void OptionalOfFunctorOfVoidFromBool_get_has_value_m7D8DFE0840F0E53FA25E3ADEE84C54DDACCBE6BA (void);
// 0x00000427 System.Void easyar.Detail/OptionalOfFunctorOfVoidFromBool::set_has_value(System.Boolean)
extern void OptionalOfFunctorOfVoidFromBool_set_has_value_m077DB0E0292E386E9A1925D6161E580CB9A57B8B (void);
// 0x00000428 System.Void easyar.Detail/FunctorOfVoidFromBool/FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_mE2C767B389B5CE333D13F751E5677121656BAF0F (void);
// 0x00000429 System.Void easyar.Detail/FunctorOfVoidFromBool/FunctionDelegate::Invoke(System.IntPtr,System.Boolean,System.IntPtr&)
extern void FunctionDelegate_Invoke_m9E4218574D741E072D017696ADA9D89B0D2A1CEF (void);
// 0x0000042A System.IAsyncResult easyar.Detail/FunctorOfVoidFromBool/FunctionDelegate::BeginInvoke(System.IntPtr,System.Boolean,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_m45CCBE1B2F615B6AB7CC51F5B5D73F60BEE22852 (void);
// 0x0000042B System.Void easyar.Detail/FunctorOfVoidFromBool/FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_m2563E5B38C368F865DE17B913ED9B532C4FC44DD (void);
// 0x0000042C System.Void easyar.Detail/FunctorOfVoidFromBool/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_mB69AB348F0C70B3340B80D97F2129B839BF52A07 (void);
// 0x0000042D System.Void easyar.Detail/FunctorOfVoidFromBool/DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m3C7CFEB46A93EB9FE0BA5990F9474CFEFF16270C (void);
// 0x0000042E System.IAsyncResult easyar.Detail/FunctorOfVoidFromBool/DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_mD43363C968B9A3893D9801E8C087D68F67C39389 (void);
// 0x0000042F System.Void easyar.Detail/FunctorOfVoidFromBool/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_m06BC0CAA5DA44EA66C164D188E3142D773C691C2 (void);
// 0x00000430 System.Boolean easyar.Detail/OptionalOfImage::get_has_value()
extern void OptionalOfImage_get_has_value_m7932A8718D733AE7A914D6726A3A8FEBBB4E132A (void);
// 0x00000431 System.Void easyar.Detail/OptionalOfImage::set_has_value(System.Boolean)
extern void OptionalOfImage_set_has_value_mFA950AF9ADE589082DE916C46EAC5280D8C41CA4 (void);
// 0x00000432 System.Void easyar.Detail/FunctorOfVoidFromBoolAndStringAndString/FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m9EA89B29E4B455B7EA0B3C16C3E972FC7D9BC2EB (void);
// 0x00000433 System.Void easyar.Detail/FunctorOfVoidFromBoolAndStringAndString/FunctionDelegate::Invoke(System.IntPtr,System.Boolean,System.IntPtr,System.IntPtr,System.IntPtr&)
extern void FunctionDelegate_Invoke_m5A91A782F5BAC0D6B428F99B8F4736041BCC210B (void);
// 0x00000434 System.IAsyncResult easyar.Detail/FunctorOfVoidFromBoolAndStringAndString/FunctionDelegate::BeginInvoke(System.IntPtr,System.Boolean,System.IntPtr,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_mA08550D10019D40D657518FA157FA543F03E69FD (void);
// 0x00000435 System.Void easyar.Detail/FunctorOfVoidFromBoolAndStringAndString/FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_m948F5CAB7D1CB208BA10DE7F57FE726F7FF63531 (void);
// 0x00000436 System.Void easyar.Detail/FunctorOfVoidFromBoolAndStringAndString/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m10EC24BC0FC7D6409DB1BAE2AA182520D7589B98 (void);
// 0x00000437 System.Void easyar.Detail/FunctorOfVoidFromBoolAndStringAndString/DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_mA21998AA8EBB2D40C97D5ABA24D572D9A92C0C79 (void);
// 0x00000438 System.IAsyncResult easyar.Detail/FunctorOfVoidFromBoolAndStringAndString/DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_m395A688BDA8B0C26AFF5AA5E19243539D6E4D95C (void);
// 0x00000439 System.Void easyar.Detail/FunctorOfVoidFromBoolAndStringAndString/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_mF137E5C05472E83FCCA73EF9EA1149F51791E021 (void);
// 0x0000043A System.Void easyar.Detail/FunctorOfVoidFromBoolAndString/FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m3B3E9DC14D9429470B8C395E0EA67316CEE44BDB (void);
// 0x0000043B System.Void easyar.Detail/FunctorOfVoidFromBoolAndString/FunctionDelegate::Invoke(System.IntPtr,System.Boolean,System.IntPtr,System.IntPtr&)
extern void FunctionDelegate_Invoke_mACB6E1C587F056EBC17E0940A3BD3CBF202643F7 (void);
// 0x0000043C System.IAsyncResult easyar.Detail/FunctorOfVoidFromBoolAndString/FunctionDelegate::BeginInvoke(System.IntPtr,System.Boolean,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_m57772D05107C253E5FE982A575453FE4F7B39099 (void);
// 0x0000043D System.Void easyar.Detail/FunctorOfVoidFromBoolAndString/FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_m14769A83B34A2B68B2F55E82623236CCD7171E16 (void);
// 0x0000043E System.Void easyar.Detail/FunctorOfVoidFromBoolAndString/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m3BB7638AB145B5134C072FDBF08F98AE74908569 (void);
// 0x0000043F System.Void easyar.Detail/FunctorOfVoidFromBoolAndString/DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_mC4C2F2DCA035415FDD9DE260A4D8B3B90CCE480E (void);
// 0x00000440 System.IAsyncResult easyar.Detail/FunctorOfVoidFromBoolAndString/DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_mD2AA6C8AC71D45A301E958F2EC86A18381631DAD (void);
// 0x00000441 System.Void easyar.Detail/FunctorOfVoidFromBoolAndString/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_m7E794503786446671BF6908DF893BA97E209EDA0 (void);
// 0x00000442 System.Boolean easyar.Detail/OptionalOfFunctorOfVoidFromVideoStatus::get_has_value()
extern void OptionalOfFunctorOfVoidFromVideoStatus_get_has_value_m4C5A5EB6B25090AC80D73CF3A3578AF663F68354 (void);
// 0x00000443 System.Void easyar.Detail/OptionalOfFunctorOfVoidFromVideoStatus::set_has_value(System.Boolean)
extern void OptionalOfFunctorOfVoidFromVideoStatus_set_has_value_m798E54F282B4763352BFE839DB21AA648DF5BABB (void);
// 0x00000444 System.Void easyar.Detail/FunctorOfVoidFromVideoStatus/FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m8EA8F047D5B7E480FFAB83D7C2CA28A963E44EC5 (void);
// 0x00000445 System.Void easyar.Detail/FunctorOfVoidFromVideoStatus/FunctionDelegate::Invoke(System.IntPtr,easyar.VideoStatus,System.IntPtr&)
extern void FunctionDelegate_Invoke_mEE8CBEDB93E161FA7FD0705B35481329815CE5A5 (void);
// 0x00000446 System.IAsyncResult easyar.Detail/FunctorOfVoidFromVideoStatus/FunctionDelegate::BeginInvoke(System.IntPtr,easyar.VideoStatus,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_mEB80D100E9E57830F6B0651806DB3B551C49E482 (void);
// 0x00000447 System.Void easyar.Detail/FunctorOfVoidFromVideoStatus/FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_mED27D12DC08E9A5465B5498887068B8290A8A714 (void);
// 0x00000448 System.Void easyar.Detail/FunctorOfVoidFromVideoStatus/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m21E28952A0A86A48CD2D2A0F9FBD1F562719E207 (void);
// 0x00000449 System.Void easyar.Detail/FunctorOfVoidFromVideoStatus/DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m0F3A0A1A6C02A77A8705661A34761DC4CBDDEBBA (void);
// 0x0000044A System.IAsyncResult easyar.Detail/FunctorOfVoidFromVideoStatus/DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_m765923CB6B874E36AE7032EEBA0A27D9F781DF69 (void);
// 0x0000044B System.Void easyar.Detail/FunctorOfVoidFromVideoStatus/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_m0E02869B92173A1BE64B79E7874FC603963552E7 (void);
// 0x0000044C System.Boolean easyar.Detail/OptionalOfFunctorOfVoid::get_has_value()
extern void OptionalOfFunctorOfVoid_get_has_value_m674E9E83E83F23CA50AAE7266ED19DFAC9EEA2ED (void);
// 0x0000044D System.Void easyar.Detail/OptionalOfFunctorOfVoid::set_has_value(System.Boolean)
extern void OptionalOfFunctorOfVoid_set_has_value_m074530599AED813FDFBE0893F9A8D84B30BBB57A (void);
// 0x0000044E System.Boolean easyar.Detail/OptionalOfFunctorOfVoidFromFeedbackFrame::get_has_value()
extern void OptionalOfFunctorOfVoidFromFeedbackFrame_get_has_value_mD61A0B63DDB2445940E42029006B6E5220AACDF8 (void);
// 0x0000044F System.Void easyar.Detail/OptionalOfFunctorOfVoidFromFeedbackFrame::set_has_value(System.Boolean)
extern void OptionalOfFunctorOfVoidFromFeedbackFrame_set_has_value_mC6C7579BB351079A6BA5EF5DD10B402EBD9E9B84 (void);
// 0x00000450 System.Void easyar.Detail/FunctorOfVoidFromFeedbackFrame/FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m5A5FD6CB94180D93408F9D68ACCBFE4826A67145 (void);
// 0x00000451 System.Void easyar.Detail/FunctorOfVoidFromFeedbackFrame/FunctionDelegate::Invoke(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void FunctionDelegate_Invoke_mB60DAA936141A634D8050E1C4FFAA93CC2DBFD88 (void);
// 0x00000452 System.IAsyncResult easyar.Detail/FunctorOfVoidFromFeedbackFrame/FunctionDelegate::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_mC8076C4E47EC2EDD343BC6C980060443E14C0B9C (void);
// 0x00000453 System.Void easyar.Detail/FunctorOfVoidFromFeedbackFrame/FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_m04BFD551DA0922500CA968ED3CA012A003C075A2 (void);
// 0x00000454 System.Void easyar.Detail/FunctorOfVoidFromFeedbackFrame/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m5E218AAD9779E8D53F5847A058AC701C213FEB66 (void);
// 0x00000455 System.Void easyar.Detail/FunctorOfVoidFromFeedbackFrame/DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m263EDC03F81A505C2B492775F6A28F3D6F6B2F39 (void);
// 0x00000456 System.IAsyncResult easyar.Detail/FunctorOfVoidFromFeedbackFrame/DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_m25E177577EC66FFECF423A059A6E81F6ACE282F6 (void);
// 0x00000457 System.Void easyar.Detail/FunctorOfVoidFromFeedbackFrame/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_mF01233B1FC427FB1B42631CC077A8C6C98DABA2B (void);
// 0x00000458 System.Void easyar.Detail/FunctorOfOutputFrameFromListOfOutputFrame/FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m14AA29712CAC6C54514BE13932411F6B600E0AC0 (void);
// 0x00000459 System.Void easyar.Detail/FunctorOfOutputFrameFromListOfOutputFrame/FunctionDelegate::Invoke(System.IntPtr,System.IntPtr,System.IntPtr&,System.IntPtr&)
extern void FunctionDelegate_Invoke_m8A4197EF260E0810954C9F4C1DF1513F5EA8F47C (void);
// 0x0000045A System.IAsyncResult easyar.Detail/FunctorOfOutputFrameFromListOfOutputFrame/FunctionDelegate::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr&,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_m3D43A7D9A2DBC3643C8916BE473D594D78A58824 (void);
// 0x0000045B System.Void easyar.Detail/FunctorOfOutputFrameFromListOfOutputFrame/FunctionDelegate::EndInvoke(System.IntPtr&,System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_m3BA1C0C451E94B13FA24EA705B0EF99294D5DC7B (void);
// 0x0000045C System.Void easyar.Detail/FunctorOfOutputFrameFromListOfOutputFrame/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_mFCEB9949974004BA3352D77660C70D614B55F30F (void);
// 0x0000045D System.Void easyar.Detail/FunctorOfOutputFrameFromListOfOutputFrame/DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m6A872C6D5937FE5764C7502E6067F1465230733A (void);
// 0x0000045E System.IAsyncResult easyar.Detail/FunctorOfOutputFrameFromListOfOutputFrame/DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_mCAFCF7983F4BE9DE4FC6EEA32D859A83D46410F9 (void);
// 0x0000045F System.Void easyar.Detail/FunctorOfOutputFrameFromListOfOutputFrame/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_m517B13FF95E4ACF5B3C01DD70B0B73DB3CEF685F (void);
// 0x00000460 System.Void easyar.Detail/<>c::.cctor()
extern void U3CU3Ec__cctor_m74A22B74D37503717109F6D8808AC60DEE890775 (void);
// 0x00000461 System.Void easyar.Detail/<>c::.ctor()
extern void U3CU3Ec__ctor_mFCF65758C59A7BDB21C6952983D42B019C7294C4 (void);
// 0x00000462 easyar.Vec3F easyar.Detail/<>c::<ListOfVec3F_to_c>b__689_0(easyar.Vec3F)
extern void U3CU3Ec_U3CListOfVec3F_to_cU3Eb__689_0_m0FC29BB3B1BE29805B61EC25B7AE61FA44DA8683 (void);
// 0x00000463 System.IntPtr easyar.Detail/<>c::<ListOfTargetInstance_to_c>b__691_0(easyar.TargetInstance)
extern void U3CU3Ec_U3CListOfTargetInstance_to_cU3Eb__691_0_mD08A8E4A39FC322C3F09787ABA8F703B6CAB62AA (void);
// 0x00000464 easyar.Detail/OptionalOfFrameFilterResult easyar.Detail/<>c::<ListOfOptionalOfFrameFilterResult_to_c>b__695_0(easyar.Optional`1<easyar.FrameFilterResult>)
extern void U3CU3Ec_U3CListOfOptionalOfFrameFilterResult_to_cU3Eb__695_0_mD3BC3FD4F7D312E4E88F002EB853787839845467 (void);
// 0x00000465 easyar.Detail/OptionalOfFrameFilterResult easyar.Detail/<>c::<ListOfOptionalOfFrameFilterResult_to_c>b__695_1(easyar.Optional`1<easyar.FrameFilterResult>)
extern void U3CU3Ec_U3CListOfOptionalOfFrameFilterResult_to_cU3Eb__695_1_mB833AC25AD4E3CCCCFC0B67B5FD822028ED95EBC (void);
// 0x00000466 easyar.Optional`1<easyar.FrameFilterResult> easyar.Detail/<>c::<ListOfOptionalOfFrameFilterResult_from_c>b__696_0(easyar.Detail/OptionalOfFrameFilterResult)
extern void U3CU3Ec_U3CListOfOptionalOfFrameFilterResult_from_cU3Eb__696_0_m71D204E7F65868D5773B6E8D4A4847CE2B633E77 (void);
// 0x00000467 System.IntPtr easyar.Detail/<>c::<ListOfTarget_to_c>b__707_0(easyar.Target)
extern void U3CU3Ec_U3CListOfTarget_to_cU3Eb__707_0_m6D5A7A1706E369DFCC7D80F040BE76D27F0F9A3C (void);
// 0x00000468 System.IntPtr easyar.Detail/<>c::<ListOfImage_to_c>b__715_0(easyar.Image)
extern void U3CU3Ec_U3CListOfImage_to_cU3Eb__715_0_m1850A414DB05BCE67375661B4BF9356B2DFF6493 (void);
// 0x00000469 easyar.BlockInfo easyar.Detail/<>c::<ListOfBlockInfo_to_c>b__721_0(easyar.BlockInfo)
extern void U3CU3Ec_U3CListOfBlockInfo_to_cU3Eb__721_0_mF1DEA748299340E1B83F89405AB6DED771AF028C (void);
// 0x0000046A System.IntPtr easyar.Detail/<>c::<ListOfPlaneData_to_c>b__748_0(easyar.PlaneData)
extern void U3CU3Ec_U3CListOfPlaneData_to_cU3Eb__748_0_mE4850FB026635DBF9878B9E258AC578D8C73C5EB (void);
// 0x0000046B System.IntPtr easyar.Detail/<>c::<ListOfOutputFrame_to_c>b__779_0(easyar.OutputFrame)
extern void U3CU3Ec_U3CListOfOutputFrame_to_cU3Eb__779_0_m2CFCA8BDEE9B5A52C44D216802331C27ED9AC911 (void);
// 0x0000046C easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_0(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_0_mE6BCB67756EFC6D753A67DDAED38C9C0F5A52503 (void);
// 0x0000046D easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_1(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_1_m6B55BE835234AAC559C6A8539838DBA5E6D25AE0 (void);
// 0x0000046E easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_2(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_2_m18FE0ED905FC020D609823EE57EB4EF49BAAA676 (void);
// 0x0000046F easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_3(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_3_m4EFD8FB46F07A922A665192A8DD970FAAFD3089B (void);
// 0x00000470 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_4(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_4_mFA86F98D192F00BA7FA0931CBBF142F59DCD0A6B (void);
// 0x00000471 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_5(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_5_m887DCF951BEAECDAFDF76DD65696CF796B5F97EC (void);
// 0x00000472 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_6(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_6_mC535A6C45C073D67DDFF17DFF5B92A2DD2424B43 (void);
// 0x00000473 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_7(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_7_mDB3B2C1BCF95BB42D4918DB18A470FC373355AE1 (void);
// 0x00000474 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_8(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_8_m857149615CABB88B4A3411642083E841759EC547 (void);
// 0x00000475 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_9(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_9_m0CF7F9F1250F43AB1DEF5BEFF3A006619254E2A3 (void);
// 0x00000476 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_10(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_10_m47438FDE3C8A0C3D09E41F6F27A1A0956CE5BB47 (void);
// 0x00000477 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_11(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_11_m1D606F1623D1FFFAB3FD27240A8527DA9FAB6F7D (void);
// 0x00000478 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_12(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_12_m2EB6E1985B59AB6265B6C5B38F784A0E22CF01BC (void);
// 0x00000479 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_13(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_13_m9D1393E1E91A563876381D29B4678D8843F9F5BF (void);
// 0x0000047A easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_14(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_14_m1E71C6E2297B08A5F8A5B65376ECE74503103EDE (void);
// 0x0000047B easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_15(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_15_mF6AA821EE6C7D2099CA71EB3070875C7EB7078CD (void);
// 0x0000047C easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_16(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_16_mF6AEE9E71185FF5809DD9658231B6196E0AF038B (void);
// 0x0000047D easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_17(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_17_mBE3B41641A7D5368BA0013A2E3648A8A0E922868 (void);
// 0x0000047E easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_18(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_18_mB464257FD6BE4BC7D95940493DC2AF3ECC4711A4 (void);
// 0x0000047F easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_19(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_19_m7F79EAD59280FABC734A3ECB11A0D18134BDD996 (void);
// 0x00000480 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_20(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_20_mFC4542077E13E3E38EE50CE8025FC75DC27C2CAD (void);
// 0x00000481 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_21(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_21_m16B5446878F1D49A62AD63DA451878901F289E76 (void);
// 0x00000482 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_22(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_22_mB156A5D35AA2F28972DA5969AE8D46D9776E26A9 (void);
// 0x00000483 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_23(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_23_mDC8BBE4DA4BA38044331CDA1310C0D674DF474A9 (void);
// 0x00000484 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_24(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_24_m0FD1E42BFBF39DEF0A019CD2C90E5BFE33CA0C10 (void);
// 0x00000485 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_25(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_25_m6BF649B70FBD11DCC79A62AFF6B0AF183B5275C8 (void);
// 0x00000486 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_26(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_26_m119E5B5D8FE7B347AF65BBB43E84988BD93D719F (void);
// 0x00000487 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_27(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_27_mA8321F6396BA2ABC41AAB98D0ABF82451630B175 (void);
// 0x00000488 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_28(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_28_mAA1BF5912922932B297C2ED4930F94EE953EB711 (void);
// 0x00000489 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_29(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_29_m7FBF0FBFE6588506B22247D6EBBC31B272F6635F (void);
// 0x0000048A easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_30(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_30_mD2953D2E79D277099385497EB90D5F1220D87957 (void);
// 0x0000048B easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_31(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_31_m6BFC588AEF83717BC322B1F20E769C008B2BEA62 (void);
// 0x0000048C easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_32(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_32_mE13AF98D98393FABF4EBAD6AAFC28B522C29632F (void);
// 0x0000048D easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_33(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_33_m21E4BE531D41C3F2448DA4623CCDB034FAAC7418 (void);
// 0x0000048E easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_34(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_34_m0AEEB7744DF1C692F41D6D9429E612BC65819AA1 (void);
// 0x0000048F easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_35(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_35_m361BD8BBECADC1D8BC65A190EF3016D75FA7340E (void);
// 0x00000490 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_36(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_36_m039D91F8E1432F61A95E198223B7DF675C1CF5A1 (void);
// 0x00000491 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_37(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_37_mDDA91DEFD4FB5FF1A9A73E64543A09E0C19A8B41 (void);
// 0x00000492 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_38(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_38_m0D01FAA0D880BCE3574C5AA994C5F058EAC65A34 (void);
// 0x00000493 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_39(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_39_m5D92E3273F0B661AFE66993858B8CCFE9EAC05F4 (void);
// 0x00000494 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_40(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_40_mA371C7CE6B16F68448B6A1026B25444DE0831499 (void);
// 0x00000495 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_41(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_41_m360DF32D8028B52C19E668FB0E888DA9B811F044 (void);
// 0x00000496 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_42(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_42_m8802C6B4F2537B3E5B03F3947DB0F1210BDF0138 (void);
// 0x00000497 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_43(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_43_m842080962EBB563A84942546C381D0946457E0A3 (void);
// 0x00000498 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_44(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_44_m928C3B171D2C4B601F28592D4B7DED349CE7ED94 (void);
// 0x00000499 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_45(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_45_m6A920540D273BF152DEDEB43AF8AEE4F7A33DE0B (void);
// 0x0000049A easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_46(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_46_m149D77197A8F4067057E97BC7BE6BEA25A3476DC (void);
// 0x0000049B easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_47(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_47_m303A4CF48CA7956BA81C3822C243DADCC805EA4B (void);
// 0x0000049C easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_48(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_48_mABAB6D857AB480021AD8CA0C6BC4D5C9C08A3182 (void);
// 0x0000049D easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_49(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_49_m4725A9DB8DEECB696835BA4817E80AA82CB94865 (void);
// 0x0000049E easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_50(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_50_m30EC54B9BFE5FC0CB365ED6F8CDFEE0D26664A30 (void);
// 0x0000049F easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_51(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_51_m67A6779679AF48EBB43FD4B2088F42AA5AA5AA1F (void);
// 0x000004A0 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_52(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_52_m240E379918EE89535EBB0CFCFC0CF757B9340D99 (void);
// 0x000004A1 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_53(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_53_m52315F68B2B51E28331BFBCC86AC3B11FA824B0D (void);
// 0x000004A2 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_54(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_54_mC0830D51C49514239D760F3E76736291149326EA (void);
// 0x000004A3 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_55(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_55_m821AFCD44C5A335BCF73B65320C970312A483B6F (void);
// 0x000004A4 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_56(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_56_mC0AB756846BC9A4582FE4A201CFA16E4630C302B (void);
// 0x000004A5 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_57(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_57_mD89143A4F0A8C51DF3C1AE1C597900F0FD1E0DE5 (void);
// 0x000004A6 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_58(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_58_m8E6AC562EA37F78D30751B1E0943CF30A549163A (void);
// 0x000004A7 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_59(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_59_mE4ED3483A3DAF625E816B04EB3C71181235A74A8 (void);
// 0x000004A8 easyar.RefBase easyar.Detail/<>c::<.cctor>b__781_60(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__781_60_mB08EEDDFFE6E77C9A84D93927129F4C5B660680D (void);
// 0x000004A9 System.Void easyar.Detail/<>c__DisplayClass700_0::.ctor()
extern void U3CU3Ec__DisplayClass700_0__ctor_m58ADB096FBD659F9A21C90257E7D8DC885D6DD8F (void);
// 0x000004AA System.Void easyar.Detail/<>c__DisplayClass700_0::<FunctorOfVoidFromOutputFrame_func>b__0()
extern void U3CU3Ec__DisplayClass700_0_U3CFunctorOfVoidFromOutputFrame_funcU3Eb__0_m35B0B7F6FF2DC6179D63C5157F514B502578CB39 (void);
// 0x000004AB System.Void easyar.Detail/<>c__DisplayClass704_0::.ctor()
extern void U3CU3Ec__DisplayClass704_0__ctor_m7F92D06FD7ECAF3C25EF03F799B16429BC1AF613 (void);
// 0x000004AC System.Void easyar.Detail/<>c__DisplayClass704_0::<FunctorOfVoidFromTargetAndBool_func>b__0()
extern void U3CU3Ec__DisplayClass704_0_U3CFunctorOfVoidFromTargetAndBool_funcU3Eb__0_m1FA75065AE63D6D1A00569A3A5D14DF72364CAC3 (void);
// 0x000004AD System.Void easyar.Detail/<>c__DisplayClass710_0::.ctor()
extern void U3CU3Ec__DisplayClass710_0__ctor_m94F770671944885F8471A6E5D586A8DF8D89E638 (void);
// 0x000004AE easyar.Optional`1<System.String> easyar.Detail/<>c__DisplayClass710_0::<FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_func>b__0(easyar.Detail/OptionalOfString)
extern void U3CU3Ec__DisplayClass710_0_U3CFunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_funcU3Eb__0_mD4D35F19A6180B2D9BE8B4750738E8CCC6D14644 (void);
// 0x000004AF System.Void easyar.Detail/<>c__DisplayClass718_0::.ctor()
extern void U3CU3Ec__DisplayClass718_0__ctor_m55B3430BC64E712EFC5C5FF225B97D997783A8EE (void);
// 0x000004B0 System.Void easyar.Detail/<>c__DisplayClass718_0::<FunctorOfVoidFromCloudRecognizationResult_func>b__0()
extern void U3CU3Ec__DisplayClass718_0_U3CFunctorOfVoidFromCloudRecognizationResult_funcU3Eb__0_m31EC853F846F08D9D99E9A8D983FEAE35DC6A107 (void);
// 0x000004B1 System.Void easyar.Detail/<>c__DisplayClass725_0::.ctor()
extern void U3CU3Ec__DisplayClass725_0__ctor_mC3161E17D4B12070F4830FF483B7F72C43A39240 (void);
// 0x000004B2 System.Void easyar.Detail/<>c__DisplayClass725_0::<FunctorOfVoidFromInputFrame_func>b__0()
extern void U3CU3Ec__DisplayClass725_0_U3CFunctorOfVoidFromInputFrame_funcU3Eb__0_m4680E2F1E98DCE9D14175430427419D60987A51F (void);
// 0x000004B3 System.Void easyar.Detail/<>c__DisplayClass772_0::.ctor()
extern void U3CU3Ec__DisplayClass772_0__ctor_mBCF774809B9D94886E30C3FE04C507C41CD94242 (void);
// 0x000004B4 System.Void easyar.Detail/<>c__DisplayClass772_0::<FunctorOfVoidFromFeedbackFrame_func>b__0()
extern void U3CU3Ec__DisplayClass772_0_U3CFunctorOfVoidFromFeedbackFrame_funcU3Eb__0_m53DDAFE7A244FF0FB38C3B320BCE53075CB60148 (void);
// 0x000004B5 System.Void easyar.Detail/<>c__DisplayClass776_0::.ctor()
extern void U3CU3Ec__DisplayClass776_0__ctor_m80CB201E31FADB519EB01849AE0A375D0193BC1F (void);
// 0x000004B6 System.Void easyar.Detail/<>c__DisplayClass776_0::<FunctorOfOutputFrameFromListOfOutputFrame_func>b__0(easyar.OutputFrame)
extern void U3CU3Ec__DisplayClass776_0_U3CFunctorOfOutputFrameFromListOfOutputFrame_funcU3Eb__0_m319C3913F62EE2A5AB8BC0290F0ADF4E0ABF4033 (void);
// 0x000004B7 System.Void easyar.Detail/<>c__DisplayClass776_1::.ctor()
extern void U3CU3Ec__DisplayClass776_1__ctor_mD712B4F6AB1E542AE081D5DBF432CFF2BB2FFA38 (void);
// 0x000004B8 System.Void easyar.Detail/<>c__DisplayClass776_1::<FunctorOfOutputFrameFromListOfOutputFrame_func>b__1()
extern void U3CU3Ec__DisplayClass776_1_U3CFunctorOfOutputFrameFromListOfOutputFrame_funcU3Eb__1_m1F62D9936816EDF7413F84D4DE99F657969A7EA2 (void);
// 0x000004B9 System.Void easyar.RefBase::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void RefBase__ctor_m7A260F9F32D29D8BA70A5673F69375938C959A03 (void);
// 0x000004BA System.IntPtr easyar.RefBase::get_cdata()
extern void RefBase_get_cdata_mB6578F201F1C3F2290BD531478E12F9C7957639A (void);
// 0x000004BB System.Void easyar.RefBase::Finalize()
extern void RefBase_Finalize_m8AF2E59BBC04F9AB27B0E8CCE8DA9EDA843679F1 (void);
// 0x000004BC System.Void easyar.RefBase::Dispose()
extern void RefBase_Dispose_m09A89B3E9CFCF8387C122921A75DB9057C34B9CB (void);
// 0x000004BD System.Object easyar.RefBase::CloneObject()
// 0x000004BE easyar.RefBase easyar.RefBase::Clone()
extern void RefBase_Clone_mC01B22B577336F9A3FE79CBAB145EF07F757BB6B (void);
// 0x000004BF System.Void easyar.RefBase/Retainer::.ctor(System.Object,System.IntPtr)
extern void Retainer__ctor_m306A515EBA144C8D5E370F4FDCA6C2BCD9E19494 (void);
// 0x000004C0 System.Void easyar.RefBase/Retainer::Invoke(System.IntPtr,System.IntPtr&)
extern void Retainer_Invoke_m84D08D6C3E2BD7F95B90B21E30DCEEA885E81E19 (void);
// 0x000004C1 System.IAsyncResult easyar.RefBase/Retainer::BeginInvoke(System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void Retainer_BeginInvoke_m3819FD3E451D8CBB2DFCE372F5A31B68E6B67B05 (void);
// 0x000004C2 System.Void easyar.RefBase/Retainer::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void Retainer_EndInvoke_m49DF86569EE4A57DF039D26F9F9DD77D28346274 (void);
// 0x000004C3 System.Void easyar.ObjectTargetParameters::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void ObjectTargetParameters__ctor_m9669D686C9CF5D8C602EB00E73E9ADD0D0B9C609 (void);
// 0x000004C4 System.Object easyar.ObjectTargetParameters::CloneObject()
extern void ObjectTargetParameters_CloneObject_m52D6E16285E1AAD07F50BFE0EDB8774E7DDEDD2E (void);
// 0x000004C5 easyar.ObjectTargetParameters easyar.ObjectTargetParameters::Clone()
extern void ObjectTargetParameters_Clone_m98EE44CC837D901E39B107E6B50297C1A40E81B2 (void);
// 0x000004C6 System.Void easyar.ObjectTargetParameters::.ctor()
extern void ObjectTargetParameters__ctor_mDCEE6534FE4309ADF2E5C1F8852A177A5FA9C581 (void);
// 0x000004C7 easyar.BufferDictionary easyar.ObjectTargetParameters::bufferDictionary()
extern void ObjectTargetParameters_bufferDictionary_mDABD4AF642361B07357E875F893BB92C7F5969C9 (void);
// 0x000004C8 System.Void easyar.ObjectTargetParameters::setBufferDictionary(easyar.BufferDictionary)
extern void ObjectTargetParameters_setBufferDictionary_mD4C682FBF7F166492EAB351A489C6C03B81870A6 (void);
// 0x000004C9 System.String easyar.ObjectTargetParameters::objPath()
extern void ObjectTargetParameters_objPath_m66930AD03179B466A592ABF89211E681009777FD (void);
// 0x000004CA System.Void easyar.ObjectTargetParameters::setObjPath(System.String)
extern void ObjectTargetParameters_setObjPath_mA943AF4181993AF422E4C6C3698EE2BB88B7E205 (void);
// 0x000004CB System.String easyar.ObjectTargetParameters::name()
extern void ObjectTargetParameters_name_mE8BE6FFF79E24D4123074842CE235FFA388FA615 (void);
// 0x000004CC System.Void easyar.ObjectTargetParameters::setName(System.String)
extern void ObjectTargetParameters_setName_m9621937A37E5A5C203E2EE6038E2C4A7A033F6F1 (void);
// 0x000004CD System.String easyar.ObjectTargetParameters::uid()
extern void ObjectTargetParameters_uid_mD104B6834CF22CAAB1776D95DA631FCB599FFD40 (void);
// 0x000004CE System.Void easyar.ObjectTargetParameters::setUid(System.String)
extern void ObjectTargetParameters_setUid_m2EF14E2DB2D12F019CE393B0EBE071E9A8D5456F (void);
// 0x000004CF System.String easyar.ObjectTargetParameters::meta()
extern void ObjectTargetParameters_meta_m2B8F6674970B96EC4F5D9B26ECE8097EDEF7A828 (void);
// 0x000004D0 System.Void easyar.ObjectTargetParameters::setMeta(System.String)
extern void ObjectTargetParameters_setMeta_mC5E6CA52704E33D73A00430CBCDC468EB0A7176B (void);
// 0x000004D1 System.Single easyar.ObjectTargetParameters::scale()
extern void ObjectTargetParameters_scale_m565BA7669B0C42A6FF44A0D43ECD00F18FADFAB8 (void);
// 0x000004D2 System.Void easyar.ObjectTargetParameters::setScale(System.Single)
extern void ObjectTargetParameters_setScale_m2B9A0EBC0ECE6405ACF82DA3C90608C89D4672CA (void);
// 0x000004D3 System.Void easyar.ObjectTarget::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void ObjectTarget__ctor_m68992F48171E218AF8531649572C6BBA4CA4B4DF (void);
// 0x000004D4 System.Object easyar.ObjectTarget::CloneObject()
extern void ObjectTarget_CloneObject_m5B5985636E1DB538A771B5D15BB9061572672D3B (void);
// 0x000004D5 easyar.ObjectTarget easyar.ObjectTarget::Clone()
extern void ObjectTarget_Clone_m7F0766C9857E9807C6601895556DB3A2D0029324 (void);
// 0x000004D6 System.Void easyar.ObjectTarget::.ctor()
extern void ObjectTarget__ctor_m6A0F54F4CFE918B68B95386DEE0A396BF10A8DBB (void);
// 0x000004D7 easyar.Optional`1<easyar.ObjectTarget> easyar.ObjectTarget::createFromParameters(easyar.ObjectTargetParameters)
extern void ObjectTarget_createFromParameters_m9C04DC6A127FDEEE748C6D751DB5604209DE6665 (void);
// 0x000004D8 easyar.Optional`1<easyar.ObjectTarget> easyar.ObjectTarget::createFromObjectFile(System.String,easyar.StorageType,System.String,System.String,System.String,System.Single)
extern void ObjectTarget_createFromObjectFile_mBA708FA754BA5C0368C5DA7BCC6CAEC657A372FC (void);
// 0x000004D9 System.Single easyar.ObjectTarget::scale()
extern void ObjectTarget_scale_m09B24B77852B8719DE926BC60DBBBCC641D05CD9 (void);
// 0x000004DA System.Collections.Generic.List`1<easyar.Vec3F> easyar.ObjectTarget::boundingBox()
extern void ObjectTarget_boundingBox_mD96A4B4D5AB63C5F200A007E7ACC504887A0FB18 (void);
// 0x000004DB System.Boolean easyar.ObjectTarget::setScale(System.Single)
extern void ObjectTarget_setScale_m5D131415E3D6E79BECA6A09FF2B4FC2BB9F6F9F3 (void);
// 0x000004DC System.Int32 easyar.ObjectTarget::runtimeID()
extern void ObjectTarget_runtimeID_m1CE1BB1328D80FE9E2BA215D98F099B91A0DF421 (void);
// 0x000004DD System.String easyar.ObjectTarget::uid()
extern void ObjectTarget_uid_m04C010BA156AC4671D80DA47A9A6651B9956A441 (void);
// 0x000004DE System.String easyar.ObjectTarget::name()
extern void ObjectTarget_name_m3CDBFE5595AB0167264CF08E219362BD349EC580 (void);
// 0x000004DF System.Void easyar.ObjectTarget::setName(System.String)
extern void ObjectTarget_setName_m9435E85FB7961205A76CB389A036F9F0C9D2A213 (void);
// 0x000004E0 System.String easyar.ObjectTarget::meta()
extern void ObjectTarget_meta_m16BABD79332153CC88486600A2D7405FF35B378D (void);
// 0x000004E1 System.Void easyar.ObjectTarget::setMeta(System.String)
extern void ObjectTarget_setMeta_m85E56ACEDF13E5C587875552B20FB667F7D9B192 (void);
// 0x000004E2 System.Void easyar.ObjectTarget/<>c::.cctor()
extern void U3CU3Ec__cctor_m2449B1C4193B993B7B828B3DA6188ECF9A2F6D08 (void);
// 0x000004E3 System.Void easyar.ObjectTarget/<>c::.ctor()
extern void U3CU3Ec__ctor_mACF643E2FADC4F362E13889C73B44F31F0C78591 (void);
// 0x000004E4 easyar.Optional`1<easyar.ObjectTarget> easyar.ObjectTarget/<>c::<createFromParameters>b__4_0(easyar.Detail/OptionalOfObjectTarget)
extern void U3CU3Ec_U3CcreateFromParametersU3Eb__4_0_m708243164B6FF02B2A2C3C659F3C3AB70A97F0F0 (void);
// 0x000004E5 easyar.Optional`1<easyar.ObjectTarget> easyar.ObjectTarget/<>c::<createFromObjectFile>b__5_0(easyar.Detail/OptionalOfObjectTarget)
extern void U3CU3Ec_U3CcreateFromObjectFileU3Eb__5_0_mB6507D12B94221377FCE750D05AD47AD210E2ECA (void);
// 0x000004E6 System.Void easyar.ObjectTrackerResult::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void ObjectTrackerResult__ctor_mEFE14AAAE06BDE7EFE6DA5A8F0F960A72F6F5399 (void);
// 0x000004E7 System.Object easyar.ObjectTrackerResult::CloneObject()
extern void ObjectTrackerResult_CloneObject_mC581232CCFEF3671C757E46AAB1574ADAAC65FFF (void);
// 0x000004E8 easyar.ObjectTrackerResult easyar.ObjectTrackerResult::Clone()
extern void ObjectTrackerResult_Clone_m7B45868A7366FE8400AAB88DFBFD29F3B6D31DC8 (void);
// 0x000004E9 System.Collections.Generic.List`1<easyar.TargetInstance> easyar.ObjectTrackerResult::targetInstances()
extern void ObjectTrackerResult_targetInstances_m1C59A3D86CED84EB2BD079E4FCFDAB35D070353A (void);
// 0x000004EA System.Void easyar.ObjectTrackerResult::setTargetInstances(System.Collections.Generic.List`1<easyar.TargetInstance>)
extern void ObjectTrackerResult_setTargetInstances_m7C8D9D96CF22EBA0CEE974E091CAE38DEEC16682 (void);
// 0x000004EB System.Void easyar.ObjectTracker::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void ObjectTracker__ctor_mC9AAFF5DAF891EDBCEEBAFF6D18D1FDCA103F610 (void);
// 0x000004EC System.Object easyar.ObjectTracker::CloneObject()
extern void ObjectTracker_CloneObject_mF854A05BC5751690BD291E1805265045E2FE58D9 (void);
// 0x000004ED easyar.ObjectTracker easyar.ObjectTracker::Clone()
extern void ObjectTracker_Clone_m8C3C6381F47B13EC0E308FC683C78054C1742C25 (void);
// 0x000004EE System.Boolean easyar.ObjectTracker::isAvailable()
extern void ObjectTracker_isAvailable_mA0E12804D767C6224A3E85C5D8340455B68D6AA6 (void);
// 0x000004EF easyar.FeedbackFrameSink easyar.ObjectTracker::feedbackFrameSink()
extern void ObjectTracker_feedbackFrameSink_m22AF786EE2ED243911CEE1045BB86D497B134D2C (void);
// 0x000004F0 System.Int32 easyar.ObjectTracker::bufferRequirement()
extern void ObjectTracker_bufferRequirement_mAFE23598F1CB287131F494B263C9BA6AD51DFEFA (void);
// 0x000004F1 easyar.OutputFrameSource easyar.ObjectTracker::outputFrameSource()
extern void ObjectTracker_outputFrameSource_m70525490C852BA61617E160A348E7AED3B71AB58 (void);
// 0x000004F2 easyar.ObjectTracker easyar.ObjectTracker::create()
extern void ObjectTracker_create_mF19D20FC41A908A8E412EF86FBC668DCD2066E69 (void);
// 0x000004F3 System.Boolean easyar.ObjectTracker::start()
extern void ObjectTracker_start_m33F86D8FD7E3BEED463AEA9D83084A9ED60E2AAB (void);
// 0x000004F4 System.Void easyar.ObjectTracker::stop()
extern void ObjectTracker_stop_m5AA63DBCBD8DCFD713AC3EC35D30D465756CC72C (void);
// 0x000004F5 System.Void easyar.ObjectTracker::close()
extern void ObjectTracker_close_mA3FCF12B8710CF8ECC5D88146F68121248ABB005 (void);
// 0x000004F6 System.Void easyar.ObjectTracker::loadTarget(easyar.Target,easyar.CallbackScheduler,System.Action`2<easyar.Target,System.Boolean>)
extern void ObjectTracker_loadTarget_m343C289BC990D2DD3DD6288FB44D89FD33B63CC4 (void);
// 0x000004F7 System.Void easyar.ObjectTracker::unloadTarget(easyar.Target,easyar.CallbackScheduler,System.Action`2<easyar.Target,System.Boolean>)
extern void ObjectTracker_unloadTarget_m85CFF1CD41835E53E1F15C039AB82AA1AA888BAF (void);
// 0x000004F8 System.Collections.Generic.List`1<easyar.Target> easyar.ObjectTracker::targets()
extern void ObjectTracker_targets_m2BBCABB0117FEB7F0D887CB7CEA1A521E6BD0008 (void);
// 0x000004F9 System.Boolean easyar.ObjectTracker::setSimultaneousNum(System.Int32)
extern void ObjectTracker_setSimultaneousNum_mAB4E3B1EEAA501290676993A90411D1127B56B63 (void);
// 0x000004FA System.Int32 easyar.ObjectTracker::simultaneousNum()
extern void ObjectTracker_simultaneousNum_mCF70C03A178939D5A355BFF9CED73D0D4592D163 (void);
// 0x000004FB System.Void easyar.CalibrationDownloader::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void CalibrationDownloader__ctor_mCBD686C41DA88862DFA184064D237DAAFDABA326 (void);
// 0x000004FC System.Object easyar.CalibrationDownloader::CloneObject()
extern void CalibrationDownloader_CloneObject_mEC861D203AB61A4986F731A2B22C2735E6421209 (void);
// 0x000004FD easyar.CalibrationDownloader easyar.CalibrationDownloader::Clone()
extern void CalibrationDownloader_Clone_mC43E57EF413B43F6B08339ADE806ABF5C851C47B (void);
// 0x000004FE System.Void easyar.CalibrationDownloader::.ctor()
extern void CalibrationDownloader__ctor_m385A7C9E24B4EBF37FACEC0A5BBF79432586FB36 (void);
// 0x000004FF System.Void easyar.CalibrationDownloader::download(easyar.CallbackScheduler,System.Action`2<easyar.CalibrationDownloadStatus,easyar.Optional`1<System.String>>)
extern void CalibrationDownloader_download_m3C68F6381860DD1ED668A7928B1F7AB39981918A (void);
// 0x00000500 System.Void easyar.CloudRecognizationResult::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void CloudRecognizationResult__ctor_mC669149A29321DC58354C778438BA2913149ECAE (void);
// 0x00000501 System.Object easyar.CloudRecognizationResult::CloneObject()
extern void CloudRecognizationResult_CloneObject_m1812DE6EBB1AFB3A87BE4E59AF9D3779CB05CFBD (void);
// 0x00000502 easyar.CloudRecognizationResult easyar.CloudRecognizationResult::Clone()
extern void CloudRecognizationResult_Clone_m5BFBC0C5EDFFE14994D4EFED2159C2A2DD538940 (void);
// 0x00000503 easyar.CloudRecognizationStatus easyar.CloudRecognizationResult::getStatus()
extern void CloudRecognizationResult_getStatus_m03896D43151D010F0A564CBCDD9AC36225707731 (void);
// 0x00000504 easyar.Optional`1<easyar.ImageTarget> easyar.CloudRecognizationResult::getTarget()
extern void CloudRecognizationResult_getTarget_m6FAF44E37DEACC260AB11F9ADB4BA1EB79DF289B (void);
// 0x00000505 easyar.Optional`1<System.String> easyar.CloudRecognizationResult::getUnknownErrorMessage()
extern void CloudRecognizationResult_getUnknownErrorMessage_mC906B72E0E8255D0556C6985EA88C7B3AFB6DABE (void);
// 0x00000506 System.Void easyar.CloudRecognizationResult/<>c::.cctor()
extern void U3CU3Ec__cctor_mC706284EAFCEB13EF8F05750A9F4B1800A635CDE (void);
// 0x00000507 System.Void easyar.CloudRecognizationResult/<>c::.ctor()
extern void U3CU3Ec__ctor_m1238C3B1824BB665CB1874E52203FD33B2954334 (void);
// 0x00000508 easyar.Optional`1<easyar.ImageTarget> easyar.CloudRecognizationResult/<>c::<getTarget>b__4_0(easyar.Detail/OptionalOfImageTarget)
extern void U3CU3Ec_U3CgetTargetU3Eb__4_0_m11B2033894190021C713D88B7B2467FEB0F34039 (void);
// 0x00000509 System.Void easyar.CloudRecognizationResult/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mD29F827AA6A906349C7BFC7AACEA6354FF64CE2B (void);
// 0x0000050A easyar.Optional`1<System.String> easyar.CloudRecognizationResult/<>c__DisplayClass5_0::<getUnknownErrorMessage>b__0(easyar.Detail/OptionalOfString)
extern void U3CU3Ec__DisplayClass5_0_U3CgetUnknownErrorMessageU3Eb__0_mC3E207C843BF6FE15A1BEB78C85C6494AA742F12 (void);
// 0x0000050B System.Void easyar.CloudRecognizer::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void CloudRecognizer__ctor_m549EC085B4AEA5F31E986060A9180A02A3EFE417 (void);
// 0x0000050C System.Object easyar.CloudRecognizer::CloneObject()
extern void CloudRecognizer_CloneObject_m6150B1A145DB15194863FA671E6F9A8936E796C0 (void);
// 0x0000050D easyar.CloudRecognizer easyar.CloudRecognizer::Clone()
extern void CloudRecognizer_Clone_mE1950F4117AF253174A743CD6886B3156C75D462 (void);
// 0x0000050E System.Boolean easyar.CloudRecognizer::isAvailable()
extern void CloudRecognizer_isAvailable_m37098B8EB063BFA93CFC16985D27011228B42517 (void);
// 0x0000050F easyar.CloudRecognizer easyar.CloudRecognizer::create(System.String,System.String,System.String,System.String)
extern void CloudRecognizer_create_m3F53BEB7A5732EBBE687120A53736AA858A249A1 (void);
// 0x00000510 easyar.CloudRecognizer easyar.CloudRecognizer::createByCloudSecret(System.String,System.String,System.String)
extern void CloudRecognizer_createByCloudSecret_m71E6387B3EB2E6848B9E40F13DBBDD949A53F54A (void);
// 0x00000511 System.Void easyar.CloudRecognizer::resolve(easyar.InputFrame,easyar.CallbackScheduler,System.Action`1<easyar.CloudRecognizationResult>)
extern void CloudRecognizer_resolve_m2BCF01C12B51ED1533D8CCF3F5606D49DA496CD1 (void);
// 0x00000512 System.Void easyar.CloudRecognizer::close()
extern void CloudRecognizer_close_m4E6786F893C4A5B7620E2F250EA52CA7248BF508 (void);
// 0x00000513 System.Void easyar.Buffer::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void Buffer__ctor_m15ADEF3BDB6E349FCE275463A209CDCCF8EA3465 (void);
// 0x00000514 System.Object easyar.Buffer::CloneObject()
extern void Buffer_CloneObject_m346C1DDE9E8B6E7C1421AEABD52A717770B70487 (void);
// 0x00000515 easyar.Buffer easyar.Buffer::Clone()
extern void Buffer_Clone_m5F3C17C6CCB50D9BEE76EEB7A9E128CD40D6E4F7 (void);
// 0x00000516 easyar.Buffer easyar.Buffer::wrap(System.IntPtr,System.Int32,System.Action)
extern void Buffer_wrap_m4C7E982C40E9E049D57EA1F551768A0BD4B52520 (void);
// 0x00000517 easyar.Buffer easyar.Buffer::create(System.Int32)
extern void Buffer_create_mA4045366B79E8BB40127703CFA8C029245789B8A (void);
// 0x00000518 System.IntPtr easyar.Buffer::data()
extern void Buffer_data_m889C1CA1DD900605C323F69DFDB7ADD1F3302662 (void);
// 0x00000519 System.Int32 easyar.Buffer::size()
extern void Buffer_size_m7166BFEDC0284B4423E619AB184B053F773309B4 (void);
// 0x0000051A System.Void easyar.Buffer::memoryCopy(System.IntPtr,System.IntPtr,System.Int32)
extern void Buffer_memoryCopy_m05CE603F8CFE5330474FFC73C2B3BACD9B9BD9D7 (void);
// 0x0000051B System.Boolean easyar.Buffer::tryCopyFrom(System.IntPtr,System.Int32,System.Int32,System.Int32)
extern void Buffer_tryCopyFrom_m95FE694E61D7F1A94E22DDCE3BC01C7A5A2FA88A (void);
// 0x0000051C System.Boolean easyar.Buffer::tryCopyTo(System.Int32,System.IntPtr,System.Int32,System.Int32)
extern void Buffer_tryCopyTo_m587CA05561E493D7867708B89C21375DABC98469 (void);
// 0x0000051D easyar.Buffer easyar.Buffer::partition(System.Int32,System.Int32)
extern void Buffer_partition_mB867839C93BA3E330AC9C8CDDD02B63CD02FFF98 (void);
// 0x0000051E easyar.Buffer easyar.Buffer::wrapByteArray(System.Byte[])
extern void Buffer_wrapByteArray_mD81AFE8D8ABA7B59044BA60448F30B5233FF5D8B (void);
// 0x0000051F easyar.Buffer easyar.Buffer::wrapByteArray(System.Byte[],System.Int32,System.Int32)
extern void Buffer_wrapByteArray_m627F2C23AE3CCA2B4A216C9BA0DD14A03FDDB6AE (void);
// 0x00000520 easyar.Buffer easyar.Buffer::wrapByteArray(System.Byte[],System.Int32,System.Int32,System.Action)
extern void Buffer_wrapByteArray_mEE6301024DE0CC550363B6E3A6620D43B7081FCB (void);
// 0x00000521 System.Void easyar.Buffer::copyFromByteArray(System.Byte[])
extern void Buffer_copyFromByteArray_m6F41156DC2DD490C222564708B1C37E535231837 (void);
// 0x00000522 System.Void easyar.Buffer::copyFromByteArray(System.Byte[],System.Int32,System.Int32,System.Int32)
extern void Buffer_copyFromByteArray_m56B142C6DCFD152829CFF3465514EF36F75B0A66 (void);
// 0x00000523 System.Void easyar.Buffer::copyToByteArray(System.Byte[])
extern void Buffer_copyToByteArray_mAE5E07696CEBC141307CB2F9F36B606756F96664 (void);
// 0x00000524 System.Void easyar.Buffer::copyToByteArray(System.Int32,System.Byte[],System.Int32,System.Int32)
extern void Buffer_copyToByteArray_mBC783F00807578362CA594BB1611C00ECF4A6C06 (void);
// 0x00000525 System.Void easyar.Buffer/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_mDFFA4231ACEDA772E5728302D601A5B9FEB3B830 (void);
// 0x00000526 System.Void easyar.Buffer/<>c__DisplayClass11_0::<wrapByteArray>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CwrapByteArrayU3Eb__0_m9A8C109B13512123C7280DAE55BB2B11DB05E94A (void);
// 0x00000527 System.Void easyar.Buffer/<>c::.cctor()
extern void U3CU3Ec__cctor_mFE7F01DBEDB32B327BB1A8B8A431053EE12331B9 (void);
// 0x00000528 System.Void easyar.Buffer/<>c::.ctor()
extern void U3CU3Ec__ctor_m846AD8143B92341281505FC87169252808836CEF (void);
// 0x00000529 System.Void easyar.Buffer/<>c::<wrapByteArray>b__12_0()
extern void U3CU3Ec_U3CwrapByteArrayU3Eb__12_0_m695AE061FBD5320FB583FEC24F79E28351D12489 (void);
// 0x0000052A System.Void easyar.Buffer/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_mD633A27694742ACDF76E85A1360A364C57805213 (void);
// 0x0000052B System.Void easyar.Buffer/<>c__DisplayClass13_0::<wrapByteArray>b__0()
extern void U3CU3Ec__DisplayClass13_0_U3CwrapByteArrayU3Eb__0_mC8E3ADF8FF5DDA971C36B4AE593D9F1540F2910E (void);
// 0x0000052C System.Void easyar.BufferDictionary::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void BufferDictionary__ctor_m05DAC6A1EBD39FF1D31B7C66D9648A2D6FF21127 (void);
// 0x0000052D System.Object easyar.BufferDictionary::CloneObject()
extern void BufferDictionary_CloneObject_m226827952B94424EC09E81CB8DE24ADD8A7AB073 (void);
// 0x0000052E easyar.BufferDictionary easyar.BufferDictionary::Clone()
extern void BufferDictionary_Clone_m3D65AACF15B2058C38EA5C630A8A71E0C804B47D (void);
// 0x0000052F System.Void easyar.BufferDictionary::.ctor()
extern void BufferDictionary__ctor_m8B1D8159857DD9183FA7B1C36CD2CA74EA069041 (void);
// 0x00000530 System.Int32 easyar.BufferDictionary::count()
extern void BufferDictionary_count_m5D4F6415665F7A3E7D32C7DF5BD5A75B39C1DF38 (void);
// 0x00000531 System.Boolean easyar.BufferDictionary::contains(System.String)
extern void BufferDictionary_contains_m47731845154BF9479676B88E49C3AE58B1B3BF07 (void);
// 0x00000532 easyar.Optional`1<easyar.Buffer> easyar.BufferDictionary::tryGet(System.String)
extern void BufferDictionary_tryGet_mF2C0B2C892BDA5714141763E78AB944AC0C87447 (void);
// 0x00000533 System.Void easyar.BufferDictionary::set(System.String,easyar.Buffer)
extern void BufferDictionary_set_m174A99369588270A81FDB69E5F58F8D64BC78B58 (void);
// 0x00000534 System.Boolean easyar.BufferDictionary::remove(System.String)
extern void BufferDictionary_remove_mA6E5F4646001810EA805E891F5537DF864EA9A34 (void);
// 0x00000535 System.Void easyar.BufferDictionary::clear()
extern void BufferDictionary_clear_m4F33E2473C29CAA14EE2A49556885FC0AB8A6B7E (void);
// 0x00000536 System.Void easyar.BufferDictionary/<>c::.cctor()
extern void U3CU3Ec__cctor_m83631C33FE0F04211C0C208B13EA2D7480F1DC24 (void);
// 0x00000537 System.Void easyar.BufferDictionary/<>c::.ctor()
extern void U3CU3Ec__ctor_m44B6C2E1AD3CBB5500FB27B81E30A2A0E275389D (void);
// 0x00000538 easyar.Optional`1<easyar.Buffer> easyar.BufferDictionary/<>c::<tryGet>b__6_0(easyar.Detail/OptionalOfBuffer)
extern void U3CU3Ec_U3CtryGetU3Eb__6_0_mE5FD321D54AA7005B83049E3B648E965DEAA68BB (void);
// 0x00000539 System.Void easyar.BufferPool::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void BufferPool__ctor_mCB97A4F9ECA2945A30B3E7515518D5D8CD0A166D (void);
// 0x0000053A System.Object easyar.BufferPool::CloneObject()
extern void BufferPool_CloneObject_m2B1129C26B03DB5890F4C2A698D9B51EC3B1ABC7 (void);
// 0x0000053B easyar.BufferPool easyar.BufferPool::Clone()
extern void BufferPool_Clone_m1CC7F95E98E2F0102F8A45653ECABEF459439A16 (void);
// 0x0000053C System.Void easyar.BufferPool::.ctor(System.Int32,System.Int32)
extern void BufferPool__ctor_mD4C3F3D08507E4B84FB7EB3DFE2DE7514697BC0F (void);
// 0x0000053D System.Int32 easyar.BufferPool::block_size()
extern void BufferPool_block_size_mFF35EA176E14727BB9A084832CCCD65F2F426CA9 (void);
// 0x0000053E System.Int32 easyar.BufferPool::capacity()
extern void BufferPool_capacity_mDE7A9D087B941FD53A1821D6D283121791CB9434 (void);
// 0x0000053F System.Int32 easyar.BufferPool::size()
extern void BufferPool_size_m247DE5CB827CDC3CBA15ECA263C9777F05C8E1B2 (void);
// 0x00000540 easyar.Optional`1<easyar.Buffer> easyar.BufferPool::tryAcquire()
extern void BufferPool_tryAcquire_m7FCDC71EF2175E1D1531DC31F2864D728272341C (void);
// 0x00000541 System.Void easyar.BufferPool/<>c::.cctor()
extern void U3CU3Ec__cctor_m527B3EBE09B6F7BE0A49CD5ABDE9082A34147F24 (void);
// 0x00000542 System.Void easyar.BufferPool/<>c::.ctor()
extern void U3CU3Ec__ctor_m7EEA7C50E5C1795F5A827566F161F37B43C05E10 (void);
// 0x00000543 easyar.Optional`1<easyar.Buffer> easyar.BufferPool/<>c::<tryAcquire>b__7_0(easyar.Detail/OptionalOfBuffer)
extern void U3CU3Ec_U3CtryAcquireU3Eb__7_0_m7DD05F73A3E4763753BB2933C790CE2777768E28 (void);
// 0x00000544 System.Void easyar.CameraParameters::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void CameraParameters__ctor_mF53D0258399082E9BBAD2D3E34C4CEB409B36E88 (void);
// 0x00000545 System.Object easyar.CameraParameters::CloneObject()
extern void CameraParameters_CloneObject_m365759F654816E583AFEEDDD2DB72FB9CBC27926 (void);
// 0x00000546 easyar.CameraParameters easyar.CameraParameters::Clone()
extern void CameraParameters_Clone_m8F39D18CD3B9DB63770AD273AA7B36E749031283 (void);
// 0x00000547 System.Void easyar.CameraParameters::.ctor(easyar.Vec2I,easyar.Vec2F,easyar.Vec2F,easyar.CameraDeviceType,System.Int32)
extern void CameraParameters__ctor_mEC67E27AC76DF14C51421A495C56E092F8828B25 (void);
// 0x00000548 easyar.Vec2I easyar.CameraParameters::size()
extern void CameraParameters_size_m5A08D3EE034A68FB064DEE5654D748055799E85C (void);
// 0x00000549 easyar.Vec2F easyar.CameraParameters::focalLength()
extern void CameraParameters_focalLength_mE5A438ECF9352D53170C269AE41B9A834C41B00A (void);
// 0x0000054A easyar.Vec2F easyar.CameraParameters::principalPoint()
extern void CameraParameters_principalPoint_mBA2D8F9CABF39378426F9488E1DFA6FDF23E70FA (void);
// 0x0000054B easyar.CameraDeviceType easyar.CameraParameters::cameraDeviceType()
extern void CameraParameters_cameraDeviceType_m9B6F4FAEB296E769EB507BE9740754589ECC32CF (void);
// 0x0000054C System.Int32 easyar.CameraParameters::cameraOrientation()
extern void CameraParameters_cameraOrientation_m0B71366F65782C1D572874D28E963597B86EB9FE (void);
// 0x0000054D easyar.CameraParameters easyar.CameraParameters::createWithDefaultIntrinsics(easyar.Vec2I,easyar.CameraDeviceType,System.Int32)
extern void CameraParameters_createWithDefaultIntrinsics_mDB2D0A438040A5F8D020B4DDFD1C6DA5FAF740D8 (void);
// 0x0000054E easyar.CameraParameters easyar.CameraParameters::getResized(easyar.Vec2I)
extern void CameraParameters_getResized_mA7097D3A610D2617F9F6B1EF2947A0C4A941B32B (void);
// 0x0000054F System.Int32 easyar.CameraParameters::imageOrientation(System.Int32)
extern void CameraParameters_imageOrientation_m81CB8939086AEFE115ABAB6AA5573A1EA44FDB29 (void);
// 0x00000550 System.Boolean easyar.CameraParameters::imageHorizontalFlip(System.Boolean)
extern void CameraParameters_imageHorizontalFlip_mFC73FF33F2788DF324265F4C2C03F9297856DCBD (void);
// 0x00000551 easyar.Matrix44F easyar.CameraParameters::projection(System.Single,System.Single,System.Single,System.Int32,System.Boolean,System.Boolean)
extern void CameraParameters_projection_mA20C66EBCC9733301C8EA3E9A958CFF7D8AE20E5 (void);
// 0x00000552 easyar.Matrix44F easyar.CameraParameters::imageProjection(System.Single,System.Int32,System.Boolean,System.Boolean)
extern void CameraParameters_imageProjection_mA27D176CBD0FF41E6E34C2337DFC2B1D044AE05B (void);
// 0x00000553 easyar.Vec2F easyar.CameraParameters::screenCoordinatesFromImageCoordinates(System.Single,System.Int32,System.Boolean,System.Boolean,easyar.Vec2F)
extern void CameraParameters_screenCoordinatesFromImageCoordinates_mBEC8FC46278277DD00AAEDA7A88543C31C9DF396 (void);
// 0x00000554 easyar.Vec2F easyar.CameraParameters::imageCoordinatesFromScreenCoordinates(System.Single,System.Int32,System.Boolean,System.Boolean,easyar.Vec2F)
extern void CameraParameters_imageCoordinatesFromScreenCoordinates_m91AC0C1514097466C81A7E5B7FBD05FC3BCCC0FF (void);
// 0x00000555 System.Boolean easyar.CameraParameters::equalsTo(easyar.CameraParameters)
extern void CameraParameters_equalsTo_mE3B1CB1DEF8B5E449E4E63F8DC83B3931724F087 (void);
// 0x00000556 System.Void easyar.Image::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void Image__ctor_m3A17CA109931FB1778D0C13FC5D55D207272920E (void);
// 0x00000557 System.Object easyar.Image::CloneObject()
extern void Image_CloneObject_mB995191457185397E38780E59AFAC512D6DD97B0 (void);
// 0x00000558 easyar.Image easyar.Image::Clone()
extern void Image_Clone_m3B57CEA8B825C8AA6408C266B8ED203BFD32DAB3 (void);
// 0x00000559 System.Void easyar.Image::.ctor(easyar.Buffer,easyar.PixelFormat,System.Int32,System.Int32)
extern void Image__ctor_mCBC1A7A24107EEB4E5B21860F46D2590333781DD (void);
// 0x0000055A easyar.Buffer easyar.Image::buffer()
extern void Image_buffer_m0B65E96E5DD0CAFB05510D452F82636051EDFE59 (void);
// 0x0000055B easyar.PixelFormat easyar.Image::format()
extern void Image_format_m2F44AE1A06FA17797FB7D4CF2C59AEE46DAC9BA8 (void);
// 0x0000055C System.Int32 easyar.Image::width()
extern void Image_width_m4DACAD96F77D6C3110FD681F9DEF88DB1550F7AB (void);
// 0x0000055D System.Int32 easyar.Image::height()
extern void Image_height_m946798A63C51B5546864C29CC88E2A58D01CDAEF (void);
// 0x0000055E System.Single[] easyar.Matrix44F::get_data()
extern void Matrix44F_get_data_m67C0A1BF271A6C366D67ED360291B61197CCA09F (void);
// 0x0000055F System.Void easyar.Matrix44F::set_data(System.Single[])
extern void Matrix44F_set_data_m7C3634F7119F87B60156682FA5A868E25791AB97 (void);
// 0x00000560 System.Void easyar.Matrix44F::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void Matrix44F__ctor_m08360D8DD00F4951F4B3A7905BF5F08D3D865036 (void);
// 0x00000561 System.Single[] easyar.Matrix33F::get_data()
extern void Matrix33F_get_data_m85D37D7D472AB79A7962EDCB3D01E70F85FDFBA4 (void);
// 0x00000562 System.Void easyar.Matrix33F::set_data(System.Single[])
extern void Matrix33F_set_data_m501DC97575DC9045BCBEA35973CEE131C695A5A0 (void);
// 0x00000563 System.Void easyar.Matrix33F::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void Matrix33F__ctor_m3683E7AB93523F0178BE0A9E5C8961B02A49F44E (void);
// 0x00000564 System.Double[] easyar.Vec3D::get_data()
extern void Vec3D_get_data_m899EF2E47941ACA4FB20AC16FAD0EB6F52C2820C (void);
// 0x00000565 System.Void easyar.Vec3D::set_data(System.Double[])
extern void Vec3D_set_data_mA768D5E64217EEE2119C35022EB2BBB34EBA91DA (void);
// 0x00000566 System.Void easyar.Vec3D::.ctor(System.Double,System.Double,System.Double)
extern void Vec3D__ctor_mE118A85DCBF981E4D1FB28A10DB58FBCCCFBC3C0 (void);
// 0x00000567 System.Single[] easyar.Vec4F::get_data()
extern void Vec4F_get_data_mAB650AF267950B46EC508F2C8B93F26FCF8363D0 (void);
// 0x00000568 System.Void easyar.Vec4F::set_data(System.Single[])
extern void Vec4F_set_data_m62535D380DECDD9CD28F7A778EDCCEC0B26C5FA9 (void);
// 0x00000569 System.Void easyar.Vec4F::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void Vec4F__ctor_mD5ED30DAD10566BBAE3D3F24E5130A516D77269E (void);
// 0x0000056A System.Single[] easyar.Vec3F::get_data()
extern void Vec3F_get_data_m6AB194630146D7297D96735F9E95AD83C044604B (void);
// 0x0000056B System.Void easyar.Vec3F::set_data(System.Single[])
extern void Vec3F_set_data_mEECA60CD3B2A9829F912884D06CC385A63B5DDB8 (void);
// 0x0000056C System.Void easyar.Vec3F::.ctor(System.Single,System.Single,System.Single)
extern void Vec3F__ctor_m048B740E6E14E759A3AE6BA9EDC537B35D3F32C8 (void);
// 0x0000056D System.Single[] easyar.Vec2F::get_data()
extern void Vec2F_get_data_m2BFD2DF1F83590E815DBF7FD2F18F2A74251DE13 (void);
// 0x0000056E System.Void easyar.Vec2F::set_data(System.Single[])
extern void Vec2F_set_data_m511CE6F198ED6BF129F53080ACE5255A69BC7C44 (void);
// 0x0000056F System.Void easyar.Vec2F::.ctor(System.Single,System.Single)
extern void Vec2F__ctor_m54CAE94E831DCCA2714C0B2C38581F238452933D (void);
// 0x00000570 System.Int32[] easyar.Vec4I::get_data()
extern void Vec4I_get_data_mA13BC5AC938DA168120751B0301BC644D7ADDD91 (void);
// 0x00000571 System.Void easyar.Vec4I::set_data(System.Int32[])
extern void Vec4I_set_data_mE1291E4B6CF192491813EAC0EA8BB331B474EBA2 (void);
// 0x00000572 System.Void easyar.Vec4I::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Vec4I__ctor_m9716FFE4C4B6410244BDBD51F1445186E25A6B08 (void);
// 0x00000573 System.Int32[] easyar.Vec2I::get_data()
extern void Vec2I_get_data_m8C330A4BEF24736568B464D9A97FE12108A53218 (void);
// 0x00000574 System.Void easyar.Vec2I::set_data(System.Int32[])
extern void Vec2I_set_data_mBD37D15E328F8F2746003C169F93976A499A342B (void);
// 0x00000575 System.Void easyar.Vec2I::.ctor(System.Int32,System.Int32)
extern void Vec2I__ctor_m79DAD76534551BB0FC8246F6948CE5BF9DF38045 (void);
// 0x00000576 System.Void easyar.DenseSpatialMap::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void DenseSpatialMap__ctor_m5BB2C39D4F330658465A57782CC7B2A28BA21CA8 (void);
// 0x00000577 System.Object easyar.DenseSpatialMap::CloneObject()
extern void DenseSpatialMap_CloneObject_m5AFB1E1D9CD91E5E0FC884CF2D55580C28E599B6 (void);
// 0x00000578 easyar.DenseSpatialMap easyar.DenseSpatialMap::Clone()
extern void DenseSpatialMap_Clone_m5EB116673DE5BD9080F5B7803E1EC9722E8C2621 (void);
// 0x00000579 System.Boolean easyar.DenseSpatialMap::isAvailable()
extern void DenseSpatialMap_isAvailable_m6B6574FD47D4F2FD406E7ED29CED29D5D192B808 (void);
// 0x0000057A easyar.InputFrameSink easyar.DenseSpatialMap::inputFrameSink()
extern void DenseSpatialMap_inputFrameSink_m41BFF5AEC85CD2B62B9D34C5DF6170D5E902B2FA (void);
// 0x0000057B System.Int32 easyar.DenseSpatialMap::bufferRequirement()
extern void DenseSpatialMap_bufferRequirement_m12A7A57946D3953E8CEBC6E14F9847E338F47AC3 (void);
// 0x0000057C easyar.DenseSpatialMap easyar.DenseSpatialMap::create()
extern void DenseSpatialMap_create_m035D3B83C875BFA994E7915E6A5ADF7F3E5C1520 (void);
// 0x0000057D System.Boolean easyar.DenseSpatialMap::start()
extern void DenseSpatialMap_start_mDA01BBC0029B5E5C4A6E6366662F9C4F42710F67 (void);
// 0x0000057E System.Void easyar.DenseSpatialMap::stop()
extern void DenseSpatialMap_stop_m624752486F3FBFC8C0FF8827EE955CCE91E2D29C (void);
// 0x0000057F System.Void easyar.DenseSpatialMap::close()
extern void DenseSpatialMap_close_mE781ADFA39C8FC16C5FA93A3F6FE3014EF29A1DB (void);
// 0x00000580 easyar.SceneMesh easyar.DenseSpatialMap::getMesh()
extern void DenseSpatialMap_getMesh_m2495EB5C19043082157D3003F7821CD63B556708 (void);
// 0x00000581 System.Boolean easyar.DenseSpatialMap::updateSceneMesh(System.Boolean)
extern void DenseSpatialMap_updateSceneMesh_m5874771E5E9D1367191AC73287363967B8175F67 (void);
// 0x00000582 System.Void easyar.BlockInfo::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void BlockInfo__ctor_m56026AA781BF97B7CE29DF961342F00D1B57FB52 (void);
// 0x00000583 System.Void easyar.SceneMesh::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void SceneMesh__ctor_mAC41970C535E5CFE31957B36C4ED44161AA480B8 (void);
// 0x00000584 System.Object easyar.SceneMesh::CloneObject()
extern void SceneMesh_CloneObject_mF808CF8F328E919C18930BFF8057E5CC6B21249D (void);
// 0x00000585 easyar.SceneMesh easyar.SceneMesh::Clone()
extern void SceneMesh_Clone_mFD22EC5DEEDF03AFD384FC50BF4A4B1BA07F2797 (void);
// 0x00000586 System.Int32 easyar.SceneMesh::getNumOfVertexAll()
extern void SceneMesh_getNumOfVertexAll_m7DC63EAA58B0492CED10933128E9747D56DEB3F8 (void);
// 0x00000587 System.Int32 easyar.SceneMesh::getNumOfIndexAll()
extern void SceneMesh_getNumOfIndexAll_m6B193D9A8E8E581751506375B68FA8DE5CB0C64A (void);
// 0x00000588 easyar.Buffer easyar.SceneMesh::getVerticesAll()
extern void SceneMesh_getVerticesAll_m35A41676BF505C872EA9B352C30BB08F1D500E7D (void);
// 0x00000589 easyar.Buffer easyar.SceneMesh::getNormalsAll()
extern void SceneMesh_getNormalsAll_m2A1C7B1C1AD74CF95F1C95284072F2EF0ABD7010 (void);
// 0x0000058A easyar.Buffer easyar.SceneMesh::getIndicesAll()
extern void SceneMesh_getIndicesAll_mEC6D584B9A11ACAB3B40991A90296370EC27D749 (void);
// 0x0000058B System.Int32 easyar.SceneMesh::getNumOfVertexIncremental()
extern void SceneMesh_getNumOfVertexIncremental_m82473C34926A654823CE08884E4EAAC0B53B71AC (void);
// 0x0000058C System.Int32 easyar.SceneMesh::getNumOfIndexIncremental()
extern void SceneMesh_getNumOfIndexIncremental_mB309AC2B20E1107DAFA433FC40FAAC3CD6DE24B0 (void);
// 0x0000058D easyar.Buffer easyar.SceneMesh::getVerticesIncremental()
extern void SceneMesh_getVerticesIncremental_m90A4B084DAB8ABF96DFEDE372680F46D1A5B7B0A (void);
// 0x0000058E easyar.Buffer easyar.SceneMesh::getNormalsIncremental()
extern void SceneMesh_getNormalsIncremental_m273A33F15DBCAFCEA33B90FE845A6F79937928F3 (void);
// 0x0000058F easyar.Buffer easyar.SceneMesh::getIndicesIncremental()
extern void SceneMesh_getIndicesIncremental_m77276CBF70BD66A5B2575660C5B19FFACF621A29 (void);
// 0x00000590 System.Collections.Generic.List`1<easyar.BlockInfo> easyar.SceneMesh::getBlocksInfoIncremental()
extern void SceneMesh_getBlocksInfoIncremental_m4434A60FF52661FCAE293AD6BC82FE0A6475CDF5 (void);
// 0x00000591 System.Single easyar.SceneMesh::getBlockDimensionInMeters()
extern void SceneMesh_getBlockDimensionInMeters_mD647782340C84AABE7C954F74E68ACE57B10B6F1 (void);
// 0x00000592 System.Void easyar.AccelerometerResult::.ctor(System.Single,System.Single,System.Single,System.Double)
extern void AccelerometerResult__ctor_m6B5CEB6914B368B1E58761FF3A309544749E2716 (void);
// 0x00000593 System.Void easyar.ARCoreCameraDevice::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void ARCoreCameraDevice__ctor_mAEDACEC9650C9F4A84D9AD2EA0E6A8579FEE1CFB (void);
// 0x00000594 System.Object easyar.ARCoreCameraDevice::CloneObject()
extern void ARCoreCameraDevice_CloneObject_mB9A646252AE4F3EE73EF3D4CFC1B67AE7556B30E (void);
// 0x00000595 easyar.ARCoreCameraDevice easyar.ARCoreCameraDevice::Clone()
extern void ARCoreCameraDevice_Clone_m892747C253B9316A7463ED38FFBFAC6E3DB86E45 (void);
// 0x00000596 System.Void easyar.ARCoreCameraDevice::.ctor()
extern void ARCoreCameraDevice__ctor_m154ED25404E862E35AD29E064342DA0F6EBD09D5 (void);
// 0x00000597 System.Boolean easyar.ARCoreCameraDevice::isAvailable()
extern void ARCoreCameraDevice_isAvailable_mC38AE95378C93C93CF3537961AB608CD9BC43110 (void);
// 0x00000598 System.Int32 easyar.ARCoreCameraDevice::bufferCapacity()
extern void ARCoreCameraDevice_bufferCapacity_m107E36E69C56A7041CA64AE1387758508AA6E51A (void);
// 0x00000599 System.Void easyar.ARCoreCameraDevice::setBufferCapacity(System.Int32)
extern void ARCoreCameraDevice_setBufferCapacity_mC7E39E57A21ADC3D71CD896FA3754385A54AE36A (void);
// 0x0000059A easyar.InputFrameSource easyar.ARCoreCameraDevice::inputFrameSource()
extern void ARCoreCameraDevice_inputFrameSource_m364B9618EB13A9ABBBB7AFBEDA562BA17CABA470 (void);
// 0x0000059B System.Boolean easyar.ARCoreCameraDevice::start()
extern void ARCoreCameraDevice_start_mF639D291680E787DB2790C06529900D73771A4AE (void);
// 0x0000059C System.Void easyar.ARCoreCameraDevice::stop()
extern void ARCoreCameraDevice_stop_mF9BFF73E2674A71D2AA87D0A29011B9528A44588 (void);
// 0x0000059D System.Void easyar.ARCoreCameraDevice::close()
extern void ARCoreCameraDevice_close_mC2EAFD194FC703248E574A79AB6978581C343017 (void);
// 0x0000059E System.Void easyar.ARKitCameraDevice::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void ARKitCameraDevice__ctor_m707437B9257ADD75B3B7C3D3D330561782F8AC6C (void);
// 0x0000059F System.Object easyar.ARKitCameraDevice::CloneObject()
extern void ARKitCameraDevice_CloneObject_mEF03F52792010F9966C9AB72C4270F28C0592CC9 (void);
// 0x000005A0 easyar.ARKitCameraDevice easyar.ARKitCameraDevice::Clone()
extern void ARKitCameraDevice_Clone_m1E3D2158103B93B0CADDDF0205F136D4949B37B5 (void);
// 0x000005A1 System.Void easyar.ARKitCameraDevice::.ctor()
extern void ARKitCameraDevice__ctor_m7FC6BE878E2A05951A09EF6664B54B8DF7B765F7 (void);
// 0x000005A2 System.Boolean easyar.ARKitCameraDevice::isAvailable()
extern void ARKitCameraDevice_isAvailable_mCC4AF95C7EBC02EA5B254EEBF50956E34D485D6D (void);
// 0x000005A3 System.Int32 easyar.ARKitCameraDevice::bufferCapacity()
extern void ARKitCameraDevice_bufferCapacity_mFF0E9B37001CD204EBEC63CF319F6E6073266007 (void);
// 0x000005A4 System.Void easyar.ARKitCameraDevice::setBufferCapacity(System.Int32)
extern void ARKitCameraDevice_setBufferCapacity_m21C8B38B98683576978C6723996B16E92948A474 (void);
// 0x000005A5 easyar.InputFrameSource easyar.ARKitCameraDevice::inputFrameSource()
extern void ARKitCameraDevice_inputFrameSource_mE64BA2EC3E64FB2D5436F7676D436121E08BEC22 (void);
// 0x000005A6 System.Boolean easyar.ARKitCameraDevice::start()
extern void ARKitCameraDevice_start_m9C78E1885DB9F6AF821D04059F659D14E6A51378 (void);
// 0x000005A7 System.Void easyar.ARKitCameraDevice::stop()
extern void ARKitCameraDevice_stop_mE89AF469B150603FDCB5742C1239866CA2035D6C (void);
// 0x000005A8 System.Void easyar.ARKitCameraDevice::close()
extern void ARKitCameraDevice_close_m426689D5398519BA49A78D1A8271FA4DD52A2036 (void);
// 0x000005A9 System.Void easyar.CameraDevice::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void CameraDevice__ctor_mA4300807CC1F45E23E64580BD640ACEC88B400AF (void);
// 0x000005AA System.Object easyar.CameraDevice::CloneObject()
extern void CameraDevice_CloneObject_mC30B7D6E2FDB67F994249CCB19BA76E87BC82613 (void);
// 0x000005AB easyar.CameraDevice easyar.CameraDevice::Clone()
extern void CameraDevice_Clone_m86FAD90C8A2E66528CB6A494BF8A56808F3B35E9 (void);
// 0x000005AC System.Void easyar.CameraDevice::.ctor()
extern void CameraDevice__ctor_mC79009D8FA04772615607AE2414194D744519EC5 (void);
// 0x000005AD System.Boolean easyar.CameraDevice::isAvailable()
extern void CameraDevice_isAvailable_mC2DF7EC79FF272120FF31F0113E4732271D532B5 (void);
// 0x000005AE easyar.AndroidCameraApiType easyar.CameraDevice::androidCameraApiType()
extern void CameraDevice_androidCameraApiType_m8F819329DB6CEAE655C14E0699B726CD619FC2B6 (void);
// 0x000005AF System.Void easyar.CameraDevice::setAndroidCameraApiType(easyar.AndroidCameraApiType)
extern void CameraDevice_setAndroidCameraApiType_m0C0467F6FEC395BDB82EAF94061B3B930443FBC6 (void);
// 0x000005B0 System.Int32 easyar.CameraDevice::bufferCapacity()
extern void CameraDevice_bufferCapacity_m8D3CCA91D44C67422024236D2FC6706C3A6E04A8 (void);
// 0x000005B1 System.Void easyar.CameraDevice::setBufferCapacity(System.Int32)
extern void CameraDevice_setBufferCapacity_m7CB10C72113692B3A221B5477949EDFDCD2C347F (void);
// 0x000005B2 easyar.InputFrameSource easyar.CameraDevice::inputFrameSource()
extern void CameraDevice_inputFrameSource_m4556B6355CCE3A0E3A10D468F7522B0B6191EB1B (void);
// 0x000005B3 System.Void easyar.CameraDevice::setStateChangedCallback(easyar.CallbackScheduler,easyar.Optional`1<System.Action`1<easyar.CameraState>>)
extern void CameraDevice_setStateChangedCallback_m79E20475BF8E1AC96A2647EDD2271BD2AD99D9EE (void);
// 0x000005B4 System.Void easyar.CameraDevice::requestPermissions(easyar.CallbackScheduler,easyar.Optional`1<System.Action`2<easyar.PermissionStatus,System.String>>)
extern void CameraDevice_requestPermissions_mE6DEB5F0EFE6DC377351CA1BC3028B5F83865372 (void);
// 0x000005B5 System.Int32 easyar.CameraDevice::cameraCount()
extern void CameraDevice_cameraCount_mBA8183A787FCEEE08F92773B60F2C649755A83E5 (void);
// 0x000005B6 System.Boolean easyar.CameraDevice::openWithIndex(System.Int32)
extern void CameraDevice_openWithIndex_m5741C367F102F37054FF604D8FC5D51AD2CEEBD9 (void);
// 0x000005B7 System.Boolean easyar.CameraDevice::openWithSpecificType(easyar.CameraDeviceType)
extern void CameraDevice_openWithSpecificType_m18528F4CF8844CBCA093B75114980B64ECEBC3C8 (void);
// 0x000005B8 System.Boolean easyar.CameraDevice::openWithPreferredType(easyar.CameraDeviceType)
extern void CameraDevice_openWithPreferredType_mB46DF3D308E5D9C5493AF7985D3375EE5718DF3D (void);
// 0x000005B9 System.Boolean easyar.CameraDevice::start()
extern void CameraDevice_start_m5F41109B9C1980B748AC594BEB7AC83FD9F4B5D4 (void);
// 0x000005BA System.Void easyar.CameraDevice::stop()
extern void CameraDevice_stop_m097BCE97FA5350AEC1F03E6D0D5AA89211BA905A (void);
// 0x000005BB System.Void easyar.CameraDevice::close()
extern void CameraDevice_close_mA16F3A0C432F99B32F573A3C2BFF9F01E4D0DCC2 (void);
// 0x000005BC System.Int32 easyar.CameraDevice::index()
extern void CameraDevice_index_m1A2352F68C8EFB7F07A63936C724FC22506331A4 (void);
// 0x000005BD easyar.CameraDeviceType easyar.CameraDevice::type()
extern void CameraDevice_type_mE077C41A6D61E80D93185B1752202DD5D1D30981 (void);
// 0x000005BE easyar.CameraParameters easyar.CameraDevice::cameraParameters()
extern void CameraDevice_cameraParameters_m27A63E80272CEDE038377CB93B2079210613AE7B (void);
// 0x000005BF System.Void easyar.CameraDevice::setCameraParameters(easyar.CameraParameters)
extern void CameraDevice_setCameraParameters_mDB9FF3997B32F60E2CC2948434989675165BE762 (void);
// 0x000005C0 easyar.Vec2I easyar.CameraDevice::size()
extern void CameraDevice_size_m2B5E07D077A823C96B3D0613F26101974A6457F0 (void);
// 0x000005C1 System.Int32 easyar.CameraDevice::supportedSizeCount()
extern void CameraDevice_supportedSizeCount_mB86F2E42FE2A5B45AB19019B87C7AFD4DC0B6286 (void);
// 0x000005C2 easyar.Vec2I easyar.CameraDevice::supportedSize(System.Int32)
extern void CameraDevice_supportedSize_mB034074836F2ACC09F0EBEA26A972455B086A9D0 (void);
// 0x000005C3 System.Boolean easyar.CameraDevice::setSize(easyar.Vec2I)
extern void CameraDevice_setSize_m62AE09B493AEADDD05D514897953808109979E53 (void);
// 0x000005C4 System.Int32 easyar.CameraDevice::supportedFrameRateRangeCount()
extern void CameraDevice_supportedFrameRateRangeCount_m95AB3C6964C40380D2CFCE1C5162E408F6CEAFD6 (void);
// 0x000005C5 System.Single easyar.CameraDevice::supportedFrameRateRangeLower(System.Int32)
extern void CameraDevice_supportedFrameRateRangeLower_m3CAAEF79A75991245552564D5CCE3BC9B0C8D012 (void);
// 0x000005C6 System.Single easyar.CameraDevice::supportedFrameRateRangeUpper(System.Int32)
extern void CameraDevice_supportedFrameRateRangeUpper_m327F6F7BE1ABE59690A75D8EC5C9D83F401318E2 (void);
// 0x000005C7 System.Int32 easyar.CameraDevice::frameRateRange()
extern void CameraDevice_frameRateRange_m2381B13ECC287979AFC0896AC773E7278C71FC14 (void);
// 0x000005C8 System.Boolean easyar.CameraDevice::setFrameRateRange(System.Int32)
extern void CameraDevice_setFrameRateRange_mA5E58D39FA142E4CA04B5A06C5DC3573A52D9F5B (void);
// 0x000005C9 System.Boolean easyar.CameraDevice::setFlashTorchMode(System.Boolean)
extern void CameraDevice_setFlashTorchMode_m67989847859EE926D8115F9E8B3B415883AAED01 (void);
// 0x000005CA System.Boolean easyar.CameraDevice::setFocusMode(easyar.CameraDeviceFocusMode)
extern void CameraDevice_setFocusMode_m0E810E1DF85A53658C3A52DBBBAEFD03A8CC131F (void);
// 0x000005CB System.Boolean easyar.CameraDevice::autoFocus()
extern void CameraDevice_autoFocus_m28FC50CFBE04C54E171C47ED6842AF024BFEC6AA (void);
// 0x000005CC System.Void easyar.CameraDevice/<>c::.cctor()
extern void U3CU3Ec__cctor_m56DC48F1AA57BCD8E0C0A0928C29F7D0DDB9F5F6 (void);
// 0x000005CD System.Void easyar.CameraDevice/<>c::.ctor()
extern void U3CU3Ec__ctor_m95E32A3042FD542402DCF80B63AEBD99EC2B6903 (void);
// 0x000005CE easyar.Detail/OptionalOfFunctorOfVoidFromCameraState easyar.CameraDevice/<>c::<setStateChangedCallback>b__10_0(easyar.Optional`1<System.Action`1<easyar.CameraState>>)
extern void U3CU3Ec_U3CsetStateChangedCallbackU3Eb__10_0_m594EE6FE03345C3592A0D3E38B51F56189DF1652 (void);
// 0x000005CF easyar.Detail/OptionalOfFunctorOfVoidFromPermissionStatusAndString easyar.CameraDevice/<>c::<requestPermissions>b__11_0(easyar.Optional`1<System.Action`2<easyar.PermissionStatus,System.String>>)
extern void U3CU3Ec_U3CrequestPermissionsU3Eb__11_0_mAAFECC9D58FCA8A81F9AC6E23FC3A26551A00BD0 (void);
// 0x000005D0 easyar.AndroidCameraApiType easyar.CameraDeviceSelector::getAndroidCameraApiType(easyar.CameraDevicePreference)
extern void CameraDeviceSelector_getAndroidCameraApiType_m1D47715F5C4A9C82783CE1D1D3286BF286C363B6 (void);
// 0x000005D1 easyar.CameraDevice easyar.CameraDeviceSelector::createCameraDevice(easyar.CameraDevicePreference)
extern void CameraDeviceSelector_createCameraDevice_m976B4B095BE5DD8EF6B42966E8414EC50EEB901B (void);
// 0x000005D2 easyar.CameraDeviceFocusMode easyar.CameraDeviceSelector::getFocusMode(easyar.CameraDevicePreference)
extern void CameraDeviceSelector_getFocusMode_m2D8E298925AA0D5513C9BFCCC342C445E38929BA (void);
// 0x000005D3 System.Void easyar.CameraDeviceSelector::.ctor()
extern void CameraDeviceSelector__ctor_m005E4293B632B7C17388EBE791006A1A8B8715E8 (void);
// 0x000005D4 System.Void easyar.MagnetometerResult::.ctor(System.Single,System.Single,System.Single,System.Double)
extern void MagnetometerResult__ctor_mD167F23B0247DBCF93D8EBA039DDA2822DD8E3ED (void);
// 0x000005D5 System.Void easyar.SurfaceTrackerResult::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void SurfaceTrackerResult__ctor_m7F947AEABC6F84ABFDF837C4AFF5C9184F50479D (void);
// 0x000005D6 System.Object easyar.SurfaceTrackerResult::CloneObject()
extern void SurfaceTrackerResult_CloneObject_m83D3A5DA96518D39DFBE63FB31912DB5EA7C7878 (void);
// 0x000005D7 easyar.SurfaceTrackerResult easyar.SurfaceTrackerResult::Clone()
extern void SurfaceTrackerResult_Clone_m326332F14411FA391812B705D8D8072721A8B692 (void);
// 0x000005D8 easyar.Matrix44F easyar.SurfaceTrackerResult::transform()
extern void SurfaceTrackerResult_transform_mAE43C2EECDCF3277A602CCBAE181567153C85D1B (void);
// 0x000005D9 System.Void easyar.SurfaceTracker::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void SurfaceTracker__ctor_m39E346A1231B0AEB65B01018EE9B9F36EDE1223A (void);
// 0x000005DA System.Object easyar.SurfaceTracker::CloneObject()
extern void SurfaceTracker_CloneObject_m3F3F9446CE2DB92115BD6BFADCC6C8B9DE44891B (void);
// 0x000005DB easyar.SurfaceTracker easyar.SurfaceTracker::Clone()
extern void SurfaceTracker_Clone_mD376A8CFEB9537FC279EB49DE87A8F40E3647977 (void);
// 0x000005DC System.Boolean easyar.SurfaceTracker::isAvailable()
extern void SurfaceTracker_isAvailable_mF38E81B97AD39BB7F6585F7E928922CE04E0D003 (void);
// 0x000005DD easyar.InputFrameSink easyar.SurfaceTracker::inputFrameSink()
extern void SurfaceTracker_inputFrameSink_m5364A7662F79B66A6138E04CB2E134A7D65386F7 (void);
// 0x000005DE System.Int32 easyar.SurfaceTracker::bufferRequirement()
extern void SurfaceTracker_bufferRequirement_m007C8BC91C5978F0F24E0FDB8ED84DD015C921A2 (void);
// 0x000005DF easyar.OutputFrameSource easyar.SurfaceTracker::outputFrameSource()
extern void SurfaceTracker_outputFrameSource_m77935476AF414D786F06AAB495841390A1088973 (void);
// 0x000005E0 easyar.SurfaceTracker easyar.SurfaceTracker::create()
extern void SurfaceTracker_create_mA471FE9DD43F301B5239A57B904512D2169A5559 (void);
// 0x000005E1 System.Boolean easyar.SurfaceTracker::start()
extern void SurfaceTracker_start_m31F700B9800709871C2B9704BF3AAC872D0337F4 (void);
// 0x000005E2 System.Void easyar.SurfaceTracker::stop()
extern void SurfaceTracker_stop_mA51E429D1506FC349FAA1AD81FDB484F0AB21380 (void);
// 0x000005E3 System.Void easyar.SurfaceTracker::close()
extern void SurfaceTracker_close_m49B362D2279077EE69F9668080BBB9913DEBA4E9 (void);
// 0x000005E4 System.Void easyar.SurfaceTracker::alignTargetToCameraImagePoint(easyar.Vec2F)
extern void SurfaceTracker_alignTargetToCameraImagePoint_mE76790785A7041778B722C3207D46F74A7ABB838 (void);
// 0x000005E5 System.Void easyar.MotionTrackerCameraDevice::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void MotionTrackerCameraDevice__ctor_m3C17852987A8D09F55DD80DBFF5C02B7866EE023 (void);
// 0x000005E6 System.Object easyar.MotionTrackerCameraDevice::CloneObject()
extern void MotionTrackerCameraDevice_CloneObject_mD72AEC7F6AABF7ECC3FB68385F926E6D54978703 (void);
// 0x000005E7 easyar.MotionTrackerCameraDevice easyar.MotionTrackerCameraDevice::Clone()
extern void MotionTrackerCameraDevice_Clone_m147C5193D30DC076C6D004FBBA54ABBC034C10B4 (void);
// 0x000005E8 System.Void easyar.MotionTrackerCameraDevice::.ctor()
extern void MotionTrackerCameraDevice__ctor_mF26DC562E6E4C4740D87354C1DDEC4A126D84C0F (void);
// 0x000005E9 System.Boolean easyar.MotionTrackerCameraDevice::isAvailable()
extern void MotionTrackerCameraDevice_isAvailable_m8BC93BECD04D2E3445696927404176B9D7066188 (void);
// 0x000005EA easyar.MotionTrackerCameraDeviceQualityLevel easyar.MotionTrackerCameraDevice::getQualityLevel()
extern void MotionTrackerCameraDevice_getQualityLevel_m4B6496FEFD8B0E3E3A6B8E877362114CDAFFEF6E (void);
// 0x000005EB System.Boolean easyar.MotionTrackerCameraDevice::setFrameRateType(easyar.MotionTrackerCameraDeviceFPS)
extern void MotionTrackerCameraDevice_setFrameRateType_mF86259740166B8359AFE2652CE556A57E03E9F49 (void);
// 0x000005EC System.Boolean easyar.MotionTrackerCameraDevice::setFocusMode(easyar.MotionTrackerCameraDeviceFocusMode)
extern void MotionTrackerCameraDevice_setFocusMode_mE11DB215BE7CBD8DF6AF4C638F0B7C8CC7F65B99 (void);
// 0x000005ED System.Boolean easyar.MotionTrackerCameraDevice::setFrameResolutionType(easyar.MotionTrackerCameraDeviceResolution)
extern void MotionTrackerCameraDevice_setFrameResolutionType_m92CF0535F26D8F58189E00A7332C46424CB67599 (void);
// 0x000005EE System.Boolean easyar.MotionTrackerCameraDevice::setTrackingMode(easyar.MotionTrackerCameraDeviceTrackingMode)
extern void MotionTrackerCameraDevice_setTrackingMode_m9694E1D9A30B2CF569900CCCD07FDC73C151A9D1 (void);
// 0x000005EF System.Void easyar.MotionTrackerCameraDevice::setBufferCapacity(System.Int32)
extern void MotionTrackerCameraDevice_setBufferCapacity_mE36E7B5E21B7213B192E5FCAC41CDFF49E5827F2 (void);
// 0x000005F0 System.Int32 easyar.MotionTrackerCameraDevice::bufferCapacity()
extern void MotionTrackerCameraDevice_bufferCapacity_mAB3E322CEA29AF30A4A4B6A35E3A8547345C85AD (void);
// 0x000005F1 easyar.InputFrameSource easyar.MotionTrackerCameraDevice::inputFrameSource()
extern void MotionTrackerCameraDevice_inputFrameSource_m71533CE3ECBB279E8FE1A02B84A5CF553BDDE9C8 (void);
// 0x000005F2 System.Boolean easyar.MotionTrackerCameraDevice::start()
extern void MotionTrackerCameraDevice_start_m2DE73E8899A1D5EB0F3206AD1B2C8E46FBC79E6E (void);
// 0x000005F3 System.Void easyar.MotionTrackerCameraDevice::stop()
extern void MotionTrackerCameraDevice_stop_m638E96EF3CC032B46424597BC4A07381515ED990 (void);
// 0x000005F4 System.Void easyar.MotionTrackerCameraDevice::close()
extern void MotionTrackerCameraDevice_close_mF6D8E415DF82B25D17C36A2432886D587AAF8D63 (void);
// 0x000005F5 System.Collections.Generic.List`1<easyar.Vec3F> easyar.MotionTrackerCameraDevice::hitTestAgainstPointCloud(easyar.Vec2F)
extern void MotionTrackerCameraDevice_hitTestAgainstPointCloud_mCA9CE1F287E0AD96DA629D7C4FE6471B383D2B2B (void);
// 0x000005F6 System.Collections.Generic.List`1<easyar.Vec3F> easyar.MotionTrackerCameraDevice::hitTestAgainstHorizontalPlane(easyar.Vec2F)
extern void MotionTrackerCameraDevice_hitTestAgainstHorizontalPlane_mEDFDC32F066AEC4329DC361B0ADA4B3D78500CAC (void);
// 0x000005F7 System.Collections.Generic.List`1<easyar.Vec3F> easyar.MotionTrackerCameraDevice::getLocalPointsCloud()
extern void MotionTrackerCameraDevice_getLocalPointsCloud_mAAB4C53D1F9A79F92D954E5181792CD9D2401605 (void);
// 0x000005F8 System.Void easyar.InputFrameRecorder::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void InputFrameRecorder__ctor_mA84AF7039033030E16CA40897C574C5D4F333346 (void);
// 0x000005F9 System.Object easyar.InputFrameRecorder::CloneObject()
extern void InputFrameRecorder_CloneObject_m639FCABAF42760CA99D3EAAC6D981E527E980095 (void);
// 0x000005FA easyar.InputFrameRecorder easyar.InputFrameRecorder::Clone()
extern void InputFrameRecorder_Clone_mE8A60C3B0365DDA74C7B53769649AEF0EEC40F02 (void);
// 0x000005FB easyar.InputFrameSink easyar.InputFrameRecorder::input()
extern void InputFrameRecorder_input_mEAF91E579CFE7DFA400E2F33C4B4F3E48375CBE9 (void);
// 0x000005FC System.Int32 easyar.InputFrameRecorder::bufferRequirement()
extern void InputFrameRecorder_bufferRequirement_m8BCDF510F87BEA9215A2F70D291C24924890043F (void);
// 0x000005FD easyar.InputFrameSource easyar.InputFrameRecorder::output()
extern void InputFrameRecorder_output_mDF1ED83C9137703917FAE94196096B62ADC01BB9 (void);
// 0x000005FE easyar.InputFrameRecorder easyar.InputFrameRecorder::create()
extern void InputFrameRecorder_create_m2890A25B4B9A2C63EB6C7517AD39D50AE97737BF (void);
// 0x000005FF System.Boolean easyar.InputFrameRecorder::start(System.String,System.Int32)
extern void InputFrameRecorder_start_m2DA0510E1EE5CC85BDE51C7A156BBEC2ED022551 (void);
// 0x00000600 System.Void easyar.InputFrameRecorder::stop()
extern void InputFrameRecorder_stop_mCD1782C3989F3DF1F7A0777A54B43C23454E2AE0 (void);
// 0x00000601 System.Void easyar.InputFramePlayer::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void InputFramePlayer__ctor_m686FF6D124EDE43E2255C0114E0B6D633163D00C (void);
// 0x00000602 System.Object easyar.InputFramePlayer::CloneObject()
extern void InputFramePlayer_CloneObject_mE7E06D95D4C6D4189C6B9524711F9C6E13A946F1 (void);
// 0x00000603 easyar.InputFramePlayer easyar.InputFramePlayer::Clone()
extern void InputFramePlayer_Clone_mA1DAD872962E6DA942BA6A6BFDC1D6EDB441F522 (void);
// 0x00000604 easyar.InputFrameSource easyar.InputFramePlayer::output()
extern void InputFramePlayer_output_mF8418E49369189C5BBA0B69A66294BC7468B244C (void);
// 0x00000605 easyar.InputFramePlayer easyar.InputFramePlayer::create()
extern void InputFramePlayer_create_mF5E3DC126A7131C25633FE677A6D8EC9A6B63303 (void);
// 0x00000606 System.Boolean easyar.InputFramePlayer::start(System.String)
extern void InputFramePlayer_start_m38A3BCBF6D4CF0CFA042CF4B721AE77FC4DC11C9 (void);
// 0x00000607 System.Void easyar.InputFramePlayer::stop()
extern void InputFramePlayer_stop_m1C3BE269E1356E65B93D8FDCAE3ED7AE30BB659F (void);
// 0x00000608 System.Void easyar.InputFramePlayer::pause()
extern void InputFramePlayer_pause_m0053EA1F8FCBF581CB9684BB90453321E5980C7E (void);
// 0x00000609 System.Boolean easyar.InputFramePlayer::resume()
extern void InputFramePlayer_resume_m4C82C6E4F5CC6C3437CD1AAAFC82CC94A202C7A4 (void);
// 0x0000060A System.Double easyar.InputFramePlayer::totalTime()
extern void InputFramePlayer_totalTime_m7854A0C11FB86D16CC86F99C3497C0C5107F7F1C (void);
// 0x0000060B System.Double easyar.InputFramePlayer::currentTime()
extern void InputFramePlayer_currentTime_m009C2F432E987987C1C104507C101B0FE5B213EE (void);
// 0x0000060C System.Int32 easyar.InputFramePlayer::initalScreenRotation()
extern void InputFramePlayer_initalScreenRotation_m55C52B6A1A7A158CB72DE898D9891C7578D48A22 (void);
// 0x0000060D System.Boolean easyar.InputFramePlayer::isCompleted()
extern void InputFramePlayer_isCompleted_m1F9BD2B6469C2BDF5DD50401A8573956D7D11C86 (void);
// 0x0000060E System.Void easyar.CallbackScheduler::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void CallbackScheduler__ctor_m1FF487731C76639AA70B1C01B0119C5CB3B0908D (void);
// 0x0000060F System.Object easyar.CallbackScheduler::CloneObject()
extern void CallbackScheduler_CloneObject_mF1D5B9116B68AE6E2345FA12B5256FE7EB51D6A3 (void);
// 0x00000610 easyar.CallbackScheduler easyar.CallbackScheduler::Clone()
extern void CallbackScheduler_Clone_m68E7D2D53AE88A6C7F449D49B6372684F0D2C580 (void);
// 0x00000611 System.Void easyar.DelayedCallbackScheduler::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void DelayedCallbackScheduler__ctor_m9C0D34522C1CEF32F46800BBD017908FDBBD9D74 (void);
// 0x00000612 System.Object easyar.DelayedCallbackScheduler::CloneObject()
extern void DelayedCallbackScheduler_CloneObject_m080302D1CE9ECB6969262D245FF509C06A048D2D (void);
// 0x00000613 easyar.DelayedCallbackScheduler easyar.DelayedCallbackScheduler::Clone()
extern void DelayedCallbackScheduler_Clone_mCED824236552F2383F03BFB974E004BAA21CE96D (void);
// 0x00000614 System.Void easyar.DelayedCallbackScheduler::.ctor()
extern void DelayedCallbackScheduler__ctor_m486B5C0EA07183C990EDA2477B400CB0BBE2887E (void);
// 0x00000615 System.Boolean easyar.DelayedCallbackScheduler::runOne()
extern void DelayedCallbackScheduler_runOne_m6B365DDFC8C9206F0C3D9A5C587D93713381384D (void);
// 0x00000616 System.Void easyar.ImmediateCallbackScheduler::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void ImmediateCallbackScheduler__ctor_m5067F45CC54460DA2E20D12084221FA66BCC25EE (void);
// 0x00000617 System.Object easyar.ImmediateCallbackScheduler::CloneObject()
extern void ImmediateCallbackScheduler_CloneObject_m7A70C79A5B2639469AD4BC804ABD4E5DF2222521 (void);
// 0x00000618 easyar.ImmediateCallbackScheduler easyar.ImmediateCallbackScheduler::Clone()
extern void ImmediateCallbackScheduler_Clone_m73BA43F3EA7DA9C2D7F144E27995DC6423E8E6D5 (void);
// 0x00000619 easyar.ImmediateCallbackScheduler easyar.ImmediateCallbackScheduler::getDefault()
extern void ImmediateCallbackScheduler_getDefault_m07A4A8FC45D20C1F0AB07672BAC6B94BAAADC991 (void);
// 0x0000061A easyar.Buffer easyar.JniUtility::wrapByteArray(System.IntPtr,System.Boolean,System.Action)
extern void JniUtility_wrapByteArray_mF0E256B4A4D47CFA663A79016100590E05D99B61 (void);
// 0x0000061B easyar.Buffer easyar.JniUtility::wrapBuffer(System.IntPtr,System.Action)
extern void JniUtility_wrapBuffer_m022A36615A6FA835400628E778B9DA0F07127514 (void);
// 0x0000061C System.IntPtr easyar.JniUtility::getDirectBufferAddress(System.IntPtr)
extern void JniUtility_getDirectBufferAddress_m23CE64B0247A8FF3586818E3D8E9E52D43D0C780 (void);
// 0x0000061D System.Void easyar.JniUtility::.ctor()
extern void JniUtility__ctor_m8C3B9D38590032F89C79D21AD71ECD268FD544A5 (void);
// 0x0000061E System.Void easyar.Log::setLogFunc(System.Action`2<easyar.LogLevel,System.String>)
extern void Log_setLogFunc_m12F9A4378D2E7CC1248328F2A0C199107E6DE88C (void);
// 0x0000061F System.Void easyar.Log::setLogFuncWithScheduler(easyar.CallbackScheduler,System.Action`2<easyar.LogLevel,System.String>)
extern void Log_setLogFuncWithScheduler_m7C957E3FCF5BC4C19D1E8B6053A3C051F661BD06 (void);
// 0x00000620 System.Void easyar.Log::resetLogFunc()
extern void Log_resetLogFunc_m4B495023D2A347F44CAC40BCC84E00774AF335AA (void);
// 0x00000621 System.Void easyar.Log::.ctor()
extern void Log__ctor_mA320166FA0DEC6584B471E9CEB28467F0180151C (void);
// 0x00000622 System.Void easyar.Storage::setAssetDirPath(System.String)
extern void Storage_setAssetDirPath_mC6288D4C79ED1CDDEDA1A1B763127355CE987F07 (void);
// 0x00000623 System.Void easyar.Storage::.ctor()
extern void Storage__ctor_mB027569BC3242F71B919CFCF552A432F9760BF12 (void);
// 0x00000624 System.Void easyar.ImageTargetParameters::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void ImageTargetParameters__ctor_mA55EEA0E4883ABD3C76328FD9C15D7B79F145717 (void);
// 0x00000625 System.Object easyar.ImageTargetParameters::CloneObject()
extern void ImageTargetParameters_CloneObject_m9B894A71ECD626E88FB5DEDAA03D84871B2B32F3 (void);
// 0x00000626 easyar.ImageTargetParameters easyar.ImageTargetParameters::Clone()
extern void ImageTargetParameters_Clone_m41AA5B30437EC0DC6CD1CAB2975A50DD0533BD65 (void);
// 0x00000627 System.Void easyar.ImageTargetParameters::.ctor()
extern void ImageTargetParameters__ctor_m9E5DBAB1C3F262FDED5A4E5E819B0470C18F010B (void);
// 0x00000628 easyar.Image easyar.ImageTargetParameters::image()
extern void ImageTargetParameters_image_mE510BFC336E96EFD7DF7E3268FC843B194C6A377 (void);
// 0x00000629 System.Void easyar.ImageTargetParameters::setImage(easyar.Image)
extern void ImageTargetParameters_setImage_m6D15510D01A51B211751FF63DCDE68E44BE3ACC2 (void);
// 0x0000062A System.String easyar.ImageTargetParameters::name()
extern void ImageTargetParameters_name_mA88BFDB6924B7DB8AB6285B603F9BCF895D13C4C (void);
// 0x0000062B System.Void easyar.ImageTargetParameters::setName(System.String)
extern void ImageTargetParameters_setName_m93F24A91541C60D960AD625294741A238A002B60 (void);
// 0x0000062C System.String easyar.ImageTargetParameters::uid()
extern void ImageTargetParameters_uid_m9454559AA9296DF475F1D413A4CA3BA3A051BFC5 (void);
// 0x0000062D System.Void easyar.ImageTargetParameters::setUid(System.String)
extern void ImageTargetParameters_setUid_m05BF76E961F1ACD20B2B50589E42AD045745494D (void);
// 0x0000062E System.String easyar.ImageTargetParameters::meta()
extern void ImageTargetParameters_meta_m9AD48070BF6A96D1716BE71DB20D0DE9F086A3BB (void);
// 0x0000062F System.Void easyar.ImageTargetParameters::setMeta(System.String)
extern void ImageTargetParameters_setMeta_m3CBD2DBFF7185C42280E23C9952E9E30CFF6671E (void);
// 0x00000630 System.Single easyar.ImageTargetParameters::scale()
extern void ImageTargetParameters_scale_m2E2275C606A059CB3ADDAA05E2594C5970583EE7 (void);
// 0x00000631 System.Void easyar.ImageTargetParameters::setScale(System.Single)
extern void ImageTargetParameters_setScale_mE269C88716E931F1DA7F30798599F8FF2849B316 (void);
// 0x00000632 System.Void easyar.ImageTarget::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void ImageTarget__ctor_m2217685673CA6131F64343C0CEF823B62C3C3F2D (void);
// 0x00000633 System.Object easyar.ImageTarget::CloneObject()
extern void ImageTarget_CloneObject_m5ABCB44EBFD7CE028F2196D6753042BB8D445253 (void);
// 0x00000634 easyar.ImageTarget easyar.ImageTarget::Clone()
extern void ImageTarget_Clone_m4305CA89E131DD2BF8482C7078C4B8CA398CF7AB (void);
// 0x00000635 System.Void easyar.ImageTarget::.ctor()
extern void ImageTarget__ctor_m3D3F65999ABB42321D7B50412F45A506609B07DF (void);
// 0x00000636 easyar.Optional`1<easyar.ImageTarget> easyar.ImageTarget::createFromParameters(easyar.ImageTargetParameters)
extern void ImageTarget_createFromParameters_m199C42E7B9137229B7B0B789F50130821780D42A (void);
// 0x00000637 easyar.Optional`1<easyar.ImageTarget> easyar.ImageTarget::createFromTargetFile(System.String,easyar.StorageType)
extern void ImageTarget_createFromTargetFile_m8FAE255E8BF61EF3513E3CBB55E607A88801F104 (void);
// 0x00000638 easyar.Optional`1<easyar.ImageTarget> easyar.ImageTarget::createFromTargetData(easyar.Buffer)
extern void ImageTarget_createFromTargetData_m2594953EAB807B9615FA3F05F583CF99933F5853 (void);
// 0x00000639 System.Boolean easyar.ImageTarget::save(System.String)
extern void ImageTarget_save_m8EF425D415A5F1FAE50A71FFD6B1F648033E3B8E (void);
// 0x0000063A easyar.Optional`1<easyar.ImageTarget> easyar.ImageTarget::createFromImageFile(System.String,easyar.StorageType,System.String,System.String,System.String,System.Single)
extern void ImageTarget_createFromImageFile_mE8390E9063BA3DED769B05D2B7B35E0C14F8BD09 (void);
// 0x0000063B System.Single easyar.ImageTarget::scale()
extern void ImageTarget_scale_m61AE3F3B2D8696CD447B30F85A4110BC7ACC5B33 (void);
// 0x0000063C System.Single easyar.ImageTarget::aspectRatio()
extern void ImageTarget_aspectRatio_mC1217BDCB829DB5D8655DD5F30E4A6C2293D8F2F (void);
// 0x0000063D System.Boolean easyar.ImageTarget::setScale(System.Single)
extern void ImageTarget_setScale_mFB091536555855D4FC028660F151E457F24A6E08 (void);
// 0x0000063E System.Collections.Generic.List`1<easyar.Image> easyar.ImageTarget::images()
extern void ImageTarget_images_m01088CD8E8CABE4DBFCA5CF7E419B6468F664C6B (void);
// 0x0000063F System.Int32 easyar.ImageTarget::runtimeID()
extern void ImageTarget_runtimeID_m3F319AA0E88DA2C393FBEB259E165AD0D9BE8205 (void);
// 0x00000640 System.String easyar.ImageTarget::uid()
extern void ImageTarget_uid_m09E8774F140F841AE508A25F3F39B3140DE708D5 (void);
// 0x00000641 System.String easyar.ImageTarget::name()
extern void ImageTarget_name_m36106C5603EE19B19B2BCCD581638F24DAC925C9 (void);
// 0x00000642 System.Void easyar.ImageTarget::setName(System.String)
extern void ImageTarget_setName_mE6A6CA6B01D37416BFF3E55832877857F1E89792 (void);
// 0x00000643 System.String easyar.ImageTarget::meta()
extern void ImageTarget_meta_m9523E284F6781FE925DEAD5347CCEAEF7657CD68 (void);
// 0x00000644 System.Void easyar.ImageTarget::setMeta(System.String)
extern void ImageTarget_setMeta_m0E6B825C5B304E65F198017762CA658339627375 (void);
// 0x00000645 System.Void easyar.ImageTarget/<>c::.cctor()
extern void U3CU3Ec__cctor_m089E6D6AB5E9D5BBFC4389C9357DFD20971909A5 (void);
// 0x00000646 System.Void easyar.ImageTarget/<>c::.ctor()
extern void U3CU3Ec__ctor_m9D44AAA1557E053D041E97B325CC2198B171C173 (void);
// 0x00000647 easyar.Optional`1<easyar.ImageTarget> easyar.ImageTarget/<>c::<createFromParameters>b__4_0(easyar.Detail/OptionalOfImageTarget)
extern void U3CU3Ec_U3CcreateFromParametersU3Eb__4_0_m76F4FE5C5665725463645F063997A8EDB46340DF (void);
// 0x00000648 easyar.Optional`1<easyar.ImageTarget> easyar.ImageTarget/<>c::<createFromTargetFile>b__5_0(easyar.Detail/OptionalOfImageTarget)
extern void U3CU3Ec_U3CcreateFromTargetFileU3Eb__5_0_mBCE48FF52FF8E57D67FE3CCDCDADDC41111B68D3 (void);
// 0x00000649 easyar.Optional`1<easyar.ImageTarget> easyar.ImageTarget/<>c::<createFromTargetData>b__6_0(easyar.Detail/OptionalOfImageTarget)
extern void U3CU3Ec_U3CcreateFromTargetDataU3Eb__6_0_m9BC6A5468AE0C61FC170B4601577E64233B57301 (void);
// 0x0000064A easyar.Optional`1<easyar.ImageTarget> easyar.ImageTarget/<>c::<createFromImageFile>b__8_0(easyar.Detail/OptionalOfImageTarget)
extern void U3CU3Ec_U3CcreateFromImageFileU3Eb__8_0_m6479FE02133FF8F3A609D1AFC59AA084CEDE167A (void);
// 0x0000064B System.Void easyar.ImageTrackerResult::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void ImageTrackerResult__ctor_m7C7D0E1171C73A861A70E7002E7E4CF9FACA7AE7 (void);
// 0x0000064C System.Object easyar.ImageTrackerResult::CloneObject()
extern void ImageTrackerResult_CloneObject_mF40EF8E645C6430669A08D3FBB8700A97850FAC4 (void);
// 0x0000064D easyar.ImageTrackerResult easyar.ImageTrackerResult::Clone()
extern void ImageTrackerResult_Clone_m83ECB2EB572667CAFA6CFA6B41B7471B0D3760B1 (void);
// 0x0000064E System.Collections.Generic.List`1<easyar.TargetInstance> easyar.ImageTrackerResult::targetInstances()
extern void ImageTrackerResult_targetInstances_m5444C33C5B030365414C61C74F964FCE98EBF984 (void);
// 0x0000064F System.Void easyar.ImageTrackerResult::setTargetInstances(System.Collections.Generic.List`1<easyar.TargetInstance>)
extern void ImageTrackerResult_setTargetInstances_m1234E539086BA9B7ABFD81B528EE33B203C03233 (void);
// 0x00000650 System.Void easyar.ImageTracker::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void ImageTracker__ctor_mDE19E84B1799570A3A3D3E107996375A4419263D (void);
// 0x00000651 System.Object easyar.ImageTracker::CloneObject()
extern void ImageTracker_CloneObject_m8A4E94C899DB11947C094C4DAF1E72299C2A91AC (void);
// 0x00000652 easyar.ImageTracker easyar.ImageTracker::Clone()
extern void ImageTracker_Clone_mF27D49FDEC92201EFBF99D035DE132174651E129 (void);
// 0x00000653 System.Boolean easyar.ImageTracker::isAvailable()
extern void ImageTracker_isAvailable_mBDAE787913931938D137D6C4E92CD7C205692140 (void);
// 0x00000654 easyar.FeedbackFrameSink easyar.ImageTracker::feedbackFrameSink()
extern void ImageTracker_feedbackFrameSink_m48496F65CBC19203A6A907592FA29B38FC84CD42 (void);
// 0x00000655 System.Int32 easyar.ImageTracker::bufferRequirement()
extern void ImageTracker_bufferRequirement_mACD43009AD0ABC818D1C7BBE74EC1C45646B34B9 (void);
// 0x00000656 easyar.OutputFrameSource easyar.ImageTracker::outputFrameSource()
extern void ImageTracker_outputFrameSource_m82E7B0A0CC7B58EA011942B8CD55E738C0F5CF5A (void);
// 0x00000657 easyar.ImageTracker easyar.ImageTracker::create()
extern void ImageTracker_create_m72A0371A03F453A7E99EED588E662A3DFEA67AD4 (void);
// 0x00000658 easyar.ImageTracker easyar.ImageTracker::createWithMode(easyar.ImageTrackerMode)
extern void ImageTracker_createWithMode_m9F11D5B9160A87D3EEB36D1D0B7EEEB09DED97FB (void);
// 0x00000659 System.Boolean easyar.ImageTracker::start()
extern void ImageTracker_start_mA72E486A71352B79ED7529FA2A03A4E6CFDBA70E (void);
// 0x0000065A System.Void easyar.ImageTracker::stop()
extern void ImageTracker_stop_m2F875E9550F84A07B410FD0AB5AB25D5987E437B (void);
// 0x0000065B System.Void easyar.ImageTracker::close()
extern void ImageTracker_close_mF07E3C927B7BF9F02632B80947FE9284E4699E43 (void);
// 0x0000065C System.Void easyar.ImageTracker::loadTarget(easyar.Target,easyar.CallbackScheduler,System.Action`2<easyar.Target,System.Boolean>)
extern void ImageTracker_loadTarget_m62F0B49EC011BFF3EE90D858D16454239BEE3543 (void);
// 0x0000065D System.Void easyar.ImageTracker::unloadTarget(easyar.Target,easyar.CallbackScheduler,System.Action`2<easyar.Target,System.Boolean>)
extern void ImageTracker_unloadTarget_m1E900E5F81BCB68D380697EB279043CD96D81A8D (void);
// 0x0000065E System.Collections.Generic.List`1<easyar.Target> easyar.ImageTracker::targets()
extern void ImageTracker_targets_m83C149A25190AF37060E1BBB0305EB56430C1A1B (void);
// 0x0000065F System.Boolean easyar.ImageTracker::setSimultaneousNum(System.Int32)
extern void ImageTracker_setSimultaneousNum_mDB3AF18FC362ACDC862527E78137766FDBA01DEB (void);
// 0x00000660 System.Int32 easyar.ImageTracker::simultaneousNum()
extern void ImageTracker_simultaneousNum_m396E3AA7B974B7238D68C94DAE2FA2A99351B063 (void);
// 0x00000661 System.Void easyar.Recorder::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void Recorder__ctor_mB8FA18780FEE4238CE35DCDD55D8ECFD7123D6FC (void);
// 0x00000662 System.Object easyar.Recorder::CloneObject()
extern void Recorder_CloneObject_m8066D619A8069DE343F1F5D8DC57E2F8E24DBF75 (void);
// 0x00000663 easyar.Recorder easyar.Recorder::Clone()
extern void Recorder_Clone_mAF604210399647BB12965C656D247A68E82FFAB0 (void);
// 0x00000664 System.Boolean easyar.Recorder::isAvailable()
extern void Recorder_isAvailable_m8A9F14E795C4909B248599572BFD147F38BA2233 (void);
// 0x00000665 System.Void easyar.Recorder::requestPermissions(easyar.CallbackScheduler,easyar.Optional`1<System.Action`2<easyar.PermissionStatus,System.String>>)
extern void Recorder_requestPermissions_m2B952A11E46AA65C4894989F30F8E9ED551FA384 (void);
// 0x00000666 easyar.Recorder easyar.Recorder::create(easyar.RecorderConfiguration,easyar.CallbackScheduler,easyar.Optional`1<System.Action`2<easyar.RecordStatus,System.String>>)
extern void Recorder_create_mAA356B8CFC814EE20C41D87863F685E57B74D10E (void);
// 0x00000667 System.Void easyar.Recorder::start()
extern void Recorder_start_mAE52606DC9DFCC66C091580E94EB1F7889C22ED9 (void);
// 0x00000668 System.Void easyar.Recorder::updateFrame(easyar.TextureId,System.Int32,System.Int32)
extern void Recorder_updateFrame_mA47F64B5CB29DD7E2E9D5863BA2AFB4CC7D91B90 (void);
// 0x00000669 System.Boolean easyar.Recorder::stop()
extern void Recorder_stop_mBD49F8860C1637BA0309286455BD6A4F5DC62B96 (void);
// 0x0000066A System.Void easyar.Recorder/<>c::.cctor()
extern void U3CU3Ec__cctor_m3BBFD3CF5307CF88D0EFED1880A703F16FC8D8E1 (void);
// 0x0000066B System.Void easyar.Recorder/<>c::.ctor()
extern void U3CU3Ec__ctor_m7E0CA6A9F9C716DCD2C1D53CE322FBFF7B78CC21 (void);
// 0x0000066C easyar.Detail/OptionalOfFunctorOfVoidFromPermissionStatusAndString easyar.Recorder/<>c::<requestPermissions>b__4_0(easyar.Optional`1<System.Action`2<easyar.PermissionStatus,System.String>>)
extern void U3CU3Ec_U3CrequestPermissionsU3Eb__4_0_m97254790976C119CD7511930575DA958187B07CE (void);
// 0x0000066D easyar.Detail/OptionalOfFunctorOfVoidFromRecordStatusAndString easyar.Recorder/<>c::<create>b__5_0(easyar.Optional`1<System.Action`2<easyar.RecordStatus,System.String>>)
extern void U3CU3Ec_U3CcreateU3Eb__5_0_mF263163369F5239315EF562CF5636C14305A7814 (void);
// 0x0000066E System.Void easyar.RecorderConfiguration::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void RecorderConfiguration__ctor_m584B949D8533DC0F343D4182786723B7D6B573BE (void);
// 0x0000066F System.Object easyar.RecorderConfiguration::CloneObject()
extern void RecorderConfiguration_CloneObject_mD95D6E22A7E6300A5F105967157D7DF4B29FB186 (void);
// 0x00000670 easyar.RecorderConfiguration easyar.RecorderConfiguration::Clone()
extern void RecorderConfiguration_Clone_m34DDF37F431614FA05C0E63BC5976A179723242E (void);
// 0x00000671 System.Void easyar.RecorderConfiguration::.ctor()
extern void RecorderConfiguration__ctor_mD311C62C0414FB918B805EF37385179F73DBFF53 (void);
// 0x00000672 System.Void easyar.RecorderConfiguration::setOutputFile(System.String)
extern void RecorderConfiguration_setOutputFile_m637E20BF8EDCD3C0329D2FCE1836CE34675B9C8C (void);
// 0x00000673 System.Boolean easyar.RecorderConfiguration::setProfile(easyar.RecordProfile)
extern void RecorderConfiguration_setProfile_m514341FB34D6914182E2F236C15EEEBE9B0865B2 (void);
// 0x00000674 System.Void easyar.RecorderConfiguration::setVideoSize(easyar.RecordVideoSize)
extern void RecorderConfiguration_setVideoSize_mF974BF5626E9304F93E2736A2A8AB451B2816AD5 (void);
// 0x00000675 System.Void easyar.RecorderConfiguration::setVideoBitrate(System.Int32)
extern void RecorderConfiguration_setVideoBitrate_m0DE1D2EA5B409FC7C37F12271EA1D09D694AE0B9 (void);
// 0x00000676 System.Void easyar.RecorderConfiguration::setChannelCount(System.Int32)
extern void RecorderConfiguration_setChannelCount_m92F3D880808D43E16C2F17A72CB0E851B2CDE440 (void);
// 0x00000677 System.Void easyar.RecorderConfiguration::setAudioSampleRate(System.Int32)
extern void RecorderConfiguration_setAudioSampleRate_mA9DE0FD62D1F1A19883A2F05B196A44FE2ACBB53 (void);
// 0x00000678 System.Void easyar.RecorderConfiguration::setAudioBitrate(System.Int32)
extern void RecorderConfiguration_setAudioBitrate_m95DCACA8EFBDDD016A8C93730DA59E6719E1188D (void);
// 0x00000679 System.Void easyar.RecorderConfiguration::setVideoOrientation(easyar.RecordVideoOrientation)
extern void RecorderConfiguration_setVideoOrientation_m893443D3761C40C1629659F72794BDAC9D37FEB5 (void);
// 0x0000067A System.Void easyar.RecorderConfiguration::setZoomMode(easyar.RecordZoomMode)
extern void RecorderConfiguration_setZoomMode_m9B78FF10A30C305C86B4416340860F2EC5A5799B (void);
// 0x0000067B System.Void easyar.SparseSpatialMapResult::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void SparseSpatialMapResult__ctor_m991D626D81B33FCC1ABF82D876EE25B9395A95A7 (void);
// 0x0000067C System.Object easyar.SparseSpatialMapResult::CloneObject()
extern void SparseSpatialMapResult_CloneObject_m719357CA3B3E4B390407997ECAA0F3350DCB1C05 (void);
// 0x0000067D easyar.SparseSpatialMapResult easyar.SparseSpatialMapResult::Clone()
extern void SparseSpatialMapResult_Clone_m74B505104160D3A211E0F0075D05F6A9AFA06047 (void);
// 0x0000067E easyar.MotionTrackingStatus easyar.SparseSpatialMapResult::getMotionTrackingStatus()
extern void SparseSpatialMapResult_getMotionTrackingStatus_m848EF55FAB9423233DDC160F6045A60D26096618 (void);
// 0x0000067F easyar.Optional`1<easyar.Matrix44F> easyar.SparseSpatialMapResult::getVioPose()
extern void SparseSpatialMapResult_getVioPose_m866FF5FD369AED40194B89953E39BCB9995440FD (void);
// 0x00000680 easyar.Optional`1<easyar.Matrix44F> easyar.SparseSpatialMapResult::getMapPose()
extern void SparseSpatialMapResult_getMapPose_m68854FFB10A9706B7938A5FF93791A421B2479E1 (void);
// 0x00000681 System.Boolean easyar.SparseSpatialMapResult::getLocalizationStatus()
extern void SparseSpatialMapResult_getLocalizationStatus_m44740C95CAFF0A3A16CBBA97F166EC0A27528D99 (void);
// 0x00000682 System.String easyar.SparseSpatialMapResult::getLocalizationMapID()
extern void SparseSpatialMapResult_getLocalizationMapID_mACB7191B5FA0F16D96B0F894B45658FADBF9CCCA (void);
// 0x00000683 System.Void easyar.SparseSpatialMapResult/<>c::.cctor()
extern void U3CU3Ec__cctor_m95FECA59D58CBCA0E9D0A6BE691DDBF8C3613F93 (void);
// 0x00000684 System.Void easyar.SparseSpatialMapResult/<>c::.ctor()
extern void U3CU3Ec__ctor_m58DA267999A69D1854CB9E7438BA0B10F2EF3D4E (void);
// 0x00000685 easyar.Optional`1<easyar.Matrix44F> easyar.SparseSpatialMapResult/<>c::<getVioPose>b__4_0(easyar.Detail/OptionalOfMatrix44F)
extern void U3CU3Ec_U3CgetVioPoseU3Eb__4_0_m9EB790BD3F85F8CD44BFE74435ABB43F7A1378A5 (void);
// 0x00000686 easyar.Optional`1<easyar.Matrix44F> easyar.SparseSpatialMapResult/<>c::<getMapPose>b__5_0(easyar.Detail/OptionalOfMatrix44F)
extern void U3CU3Ec_U3CgetMapPoseU3Eb__5_0_mA7B0D4F39D30AA1A4793B74F5617020066B75BC9 (void);
// 0x00000687 System.Void easyar.PlaneData::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void PlaneData__ctor_mBC847736FD9139351525C99BFF9449879CCECCAE (void);
// 0x00000688 System.Object easyar.PlaneData::CloneObject()
extern void PlaneData_CloneObject_mC664AB789B7978EA87CAF563372635D3C86E800F (void);
// 0x00000689 easyar.PlaneData easyar.PlaneData::Clone()
extern void PlaneData_Clone_mA79E49A5E50860E28930F4DE27E15AA8E25B2051 (void);
// 0x0000068A System.Void easyar.PlaneData::.ctor()
extern void PlaneData__ctor_mB20247057DA16BB4EEB8A52F3EAA91D82759733B (void);
// 0x0000068B easyar.PlaneType easyar.PlaneData::getType()
extern void PlaneData_getType_m1678E832D9515F76F6139663011C50F983C6772E (void);
// 0x0000068C easyar.Matrix44F easyar.PlaneData::getPose()
extern void PlaneData_getPose_mDD0ADF26432E586FD3B8816350768587209A5C38 (void);
// 0x0000068D System.Single easyar.PlaneData::getExtentX()
extern void PlaneData_getExtentX_m95A206F9E55AF3AC8A049B2D20293C83BA63A5F2 (void);
// 0x0000068E System.Single easyar.PlaneData::getExtentZ()
extern void PlaneData_getExtentZ_m279DDCBBA38C56697932800C405731C6525A4203 (void);
// 0x0000068F System.Void easyar.SparseSpatialMapConfig::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void SparseSpatialMapConfig__ctor_mDA8250C3AC98319D12B9CB2A81AD2480AD7EF857 (void);
// 0x00000690 System.Object easyar.SparseSpatialMapConfig::CloneObject()
extern void SparseSpatialMapConfig_CloneObject_mF559EFE22921C6F82685C607A1252ED5A874F79A (void);
// 0x00000691 easyar.SparseSpatialMapConfig easyar.SparseSpatialMapConfig::Clone()
extern void SparseSpatialMapConfig_Clone_mE2DE77CDF5A5499B0F472D4A7368C0980545AF98 (void);
// 0x00000692 System.Void easyar.SparseSpatialMapConfig::.ctor()
extern void SparseSpatialMapConfig__ctor_m9C2C40A6EC187AA460CDF981D98A88DD43A64A43 (void);
// 0x00000693 System.Void easyar.SparseSpatialMapConfig::setLocalizationMode(easyar.LocalizationMode)
extern void SparseSpatialMapConfig_setLocalizationMode_m8F4D2CF90DC7C008AB8E5814DCFEB33219D55892 (void);
// 0x00000694 easyar.LocalizationMode easyar.SparseSpatialMapConfig::getLocalizationMode()
extern void SparseSpatialMapConfig_getLocalizationMode_m6B0788FD02E6002097F792D32B5C1FB723B2BF17 (void);
// 0x00000695 System.Void easyar.SparseSpatialMap::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void SparseSpatialMap__ctor_mCB2F9FA6F8AA34415BED7426B1BE7A720AB6E46B (void);
// 0x00000696 System.Object easyar.SparseSpatialMap::CloneObject()
extern void SparseSpatialMap_CloneObject_m6AEC0CCB8E3C92F6E6933EE1ADBAFF840CB007DA (void);
// 0x00000697 easyar.SparseSpatialMap easyar.SparseSpatialMap::Clone()
extern void SparseSpatialMap_Clone_m33C79259F025794B8C0FFC14F1198C0F73B9018B (void);
// 0x00000698 System.Boolean easyar.SparseSpatialMap::isAvailable()
extern void SparseSpatialMap_isAvailable_m3A3DA49492871D1F7384155D290248F417A97A6E (void);
// 0x00000699 easyar.InputFrameSink easyar.SparseSpatialMap::inputFrameSink()
extern void SparseSpatialMap_inputFrameSink_m7B31A2789844D69C23AA442D79E4A4A960281562 (void);
// 0x0000069A System.Int32 easyar.SparseSpatialMap::bufferRequirement()
extern void SparseSpatialMap_bufferRequirement_mA8E26C7CC3C1E1EB79FAC893458E3EA9CCEEE5FA (void);
// 0x0000069B easyar.OutputFrameSource easyar.SparseSpatialMap::outputFrameSource()
extern void SparseSpatialMap_outputFrameSource_m722F1B6FF8E2B856304972C1BF44C54E2BFAECD1 (void);
// 0x0000069C easyar.SparseSpatialMap easyar.SparseSpatialMap::create()
extern void SparseSpatialMap_create_m2BF7A42F6A5920AA5B33038924252DD0820BFD3C (void);
// 0x0000069D System.Boolean easyar.SparseSpatialMap::start()
extern void SparseSpatialMap_start_m11265C9AA5777E18B78DF52531E3383A1532FE98 (void);
// 0x0000069E System.Void easyar.SparseSpatialMap::stop()
extern void SparseSpatialMap_stop_m0E1956586A1A760EEAD216949FC908E5A3BCB9E0 (void);
// 0x0000069F System.Void easyar.SparseSpatialMap::close()
extern void SparseSpatialMap_close_m83D072606653EE4D18FE0177F5B5926840DE6F70 (void);
// 0x000006A0 easyar.Buffer easyar.SparseSpatialMap::getPointCloudBuffer()
extern void SparseSpatialMap_getPointCloudBuffer_m0671798CEB40836C93EC9C5252FCB8BD470E408A (void);
// 0x000006A1 System.Collections.Generic.List`1<easyar.PlaneData> easyar.SparseSpatialMap::getMapPlanes()
extern void SparseSpatialMap_getMapPlanes_mBE5B56F55E2A8A6EA5E1F2FA30E7E632FC2FF6BD (void);
// 0x000006A2 System.Collections.Generic.List`1<easyar.Vec3F> easyar.SparseSpatialMap::hitTestAgainstPointCloud(easyar.Vec2F)
extern void SparseSpatialMap_hitTestAgainstPointCloud_mC3AF0ABC46ABB5607785F1E0104703498DE0D383 (void);
// 0x000006A3 System.Collections.Generic.List`1<easyar.Vec3F> easyar.SparseSpatialMap::hitTestAgainstPlanes(easyar.Vec2F)
extern void SparseSpatialMap_hitTestAgainstPlanes_m175C001A81D1EF29E992D92860AB295209AE8C57 (void);
// 0x000006A4 System.String easyar.SparseSpatialMap::getMapVersion()
extern void SparseSpatialMap_getMapVersion_m72C8D223B5DF0A7C3ED5FFB784DCBCDA98FB8395 (void);
// 0x000006A5 System.Void easyar.SparseSpatialMap::unloadMap(System.String,easyar.CallbackScheduler,easyar.Optional`1<System.Action`1<System.Boolean>>)
extern void SparseSpatialMap_unloadMap_mE5A5615C9D082D6C5B930B624670F36595CD3408 (void);
// 0x000006A6 System.Void easyar.SparseSpatialMap::setConfig(easyar.SparseSpatialMapConfig)
extern void SparseSpatialMap_setConfig_mFC0F9C6BB93A3131DA522593A9961BE04298556C (void);
// 0x000006A7 easyar.SparseSpatialMapConfig easyar.SparseSpatialMap::getConfig()
extern void SparseSpatialMap_getConfig_m11D3B86A7668F20A982063F127BCF3CB6CFA082C (void);
// 0x000006A8 System.Boolean easyar.SparseSpatialMap::startLocalization()
extern void SparseSpatialMap_startLocalization_mF1B1840F0372DC12BB54F96BA4492BB035BFDE72 (void);
// 0x000006A9 System.Void easyar.SparseSpatialMap::stopLocalization()
extern void SparseSpatialMap_stopLocalization_m99461D40C9BCED8F809D649ED903564A0115914C (void);
// 0x000006AA System.Void easyar.SparseSpatialMap/<>c::.cctor()
extern void U3CU3Ec__cctor_m133C60B44966B28286298FCAA8442EAAE4348C81 (void);
// 0x000006AB System.Void easyar.SparseSpatialMap/<>c::.ctor()
extern void U3CU3Ec__ctor_m6D9831E705A18F012C45E234DA1C8C700D237E73 (void);
// 0x000006AC easyar.Detail/OptionalOfFunctorOfVoidFromBool easyar.SparseSpatialMap/<>c::<unloadMap>b__16_0(easyar.Optional`1<System.Action`1<System.Boolean>>)
extern void U3CU3Ec_U3CunloadMapU3Eb__16_0_mE449AB43C94206323DD96EBF230E3A3639529207 (void);
// 0x000006AD System.Void easyar.SparseSpatialMapManager::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void SparseSpatialMapManager__ctor_m01D00DFB39A05436366E33D044EF65FB1BAE63CF (void);
// 0x000006AE System.Object easyar.SparseSpatialMapManager::CloneObject()
extern void SparseSpatialMapManager_CloneObject_mF19C31CC27097D713A9BAC2B20F427DD36AA26BC (void);
// 0x000006AF easyar.SparseSpatialMapManager easyar.SparseSpatialMapManager::Clone()
extern void SparseSpatialMapManager_Clone_m23F9D87E0ED64E8FBF85329C4397321A096FC074 (void);
// 0x000006B0 System.Boolean easyar.SparseSpatialMapManager::isAvailable()
extern void SparseSpatialMapManager_isAvailable_mD347B0F546077619E8C4E94DCAE5780D3561D461 (void);
// 0x000006B1 easyar.SparseSpatialMapManager easyar.SparseSpatialMapManager::create()
extern void SparseSpatialMapManager_create_m0C93C3656C659F332E1CE8E577C66C54AC1E58AB (void);
// 0x000006B2 System.Void easyar.SparseSpatialMapManager::host(easyar.SparseSpatialMap,System.String,System.String,System.String,System.String,easyar.Optional`1<easyar.Image>,easyar.CallbackScheduler,System.Action`3<System.Boolean,System.String,System.String>)
extern void SparseSpatialMapManager_host_m8125D9AE8B69601D4AF67A5AD013B810AD1E2624 (void);
// 0x000006B3 System.Void easyar.SparseSpatialMapManager::load(easyar.SparseSpatialMap,System.String,System.String,System.String,System.String,easyar.CallbackScheduler,System.Action`2<System.Boolean,System.String>)
extern void SparseSpatialMapManager_load_m088C435DEA421C9881B0E1DB70F190CCEB705EA5 (void);
// 0x000006B4 System.Void easyar.SparseSpatialMapManager::clear()
extern void SparseSpatialMapManager_clear_m40E562AABB92A094D5516BF696814FF63545DD8C (void);
// 0x000006B5 System.Void easyar.SparseSpatialMapManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m8AF851FE9A4312AE045CA37DE70A50D94BBDE381 (void);
// 0x000006B6 System.Void easyar.SparseSpatialMapManager/<>c::.ctor()
extern void U3CU3Ec__ctor_mE8DB511B18D8EA1592726383A56EBEE8FFC8BB73 (void);
// 0x000006B7 easyar.Detail/OptionalOfImage easyar.SparseSpatialMapManager/<>c::<host>b__5_0(easyar.Optional`1<easyar.Image>)
extern void U3CU3Ec_U3ChostU3Eb__5_0_m6A7E0D0D9DCE907588DD5C3507F6202B4C6A7D31 (void);
// 0x000006B8 System.Int32 easyar.Engine::schemaHash()
extern void Engine_schemaHash_mE29C355DA7FE433F156F0AD9F206C01E9D207D13 (void);
// 0x000006B9 System.Boolean easyar.Engine::initialize(System.String)
extern void Engine_initialize_m11A436D24ACCBFCEA59056359E8AADCAF93264BC (void);
// 0x000006BA System.Void easyar.Engine::onPause()
extern void Engine_onPause_mD1CB8E189883AF7D2DB9EB5561395165B029969C (void);
// 0x000006BB System.Void easyar.Engine::onResume()
extern void Engine_onResume_m92D596C9F77D69647CD00F3B3062FEF2B72F763E (void);
// 0x000006BC System.String easyar.Engine::errorMessage()
extern void Engine_errorMessage_m79BE0BD4ECFD4CF5FB0A86C032FB3F6F7DC482C6 (void);
// 0x000006BD System.String easyar.Engine::versionString()
extern void Engine_versionString_mBD0B9F6F029448E0A670FF25CCA313CDBAD98649 (void);
// 0x000006BE System.String easyar.Engine::name()
extern void Engine_name_mBB673AB4504EBD510F636DE83285BBA5AD71AD9B (void);
// 0x000006BF System.Void easyar.Engine::.ctor()
extern void Engine__ctor_mA7E53F759C96FC1E785D79ECE02BED3E0291A4AB (void);
// 0x000006C0 System.Void easyar.VideoPlayer::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void VideoPlayer__ctor_mE63EC7FBE845F39BE13135497BD05CAF4D12F3AC (void);
// 0x000006C1 System.Object easyar.VideoPlayer::CloneObject()
extern void VideoPlayer_CloneObject_m539E8DE984620130ABAB7A50ED8F14355AEC45A1 (void);
// 0x000006C2 easyar.VideoPlayer easyar.VideoPlayer::Clone()
extern void VideoPlayer_Clone_mCC198F58E166E56504484888FB0F9CB631D72AB3 (void);
// 0x000006C3 System.Void easyar.VideoPlayer::.ctor()
extern void VideoPlayer__ctor_mADE34AF284005C6A1B8AB65A35915DE098F28390 (void);
// 0x000006C4 System.Boolean easyar.VideoPlayer::isAvailable()
extern void VideoPlayer_isAvailable_m1EAD4D4980F19F33CF6515BBB025D689E1F73F67 (void);
// 0x000006C5 System.Void easyar.VideoPlayer::setVideoType(easyar.VideoType)
extern void VideoPlayer_setVideoType_m5A8A50779AA7D9CE0DEA7CD06C28BDEE15CD49DE (void);
// 0x000006C6 System.Void easyar.VideoPlayer::setRenderTexture(easyar.TextureId)
extern void VideoPlayer_setRenderTexture_mEC891F4DFE78F90891E6F339253EF902FE6D9DE0 (void);
// 0x000006C7 System.Void easyar.VideoPlayer::open(System.String,easyar.StorageType,easyar.CallbackScheduler,easyar.Optional`1<System.Action`1<easyar.VideoStatus>>)
extern void VideoPlayer_open_m620AF087A910AE5A81955FA2A863B006289F2F1E (void);
// 0x000006C8 System.Void easyar.VideoPlayer::close()
extern void VideoPlayer_close_m972383C93ACF5CF9C27D9670CDC38CE6B1C250F6 (void);
// 0x000006C9 System.Boolean easyar.VideoPlayer::play()
extern void VideoPlayer_play_m220AF7A3898D95CD1E83F2BC278459D668BBB488 (void);
// 0x000006CA System.Void easyar.VideoPlayer::stop()
extern void VideoPlayer_stop_mB5506354738E36120BFFB53D56D74344A21C5A50 (void);
// 0x000006CB System.Void easyar.VideoPlayer::pause()
extern void VideoPlayer_pause_m0127DCC93E2761A5EED419EE3FD388827DAF711C (void);
// 0x000006CC System.Boolean easyar.VideoPlayer::isRenderTextureAvailable()
extern void VideoPlayer_isRenderTextureAvailable_m03A1A75AD2B59DE330F29D19DFECC43117EBF67C (void);
// 0x000006CD System.Void easyar.VideoPlayer::updateFrame()
extern void VideoPlayer_updateFrame_m2066792A001DD9CDF61ECD060D379BF33FDF6749 (void);
// 0x000006CE System.Int32 easyar.VideoPlayer::duration()
extern void VideoPlayer_duration_mFDD1771DC1896253EEB75586A2EA86375C1D1D9B (void);
// 0x000006CF System.Int32 easyar.VideoPlayer::currentPosition()
extern void VideoPlayer_currentPosition_mCD5EEA47EA030D603766EACAED4AB078A9E64F1D (void);
// 0x000006D0 System.Boolean easyar.VideoPlayer::seek(System.Int32)
extern void VideoPlayer_seek_m28E4AA77A3E9258BF5F6859FEEFCF1F56EB5B77E (void);
// 0x000006D1 easyar.Vec2I easyar.VideoPlayer::size()
extern void VideoPlayer_size_mE5ECC2479A815D956972478A2B9067BB5670C118 (void);
// 0x000006D2 System.Single easyar.VideoPlayer::volume()
extern void VideoPlayer_volume_mB0B2EC40637C399BCD97911BD071B24F487B9989 (void);
// 0x000006D3 System.Boolean easyar.VideoPlayer::setVolume(System.Single)
extern void VideoPlayer_setVolume_mE22854258C4BA5B22672D286B67EFF1617FA4402 (void);
// 0x000006D4 System.Void easyar.VideoPlayer/<>c::.cctor()
extern void U3CU3Ec__cctor_m4816339B3F66B0F26F0B398F0F07DDCFB6BB3BA9 (void);
// 0x000006D5 System.Void easyar.VideoPlayer/<>c::.ctor()
extern void U3CU3Ec__ctor_mD4C07DF2E42B8793DBACA1F912D8267110FCF873 (void);
// 0x000006D6 easyar.Detail/OptionalOfFunctorOfVoidFromVideoStatus easyar.VideoPlayer/<>c::<open>b__7_0(easyar.Optional`1<System.Action`1<easyar.VideoStatus>>)
extern void U3CU3Ec_U3CopenU3Eb__7_0_mA0B920BEBB24C1A44E0262C2A628D46388601A87 (void);
// 0x000006D7 easyar.Optional`1<easyar.Image> easyar.ImageHelper::decode(easyar.Buffer)
extern void ImageHelper_decode_mD6E23B7CFB1B3A52E3F847AC5C656AD7D8D8B55A (void);
// 0x000006D8 System.Void easyar.ImageHelper::.ctor()
extern void ImageHelper__ctor_m9F3EBC666287C035D2E5919AEBC12FAFD0FF918F (void);
// 0x000006D9 System.Void easyar.ImageHelper/<>c::.cctor()
extern void U3CU3Ec__cctor_mAFA7C8FEA8A2CFEC22B832EC0FA9371D9E9FEADD (void);
// 0x000006DA System.Void easyar.ImageHelper/<>c::.ctor()
extern void U3CU3Ec__ctor_mA62E6106E43A3D2FFC711C2B28C10824BB34EDA3 (void);
// 0x000006DB easyar.Optional`1<easyar.Image> easyar.ImageHelper/<>c::<decode>b__0_0(easyar.Detail/OptionalOfImage)
extern void U3CU3Ec_U3CdecodeU3Eb__0_0_mC459140F49E041E56D1D3AEF0839B4044C748B3B (void);
// 0x000006DC System.Void easyar.SignalSink::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void SignalSink__ctor_mF6AF7A95F070F4C83FEB971FF89806CD565B0FF8 (void);
// 0x000006DD System.Object easyar.SignalSink::CloneObject()
extern void SignalSink_CloneObject_mB31BBDEF20296F1E441B65CCC66E80423084CE2B (void);
// 0x000006DE easyar.SignalSink easyar.SignalSink::Clone()
extern void SignalSink_Clone_m2A70AE4EF7427E48BED0D4A383FE052C60151D3E (void);
// 0x000006DF System.Void easyar.SignalSink::handle()
extern void SignalSink_handle_m0D8833FDC2166DF77FDDBCB26CC224BFABF5C990 (void);
// 0x000006E0 System.Void easyar.SignalSource::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void SignalSource__ctor_m39AFB9C077865268CB6459362326886625EB4B41 (void);
// 0x000006E1 System.Object easyar.SignalSource::CloneObject()
extern void SignalSource_CloneObject_m100C35A7EA0D30E9429866AE3F98D7E1E75C0A01 (void);
// 0x000006E2 easyar.SignalSource easyar.SignalSource::Clone()
extern void SignalSource_Clone_m972442D5C977160332E8B12FEB5371A7F0A88A46 (void);
// 0x000006E3 System.Void easyar.SignalSource::setHandler(easyar.Optional`1<System.Action>)
extern void SignalSource_setHandler_m48FE5636FAB2A1307F5CA316DA41A51A9412865D (void);
// 0x000006E4 System.Void easyar.SignalSource::connect(easyar.SignalSink)
extern void SignalSource_connect_mF691203C0003586CD8C4082BF7463D9E617371E8 (void);
// 0x000006E5 System.Void easyar.SignalSource::disconnect()
extern void SignalSource_disconnect_m6E0F810B8D69256FE3603B91AD1C708646003BE7 (void);
// 0x000006E6 System.Void easyar.SignalSource/<>c::.cctor()
extern void U3CU3Ec__cctor_mCDA8D804649F3793307B5B558543BF7EB04366B3 (void);
// 0x000006E7 System.Void easyar.SignalSource/<>c::.ctor()
extern void U3CU3Ec__ctor_m9C50CA93EFE31D8FFDBA0E8E37F18657A1BF816E (void);
// 0x000006E8 easyar.Detail/OptionalOfFunctorOfVoid easyar.SignalSource/<>c::<setHandler>b__3_0(easyar.Optional`1<System.Action>)
extern void U3CU3Ec_U3CsetHandlerU3Eb__3_0_m089A90F360FA982EF63FF78484472203A5CE12DF (void);
// 0x000006E9 System.Void easyar.InputFrameSink::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void InputFrameSink__ctor_m3287F7C9257FFE56753ECCB5B5D93EFDC77FE0BA (void);
// 0x000006EA System.Object easyar.InputFrameSink::CloneObject()
extern void InputFrameSink_CloneObject_m07B291D95CAE9B47CE34CD9D6EE42B0B704F4F05 (void);
// 0x000006EB easyar.InputFrameSink easyar.InputFrameSink::Clone()
extern void InputFrameSink_Clone_mD990F744AE90A9AC6C99A1FFCDDD2CC54827D824 (void);
// 0x000006EC System.Void easyar.InputFrameSink::handle(easyar.InputFrame)
extern void InputFrameSink_handle_mE6907122445B68170F796D3AD2DA52D7639B3C30 (void);
// 0x000006ED System.Void easyar.InputFrameSource::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void InputFrameSource__ctor_mBFC30B32C94A8C569B400E61DF7F74A63CD6CC8D (void);
// 0x000006EE System.Object easyar.InputFrameSource::CloneObject()
extern void InputFrameSource_CloneObject_m3A904074EE0539A618596CBC3F02234C2C39A164 (void);
// 0x000006EF easyar.InputFrameSource easyar.InputFrameSource::Clone()
extern void InputFrameSource_Clone_m60F5F350C4EB248516D8C81B47F4F4743BABEC74 (void);
// 0x000006F0 System.Void easyar.InputFrameSource::setHandler(easyar.Optional`1<System.Action`1<easyar.InputFrame>>)
extern void InputFrameSource_setHandler_m67D138E16CB0325A82249FAFBBC2D3D67F51CE60 (void);
// 0x000006F1 System.Void easyar.InputFrameSource::connect(easyar.InputFrameSink)
extern void InputFrameSource_connect_mA010C74664C629D28A1FE86DFD5FB079DD6949F4 (void);
// 0x000006F2 System.Void easyar.InputFrameSource::disconnect()
extern void InputFrameSource_disconnect_m985D10F6191C8B6F4D7A7BA1136D2819956E28B8 (void);
// 0x000006F3 System.Void easyar.InputFrameSource/<>c::.cctor()
extern void U3CU3Ec__cctor_m5D4839887B41ECC90F25F7EC6A8B823D49261CA6 (void);
// 0x000006F4 System.Void easyar.InputFrameSource/<>c::.ctor()
extern void U3CU3Ec__ctor_m2D65CE67E8FBDC5A7BF08BD591DAD8C6E4B5845E (void);
// 0x000006F5 easyar.Detail/OptionalOfFunctorOfVoidFromInputFrame easyar.InputFrameSource/<>c::<setHandler>b__3_0(easyar.Optional`1<System.Action`1<easyar.InputFrame>>)
extern void U3CU3Ec_U3CsetHandlerU3Eb__3_0_m6BB0C47808500F8D68DCC6E9C12137EC24C41AA2 (void);
// 0x000006F6 System.Void easyar.OutputFrameSink::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void OutputFrameSink__ctor_m59152189CA74683CCBB2E3E63A10BE2C202A9B6C (void);
// 0x000006F7 System.Object easyar.OutputFrameSink::CloneObject()
extern void OutputFrameSink_CloneObject_mC81058D0E2D56C6FC97BCFA8EBB000A19BF9E6EB (void);
// 0x000006F8 easyar.OutputFrameSink easyar.OutputFrameSink::Clone()
extern void OutputFrameSink_Clone_mE1DB31376C8C6E5B587BD31AEA942FA477BE571F (void);
// 0x000006F9 System.Void easyar.OutputFrameSink::handle(easyar.OutputFrame)
extern void OutputFrameSink_handle_m2BC5F361F6450EFE5334185395F1B6F5E058BFED (void);
// 0x000006FA System.Void easyar.OutputFrameSource::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void OutputFrameSource__ctor_m7B1B093801B9830C6A10D40997125E261FE05695 (void);
// 0x000006FB System.Object easyar.OutputFrameSource::CloneObject()
extern void OutputFrameSource_CloneObject_m7B0B3CE2602DE1381DC675321585B84E2C8E182D (void);
// 0x000006FC easyar.OutputFrameSource easyar.OutputFrameSource::Clone()
extern void OutputFrameSource_Clone_m79F86970969005956C2D90CF5DC799936F31F7F2 (void);
// 0x000006FD System.Void easyar.OutputFrameSource::setHandler(easyar.Optional`1<System.Action`1<easyar.OutputFrame>>)
extern void OutputFrameSource_setHandler_mE249CA5CA073F5A1425B04E45D784E7C9C1FA99B (void);
// 0x000006FE System.Void easyar.OutputFrameSource::connect(easyar.OutputFrameSink)
extern void OutputFrameSource_connect_m95CD69C3BDBE03CAC8B8352B4C8CF17A570178B9 (void);
// 0x000006FF System.Void easyar.OutputFrameSource::disconnect()
extern void OutputFrameSource_disconnect_mD824580F9CF482E579E3BBA0EF501EDE1ADBB2BB (void);
// 0x00000700 System.Void easyar.OutputFrameSource/<>c::.cctor()
extern void U3CU3Ec__cctor_m1A728C179F576784D834E83D114F19717DA70CBA (void);
// 0x00000701 System.Void easyar.OutputFrameSource/<>c::.ctor()
extern void U3CU3Ec__ctor_m1069BA0026EB506FFCC8779BAB0C0D018DA0D337 (void);
// 0x00000702 easyar.Detail/OptionalOfFunctorOfVoidFromOutputFrame easyar.OutputFrameSource/<>c::<setHandler>b__3_0(easyar.Optional`1<System.Action`1<easyar.OutputFrame>>)
extern void U3CU3Ec_U3CsetHandlerU3Eb__3_0_mB1F105F26E48763225CC384FFAD7B986B92D4A88 (void);
// 0x00000703 System.Void easyar.FeedbackFrameSink::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void FeedbackFrameSink__ctor_m66EB61B466EE16DAF192525B3526EEFE71E00948 (void);
// 0x00000704 System.Object easyar.FeedbackFrameSink::CloneObject()
extern void FeedbackFrameSink_CloneObject_m60167008FE8EDF83B67AF12279FDA5A25C9272D8 (void);
// 0x00000705 easyar.FeedbackFrameSink easyar.FeedbackFrameSink::Clone()
extern void FeedbackFrameSink_Clone_mA60B9FE10D30448C55F9BC499862A1FFE59A2D2C (void);
// 0x00000706 System.Void easyar.FeedbackFrameSink::handle(easyar.FeedbackFrame)
extern void FeedbackFrameSink_handle_m87587A957222D425211EFE576BDC42C617A25AFB (void);
// 0x00000707 System.Void easyar.FeedbackFrameSource::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void FeedbackFrameSource__ctor_mC2E77C18906210037B4C1F91B5E692DB8022F966 (void);
// 0x00000708 System.Object easyar.FeedbackFrameSource::CloneObject()
extern void FeedbackFrameSource_CloneObject_m1AC640C09FE6E4C0AE46500F40F751210E44CD08 (void);
// 0x00000709 easyar.FeedbackFrameSource easyar.FeedbackFrameSource::Clone()
extern void FeedbackFrameSource_Clone_mD93996E1934B36D4B3E1758EFAE49A5ED20998B2 (void);
// 0x0000070A System.Void easyar.FeedbackFrameSource::setHandler(easyar.Optional`1<System.Action`1<easyar.FeedbackFrame>>)
extern void FeedbackFrameSource_setHandler_m3CE9B290717D7A8CE0C930CF95D44F85B3507C0D (void);
// 0x0000070B System.Void easyar.FeedbackFrameSource::connect(easyar.FeedbackFrameSink)
extern void FeedbackFrameSource_connect_mBB4835615A9C5E9B5656685256D1F137DD8996A2 (void);
// 0x0000070C System.Void easyar.FeedbackFrameSource::disconnect()
extern void FeedbackFrameSource_disconnect_m66BC4E4D809F6FD23F5247F669224955510BE020 (void);
// 0x0000070D System.Void easyar.FeedbackFrameSource/<>c::.cctor()
extern void U3CU3Ec__cctor_m9858EBDD939010A4CE3E3C611022123BE3F02A77 (void);
// 0x0000070E System.Void easyar.FeedbackFrameSource/<>c::.ctor()
extern void U3CU3Ec__ctor_m19A1D3C7D242B1662C7FF580DE996D690C44724C (void);
// 0x0000070F easyar.Detail/OptionalOfFunctorOfVoidFromFeedbackFrame easyar.FeedbackFrameSource/<>c::<setHandler>b__3_0(easyar.Optional`1<System.Action`1<easyar.FeedbackFrame>>)
extern void U3CU3Ec_U3CsetHandlerU3Eb__3_0_mD514007AC7F366E44236D233F2FFA14B43B38C3B (void);
// 0x00000710 System.Void easyar.InputFrameFork::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void InputFrameFork__ctor_m17118CE4AB9A5176001E3B8474DA9EAAB9A54839 (void);
// 0x00000711 System.Object easyar.InputFrameFork::CloneObject()
extern void InputFrameFork_CloneObject_m3DB48DED011601A2A1F6CC6E742D52A45E1E1876 (void);
// 0x00000712 easyar.InputFrameFork easyar.InputFrameFork::Clone()
extern void InputFrameFork_Clone_mC5C061DBF80105A21FE988BF8DBC29CE99362652 (void);
// 0x00000713 easyar.InputFrameSink easyar.InputFrameFork::input()
extern void InputFrameFork_input_mD04733942558F38A26DFE88A68E142F1D48E8A19 (void);
// 0x00000714 easyar.InputFrameSource easyar.InputFrameFork::output(System.Int32)
extern void InputFrameFork_output_mDF50E3856CC5F778A1CC444CEAC523007E8B6A8E (void);
// 0x00000715 System.Int32 easyar.InputFrameFork::outputCount()
extern void InputFrameFork_outputCount_mDBEFE3BBF510D74C6710FE1F207D6A04AFA39C92 (void);
// 0x00000716 easyar.InputFrameFork easyar.InputFrameFork::create(System.Int32)
extern void InputFrameFork_create_m999F1022EA13ABA8FD8925B0F606E3711FBD0232 (void);
// 0x00000717 System.Void easyar.OutputFrameFork::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void OutputFrameFork__ctor_m6384B8744D108857AABC1ECA11258F3457039C09 (void);
// 0x00000718 System.Object easyar.OutputFrameFork::CloneObject()
extern void OutputFrameFork_CloneObject_mB8A53AC9838CFB099B2AAF9152444608E2AA72CF (void);
// 0x00000719 easyar.OutputFrameFork easyar.OutputFrameFork::Clone()
extern void OutputFrameFork_Clone_m08972987705B87A777F5E4266A3E52BDD0FBBCBB (void);
// 0x0000071A easyar.OutputFrameSink easyar.OutputFrameFork::input()
extern void OutputFrameFork_input_m0686138B31FC557A39E3AF83BCE00CFCB872F372 (void);
// 0x0000071B easyar.OutputFrameSource easyar.OutputFrameFork::output(System.Int32)
extern void OutputFrameFork_output_m32E1B3B2AD2E00CA2C9CDB55937C75BAF9A72F33 (void);
// 0x0000071C System.Int32 easyar.OutputFrameFork::outputCount()
extern void OutputFrameFork_outputCount_mB7C218FEE5ABD955E77D7D58194D1E353B3BDFC6 (void);
// 0x0000071D easyar.OutputFrameFork easyar.OutputFrameFork::create(System.Int32)
extern void OutputFrameFork_create_m04CAC3B9F4E72C447A163438AB4CFFF2DE0E4032 (void);
// 0x0000071E System.Void easyar.OutputFrameJoin::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void OutputFrameJoin__ctor_m2D30ED77EE63BFF843D93B23B5C95A936F8F8AA5 (void);
// 0x0000071F System.Object easyar.OutputFrameJoin::CloneObject()
extern void OutputFrameJoin_CloneObject_mE7415677B8811DE3EA1F6C34EA6672F435A0D400 (void);
// 0x00000720 easyar.OutputFrameJoin easyar.OutputFrameJoin::Clone()
extern void OutputFrameJoin_Clone_m32BE2C59D5C6BF7702E4A4552DEE532FB7C54E91 (void);
// 0x00000721 easyar.OutputFrameSink easyar.OutputFrameJoin::input(System.Int32)
extern void OutputFrameJoin_input_mA41E326C752DD455C0E371F0F399D7CAFC7181ED (void);
// 0x00000722 easyar.OutputFrameSource easyar.OutputFrameJoin::output()
extern void OutputFrameJoin_output_mDBB48BDF3A421A24E702F2DEB97952CB64750610 (void);
// 0x00000723 System.Int32 easyar.OutputFrameJoin::inputCount()
extern void OutputFrameJoin_inputCount_m8B0CFD17DA4D5A2E67D356545CCD3A2D949F58AA (void);
// 0x00000724 easyar.OutputFrameJoin easyar.OutputFrameJoin::create(System.Int32)
extern void OutputFrameJoin_create_m1883720ED8FEF4945E1859A4C0BACC92EDA18B78 (void);
// 0x00000725 easyar.OutputFrameJoin easyar.OutputFrameJoin::createWithJoiner(System.Int32,System.Func`2<System.Collections.Generic.List`1<easyar.OutputFrame>,easyar.OutputFrame>)
extern void OutputFrameJoin_createWithJoiner_mF2CC13FD592928B8555C0D05E82ED8350B492122 (void);
// 0x00000726 System.Void easyar.FeedbackFrameFork::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void FeedbackFrameFork__ctor_m1DC2FEA03B57CE0FD44145413093C6BC93100DA8 (void);
// 0x00000727 System.Object easyar.FeedbackFrameFork::CloneObject()
extern void FeedbackFrameFork_CloneObject_m10CAE68BCCAD36865C1C2A65914040E4FA8BA6FF (void);
// 0x00000728 easyar.FeedbackFrameFork easyar.FeedbackFrameFork::Clone()
extern void FeedbackFrameFork_Clone_m66431A91CE860DC56D1E5851D0DDB57D3301F915 (void);
// 0x00000729 easyar.FeedbackFrameSink easyar.FeedbackFrameFork::input()
extern void FeedbackFrameFork_input_m83A845198F703166D153677154E9E8CBF26CD800 (void);
// 0x0000072A easyar.FeedbackFrameSource easyar.FeedbackFrameFork::output(System.Int32)
extern void FeedbackFrameFork_output_m1FF4AF1F564A6959FB47EFA171BECB6DFA8F7A95 (void);
// 0x0000072B System.Int32 easyar.FeedbackFrameFork::outputCount()
extern void FeedbackFrameFork_outputCount_mCDDA337990E6F135792508008BDADB29241D544F (void);
// 0x0000072C easyar.FeedbackFrameFork easyar.FeedbackFrameFork::create(System.Int32)
extern void FeedbackFrameFork_create_m65592FF80794A1689DA76C173D6A754288983656 (void);
// 0x0000072D System.Void easyar.InputFrameThrottler::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void InputFrameThrottler__ctor_mC22472E10E7E9B877597F47F17077466959F1474 (void);
// 0x0000072E System.Object easyar.InputFrameThrottler::CloneObject()
extern void InputFrameThrottler_CloneObject_m447C2CDDD0B2EA349C85E1CAC313181D032FE3D1 (void);
// 0x0000072F easyar.InputFrameThrottler easyar.InputFrameThrottler::Clone()
extern void InputFrameThrottler_Clone_m0739F6857D3955D3D669515363B18E89993FCA63 (void);
// 0x00000730 easyar.InputFrameSink easyar.InputFrameThrottler::input()
extern void InputFrameThrottler_input_mF80B6D53658144F0C31D42B72F65BECCE01ADF2D (void);
// 0x00000731 System.Int32 easyar.InputFrameThrottler::bufferRequirement()
extern void InputFrameThrottler_bufferRequirement_mC49D379EA541D8E48B41919BFCF20C481C65D9AD (void);
// 0x00000732 easyar.InputFrameSource easyar.InputFrameThrottler::output()
extern void InputFrameThrottler_output_m0F63EED956B251537AE2003FCBA17A5FAEDF3888 (void);
// 0x00000733 easyar.SignalSink easyar.InputFrameThrottler::signalInput()
extern void InputFrameThrottler_signalInput_mA9AE8B8F4E66474791454B26D6D57C6135626F98 (void);
// 0x00000734 easyar.InputFrameThrottler easyar.InputFrameThrottler::create()
extern void InputFrameThrottler_create_mB75A540454EF3A2D92BCE4FED1B406CF0E755B50 (void);
// 0x00000735 System.Void easyar.OutputFrameBuffer::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void OutputFrameBuffer__ctor_mB7491291402EF4BD50E11AB74F11E34F188A86CA (void);
// 0x00000736 System.Object easyar.OutputFrameBuffer::CloneObject()
extern void OutputFrameBuffer_CloneObject_m939D2A12BF7200FA934E95734014179DF0BBF196 (void);
// 0x00000737 easyar.OutputFrameBuffer easyar.OutputFrameBuffer::Clone()
extern void OutputFrameBuffer_Clone_m6F7500202390D5EDB2CC22A5FD403A5C7B40389B (void);
// 0x00000738 easyar.OutputFrameSink easyar.OutputFrameBuffer::input()
extern void OutputFrameBuffer_input_mE624A9BBD675A37B31F4E35198BD48765B772A48 (void);
// 0x00000739 System.Int32 easyar.OutputFrameBuffer::bufferRequirement()
extern void OutputFrameBuffer_bufferRequirement_mD7AC36795AE73AE223D0866B1B3295DD9F3F7D28 (void);
// 0x0000073A easyar.SignalSource easyar.OutputFrameBuffer::signalOutput()
extern void OutputFrameBuffer_signalOutput_mC2E7C1A10A70119446AF41BDFFE416A9492AEB41 (void);
// 0x0000073B easyar.Optional`1<easyar.OutputFrame> easyar.OutputFrameBuffer::peek()
extern void OutputFrameBuffer_peek_m1F1F6547ECAA7AC886932F61E8E74AFDAC055B06 (void);
// 0x0000073C easyar.OutputFrameBuffer easyar.OutputFrameBuffer::create()
extern void OutputFrameBuffer_create_m5BE4D97F5B4C7C5F2C176E60664F79E73056A6F4 (void);
// 0x0000073D System.Void easyar.OutputFrameBuffer::pause()
extern void OutputFrameBuffer_pause_mE8A667EE393FCFA3E4E5C9F0B34CA452F9377583 (void);
// 0x0000073E System.Void easyar.OutputFrameBuffer::resume()
extern void OutputFrameBuffer_resume_m284DB54061DE37B4936DB9427FE2A913608088DA (void);
// 0x0000073F System.Void easyar.OutputFrameBuffer/<>c::.cctor()
extern void U3CU3Ec__cctor_mC830C71099D43EA6C411964DF571AC00A82DB7B5 (void);
// 0x00000740 System.Void easyar.OutputFrameBuffer/<>c::.ctor()
extern void U3CU3Ec__ctor_mFB9C6C8A3ADBC9F4629CBB7314AC6154F572C64E (void);
// 0x00000741 easyar.Optional`1<easyar.OutputFrame> easyar.OutputFrameBuffer/<>c::<peek>b__6_0(easyar.Detail/OptionalOfOutputFrame)
extern void U3CU3Ec_U3CpeekU3Eb__6_0_m53220CCE8E132C636E720A9E8C3536E34C0C4368 (void);
// 0x00000742 System.Void easyar.InputFrameToOutputFrameAdapter::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void InputFrameToOutputFrameAdapter__ctor_mC608FBFDD92AF7B57CD1BEC4E1AF70D5F62CC637 (void);
// 0x00000743 System.Object easyar.InputFrameToOutputFrameAdapter::CloneObject()
extern void InputFrameToOutputFrameAdapter_CloneObject_mC42CF32EC7FB384BD3C7E222CAAFB62E2DD376D0 (void);
// 0x00000744 easyar.InputFrameToOutputFrameAdapter easyar.InputFrameToOutputFrameAdapter::Clone()
extern void InputFrameToOutputFrameAdapter_Clone_m86B7F0EBAFD742B7F4F90D92BD8451CEF67A65FF (void);
// 0x00000745 easyar.InputFrameSink easyar.InputFrameToOutputFrameAdapter::input()
extern void InputFrameToOutputFrameAdapter_input_m0ADAA0D1B2017465D5F750C5733D0DE12608DA38 (void);
// 0x00000746 easyar.OutputFrameSource easyar.InputFrameToOutputFrameAdapter::output()
extern void InputFrameToOutputFrameAdapter_output_mDC5D7D8D160264836C5D4E266259C30B439912FA (void);
// 0x00000747 easyar.InputFrameToOutputFrameAdapter easyar.InputFrameToOutputFrameAdapter::create()
extern void InputFrameToOutputFrameAdapter_create_m1BAFC1EE15A2ED9BE7BA524194B04A121555DCAC (void);
// 0x00000748 System.Void easyar.InputFrameToFeedbackFrameAdapter::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void InputFrameToFeedbackFrameAdapter__ctor_m4390C1F142C9F92EFA9E5084A2547F1321134F69 (void);
// 0x00000749 System.Object easyar.InputFrameToFeedbackFrameAdapter::CloneObject()
extern void InputFrameToFeedbackFrameAdapter_CloneObject_m2DAE79D9AF7029A13FF5CA6B030633FBD275E4E1 (void);
// 0x0000074A easyar.InputFrameToFeedbackFrameAdapter easyar.InputFrameToFeedbackFrameAdapter::Clone()
extern void InputFrameToFeedbackFrameAdapter_Clone_mB68AB89A79EE4D28007158F5A54A8F08D74EFA28 (void);
// 0x0000074B easyar.InputFrameSink easyar.InputFrameToFeedbackFrameAdapter::input()
extern void InputFrameToFeedbackFrameAdapter_input_mD15FDE6F26F3EB144D0B1E51935A9028E5B0533F (void);
// 0x0000074C System.Int32 easyar.InputFrameToFeedbackFrameAdapter::bufferRequirement()
extern void InputFrameToFeedbackFrameAdapter_bufferRequirement_m15FF3CF1B171BEBFD345EB4EFEB8C2B1B7F6FB07 (void);
// 0x0000074D easyar.OutputFrameSink easyar.InputFrameToFeedbackFrameAdapter::sideInput()
extern void InputFrameToFeedbackFrameAdapter_sideInput_mB98F4ED017BF05CF1842D92230C88025502D2363 (void);
// 0x0000074E easyar.FeedbackFrameSource easyar.InputFrameToFeedbackFrameAdapter::output()
extern void InputFrameToFeedbackFrameAdapter_output_mF4AD1D647991F8B1B40CCABDFC910D5EC243199D (void);
// 0x0000074F easyar.InputFrameToFeedbackFrameAdapter easyar.InputFrameToFeedbackFrameAdapter::create()
extern void InputFrameToFeedbackFrameAdapter_create_m0B6CEC2D513888586480D8FA109D7469FBE853E5 (void);
// 0x00000750 System.Void easyar.InputFrame::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void InputFrame__ctor_m81DD5033A8A698CDACE58150C6E6BA620EA619CC (void);
// 0x00000751 System.Object easyar.InputFrame::CloneObject()
extern void InputFrame_CloneObject_mBC899D9EC74733B024E36D30A5B2EF4D90A218D0 (void);
// 0x00000752 easyar.InputFrame easyar.InputFrame::Clone()
extern void InputFrame_Clone_m8FA11D41A8F2F6055F990F73205187851B796809 (void);
// 0x00000753 System.Int32 easyar.InputFrame::index()
extern void InputFrame_index_m27F54C00B774FF87966F2457F824C2A7C2811B3E (void);
// 0x00000754 easyar.Image easyar.InputFrame::image()
extern void InputFrame_image_m536F0E29192D23AF7E38CC51049C184FDEB967DE (void);
// 0x00000755 System.Boolean easyar.InputFrame::hasCameraParameters()
extern void InputFrame_hasCameraParameters_m0F90CE7C2ACA9D51A6FF7C555F9EF1FBB1ADB55E (void);
// 0x00000756 easyar.CameraParameters easyar.InputFrame::cameraParameters()
extern void InputFrame_cameraParameters_m9FC20524F32BAE442FD99BD4EB95D48EF419B7B7 (void);
// 0x00000757 System.Boolean easyar.InputFrame::hasTemporalInformation()
extern void InputFrame_hasTemporalInformation_mD4EB7502D896016121DF12AFC8553D4A52FDDACF (void);
// 0x00000758 System.Double easyar.InputFrame::timestamp()
extern void InputFrame_timestamp_m36ECBEA832F71C81E73059F623DB588C7F7DF9AC (void);
// 0x00000759 System.Boolean easyar.InputFrame::hasSpatialInformation()
extern void InputFrame_hasSpatialInformation_mE5007CE57DB1751E90F92EA732EC335B9932EFC4 (void);
// 0x0000075A easyar.Matrix44F easyar.InputFrame::cameraTransform()
extern void InputFrame_cameraTransform_mDA4DB985E2E5BB72AADFA417A9613E9D36D0B37A (void);
// 0x0000075B easyar.MotionTrackingStatus easyar.InputFrame::trackingStatus()
extern void InputFrame_trackingStatus_m4325CED81A205A8DE188C8CA7BBA761B4E662E6B (void);
// 0x0000075C easyar.InputFrame easyar.InputFrame::create(easyar.Image,easyar.CameraParameters,System.Double,easyar.Matrix44F,easyar.MotionTrackingStatus)
extern void InputFrame_create_mCE51842BFFCE90F24821A6D89F518A9C8F12411D (void);
// 0x0000075D easyar.InputFrame easyar.InputFrame::createWithImageAndCameraParametersAndTemporal(easyar.Image,easyar.CameraParameters,System.Double)
extern void InputFrame_createWithImageAndCameraParametersAndTemporal_m49D5A2847CB71BE635240ECF1C50112E0A1C5E8A (void);
// 0x0000075E easyar.InputFrame easyar.InputFrame::createWithImageAndCameraParameters(easyar.Image,easyar.CameraParameters)
extern void InputFrame_createWithImageAndCameraParameters_mC84C387CE178227D6BCD276081802F9442A9F0C4 (void);
// 0x0000075F easyar.InputFrame easyar.InputFrame::createWithImage(easyar.Image)
extern void InputFrame_createWithImage_m15D62333429079C139FC57B665A00EFF307BEB96 (void);
// 0x00000760 System.Void easyar.FrameFilterResult::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void FrameFilterResult__ctor_m0FC7681961D80786AD40895C713546BCE0218B2C (void);
// 0x00000761 System.Object easyar.FrameFilterResult::CloneObject()
extern void FrameFilterResult_CloneObject_m9D8DF86977E41FF19F1A1733B03CFDF6D23CE2FD (void);
// 0x00000762 easyar.FrameFilterResult easyar.FrameFilterResult::Clone()
extern void FrameFilterResult_Clone_mAFE92B83CF6D168657A2C1F7767DCAC5D27D0713 (void);
// 0x00000763 System.Void easyar.OutputFrame::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void OutputFrame__ctor_m0443F67E82979F438FDEE13055404E09489F57E1 (void);
// 0x00000764 System.Object easyar.OutputFrame::CloneObject()
extern void OutputFrame_CloneObject_mF2D1C1F6091C6F0DCD3B47898189BB1AE534432C (void);
// 0x00000765 easyar.OutputFrame easyar.OutputFrame::Clone()
extern void OutputFrame_Clone_mBF41E503B01E569D5D43FDFD2C0AA8CAA90644E7 (void);
// 0x00000766 System.Void easyar.OutputFrame::.ctor(easyar.InputFrame,System.Collections.Generic.List`1<easyar.Optional`1<easyar.FrameFilterResult>>)
extern void OutputFrame__ctor_m8998855452A69F094037351EC512202637D0BB9F (void);
// 0x00000767 System.Int32 easyar.OutputFrame::index()
extern void OutputFrame_index_mBAB93E8678567DC8EBB057F0C76C8A06A7C2417F (void);
// 0x00000768 easyar.InputFrame easyar.OutputFrame::inputFrame()
extern void OutputFrame_inputFrame_m293C0B2D0DA5D7FFCD6B81EA941584F74947191A (void);
// 0x00000769 System.Collections.Generic.List`1<easyar.Optional`1<easyar.FrameFilterResult>> easyar.OutputFrame::results()
extern void OutputFrame_results_m761ADE9ADBB5AC1FB7F00BDBDC4A16CA039A124E (void);
// 0x0000076A System.Void easyar.FeedbackFrame::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void FeedbackFrame__ctor_mC3EC8158AECF3CC976968B6ACACED03C11DB4EC3 (void);
// 0x0000076B System.Object easyar.FeedbackFrame::CloneObject()
extern void FeedbackFrame_CloneObject_mAC3D44752A000A8038E27974B8438C0B6BDA817D (void);
// 0x0000076C easyar.FeedbackFrame easyar.FeedbackFrame::Clone()
extern void FeedbackFrame_Clone_m1DE009912835532D9A66907AEDBC8B112EB4F437 (void);
// 0x0000076D System.Void easyar.FeedbackFrame::.ctor(easyar.InputFrame,easyar.Optional`1<easyar.OutputFrame>)
extern void FeedbackFrame__ctor_m640980DB624B1C99109140C58AE54599FF0048DC (void);
// 0x0000076E easyar.InputFrame easyar.FeedbackFrame::inputFrame()
extern void FeedbackFrame_inputFrame_mB058D6B78748B6FE3775BAF2CB398CEB3443359D (void);
// 0x0000076F easyar.Optional`1<easyar.OutputFrame> easyar.FeedbackFrame::previousOutputFrame()
extern void FeedbackFrame_previousOutputFrame_m6914A0A65CCCAB23DBF21E9CBDEA1DCF8D674160 (void);
// 0x00000770 System.Void easyar.FeedbackFrame/<>c::.cctor()
extern void U3CU3Ec__cctor_m2506F497A721EC7F7456032A79B4F9E21C7751CC (void);
// 0x00000771 System.Void easyar.FeedbackFrame/<>c::.ctor()
extern void U3CU3Ec__ctor_m822E5BA08E2770EB951977E4A218593C60E2E5E2 (void);
// 0x00000772 easyar.Detail/OptionalOfOutputFrame easyar.FeedbackFrame/<>c::<.ctor>b__3_0(easyar.Optional`1<easyar.OutputFrame>)
extern void U3CU3Ec_U3C_ctorU3Eb__3_0_mFD92DDE4DEAA15AF0CC560F99AE3E021BFC345B5 (void);
// 0x00000773 easyar.Optional`1<easyar.OutputFrame> easyar.FeedbackFrame/<>c::<previousOutputFrame>b__5_0(easyar.Detail/OptionalOfOutputFrame)
extern void U3CU3Ec_U3CpreviousOutputFrameU3Eb__5_0_m06C8AF5E3FD194455043062AD0F9E2F87D11AEAE (void);
// 0x00000774 System.Void easyar.Target::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void Target__ctor_m0F8C993DA2C6A0F4A4195AABBFE2CA1F3D36FE8B (void);
// 0x00000775 System.Object easyar.Target::CloneObject()
extern void Target_CloneObject_m5DAA36082516F47CBE4782EF1CFF0725F9A228D7 (void);
// 0x00000776 easyar.Target easyar.Target::Clone()
extern void Target_Clone_mFF613F20D81D0FB57E892A0114DB8F2015AF7435 (void);
// 0x00000777 System.Int32 easyar.Target::runtimeID()
extern void Target_runtimeID_m2141C7AD5E6034F57D5B58AA3F50D247F8568233 (void);
// 0x00000778 System.String easyar.Target::uid()
extern void Target_uid_m6B84D7B0B65CB41A017957EF964C8B4C6B759A75 (void);
// 0x00000779 System.String easyar.Target::name()
extern void Target_name_m0CD7B1AE425F364629BB2F20F7DCA3358E878145 (void);
// 0x0000077A System.Void easyar.Target::setName(System.String)
extern void Target_setName_mE313CEF02B8D1182EBF3C0FBC0FD3D8542B2C8E8 (void);
// 0x0000077B System.String easyar.Target::meta()
extern void Target_meta_mA66CD47AFC055BE46703360607370BD97C44A31D (void);
// 0x0000077C System.Void easyar.Target::setMeta(System.String)
extern void Target_setMeta_mFB750089A0644D006BEAA60BBD3CBB58D1F53551 (void);
// 0x0000077D System.Void easyar.TargetInstance::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void TargetInstance__ctor_mBAE8860180C7B7F414CBBD19F49ED4A2AC9C88EC (void);
// 0x0000077E System.Object easyar.TargetInstance::CloneObject()
extern void TargetInstance_CloneObject_m21C8DAC0824C77414FA815DE1FC1140B8D383FD0 (void);
// 0x0000077F easyar.TargetInstance easyar.TargetInstance::Clone()
extern void TargetInstance_Clone_mD0EA63A05D60BC6268E26722089B45733FA4271E (void);
// 0x00000780 System.Void easyar.TargetInstance::.ctor()
extern void TargetInstance__ctor_mDA3276A19EB368FED4C1E1F413FBD0799F67F23A (void);
// 0x00000781 easyar.TargetStatus easyar.TargetInstance::status()
extern void TargetInstance_status_mF64B473A947B8BFEB340824123163B7137791F77 (void);
// 0x00000782 easyar.Optional`1<easyar.Target> easyar.TargetInstance::target()
extern void TargetInstance_target_mE658848D1B612A20841A195EBE206DD384C460B6 (void);
// 0x00000783 easyar.Matrix44F easyar.TargetInstance::pose()
extern void TargetInstance_pose_m827C307640827DF3E224B3A3806CCEF370E9475E (void);
// 0x00000784 System.Void easyar.TargetInstance/<>c::.cctor()
extern void U3CU3Ec__cctor_m31F943B8F265DE2DA8C5852063EAF0C48CBBFE2B (void);
// 0x00000785 System.Void easyar.TargetInstance/<>c::.ctor()
extern void U3CU3Ec__ctor_mF61004C209C4C13F8C0441A3144F1FA8017148A5 (void);
// 0x00000786 easyar.Optional`1<easyar.Target> easyar.TargetInstance/<>c::<target>b__5_0(easyar.Detail/OptionalOfTarget)
extern void U3CU3Ec_U3CtargetU3Eb__5_0_mC0DE2A81D0356AABFE643F12258AFC67A11DEDFF (void);
// 0x00000787 System.Void easyar.TargetTrackerResult::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void TargetTrackerResult__ctor_m82E004D3CEB2D0AFEF65965F3366EC940D5A45F9 (void);
// 0x00000788 System.Object easyar.TargetTrackerResult::CloneObject()
extern void TargetTrackerResult_CloneObject_mD61B16D179823FAC6C70EEB1A73EC27CE66CBD11 (void);
// 0x00000789 easyar.TargetTrackerResult easyar.TargetTrackerResult::Clone()
extern void TargetTrackerResult_Clone_mB63D4259A74C5479A6CB4C902C48FD5711B7020F (void);
// 0x0000078A System.Collections.Generic.List`1<easyar.TargetInstance> easyar.TargetTrackerResult::targetInstances()
extern void TargetTrackerResult_targetInstances_m32C1C586D9538A8074325E10A785BD8AC03D86A6 (void);
// 0x0000078B System.Void easyar.TargetTrackerResult::setTargetInstances(System.Collections.Generic.List`1<easyar.TargetInstance>)
extern void TargetTrackerResult_setTargetInstances_m45F8BDA021ACBDC84D180B0E4ACEB714ADADCD08 (void);
// 0x0000078C System.Void easyar.TextureId::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase/Retainer)
extern void TextureId__ctor_m2226206AA94326DE9DD1023DF055FC7DB57DF8CB (void);
// 0x0000078D System.Object easyar.TextureId::CloneObject()
extern void TextureId_CloneObject_mFC5AB1E7855CF0A036AFB31E847955286AB0C6F9 (void);
// 0x0000078E easyar.TextureId easyar.TextureId::Clone()
extern void TextureId_Clone_mDC6E21EE1197E2E103AF60F3CFD333C32395177E (void);
// 0x0000078F System.Int32 easyar.TextureId::getInt()
extern void TextureId_getInt_mB2FC4BA8235670FB1BD37D7C2A5FF7E0410E4027 (void);
// 0x00000790 System.IntPtr easyar.TextureId::getPointer()
extern void TextureId_getPointer_m79CD499A5A756FD93E6B09DC176F599355B9581E (void);
// 0x00000791 easyar.TextureId easyar.TextureId::fromInt(System.Int32)
extern void TextureId_fromInt_mA0500F164F6B0F0F529E4D9904CBDB7DB9BB990F (void);
// 0x00000792 easyar.TextureId easyar.TextureId::fromPointer(System.IntPtr)
extern void TextureId_fromPointer_m0940855F9DCDDBC505BD90E8CA38EA147BC19AF0 (void);
// 0x00000793 System.Void easyar.CameraImageMaterial::.ctor()
extern void CameraImageMaterial__ctor_m891D7300151DF69FE6DEBB8906F55A7E4B93F523 (void);
// 0x00000794 System.Void easyar.CameraImageMaterial::Finalize()
extern void CameraImageMaterial_Finalize_m05D1DE9C744A36E76A4ACE6CD58A6E4D265DFFB7 (void);
// 0x00000795 System.Void easyar.CameraImageMaterial::Dispose()
extern void CameraImageMaterial_Dispose_m5518D807CB2F800B58FD34A39305425AAC66EB8D (void);
// 0x00000796 UnityEngine.Material easyar.CameraImageMaterial::UpdateByImage(easyar.Image)
extern void CameraImageMaterial_UpdateByImage_mD68BDE3FF5CC6A31F32F30680F53EACB0E78F500 (void);
// 0x00000797 System.Void easyar.CameraImageMaterial::DisposeResources()
extern void CameraImageMaterial_DisposeResources_m75B0E76CE09C46DD369BC518C30147AD5CA8C5F8 (void);
// 0x00000798 System.Void easyar.CameraImageRenderer::add_OnFrameRenderUpdate(System.Action`2<UnityEngine.Material,UnityEngine.Vector2>)
extern void CameraImageRenderer_add_OnFrameRenderUpdate_m51A0C232CFF90B955C67688FDF8BB86EA34997D7 (void);
// 0x00000799 System.Void easyar.CameraImageRenderer::remove_OnFrameRenderUpdate(System.Action`2<UnityEngine.Material,UnityEngine.Vector2>)
extern void CameraImageRenderer_remove_OnFrameRenderUpdate_mC9D3AE9A0D3839A54144AF9F19A5CD119EB3DB34 (void);
// 0x0000079A System.Void easyar.CameraImageRenderer::add_TargetTextureChange(System.Action`2<UnityEngine.Camera,UnityEngine.RenderTexture>)
extern void CameraImageRenderer_add_TargetTextureChange_mAF7C7E5E3E3C695B331C152F9B904C8C3B7FD14F (void);
// 0x0000079B System.Void easyar.CameraImageRenderer::remove_TargetTextureChange(System.Action`2<UnityEngine.Camera,UnityEngine.RenderTexture>)
extern void CameraImageRenderer_remove_TargetTextureChange_m78E2A24B9CA80FDBE52FA01D1B9A6A551013A8A3 (void);
// 0x0000079C System.Void easyar.CameraImageRenderer::Awake()
extern void CameraImageRenderer_Awake_mC115975632F4D94D27E945871FA58A421776BA8D (void);
// 0x0000079D System.Void easyar.CameraImageRenderer::OnEnable()
extern void CameraImageRenderer_OnEnable_mFE28229ED1A0278ECACAC21CFA9F0FFA3E56721C (void);
// 0x0000079E System.Void easyar.CameraImageRenderer::OnDisable()
extern void CameraImageRenderer_OnDisable_mCBFA7D127848088EBFCF47D6460E3DDF8B39382C (void);
// 0x0000079F System.Void easyar.CameraImageRenderer::OnDestroy()
extern void CameraImageRenderer_OnDestroy_mC6CFEB935DB66F8D48FC0A483F22976B173E0CD8 (void);
// 0x000007A0 System.Void easyar.CameraImageRenderer::RequestTargetTexture(System.Action`2<UnityEngine.Camera,UnityEngine.RenderTexture>)
extern void CameraImageRenderer_RequestTargetTexture_m01D2F8CDDB119CECB1E4438F3C8A4B3573710F06 (void);
// 0x000007A1 System.Void easyar.CameraImageRenderer::DropTargetTexture(System.Action`2<UnityEngine.Camera,UnityEngine.RenderTexture>)
extern void CameraImageRenderer_DropTargetTexture_m0BD2E03B5D4483F3CE929C9BA30DE6F950412DED (void);
// 0x000007A2 System.Void easyar.CameraImageRenderer::OnAssemble(easyar.ARSession)
extern void CameraImageRenderer_OnAssemble_mAC0ACF6E3253578B90C5F73933622C6B6B56A463 (void);
// 0x000007A3 System.Void easyar.CameraImageRenderer::SetHFilp(System.Boolean)
extern void CameraImageRenderer_SetHFilp_m988D8ADB2EF66819A92044454D46EFEB6B96D85A (void);
// 0x000007A4 System.Void easyar.CameraImageRenderer::OnFrameChange(easyar.OutputFrame,UnityEngine.Matrix4x4)
extern void CameraImageRenderer_OnFrameChange_mFCCF3E5396F487D6FA26B8B71CB496A675D36114 (void);
// 0x000007A5 System.Void easyar.CameraImageRenderer::OnFrameUpdate(easyar.OutputFrame)
extern void CameraImageRenderer_OnFrameUpdate_mAF1315BB7EDB305911FA219C9CF4303A239A77E7 (void);
// 0x000007A6 System.Void easyar.CameraImageRenderer::UpdateCommandBuffer(UnityEngine.Camera,UnityEngine.Material)
extern void CameraImageRenderer_UpdateCommandBuffer_mF5ACA819D6E6FC5244FBBCA098107F193C5873A9 (void);
// 0x000007A7 System.Void easyar.CameraImageRenderer::RemoveCommandBuffer(UnityEngine.Camera)
extern void CameraImageRenderer_RemoveCommandBuffer_mA25A74A92D650A93EDE29FE5E58CEEB79DFDEBF2 (void);
// 0x000007A8 System.Void easyar.CameraImageRenderer::.ctor()
extern void CameraImageRenderer__ctor_mD5780AB6A458E50A4D973F98558926992610A4D1 (void);
// 0x000007A9 System.Void easyar.CameraImageRenderer/UserRequest::Finalize()
extern void UserRequest_Finalize_mB22508F92B12DBC3B0093B345D973E39F86A73D3 (void);
// 0x000007AA System.Void easyar.CameraImageRenderer/UserRequest::Dispose()
extern void UserRequest_Dispose_m7C6AF56AF6AECE1ACB106AB6CA44C1FD31546D48 (void);
// 0x000007AB System.Boolean easyar.CameraImageRenderer/UserRequest::UpdateTexture(UnityEngine.Camera,UnityEngine.Material,UnityEngine.RenderTexture&)
extern void UserRequest_UpdateTexture_m3282FE8EB2D6CE8CD9D38684E1891DF21340156B (void);
// 0x000007AC System.Void easyar.CameraImageRenderer/UserRequest::UpdateCommandBuffer(UnityEngine.Camera,UnityEngine.Material)
extern void UserRequest_UpdateCommandBuffer_m1CC087B85E9211B0F93F022FB1BB5F0D35042EDA (void);
// 0x000007AD System.Void easyar.CameraImageRenderer/UserRequest::RemoveCommandBuffer(UnityEngine.Camera)
extern void UserRequest_RemoveCommandBuffer_m9D25DA2C0EDE47C8056DAE1093122E861C20D4B8 (void);
// 0x000007AE System.Void easyar.CameraImageRenderer/UserRequest::.ctor()
extern void UserRequest__ctor_mE441F2A8FA332F749C37009ED40D52BB90CB1A4F (void);
// 0x000007AF System.Void easyar.CameraImageShaders::.ctor()
extern void CameraImageShaders__ctor_m74134E120B7AB7B1DA8A08665FB98AE21D493B46 (void);
// 0x000007B0 System.Int32 easyar.CameraSource::get_BufferCapacity()
extern void CameraSource_get_BufferCapacity_m479124B776E159091ABF4196E7C0C71E92A365D9 (void);
// 0x000007B1 System.Void easyar.CameraSource::set_BufferCapacity(System.Int32)
extern void CameraSource_set_BufferCapacity_mDBC7729ABCB5C9B32C01A7B5C06967B497D40809 (void);
// 0x000007B2 System.Void easyar.CameraSource::Start()
extern void CameraSource_Start_m9CE8615E485AB424C7432D08446A13017FAAE8EC (void);
// 0x000007B3 System.Void easyar.CameraSource::OnDestroy()
extern void CameraSource_OnDestroy_m4F320EE0941FBF33C8E406D61D58F6D269C8F592 (void);
// 0x000007B4 System.Void easyar.CameraSource::Open()
// 0x000007B5 System.Void easyar.CameraSource::Close()
// 0x000007B6 System.Void easyar.CameraSource::.ctor()
extern void CameraSource__ctor_m79F649183C3751B904BDDCD3BD349B28C0B33A9D (void);
// 0x000007B7 System.Int32 easyar.IDisplay::get_Rotation()
// 0x000007B8 System.Void easyar.Display::.ctor()
extern void Display__ctor_mCB708C6F79C556534C00BD513B744B3262C7E8DA (void);
// 0x000007B9 System.Void easyar.Display::Finalize()
extern void Display_Finalize_m9A63397FF5AB80EF28262A82F2A1B0190A8F97E0 (void);
// 0x000007BA System.Int32 easyar.Display::get_Rotation()
extern void Display_get_Rotation_m184494F525116017620BD3493403CAC5AE66D54E (void);
// 0x000007BB System.Void easyar.Display::Dispose()
extern void Display_Dispose_m2FA572504702BBDD96F38A17117B6853818888C2 (void);
// 0x000007BC System.Void easyar.Display::InitializeIOS()
extern void Display_InitializeIOS_m7EC74EF42B7B1D941D046F1EFEEA6C22463CE625 (void);
// 0x000007BD System.Void easyar.Display::InitializeAndroid()
extern void Display_InitializeAndroid_m3595BB80B4C5BA8099475AEBE7107BAD159432A4 (void);
// 0x000007BE System.Void easyar.Display::DeleteAndroidJavaObjects()
extern void Display_DeleteAndroidJavaObjects_m46A6CEBA7D1708D79840F2B5E4D5DB29A4F4E232 (void);
// 0x000007BF System.Int32 easyar.DisplayEmulator::get_Rotation()
extern void DisplayEmulator_get_Rotation_mB9E15EBA1BFAE08CB03D0509C103E91E1BED84C0 (void);
// 0x000007C0 System.Void easyar.DisplayEmulator::set_Rotation(System.Int32)
extern void DisplayEmulator_set_Rotation_m5626FF085412A9BF4F86CDC79D842FD29A94F305 (void);
// 0x000007C1 System.Void easyar.DisplayEmulator::EmulateRotation(System.Int32)
extern void DisplayEmulator_EmulateRotation_mA5A0F332F57E65F01412C3A1836C11F149609B27 (void);
// 0x000007C2 System.Void easyar.DisplayEmulator::.ctor()
extern void DisplayEmulator__ctor_mC13A24C4AEB20EA0E333A31F41E2581374518D81 (void);
// 0x000007C3 System.Void easyar.RenderCameraController::OnEnable()
extern void RenderCameraController_OnEnable_m7D4712EE9023B4769508F7FB5EB7CA7D1B67FBBD (void);
// 0x000007C4 System.Void easyar.RenderCameraController::OnDisable()
extern void RenderCameraController_OnDisable_m2EBC51E929E67810E6B5F2019F3B9D34FD6450EE (void);
// 0x000007C5 System.Void easyar.RenderCameraController::OnDestroy()
extern void RenderCameraController_OnDestroy_mC6E7478523B725A031825072F335EB0F29414002 (void);
// 0x000007C6 System.Void easyar.RenderCameraController::OnAssemble(easyar.ARSession)
extern void RenderCameraController_OnAssemble_m28826376DEAAEA8B5A6C9284587A22B4EAF794C5 (void);
// 0x000007C7 System.Void easyar.RenderCameraController::SetProjectHFlip(System.Boolean)
extern void RenderCameraController_SetProjectHFlip_m534466C602B0A3DB8A965D1B2018C05E3A09EF42 (void);
// 0x000007C8 System.Void easyar.RenderCameraController::SetRenderImageHFilp(System.Boolean)
extern void RenderCameraController_SetRenderImageHFilp_m4CAEA16AC2589DF87F27473DB889F7AD0EE97A57 (void);
// 0x000007C9 System.Void easyar.RenderCameraController::OnFrameChange(easyar.OutputFrame,UnityEngine.Matrix4x4)
extern void RenderCameraController_OnFrameChange_m50C92530C5DBE72E424BB8853C26EA91AE6C170C (void);
// 0x000007CA System.Void easyar.RenderCameraController::OnFrameUpdate(easyar.OutputFrame)
extern void RenderCameraController_OnFrameUpdate_m15509507D757103F83B776C45405896EF1F98C71 (void);
// 0x000007CB System.Void easyar.RenderCameraController::.ctor()
extern void RenderCameraController__ctor_m417EF82791B213B2CB05ED13E8C30B929606B945 (void);
// 0x000007CC System.Void easyar.RenderCameraController::<OnFrameUpdate>b__15_0()
extern void RenderCameraController_U3COnFrameUpdateU3Eb__15_0_mB648F9A4567EEEE592C4E8366B39BB4C11C24152 (void);
// 0x000007CD System.Void easyar.RenderCameraController::<OnFrameUpdate>b__15_1()
extern void RenderCameraController_U3COnFrameUpdateU3Eb__15_1_m2E3D602A08DF91717909CC18E505A4AB28907002 (void);
// 0x000007CE System.Void easyar.RenderCameraEventHandler::add_PreRender(System.Action)
extern void RenderCameraEventHandler_add_PreRender_m2B5CB28BCD7FD08991DA0C37A3FD8572B5B50FE8 (void);
// 0x000007CF System.Void easyar.RenderCameraEventHandler::remove_PreRender(System.Action)
extern void RenderCameraEventHandler_remove_PreRender_m170313DE9016B8BE301148361DB4385E5B7102F1 (void);
// 0x000007D0 System.Void easyar.RenderCameraEventHandler::add_PostRender(System.Action)
extern void RenderCameraEventHandler_add_PostRender_mAE8817443F0267F4799FF8FD04DC718B7799C2EC (void);
// 0x000007D1 System.Void easyar.RenderCameraEventHandler::remove_PostRender(System.Action)
extern void RenderCameraEventHandler_remove_PostRender_mB83C13F707EB6CBA4786BD8C6AD3D580680004D3 (void);
// 0x000007D2 System.Void easyar.RenderCameraEventHandler::OnPreRender()
extern void RenderCameraEventHandler_OnPreRender_mC832C98837259BE583FC3CF87794818B5C5E6DAD (void);
// 0x000007D3 System.Void easyar.RenderCameraEventHandler::OnPostRender()
extern void RenderCameraEventHandler_OnPostRender_m4B0563A488FA9B3DA6103CEA95FFD5B0A283AEAD (void);
// 0x000007D4 System.Void easyar.RenderCameraEventHandler::.ctor()
extern void RenderCameraEventHandler__ctor_m6113923386E51F7322883A98BD9B733A0A260D39 (void);
// 0x000007D5 System.Void easyar.RenderCameraParameters::Finalize()
extern void RenderCameraParameters_Finalize_m26AE8B5D9789DB57830B48FAC8F8EAF331775D6A (void);
// 0x000007D6 UnityEngine.Matrix4x4 easyar.RenderCameraParameters::get_Transform()
extern void RenderCameraParameters_get_Transform_m3E7E68A87D3D8F5EFCC7A209DCD42545BF62B0DD (void);
// 0x000007D7 System.Void easyar.RenderCameraParameters::set_Transform(UnityEngine.Matrix4x4)
extern void RenderCameraParameters_set_Transform_m72CB53FC12350A7F3F15769ACE79F46F9F481708 (void);
// 0x000007D8 easyar.CameraParameters easyar.RenderCameraParameters::get_Parameters()
extern void RenderCameraParameters_get_Parameters_m0EDC1415649355013C64BBAABEEE13E84178A618 (void);
// 0x000007D9 System.Void easyar.RenderCameraParameters::set_Parameters(easyar.CameraParameters)
extern void RenderCameraParameters_set_Parameters_m573A60BFD02F1DAF1316611AEAC9520681530AE7 (void);
// 0x000007DA System.Void easyar.RenderCameraParameters::Build(easyar.CameraParameters)
extern void RenderCameraParameters_Build_m4E10A9681C735D1FE207AAEE8608CAECFD232743 (void);
// 0x000007DB System.Void easyar.RenderCameraParameters::Dispose()
extern void RenderCameraParameters_Dispose_mC430B7A598EE0F8949E065269D2238354D413352 (void);
// 0x000007DC System.Void easyar.RenderCameraParameters::.ctor()
extern void RenderCameraParameters__ctor_m5BD82DF7FA740112DFB6154FB49936B4B4C7AE53 (void);
// 0x000007DD System.Void easyar.RenderCameraParameters::.cctor()
extern void RenderCameraParameters__cctor_mC2C503329DD10E64C6336E4C78329CC845AB8DF5 (void);
// 0x000007DE easyar.CameraDevice easyar.VideoCameraDevice::get_Device()
extern void VideoCameraDevice_get_Device_m926B3C984C4B5D61119460CA890229146FDE6355 (void);
// 0x000007DF System.Void easyar.VideoCameraDevice::set_Device(easyar.CameraDevice)
extern void VideoCameraDevice_set_Device_m79AD8296E6911D67EE9443AF8002911C250CD6AE (void);
// 0x000007E0 System.Void easyar.VideoCameraDevice::add_DeviceCreated(System.Action)
extern void VideoCameraDevice_add_DeviceCreated_m3E212D2F198B64A65897F769F074B15BEBFFAA36 (void);
// 0x000007E1 System.Void easyar.VideoCameraDevice::remove_DeviceCreated(System.Action)
extern void VideoCameraDevice_remove_DeviceCreated_m515E14F76CBAA5895B5E25B38A206DD27F0A5903 (void);
// 0x000007E2 System.Void easyar.VideoCameraDevice::add_DeviceOpened(System.Action)
extern void VideoCameraDevice_add_DeviceOpened_m77F110C7EC10229FA0081A405A144D24F9403DE1 (void);
// 0x000007E3 System.Void easyar.VideoCameraDevice::remove_DeviceOpened(System.Action)
extern void VideoCameraDevice_remove_DeviceOpened_mB4CD73BD43608CCAE16B64BCC970E50C3F2C1B50 (void);
// 0x000007E4 System.Void easyar.VideoCameraDevice::add_DeviceClosed(System.Action)
extern void VideoCameraDevice_add_DeviceClosed_m3859CE363FA4A25C778697DF921AF7FEEA05BD33 (void);
// 0x000007E5 System.Void easyar.VideoCameraDevice::remove_DeviceClosed(System.Action)
extern void VideoCameraDevice_remove_DeviceClosed_m65127FC471D2D556004BC3FCF95979A663D9FAC6 (void);
// 0x000007E6 System.Int32 easyar.VideoCameraDevice::get_BufferCapacity()
extern void VideoCameraDevice_get_BufferCapacity_m8CAE82C03FAC65C466F4B1672E733A6B2F9B9D74 (void);
// 0x000007E7 System.Void easyar.VideoCameraDevice::set_BufferCapacity(System.Int32)
extern void VideoCameraDevice_set_BufferCapacity_mC0ECB8987900A7A99D520FE6494BB1AF9ECF9A1D (void);
// 0x000007E8 System.Boolean easyar.VideoCameraDevice::get_HasSpatialInformation()
extern void VideoCameraDevice_get_HasSpatialInformation_m105F2ED4D48A6DA19370DA0A0807399618ED10AB (void);
// 0x000007E9 easyar.CameraDevicePreference easyar.VideoCameraDevice::get_CameraPreference()
extern void VideoCameraDevice_get_CameraPreference_m9060E303773933E9FC46E1E5F91CBD4CEEA855C3 (void);
// 0x000007EA System.Void easyar.VideoCameraDevice::set_CameraPreference(easyar.CameraDevicePreference)
extern void VideoCameraDevice_set_CameraPreference_mFD218A17FEB6DACFD057AC006341FAF222B2B6CA (void);
// 0x000007EB easyar.CameraParameters easyar.VideoCameraDevice::get_Parameters()
extern void VideoCameraDevice_get_Parameters_mDC0A0FB9F550111DEC85835E8B44CF322371B314 (void);
// 0x000007EC System.Void easyar.VideoCameraDevice::set_Parameters(easyar.CameraParameters)
extern void VideoCameraDevice_set_Parameters_m85A3BBCBE499206C58889499C8ADA543713DDC64 (void);
// 0x000007ED System.Void easyar.VideoCameraDevice::OnEnable()
extern void VideoCameraDevice_OnEnable_m25EDBD1AC7230FBE57152BD4BD7514FF4876A93F (void);
// 0x000007EE System.Void easyar.VideoCameraDevice::Start()
extern void VideoCameraDevice_Start_mA825191047A6CBEACAF10A30B5F623E5DFE9D2FB (void);
// 0x000007EF System.Void easyar.VideoCameraDevice::OnDisable()
extern void VideoCameraDevice_OnDisable_m8449FEE1BC9875B52537288897521DF432FA18E2 (void);
// 0x000007F0 System.Void easyar.VideoCameraDevice::Open()
extern void VideoCameraDevice_Open_mBA044207C8C73CE4C8B293FFF9056577BFEE6335 (void);
// 0x000007F1 System.Void easyar.VideoCameraDevice::Close()
extern void VideoCameraDevice_Close_m654B136E1E79F2EAB85390859CD06E09A9F03A15 (void);
// 0x000007F2 System.Void easyar.VideoCameraDevice::Connect(easyar.InputFrameSink)
extern void VideoCameraDevice_Connect_mB1077D20F8B9B510A76BED74443F002092B4C672 (void);
// 0x000007F3 System.Void easyar.VideoCameraDevice::.ctor()
extern void VideoCameraDevice__ctor_m234E16844A3093CD04BE199560C41368186DE6BE (void);
// 0x000007F4 System.Void easyar.VideoCameraDevice::<Open>b__36_0(easyar.PermissionStatus,System.String)
extern void VideoCameraDevice_U3COpenU3Eb__36_0_m190320BF0CC8352DE99FFCD853BC7D15C35AAC9D (void);
// 0x000007F5 System.Void easyar.VideoRecorder::add_StatusUpdate(System.Action`2<easyar.RecordStatus,System.String>)
extern void VideoRecorder_add_StatusUpdate_m063C7113C310771CDE2D387DC7F8BB834660BB86 (void);
// 0x000007F6 System.Void easyar.VideoRecorder::remove_StatusUpdate(System.Action`2<easyar.RecordStatus,System.String>)
extern void VideoRecorder_remove_StatusUpdate_m2A7DD8BCF5AEAC7DD6065798C822E87B2FF420B1 (void);
// 0x000007F7 System.Boolean easyar.VideoRecorder::get_IsReady()
extern void VideoRecorder_get_IsReady_mEC576F4B82E4DDA3400B8EF8593EAC7429F2639C (void);
// 0x000007F8 System.Void easyar.VideoRecorder::set_IsReady(System.Boolean)
extern void VideoRecorder_set_IsReady_m45B7BD7D9955CF1B069513E7E670DED48E63FE5E (void);
// 0x000007F9 System.Void easyar.VideoRecorder::Start()
extern void VideoRecorder_Start_m73AA2322830A08014A7C34950909F106052AE67C (void);
// 0x000007FA System.Void easyar.VideoRecorder::OnDestroy()
extern void VideoRecorder_OnDestroy_m0911AA05822C1F12C4A97AC5B63A44F433410914 (void);
// 0x000007FB System.Boolean easyar.VideoRecorder::StartRecording()
extern void VideoRecorder_StartRecording_m0E5464CACB87C8F71182071AD7CE082BA58FDEF8 (void);
// 0x000007FC System.Boolean easyar.VideoRecorder::StartRecording(easyar.RecorderConfiguration)
extern void VideoRecorder_StartRecording_mA47FFA0671EA3ACD7DBD0BBC38113AC162AF54C3 (void);
// 0x000007FD System.Boolean easyar.VideoRecorder::StopRecording()
extern void VideoRecorder_StopRecording_m5CB89ECDC7A32563CB35A6D0E1D2D18AA522C69E (void);
// 0x000007FE System.Boolean easyar.VideoRecorder::RecordFrame(UnityEngine.RenderTexture)
extern void VideoRecorder_RecordFrame_mEB4FA7E5EA4E4C018448BD346F3CC71FD32791D8 (void);
// 0x000007FF System.Void easyar.VideoRecorder::.ctor()
extern void VideoRecorder__ctor_mCBD3F8E27220F67A10F5407D82EFC6AA980559E8 (void);
// 0x00000800 System.Void easyar.VideoRecorder::<Start>b__14_0(easyar.PermissionStatus,System.String)
extern void VideoRecorder_U3CStartU3Eb__14_0_m19D1B3C3E07883B5B2F14E30A2B681AAB83DBAB6 (void);
// 0x00000801 System.Void easyar.VideoRecorder::<StartRecording>b__17_0(easyar.RecordStatus,System.String)
extern void VideoRecorder_U3CStartRecordingU3Eb__17_0_m2BA06D3E64FB113F664EBC97423BCCA9FA5BDE1A (void);
// 0x00000802 easyar.EasyARController easyar.EasyARController::get_Instance()
extern void EasyARController_get_Instance_m4D9BEEAA229476C1639FC24DA46A94A5620A0D30 (void);
// 0x00000803 System.Void easyar.EasyARController::set_Instance(easyar.EasyARController)
extern void EasyARController_set_Instance_m0644DDA7B23B4DF33F6CA10E1EBBAF23EC11AAC5 (void);
// 0x00000804 System.Boolean easyar.EasyARController::get_Initialized()
extern void EasyARController_get_Initialized_m0FCA276095BC052B4B970D3D1354FB1E6AA98C41 (void);
// 0x00000805 System.Void easyar.EasyARController::set_Initialized(System.Boolean)
extern void EasyARController_set_Initialized_mFD745ACB6C6E691DABA81F36CBC5525734185717 (void);
// 0x00000806 easyar.DelayedCallbackScheduler easyar.EasyARController::get_Scheduler()
extern void EasyARController_get_Scheduler_mC7D3A3E19D863258A8AEF0905F37C16AE54760E6 (void);
// 0x00000807 System.Void easyar.EasyARController::set_Scheduler(easyar.DelayedCallbackScheduler)
extern void EasyARController_set_Scheduler_m5DB7C2DB70602A75751F494AB8DB5B86ABBFB15F (void);
// 0x00000808 easyar.EasyARSettings easyar.EasyARController::get_Settings()
extern void EasyARController_get_Settings_mBDF979F9B6F8A3D7585EE709A953D2610FB717BD (void);
// 0x00000809 System.String easyar.EasyARController::get_settingsPath()
extern void EasyARController_get_settingsPath_m879FAD5ADDEEE11E82DED4D955946D09980B7B41 (void);
// 0x0000080A easyar.ThreadWorker easyar.EasyARController::get_Worker()
extern void EasyARController_get_Worker_m7EC07F0D41BF1A762F1B21D68779BA48B00B3E9E (void);
// 0x0000080B System.Void easyar.EasyARController::set_Worker(easyar.ThreadWorker)
extern void EasyARController_set_Worker_mFB4F26C6EA7D7CA0AA5D741339D8E34538D1EC91 (void);
// 0x0000080C easyar.Display easyar.EasyARController::get_Display()
extern void EasyARController_get_Display_mD3A3DC439FCED5221F7A096AF46AB2EB43D4A32E (void);
// 0x0000080D System.Void easyar.EasyARController::set_Display(easyar.Display)
extern void EasyARController_set_Display_m8172B0A0B2853E66E36B9EEA28F929E6D08EC73D (void);
// 0x0000080E System.Void easyar.EasyARController::GlobalInitialization()
extern void EasyARController_GlobalInitialization_mB63A67532D901C55F5B3A086D12E443A395FB87F (void);
// 0x0000080F System.Void easyar.EasyARController::Awake()
extern void EasyARController_Awake_m20721285FE4E5BE95DE758D9EAFA191F86EDE51C (void);
// 0x00000810 System.Void easyar.EasyARController::Update()
extern void EasyARController_Update_m4EB3B1167A1587D0571D342AB08CACD19AE987D6 (void);
// 0x00000811 System.Void easyar.EasyARController::OnApplicationPause(System.Boolean)
extern void EasyARController_OnApplicationPause_m155C86A2532D2A279161052A46B0024DA58AD38C (void);
// 0x00000812 System.Void easyar.EasyARController::OnDestroy()
extern void EasyARController_OnDestroy_m0C2A8C69599AAC81F21482374CE00479D307B33E (void);
// 0x00000813 System.Void easyar.EasyARController::ShowErrorMessage()
extern void EasyARController_ShowErrorMessage_m1D87BEB50E1689DC937EDA8A0495A980DDBC072B (void);
// 0x00000814 System.Void easyar.EasyARController::.ctor()
extern void EasyARController__ctor_m52AD84F1B8E607D4D3932A146BFF5EEB9D080AF1 (void);
// 0x00000815 System.Void easyar.EasyARController/<>c::.cctor()
extern void U3CU3Ec__cctor_m6EBCCA933137A54884E44EDCAB072CBD529A7D32 (void);
// 0x00000816 System.Void easyar.EasyARController/<>c::.ctor()
extern void U3CU3Ec__ctor_mB9C9D7A9CCB189D9FAA2ACFDF4F9B3566C13D872 (void);
// 0x00000817 System.Void easyar.EasyARController/<>c::<GlobalInitialization>b__26_0(System.Object,System.EventArgs)
extern void U3CU3Ec_U3CGlobalInitializationU3Eb__26_0_m9CDF7A53069A9F7678CF50899CABD21FBE87FDC7 (void);
// 0x00000818 System.Void easyar.EasyARController/<>c::<GlobalInitialization>b__26_1(System.Object,System.UnhandledExceptionEventArgs)
extern void U3CU3Ec_U3CGlobalInitializationU3Eb__26_1_m0BCD9FB4FF3FC073C8AAF36504B5B64399544FF6 (void);
// 0x00000819 System.Void easyar.EasyARSettings::.ctor()
extern void EasyARSettings__ctor_m5374E04073BEBE36CF622A552961728E3261B496 (void);
// 0x0000081A System.Void easyar.EasyARSettings/TargetGizmoConfig::.ctor()
extern void TargetGizmoConfig__ctor_mBAB4814FAC6CA9C9153268D9A2377AA7F166E6E8 (void);
// 0x0000081B System.Void easyar.EasyARSettings/TargetGizmoConfig/ImageTargetConfig::.ctor()
extern void ImageTargetConfig__ctor_mE1D2417A07CBEFDB3E8D087170D349BD95F3A50C (void);
// 0x0000081C System.Void easyar.EasyARSettings/TargetGizmoConfig/ObjectTargetConfig::.ctor()
extern void ObjectTargetConfig__ctor_m840408CBA0C203A1982B4020B21FF3342CAA32F5 (void);
// 0x0000081D System.Void easyar.EasyARSettings/AndroidManifestPermission::.ctor()
extern void AndroidManifestPermission__ctor_mD2CDFFDB852784CBF5F0839B422455B2E14BF30D (void);
// 0x0000081E easyar.CloudRecognizer easyar.CloudRecognizerFrameFilter::get_CloudRecognizer()
extern void CloudRecognizerFrameFilter_get_CloudRecognizer_m354F93CE6C51A14562194DE2BCEA51F27EE49D5F (void);
// 0x0000081F System.Void easyar.CloudRecognizerFrameFilter::set_CloudRecognizer(easyar.CloudRecognizer)
extern void CloudRecognizerFrameFilter_set_CloudRecognizer_m63F102BA1221D4CB3E75A63FB2E693A3B4B054F0 (void);
// 0x00000820 System.Int32 easyar.CloudRecognizerFrameFilter::get_BufferRequirement()
extern void CloudRecognizerFrameFilter_get_BufferRequirement_m18153E863488C9038CAE9F67A6FE76FA9F96B6A3 (void);
// 0x00000821 System.Void easyar.CloudRecognizerFrameFilter::Start()
extern void CloudRecognizerFrameFilter_Start_m014FF15AC1CE4DF9F69449B59B64C0A63C2BE0D9 (void);
// 0x00000822 System.Void easyar.CloudRecognizerFrameFilter::OnDestroy()
extern void CloudRecognizerFrameFilter_OnDestroy_m1E778CF275B87416A85F20415CDAA433E017C807 (void);
// 0x00000823 System.Void easyar.CloudRecognizerFrameFilter::Resolve(System.Action`1<easyar.InputFrame>,System.Action`1<easyar.CloudRecognizationResult>)
extern void CloudRecognizerFrameFilter_Resolve_mA0BA7D6AC33924752B0AF2A52500DC879CAFDE77 (void);
// 0x00000824 System.Void easyar.CloudRecognizerFrameFilter::OnAssemble(easyar.ARSession)
extern void CloudRecognizerFrameFilter_OnAssemble_mC446E6A7D90669466A6042D439E408F395C41B97 (void);
// 0x00000825 System.Void easyar.CloudRecognizerFrameFilter::OnFrameUpdate(easyar.OutputFrame)
extern void CloudRecognizerFrameFilter_OnFrameUpdate_m60D7EAC06807B5D1851940D3BD366D4EB09812C6 (void);
// 0x00000826 System.Void easyar.CloudRecognizerFrameFilter::NotifyEmptyConfig(easyar.CloudRecognizerFrameFilter/CloudRecognizerServiceConfig)
extern void CloudRecognizerFrameFilter_NotifyEmptyConfig_m4D40B0961815B84706B0AFC19BB61F6BA68A34F5 (void);
// 0x00000827 System.Void easyar.CloudRecognizerFrameFilter::NotifyEmptyPrivateConfig(easyar.CloudRecognizerFrameFilter/PrivateCloudRecognizerServiceConfig)
extern void CloudRecognizerFrameFilter_NotifyEmptyPrivateConfig_m1E469C9CF3B114D611CA61A21EFE350C8F841946 (void);
// 0x00000828 System.Void easyar.CloudRecognizerFrameFilter::.ctor()
extern void CloudRecognizerFrameFilter__ctor_m973DD3E7D4A44A8C175949A8168C93BFEBAF2A7C (void);
// 0x00000829 System.Void easyar.CloudRecognizerFrameFilter/CloudRecognizerServiceConfig::.ctor()
extern void CloudRecognizerServiceConfig__ctor_mFBFFA6D0CC5FBDC497A8078E229F903918952AA5 (void);
// 0x0000082A System.Void easyar.CloudRecognizerFrameFilter/PrivateCloudRecognizerServiceConfig::.ctor()
extern void PrivateCloudRecognizerServiceConfig__ctor_m88055E744DF478668D83D729C446444DC4365D38 (void);
// 0x0000082B System.Void easyar.CloudRecognizerFrameFilter/Request::.ctor()
extern void Request__ctor_m518394CDF817588BDD9A9F9FBFA8CA766EE0BCA5 (void);
// 0x0000082C easyar.ImageTarget easyar.ImageTargetController::get_Target()
extern void ImageTargetController_get_Target_m0F1C3C12D9FBF9ADC0AEB5320D86F30D19C0D811 (void);
// 0x0000082D System.Void easyar.ImageTargetController::set_Target(easyar.ImageTarget)
extern void ImageTargetController_set_Target_m691E8B52130CF50D8A749C600CF8E1A9DFC5442B (void);
// 0x0000082E System.Void easyar.ImageTargetController::add_TargetAvailable(System.Action)
extern void ImageTargetController_add_TargetAvailable_m9467BEDC472B4D21DEEC28741A0CEAFB29DFA820 (void);
// 0x0000082F System.Void easyar.ImageTargetController::remove_TargetAvailable(System.Action)
extern void ImageTargetController_remove_TargetAvailable_m8329089F509E9519EFF04A625CC1666E5A635B50 (void);
// 0x00000830 System.Void easyar.ImageTargetController::add_TargetLoad(System.Action`2<easyar.Target,System.Boolean>)
extern void ImageTargetController_add_TargetLoad_m0D7180BCFF4AEAEB052DD4EA929097BE08EF188C (void);
// 0x00000831 System.Void easyar.ImageTargetController::remove_TargetLoad(System.Action`2<easyar.Target,System.Boolean>)
extern void ImageTargetController_remove_TargetLoad_m02E72F6F67A3C78E8AFB012F713611088C6AF932 (void);
// 0x00000832 System.Void easyar.ImageTargetController::add_TargetUnload(System.Action`2<easyar.Target,System.Boolean>)
extern void ImageTargetController_add_TargetUnload_mF3BD3D47A1BA72C2A2A9A5FBAEDD875AA78AC24B (void);
// 0x00000833 System.Void easyar.ImageTargetController::remove_TargetUnload(System.Action`2<easyar.Target,System.Boolean>)
extern void ImageTargetController_remove_TargetUnload_mFC4F4CC13F66062F92A926CB7B71B135F11D9B4D (void);
// 0x00000834 easyar.ImageTrackerFrameFilter easyar.ImageTargetController::get_Tracker()
extern void ImageTargetController_get_Tracker_mE03FC1F82F8E0C6FD3E3B92AB7CACA07AF49E19A (void);
// 0x00000835 System.Void easyar.ImageTargetController::set_Tracker(easyar.ImageTrackerFrameFilter)
extern void ImageTargetController_set_Tracker_mC89AB8C2EBA0BD373F609D9B9D62BCBF9926BBB2 (void);
// 0x00000836 UnityEngine.Vector2 easyar.ImageTargetController::get_Size()
extern void ImageTargetController_get_Size_mFB5B22CC61808686226D884EACC483548F4C681A (void);
// 0x00000837 System.Void easyar.ImageTargetController::set_Size(UnityEngine.Vector2)
extern void ImageTargetController_set_Size_mA8B2E028DB76456BA724C9F7AB03A55F7284C7FC (void);
// 0x00000838 System.Void easyar.ImageTargetController::Start()
extern void ImageTargetController_Start_mB95572C8073F005F283E54B98C688316DD551BA0 (void);
// 0x00000839 System.Void easyar.ImageTargetController::Update()
extern void ImageTargetController_Update_m2F7449CEA3063F93B13BC0E681C42C0D6F932739 (void);
// 0x0000083A System.Void easyar.ImageTargetController::OnDestroy()
extern void ImageTargetController_OnDestroy_mC7D2FC8958B5982D68ACD59E88F97A07C7474C04 (void);
// 0x0000083B System.Void easyar.ImageTargetController::OnTracking()
extern void ImageTargetController_OnTracking_m1770BE57B5C0A7A79634F3227EB7DBF7ACF2992D (void);
// 0x0000083C System.Void easyar.ImageTargetController::LoadImageFile(easyar.ImageTargetController/ImageFileSourceData)
extern void ImageTargetController_LoadImageFile_m54813DDF2569E837F90E668F2322117EB09ABE94 (void);
// 0x0000083D System.Void easyar.ImageTargetController::LoadTargetDataFile(easyar.ImageTargetController/TargetDataFileSourceData)
extern void ImageTargetController_LoadTargetDataFile_m5FF7AC85BB633051C3374596A18744A9E4CB1FD9 (void);
// 0x0000083E System.Void easyar.ImageTargetController::LoadTarget(easyar.ImageTarget)
extern void ImageTargetController_LoadTarget_m753797B436A84D91896419D3D347C27249E1DD6C (void);
// 0x0000083F System.Collections.IEnumerator easyar.ImageTargetController::LoadImageBuffer(easyar.Buffer,easyar.ImageTargetController/ImageFileSourceData)
extern void ImageTargetController_LoadImageBuffer_mB52CC13907B585A80CD0E74F48F4AC4D04F9CF86 (void);
// 0x00000840 System.Collections.IEnumerator easyar.ImageTargetController::LoadTargetDataBuffer(easyar.Buffer)
extern void ImageTargetController_LoadTargetDataBuffer_mDCC3A373F4D20BBB75237CAA55538F03F15E976A (void);
// 0x00000841 System.Void easyar.ImageTargetController::UpdateTargetInTracker()
extern void ImageTargetController_UpdateTargetInTracker_m1B4B7735ACB8DEED5757D4B829AE9DADABDEA775 (void);
// 0x00000842 System.Void easyar.ImageTargetController::UpdateScale()
extern void ImageTargetController_UpdateScale_mD04D7FA8EC2FDBB1A2C26D689DC202C34C2C2883 (void);
// 0x00000843 System.Void easyar.ImageTargetController::CheckScale()
extern void ImageTargetController_CheckScale_m0F4D1461DB0C79333C54E75476BC76864548B700 (void);
// 0x00000844 System.Void easyar.ImageTargetController::.ctor()
extern void ImageTargetController__ctor_mDD385F7B1202536BE4B6F67CCCF19A95C8BE8773 (void);
// 0x00000845 System.Void easyar.ImageTargetController::<LoadTargetDataFile>b__35_0(easyar.Buffer)
extern void ImageTargetController_U3CLoadTargetDataFileU3Eb__35_0_m376DC236B4B665D74CB1642A180D1DF362DB8FA5 (void);
// 0x00000846 System.Void easyar.ImageTargetController::<UpdateTargetInTracker>b__39_0(easyar.Target,System.Boolean)
extern void ImageTargetController_U3CUpdateTargetInTrackerU3Eb__39_0_mC5711419892B651ACFD0BF21371CB426D3C5C10A (void);
// 0x00000847 System.Void easyar.ImageTargetController/ImageFileSourceData::.ctor()
extern void ImageFileSourceData__ctor_mDEA8F58FDC88D9E5CAA030AC6A05B6AE1BBD01E2 (void);
// 0x00000848 System.Void easyar.ImageTargetController/TargetDataFileSourceData::.ctor()
extern void TargetDataFileSourceData__ctor_m7F8546325EC90508CF9A14D3499EF6F48A72869B (void);
// 0x00000849 System.Void easyar.ImageTargetController/<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_m03F77038B0975BF8B645BF622B5BB531D5DC9D77 (void);
// 0x0000084A System.Void easyar.ImageTargetController/<>c__DisplayClass34_0::<LoadImageFile>b__0(easyar.Buffer)
extern void U3CU3Ec__DisplayClass34_0_U3CLoadImageFileU3Eb__0_m9957693513E83EE4788DB8947D031C7F38B30319 (void);
// 0x0000084B System.Void easyar.ImageTargetController/<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_mBAC544DA80A8F17F86D93C88452F013642FDEEB3 (void);
// 0x0000084C System.Void easyar.ImageTargetController/<>c__DisplayClass37_0::<LoadImageBuffer>b__0()
extern void U3CU3Ec__DisplayClass37_0_U3CLoadImageBufferU3Eb__0_mDDB31AC06D55CA990525A40B5E30EE838E189848 (void);
// 0x0000084D System.Void easyar.ImageTargetController/<LoadImageBuffer>d__37::.ctor(System.Int32)
extern void U3CLoadImageBufferU3Ed__37__ctor_m316EB6054CB5AECB04721EA523876CB3B8F7CFF4 (void);
// 0x0000084E System.Void easyar.ImageTargetController/<LoadImageBuffer>d__37::System.IDisposable.Dispose()
extern void U3CLoadImageBufferU3Ed__37_System_IDisposable_Dispose_m9549C468CA4876B3B335D784700FB6C36890511D (void);
// 0x0000084F System.Boolean easyar.ImageTargetController/<LoadImageBuffer>d__37::MoveNext()
extern void U3CLoadImageBufferU3Ed__37_MoveNext_m79CDDD968D06BF040B34605939751B570404232E (void);
// 0x00000850 System.Void easyar.ImageTargetController/<LoadImageBuffer>d__37::<>m__Finally1()
extern void U3CLoadImageBufferU3Ed__37_U3CU3Em__Finally1_m1FA515671DFAE081EEC0F649834566FF740FD131 (void);
// 0x00000851 System.Object easyar.ImageTargetController/<LoadImageBuffer>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadImageBufferU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA37C5AAEACB0AA727307F7BE6217A3E2B3EDD78B (void);
// 0x00000852 System.Void easyar.ImageTargetController/<LoadImageBuffer>d__37::System.Collections.IEnumerator.Reset()
extern void U3CLoadImageBufferU3Ed__37_System_Collections_IEnumerator_Reset_mE9E76167788A27BAF0B470EBF78DE2E3BD92946F (void);
// 0x00000853 System.Object easyar.ImageTargetController/<LoadImageBuffer>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CLoadImageBufferU3Ed__37_System_Collections_IEnumerator_get_Current_m0A268429535E1FFCB1091F7B5CE73BBB1AC6FD49 (void);
// 0x00000854 System.Void easyar.ImageTargetController/<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_m07EEDC11957897E2C5A11D6B644D24A7F9849A84 (void);
// 0x00000855 System.Void easyar.ImageTargetController/<>c__DisplayClass38_0::<LoadTargetDataBuffer>b__0()
extern void U3CU3Ec__DisplayClass38_0_U3CLoadTargetDataBufferU3Eb__0_m89E626FEEAD3E79B22ED80836208CB2751781783 (void);
// 0x00000856 System.Void easyar.ImageTargetController/<LoadTargetDataBuffer>d__38::.ctor(System.Int32)
extern void U3CLoadTargetDataBufferU3Ed__38__ctor_m77E8BEB2D46B0FC1D095FFEB0CA65C72FC0C3C3C (void);
// 0x00000857 System.Void easyar.ImageTargetController/<LoadTargetDataBuffer>d__38::System.IDisposable.Dispose()
extern void U3CLoadTargetDataBufferU3Ed__38_System_IDisposable_Dispose_mED0D6ADF10935E5905B204001A364D6B3267C858 (void);
// 0x00000858 System.Boolean easyar.ImageTargetController/<LoadTargetDataBuffer>d__38::MoveNext()
extern void U3CLoadTargetDataBufferU3Ed__38_MoveNext_m068E451D3DD00141574A30F0D2903EC196B2146F (void);
// 0x00000859 System.Void easyar.ImageTargetController/<LoadTargetDataBuffer>d__38::<>m__Finally1()
extern void U3CLoadTargetDataBufferU3Ed__38_U3CU3Em__Finally1_mE8E170661AF53D113C6C42049C50779A7119F369 (void);
// 0x0000085A System.Object easyar.ImageTargetController/<LoadTargetDataBuffer>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadTargetDataBufferU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m965E6D287A33DAA0DA332F7023DC1E356F17F0F0 (void);
// 0x0000085B System.Void easyar.ImageTargetController/<LoadTargetDataBuffer>d__38::System.Collections.IEnumerator.Reset()
extern void U3CLoadTargetDataBufferU3Ed__38_System_Collections_IEnumerator_Reset_m191C863FD554A6ABFCCBDDA70C1209F28B0B32D8 (void);
// 0x0000085C System.Object easyar.ImageTargetController/<LoadTargetDataBuffer>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CLoadTargetDataBufferU3Ed__38_System_Collections_IEnumerator_get_Current_mC4E576DFA43B13AFBD8812DF07ECD41A9EC5B13A (void);
// 0x0000085D System.Void easyar.ImageTargetController/<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_m13BC7F7ED4D79AFF70FE74CBE4327DE61090516E (void);
// 0x0000085E System.Void easyar.ImageTargetController/<>c__DisplayClass39_0::<UpdateTargetInTracker>b__1(easyar.Target,System.Boolean)
extern void U3CU3Ec__DisplayClass39_0_U3CUpdateTargetInTrackerU3Eb__1_m7448F405158C593AE316E4ED9CE17E6B25DCA78C (void);
// 0x0000085F easyar.ImageTracker easyar.ImageTrackerFrameFilter::get_Tracker()
extern void ImageTrackerFrameFilter_get_Tracker_m921C5DE4FA7D22775D7A0DBF9E39BB6D283E765F (void);
// 0x00000860 System.Void easyar.ImageTrackerFrameFilter::set_Tracker(easyar.ImageTracker)
extern void ImageTrackerFrameFilter_set_Tracker_m0C3EA99FAF9A0B03278CE3C9127E3FEFC7CDBDB5 (void);
// 0x00000861 System.Void easyar.ImageTrackerFrameFilter::add_TargetLoad(System.Action`3<easyar.ImageTargetController,easyar.Target,System.Boolean>)
extern void ImageTrackerFrameFilter_add_TargetLoad_mAC6919010D498254A50B080CD9E7D06BA4263FBD (void);
// 0x00000862 System.Void easyar.ImageTrackerFrameFilter::remove_TargetLoad(System.Action`3<easyar.ImageTargetController,easyar.Target,System.Boolean>)
extern void ImageTrackerFrameFilter_remove_TargetLoad_m3578D6764A9429D55F53A2702D352A6157507745 (void);
// 0x00000863 System.Void easyar.ImageTrackerFrameFilter::add_TargetUnload(System.Action`3<easyar.ImageTargetController,easyar.Target,System.Boolean>)
extern void ImageTrackerFrameFilter_add_TargetUnload_m9784EAE4580C2EA58AF655BBC2CDF523A78EA59F (void);
// 0x00000864 System.Void easyar.ImageTrackerFrameFilter::remove_TargetUnload(System.Action`3<easyar.ImageTargetController,easyar.Target,System.Boolean>)
extern void ImageTrackerFrameFilter_remove_TargetUnload_m55826DC7A7DD08B92535FE77F01F5A9D03343DC2 (void);
// 0x00000865 System.Void easyar.ImageTrackerFrameFilter::add_SimultaneousNumChanged(System.Action)
extern void ImageTrackerFrameFilter_add_SimultaneousNumChanged_mE16608F77817AA1C35B60C4C55F8CB86B88F1480 (void);
// 0x00000866 System.Void easyar.ImageTrackerFrameFilter::remove_SimultaneousNumChanged(System.Action)
extern void ImageTrackerFrameFilter_remove_SimultaneousNumChanged_m96B16C9C0818E2E2EB5916212E096CAE78BF02CA (void);
// 0x00000867 System.Int32 easyar.ImageTrackerFrameFilter::get_BufferRequirement()
extern void ImageTrackerFrameFilter_get_BufferRequirement_m43C802AFEEB0B85ACC64A6213CDE516B870521E7 (void);
// 0x00000868 System.Int32 easyar.ImageTrackerFrameFilter::get_SimultaneousNum()
extern void ImageTrackerFrameFilter_get_SimultaneousNum_m104FBA7D54E4F2908816EB921D74AE23A96D4BE4 (void);
// 0x00000869 System.Void easyar.ImageTrackerFrameFilter::set_SimultaneousNum(System.Int32)
extern void ImageTrackerFrameFilter_set_SimultaneousNum_m340F710A6FEF0CDDDF99650BD232E593A4C44085 (void);
// 0x0000086A System.Collections.Generic.List`1<easyar.TargetController> easyar.ImageTrackerFrameFilter::get_TargetControllers()
extern void ImageTrackerFrameFilter_get_TargetControllers_mC0ADCA187166B49DE745682579B96ECE6BA77BEC (void);
// 0x0000086B System.Void easyar.ImageTrackerFrameFilter::set_TargetControllers(System.Collections.Generic.List`1<easyar.TargetController>)
extern void ImageTrackerFrameFilter_set_TargetControllers_mF46C7BB3F4488795C357A23535F4DBB1AA1B49AC (void);
// 0x0000086C System.Void easyar.ImageTrackerFrameFilter::Awake()
extern void ImageTrackerFrameFilter_Awake_mBA9A9FACCE5A913D69691D0D1888FAA60666DABC (void);
// 0x0000086D System.Void easyar.ImageTrackerFrameFilter::OnEnable()
extern void ImageTrackerFrameFilter_OnEnable_m1B76FC2657BB27222BDF7AC3300636037CAF4853 (void);
// 0x0000086E System.Void easyar.ImageTrackerFrameFilter::Start()
extern void ImageTrackerFrameFilter_Start_m0BCEA9869AB4740C6245F69756D3CA6782C6D608 (void);
// 0x0000086F System.Void easyar.ImageTrackerFrameFilter::OnDisable()
extern void ImageTrackerFrameFilter_OnDisable_m27AF0890FFD39D1376CF4E4A06C3B8C2681888E8 (void);
// 0x00000870 System.Void easyar.ImageTrackerFrameFilter::OnDestroy()
extern void ImageTrackerFrameFilter_OnDestroy_m3FAC3A81B751ED8425AFF30089A41A112CAAB9EF (void);
// 0x00000871 System.Void easyar.ImageTrackerFrameFilter::LoadTarget(easyar.ImageTargetController)
extern void ImageTrackerFrameFilter_LoadTarget_mAD2712D4BDBCAF841DD58D928E48397F9747F117 (void);
// 0x00000872 System.Void easyar.ImageTrackerFrameFilter::UnloadTarget(easyar.ImageTargetController)
extern void ImageTrackerFrameFilter_UnloadTarget_m9BDD5C1F5CC2608F175B4EEA74888DEA4D679ED1 (void);
// 0x00000873 easyar.FeedbackFrameSink easyar.ImageTrackerFrameFilter::FeedbackFrameSink()
extern void ImageTrackerFrameFilter_FeedbackFrameSink_m093A1E7A28078D13C5DFDADAFB4A4A991A4F7DC9 (void);
// 0x00000874 easyar.OutputFrameSource easyar.ImageTrackerFrameFilter::OutputFrameSource()
extern void ImageTrackerFrameFilter_OutputFrameSource_m947CC6F1A92D488C2F8158941F71C1E7825E075B (void);
// 0x00000875 System.Void easyar.ImageTrackerFrameFilter::OnAssemble(easyar.ARSession)
extern void ImageTrackerFrameFilter_OnAssemble_m5A07A10C650CE77D1BFD0F0B1FA4E79165D9A3E9 (void);
// 0x00000876 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<easyar.Optional`1<easyar.TargetController>,easyar.Matrix44F>> easyar.ImageTrackerFrameFilter::OnResult(easyar.Optional`1<easyar.FrameFilterResult>)
extern void ImageTrackerFrameFilter_OnResult_m0D22C00DC6B75DEB1860E38A433809F6193B8E53 (void);
// 0x00000877 System.Void easyar.ImageTrackerFrameFilter::LoadImageTarget(easyar.ImageTargetController,System.Action`2<easyar.Target,System.Boolean>)
extern void ImageTrackerFrameFilter_LoadImageTarget_mB89085E278CF6C843A9880AE65A46F3FE06E6836 (void);
// 0x00000878 System.Void easyar.ImageTrackerFrameFilter::UnloadImageTarget(easyar.ImageTargetController,System.Action`2<easyar.Target,System.Boolean>)
extern void ImageTrackerFrameFilter_UnloadImageTarget_mA61853F37CD866227390890068ABA8B9EB6749BF (void);
// 0x00000879 System.Void easyar.ImageTrackerFrameFilter::OnHFlipChange(System.Boolean)
extern void ImageTrackerFrameFilter_OnHFlipChange_m516A6D3B38FA0FCB17FAC1F235EFFFDA8490B84C (void);
// 0x0000087A easyar.TargetController easyar.ImageTrackerFrameFilter::TryGetTargetController(System.Int32)
extern void ImageTrackerFrameFilter_TryGetTargetController_m18DDCACF469F90C32765E53749A7A505827FE13C (void);
// 0x0000087B System.Void easyar.ImageTrackerFrameFilter::.ctor()
extern void ImageTrackerFrameFilter__ctor_m925ACEB7857F04DEC5F3D656408F95DEEE3F35D1 (void);
// 0x0000087C System.Void easyar.ImageTrackerFrameFilter/<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_m82DF8184A0F6CDDAD1E2BB5B50AD7E3D2B3BC034 (void);
// 0x0000087D System.Void easyar.ImageTrackerFrameFilter/<>c__DisplayClass37_0::<LoadImageTarget>b__0(easyar.Target,System.Boolean)
extern void U3CU3Ec__DisplayClass37_0_U3CLoadImageTargetU3Eb__0_mC76E6D0697AD4535DE2B039B61ACDEB6EBFF19B1 (void);
// 0x0000087E System.Void easyar.ImageTrackerFrameFilter/<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_m814F171A1C5EDEFB38153E3F33A7573BC4DC5743 (void);
// 0x0000087F System.Void easyar.ImageTrackerFrameFilter/<>c__DisplayClass38_0::<UnloadImageTarget>b__0(easyar.Target,System.Boolean)
extern void U3CU3Ec__DisplayClass38_0_U3CUnloadImageTargetU3Eb__0_m12A42B10C360CD78CADE838AD9C35053944D18C4 (void);
// 0x00000880 easyar.ObjectTarget easyar.ObjectTargetController::get_Target()
extern void ObjectTargetController_get_Target_m6A2FD3B44FC41DA411C2E3944988452E2733321B (void);
// 0x00000881 System.Void easyar.ObjectTargetController::set_Target(easyar.ObjectTarget)
extern void ObjectTargetController_set_Target_mCE5C95E720E26090653430B9C1FBF667388A4BDC (void);
// 0x00000882 System.Void easyar.ObjectTargetController::add_TargetAvailable(System.Action)
extern void ObjectTargetController_add_TargetAvailable_mCAC4FBCFA1A94B4CF3C6B5733E78246AE971B0AD (void);
// 0x00000883 System.Void easyar.ObjectTargetController::remove_TargetAvailable(System.Action)
extern void ObjectTargetController_remove_TargetAvailable_mB4F3B4DA1CE06FC264792F198ACDF17893EDB3F7 (void);
// 0x00000884 System.Void easyar.ObjectTargetController::add_TargetLoad(System.Action`2<easyar.Target,System.Boolean>)
extern void ObjectTargetController_add_TargetLoad_m8BC5700373C419E7FD833DFE691298443DE9BBE6 (void);
// 0x00000885 System.Void easyar.ObjectTargetController::remove_TargetLoad(System.Action`2<easyar.Target,System.Boolean>)
extern void ObjectTargetController_remove_TargetLoad_mED573C2E2B49FBAE05701F23B2DFE9D8D884C54F (void);
// 0x00000886 System.Void easyar.ObjectTargetController::add_TargetUnload(System.Action`2<easyar.Target,System.Boolean>)
extern void ObjectTargetController_add_TargetUnload_m754EF6ECA76B0D74D7FFABAE46697A4F4C9BA7B7 (void);
// 0x00000887 System.Void easyar.ObjectTargetController::remove_TargetUnload(System.Action`2<easyar.Target,System.Boolean>)
extern void ObjectTargetController_remove_TargetUnload_m636650C9CA43F4DBA220C8DFE0134C1229C16907 (void);
// 0x00000888 easyar.ObjectTrackerFrameFilter easyar.ObjectTargetController::get_Tracker()
extern void ObjectTargetController_get_Tracker_mCC20560339BE0353742B6F44B5254226AE56B4A6 (void);
// 0x00000889 System.Void easyar.ObjectTargetController::set_Tracker(easyar.ObjectTrackerFrameFilter)
extern void ObjectTargetController_set_Tracker_m6EB7D38EBFBF87A55953A4545D6C18A0D6F75EE7 (void);
// 0x0000088A System.Collections.Generic.List`1<UnityEngine.Vector3> easyar.ObjectTargetController::get_BoundingBox()
extern void ObjectTargetController_get_BoundingBox_m55C9156ECBE728F0C28820D32A32000614D8ABF6 (void);
// 0x0000088B System.Void easyar.ObjectTargetController::set_BoundingBox(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void ObjectTargetController_set_BoundingBox_mA3716CAAE726F3E1A264BB99573631049A1C6084 (void);
// 0x0000088C System.Void easyar.ObjectTargetController::Start()
extern void ObjectTargetController_Start_mF7469B78923DA4EDAD6D1A6AE9E2BA3A78A665B9 (void);
// 0x0000088D System.Void easyar.ObjectTargetController::Update()
extern void ObjectTargetController_Update_mC7A8C5FA613F7F49DBEEE1D7E70CA103E5948D94 (void);
// 0x0000088E System.Void easyar.ObjectTargetController::OnDestroy()
extern void ObjectTargetController_OnDestroy_m3D387D05568CD4D1F6AF87525986E0B5C0B2BC5D (void);
// 0x0000088F System.Void easyar.ObjectTargetController::OnTracking()
extern void ObjectTargetController_OnTracking_mE8D26AE98B0A8F01BCC267E20AEC2BD48A718DE6 (void);
// 0x00000890 System.Void easyar.ObjectTargetController::LoadObjFile(easyar.ObjectTargetController/ObjFileSourceData)
extern void ObjectTargetController_LoadObjFile_mAA8AD8DA11BFCD084B0BFFD8271B931C5F59CFA4 (void);
// 0x00000891 System.Void easyar.ObjectTargetController::LoadTarget(easyar.ObjectTarget)
extern void ObjectTargetController_LoadTarget_m6E48F3AAED98969C3BA058CBAD6E5442E3EE6028 (void);
// 0x00000892 System.Collections.IEnumerator easyar.ObjectTargetController::LoadObjFileFromSource(easyar.ObjectTargetController/ObjFileSourceData)
extern void ObjectTargetController_LoadObjFileFromSource_mD45D611FC2B2F282424481E62F8CD8A9852F9DCB (void);
// 0x00000893 System.Void easyar.ObjectTargetController::UpdateTargetInTracker()
extern void ObjectTargetController_UpdateTargetInTracker_m3F506478FBAB480BC40B0ADBC386E4BFC651E4F0 (void);
// 0x00000894 System.Void easyar.ObjectTargetController::UpdateScale()
extern void ObjectTargetController_UpdateScale_m03D18290AEC681A567B28FA5116379A33BA96BCB (void);
// 0x00000895 System.Void easyar.ObjectTargetController::CheckScale()
extern void ObjectTargetController_CheckScale_m09CCDC5F37B4FC47E92943788F7C3CB2E1A9E5D4 (void);
// 0x00000896 System.Void easyar.ObjectTargetController::.ctor()
extern void ObjectTargetController__ctor_m6E766FFAF06FFBA9B70B1872FB15D120CAEB0E45 (void);
// 0x00000897 System.Void easyar.ObjectTargetController::<UpdateTargetInTracker>b__36_0(easyar.Target,System.Boolean)
extern void ObjectTargetController_U3CUpdateTargetInTrackerU3Eb__36_0_m908DC12A830F0908B6ADFC0F462C6661446B896D (void);
// 0x00000898 System.Void easyar.ObjectTargetController/ObjFileSourceData::.ctor()
extern void ObjFileSourceData__ctor_mB93AAF27E0FDC3474989B2219A4D9B453C7CFF4D (void);
// 0x00000899 System.Void easyar.ObjectTargetController/<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_m935ECE2E28A101A24DC5F7BFD1D2DF7729DF7685 (void);
// 0x0000089A System.Void easyar.ObjectTargetController/<>c__DisplayClass35_0::<LoadObjFileFromSource>b__0(easyar.Buffer)
extern void U3CU3Ec__DisplayClass35_0_U3CLoadObjFileFromSourceU3Eb__0_m7CD6FCB9448321EF83D05676032BAD03FBBE007E (void);
// 0x0000089B System.Void easyar.ObjectTargetController/<>c__DisplayClass35_1::.ctor()
extern void U3CU3Ec__DisplayClass35_1__ctor_m84F80C9CB69DF6B0B9ACA5C7963BA187D7557B18 (void);
// 0x0000089C System.Void easyar.ObjectTargetController/<>c__DisplayClass35_1::<LoadObjFileFromSource>b__1(easyar.Buffer)
extern void U3CU3Ec__DisplayClass35_1_U3CLoadObjFileFromSourceU3Eb__1_mD0E5E14CF561D3CCEB463DAA1CBC94196102859A (void);
// 0x0000089D System.Void easyar.ObjectTargetController/<LoadObjFileFromSource>d__35::.ctor(System.Int32)
extern void U3CLoadObjFileFromSourceU3Ed__35__ctor_m7A6A05D9063B632855EC5CA6BA48A96F054B8BFE (void);
// 0x0000089E System.Void easyar.ObjectTargetController/<LoadObjFileFromSource>d__35::System.IDisposable.Dispose()
extern void U3CLoadObjFileFromSourceU3Ed__35_System_IDisposable_Dispose_m5A04B92ED61DC373ACAB6C6D5078C3FCFB50E888 (void);
// 0x0000089F System.Boolean easyar.ObjectTargetController/<LoadObjFileFromSource>d__35::MoveNext()
extern void U3CLoadObjFileFromSourceU3Ed__35_MoveNext_m7B46AE24F55220A6867AA0C52912BE79144D4BAD (void);
// 0x000008A0 System.Void easyar.ObjectTargetController/<LoadObjFileFromSource>d__35::<>m__Finally1()
extern void U3CLoadObjFileFromSourceU3Ed__35_U3CU3Em__Finally1_m9711BC881ABB639C2ADCF04A52E9CE1553ED7AC1 (void);
// 0x000008A1 System.Void easyar.ObjectTargetController/<LoadObjFileFromSource>d__35::<>m__Finally2()
extern void U3CLoadObjFileFromSourceU3Ed__35_U3CU3Em__Finally2_mCC70FEDDF8F239EC933CC54C5ABB8520F1D15FD6 (void);
// 0x000008A2 System.Object easyar.ObjectTargetController/<LoadObjFileFromSource>d__35::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadObjFileFromSourceU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m88255FE97B48643ACC9CE2B55EDD4FBCC117AABC (void);
// 0x000008A3 System.Void easyar.ObjectTargetController/<LoadObjFileFromSource>d__35::System.Collections.IEnumerator.Reset()
extern void U3CLoadObjFileFromSourceU3Ed__35_System_Collections_IEnumerator_Reset_mC27E618A38368C2677E9B791ADDF7563C7000A66 (void);
// 0x000008A4 System.Object easyar.ObjectTargetController/<LoadObjFileFromSource>d__35::System.Collections.IEnumerator.get_Current()
extern void U3CLoadObjFileFromSourceU3Ed__35_System_Collections_IEnumerator_get_Current_mA8A08EB1A756E18E934D954AA573F97B8EE915C3 (void);
// 0x000008A5 System.Void easyar.ObjectTargetController/<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_m913CE0503AE024BBD7879A925E77718F9C1978C0 (void);
// 0x000008A6 System.Void easyar.ObjectTargetController/<>c__DisplayClass36_0::<UpdateTargetInTracker>b__1(easyar.Target,System.Boolean)
extern void U3CU3Ec__DisplayClass36_0_U3CUpdateTargetInTrackerU3Eb__1_m15EF9A811AF74EB7F8B5938CE8B3130201AFC3E7 (void);
// 0x000008A7 easyar.ObjectTracker easyar.ObjectTrackerFrameFilter::get_Tracker()
extern void ObjectTrackerFrameFilter_get_Tracker_mA297A04811AA9A4D809E939C83CFB45579E0B960 (void);
// 0x000008A8 System.Void easyar.ObjectTrackerFrameFilter::set_Tracker(easyar.ObjectTracker)
extern void ObjectTrackerFrameFilter_set_Tracker_m3B1E1011F4FCBE741BA781359354DB7308564A2A (void);
// 0x000008A9 System.Void easyar.ObjectTrackerFrameFilter::add_TargetLoad(System.Action`3<easyar.ObjectTargetController,easyar.Target,System.Boolean>)
extern void ObjectTrackerFrameFilter_add_TargetLoad_m873A78DB962501BC4DD7523EA6949F455BE30A24 (void);
// 0x000008AA System.Void easyar.ObjectTrackerFrameFilter::remove_TargetLoad(System.Action`3<easyar.ObjectTargetController,easyar.Target,System.Boolean>)
extern void ObjectTrackerFrameFilter_remove_TargetLoad_m82D9803297B69CF5AFEAB7AA0F12CF44AE7F52C4 (void);
// 0x000008AB System.Void easyar.ObjectTrackerFrameFilter::add_TargetUnload(System.Action`3<easyar.ObjectTargetController,easyar.Target,System.Boolean>)
extern void ObjectTrackerFrameFilter_add_TargetUnload_mA1C4BB8E00F52698BE5A5A21B5103BC342D4EE17 (void);
// 0x000008AC System.Void easyar.ObjectTrackerFrameFilter::remove_TargetUnload(System.Action`3<easyar.ObjectTargetController,easyar.Target,System.Boolean>)
extern void ObjectTrackerFrameFilter_remove_TargetUnload_m091E7E675C61AC4B2D7E4785726A5F25BA57E33D (void);
// 0x000008AD System.Void easyar.ObjectTrackerFrameFilter::add_SimultaneousNumChanged(System.Action)
extern void ObjectTrackerFrameFilter_add_SimultaneousNumChanged_m365A36A03D58A673DF10564813B2DB35C9306846 (void);
// 0x000008AE System.Void easyar.ObjectTrackerFrameFilter::remove_SimultaneousNumChanged(System.Action)
extern void ObjectTrackerFrameFilter_remove_SimultaneousNumChanged_m4571D7E0F96DCD389C730718FDDEA39C507F7971 (void);
// 0x000008AF System.Int32 easyar.ObjectTrackerFrameFilter::get_BufferRequirement()
extern void ObjectTrackerFrameFilter_get_BufferRequirement_m4498B9C4D0356B519BDC7C507E452E30FA8FE846 (void);
// 0x000008B0 System.Int32 easyar.ObjectTrackerFrameFilter::get_SimultaneousNum()
extern void ObjectTrackerFrameFilter_get_SimultaneousNum_mA8828788A98B0E51E8FE9B3B9FAFE50B7A3F0830 (void);
// 0x000008B1 System.Void easyar.ObjectTrackerFrameFilter::set_SimultaneousNum(System.Int32)
extern void ObjectTrackerFrameFilter_set_SimultaneousNum_mAC8B3B18397A774CA0956CA5BA87AC93CD21EE1F (void);
// 0x000008B2 System.Collections.Generic.List`1<easyar.TargetController> easyar.ObjectTrackerFrameFilter::get_TargetControllers()
extern void ObjectTrackerFrameFilter_get_TargetControllers_m98076F1CABEC0673C73D9DF2C46D16C413C781E6 (void);
// 0x000008B3 System.Void easyar.ObjectTrackerFrameFilter::set_TargetControllers(System.Collections.Generic.List`1<easyar.TargetController>)
extern void ObjectTrackerFrameFilter_set_TargetControllers_mD20434FDD01492103078E5A16E78EAF257CB8535 (void);
// 0x000008B4 System.Void easyar.ObjectTrackerFrameFilter::Awake()
extern void ObjectTrackerFrameFilter_Awake_m188B4C3E822EDE35D6101A5755D4E4205CCD3C23 (void);
// 0x000008B5 System.Void easyar.ObjectTrackerFrameFilter::OnEnable()
extern void ObjectTrackerFrameFilter_OnEnable_m5D9853B6FF844AA3C9F942DABD17FB5EECDBB527 (void);
// 0x000008B6 System.Void easyar.ObjectTrackerFrameFilter::Start()
extern void ObjectTrackerFrameFilter_Start_m8B49466E2555E9625392932F2E4CD0322793A36E (void);
// 0x000008B7 System.Void easyar.ObjectTrackerFrameFilter::OnDisable()
extern void ObjectTrackerFrameFilter_OnDisable_m784F3C885D7381DA26B89CD047412423D7D34015 (void);
// 0x000008B8 System.Void easyar.ObjectTrackerFrameFilter::OnDestroy()
extern void ObjectTrackerFrameFilter_OnDestroy_mF1E6ED7C0E7A5AF822FE4C7D2BBBF222A2158364 (void);
// 0x000008B9 System.Void easyar.ObjectTrackerFrameFilter::LoadTarget(easyar.ObjectTargetController)
extern void ObjectTrackerFrameFilter_LoadTarget_m5FF3F323C026DF36C23B90D4FF79F674FF94C096 (void);
// 0x000008BA System.Void easyar.ObjectTrackerFrameFilter::UnloadTarget(easyar.ObjectTargetController)
extern void ObjectTrackerFrameFilter_UnloadTarget_m68EE25FF25C7C3584859088C0E5E9A277D28935B (void);
// 0x000008BB easyar.FeedbackFrameSink easyar.ObjectTrackerFrameFilter::FeedbackFrameSink()
extern void ObjectTrackerFrameFilter_FeedbackFrameSink_m9B6A66C8F4D1FB0B4022E2410E9D33A3B5540733 (void);
// 0x000008BC easyar.OutputFrameSource easyar.ObjectTrackerFrameFilter::OutputFrameSource()
extern void ObjectTrackerFrameFilter_OutputFrameSource_m3E11417146D1480F6B43690E51D904D14AFA4FBD (void);
// 0x000008BD System.Void easyar.ObjectTrackerFrameFilter::OnAssemble(easyar.ARSession)
extern void ObjectTrackerFrameFilter_OnAssemble_mB8E0C73050D856ECF0FD265A57BD0B3C5A0879C5 (void);
// 0x000008BE System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<easyar.Optional`1<easyar.TargetController>,easyar.Matrix44F>> easyar.ObjectTrackerFrameFilter::OnResult(easyar.Optional`1<easyar.FrameFilterResult>)
extern void ObjectTrackerFrameFilter_OnResult_mFDD30FED72D2598025C8B3F00DA36EFC8E3E5B0B (void);
// 0x000008BF System.Void easyar.ObjectTrackerFrameFilter::LoadObjectTarget(easyar.ObjectTargetController,System.Action`2<easyar.Target,System.Boolean>)
extern void ObjectTrackerFrameFilter_LoadObjectTarget_m68788903472A4719107DBE18E06AD41F3B0C1A6D (void);
// 0x000008C0 System.Void easyar.ObjectTrackerFrameFilter::UnloadObjectTarget(easyar.ObjectTargetController,System.Action`2<easyar.Target,System.Boolean>)
extern void ObjectTrackerFrameFilter_UnloadObjectTarget_mF0E970C536D4B9698A51D574D1F23374453EA0B1 (void);
// 0x000008C1 System.Void easyar.ObjectTrackerFrameFilter::OnHFlipChange(System.Boolean)
extern void ObjectTrackerFrameFilter_OnHFlipChange_m0536FE26C38BBB22C14486145F7E435264FC94C2 (void);
// 0x000008C2 easyar.TargetController easyar.ObjectTrackerFrameFilter::TryGetTargetController(System.Int32)
extern void ObjectTrackerFrameFilter_TryGetTargetController_m1B4ADBC152722C759F690F539D7EF447BAEC0851 (void);
// 0x000008C3 System.Void easyar.ObjectTrackerFrameFilter::.ctor()
extern void ObjectTrackerFrameFilter__ctor_m37771611EB65154C59C574FCA21E8ABE67492C9A (void);
// 0x000008C4 System.Void easyar.ObjectTrackerFrameFilter/<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_mBEA30685FA373FDF4D82E18595112CD1E6CFF166 (void);
// 0x000008C5 System.Void easyar.ObjectTrackerFrameFilter/<>c__DisplayClass36_0::<LoadObjectTarget>b__0(easyar.Target,System.Boolean)
extern void U3CU3Ec__DisplayClass36_0_U3CLoadObjectTargetU3Eb__0_m05BA5AA826D3B7452DBAC1D3229DF49A26A4EA9E (void);
// 0x000008C6 System.Void easyar.ObjectTrackerFrameFilter/<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_mBC1C7AE887490DE87BBFE22974AB3CC25558377A (void);
// 0x000008C7 System.Void easyar.ObjectTrackerFrameFilter/<>c__DisplayClass37_0::<UnloadObjectTarget>b__0(easyar.Target,System.Boolean)
extern void U3CU3Ec__DisplayClass37_0_U3CUnloadObjectTargetU3Eb__0_m514F3237CA127E51C944ACBE64ED37552F0DE50A (void);
// 0x000008C8 System.Void easyar.AliasAttribute::.ctor()
extern void AliasAttribute__ctor_m4AECECEC85441A02B4FAB76842154B86687FB368 (void);
// 0x000008C9 System.Void easyar.RecordAttribute::.ctor()
extern void RecordAttribute__ctor_mB1E4F65426EE78E761977E7DF8232A49CC6AAC55 (void);
// 0x000008CA System.Void easyar.TaggedUnionAttribute::.ctor()
extern void TaggedUnionAttribute__ctor_m50D336CC0A29803F16677978D8227E974BBF480E (void);
// 0x000008CB System.Void easyar.TagAttribute::.ctor()
extern void TagAttribute__ctor_m44A1127B7D01D36A479963B3C559993271CDF1A4 (void);
// 0x000008CC System.Void easyar.TupleAttribute::.ctor()
extern void TupleAttribute__ctor_m810F11BFA55CAA47E845A4FA7DF9E5B23B5535B4 (void);
// 0x000008CD easyar.Optional`1<T> easyar.Optional`1::CreateNone()
// 0x000008CE easyar.Optional`1<T> easyar.Optional`1::CreateSome(T)
// 0x000008CF System.Boolean easyar.Optional`1::get_OnNone()
// 0x000008D0 System.Boolean easyar.Optional`1::get_OnSome()
// 0x000008D1 easyar.Optional`1<T> easyar.Optional`1::get_Empty()
// 0x000008D2 easyar.Optional`1<T> easyar.Optional`1::op_Implicit(T)
// 0x000008D3 T easyar.Optional`1::op_Explicit(easyar.Optional`1<T>)
// 0x000008D4 System.Boolean easyar.Optional`1::op_Equality(easyar.Optional`1<T>,easyar.Optional`1<T>)
// 0x000008D5 System.Boolean easyar.Optional`1::op_Inequality(easyar.Optional`1<T>,easyar.Optional`1<T>)
// 0x000008D6 System.Boolean easyar.Optional`1::op_Equality(System.Nullable`1<easyar.Optional`1<T>>,System.Nullable`1<easyar.Optional`1<T>>)
// 0x000008D7 System.Boolean easyar.Optional`1::op_Inequality(System.Nullable`1<easyar.Optional`1<T>>,System.Nullable`1<easyar.Optional`1<T>>)
// 0x000008D8 System.Boolean easyar.Optional`1::Equals(System.Object)
// 0x000008D9 System.Int32 easyar.Optional`1::GetHashCode()
// 0x000008DA System.Boolean easyar.Optional`1::Equals(easyar.Optional`1<T>,easyar.Optional`1<T>)
// 0x000008DB System.Boolean easyar.Optional`1::Equals(System.Nullable`1<easyar.Optional`1<T>>,System.Nullable`1<easyar.Optional`1<T>>)
// 0x000008DC T easyar.Optional`1::get_Value()
// 0x000008DD T easyar.Optional`1::ValueOrDefault(T)
// 0x000008DE System.String easyar.Optional`1::ToString()
// 0x000008DF UnityEngine.Matrix4x4 easyar.APIExtend::ToUnityMatrix(easyar.Matrix44F)
extern void APIExtend_ToUnityMatrix_m206A3ACA63D8BB98326D5DFB039F3FF9DED64550 (void);
// 0x000008E0 easyar.Vec2F easyar.APIExtend::ToEasyARVector(UnityEngine.Vector2)
extern void APIExtend_ToEasyARVector_mE2724A0CB62CD9CDEAE1046A8B79894F648985F1 (void);
// 0x000008E1 easyar.Vec3F easyar.APIExtend::ToEasyARVector(UnityEngine.Vector3)
extern void APIExtend_ToEasyARVector_m9600E8DE018615DF428475EA76401EFC81B2A242 (void);
// 0x000008E2 UnityEngine.Vector2 easyar.APIExtend::ToUnityVector(easyar.Vec2F)
extern void APIExtend_ToUnityVector_mF8D534B98643DC1F8A61AD769F2246627141661F (void);
// 0x000008E3 UnityEngine.Vector3 easyar.APIExtend::ToUnityVector(easyar.Vec3F)
extern void APIExtend_ToUnityVector_m091751C847170124879708574534A1AF98CE92CD (void);
// 0x000008E4 System.Collections.IEnumerator easyar.FileUtil::LoadFile(System.String,easyar.PathType,System.Action`1<easyar.Buffer>)
extern void FileUtil_LoadFile_m88EBB2907ACDE55485823A4C26A2B9D7BFD403AD (void);
// 0x000008E5 System.Collections.IEnumerator easyar.FileUtil::LoadFile(System.String,easyar.PathType,System.Action`1<System.Byte[]>,System.Action`1<System.String>)
extern void FileUtil_LoadFile_m4AC9BF927CBA7B25789521DD69A1D0BC5CFC5919 (void);
// 0x000008E6 System.String easyar.FileUtil::PathToUrl(System.String)
extern void FileUtil_PathToUrl_mC2490350AFE724942E8681D0F92A8D10C3B638D4 (void);
// 0x000008E7 System.Void easyar.FileUtil::ImportSampleStreamingAssets()
extern void FileUtil_ImportSampleStreamingAssets_mA3FAD1235B9E311CEECCF77902310CDDFA9025C8 (void);
// 0x000008E8 System.Void easyar.FileUtil/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m849D71F8C1DE1CDD64B657B89278D01AA11057BE (void);
// 0x000008E9 System.Void easyar.FileUtil/<>c__DisplayClass1_0::<LoadFile>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass1_0_U3CLoadFileU3Eb__0_m0B88454562B365A3172F4FABD19C6F7F24CD87F2 (void);
// 0x000008EA System.Void easyar.FileUtil/<LoadFile>d__2::.ctor(System.Int32)
extern void U3CLoadFileU3Ed__2__ctor_m7DAF075882D1195526359B33F1A62ED93E14812D (void);
// 0x000008EB System.Void easyar.FileUtil/<LoadFile>d__2::System.IDisposable.Dispose()
extern void U3CLoadFileU3Ed__2_System_IDisposable_Dispose_mCC55578A4367C5E8FD2DB846C963A4C38CABBD79 (void);
// 0x000008EC System.Boolean easyar.FileUtil/<LoadFile>d__2::MoveNext()
extern void U3CLoadFileU3Ed__2_MoveNext_m18AD8EDEBA7E999263C81C588A70FC92461D654C (void);
// 0x000008ED System.Void easyar.FileUtil/<LoadFile>d__2::<>m__Finally1()
extern void U3CLoadFileU3Ed__2_U3CU3Em__Finally1_m7902C9828A9B4EDD334836FDF9927B36B10E1D8E (void);
// 0x000008EE System.Object easyar.FileUtil/<LoadFile>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadFileU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4177F95AB6FB19CBE473039096963BB7E9635A57 (void);
// 0x000008EF System.Void easyar.FileUtil/<LoadFile>d__2::System.Collections.IEnumerator.Reset()
extern void U3CLoadFileU3Ed__2_System_Collections_IEnumerator_Reset_mA8A4FE670BD8AFE9610995CDDB1CA59D0A3C447A (void);
// 0x000008F0 System.Object easyar.FileUtil/<LoadFile>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CLoadFileU3Ed__2_System_Collections_IEnumerator_get_Current_mCBCAEC97C03DA3D9F1F58642A8A252E93C8FE366 (void);
// 0x000008F1 System.Void easyar.GUIPopup::Start()
extern void GUIPopup_Start_m45B63FE0D40EA02CB9FD1FEBA8AEFA3F6B4D78CB (void);
// 0x000008F2 System.Void easyar.GUIPopup::OnDestroy()
extern void GUIPopup_OnDestroy_mF5EB996234CB30ECBF0A4EC74CCEAC96D0BEE7C9 (void);
// 0x000008F3 System.Void easyar.GUIPopup::EnqueueMessage(System.String,System.Single)
extern void GUIPopup_EnqueueMessage_m1FF5C5310444511BAC722C9EC22C68FA62716CDE (void);
// 0x000008F4 System.Collections.IEnumerator easyar.GUIPopup::ShowMessage()
extern void GUIPopup_ShowMessage_mD81F2280B13C2F0CA60575456D4707531137D1F6 (void);
// 0x000008F5 System.Void easyar.GUIPopup::OnGUI()
extern void GUIPopup_OnGUI_m5DADAC429F40EA65BFFBFF907855934C588EA830 (void);
// 0x000008F6 System.Void easyar.GUIPopup::.ctor()
extern void GUIPopup__ctor_mFDED8A24E14871FE680DE90F9B91F54971826B97 (void);
// 0x000008F7 System.Void easyar.GUIPopup/<ShowMessage>d__8::.ctor(System.Int32)
extern void U3CShowMessageU3Ed__8__ctor_mAB6F5B533E4E6F9E8A40EBAF7760CE490A96C8C9 (void);
// 0x000008F8 System.Void easyar.GUIPopup/<ShowMessage>d__8::System.IDisposable.Dispose()
extern void U3CShowMessageU3Ed__8_System_IDisposable_Dispose_mE9E1ED495359DED79D0F13CD157EBDE403127D68 (void);
// 0x000008F9 System.Boolean easyar.GUIPopup/<ShowMessage>d__8::MoveNext()
extern void U3CShowMessageU3Ed__8_MoveNext_m3C9BB8103AD7C6D794CE6746E11FE8F84171778D (void);
// 0x000008FA System.Object easyar.GUIPopup/<ShowMessage>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowMessageU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4ACF3C5DA26FB7206A9E7D9F1716CF3DCF4C5B3B (void);
// 0x000008FB System.Void easyar.GUIPopup/<ShowMessage>d__8::System.Collections.IEnumerator.Reset()
extern void U3CShowMessageU3Ed__8_System_Collections_IEnumerator_Reset_mD67A86850AFFE89CC4F85E8D08C43BDEDE9DA34F (void);
// 0x000008FC System.Object easyar.GUIPopup/<ShowMessage>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CShowMessageU3Ed__8_System_Collections_IEnumerator_get_Current_mCA7C149643CD2D0E0987E92DD490202826DA5818 (void);
// 0x000008FD System.Void easyar.UIPopupException::.ctor(System.String,System.Single)
extern void UIPopupException__ctor_m8C5AB7EFA4151FF4961EA444C19C5865F0702967 (void);
// 0x000008FE System.Void easyar.UIPopupException::.ctor(System.String)
extern void UIPopupException__ctor_m3F4A7CC28A89A468F68069B67F2CC83D79754338 (void);
// 0x000008FF System.Void easyar.ThreadWorker::Finalize()
extern void ThreadWorker_Finalize_mBEFF3C1AB6727B72C5747A10847C86E850E49EEC (void);
// 0x00000900 System.Void easyar.ThreadWorker::Dispose()
extern void ThreadWorker_Dispose_m6860E0C54304EA3A8E95DC3B783A5822F17A2B0A (void);
// 0x00000901 System.Void easyar.ThreadWorker::Run(System.Action)
extern void ThreadWorker_Run_m669EF0D58EFDF157F92FCBCE640CA2C17BAE5AAA (void);
// 0x00000902 System.Void easyar.ThreadWorker::CreateThread()
extern void ThreadWorker_CreateThread_mBDA0B321F671AB0161E9D9E169927B9F3AD62EF0 (void);
// 0x00000903 System.Void easyar.ThreadWorker::Finish()
extern void ThreadWorker_Finish_m26C0F4D2140FC2E36CED8AC7C7CE385E9FA1377F (void);
// 0x00000904 System.Void easyar.ThreadWorker::.ctor()
extern void ThreadWorker__ctor_m8E1021EA4750802C61E540330F41FA3E933BF6FD (void);
// 0x00000905 System.Void easyar.ThreadWorker::<CreateThread>b__6_0()
extern void ThreadWorker_U3CCreateThreadU3Eb__6_0_m80AF9A8C9FD34024464AB15ADD3C185E5600DD6B (void);
// 0x00000906 System.Void easyar.TransformUtil::SetCameraPoseOnCamera(UnityEngine.Transform,easyar.WorldRootController,easyar.Matrix44F,UnityEngine.Matrix4x4,System.Boolean)
extern void TransformUtil_SetCameraPoseOnCamera_m4B320D0AA8738A3499A7190C6E3E52F8B622BD32 (void);
// 0x00000907 System.Void easyar.TransformUtil::SetCameraPoseOnWorldRoot(UnityEngine.Transform,easyar.WorldRootController,easyar.Matrix44F,UnityEngine.Matrix4x4,System.Boolean)
extern void TransformUtil_SetCameraPoseOnWorldRoot_m36BCF286E2AA7335D2CC5CA99F7B54254BAD06D6 (void);
// 0x00000908 System.Void easyar.TransformUtil::SetTargetPoseOnCamera(UnityEngine.Transform,easyar.TargetController,easyar.Matrix44F,UnityEngine.Matrix4x4,System.Boolean)
extern void TransformUtil_SetTargetPoseOnCamera_m110BF66AB55BF57E2B0AEBD9B9BBBC013E43F8EF (void);
// 0x00000909 System.Void easyar.TransformUtil::SetTargetPoseOnTarget(UnityEngine.Transform,easyar.TargetController,easyar.Matrix44F,UnityEngine.Matrix4x4,System.Boolean)
extern void TransformUtil_SetTargetPoseOnTarget_m61EB662F2A768976ABD51E32FC1B56F95EA9C61E (void);
// 0x0000090A System.Void easyar.TransformUtil::SetMatrixOnTransform(UnityEngine.Transform,UnityEngine.Matrix4x4,System.Boolean)
extern void TransformUtil_SetMatrixOnTransform_m94ED2BF7230D64F0F6F5AFA7AE32B01DB505F08F (void);
// 0x0000090B System.Void easyar.TransformUtil::SetPoseOnTransform(UnityEngine.Transform,UnityEngine.Transform,easyar.Matrix44F,UnityEngine.Matrix4x4,System.Boolean,System.Boolean,System.Boolean)
extern void TransformUtil_SetPoseOnTransform_m4616153C504051BBFA2DAE5DF9F38E3E13E71B2F (void);
// 0x0000090C System.Void easyar.EasyARVersion::.ctor()
extern void EasyARVersion__ctor_mE3A9AA4BF90D37A8D2F2D0AF490DEF78264FD4BB (void);
// 0x0000090D System.Void easyar.ARAssembly::Finalize()
extern void ARAssembly_Finalize_m4E59E92137FEF5C9A09688C59057C387E7AF29FE (void);
// 0x0000090E System.Boolean easyar.ARAssembly::get_Ready()
extern void ARAssembly_get_Ready_mE0D7D9A363183508328A72FA7C587F88859F5A95 (void);
// 0x0000090F System.Void easyar.ARAssembly::set_Ready(System.Boolean)
extern void ARAssembly_set_Ready_m4628605213B7465AA5147702C0AAA558D806DADF (void);
// 0x00000910 System.Boolean easyar.ARAssembly::get_RequireWorldCenter()
extern void ARAssembly_get_RequireWorldCenter_m0C74C6CC8937D31FFE68E706A2F279E258C4DA2F (void);
// 0x00000911 System.Void easyar.ARAssembly::set_RequireWorldCenter(System.Boolean)
extern void ARAssembly_set_RequireWorldCenter_m7C8C2237D1D06E9E9FEDCE171B6F758FEE8094D9 (void);
// 0x00000912 easyar.Optional`1<easyar.OutputFrame> easyar.ARAssembly::get_OutputFrame()
extern void ARAssembly_get_OutputFrame_m3A32D230E7F77FC062789DF06BFB82CC094F1BDE (void);
// 0x00000913 System.Int32 easyar.ARAssembly::get_ExtraBufferCapacity()
extern void ARAssembly_get_ExtraBufferCapacity_m35B81FB31E8A3A0457388122576D92CC5BDF3C47 (void);
// 0x00000914 System.Void easyar.ARAssembly::set_ExtraBufferCapacity(System.Int32)
extern void ARAssembly_set_ExtraBufferCapacity_mAFE9D14E0834C89BF192ACB5E64BEC872B25E502 (void);
// 0x00000915 easyar.IDisplay easyar.ARAssembly::get_Display()
extern void ARAssembly_get_Display_mBC26348B8CEDF967753AC29C6CF134432C45267F (void);
// 0x00000916 System.Void easyar.ARAssembly::Dispose()
extern void ARAssembly_Dispose_mE0840209F40AE98F84E433435C2A781A9E20292A (void);
// 0x00000917 System.Void easyar.ARAssembly::Assemble(easyar.ARSession)
extern void ARAssembly_Assemble_mBA4E331E478C136578F7818934FA8B38F2697D6E (void);
// 0x00000918 System.Void easyar.ARAssembly::Break()
extern void ARAssembly_Break_m73276F4521C717870F2A786E5F17ADC16E2FC5B4 (void);
// 0x00000919 System.Void easyar.ARAssembly::Pause()
extern void ARAssembly_Pause_m6662699E2BD483BAFBA4CBB62E928539B7CEE32A (void);
// 0x0000091A System.Void easyar.ARAssembly::Resume()
extern void ARAssembly_Resume_mDD31AAED79DA43547509D498F4299A026BF99836 (void);
// 0x0000091B System.Void easyar.ARAssembly::ResetBufferCapacity()
extern void ARAssembly_ResetBufferCapacity_m5D0A3FBE5FF894EC9E8FAFC6D15AFC0ACFECC053 (void);
// 0x0000091C System.Int32 easyar.ARAssembly::GetBufferRequirement()
extern void ARAssembly_GetBufferRequirement_m565D94F7788098DFEB9B60CB7912BDA4C315E229 (void);
// 0x0000091D System.Int32 easyar.ARAssembly::GetFrameFilterCount()
// 0x0000091E System.Void easyar.ARAssembly::Assemble()
extern void ARAssembly_Assemble_m03A0886758FE77CD2ED5CC31BB35C2641FCF0E89 (void);
// 0x0000091F System.Void easyar.ARAssembly::DisposeAll()
extern void ARAssembly_DisposeAll_m3D2063F105AB4054D9404E82F0B8CED238C892B0 (void);
// 0x00000920 System.Void easyar.ARAssembly::.ctor()
extern void ARAssembly__ctor_m4C7629E9D99085D2920460784ECC8A0CD02E70C1 (void);
// 0x00000921 System.Void easyar.ARSession::add_FrameChange(easyar.ARSession/FrameChangeAction)
extern void ARSession_add_FrameChange_m77548873BBFE5CEE577894326237892B05F413E0 (void);
// 0x00000922 System.Void easyar.ARSession::remove_FrameChange(easyar.ARSession/FrameChangeAction)
extern void ARSession_remove_FrameChange_mC3AEE88DD4EF22F919C173CFA09EC594ECF29F2E (void);
// 0x00000923 System.Void easyar.ARSession::add_FrameUpdate(System.Action`1<easyar.OutputFrame>)
extern void ARSession_add_FrameUpdate_m3CFB06B201EC6F834CA4ABF4F200695E4FACB08A (void);
// 0x00000924 System.Void easyar.ARSession::remove_FrameUpdate(System.Action`1<easyar.OutputFrame>)
extern void ARSession_remove_FrameUpdate_mDB1136F48109CC9DA0B48657022D986E0AC516D9 (void);
// 0x00000925 System.Void easyar.ARSession::add_WorldRootChanged(System.Action`1<easyar.WorldRootController>)
extern void ARSession_add_WorldRootChanged_m7FD7A4B2571EB42293F054A5C23127CB960D6FDE (void);
// 0x00000926 System.Void easyar.ARSession::remove_WorldRootChanged(System.Action`1<easyar.WorldRootController>)
extern void ARSession_remove_WorldRootChanged_m5AFE86652B914D379381B33B0C64DB303D8DE049 (void);
// 0x00000927 easyar.Optional`1<easyar.CameraParameters> easyar.ARSession::get_FrameCameraParameters()
extern void ARSession_get_FrameCameraParameters_m894DA7ABA5970C3B7EEE05BEA3FE8598C3D276B5 (void);
// 0x00000928 System.Void easyar.ARSession::set_FrameCameraParameters(easyar.Optional`1<easyar.CameraParameters>)
extern void ARSession_set_FrameCameraParameters_mEA8C1C870A72E8AD0C7C1B6845FE332624923178 (void);
// 0x00000929 System.Void easyar.ARSession::Start()
extern void ARSession_Start_m89C7E9E648DA5641C34299E9E43A84211B2FB0AB (void);
// 0x0000092A System.Void easyar.ARSession::Update()
extern void ARSession_Update_mB721CDA43C5CB02392EB29628A986CC0223A067C (void);
// 0x0000092B System.Void easyar.ARSession::OnDestroy()
extern void ARSession_OnDestroy_m3C2B80B1956D6584AA68D4BEC48A0029806859B8 (void);
// 0x0000092C easyar.Optional`1<UnityEngine.Vector2> easyar.ARSession::ImageCoordinatesFromScreenCoordinates(UnityEngine.Vector2)
extern void ARSession_ImageCoordinatesFromScreenCoordinates_m19D24287D63781FD9389B06367F276125B8355F6 (void);
// 0x0000092D System.Void easyar.ARSession::OnFrameUpdate(easyar.OutputFrame,easyar.InputFrame,UnityEngine.Matrix4x4)
extern void ARSession_OnFrameUpdate_m8B02AE6174BD04F44EED93B50F1CEC02B7217812 (void);
// 0x0000092E System.Void easyar.ARSession::OnEmptyFrame()
extern void ARSession_OnEmptyFrame_m394392E85E00A5B8D471015928FE1B809961CDD3 (void);
// 0x0000092F System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<easyar.Optional`1<easyar.TargetController>,easyar.Matrix44F>> easyar.ARSession::DispatchResults(easyar.Optional`1<System.Collections.Generic.List`1<easyar.Optional`1<easyar.FrameFilterResult>>>,easyar.Optional`1<easyar.MotionTrackingStatus>)
extern void ARSession_DispatchResults_m5AD75B825155E77A5ED57B1F023760642964C420 (void);
// 0x00000930 UnityEngine.Matrix4x4 easyar.ARSession::GetCompensation(easyar.CameraParameters)
extern void ARSession_GetCompensation_m2DE268AE4B2D196DA4EBAFE647BD9B3BDEF96C82 (void);
// 0x00000931 System.Void easyar.ARSession::.ctor()
extern void ARSession__ctor_m781F584CFE3BC73219BEA343D9E5D288B6636DEF (void);
// 0x00000932 System.Void easyar.ARSession/FrameChangeAction::.ctor(System.Object,System.IntPtr)
extern void FrameChangeAction__ctor_m3893ED7641A1E985C2C14E001E1604E2C2360DCD (void);
// 0x00000933 System.Void easyar.ARSession/FrameChangeAction::Invoke(easyar.OutputFrame,UnityEngine.Matrix4x4)
extern void FrameChangeAction_Invoke_m88B7FD21D43F0071691C8D75CBF1999C80747F10 (void);
// 0x00000934 System.IAsyncResult easyar.ARSession/FrameChangeAction::BeginInvoke(easyar.OutputFrame,UnityEngine.Matrix4x4,System.AsyncCallback,System.Object)
extern void FrameChangeAction_BeginInvoke_mC122423CDA618E1D7BABEFCF6DA04D1F421EC533 (void);
// 0x00000935 System.Void easyar.ARSession/FrameChangeAction::EndInvoke(System.IAsyncResult)
extern void FrameChangeAction_EndInvoke_m6D6C0408DD895B92005EE8AC38FF14936606CFDC (void);
// 0x00000936 System.Int32 easyar.FrameFilter::get_BufferRequirement()
// 0x00000937 System.Void easyar.FrameFilter::OnAssemble(easyar.ARSession)
extern void FrameFilter_OnAssemble_mD9A848689E8E4CAF441B3BD366424D4B968C929B (void);
// 0x00000938 System.Void easyar.FrameFilter::SetHFlip(System.Boolean)
extern void FrameFilter_SetHFlip_m0A7568E1EE6E8F1006907D7B18A2E6B886D4AB4D (void);
// 0x00000939 System.Void easyar.FrameFilter::OnHFlipChange(System.Boolean)
extern void FrameFilter_OnHFlipChange_m59055D8C12A22CDE1275F7E0F4D6A7B886379559 (void);
// 0x0000093A System.Void easyar.FrameFilter::.ctor()
extern void FrameFilter__ctor_mD5A76B3475BAB035EF935F13EC901CCA8DC6DFA6 (void);
// 0x0000093B easyar.FeedbackFrameSink easyar.FrameFilter/IFeedbackFrameSink::FeedbackFrameSink()
// 0x0000093C easyar.InputFrameSink easyar.FrameFilter/IInputFrameSink::InputFrameSink()
// 0x0000093D System.Void easyar.FrameFilter/IInputFrameSinkDelayConnect::ConnectedTo(easyar.InputFrameSource,System.Action)
// 0x0000093E easyar.OutputFrameSource easyar.FrameFilter/IOutputFrameSource::OutputFrameSource()
// 0x0000093F System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<easyar.Optional`1<easyar.TargetController>,easyar.Matrix44F>> easyar.FrameFilter/IOutputFrameSource::OnResult(easyar.Optional`1<easyar.FrameFilterResult>)
// 0x00000940 easyar.MotionTrackingStatus easyar.FrameFilter/ISpatialInformationSink::get_TrackingStatus()
// 0x00000941 System.Void easyar.FrameFilter/ISpatialInformationSink::OnTracking(easyar.MotionTrackingStatus)
// 0x00000942 System.Boolean easyar.FramePlayer::get_IsCompleted()
extern void FramePlayer_get_IsCompleted_mD323DE4203F437E19C3D052737EC829A2E62831E (void);
// 0x00000943 System.Single easyar.FramePlayer::get_Length()
extern void FramePlayer_get_Length_mBD1475F362608C9215478EEFA874C4A56D36CE2E (void);
// 0x00000944 System.Single easyar.FramePlayer::get_Time()
extern void FramePlayer_get_Time_mABE46BAD8804FFF9D9AC3EEC454D78179B9FAF30 (void);
// 0x00000945 System.Boolean easyar.FramePlayer::get_HasSpatialInformation()
extern void FramePlayer_get_HasSpatialInformation_mA13BBDA98504BF1AF1FA866459AA0BE1B9BA2FC8 (void);
// 0x00000946 easyar.IDisplay easyar.FramePlayer::get_Display()
extern void FramePlayer_get_Display_mC71E926B807071F89F2EF0CFD1928EA35D0DD3DD (void);
// 0x00000947 System.Void easyar.FramePlayer::Awake()
extern void FramePlayer_Awake_mEB16B8D9DB0D86B92A5D2E5F12846E5D5C5AB705 (void);
// 0x00000948 System.Void easyar.FramePlayer::OnEnable()
extern void FramePlayer_OnEnable_m112DA05A2E50A1E6902E7E9FC3C564945EB4D54C (void);
// 0x00000949 System.Void easyar.FramePlayer::Start()
extern void FramePlayer_Start_m41F9FD32489F3705E496F939CB806226A07EBE7B (void);
// 0x0000094A System.Void easyar.FramePlayer::OnDisable()
extern void FramePlayer_OnDisable_m9B5FAD0E9698C07CB612809DE5F61FD66128107A (void);
// 0x0000094B System.Void easyar.FramePlayer::OnDestroy()
extern void FramePlayer_OnDestroy_m3E455EE322BBD3451BA5AC15C907AF12091AB139 (void);
// 0x0000094C System.Boolean easyar.FramePlayer::Play()
extern void FramePlayer_Play_m8D693EE1E71F39400C5BB1BB28B1650C764A8E49 (void);
// 0x0000094D System.Void easyar.FramePlayer::Stop()
extern void FramePlayer_Stop_mAE3B5E2358CFF4751FFB95D469F49569F9D357E4 (void);
// 0x0000094E System.Void easyar.FramePlayer::Pause()
extern void FramePlayer_Pause_m95E8A405F632DA7B08A79FF62FDF370D8C4CC689 (void);
// 0x0000094F System.Void easyar.FramePlayer::Connect(easyar.InputFrameSink)
extern void FramePlayer_Connect_m972AD0B00402DC4D23FBF0BD7D1073F7A43CC701 (void);
// 0x00000950 System.Void easyar.FramePlayer::.ctor()
extern void FramePlayer__ctor_m4D38929BECC90C9D3BEF4E25931C543164DDB37F (void);
// 0x00000951 System.Int32 easyar.FrameRecorder::get_BufferRequirement()
extern void FrameRecorder_get_BufferRequirement_m4CF02FEA5B5260729441053A03387AE18EE963F6 (void);
// 0x00000952 System.Void easyar.FrameRecorder::Awake()
extern void FrameRecorder_Awake_mF7F2CEFDD245D9BE0CB4A2308210F29165E2CA47 (void);
// 0x00000953 System.Void easyar.FrameRecorder::OnEnable()
extern void FrameRecorder_OnEnable_m8A358D185F40B930618167B68346882D1769C0CD (void);
// 0x00000954 System.Void easyar.FrameRecorder::Start()
extern void FrameRecorder_Start_mCC7D9595A90A4452BF11B3F303442A441E24F9FC (void);
// 0x00000955 System.Void easyar.FrameRecorder::OnDisable()
extern void FrameRecorder_OnDisable_m0FC86AFB6D4375B2B6BED75EE5D0815547A40F9C (void);
// 0x00000956 System.Void easyar.FrameRecorder::OnDestroy()
extern void FrameRecorder_OnDestroy_m384E2CAB3D791D1BEBEA509607F0D6F96522419F (void);
// 0x00000957 System.Void easyar.FrameRecorder::OnAssemble(easyar.ARSession)
extern void FrameRecorder_OnAssemble_mE5ACB1466ED9768D130AD3A9ED5922E1688A90E8 (void);
// 0x00000958 easyar.InputFrameSource easyar.FrameRecorder::Output()
extern void FrameRecorder_Output_m5A55122086217EB4FF1540367F93A28037136B95 (void);
// 0x00000959 easyar.InputFrameSink easyar.FrameRecorder::Input()
extern void FrameRecorder_Input_mC803B50D44D0770B6DC271870E12FF085511141F (void);
// 0x0000095A System.Void easyar.FrameRecorder::.ctor()
extern void FrameRecorder__ctor_m1DF3BA697C5948E1E288372749E2D375BFF040A8 (void);
// 0x0000095B System.Boolean easyar.FrameSource::get_HasSpatialInformation()
// 0x0000095C System.Boolean easyar.FrameSource::get_IsHMD()
extern void FrameSource_get_IsHMD_mE8BB50ADAFF835C5B04C582C205212C7DDB260D4 (void);
// 0x0000095D System.Void easyar.FrameSource::OnEnable()
extern void FrameSource_OnEnable_m7A12C3BF130A450A12F09E08F7A1FB108855FFF2 (void);
// 0x0000095E System.Void easyar.FrameSource::OnDisable()
extern void FrameSource_OnDisable_mBA86309F77ADC1C11D8A34B7DF8E9C9DE0B20AFC (void);
// 0x0000095F System.Void easyar.FrameSource::Connect(easyar.InputFrameSink)
extern void FrameSource_Connect_m0E873AAE4D086BEA318B5201F01705CD68CCFB31 (void);
// 0x00000960 System.Void easyar.FrameSource::OnAssemble(easyar.ARSession)
extern void FrameSource_OnAssemble_m2E417FE61B5B4F8AFD74F97B8B24F7236241DB3C (void);
// 0x00000961 System.Void easyar.FrameSource::.ctor()
extern void FrameSource__ctor_m6E6038D6F73663C7F145AA71A58AC2D0BE97363F (void);
// 0x00000962 System.Boolean easyar.TargetController::get_IsTracked()
extern void TargetController_get_IsTracked_mCDCD168D5AE7E4FBB89191F60A086BB2159C02B9 (void);
// 0x00000963 System.Void easyar.TargetController::set_IsTracked(System.Boolean)
extern void TargetController_set_IsTracked_mEB0AC36E8C682FA8B2D00B02E65CAE450F40741F (void);
// 0x00000964 System.Boolean easyar.TargetController::get_IsLoaded()
extern void TargetController_get_IsLoaded_m74BD4BCDAC8BDA2A60482DB96BEC34F944E38D51 (void);
// 0x00000965 System.Void easyar.TargetController::set_IsLoaded(System.Boolean)
extern void TargetController_set_IsLoaded_m7C2707C8D99CAAFC51A250614A4790FD43D99472 (void);
// 0x00000966 System.Void easyar.TargetController::Start()
extern void TargetController_Start_m8A33886CADFA264C59035E4BC9ADC101B58FEE2C (void);
// 0x00000967 System.Void easyar.TargetController::OnTracking(System.Boolean)
extern void TargetController_OnTracking_mF774DB9A49FD1A188D3AFE69BCCC65C623F4AE52 (void);
// 0x00000968 System.Void easyar.TargetController::OnTracking()
// 0x00000969 System.Void easyar.TargetController::.ctor()
extern void TargetController__ctor_m94341ECEBEE9443387A6BCC7FB1E9F2AA13A083B (void);
// 0x0000096A System.Void easyar.WorldRootController::add_TrackingStatusChanged(System.Action`1<easyar.MotionTrackingStatus>)
extern void WorldRootController_add_TrackingStatusChanged_mD2CE170147C8A715C42BBD4EF9951AB7C9977A51 (void);
// 0x0000096B System.Void easyar.WorldRootController::remove_TrackingStatusChanged(System.Action`1<easyar.MotionTrackingStatus>)
extern void WorldRootController_remove_TrackingStatusChanged_m0FF0A489070271B1B9B65CB749D396FE388D7508 (void);
// 0x0000096C easyar.MotionTrackingStatus easyar.WorldRootController::get_TrackingStatus()
extern void WorldRootController_get_TrackingStatus_m93D0D79F1E994459E25C2692A96D498D1599EB5E (void);
// 0x0000096D System.Void easyar.WorldRootController::set_TrackingStatus(easyar.MotionTrackingStatus)
extern void WorldRootController_set_TrackingStatus_m1573ED629C5CAA28C2907D85CE636576BBF05968 (void);
// 0x0000096E System.Void easyar.WorldRootController::Start()
extern void WorldRootController_Start_m7B383CD7B63F42CB1687EDF04532A7E7710ECD66 (void);
// 0x0000096F System.Void easyar.WorldRootController::OnTracking(easyar.MotionTrackingStatus)
extern void WorldRootController_OnTracking_m9B218A028C9082D84A377F24F0F410DD02B6B1BE (void);
// 0x00000970 System.Void easyar.WorldRootController::.ctor()
extern void WorldRootController__ctor_mE8D1F5FEDB84931D35D2E5C92BF4C374F53E1505 (void);
// 0x00000971 easyar.BlockInfo easyar.DenseSpatialMapBlockController::get_Info()
extern void DenseSpatialMapBlockController_get_Info_m9B5FAAE3691BEA4AAA516AF8BC75A971D265267F (void);
// 0x00000972 System.Void easyar.DenseSpatialMapBlockController::set_Info(easyar.BlockInfo)
extern void DenseSpatialMapBlockController_set_Info_m13F0699BCA49F39EB168F49AE2199DD7CC7ABFFC (void);
// 0x00000973 System.Void easyar.DenseSpatialMapBlockController::Awake()
extern void DenseSpatialMapBlockController_Awake_m54155F58835F151B71F86BA20E51246A2DBCB90B (void);
// 0x00000974 System.Void easyar.DenseSpatialMapBlockController::OnDestroy()
extern void DenseSpatialMapBlockController_OnDestroy_mE6F8A03B9653202DB9C80A8CF0BCCEB280D0459F (void);
// 0x00000975 System.Void easyar.DenseSpatialMapBlockController::UpdateData(easyar.BlockInfo,easyar.SceneMesh)
extern void DenseSpatialMapBlockController_UpdateData_mD69B5EC20C0696EF9E1EFE19D7A4303B1ECB3260 (void);
// 0x00000976 System.Void easyar.DenseSpatialMapBlockController::UpdateMesh()
extern void DenseSpatialMapBlockController_UpdateMesh_m362AE28E9099A6783A11CD7EE325759FA9BED1EF (void);
// 0x00000977 System.Void easyar.DenseSpatialMapBlockController::CopyMeshData(easyar.SceneMesh)
extern void DenseSpatialMapBlockController_CopyMeshData_mCA2471B7D717A385962B8DFDFE45AD688189A75A (void);
// 0x00000978 System.Void easyar.DenseSpatialMapBlockController::.ctor()
extern void DenseSpatialMapBlockController__ctor_mD187E02BBA42E11DF1488C6083E79B0F5EDBA9DB (void);
// 0x00000979 easyar.DenseSpatialMap easyar.DenseSpatialMapBuilderFrameFilter::get_Builder()
extern void DenseSpatialMapBuilderFrameFilter_get_Builder_m758140881A3D5EB2FDD98AEF2DBE5412150F6ABD (void);
// 0x0000097A System.Void easyar.DenseSpatialMapBuilderFrameFilter::set_Builder(easyar.DenseSpatialMap)
extern void DenseSpatialMapBuilderFrameFilter_set_Builder_m737FCF073162DE6634880B16969D488F26D0A7C6 (void);
// 0x0000097B System.Void easyar.DenseSpatialMapBuilderFrameFilter::add_MapCreate(System.Action`1<easyar.DenseSpatialMapBlockController>)
extern void DenseSpatialMapBuilderFrameFilter_add_MapCreate_m50792A04FFC1A0E331C87EBAD4429A612F55392D (void);
// 0x0000097C System.Void easyar.DenseSpatialMapBuilderFrameFilter::remove_MapCreate(System.Action`1<easyar.DenseSpatialMapBlockController>)
extern void DenseSpatialMapBuilderFrameFilter_remove_MapCreate_m672731B5112CA728EAC9F8C238D9B1BEEB4C13FB (void);
// 0x0000097D System.Void easyar.DenseSpatialMapBuilderFrameFilter::add_MapUpdate(System.Action`1<System.Collections.Generic.List`1<easyar.DenseSpatialMapBlockController>>)
extern void DenseSpatialMapBuilderFrameFilter_add_MapUpdate_m2DED4A7DD8A17E497F480B12B1F438CDE6099BBC (void);
// 0x0000097E System.Void easyar.DenseSpatialMapBuilderFrameFilter::remove_MapUpdate(System.Action`1<System.Collections.Generic.List`1<easyar.DenseSpatialMapBlockController>>)
extern void DenseSpatialMapBuilderFrameFilter_remove_MapUpdate_m210F0D63D0E6A2AC2A3B88A55296E30729EEDA18 (void);
// 0x0000097F System.Int32 easyar.DenseSpatialMapBuilderFrameFilter::get_BufferRequirement()
extern void DenseSpatialMapBuilderFrameFilter_get_BufferRequirement_m314B7BFE417C2434BFD3C4695775D8E198DCF251 (void);
// 0x00000980 easyar.MotionTrackingStatus easyar.DenseSpatialMapBuilderFrameFilter::get_TrackingStatus()
extern void DenseSpatialMapBuilderFrameFilter_get_TrackingStatus_m3E17B76E936462D507DD2C5DDC6F6EA56BF48F03 (void);
// 0x00000981 System.Void easyar.DenseSpatialMapBuilderFrameFilter::set_TrackingStatus(easyar.MotionTrackingStatus)
extern void DenseSpatialMapBuilderFrameFilter_set_TrackingStatus_m6FB6FEC5F485ECC8E81867E643274C679E6FB8E1 (void);
// 0x00000982 System.Boolean easyar.DenseSpatialMapBuilderFrameFilter::get_RenderMesh()
extern void DenseSpatialMapBuilderFrameFilter_get_RenderMesh_m4457DCC80C7C4912AB9DBD853F7C4CC9241AF894 (void);
// 0x00000983 System.Void easyar.DenseSpatialMapBuilderFrameFilter::set_RenderMesh(System.Boolean)
extern void DenseSpatialMapBuilderFrameFilter_set_RenderMesh_m4872E7CB0A65DF3D1C1AC0870160E61E94DB99A5 (void);
// 0x00000984 UnityEngine.Color easyar.DenseSpatialMapBuilderFrameFilter::get_MeshColor()
extern void DenseSpatialMapBuilderFrameFilter_get_MeshColor_m6A366E4F415167C1716E553BDF40525E88483C77 (void);
// 0x00000985 System.Void easyar.DenseSpatialMapBuilderFrameFilter::set_MeshColor(UnityEngine.Color)
extern void DenseSpatialMapBuilderFrameFilter_set_MeshColor_m625234A459EFEFAE7ED2CF12F2244401BB5CC404 (void);
// 0x00000986 System.Collections.Generic.List`1<easyar.DenseSpatialMapBlockController> easyar.DenseSpatialMapBuilderFrameFilter::get_MeshBlocks()
extern void DenseSpatialMapBuilderFrameFilter_get_MeshBlocks_mDD92DBC2AD9F3404C0B37AF4FEBDA82E3C6FA622 (void);
// 0x00000987 System.Void easyar.DenseSpatialMapBuilderFrameFilter::Awake()
extern void DenseSpatialMapBuilderFrameFilter_Awake_mA981315171723F0990D547B6934AA7CC4E029CC8 (void);
// 0x00000988 System.Void easyar.DenseSpatialMapBuilderFrameFilter::OnEnable()
extern void DenseSpatialMapBuilderFrameFilter_OnEnable_m2D80CD2DE5067F0D30BC1ACF2F13EB1B11F746C2 (void);
// 0x00000989 System.Void easyar.DenseSpatialMapBuilderFrameFilter::Start()
extern void DenseSpatialMapBuilderFrameFilter_Start_mE36AAD3A756729CD440A5E55DA92BDBEC4C991DF (void);
// 0x0000098A System.Void easyar.DenseSpatialMapBuilderFrameFilter::Update()
extern void DenseSpatialMapBuilderFrameFilter_Update_m532A4E73A23BE40BC3F756F286EC6E55C1D56E0B (void);
// 0x0000098B System.Void easyar.DenseSpatialMapBuilderFrameFilter::OnDisable()
extern void DenseSpatialMapBuilderFrameFilter_OnDisable_m77913A7DEA1D62AFBE1D354F0F8D047E625F6469 (void);
// 0x0000098C System.Void easyar.DenseSpatialMapBuilderFrameFilter::OnDestroy()
extern void DenseSpatialMapBuilderFrameFilter_OnDestroy_m3D0C342D5AA66680219616DDA693B1C58B024625 (void);
// 0x0000098D easyar.InputFrameSink easyar.DenseSpatialMapBuilderFrameFilter::InputFrameSink()
extern void DenseSpatialMapBuilderFrameFilter_InputFrameSink_m87AAF6A72DD8C272A5C1E35F35780BBE0A654637 (void);
// 0x0000098E System.Void easyar.DenseSpatialMapBuilderFrameFilter::OnTracking(easyar.MotionTrackingStatus)
extern void DenseSpatialMapBuilderFrameFilter_OnTracking_mE134B8B290413E5294D80342E054E4EB9AE3542B (void);
// 0x0000098F System.Void easyar.DenseSpatialMapBuilderFrameFilter::OnAssemble(easyar.ARSession)
extern void DenseSpatialMapBuilderFrameFilter_OnAssemble_m218526BA10CDFB35BA8B954E4804356EF7926C1B (void);
// 0x00000990 System.Void easyar.DenseSpatialMapBuilderFrameFilter::.ctor()
extern void DenseSpatialMapBuilderFrameFilter__ctor_mEE0F6D254AC8124935E87875DEDBE4142F20CA45 (void);
// 0x00000991 System.Void easyar.DenseSpatialMapBuilderFrameFilter::<OnAssemble>b__41_0(easyar.WorldRootController)
extern void DenseSpatialMapBuilderFrameFilter_U3COnAssembleU3Eb__41_0_mB7091E6C9A2299026A8DBA9B6790D69D225AEB91 (void);
// 0x00000992 UnityEngine.Camera easyar.DenseSpatialMapDepthRenderer::get_RenderDepthCamera()
extern void DenseSpatialMapDepthRenderer_get_RenderDepthCamera_m681A99424571E53FC47EE5FA06DA4662882FC1F2 (void);
// 0x00000993 System.Void easyar.DenseSpatialMapDepthRenderer::set_RenderDepthCamera(UnityEngine.Camera)
extern void DenseSpatialMapDepthRenderer_set_RenderDepthCamera_m492EF4A696EF204AD15D3E94CF170B8E9ABD6F45 (void);
// 0x00000994 UnityEngine.Material easyar.DenseSpatialMapDepthRenderer::get_MapMeshMaterial()
extern void DenseSpatialMapDepthRenderer_get_MapMeshMaterial_m0BBD1D7CD30168722CCD1758403766320276DF87 (void);
// 0x00000995 System.Void easyar.DenseSpatialMapDepthRenderer::set_MapMeshMaterial(UnityEngine.Material)
extern void DenseSpatialMapDepthRenderer_set_MapMeshMaterial_mC72C0357A3454D90FE3AD3493D0ADE32C158EF36 (void);
// 0x00000996 System.Void easyar.DenseSpatialMapDepthRenderer::LateUpdate()
extern void DenseSpatialMapDepthRenderer_LateUpdate_mFF013734FA3C55BDFA17E066762ADDBE24C0D161 (void);
// 0x00000997 System.Void easyar.DenseSpatialMapDepthRenderer::OnDestroy()
extern void DenseSpatialMapDepthRenderer_OnDestroy_m3178E1B3E24B2BB94325319DFCEB1A670E9B0249 (void);
// 0x00000998 System.Void easyar.DenseSpatialMapDepthRenderer::.ctor()
extern void DenseSpatialMapDepthRenderer__ctor_m2DE033ADB30C69621A1C40F761CECEDC2584EFC5 (void);
// 0x00000999 easyar.SparseSpatialMapController/SparseSpatialMapInfo easyar.SparseSpatialMapController::get_MapInfo()
extern void SparseSpatialMapController_get_MapInfo_m9F7973AF582AB3D534F5A78C60A5B9EA4FF77A09 (void);
// 0x0000099A System.Void easyar.SparseSpatialMapController::set_MapInfo(easyar.SparseSpatialMapController/SparseSpatialMapInfo)
extern void SparseSpatialMapController_set_MapInfo_m9A2E132F01CDA34B585D1393BDB53914392E6148 (void);
// 0x0000099B System.Void easyar.SparseSpatialMapController::add_MapInfoAvailable(System.Action)
extern void SparseSpatialMapController_add_MapInfoAvailable_m2862A31F3E802A743EB2EFD089685CAAEE2ABD56 (void);
// 0x0000099C System.Void easyar.SparseSpatialMapController::remove_MapInfoAvailable(System.Action)
extern void SparseSpatialMapController_remove_MapInfoAvailable_mE56EBAB6010C146C210663CDF819CD10602D897F (void);
// 0x0000099D System.Void easyar.SparseSpatialMapController::add_MapLocalized(System.Action)
extern void SparseSpatialMapController_add_MapLocalized_m1E916CEF68A96614F5550D3ED1E13B5BAE9F64B3 (void);
// 0x0000099E System.Void easyar.SparseSpatialMapController::remove_MapLocalized(System.Action)
extern void SparseSpatialMapController_remove_MapLocalized_m9B100A0D840C4240F4EE3D07078C36B28E4CC18C (void);
// 0x0000099F System.Void easyar.SparseSpatialMapController::add_MapStopLocalize(System.Action)
extern void SparseSpatialMapController_add_MapStopLocalize_m47AD420BFD0385FEEFC65E3FE60316C2A3BA65F6 (void);
// 0x000009A0 System.Void easyar.SparseSpatialMapController::remove_MapStopLocalize(System.Action)
extern void SparseSpatialMapController_remove_MapStopLocalize_mA18DA2D96E3D564B641366E2D1878197A572F88C (void);
// 0x000009A1 System.Void easyar.SparseSpatialMapController::add_MapLoad(System.Action`3<easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapController_add_MapLoad_m1EAD1D28F05893F5F24E830D895C07650119676E (void);
// 0x000009A2 System.Void easyar.SparseSpatialMapController::remove_MapLoad(System.Action`3<easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapController_remove_MapLoad_m948B3F5628A733F6ED2E96668F4FFCFACD494333 (void);
// 0x000009A3 System.Void easyar.SparseSpatialMapController::add_MapUnload(System.Action`3<easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapController_add_MapUnload_m2FB811A896D21DDF1CE0365F4672BF245B99F69F (void);
// 0x000009A4 System.Void easyar.SparseSpatialMapController::remove_MapUnload(System.Action`3<easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapController_remove_MapUnload_m662469C6CE6B9C1ADCE099947BF8FB0AAAE3883B (void);
// 0x000009A5 System.Void easyar.SparseSpatialMapController::add_MapHost(System.Action`3<easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapController_add_MapHost_mDD77B7155B17BBB8F9A4737B6C8F03CE408208BE (void);
// 0x000009A6 System.Void easyar.SparseSpatialMapController::remove_MapHost(System.Action`3<easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapController_remove_MapHost_m053C92797E2D6D610C67E10ECB87A00ADC7459B2 (void);
// 0x000009A7 easyar.SparseSpatialMapWorkerFrameFilter easyar.SparseSpatialMapController::get_MapWorker()
extern void SparseSpatialMapController_get_MapWorker_mB62DF8B969EC1EFFF172B53578B16CD6501A37D0 (void);
// 0x000009A8 System.Void easyar.SparseSpatialMapController::set_MapWorker(easyar.SparseSpatialMapWorkerFrameFilter)
extern void SparseSpatialMapController_set_MapWorker_m0335162EA6291F30BE50023CBFFA82E0BB41F57F (void);
// 0x000009A9 easyar.SparseSpatialMapController/ParticleParameter easyar.SparseSpatialMapController::get_PointCloudParticleParameter()
extern void SparseSpatialMapController_get_PointCloudParticleParameter_m669D5277F4CD0CE6847FAD89CB6BFC7B5931982F (void);
// 0x000009AA System.Void easyar.SparseSpatialMapController::set_PointCloudParticleParameter(easyar.SparseSpatialMapController/ParticleParameter)
extern void SparseSpatialMapController_set_PointCloudParticleParameter_m546461FB2170E19D1C80C4D81E86AB10041E7F40 (void);
// 0x000009AB System.Collections.Generic.List`1<UnityEngine.Vector3> easyar.SparseSpatialMapController::get_PointCloud()
extern void SparseSpatialMapController_get_PointCloud_m9E47A3C47C14727138FFB5EF22CFB19EED8DDC83 (void);
// 0x000009AC System.Void easyar.SparseSpatialMapController::set_PointCloud(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void SparseSpatialMapController_set_PointCloud_m5741878569506D3FF8A9541478C658577DA831EF (void);
// 0x000009AD System.Boolean easyar.SparseSpatialMapController::get_ShowPointCloud()
extern void SparseSpatialMapController_get_ShowPointCloud_mB5F6D574CBE38ADC064D67BB2A3867AFEFC7B21D (void);
// 0x000009AE System.Void easyar.SparseSpatialMapController::set_ShowPointCloud(System.Boolean)
extern void SparseSpatialMapController_set_ShowPointCloud_m5D28774E6B24850387C5986D315509A61AEA1BD5 (void);
// 0x000009AF System.Boolean easyar.SparseSpatialMapController::get_IsLocalizing()
extern void SparseSpatialMapController_get_IsLocalizing_m18ECA11E492F62E020E84D70D19E52FAA5418A14 (void);
// 0x000009B0 System.Void easyar.SparseSpatialMapController::set_IsLocalizing(System.Boolean)
extern void SparseSpatialMapController_set_IsLocalizing_m3953314E7CC78205D195598D224B1FA8C7F0885E (void);
// 0x000009B1 System.Void easyar.SparseSpatialMapController::Awake()
extern void SparseSpatialMapController_Awake_m9ED6E1D46820FE5A085F20411262693A30EE5941 (void);
// 0x000009B2 System.Void easyar.SparseSpatialMapController::Start()
extern void SparseSpatialMapController_Start_mE9783C356927904369A8C7B09974F54A0B5A5A60 (void);
// 0x000009B3 System.Void easyar.SparseSpatialMapController::OnDestroy()
extern void SparseSpatialMapController_OnDestroy_m1170B19319F3589E968540CFE91CA1A295F68371 (void);
// 0x000009B4 System.Collections.Generic.List`1<UnityEngine.Vector3> easyar.SparseSpatialMapController::HitTest(UnityEngine.Vector2)
extern void SparseSpatialMapController_HitTest_m5C0FB7B9A56193CFB2B79F0117F4B0C892AD5D6E (void);
// 0x000009B5 System.Void easyar.SparseSpatialMapController::Host(System.String,easyar.Optional`1<easyar.Image>)
extern void SparseSpatialMapController_Host_m60B0F3963441A28AFD59A9D43FD96E544F0903F3 (void);
// 0x000009B6 System.Void easyar.SparseSpatialMapController::OnLocalization(System.Boolean)
extern void SparseSpatialMapController_OnLocalization_m1913FDDB9CA77D2CB3A57A2540991873E6BEDB1C (void);
// 0x000009B7 System.Void easyar.SparseSpatialMapController::UpdatePointCloud(easyar.Buffer)
extern void SparseSpatialMapController_UpdatePointCloud_m848BCD707665213E68F268789CD6D17B6687D868 (void);
// 0x000009B8 System.Void easyar.SparseSpatialMapController::UpdatePointCloud()
extern void SparseSpatialMapController_UpdatePointCloud_mB15FB7D462ADDC401AB1FFE3A1DCB2BC3D4DDBF1 (void);
// 0x000009B9 System.Void easyar.SparseSpatialMapController::LoadMapBuilderInfo()
extern void SparseSpatialMapController_LoadMapBuilderInfo_mCB3ED40C23F25BA1D568637F0EC96476C985658A (void);
// 0x000009BA System.Void easyar.SparseSpatialMapController::LoadMapManagerInfo(easyar.SparseSpatialMapController/MapManagerSourceData)
extern void SparseSpatialMapController_LoadMapManagerInfo_mDDAE856BB30D0C66B8C47399147E40557A9E27A0 (void);
// 0x000009BB System.Void easyar.SparseSpatialMapController::LoadMapInfo(easyar.SparseSpatialMapController/SparseSpatialMapInfo)
extern void SparseSpatialMapController_LoadMapInfo_mB3B752B04D5C4B922DF3AF62E241AAB1CF827B17 (void);
// 0x000009BC System.Void easyar.SparseSpatialMapController::UpdateMapInLocalizer()
extern void SparseSpatialMapController_UpdateMapInLocalizer_m6C453C3768E0C64A06D39BFFAB245AE87DB8F6F0 (void);
// 0x000009BD System.Void easyar.SparseSpatialMapController::.ctor()
extern void SparseSpatialMapController__ctor_m1060907071F7E835CFC52DF13D20E638662F5F80 (void);
// 0x000009BE System.Void easyar.SparseSpatialMapController::<Host>b__56_0(easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String)
extern void SparseSpatialMapController_U3CHostU3Eb__56_0_m3CAEB4EC3D350CCFA68C581013A003178E786564 (void);
// 0x000009BF UnityEngine.ParticleSystem/Particle easyar.SparseSpatialMapController::<UpdatePointCloud>b__59_0(UnityEngine.Vector3)
extern void SparseSpatialMapController_U3CUpdatePointCloudU3Eb__59_0_m02E8F1ACE41A94F38155BAB09A9EC88EED9B1202 (void);
// 0x000009C0 System.Void easyar.SparseSpatialMapController::<UpdateMapInLocalizer>b__63_0(easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String)
extern void SparseSpatialMapController_U3CUpdateMapInLocalizerU3Eb__63_0_mF3859885855B1D4DAE5DE4A90F9F337DB9E26733 (void);
// 0x000009C1 System.Void easyar.SparseSpatialMapController/SparseSpatialMapInfo::.ctor()
extern void SparseSpatialMapInfo__ctor_mCD732AB4A876F3155F62AFB7583B8C443DF656F1 (void);
// 0x000009C2 System.Void easyar.SparseSpatialMapController/MapManagerSourceData::.ctor()
extern void MapManagerSourceData__ctor_mEC4C9781E8F8E872CE3D43FEF52B76F7E37C3D61 (void);
// 0x000009C3 System.Void easyar.SparseSpatialMapController/ParticleParameter::.ctor()
extern void ParticleParameter__ctor_mF348917BAFD52AADB96F0E5649C604CFA3770739 (void);
// 0x000009C4 System.Void easyar.SparseSpatialMapController/<>c__DisplayClass58_0::.ctor()
extern void U3CU3Ec__DisplayClass58_0__ctor_m9BF560C6D2F6042C8B3E8A19D94B21C8924DB2F5 (void);
// 0x000009C5 UnityEngine.Vector3 easyar.SparseSpatialMapController/<>c__DisplayClass58_0::<UpdatePointCloud>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass58_0_U3CUpdatePointCloudU3Eb__0_m2A25C9C0405219D6C784FD053A43A3317477DF74 (void);
// 0x000009C6 System.Void easyar.SparseSpatialMapController/<>c__DisplayClass63_0::.ctor()
extern void U3CU3Ec__DisplayClass63_0__ctor_m19AE58683953A5C40AF6F2760B6FCC02266EE1DF (void);
// 0x000009C7 System.Void easyar.SparseSpatialMapController/<>c__DisplayClass63_0::<UpdateMapInLocalizer>b__1(easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String)
extern void U3CU3Ec__DisplayClass63_0_U3CUpdateMapInLocalizerU3Eb__1_mBB4730BEC677A6AD74141F35CF25FCB5B7AB3FCC (void);
// 0x000009C8 easyar.SparseSpatialMap easyar.SparseSpatialMapWorkerFrameFilter::get_Builder()
extern void SparseSpatialMapWorkerFrameFilter_get_Builder_mBDD7FD5C1D5874ECEA16C70E90EBB24B60A156D2 (void);
// 0x000009C9 System.Void easyar.SparseSpatialMapWorkerFrameFilter::set_Builder(easyar.SparseSpatialMap)
extern void SparseSpatialMapWorkerFrameFilter_set_Builder_m8FB3708CF2035B1B9F3143EF73679478F41D533B (void);
// 0x000009CA easyar.SparseSpatialMap easyar.SparseSpatialMapWorkerFrameFilter::get_Localizer()
extern void SparseSpatialMapWorkerFrameFilter_get_Localizer_m481D0F8E9147E444A72E4E5314E274A149015FFD (void);
// 0x000009CB System.Void easyar.SparseSpatialMapWorkerFrameFilter::set_Localizer(easyar.SparseSpatialMap)
extern void SparseSpatialMapWorkerFrameFilter_set_Localizer_mF300368DA1D996D55EE36DCDB612A2A19A1205FF (void);
// 0x000009CC easyar.SparseSpatialMapManager easyar.SparseSpatialMapWorkerFrameFilter::get_Manager()
extern void SparseSpatialMapWorkerFrameFilter_get_Manager_m3FEB082165C0C8373D273049674055AF80FEA82C (void);
// 0x000009CD System.Void easyar.SparseSpatialMapWorkerFrameFilter::set_Manager(easyar.SparseSpatialMapManager)
extern void SparseSpatialMapWorkerFrameFilter_set_Manager_mD110FC2EDB5EF821F0FE1CB19AAACD3EB92927CD (void);
// 0x000009CE System.Void easyar.SparseSpatialMapWorkerFrameFilter::add_MapLoad(System.Action`4<easyar.SparseSpatialMapController,easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapWorkerFrameFilter_add_MapLoad_m9761FF716AD4AB2E7B31B8B95F167D55A743EEDF (void);
// 0x000009CF System.Void easyar.SparseSpatialMapWorkerFrameFilter::remove_MapLoad(System.Action`4<easyar.SparseSpatialMapController,easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapWorkerFrameFilter_remove_MapLoad_mF1EA708E880E9F28E711B6189275A1549F55E704 (void);
// 0x000009D0 System.Void easyar.SparseSpatialMapWorkerFrameFilter::add_MapUnload(System.Action`4<easyar.SparseSpatialMapController,easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapWorkerFrameFilter_add_MapUnload_mACB33B7581BAAAE760DDC69C6C91034978F21414 (void);
// 0x000009D1 System.Void easyar.SparseSpatialMapWorkerFrameFilter::remove_MapUnload(System.Action`4<easyar.SparseSpatialMapController,easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapWorkerFrameFilter_remove_MapUnload_m0AD3F4D12187B774221F0895C7DDC7B60831EC7E (void);
// 0x000009D2 System.Void easyar.SparseSpatialMapWorkerFrameFilter::add_MapHost(System.Action`4<easyar.SparseSpatialMapController,easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapWorkerFrameFilter_add_MapHost_mB906F4AF336AFAB07D645A0EEAEFCE1516CE5727 (void);
// 0x000009D3 System.Void easyar.SparseSpatialMapWorkerFrameFilter::remove_MapHost(System.Action`4<easyar.SparseSpatialMapController,easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapWorkerFrameFilter_remove_MapHost_mF4B8E4EEC4D44230DAD3148CB92E1A7744B279B7 (void);
// 0x000009D4 System.Int32 easyar.SparseSpatialMapWorkerFrameFilter::get_BufferRequirement()
extern void SparseSpatialMapWorkerFrameFilter_get_BufferRequirement_m7845B242B6C5ED0227EAA1D157E28818FBDD4C88 (void);
// 0x000009D5 easyar.MotionTrackingStatus easyar.SparseSpatialMapWorkerFrameFilter::get_TrackingStatus()
extern void SparseSpatialMapWorkerFrameFilter_get_TrackingStatus_mF01A0D9EADE683AA43A9A09567B9C29CA231F300 (void);
// 0x000009D6 System.Void easyar.SparseSpatialMapWorkerFrameFilter::set_TrackingStatus(easyar.MotionTrackingStatus)
extern void SparseSpatialMapWorkerFrameFilter_set_TrackingStatus_mFEA33C5312DB9C0A699EE011CF98CF4955465D9A (void);
// 0x000009D7 easyar.SparseSpatialMapWorkerFrameFilter/Mode easyar.SparseSpatialMapWorkerFrameFilter::get_WorkingMode()
extern void SparseSpatialMapWorkerFrameFilter_get_WorkingMode_mF8E9D3D97D5F1BC30860D7CAD2733F96EC97DE84 (void);
// 0x000009D8 System.Void easyar.SparseSpatialMapWorkerFrameFilter::set_WorkingMode(easyar.SparseSpatialMapWorkerFrameFilter/Mode)
extern void SparseSpatialMapWorkerFrameFilter_set_WorkingMode_m149AC1EE7A6BAEF8771908A6BD493F27AD0BF5CE (void);
// 0x000009D9 easyar.SparseSpatialMapController easyar.SparseSpatialMapWorkerFrameFilter::get_LocalizedMap()
extern void SparseSpatialMapWorkerFrameFilter_get_LocalizedMap_m58350A5B898D5E2D5437F76AD6B670CDC0BFC55F (void);
// 0x000009DA System.Void easyar.SparseSpatialMapWorkerFrameFilter::set_LocalizedMap(easyar.SparseSpatialMapController)
extern void SparseSpatialMapWorkerFrameFilter_set_LocalizedMap_m2006E253D0EE4E6D526491D2287EC70DBE4D6353 (void);
// 0x000009DB easyar.SparseSpatialMapController easyar.SparseSpatialMapWorkerFrameFilter::get_BuilderMapController()
extern void SparseSpatialMapWorkerFrameFilter_get_BuilderMapController_m3B422BADE125C970974568C6AEA40AA76B33988A (void);
// 0x000009DC System.Void easyar.SparseSpatialMapWorkerFrameFilter::set_BuilderMapController(easyar.SparseSpatialMapController)
extern void SparseSpatialMapWorkerFrameFilter_set_BuilderMapController_mF5A767EE02F7F72607BC17A0A188478C3BF3AF88 (void);
// 0x000009DD System.Collections.Generic.List`1<easyar.SparseSpatialMapController> easyar.SparseSpatialMapWorkerFrameFilter::get_MapControllers()
extern void SparseSpatialMapWorkerFrameFilter_get_MapControllers_m7F54CCB59965E81B6E646442391498323B64B1E5 (void);
// 0x000009DE System.Void easyar.SparseSpatialMapWorkerFrameFilter::Awake()
extern void SparseSpatialMapWorkerFrameFilter_Awake_m25F8F48DB6D1F5E56DECA3E81B15336948C5416E (void);
// 0x000009DF System.Void easyar.SparseSpatialMapWorkerFrameFilter::OnEnable()
extern void SparseSpatialMapWorkerFrameFilter_OnEnable_m9511A8AFB2D8973E375E8A4352C712F5F0F7B245 (void);
// 0x000009E0 System.Void easyar.SparseSpatialMapWorkerFrameFilter::Start()
extern void SparseSpatialMapWorkerFrameFilter_Start_mA8A0F7D64CA02DDCBF4E97A057343ED30D01EDB6 (void);
// 0x000009E1 System.Void easyar.SparseSpatialMapWorkerFrameFilter::OnDisable()
extern void SparseSpatialMapWorkerFrameFilter_OnDisable_m99E4BB1020148384E0469EFCDB69D3948F68D619 (void);
// 0x000009E2 System.Void easyar.SparseSpatialMapWorkerFrameFilter::OnDestroy()
extern void SparseSpatialMapWorkerFrameFilter_OnDestroy_m1098AB5C3FC342DC9A8ED83E2D058F90B8A1F8F8 (void);
// 0x000009E3 System.Void easyar.SparseSpatialMapWorkerFrameFilter::LoadMap(easyar.SparseSpatialMapController)
extern void SparseSpatialMapWorkerFrameFilter_LoadMap_m06F7C2E177AE7E9BAFD3CB698DC334CE3F43ECC6 (void);
// 0x000009E4 System.Void easyar.SparseSpatialMapWorkerFrameFilter::UnloadMap(easyar.SparseSpatialMapController)
extern void SparseSpatialMapWorkerFrameFilter_UnloadMap_mCBFAEDDEA7601B1BC0DD7BC0548C3FDA4651E7A3 (void);
// 0x000009E5 System.Void easyar.SparseSpatialMapWorkerFrameFilter::HostMap(easyar.SparseSpatialMapController,System.String,easyar.Optional`1<easyar.Image>)
extern void SparseSpatialMapWorkerFrameFilter_HostMap_m2D6DBF8C93EDB848B1A5DD74BEC74B1780407571 (void);
// 0x000009E6 easyar.InputFrameSink easyar.SparseSpatialMapWorkerFrameFilter::InputFrameSink()
extern void SparseSpatialMapWorkerFrameFilter_InputFrameSink_m5F5BB68CCE1223BD89A6F6CFBAF3BAC2FC1FF230 (void);
// 0x000009E7 easyar.OutputFrameSource easyar.SparseSpatialMapWorkerFrameFilter::OutputFrameSource()
extern void SparseSpatialMapWorkerFrameFilter_OutputFrameSource_m5D80F2259FBD057FD2BF029B5E50F7EC65876FBE (void);
// 0x000009E8 System.Void easyar.SparseSpatialMapWorkerFrameFilter::OnTracking(easyar.MotionTrackingStatus)
extern void SparseSpatialMapWorkerFrameFilter_OnTracking_m98D51644FF122D808823EC0878ED351E7F9E15D0 (void);
// 0x000009E9 System.Void easyar.SparseSpatialMapWorkerFrameFilter::OnAssemble(easyar.ARSession)
extern void SparseSpatialMapWorkerFrameFilter_OnAssemble_mF947FBECB928CFF393F6BAA87BD9BE909C4998F4 (void);
// 0x000009EA System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<easyar.Optional`1<easyar.TargetController>,easyar.Matrix44F>> easyar.SparseSpatialMapWorkerFrameFilter::OnResult(easyar.Optional`1<easyar.FrameFilterResult>)
extern void SparseSpatialMapWorkerFrameFilter_OnResult_m394FA667CFD72EAD0B31CB6A0D19EB3A6824EE6B (void);
// 0x000009EB System.Void easyar.SparseSpatialMapWorkerFrameFilter::LoadSparseSpatialMap(easyar.SparseSpatialMapController,System.Action`3<easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapWorkerFrameFilter_LoadSparseSpatialMap_mB9F12BA3C6BC632D600B9A67F4D4E48CC5DD2843 (void);
// 0x000009EC System.Void easyar.SparseSpatialMapWorkerFrameFilter::UnloadSparseSpatialMap(easyar.SparseSpatialMapController,System.Action`3<easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapWorkerFrameFilter_UnloadSparseSpatialMap_m67A02DF6BE72011040F2358358ECE1DAA3CB96FD (void);
// 0x000009ED System.Void easyar.SparseSpatialMapWorkerFrameFilter::HostSparseSpatialMap(easyar.SparseSpatialMapController,System.String,easyar.Optional`1<easyar.Image>,System.Action`3<easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapWorkerFrameFilter_HostSparseSpatialMap_m2771594249E5505ECA30A742F7E9B67EFB1DF646 (void);
// 0x000009EE System.Void easyar.SparseSpatialMapWorkerFrameFilter::LoadSparseSpatialMapBuild(easyar.SparseSpatialMapController)
extern void SparseSpatialMapWorkerFrameFilter_LoadSparseSpatialMapBuild_m59759E14AA4A10642A76B1ABDABA4729932BE984 (void);
// 0x000009EF System.Void easyar.SparseSpatialMapWorkerFrameFilter::UnloadSparseSpatialMapBuild(easyar.SparseSpatialMapController)
extern void SparseSpatialMapWorkerFrameFilter_UnloadSparseSpatialMapBuild_m8B3CBCF56AC0BD1178D44C16D05778E6109AE02D (void);
// 0x000009F0 easyar.SparseSpatialMapController easyar.SparseSpatialMapWorkerFrameFilter::TryGetMapController(System.String)
extern void SparseSpatialMapWorkerFrameFilter_TryGetMapController_m1E5CB46E6EB44D12426290FEC8ACBD8C8656EA34 (void);
// 0x000009F1 System.Void easyar.SparseSpatialMapWorkerFrameFilter::NotifyEmptyConfig(easyar.SparseSpatialMapWorkerFrameFilter/SpatialMapServiceConfig)
extern void SparseSpatialMapWorkerFrameFilter_NotifyEmptyConfig_mAE65C3A3AAF64FC0BC741F9ECDC086EBC3815759 (void);
// 0x000009F2 System.Void easyar.SparseSpatialMapWorkerFrameFilter::.ctor()
extern void SparseSpatialMapWorkerFrameFilter__ctor_m1EFB5826D5C0EFE2723DE27E5DCA43E8B4B3CE84 (void);
// 0x000009F3 System.Void easyar.SparseSpatialMapWorkerFrameFilter::<OnAssemble>b__62_0(easyar.WorldRootController)
extern void SparseSpatialMapWorkerFrameFilter_U3COnAssembleU3Eb__62_0_m1C6E2C8CF69B82D1DF8CE6FBECC0F00C9EA2AA15 (void);
// 0x000009F4 System.Void easyar.SparseSpatialMapWorkerFrameFilter/MapLocalizerConfig::.ctor()
extern void MapLocalizerConfig__ctor_m4FC8CD78E895F314865F830C33080C0C96A0F775 (void);
// 0x000009F5 System.Void easyar.SparseSpatialMapWorkerFrameFilter/SpatialMapServiceConfig::.ctor()
extern void SpatialMapServiceConfig__ctor_mA4998B092B643B05664589BF6CACB5BD001B4DB1 (void);
// 0x000009F6 System.Void easyar.SparseSpatialMapWorkerFrameFilter/<>c__DisplayClass64_0::.ctor()
extern void U3CU3Ec__DisplayClass64_0__ctor_mEA69A6219847D01B58AEEA53C1A3C3D1D2745EC9 (void);
// 0x000009F7 System.Void easyar.SparseSpatialMapWorkerFrameFilter/<>c__DisplayClass64_0::<LoadSparseSpatialMap>b__0(System.Boolean,System.String)
extern void U3CU3Ec__DisplayClass64_0_U3CLoadSparseSpatialMapU3Eb__0_m1E4A235FD28AAA321B41363A3DE85D6D054D6127 (void);
// 0x000009F8 System.Void easyar.SparseSpatialMapWorkerFrameFilter/<>c__DisplayClass65_0::.ctor()
extern void U3CU3Ec__DisplayClass65_0__ctor_m3D4794B9D67A798D9C8EC7A48437F6E25BB028CE (void);
// 0x000009F9 System.Void easyar.SparseSpatialMapWorkerFrameFilter/<>c__DisplayClass65_0::<UnloadSparseSpatialMap>b__0(System.Boolean)
extern void U3CU3Ec__DisplayClass65_0_U3CUnloadSparseSpatialMapU3Eb__0_m4175627CA8C02EA9C439FE7F58E845E06851330F (void);
// 0x000009FA System.Void easyar.SparseSpatialMapWorkerFrameFilter/<>c__DisplayClass66_0::.ctor()
extern void U3CU3Ec__DisplayClass66_0__ctor_m12FC1D51B727584157542E9DEC9D57B3BD8AD77A (void);
// 0x000009FB System.Void easyar.SparseSpatialMapWorkerFrameFilter/<>c__DisplayClass66_0::<HostSparseSpatialMap>b__0(System.Boolean,System.String,System.String)
extern void U3CU3Ec__DisplayClass66_0_U3CHostSparseSpatialMapU3Eb__0_m9182CA103270E33CA7D49BC825B645AA54B56113 (void);
// 0x000009FC easyar.SurfaceTracker easyar.SurfaceTrackerFrameFilter::get_Tracker()
extern void SurfaceTrackerFrameFilter_get_Tracker_m3515EE44BB36EDE69EA10E7DFB43725F308B37E8 (void);
// 0x000009FD System.Void easyar.SurfaceTrackerFrameFilter::set_Tracker(easyar.SurfaceTracker)
extern void SurfaceTrackerFrameFilter_set_Tracker_mE4E2C8642D3A95893D082229CB198D89059ACCB8 (void);
// 0x000009FE System.Int32 easyar.SurfaceTrackerFrameFilter::get_BufferRequirement()
extern void SurfaceTrackerFrameFilter_get_BufferRequirement_mD8E622F9ABF6B7C6A177027B6E14ED848864BC78 (void);
// 0x000009FF System.Void easyar.SurfaceTrackerFrameFilter::Awake()
extern void SurfaceTrackerFrameFilter_Awake_m94269E304BDEDE5FB4034A25FD94636FFEDFD20A (void);
// 0x00000A00 System.Void easyar.SurfaceTrackerFrameFilter::OnEnable()
extern void SurfaceTrackerFrameFilter_OnEnable_m3CEFCB2BBBC576E03D8EE258D8C45C97B52AC1D4 (void);
// 0x00000A01 System.Void easyar.SurfaceTrackerFrameFilter::Start()
extern void SurfaceTrackerFrameFilter_Start_m3398EA0F512EB7EF15107E95BF754E5D1BC47E95 (void);
// 0x00000A02 System.Void easyar.SurfaceTrackerFrameFilter::OnDisable()
extern void SurfaceTrackerFrameFilter_OnDisable_m2DA389874D3D534850F2A8BFC3F70706CC20EFE9 (void);
// 0x00000A03 System.Void easyar.SurfaceTrackerFrameFilter::OnDestroy()
extern void SurfaceTrackerFrameFilter_OnDestroy_mB5A68C415E73D1648BF5D85A253CD0768E581B4B (void);
// 0x00000A04 easyar.InputFrameSink easyar.SurfaceTrackerFrameFilter::InputFrameSink()
extern void SurfaceTrackerFrameFilter_InputFrameSink_mCE559F0CF774BB125F1AF76AA777C0CC5B0EA6C5 (void);
// 0x00000A05 easyar.OutputFrameSource easyar.SurfaceTrackerFrameFilter::OutputFrameSource()
extern void SurfaceTrackerFrameFilter_OutputFrameSource_mE86E2ADE305407A9971A95C7BEB83D99970201AB (void);
// 0x00000A06 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<easyar.Optional`1<easyar.TargetController>,easyar.Matrix44F>> easyar.SurfaceTrackerFrameFilter::OnResult(easyar.Optional`1<easyar.FrameFilterResult>)
extern void SurfaceTrackerFrameFilter_OnResult_mD11A18271B13CEEC49AFDFF9347B6DBEF3F8F79C (void);
// 0x00000A07 System.Void easyar.SurfaceTrackerFrameFilter::.ctor()
extern void SurfaceTrackerFrameFilter__ctor_mA3A77E0400ADEEDF557453CED049CE43E0AF2696 (void);
// 0x00000A08 easyar.VIOCameraDeviceUnion/DeviceUnion easyar.VIOCameraDeviceUnion::get_Device()
extern void VIOCameraDeviceUnion_get_Device_mFE46D8F19218FECD9FF165FE6E9759992BD707AA (void);
// 0x00000A09 System.Void easyar.VIOCameraDeviceUnion::set_Device(easyar.VIOCameraDeviceUnion/DeviceUnion)
extern void VIOCameraDeviceUnion_set_Device_m9B7BA8186E8CA67F5674BBA422FC088F10786232 (void);
// 0x00000A0A System.Void easyar.VIOCameraDeviceUnion::add_DeviceCreated(System.Action)
extern void VIOCameraDeviceUnion_add_DeviceCreated_m74AD89492D2711324561C8E330BADB762414D47C (void);
// 0x00000A0B System.Void easyar.VIOCameraDeviceUnion::remove_DeviceCreated(System.Action)
extern void VIOCameraDeviceUnion_remove_DeviceCreated_m814DE270549834E858D6F5DA0DA2998774673B23 (void);
// 0x00000A0C System.Void easyar.VIOCameraDeviceUnion::add_DeviceOpened(System.Action)
extern void VIOCameraDeviceUnion_add_DeviceOpened_m19356CB413BC5584A315200A6C88CCDD82736F90 (void);
// 0x00000A0D System.Void easyar.VIOCameraDeviceUnion::remove_DeviceOpened(System.Action)
extern void VIOCameraDeviceUnion_remove_DeviceOpened_m23CAF95355804DDDF8E0FABA8BC358D9638813E6 (void);
// 0x00000A0E System.Void easyar.VIOCameraDeviceUnion::add_DeviceClosed(System.Action)
extern void VIOCameraDeviceUnion_add_DeviceClosed_m5B573063D9E27DA34B996A6C0220A4F4579AE97B (void);
// 0x00000A0F System.Void easyar.VIOCameraDeviceUnion::remove_DeviceClosed(System.Action)
extern void VIOCameraDeviceUnion_remove_DeviceClosed_m29E68A81FFAEB479EE86B4F072F416A8CE65B1EA (void);
// 0x00000A10 System.Int32 easyar.VIOCameraDeviceUnion::get_BufferCapacity()
extern void VIOCameraDeviceUnion_get_BufferCapacity_mF92FB40A1AA7185C05055BC8BA1B258C63D425C4 (void);
// 0x00000A11 System.Void easyar.VIOCameraDeviceUnion::set_BufferCapacity(System.Int32)
extern void VIOCameraDeviceUnion_set_BufferCapacity_m43DA832DE9AFD12060C1A5D5FF46025B1E4CA0BB (void);
// 0x00000A12 System.Boolean easyar.VIOCameraDeviceUnion::get_HasSpatialInformation()
extern void VIOCameraDeviceUnion_get_HasSpatialInformation_mFE45759DD9E710CCC60CE6E0EF382555CBF6F4C5 (void);
// 0x00000A13 System.Void easyar.VIOCameraDeviceUnion::OnEnable()
extern void VIOCameraDeviceUnion_OnEnable_m1CD9C7E567F7916713973C710A34683660E00063 (void);
// 0x00000A14 System.Void easyar.VIOCameraDeviceUnion::Start()
extern void VIOCameraDeviceUnion_Start_m9A131FC6ECDF09499BB174DD6D77CC544C3D3FBA (void);
// 0x00000A15 System.Void easyar.VIOCameraDeviceUnion::OnDisable()
extern void VIOCameraDeviceUnion_OnDisable_m2FE3A6583CD9A17742AA50527BCF6570A080AE54 (void);
// 0x00000A16 System.Collections.Generic.List`1<UnityEngine.Vector3> easyar.VIOCameraDeviceUnion::HitTestAgainstHorizontalPlane(UnityEngine.Vector2)
extern void VIOCameraDeviceUnion_HitTestAgainstHorizontalPlane_m324F64200611FDE8B4BD937E0C7427B53D1F759E (void);
// 0x00000A17 System.Collections.Generic.List`1<UnityEngine.Vector3> easyar.VIOCameraDeviceUnion::HitTestAgainstPointCloud(UnityEngine.Vector2)
extern void VIOCameraDeviceUnion_HitTestAgainstPointCloud_mC7767E6028282E2C202074F6B0BDF6054A67D1A1 (void);
// 0x00000A18 System.Void easyar.VIOCameraDeviceUnion::Open()
extern void VIOCameraDeviceUnion_Open_m612581D070F3149183FAB906756EF4876A59F49E (void);
// 0x00000A19 System.Void easyar.VIOCameraDeviceUnion::Close()
extern void VIOCameraDeviceUnion_Close_mB9E611FE58DE480C8DAAADA67FDA46D6475D38D8 (void);
// 0x00000A1A System.Void easyar.VIOCameraDeviceUnion::Connect(easyar.InputFrameSink)
extern void VIOCameraDeviceUnion_Connect_m414A550851F691BE79005861A1FE3032A2A23DE2 (void);
// 0x00000A1B System.Void easyar.VIOCameraDeviceUnion::CreateMotionTrackerCameraDevice()
extern void VIOCameraDeviceUnion_CreateMotionTrackerCameraDevice_m0223C71EDA1D077DE1191B9CB6377665E48A9FBD (void);
// 0x00000A1C System.Void easyar.VIOCameraDeviceUnion::CreateARKitCameraDevice()
extern void VIOCameraDeviceUnion_CreateARKitCameraDevice_m87901367060D75FB00E07F4C9919F01200032A7E (void);
// 0x00000A1D System.Void easyar.VIOCameraDeviceUnion::CreateARCoreCameraDevice()
extern void VIOCameraDeviceUnion_CreateARCoreCameraDevice_m4C8D7FF57E06C4FC676C2FBAE0F843CD2C51BE2F (void);
// 0x00000A1E System.Boolean easyar.VIOCameraDeviceUnion::CheckARCore()
extern void VIOCameraDeviceUnion_CheckARCore_m35855E7EAFA4867DAC69FEC8DC90A53E888E78B7 (void);
// 0x00000A1F System.Void easyar.VIOCameraDeviceUnion::.ctor()
extern void VIOCameraDeviceUnion__ctor_mED699082D218F5A5E3B9EC7747CB1E9AC2129967 (void);
// 0x00000A20 System.Void easyar.VIOCameraDeviceUnion::<Open>b__35_0(easyar.PermissionStatus,System.String)
extern void VIOCameraDeviceUnion_U3COpenU3Eb__35_0_m6EF254CCD5BFA74F72BCE71EA2E0C494780258A0 (void);
// 0x00000A21 System.Void easyar.VIOCameraDeviceUnion/DeviceUnion::.ctor(easyar.MotionTrackerCameraDevice)
extern void DeviceUnion__ctor_m42BE7827CFB2D9FF4E6253373351A8FC3562A937 (void);
// 0x00000A22 System.Void easyar.VIOCameraDeviceUnion/DeviceUnion::.ctor(easyar.ARKitCameraDevice)
extern void DeviceUnion__ctor_mFE6386AF99318072092D638781A86280C9BFD81F (void);
// 0x00000A23 System.Void easyar.VIOCameraDeviceUnion/DeviceUnion::.ctor(easyar.ARCoreCameraDevice)
extern void DeviceUnion__ctor_mFE048003E68E9B6BE5DAC1A4EF3C76E1A8DF0E3B (void);
// 0x00000A24 easyar.VIOCameraDeviceUnion/DeviceUnion/VIODeviceType easyar.VIOCameraDeviceUnion/DeviceUnion::get_DeviceType()
extern void DeviceUnion_get_DeviceType_m3C4A65E1E009312D2B43DA6C9A89ADBDFC457536 (void);
// 0x00000A25 System.Void easyar.VIOCameraDeviceUnion/DeviceUnion::set_DeviceType(easyar.VIOCameraDeviceUnion/DeviceUnion/VIODeviceType)
extern void DeviceUnion_set_DeviceType_m07F63903240ECF385A4C8E518FC8A7733BFF528C (void);
// 0x00000A26 easyar.MotionTrackerCameraDevice easyar.VIOCameraDeviceUnion/DeviceUnion::get_MotionTrackerCameraDevice()
extern void DeviceUnion_get_MotionTrackerCameraDevice_m8B42F6AC6B41F0901184C306406564D1B48F9C77 (void);
// 0x00000A27 System.Void easyar.VIOCameraDeviceUnion/DeviceUnion::set_MotionTrackerCameraDevice(easyar.MotionTrackerCameraDevice)
extern void DeviceUnion_set_MotionTrackerCameraDevice_mF15CA8EAB1010703DCC5361C73D1D1FF1C95DD1A (void);
// 0x00000A28 easyar.ARKitCameraDevice easyar.VIOCameraDeviceUnion/DeviceUnion::get_ARKitCameraDevice()
extern void DeviceUnion_get_ARKitCameraDevice_m749823C8890CD578CE733E922602E55369A67597 (void);
// 0x00000A29 System.Void easyar.VIOCameraDeviceUnion/DeviceUnion::set_ARKitCameraDevice(easyar.ARKitCameraDevice)
extern void DeviceUnion_set_ARKitCameraDevice_m8D56622C96FC6135148139F74BBC2BBC3FFB8511 (void);
// 0x00000A2A easyar.ARCoreCameraDevice easyar.VIOCameraDeviceUnion/DeviceUnion::get_ARCoreCameraDevice()
extern void DeviceUnion_get_ARCoreCameraDevice_mBFB7644ACC4238AE4A76C336CBAAAD435BBCDF36 (void);
// 0x00000A2B System.Void easyar.VIOCameraDeviceUnion/DeviceUnion::set_ARCoreCameraDevice(easyar.ARCoreCameraDevice)
extern void DeviceUnion_set_ARCoreCameraDevice_mB09C9671ABD8F810CE339DBDFF767BFA42C8220B (void);
// 0x00000A2C easyar.MotionTrackerCameraDevice easyar.VIOCameraDeviceUnion/DeviceUnion::op_Explicit(easyar.VIOCameraDeviceUnion/DeviceUnion)
extern void DeviceUnion_op_Explicit_m097C87EB49833AFBA43BFDDA9212A814B639882B (void);
// 0x00000A2D easyar.ARKitCameraDevice easyar.VIOCameraDeviceUnion/DeviceUnion::op_Explicit(easyar.VIOCameraDeviceUnion/DeviceUnion)
extern void DeviceUnion_op_Explicit_mF59DBDA51A7A0882EBDF6DBDBB2E988B4371FD79 (void);
// 0x00000A2E easyar.ARCoreCameraDevice easyar.VIOCameraDeviceUnion/DeviceUnion::op_Explicit(easyar.VIOCameraDeviceUnion/DeviceUnion)
extern void DeviceUnion_op_Explicit_mB33D9559094F3DD71CD9A572888D53762FABC1D8 (void);
// 0x00000A2F easyar.VIOCameraDeviceUnion/DeviceUnion easyar.VIOCameraDeviceUnion/DeviceUnion::op_Implicit(easyar.MotionTrackerCameraDevice)
extern void DeviceUnion_op_Implicit_m317B7F9378C62D68CBD7024124ADB1816A8BE2C3 (void);
// 0x00000A30 easyar.VIOCameraDeviceUnion/DeviceUnion easyar.VIOCameraDeviceUnion/DeviceUnion::op_Implicit(easyar.ARKitCameraDevice)
extern void DeviceUnion_op_Implicit_m897FB74DF520F9EE362080EA7F50E16B035512E0 (void);
// 0x00000A31 easyar.VIOCameraDeviceUnion/DeviceUnion easyar.VIOCameraDeviceUnion/DeviceUnion::op_Implicit(easyar.ARCoreCameraDevice)
extern void DeviceUnion_op_Implicit_mA20D8DD8D1AEE404315C07C11F448DD04401B556 (void);
// 0x00000A32 System.Type easyar.VIOCameraDeviceUnion/DeviceUnion::Type()
extern void DeviceUnion_Type_m6524D087BBA8401EA8EF88FCB0F7BB77580B45AD (void);
// 0x00000A33 System.String easyar.VIOCameraDeviceUnion/DeviceUnion::ToString()
extern void DeviceUnion_ToString_mF0EA7AC8B8DCDFD3A70E06CD19DD33B391E8D0C4 (void);
// 0x00000A34 System.Void easyar.VIOCameraDeviceUnion/MotionTrackerCameraDeviceParameters::.ctor()
extern void MotionTrackerCameraDeviceParameters__ctor_m34CDA873F01AEF2523CA2E9D8B4040CC46808C3E (void);
// 0x00000A35 System.Void easyar.VIOCameraDeviceUnion/<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_mD2E9CE71AE78A1396ED6577530F4853C47D242E1 (void);
// 0x00000A36 System.Void easyar.VIOCameraDeviceUnion/<>c__DisplayClass31_0::<Start>b__0(easyar.CalibrationDownloadStatus,easyar.Optional`1<System.String>)
extern void U3CU3Ec__DisplayClass31_0_U3CStartU3Eb__0_mD753B72621EC50CA6FC0779D4A1B5536E33D8B1B (void);
// 0x00000A37 System.Void easyar.VIOCameraDeviceUnion/<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_mE10B400AC5B66BD025374C271F0FB1F15E6AE000 (void);
// 0x00000A38 System.Void easyar.VIOCameraDeviceUnion/<>c__DisplayClass38_0::<CreateMotionTrackerCameraDevice>b__0()
extern void U3CU3Ec__DisplayClass38_0_U3CCreateMotionTrackerCameraDeviceU3Eb__0_m9C1E3A91D3151CE4A96E9C7F314D15D329917DB5 (void);
// 0x00000A39 System.Void easyar.VIOCameraDeviceUnion/<>c__DisplayClass38_0::<CreateMotionTrackerCameraDevice>b__1()
extern void U3CU3Ec__DisplayClass38_0_U3CCreateMotionTrackerCameraDeviceU3Eb__1_m93D64D686BB2A1514134BA801D1F178F210D4E21 (void);
// 0x00000A3A System.Void easyar.VIOCameraDeviceUnion/<>c__DisplayClass38_0::<CreateMotionTrackerCameraDevice>b__2()
extern void U3CU3Ec__DisplayClass38_0_U3CCreateMotionTrackerCameraDeviceU3Eb__2_m6A5523B26A148FA5ADE6117ED75857C612573F16 (void);
// 0x00000A3B System.Void easyar.VIOCameraDeviceUnion/<>c__DisplayClass38_0::<CreateMotionTrackerCameraDevice>b__3(System.Int32)
extern void U3CU3Ec__DisplayClass38_0_U3CCreateMotionTrackerCameraDeviceU3Eb__3_m0B0F363DA396E8704E9F910B3922AAF6FF167ECA (void);
// 0x00000A3C System.Int32 easyar.VIOCameraDeviceUnion/<>c__DisplayClass38_0::<CreateMotionTrackerCameraDevice>b__4()
extern void U3CU3Ec__DisplayClass38_0_U3CCreateMotionTrackerCameraDeviceU3Eb__4_m4533E42DCA0BE1194634AAEE3387ED5D40140E84 (void);
// 0x00000A3D System.Void easyar.VIOCameraDeviceUnion/<>c__DisplayClass38_0::<CreateMotionTrackerCameraDevice>b__5(easyar.InputFrameSink)
extern void U3CU3Ec__DisplayClass38_0_U3CCreateMotionTrackerCameraDeviceU3Eb__5_m23E0990A11F325669E4B63986E6EEF0E85BA4CF5 (void);
// 0x00000A3E System.Void easyar.VIOCameraDeviceUnion/<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_mEB93E2798B4E3F271E37BD63A51C2C897EEAFC0B (void);
// 0x00000A3F System.Void easyar.VIOCameraDeviceUnion/<>c__DisplayClass39_0::<CreateARKitCameraDevice>b__0()
extern void U3CU3Ec__DisplayClass39_0_U3CCreateARKitCameraDeviceU3Eb__0_mE54857557C90DA065E65A151B7213B659ABE5B7D (void);
// 0x00000A40 System.Void easyar.VIOCameraDeviceUnion/<>c__DisplayClass39_0::<CreateARKitCameraDevice>b__1()
extern void U3CU3Ec__DisplayClass39_0_U3CCreateARKitCameraDeviceU3Eb__1_m132BC42564B6E6893A7CFB36CC162CB15A76F608 (void);
// 0x00000A41 System.Void easyar.VIOCameraDeviceUnion/<>c__DisplayClass39_0::<CreateARKitCameraDevice>b__2()
extern void U3CU3Ec__DisplayClass39_0_U3CCreateARKitCameraDeviceU3Eb__2_m7D6102876579DE633CBB9C25DCA95EC6605A485A (void);
// 0x00000A42 System.Void easyar.VIOCameraDeviceUnion/<>c__DisplayClass39_0::<CreateARKitCameraDevice>b__3(System.Int32)
extern void U3CU3Ec__DisplayClass39_0_U3CCreateARKitCameraDeviceU3Eb__3_m74C6365AEF2BB9E23DA65E9489D787AAE3B299DB (void);
// 0x00000A43 System.Int32 easyar.VIOCameraDeviceUnion/<>c__DisplayClass39_0::<CreateARKitCameraDevice>b__4()
extern void U3CU3Ec__DisplayClass39_0_U3CCreateARKitCameraDeviceU3Eb__4_mD745157CE408F62396258C3AB7AB19C3F23D61A8 (void);
// 0x00000A44 System.Void easyar.VIOCameraDeviceUnion/<>c__DisplayClass39_0::<CreateARKitCameraDevice>b__5(easyar.InputFrameSink)
extern void U3CU3Ec__DisplayClass39_0_U3CCreateARKitCameraDeviceU3Eb__5_mBB62A04157673A22F093EF85C050CB2BB6FDD444 (void);
// 0x00000A45 System.Void easyar.VIOCameraDeviceUnion/<>c__DisplayClass40_0::.ctor()
extern void U3CU3Ec__DisplayClass40_0__ctor_m3D36A66D351565CB02679989918F8BEFD97EF0A4 (void);
// 0x00000A46 System.Void easyar.VIOCameraDeviceUnion/<>c__DisplayClass40_0::<CreateARCoreCameraDevice>b__0()
extern void U3CU3Ec__DisplayClass40_0_U3CCreateARCoreCameraDeviceU3Eb__0_mE59BA8E0790239EB80BD815A7CF374158DB2EB2A (void);
// 0x00000A47 System.Void easyar.VIOCameraDeviceUnion/<>c__DisplayClass40_0::<CreateARCoreCameraDevice>b__1()
extern void U3CU3Ec__DisplayClass40_0_U3CCreateARCoreCameraDeviceU3Eb__1_mBB2F2A9508CCC94BDABB5CE902D0B9E30DCCD5EF (void);
// 0x00000A48 System.Void easyar.VIOCameraDeviceUnion/<>c__DisplayClass40_0::<CreateARCoreCameraDevice>b__2()
extern void U3CU3Ec__DisplayClass40_0_U3CCreateARCoreCameraDeviceU3Eb__2_m86D4C558C4B88BCB133288CDA72CF5DE991A815B (void);
// 0x00000A49 System.Void easyar.VIOCameraDeviceUnion/<>c__DisplayClass40_0::<CreateARCoreCameraDevice>b__3(System.Int32)
extern void U3CU3Ec__DisplayClass40_0_U3CCreateARCoreCameraDeviceU3Eb__3_m722A845CC0BF1A95FD760C46570890F5FDB95865 (void);
// 0x00000A4A System.Int32 easyar.VIOCameraDeviceUnion/<>c__DisplayClass40_0::<CreateARCoreCameraDevice>b__4()
extern void U3CU3Ec__DisplayClass40_0_U3CCreateARCoreCameraDeviceU3Eb__4_m9F9086D23582BA66C5F0E3B62A2E08715E8B0AA2 (void);
// 0x00000A4B System.Void easyar.VIOCameraDeviceUnion/<>c__DisplayClass40_0::<CreateARCoreCameraDevice>b__5(easyar.InputFrameSink)
extern void U3CU3Ec__DisplayClass40_0_U3CCreateARCoreCameraDeviceU3Eb__5_m7710FE5A0C4DF48A3FF0EA2219CEFD63DBDFE069 (void);
static Il2CppMethodPointer s_methodPointers[2635] = 
{
	KembaliDariEasyAR_kembaliDariEasyAR_m5A4148BD3FAABBEF05148EEDD274C2A84E161ADD,
	KembaliDariEasyAR_aktifkanAR_m18662C8381CB5E87A4A409CE5B9CA0798C87200A,
	KembaliDariEasyAR__ctor_m232E4884579EC832A4944E2310C9A4D7539D087E,
	ObjectSpawner_Start_mB5C9193D6076E2D9586CA1F5862D4C7D9AF008DE,
	ObjectSpawner_Update_mBCDB36C4FF21AC8AF713E11BDB1FE504B0EC8D19,
	ObjectSpawner__ctor_mD103BAD86ABBCFC5726513C071E31158C1A150D3,
	PlacementIndikator_Start_m5B6CB5353735CDE6E03933108AB81D1491F5B137,
	PlacementIndikator_Update_m135BFAE5D934DB8B9F8D7040A82A782786ECFF4F,
	PlacementIndikator__ctor_mA4B09B95B69DF6D34E510FE47750015A98BDFD71,
	PlaneControl_Awake_m5C2AE092D75FA1EB4A2188B5B540A1DB5007E368,
	PlaneControl_Start_mBC9449251D6CA209730FB231E4129015EE1B33C3,
	PlaneControl_Update_mAC2F27CBEAE431DB346B68F325188598F5291C6F,
	PlaneControl_TogglePlaneDetectionAndVisibility_mC69B8FD2F19FD4F2404219864C9C1FEF3FD6228F,
	PlaneControl_setAllPlaneActiveOrDeactive_mA4EB838468BA4B4D9A3153467360F9FCC7E11748,
	PlaneControl__ctor_m45312DC01E68365151FBB48E1774961CCE5FEFC8,
	PlaneControlerr_Awake_m6596122CFE455A44468FFCF71C5E9020DC158C94,
	PlaneControlerr_Start_m7E6958B9FB8C3AEC852A10D5B46995090DBB7592,
	PlaneControlerr_Update_mA612E45B5188888557F8F059EC6EF1C6C853566C,
	PlaneControlerr_TogglePlaneDetectionAndVisibility_mDEEF2B5D0E7CF4EFF0263F04448AF15E2584261E,
	PlaneControlerr_setAllPlaneActiveOrDeactive_m6FEFED64E94EE4F94D78B8682D1285FAE5C2D5DE,
	PlaneControlerr__ctor_mA4B4EC97C45FDB39A861D6ADCDB82D33AD1B4E0C,
	PortalPlacer_Awake_mF75A0F743CD792E50FAE8F13E07E6CECEE35E80F,
	PortalPlacer_Start_mAB9088429140C18FFA055513A151CB7CE1701C7D,
	PortalPlacer_Update_m878E0F76F8807C08AB9F41A32C681A0777E97F47,
	PortalPlacer__ctor_m266527257FCC26F3F1DDF63B696465F391018D85,
	Potal_Start_mC78472AC4C18A70A25252379398AB4394743C104,
	Potal_Update_m0FB35F0BCE2504F4B8C053F4C351532400671214,
	Potal_OnTriggerStay_m8254040F35D9335D2CFF2E7AF80E68C117A514DB,
	Potal_TogglePlaneDetectionAndVisibility_m10FA42F6907B6BB5866F87BA44F831EB73646543,
	Potal_setAllPlaneActiveOrDeactive_mE9AD31C5C45F690E4511CE832E803999512142FC,
	Potal__ctor_m5B6826C2EA366992F2633BF6FD2CAF7DC5B0540E,
	ARCoreSuport_Start_mBFD408374D7E30C82694DBE2D3C2896217CE1006,
	ARCoreSuport__ctor_mCF22F9C1DB756B05465502964E4B71E732F01C04,
	U3CStartU3Ed__2__ctor_m76493853135E7B3E98D72CAAFD17980374605DE0,
	U3CStartU3Ed__2_System_IDisposable_Dispose_mF2D6ACAB9BD8472C08C90F50438A0657E8412A70,
	U3CStartU3Ed__2_MoveNext_mC0911D70346CA9891F9A0FAE1D05E44E7D8DBA86,
	U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m42BEA4DE7F2E3D42FAC40F1BD820B34ECA178777,
	U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m4E31F02CE2F73E30AA546D239DA3D2DFCF2DDED3,
	U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_mD08A543706D0F525E2B14CA73AD25DDB7336F8B9,
	Activate_Start_m08C8888DB0894B521E7A391FE19387E224C5C10A,
	Activate_Update_mA3BB4F4A0B7BBFE9B6F62B2FB0086021D43760D4,
	Activate_aktifkanGameObject_m23283F6F3095D28077566382E1F926F1BC985A09,
	Activate_nonaktifkanGameObject_m8E2CEDFD2FA7912D361D44E528696247C7D7D84A,
	Activate_assss_mA1AC697A6E9A852727107D362EEEF8DA3DCCE5FD,
	Activate__ctor_mFDE6E64A8AE313D7BCEC986811153A8443E2306C,
	Anim_Update_m246B9B6F3A5A51531603888DD2E0C56FDB496B46,
	Anim_playanimation_m68C20969E30DCA86C059A75C74B6A62FFDF60A1B,
	Anim_stand_m9B3427C6CB70E7B4326709D1318868D5C66E9B21,
	Anim_delay_m49B92C3EC4783A88BB915F7B9E5828E35676DFE3,
	Anim__ctor_mD06BEBFB08FB944CC36A2F70EDCD029A8B0DD0D8,
	U3CdelayU3Ed__3__ctor_mB41968B0DEEA004DDB90B2E10A791EB96E48D2CD,
	U3CdelayU3Ed__3_System_IDisposable_Dispose_m1F06D74D5874EB191FBFA69957B9C26470ADAC4A,
	U3CdelayU3Ed__3_MoveNext_mFDE630DACFE2D69CE5A5DE03FE60482B185AFECF,
	U3CdelayU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96FF82B4AFBECC14FD6D4D385BD51F77F6B96E26,
	U3CdelayU3Ed__3_System_Collections_IEnumerator_Reset_mDDF68C47CED558B3AB02F86D903391C0BED82521,
	U3CdelayU3Ed__3_System_Collections_IEnumerator_get_Current_m9E064515BE7020B0F9EF09EA13C41705F94DE64D,
	EasyARControler_Start_m1092DCC1644878FF6D86EA66E34662C6AA7B6BF7,
	EasyARControler_Next_m0866CDC4A065A9536088EE8FA586983F22AA60B2,
	EasyARControler_prev_m274893F4512C3232546C36C9DA28FCF582C59ECC,
	EasyARControler_PlayAnimasi_m8AEF21F340DBC4904B1C88966865EC3CB938E16A,
	EasyARControler_animCOntrol_mEB0904D4FC397AE091770585D7468F2507E3A02C,
	EasyARControler__ctor_mA8B53629B1A3D1766F29B278CE5C7A0D49415FB4,
	SelesaiMembacaMateri_selesaiMembacaMateri_m0082ABE8F9F8EC2765B51433B8FFE5BBDB7AAC2A,
	SelesaiMembacaMateri__ctor_m5E2BAEE86B86E242F69F53043FC61DAFE93CD996,
	btnManagement_Start_mF48E0EBD090E2DDED5807A39183C9FE90099FA4B,
	btnManagement_cek_mCCD1DC838CC0B0F1702ED6B5CC3C65E71A8130F4,
	btnManagement_Update_m3E3EB97A2B03CC47E0FCF8BE621A3A64CA2044F6,
	btnManagement__ctor_mC22F4E3DF7589275A0EBD5D94A035135B5FCA7DC,
	Navigasi_OpenScene_m573932EBABBE9DFB8F7762AE9EB1D9C8C222A7C4,
	Navigasi_MenuActivation_mA700273D1FA27CD1491E1491E01598885CB0418D,
	Navigasi_exit_m674CE2DB540A966B2042A550EE129D75AF2C875B,
	Navigasi_Aktifkan_m613C59BC8DE196A9AF3DFCB4A58BA8212D43AA49,
	Navigasi__ctor_m2DA66A4CFA2ABA1173FE29EAEB2AE8FAE319E132,
	ManagemntPetunjukFlow_Start_mC80FD3F6FAE704EA1246FC88A22914DFE4C677D3,
	ManagemntPetunjukFlow_openInformasi_mFDA0C10D88BF0C146482B68D48E25BF2E41A4FE6,
	ManagemntPetunjukFlow_Update_mA4924B875F7103D09EC2A9E47843375DEAB4BB44,
	ManagemntPetunjukFlow__ctor_mDF42BBBF5A762596FF7D9AE9B054A5096DB1C2CE,
	ObjectsSpawner_Start_mBF52F9451978F4D58543DED1B8E22CC25F2A63A3,
	ObjectsSpawner_Activate_mC0A44C68979AA05085759BB56B42E157DB2DC50E,
	ObjectsSpawner__ctor_m9BC18DBC0299BC5FD412959A0FEE190ADD31EC01,
	PlacementIndicator_Start_mC0F1E7B8F84514D2EDEA712785CDC9399A376200,
	PlacementIndicator_Update_m04B65B8903A034EA4C717E34BE6B8EF861382FE4,
	PlacementIndicator__ctor_mC5EF49C57C7F6519CA9E925EC1A0B13C7D3A08BE,
	aktifkanARFoundation_aktifkan_m41637420386F2152FD39F4E6FC6597E46C4B87D1,
	aktifkanARFoundation__ctor_m4BB032E4BE8924FE10675A4D8CA61191C424E469,
	btnClickOnPortal_Start_m6FBE4A93973B9C19E5011B86BB48F01B8F2FFF02,
	btnClickOnPortal_OnMouseDown_m929B6F6F5A4FA7C0388A44178F76918A9B4E76CD,
	btnClickOnPortal__ctor_m2997580A817FAA16CCC54E7B9FC6D25AA6B0B02C,
	bukaSpawner_karakterAnimPlay_mBC5542E23F774DA358CE324B5D2E068CC3370C6C,
	bukaSpawner_Start_m43ADC1E1A6F16021C060577B717B613512E6ADA0,
	bukaSpawner_OnMouseDown_mFE274A6A878276CA61A520AAB129EB28313F0727,
	bukaSpawner__ctor_m27A97EA406DD50760D700008FA38099EDF9C8F48,
	managementTextInSimulasi_Start_mEE66FDC6C39BF44E18A4A3438C49EEBB2FB98620,
	managementTextInSimulasi_Update_m59357B8EEFA71F874FF3984DC01B28709EE7939C,
	managementTextInSimulasi__ctor_m88D48C28281F88CA536D6B09203C69A8E51C4D5D,
	simulasiManagement_Start_mAD4BD2C81906E79DC15DDBABB29B8091F879BC05,
	simulasiManagement_btnSimulasiOnCLick_m157E9802A2356279E116807AD25B1DB19ECF7960,
	simulasiManagement_btnBackToPortalClicked_mBFD598E7B1B4716069DF00A24230D467311845DC,
	simulasiManagement__ctor_m80F20B2919404EFAE2B21839EB41ACB1883B50D8,
	managementAnimasiSimulasi_Start_mA01F9567FD11A206538A28450925DD056CA2B294,
	managementAnimasiSimulasi_Update_m4BFF1AD5945BDEE79A68F8AB26A663E55CD5CF86,
	managementAnimasiSimulasi_aktifkanAnimasiIdle_m8A4192A4CBA87EA14D9209CE18F26E278EBEF652,
	managementAnimasiSimulasi_nextKlik_mA30AF6B07DB9CE154F8B5EBFEF0CE91E0E801518,
	managementAnimasiSimulasi_PrevKlik_m9AFA86B69756DD9C9C7B102280281A5E888B632E,
	managementAnimasiSimulasi_PlayVideo_m7A1EEF33C8BEF572DE34C490782889100FBE35AE,
	managementAnimasiSimulasi_PauseVideo_m45429CE849824859C0A38295DC8453452A3DBE76,
	managementAnimasiSimulasi_PlaykumpulanAnimasi_m857A5B1E4D2606440522722F7D63F23924AA9FFB,
	managementAnimasiSimulasi__ctor_m475216434EA1295B759C9730B82E22998F14CE17,
	Puzel_Start_m14B74B4823B527582FBF9A8255771F246DF6F4C5,
	Puzel_DragA_m28A63CD501FB6FB3D2A200CABE8697918E53098F,
	Puzel_DragB_mA27B72B871E348D12BA6A64BA3A4F52B165DE8A6,
	Puzel_DragC_m769A8662D4347E4EAE884AA7F818F1A3E2595E99,
	Puzel_DragD_mB614451940FD020131BC13F3E54927D19E431783,
	Puzel_DragE_m80B0D1EE5325BE8D7BA6C5054604655E153662CA,
	Puzel_DragF_m456BC4D4D9237A228EAB68744D8B9448109C98E0,
	Puzel_DragG_m274EC2D0157B68AF06C9B03BEAC872BB88C8029E,
	Puzel_DragH_m80310CC37453D25E45D951155959286216EB6613,
	Puzel_DragI_m45C978FF9078AD152566445A38E682955B7C2EDD,
	Puzel_DropA_m44A99CFE681F22AA7E19D5C77C552F7C3A673B39,
	Puzel_DropB_m19D38314C70F9EDBFEC5257E91131F8D222F808C,
	Puzel_DropC_m15C6DF045568FB2058A7FFD40AB5495F25598C4C,
	Puzel_DropD_mEBDC6A624F90DD312C8E889E6041B27C97580117,
	Puzel_DropE_mB0AECA6DD370BBFC16E941C72622E6A4BEF93F05,
	Puzel_DropF_m68DDF56F06320278ABE5C69BC8CB0B3081195ACB,
	Puzel_DropG_mB4CA6A0237FD6CD6F2C941801CFD5E14F597203E,
	Puzel_DropH_m70A03B096B0627EA0B78CB17ED671F04463161EC,
	Puzel_DropI_mD8B728217AE9413B1A14770FBF38F1C6427748B4,
	Puzel_ResetNilai_m6F8B5397C06BB920DF97CB3A2EB33A2ABDE3E70F,
	Puzel_Update_mF6891C58AB28A5104F307C217E4FDEB89E735B68,
	Puzel__ctor_m67EADCAA4061E27AE3FBA845818C869207A04739,
	hasilPuzel_Start_mF82B330C7C647D55647C0B4904B99BB2F26D8650,
	hasilPuzel_Update_m5CCC3D5A4D4484E75724646A5D4D0427E6C14514,
	hasilPuzel__ctor_m0C60ED81BDFCF89B3728FB611ECDE9D71BDD93F7,
	ScreenShotShare_ShareScreenshotWithText_m64E941F7BA653D37750F8EE9EAFF406C6ADB2E22,
	ScreenShotShare_Share_m66BF412525C3CCD017FD8E999F8794E731852E89,
	ScreenShotShare_ShareScreenshot_m246A5054CA729C909BFCC37EFD7826BDCD8CE5A1,
	ScreenShotShare__ctor_m76FF50308F01467D18A49DA80ADBD099F6BBBEEA,
	U3CShareScreenshotU3Ed__7__ctor_m272767E0AF9F078E6DCED77A092EBC7439D54C70,
	U3CShareScreenshotU3Ed__7_System_IDisposable_Dispose_mBD94F610B2DE5685FB673119CDF7237905DD2B27,
	U3CShareScreenshotU3Ed__7_MoveNext_mE87D776E207C595C06B0A9CDA7F6C9087911D70B,
	U3CShareScreenshotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFCABB45518780891A94828CA071C36CF1A36E5C4,
	U3CShareScreenshotU3Ed__7_System_Collections_IEnumerator_Reset_mBFE3415BA35E908E556C1CB1A61028BE711AB897,
	U3CShareScreenshotU3Ed__7_System_Collections_IEnumerator_get_Current_mB20376FE895FF4BAF3C18F02580F4DE96ABBE28D,
	MusicPlayer_Start_m196809CDCF89356E622F9AAA17375AC2A0F4DAB5,
	MusicPlayer_UpdateVolume_mC4DE5B277CD5443466F505D98A14754FA816B6DC,
	MusicPlayer__ctor_mA83600A144210E8586F4618ED75C614BFE6450FC,
	PlayerNameSetting_Start_m74AF951D982F204EE018879F5135FC9F01F6CC79,
	PlayerNameSetting_Update_mCDFF975992B9E4C2548CB09D098816D7BAEC626E,
	PlayerNameSetting_setUserName_mB529CC03F5ECC3167FAF427DA4802308765990DD,
	PlayerNameSetting_ResetUserName_mF265BD82F5F582A894B8F422695A779350985404,
	PlayerNameSetting__ctor_m46602F82A3957B8CAD73C02E30D3C71BE7E0B440,
	ShowUserName_Start_m473417E40D053C0DA3804D5F6B576D8A085D6C5C,
	ShowUserName_Update_m576972F642F519E619BB9722317ADC543FEF1B90,
	ShowUserName__ctor_m42C72C60BFBF2853C045D29CD02162976303C087,
	DialogText3_Start_mFCD1B78FFC3320229DFF74FBA38494231277D759,
	DialogText3_sangatBaikFunction_m720E11482673EEFC207C4A4EE4173017391A006D,
	DialogText3_kurangBaikFunction_m14095A431941002F103A8D046C0D756EBA9D63ED,
	DialogText3__ctor_m2586B91AF3A964E5A8F49B466FF009A7F1EE9AA6,
	ShowUserNameInStory_Start_mD469348304F28D75A8F946A37B752CFF5244DED4,
	ShowUserNameInStory_Update_m17F8E6D341122CC319EF219FB27FB55E1F0D81FC,
	ShowUserNameInStory__ctor_m56263559375C3354928A6562E3C5C6646FB29F3A,
	StorymanagementScript_Start_m54ACBB1633BAA46C22E3F7D03AE3D620E09C7263,
	StorymanagementScript_setUserName_mB1BDC2C5481FB40F0731BE31ADEFB56E19AB5427,
	StorymanagementScript_ResetUserName_mA7884C10679D41D623BFCE781A2434327D95CB6A,
	StorymanagementScript__ctor_mB5EBE5FDFC3670914FAB4DE6109F7164D677224A,
	loadStart_Start_m5E430157AF3A4B38EE1E04DA7B97AC027867BAAF,
	loadStart_Update_m6BBD2E3F2DFA59386BF81AB1D2ABBF4DBA937939,
	loadStart__ctor_m24BB1A8FCEA23ABF831E194250FE3B31BFE4F8FD,
	tombolStoryLewati_Start_m5921EF680B1593EE1EDE534871A69BD97D332174,
	tombolStoryLewati_Update_m608B4116A06F7E1EDF4F042EEDEF528370692D5F,
	tombolStoryLewati__ctor_mC51FCD1001F316CDCF03E2040530484760D2C79B,
	btnTest_Test_mCF7869A83B7AE27B6C7A878A855327CB7840871D,
	btnTest__ctor_m5EE8B3A0F712B8F17AE1DC62D71C57BC56AC2D0E,
	CustomSoal_Start_mBC47880AF29DC211386632CBCAF32532F4026F28,
	CustomSoal_jawaban_m22C4AA0F946C65C9FF46C27AE751B8C7ABE9B740,
	CustomSoal_Update_m0073C4D0BA1FF40D00D159D6A71066837CD8599E,
	CustomSoal__ctor_mCCB0804E9F516BDA0890D6FE4ADB89C4950F6645,
	HasilQuis_Start_m8D932902D418AB6DEEF2CF057BC1645848AF186C,
	HasilQuis_Update_m605F585D1E35CF1FF5BA8CF09D6359DFA4E24E53,
	HasilQuis__ctor_m35E837EBA8347446331B6CBCA1F01481E5C53BED,
	Randomm_Start_m114460E631D0F6BE85193526DDF4EA659532F1E5,
	Randomm_Update_m5918D3E8DFCF928167FD00444399A3CB1877FB5E,
	Randomm_RandomSoal_m0A5678454724EFCE70A04BD2E4AA44C06C2799E6,
	Randomm__ctor_m576FF3A9151ED34AAADC1B20ADBB99C099972CA6,
	openingQuis_Start_m60390CED62CE4D3F3C132C6BAA141A449FCA130F,
	openingQuis_SceneQuisOpen_mD104430B324D3BC1DEEE86ABB8A5167D644767CC,
	openingQuis__ctor_mB36E233662D1A595341AAE5D61F849155DD1124E,
	Loading_LoadScene_m1E31C730D00720B81EFBC578D3FD0C11A4D79B56,
	Loading_sceneStorySudahDibuka_m6D7B39264D158CC50CD84587D1188378301EE031,
	Loading__ctor_mE163CCF6BD5A7EDA90F61A352261F32C2DE99FFB,
	aktifkanAnimasiPlayer_PlayAnimasi_mE72F4442D762ABB8559171C3B0D1508ED82813AF,
	aktifkanAnimasiPlayer__ctor_m6FA86E01FDE0035C4A342545457F048AB544ED8D,
	arCameraOpenPremission_Start_m1126791C268C532291FE4BC5A39E4D3016319548,
	arCameraOpenPremission_Update_m14BB67B3AD2DA273F30E2AE2D0AB77918B1FA092,
	arCameraOpenPremission__ctor_m2B6CF41AAA6926494DF46384317089A03A709DCC,
	bacaTutupMateri_Start_m0502E114A0C09BBCAE995358548EA108D1DA5AA1,
	bacaTutupMateri_Update_m39718EF691286B08B92DB339B0265658EFB59571,
	bacaTutupMateri__ctor_m5CC18EC7E0F48156D11D6142A4E4935CF98C21DD,
	cekLulus_Start_m8DDB15CD052E446410A7D7B52BCCFBFC61CF158C,
	cekLulus_Update_m998ACFA6C2F27219C26E7F753313B9B40CC78162,
	cekLulus__ctor_m959BC18ED7F73C262E28261F06F86217B871084C,
	videoPlayerControler_Start_m5F789D894C6E086C351B000691D0FA5A4912E90D,
	videoPlayerControler_Update_m3F1AF2AC8196B0BCC2B9F9E6F65A2935B94DE0E3,
	videoPlayerControler__ctor_m0D00727283C93BC954117C4FCF8DB40FC7507F89,
	Detail_easyar_String_from_utf8_m2DA7CAF4C393BD0D25A8B97C5CD294A4A25006B3,
	Detail_easyar_String_from_utf8_begin_mDB4D71C0566596CB79CDC60A78E9308A78040D6F,
	Detail_easyar_String_begin_m80F1E6E27F082E51C13AF2FB7E2747B7821F9D73,
	Detail_easyar_String_end_mA908EC1799B9F8F4F974B8F08107EF2C5E70A621,
	Detail_easyar_String_copy_m9F7AF2499A7DA63113578874CBB0F5A91F983F40,
	Detail_easyar_String__dtor_m2DED6C3DD6968034E40AFAA0066CFC5BD7A048FE,
	Detail_easyar_ObjectTargetParameters__ctor_m637003D10781D34D80C3CB7D5DB203882D0094D4,
	Detail_easyar_ObjectTargetParameters_bufferDictionary_m2D457379C80C16558B9AA9032852C7299B010EEB,
	Detail_easyar_ObjectTargetParameters_setBufferDictionary_m361CE7143BF579234CB0FFF2C12420AE2CD856FB,
	Detail_easyar_ObjectTargetParameters_objPath_mB65B60D291C1E534A30F6E18A603EBA13B666ABD,
	Detail_easyar_ObjectTargetParameters_setObjPath_m8A4FECE44F4D097042CC63C0D5C9176BFE7D8924,
	Detail_easyar_ObjectTargetParameters_name_mEBC3980323799362B9221DCF32CE1D757AC2C08A,
	Detail_easyar_ObjectTargetParameters_setName_m3A7BBB751A11E47B3F45433C09095C05415E9EB4,
	Detail_easyar_ObjectTargetParameters_uid_m3F691FCAD5ED21152FB40412CC6F4D464B085135,
	Detail_easyar_ObjectTargetParameters_setUid_m69D87019EC1BF25A92B8119B8AF5FD1571E931B1,
	Detail_easyar_ObjectTargetParameters_meta_m78D60ADBF5FDEE14A7A8746025CC8E738D66377E,
	Detail_easyar_ObjectTargetParameters_setMeta_m9E19EFFBD10EE50CFEB0E94B2F0DDC6CFD6C0253,
	Detail_easyar_ObjectTargetParameters_scale_m5591D3289429860EF877BED3D550512B933209FA,
	Detail_easyar_ObjectTargetParameters_setScale_m826B60C7D3AA452FE4371B4F700FFCCFDC0EAE2A,
	Detail_easyar_ObjectTargetParameters__dtor_m60AB2D26961BDC225AD482947647717E214E780E,
	Detail_easyar_ObjectTargetParameters__retain_m8AF0772D8450F8AF6C5D0B35103A5F0C30F71D38,
	Detail_easyar_ObjectTargetParameters__typeName_m0A4DABD98CF0388AF11B16F25D9E999A685F3B04,
	Detail_easyar_ObjectTarget__ctor_m2EDDAA4703683E11267B44737BEB26D3E5646C41,
	Detail_easyar_ObjectTarget_createFromParameters_m470466EB6BF9AAD4921F47D996C62AC3A61C772A,
	Detail_easyar_ObjectTarget_createFromObjectFile_m76BBA0202F75E7C9F5F39361E143353D21F32D16,
	Detail_easyar_ObjectTarget_scale_m7E0D3C5AA15A14221D5427ABE6D960AC4B3BCE6C,
	Detail_easyar_ObjectTarget_boundingBox_mDA96465330FD5B9EA92FB95F1F1B2F71B6A419FE,
	Detail_easyar_ObjectTarget_setScale_m50F08EAC87B5EE26C9D1051819222ECEACF52F57,
	Detail_easyar_ObjectTarget_runtimeID_mF9FA8E9A2A0B0E16096B38C1A6BE54A09A0F26B0,
	Detail_easyar_ObjectTarget_uid_m6297C6C66FAB9FC7622E5A06E2E63AD7272E591C,
	Detail_easyar_ObjectTarget_name_m5A22B3B603982084D94C52A92461360DA8FEEECC,
	Detail_easyar_ObjectTarget_setName_mFE0BF726DB2173ABD89B6CEA583402A00DC386C9,
	Detail_easyar_ObjectTarget_meta_m1A7E9B2D79F1B9734827F6AC286485C3F280F0F2,
	Detail_easyar_ObjectTarget_setMeta_mD487FE98D13C41DFCDF8B183DAE2997F0FC0636B,
	Detail_easyar_ObjectTarget__dtor_m04F8976E44AB38C1A3B16CA5979D4B850238B779,
	Detail_easyar_ObjectTarget__retain_m47A9EE0292C69A5585A2BDCEB10C81A0F62AE074,
	Detail_easyar_ObjectTarget__typeName_mEBBEE04D4886FB8C2612D31B9C924150975A8FF9,
	Detail_easyar_castObjectTargetToTarget_m8FF61E65D8F79C363BCD438E12C0D702F1E42ADF,
	Detail_easyar_tryCastTargetToObjectTarget_m5B7770E0A6BD5AFF24FD45577EBCF113DB792039,
	Detail_easyar_ObjectTrackerResult_targetInstances_m8F8F0B23D489B9B33EB43457983BC56910991991,
	Detail_easyar_ObjectTrackerResult_setTargetInstances_m8C761F266081908892FD9260FD97AF90B6CC4DA1,
	Detail_easyar_ObjectTrackerResult__dtor_m3D400E997315FAF2A6C5CEECF6A3A650A329C472,
	Detail_easyar_ObjectTrackerResult__retain_m871040417045170E03972F49F22F62A6BBC67297,
	Detail_easyar_ObjectTrackerResult__typeName_m6432181585669CF6F6C34213F3D537B982A049FA,
	Detail_easyar_castObjectTrackerResultToFrameFilterResult_m1C3BFC7F8EBEB1DE6DD156CB3556FE6B8C82CD8C,
	Detail_easyar_tryCastFrameFilterResultToObjectTrackerResult_m436EB30B1F1C620F8BEAEE5377290D56A01D28AF,
	Detail_easyar_castObjectTrackerResultToTargetTrackerResult_m0C350F7F99536F8A36FEDF200C55D66D0CBFDB58,
	Detail_easyar_tryCastTargetTrackerResultToObjectTrackerResult_m1551343F131EF56787B3A79A25C2F8DE08DF53B8,
	Detail_easyar_ObjectTracker_isAvailable_m1E3C403BE9935EA51C818555DD28A19DD3BE5FC6,
	Detail_easyar_ObjectTracker_feedbackFrameSink_mCEC4367BD2973A9FB803539EB989FE294817292B,
	Detail_easyar_ObjectTracker_bufferRequirement_mC9316E49B4E5F5CEC579BD77B9A20AB8D7B3C04C,
	Detail_easyar_ObjectTracker_outputFrameSource_m62BDAA2EE46D43F570C9FCDBD1E2B35ED2A49544,
	Detail_easyar_ObjectTracker_create_mC42354BC3FA2A959BC320DD203F89B5FE6903B38,
	Detail_easyar_ObjectTracker_start_m39CFED7004F8A9AF7A4CDA674180C6C46B18D52A,
	Detail_easyar_ObjectTracker_stop_mC37B66991A81F581622F634C5633C1E678A5206F,
	Detail_easyar_ObjectTracker_close_mCF7E201075A213E8521D1A6C3B85F360BF4EA90C,
	Detail_easyar_ObjectTracker_loadTarget_mC74F26A7098B27B752B383481877FE646ABBF462,
	Detail_easyar_ObjectTracker_unloadTarget_m0BA497EE6C241FB007D56723C983EC393D30DA95,
	Detail_easyar_ObjectTracker_targets_m391902AAD98CF580B37B371D6ACBBC8ED370BDD1,
	Detail_easyar_ObjectTracker_setSimultaneousNum_mCA264512CCC0248D3AF26BF911126975DCF24D64,
	Detail_easyar_ObjectTracker_simultaneousNum_m7197D4C5E9CFE0F59865EE8B43E5C450C4025997,
	Detail_easyar_ObjectTracker__dtor_m75F8E305F1EAA92CAE90E5F686153808DAA6EF6E,
	Detail_easyar_ObjectTracker__retain_m3782C67AEECDD44345451C8B341C9072ADDB84DF,
	Detail_easyar_ObjectTracker__typeName_m9F8CD32CDC1DE76B5DE9887DB1A418B23C4D405C,
	Detail_easyar_CalibrationDownloader__ctor_m5653A935C9FEA7A8A95FAC89D17D398178A17590,
	Detail_easyar_CalibrationDownloader_download_mA2C9E0C64A35D16752A73C6BA8F6C2B4B230ADBB,
	Detail_easyar_CalibrationDownloader__dtor_m8F40F3EB22D93CC867E2426E792D9F3C8D7C3F01,
	Detail_easyar_CalibrationDownloader__retain_m8FB19D3C25874C3C96F35B259C494207D4CCB6C2,
	Detail_easyar_CalibrationDownloader__typeName_m4E4D8F4FE9B3DC49D9B7211C9DFEB6EF27E487B1,
	Detail_easyar_CloudRecognizationResult_getStatus_mA0CBB446D4AE77F95BB4F55A94960556B3E80D63,
	Detail_easyar_CloudRecognizationResult_getTarget_m96AD1AD13C9D87B517CEC285A031D6F6245D264B,
	Detail_easyar_CloudRecognizationResult_getUnknownErrorMessage_m6340BC91259268F35627F5FF5DDEBD691EE0B229,
	Detail_easyar_CloudRecognizationResult__dtor_mE0D3D9C33F87C8B14C22E282ADC056EB8B2F750D,
	Detail_easyar_CloudRecognizationResult__retain_mFE4630327ED2EFAA9806DAE1019C71C96916E891,
	Detail_easyar_CloudRecognizationResult__typeName_m0ABD21542E0F97A741116D500C821978092BC9D2,
	Detail_easyar_CloudRecognizer_isAvailable_mC76EE2FEE6C102B7E277B89899DBAF35E28D684B,
	Detail_easyar_CloudRecognizer_create_m922E0ABC6961AE8893661BFDB14965112045AD17,
	Detail_easyar_CloudRecognizer_createByCloudSecret_m15002FB5AFC5010A913C5A6A1D948923FB0377D0,
	Detail_easyar_CloudRecognizer_resolve_mD60F910D2A1BE5DD6966EFC87C9E4A9B10877AA4,
	Detail_easyar_CloudRecognizer_close_m13330A20A7CB643D3921225299FB255C42D8E7AC,
	Detail_easyar_CloudRecognizer__dtor_m1E9F71693E60211683D4AE6FA33ADB3DF837D73E,
	Detail_easyar_CloudRecognizer__retain_m6B3C33C272A25093D0F8FBB586DE31AE5EEE44B9,
	Detail_easyar_CloudRecognizer__typeName_mD4EE220D5D4170E93964BA1C253F57BF26B4DDFF,
	Detail_easyar_Buffer_wrap_m03192D7725A4F933C8E9594CCEF280F03F96043A,
	Detail_easyar_Buffer_create_mA0EC37C69901901623726466D0BD9938A6047E70,
	Detail_easyar_Buffer_data_m4A521871FF7E44394845F01D265D7D32175AFA33,
	Detail_easyar_Buffer_size_mA46A2EF329426A444A825206697FC2E984AAC3C4,
	Detail_easyar_Buffer_memoryCopy_m7D60428F5761D0FFB8BBDABAB0098C69C352BDFF,
	Detail_easyar_Buffer_tryCopyFrom_m01DABB0E2A53906DC6C51C583727B5DFBB1CC1EA,
	Detail_easyar_Buffer_tryCopyTo_mCFCA78D42FE02C740B48AF275BD9A0C0571B605D,
	Detail_easyar_Buffer_partition_m73B23B19B4264F1EDDE1957DA691A07337F48619,
	Detail_easyar_Buffer__dtor_m7D775BC77DC4D413AF93AB0D3AC9372733A54D23,
	Detail_easyar_Buffer__retain_mBCCFAFC1EBDDAE734CDC1E0FAB247DFFCE9D7350,
	Detail_easyar_Buffer__typeName_mE58A1E5A96223257916EEE5542EB539543DDE073,
	Detail_easyar_BufferDictionary__ctor_m76333B59F69D1621B74A6DF7D2D721EAB9D6B5F1,
	Detail_easyar_BufferDictionary_count_m100A7DE259532BD8FE29070E3E666ACC903966C7,
	Detail_easyar_BufferDictionary_contains_m00C0D752D50D27180AA976364FFD52574B6DFFF4,
	Detail_easyar_BufferDictionary_tryGet_mD9621CFE66A384EADCBBB9728AAE4113C26017B6,
	Detail_easyar_BufferDictionary_set_mE0D4CADCCABD4AEBFB5427489321284B74514CB9,
	Detail_easyar_BufferDictionary_remove_m3AD4EDDEA3F3F433F4F426DD32442788433F5728,
	Detail_easyar_BufferDictionary_clear_mA4F8DF88321B5B77EE6FEE71FD73E8F0734963F5,
	Detail_easyar_BufferDictionary__dtor_m197AC42D329F36167C679A6A1BC3E58E20A8E2FD,
	Detail_easyar_BufferDictionary__retain_m868AB31532852DCC7E2BBF3EACB793F361E4848E,
	Detail_easyar_BufferDictionary__typeName_mE10EFBAAA048062AE980ACE6CF1F870BA5B27902,
	Detail_easyar_BufferPool__ctor_mBAD8F4AF31F24BA677A3F5E807F988ABF4C625FF,
	Detail_easyar_BufferPool_block_size_m6CD55C63BEFB20B6399B21343B455A38B3E02884,
	Detail_easyar_BufferPool_capacity_mD2F803DCBEDACD30CA57969DFBBBDCB988235101,
	Detail_easyar_BufferPool_size_m1FE3A5C417590CDE94C8525A84170921A94049C1,
	Detail_easyar_BufferPool_tryAcquire_m6165F642E008317A90C412FA04B51FE72E656E37,
	Detail_easyar_BufferPool__dtor_mE2344B8EC0FD2FEC666B499843993F2006177509,
	Detail_easyar_BufferPool__retain_mBC255825EC10D247F276F9815E6EA4B8599234B6,
	Detail_easyar_BufferPool__typeName_m7509C755A1553F321D9CD8AC6F0D5C110FC7F5DF,
	Detail_easyar_CameraParameters__ctor_mAA215A450F4B77054881B0DAACA03D19A8B126E1,
	Detail_easyar_CameraParameters_size_m7A534D7E1A49FCFB8491958C1F72724AC41A4A66,
	Detail_easyar_CameraParameters_focalLength_m01C45DBB4D545AB63EFD2652BF0FC25A10C70198,
	Detail_easyar_CameraParameters_principalPoint_m82E6599234ADDDF64BBCDE27F5AB832E640440C8,
	Detail_easyar_CameraParameters_cameraDeviceType_m60FF26D9F0242785DB82081357FC73AD0CD27C33,
	Detail_easyar_CameraParameters_cameraOrientation_mAAE737042FB7C3AABD05172ED5A8A79A104E5D1C,
	Detail_easyar_CameraParameters_createWithDefaultIntrinsics_m40AD6FD2340AAB6B383B14E7BC48D203AA208A4D,
	Detail_easyar_CameraParameters_getResized_mA56516800D9AB0C95C154869392A1D94A69EF4D4,
	Detail_easyar_CameraParameters_imageOrientation_m3FDB4F8C3E30165F41F1DB4EDA397060B163E6C7,
	Detail_easyar_CameraParameters_imageHorizontalFlip_m879D20F6F9036BA2FDAA449F53DAFE1385160C5B,
	Detail_easyar_CameraParameters_projection_m046E0733F146345E09B4A0A451F3F5436DABBFA9,
	Detail_easyar_CameraParameters_imageProjection_m2CD5CB900916AE81CCEF3031F331B70FBAF9F7CC,
	Detail_easyar_CameraParameters_screenCoordinatesFromImageCoordinates_mB2F49481601C3B9D9A443D6F84AAEB8DA285F39E,
	Detail_easyar_CameraParameters_imageCoordinatesFromScreenCoordinates_m52004AE3E22B85CD437640501415DD8B877CD549,
	Detail_easyar_CameraParameters_equalsTo_m8494B08EDC74086C83F72C7737BCDF12FA6724D9,
	Detail_easyar_CameraParameters__dtor_mC5C64EE4AFC57ED49946AB3AEF42D6F1955BFE39,
	Detail_easyar_CameraParameters__retain_mBD8E6C8FC84ECB81491554FB7FA1140DBA7207E9,
	Detail_easyar_CameraParameters__typeName_m0A9B931AA2A9651D42CCDD6715C84A835D84EF48,
	Detail_easyar_Image__ctor_m44A471BE21CE2248820B2B20C0EBBECA50FE4997,
	Detail_easyar_Image_buffer_m97EC18D4DFB7DF285534889A34BF774ED98CA3A4,
	Detail_easyar_Image_format_m2D7C24910CC6D6691165389D0B43B165F5B1FC15,
	Detail_easyar_Image_width_m45359AFEB1DF248FB74D9E263BCF8418F7D54118,
	Detail_easyar_Image_height_m5B065D782DCF8E48ED1CD366B4E7378B90232D26,
	Detail_easyar_Image__dtor_m88CA1CF1706F83BFC0057F118A23C88D6AFD58A4,
	Detail_easyar_Image__retain_m7C0AB58E963C7E993DD48EEBAD78D3AAB27FEDC0,
	Detail_easyar_Image__typeName_m1A1A436ACAB1DEC5E45E6D981E00E42EEF8882DE,
	Detail_easyar_DenseSpatialMap_isAvailable_mAABE73FA136DCEA3EBBEBABC8DE51DB878D64727,
	Detail_easyar_DenseSpatialMap_inputFrameSink_mD5564EE366F474301A0C21BA35DE2608B54F113A,
	Detail_easyar_DenseSpatialMap_bufferRequirement_m1441A26D30934DD772A37776B499B4F87690709C,
	Detail_easyar_DenseSpatialMap_create_m7C71437D6B834C9DC752F5980460010BBC927FBE,
	Detail_easyar_DenseSpatialMap_start_mC6B54765AB2E19FC31A9289D3694122F0285A29D,
	Detail_easyar_DenseSpatialMap_stop_m11E9E6D55028812A5091261386693761B875467A,
	Detail_easyar_DenseSpatialMap_close_mB81A7395F1C8EFB1707375C05B37D17632F27C59,
	Detail_easyar_DenseSpatialMap_getMesh_mBEDAA0D457D9AD2723D42FCC7BECE3B0BE701BF0,
	Detail_easyar_DenseSpatialMap_updateSceneMesh_m6191CD770946FD9275F3160429F376D06558677F,
	Detail_easyar_DenseSpatialMap__dtor_mA76C8E31C78960C65135230A18C2BB89CD9F03A9,
	Detail_easyar_DenseSpatialMap__retain_mED37119E5C69B6172D2757E7B0CEE58E6571306B,
	Detail_easyar_DenseSpatialMap__typeName_mE2C64E053664FFBBD425FAC93E675922CB8A6D1C,
	Detail_easyar_SceneMesh_getNumOfVertexAll_m7146D06DE77DDC6B0BFBAE578407AA060B3E02A3,
	Detail_easyar_SceneMesh_getNumOfIndexAll_m9D6F8F84E7C6809D1CAC11A342F6F2057B9880BF,
	Detail_easyar_SceneMesh_getVerticesAll_m96D1BB0BA1B8DABBE84569AEB2774D43E66CFF7A,
	Detail_easyar_SceneMesh_getNormalsAll_m1F185562AF0769074DBB586AA69C45A448AA4F7E,
	Detail_easyar_SceneMesh_getIndicesAll_mCEE0580B9064316E28BAC1A1D91E78FB213345AF,
	Detail_easyar_SceneMesh_getNumOfVertexIncremental_m3DAC2EFBD191DB08C49F028C8E9155C438F82CC0,
	Detail_easyar_SceneMesh_getNumOfIndexIncremental_mFCE7C74CD9E206D767055C916EFCBD330D77FEDE,
	Detail_easyar_SceneMesh_getVerticesIncremental_mC18389F6F4702A20AF583F91AD8CF766BE64EB8B,
	Detail_easyar_SceneMesh_getNormalsIncremental_mB15201BFE2D01D7C4489F2C4A27E0A543CE8D9C1,
	Detail_easyar_SceneMesh_getIndicesIncremental_m56ABECD0C7D5277BFFBD75DB9E66AC6FA1B013CD,
	Detail_easyar_SceneMesh_getBlocksInfoIncremental_m201C45354BE5D6E3CEC5E75F5E31671C6BD720B0,
	Detail_easyar_SceneMesh_getBlockDimensionInMeters_mA2781961B708806E3B60B99B24A461CA94A2EE31,
	Detail_easyar_SceneMesh__dtor_mC025311DE470C0FD1110593EECEB01028A9D9FE0,
	Detail_easyar_SceneMesh__retain_m7BAAD5C1F051F018819C9570C491A633C4C03E56,
	Detail_easyar_SceneMesh__typeName_mD385DE632E36FA0BBAADDB6A7787B83F876E7C25,
	Detail_easyar_ARCoreCameraDevice__ctor_mD0E25F1340C204C238EBD7CA7115EF4D2A19A859,
	Detail_easyar_ARCoreCameraDevice_isAvailable_m7A20554F3DD889B92D88B709547ED0D40A8B209F,
	Detail_easyar_ARCoreCameraDevice_bufferCapacity_mC17684BDBC3C2CD405371597170FA7B58980CD1B,
	Detail_easyar_ARCoreCameraDevice_setBufferCapacity_mD3EA381C904F693B4654243972AD4996BAA6A778,
	Detail_easyar_ARCoreCameraDevice_inputFrameSource_m4D2A49B60FA5FDE7B51E37D2F939C72FB152FFD8,
	Detail_easyar_ARCoreCameraDevice_start_m204A0AB38BD80B0E705FDC8596FB99074425B06E,
	Detail_easyar_ARCoreCameraDevice_stop_m5B00A1744E508D416060699BD66A43DF32F4F89D,
	Detail_easyar_ARCoreCameraDevice_close_m4EE6ABB1636153CF2A1CF137A4FE2132975AF10A,
	Detail_easyar_ARCoreCameraDevice__dtor_mF59A3D8CB30459215F7FDDE8D9E385C98B0DF20A,
	Detail_easyar_ARCoreCameraDevice__retain_mD63517CDDD0020B7F7822495086C6768A6F6327A,
	Detail_easyar_ARCoreCameraDevice__typeName_m36C150485151F73F3CDBD12A3189F4966165E37D,
	Detail_easyar_ARKitCameraDevice__ctor_m86DB727BBD35A50964F030CC28A182C7DFA04BC0,
	Detail_easyar_ARKitCameraDevice_isAvailable_mB9C209C79F87E01D9A16E5238BB3876B9E737CA9,
	Detail_easyar_ARKitCameraDevice_bufferCapacity_m183DA6C9B9DBDA9A1C738B4C1293162254FEA5B8,
	Detail_easyar_ARKitCameraDevice_setBufferCapacity_m5D28F4726A39A1E2EF4DA002C82C08C884B14185,
	Detail_easyar_ARKitCameraDevice_inputFrameSource_m9E16719FA7CB888509E55417C971ACE74346C043,
	Detail_easyar_ARKitCameraDevice_start_m2041B2CD32F3919AB0C1DAD5438C821C9D14FE25,
	Detail_easyar_ARKitCameraDevice_stop_m6829EC000DD6BD23E69C4593303CBAAD4613836F,
	Detail_easyar_ARKitCameraDevice_close_mDEDE6B8F7C9B4DFC2E481944F6C4D3D394261929,
	Detail_easyar_ARKitCameraDevice__dtor_m9D8243E2282CC26CDE8409C926F654C30A482635,
	Detail_easyar_ARKitCameraDevice__retain_m99542F0C3CBA00CB4119EB9A24E0B36EEABFE80B,
	Detail_easyar_ARKitCameraDevice__typeName_m870F68D8D2468DAF2382071A87CDDF5222C1DA1D,
	Detail_easyar_CameraDevice__ctor_mB405C541CF4727B7107B6447A0DDE0ED4519D42F,
	Detail_easyar_CameraDevice_isAvailable_mA981320BD0FCAD60E0B61E9AE0F89160991F197C,
	Detail_easyar_CameraDevice_androidCameraApiType_m3D69DFED8EC373B9262281D0AB3CE3D754E792C7,
	Detail_easyar_CameraDevice_setAndroidCameraApiType_m1B47F2F719EAD5FCE16F6AD1D8117DE982BC7995,
	Detail_easyar_CameraDevice_bufferCapacity_m07237BC25FB91FB5967418048E5D8E1145DF231D,
	Detail_easyar_CameraDevice_setBufferCapacity_m038DC508F2EE5D8ADC18903DB3FD2C545F148BB7,
	Detail_easyar_CameraDevice_inputFrameSource_mFDF8E18D2290A71D0AC87647848F0E939CB0474F,
	Detail_easyar_CameraDevice_setStateChangedCallback_mFEAFB69C164BF4773F28CA25AEE348846AE37A4F,
	Detail_easyar_CameraDevice_requestPermissions_m0B0085346DAC92BFB7AF8A9F85713157F91CDA21,
	Detail_easyar_CameraDevice_cameraCount_mD57207D600584A13C3D9C1B07E75F6811D251624,
	Detail_easyar_CameraDevice_openWithIndex_m4073550678E1FCEB99D97E2397C914798B821133,
	Detail_easyar_CameraDevice_openWithSpecificType_mF3B2320DDCAC228770AEA01BDF5478DA8CC556B5,
	Detail_easyar_CameraDevice_openWithPreferredType_mB956E4CB4A931E84FC6ABDD69661DE54449CD948,
	Detail_easyar_CameraDevice_start_m960EA265AF0BE266722D3BD009C14E7D8225CEF3,
	Detail_easyar_CameraDevice_stop_m366C4F59C27B36EA4396F079E9303A294010F4AA,
	Detail_easyar_CameraDevice_close_m9836A272E51099812771C1283AFC7B91FD7CD7C9,
	Detail_easyar_CameraDevice_index_m8F498A0B1FAEA6C122A35F9D8240881FC1632999,
	Detail_easyar_CameraDevice_type_m9E6DC493DB342DEA32EB947273FAF2C5E5475E01,
	Detail_easyar_CameraDevice_cameraParameters_m1387A96109B6BBFBD990DE6D6C3132A5EECBF2CD,
	Detail_easyar_CameraDevice_setCameraParameters_m96FB515B58EE378AFFC70D9A102FAA3890495A91,
	Detail_easyar_CameraDevice_size_mD134A37CAD76E1636E69FDA95CDE5E0F39B37499,
	Detail_easyar_CameraDevice_supportedSizeCount_mEDFA70CE8ADB04288E8637A45950CB4A71642CBF,
	Detail_easyar_CameraDevice_supportedSize_m201FC7AF4AD527208CB4B601C66F97F4E8E2D679,
	Detail_easyar_CameraDevice_setSize_mEF8C1866B2536CEB2F3CBF37BC212736760A93BE,
	Detail_easyar_CameraDevice_supportedFrameRateRangeCount_mF01B0EB632E1FCB58EB820347A7580F952164364,
	Detail_easyar_CameraDevice_supportedFrameRateRangeLower_mE7B65520F6C6A078086320F1E37DD656FE227791,
	Detail_easyar_CameraDevice_supportedFrameRateRangeUpper_m4DAD75C7158067F7671BCD5F60A0D0C573DC95DD,
	Detail_easyar_CameraDevice_frameRateRange_m51DF0304024EE5E4300BA565E93E15FA96995EF4,
	Detail_easyar_CameraDevice_setFrameRateRange_m9344A9BA4713FFB9F28B15BE534D37F9C7EDC5A3,
	Detail_easyar_CameraDevice_setFlashTorchMode_m3527FF29F6EB7DC46BBE6116F734123DEBE4D675,
	Detail_easyar_CameraDevice_setFocusMode_mFE26FB8E92420728952E476D40E413B75A571328,
	Detail_easyar_CameraDevice_autoFocus_m5A1A762266717912EBF4864AAFA9802F199F1839,
	Detail_easyar_CameraDevice__dtor_mFD2E20EFA823BCD671E9D617D03223341D8917A0,
	Detail_easyar_CameraDevice__retain_mF5CE38212B060044A5EC6DBCD98ACB7E39A9D90D,
	Detail_easyar_CameraDevice__typeName_mF55756C09B1228F8E189673858CF7668CB480494,
	Detail_easyar_CameraDeviceSelector_getAndroidCameraApiType_mA486C42BB7E399900705043EC168923BD2EDD836,
	Detail_easyar_CameraDeviceSelector_createCameraDevice_m6C66003EE767694DB312A06455FA8A8E14C3AEE2,
	Detail_easyar_CameraDeviceSelector_getFocusMode_mFC2F7BFB64FEE71FE7555F68039ED142E9BE433D,
	Detail_easyar_SurfaceTrackerResult_transform_m6D4768FCC861984CECD1F5C642406DDE22FC4680,
	Detail_easyar_SurfaceTrackerResult__dtor_mEE66403D4D611D169E7CD292219E485780D08200,
	Detail_easyar_SurfaceTrackerResult__retain_mB7EE5CA1F170B0B98CB0D40594E81D38D9B71EAE,
	Detail_easyar_SurfaceTrackerResult__typeName_m58D260C01BD13BF53E190200CE9AD10AED5C99D8,
	Detail_easyar_castSurfaceTrackerResultToFrameFilterResult_mA006B3D8F339F9E51DC2B1B7C1E77F54B63AAFC7,
	Detail_easyar_tryCastFrameFilterResultToSurfaceTrackerResult_m72C70FFCA274B1792A53F4B6C47F741F3521EB2E,
	Detail_easyar_SurfaceTracker_isAvailable_mBA249973FA782ACBB1ABC5CCE5C7D1AC9F032073,
	Detail_easyar_SurfaceTracker_inputFrameSink_m67B09A786EEB1C0393C169053807E5645DC06970,
	Detail_easyar_SurfaceTracker_bufferRequirement_mC37903CCF5F9C80996833585F116699783BEABF6,
	Detail_easyar_SurfaceTracker_outputFrameSource_mE18F6A531B109BC80128815B72BF402C44BCD2BE,
	Detail_easyar_SurfaceTracker_create_mBFF3374FD2EFAF562D40022121828F90D5F007DB,
	Detail_easyar_SurfaceTracker_start_m730AE242ACDF7B8B5CDEF6827C8431AC43659475,
	Detail_easyar_SurfaceTracker_stop_m51E2D6D19F28986AB22610F4AAC119FF1D9BE715,
	Detail_easyar_SurfaceTracker_close_mED0BB4FD96C4079C9C92454FAFD5DD3F1C9A0D4E,
	Detail_easyar_SurfaceTracker_alignTargetToCameraImagePoint_m9A24550BD5984AA4497390F1CEAD4E4B08FA52E1,
	Detail_easyar_SurfaceTracker__dtor_m982521FEB1B05367B07A569632640FE80E485799,
	Detail_easyar_SurfaceTracker__retain_m989AA65BBBE31C3FFC751F6B5040F54ADC02D34E,
	Detail_easyar_SurfaceTracker__typeName_mC41CC41D9CE19BA9FBAF6D3661FB289680D3D303,
	Detail_easyar_MotionTrackerCameraDevice__ctor_m6CAC90B6251117DE20D92F11F84745C36563F31D,
	Detail_easyar_MotionTrackerCameraDevice_isAvailable_m55C63DE5CC04307E314BC84319CD6FBA797C6943,
	Detail_easyar_MotionTrackerCameraDevice_getQualityLevel_m519ECCF3D076A17F090BF9ED2985C3157EE51E04,
	Detail_easyar_MotionTrackerCameraDevice_setFrameRateType_mCE673716577DE0B7D8327F4E59AEA2A83A541B86,
	Detail_easyar_MotionTrackerCameraDevice_setFocusMode_mC68112FCEF401780EDEBC886A5D4B713C27D2988,
	Detail_easyar_MotionTrackerCameraDevice_setFrameResolutionType_m96EC8E6E4F95F351D557B7F961740DE140A1676A,
	Detail_easyar_MotionTrackerCameraDevice_setTrackingMode_m23DCB25ADC3BA479129AEF84EFC05C164B4AE555,
	Detail_easyar_MotionTrackerCameraDevice_setBufferCapacity_m728C01BC05C3B8A03F5599864DDC1EAC205BDF18,
	Detail_easyar_MotionTrackerCameraDevice_bufferCapacity_m2D2F0AC88FA2C5B2AC19AAF4663B2D9AAB19373F,
	Detail_easyar_MotionTrackerCameraDevice_inputFrameSource_m9C835F6E423C0899A0FEBAE196D5DDBEE0DB5A26,
	Detail_easyar_MotionTrackerCameraDevice_start_m19C6583624F012F051B994A60C2F5A35DAF9D814,
	Detail_easyar_MotionTrackerCameraDevice_stop_mDBA64624E2BB2B48D3D82CDFF58761428430DCB1,
	Detail_easyar_MotionTrackerCameraDevice_close_m983F8EAFFEB084FC2DEFCEC5E139A2D3E0EBCB39,
	Detail_easyar_MotionTrackerCameraDevice_hitTestAgainstPointCloud_mADE44C2D8A6585D2332FCB7757CBCF256C7EDAA9,
	Detail_easyar_MotionTrackerCameraDevice_hitTestAgainstHorizontalPlane_m39258207EC4E55FB29DD892E0914173B596788D8,
	Detail_easyar_MotionTrackerCameraDevice_getLocalPointsCloud_mC68EA7E86F505C042CA1ED1EA8D02DF328644685,
	Detail_easyar_MotionTrackerCameraDevice__dtor_mC7FCCB20D8571C4305260D2A320A71DBD91772C0,
	Detail_easyar_MotionTrackerCameraDevice__retain_m0FA38323C0918FB6CB7C263E2FD0FFE3FBCC37C0,
	Detail_easyar_MotionTrackerCameraDevice__typeName_m1C756B0684C70B1B5A00B68F2A54700F4F18D94A,
	Detail_easyar_InputFrameRecorder_input_mBEB513D8F9314E43C1282F90A8FABD982CF7F076,
	Detail_easyar_InputFrameRecorder_bufferRequirement_mA086537AD808ED3532744AECF24CC1E0FC6271AC,
	Detail_easyar_InputFrameRecorder_output_mC44495B86E433BB1ECFA61A8E5B5DA604B8CA3D7,
	Detail_easyar_InputFrameRecorder_create_m686864C821DC3EC89D8A32C424EB5ECE8313C212,
	Detail_easyar_InputFrameRecorder_start_m04BBFDC50C6F94806DD23031CFA15E73E00016CC,
	Detail_easyar_InputFrameRecorder_stop_m028E67230F996DA136BDF8353ABFC4905DDF3970,
	Detail_easyar_InputFrameRecorder__dtor_m7130C3D430866640ED4F23387B9B5EC34966A363,
	Detail_easyar_InputFrameRecorder__retain_m600C7B382D3DE54821EDBE53FD2A1B266E653BF1,
	Detail_easyar_InputFrameRecorder__typeName_m444B7BAE02641E4D13249CD5DD58723CDFD40A2B,
	Detail_easyar_InputFramePlayer_output_mA0677C2D76C833F66489C16A558BFE8EB0095639,
	Detail_easyar_InputFramePlayer_create_mB67CC5EA5ECB16A8CC2791ABEA3CDE91EE01D4D5,
	Detail_easyar_InputFramePlayer_start_mA67B099F2B54F4D8C00C2F28588D72A32879CB21,
	Detail_easyar_InputFramePlayer_stop_mF90457A2CD97CA08993F0CD3CADC2DEB94729F8B,
	Detail_easyar_InputFramePlayer_pause_m13D23B49180F2953E002BD04832EAB662EC3F673,
	Detail_easyar_InputFramePlayer_resume_mDB57C847FA7BDF8602EDD49D2BC6EDB4F93B6A5B,
	Detail_easyar_InputFramePlayer_totalTime_m4435576D974E4528C367374F60A33587D57AAAFC,
	Detail_easyar_InputFramePlayer_currentTime_m1534A0F0EC4A6FA5F45E5CBE6BA52DEE9386F3C4,
	Detail_easyar_InputFramePlayer_initalScreenRotation_m9486226AC2D6594392615FB44CF3A8590D55A19C,
	Detail_easyar_InputFramePlayer_isCompleted_m4A73C2541210A781BA857EC88988A2D41B5407E1,
	Detail_easyar_InputFramePlayer__dtor_m1ADD1CA37CD9C05F797878CBDF32BF0BCDFE3CC3,
	Detail_easyar_InputFramePlayer__retain_mEFA522C2E75E4B658001122549E2BA3DFA26780D,
	Detail_easyar_InputFramePlayer__typeName_m4FD18FD1009B83A0E8645A95D1062C99B98214D5,
	Detail_easyar_CallbackScheduler__dtor_m684352FC0B0167FC5B85E91CF5B4BBBA44CCA1A1,
	Detail_easyar_CallbackScheduler__retain_m0EC514E939119FA3047090AA8545DF35547F06F8,
	Detail_easyar_CallbackScheduler__typeName_mBF2A950D049126E3EFD1000BB9B0ADD7E3DD4FB1,
	Detail_easyar_DelayedCallbackScheduler__ctor_mBE6B4CC0B10CA546A0DADC47C0A696CF4A3D94E1,
	Detail_easyar_DelayedCallbackScheduler_runOne_mBCDE38C512F70FC3FB9FBC89873B4603EBDA1FC0,
	Detail_easyar_DelayedCallbackScheduler__dtor_m07B96B20830AAC9882F8B8F47EC2100CF3150FA8,
	Detail_easyar_DelayedCallbackScheduler__retain_m0B6DA117F0F5A4795BDE11E49FA98FF112067A4B,
	Detail_easyar_DelayedCallbackScheduler__typeName_m236DE9591BEF48F199261D9B8FB6E1CCC8572233,
	Detail_easyar_castDelayedCallbackSchedulerToCallbackScheduler_m0060229DEC0711FFD8732005E154D946D2A6DE2D,
	Detail_easyar_tryCastCallbackSchedulerToDelayedCallbackScheduler_mE6AB2B617A2A07F90EFA8EDDC002CAE65CABC4AD,
	Detail_easyar_ImmediateCallbackScheduler_getDefault_mE6D1E97D7947162FE757124F0D479753DBF30A5F,
	Detail_easyar_ImmediateCallbackScheduler__dtor_mCCD9D47AAC9BEE9C92AE1251060B6F126588DBB6,
	Detail_easyar_ImmediateCallbackScheduler__retain_m3F9F8F0AD545EBC2C64D7B492DB6179D92D466CD,
	Detail_easyar_ImmediateCallbackScheduler__typeName_m64622B1248D85297D2EF633DBE301B32326DD3B3,
	Detail_easyar_castImmediateCallbackSchedulerToCallbackScheduler_m97C104D04CBA8660F7D844185F90F60F57786B24,
	Detail_easyar_tryCastCallbackSchedulerToImmediateCallbackScheduler_mC1C86E6289AE80F2649853564E676790EF450F5D,
	Detail_easyar_JniUtility_wrapByteArray_mEE60CE941623DDB8C72367874AFF01CCEF11CC55,
	Detail_easyar_JniUtility_wrapBuffer_m84CF87BE545230D4334EF7B818AE9A049F5B30B8,
	Detail_easyar_JniUtility_getDirectBufferAddress_m93D341139E763CB641476BB55E8E7AC48F477F6C,
	Detail_easyar_Log_setLogFunc_m569769815FBF7CC1F961D7E8E8D03131119D5E63,
	Detail_easyar_Log_setLogFuncWithScheduler_m4069E258D639C2F41D29C74A35C451BD26980FCE,
	Detail_easyar_Log_resetLogFunc_m677D66DDFADBAC6B4CCB3CAB54538C547EA2C64C,
	Detail_easyar_Storage_setAssetDirPath_m5B18609ABCBDE6DCCC8551CEFE33A52E9D429D79,
	Detail_easyar_ImageTargetParameters__ctor_m0FD27677B99B1027C9C6B9CB06637C04FE9D2054,
	Detail_easyar_ImageTargetParameters_image_mFC22D31662AD505C78D620AC85E0230E56B735DF,
	Detail_easyar_ImageTargetParameters_setImage_m65C905D9310FF108FB23E34587016F532F420EDD,
	Detail_easyar_ImageTargetParameters_name_mB52F62216DE6573A86B558910B2A85E1BF618DD6,
	Detail_easyar_ImageTargetParameters_setName_m6DCBA29AEC9301D3E75FF1F1D698EE82C102093D,
	Detail_easyar_ImageTargetParameters_uid_m332228BD543BD3DA50BA960135D67F9DC9A373C2,
	Detail_easyar_ImageTargetParameters_setUid_mBB755087E4E95CDEF7A13296BF47D4B4E857F9DA,
	Detail_easyar_ImageTargetParameters_meta_mA2EAC3A3CA689D3F892F7493657E481EC1EB9B7F,
	Detail_easyar_ImageTargetParameters_setMeta_m4C106F0F3F51175DA9B1315D9CF1750895AA9D0C,
	Detail_easyar_ImageTargetParameters_scale_mFCBFE00863217B61B1BC2298F3590BC616AB40A5,
	Detail_easyar_ImageTargetParameters_setScale_m0D22E88804FA5BAB387824E894668191BCFF1EDC,
	Detail_easyar_ImageTargetParameters__dtor_m3EDFAD17DAE6D2AD58DC6077E47FE9DD043A697C,
	Detail_easyar_ImageTargetParameters__retain_m11576149B985B35F02B2F470166A7E18CED12685,
	Detail_easyar_ImageTargetParameters__typeName_mB60D034F827A9EC0F5F68AF39C57C67AD0342ACC,
	Detail_easyar_ImageTarget__ctor_m0A6D9681AC02EFF28EE0496ECE5654DAF6B54E00,
	Detail_easyar_ImageTarget_createFromParameters_m5CB26DBCD0CF96F2FB0E65A991BFEAEF323827ED,
	Detail_easyar_ImageTarget_createFromTargetFile_mD4078BCCEDC80E69BB0B19B8E1D1F2A8EB9BF3C5,
	Detail_easyar_ImageTarget_createFromTargetData_mFC343E6A14B73670FDF90B58876310E7BDB1C80E,
	Detail_easyar_ImageTarget_save_m479F63B3F9A3FF9939693CF2324179444E92F8CF,
	Detail_easyar_ImageTarget_createFromImageFile_mBE2239162C93CDCD6F99EFFA249A02FBA104FB44,
	Detail_easyar_ImageTarget_scale_m409DEBB5D1D16F02CC66B531F164EBF652E16BC0,
	Detail_easyar_ImageTarget_aspectRatio_mA433065636D99050C732D168F505253D12E2F4C4,
	Detail_easyar_ImageTarget_setScale_mBF3F603817936A58699F859A0882A4D40C5D7DB0,
	Detail_easyar_ImageTarget_images_m09CE7654B246725DD07EC1F3AA92DEF50D5A612A,
	Detail_easyar_ImageTarget_runtimeID_mAB4E845C7795CCF7ED85E746CDC9065175AA1D98,
	Detail_easyar_ImageTarget_uid_mA8173C56ECBC44D2E93BEC608F3E3B9A56F6C480,
	Detail_easyar_ImageTarget_name_m0C1E5D8FEECCEF4E3B0F3729E094D26614B69776,
	Detail_easyar_ImageTarget_setName_mF8DCDCBB0684ADE2FE3FC02F8C575AE95CAAAAD8,
	Detail_easyar_ImageTarget_meta_m09437F1A16D9DCF26AAE3FC5456CE4B1DDFA2A39,
	Detail_easyar_ImageTarget_setMeta_m89E3CA09FA56941DEC74C5E15089039F85933AF4,
	Detail_easyar_ImageTarget__dtor_mDBA6E7F38A80CD7B65C1F45F3836BB6AC3DCBB02,
	Detail_easyar_ImageTarget__retain_m46AF17AF6C17F39ACB58E24484208D5143BA6718,
	Detail_easyar_ImageTarget__typeName_m0F5B48F3598E4A49CF3D4115A837B2EFB69C8C9A,
	Detail_easyar_castImageTargetToTarget_m7F4128D5FA318D391B3D23C3E661E0ABA6A205DF,
	Detail_easyar_tryCastTargetToImageTarget_m3049137DA10F63138107F6CF004A0B08698CD6F0,
	Detail_easyar_ImageTrackerResult_targetInstances_m63558BB4F19A4EDD4AF92B0F87BD34934195C44A,
	Detail_easyar_ImageTrackerResult_setTargetInstances_mCB517A260A368F68E46FCBEC7925592F265FE8AA,
	Detail_easyar_ImageTrackerResult__dtor_mD002F891E531F62C9D76910B6140AA85D233C011,
	Detail_easyar_ImageTrackerResult__retain_m6E2FEA1B58E5C73F20812DEC7901E4F2860F4C15,
	Detail_easyar_ImageTrackerResult__typeName_m176C4305D7121D8F0EF63AFD372859E1B95448A3,
	Detail_easyar_castImageTrackerResultToFrameFilterResult_m334E3E97A78E3B16C329243193EB5E7D25281876,
	Detail_easyar_tryCastFrameFilterResultToImageTrackerResult_m213E1A5C01318166BC6E6170177F51F9CC95F739,
	Detail_easyar_castImageTrackerResultToTargetTrackerResult_mA431BA21BF4A2AD5E392D9780814E76904663283,
	Detail_easyar_tryCastTargetTrackerResultToImageTrackerResult_m1AC368B01616FAE615F782779D66484B2D58C3D7,
	Detail_easyar_ImageTracker_isAvailable_mE80A18D690EF0A78E1EB23A14CF503273C5925A7,
	Detail_easyar_ImageTracker_feedbackFrameSink_m63B655DDCC8B577E5B8E505E93AE05284497738E,
	Detail_easyar_ImageTracker_bufferRequirement_m7AFE49C1D96DC67B11A4CF7C3615C74C6BDBAABF,
	Detail_easyar_ImageTracker_outputFrameSource_m30B4CDABDA48DC4DC270177C6B0260F8FC7FF1EA,
	Detail_easyar_ImageTracker_create_m4C3D0862BFA18DD95221488B84D3E4C7F26CA596,
	Detail_easyar_ImageTracker_createWithMode_mC082919C46FB3B330FF3D79817692B3945A007E9,
	Detail_easyar_ImageTracker_start_m2D69BB4E7851277B995D4CA049703995FC3EFBF5,
	Detail_easyar_ImageTracker_stop_m460D9E4FCD73D32E978372C3C54A5F32C54405D7,
	Detail_easyar_ImageTracker_close_m90E992BE9A139911D536566643F057B6B5DF1197,
	Detail_easyar_ImageTracker_loadTarget_m738ED9839F9F3C0A6E5C124E2712BE181014D045,
	Detail_easyar_ImageTracker_unloadTarget_m8D384CEDC2BDA5B6647729D354560430A3BA70BB,
	Detail_easyar_ImageTracker_targets_m3CB2F552B0A2223541AAE00A14745C499A5DB9A6,
	Detail_easyar_ImageTracker_setSimultaneousNum_m8772322A8E75DC4ECF01A3000515A4AD7DB77742,
	Detail_easyar_ImageTracker_simultaneousNum_mD5B45A136B234CDD7F9BA096E51DFECD219A423F,
	Detail_easyar_ImageTracker__dtor_mA0E4CB6DD08894DD88D0CA6AF236F15F3ED0F9AD,
	Detail_easyar_ImageTracker__retain_mB779C69D6AF8E6280B867877E88BCB0D71B41BF4,
	Detail_easyar_ImageTracker__typeName_m50D975B4B6295076955DE693D7F5B211C31922D8,
	Detail_easyar_Recorder_isAvailable_m664DE466561E56522DDF21AD391A07AEDF79F81E,
	Detail_easyar_Recorder_requestPermissions_mA51414AF0CB31D8DFDF0BC6C24826AA9A8B38B94,
	Detail_easyar_Recorder_create_m9A1A191031C52AA367BB0FE4A32408953D24A336,
	Detail_easyar_Recorder_start_m4B750E1F4DFE6739C9CC3790DF520F2007426BB0,
	Detail_easyar_Recorder_updateFrame_m2FE475EF987E691FD62BC5D8EA5578E2B91F73E0,
	Detail_easyar_Recorder_stop_m88B9CCF0F3A833BD15CDC83DC11244CF29E3B6A8,
	Detail_easyar_Recorder__dtor_m3DE91B8C335D3DDB51C1B0ACC23FB22BD0C1082D,
	Detail_easyar_Recorder__retain_mD4C9BB52F95124213CBF97CCC404E0A4F04008CE,
	Detail_easyar_Recorder__typeName_m646AC8DF6B53BB671F43015026DC8CC1F889434F,
	Detail_easyar_RecorderConfiguration__ctor_m2D1D7531379DAEA2A65F56A86E38B1B75EFC766D,
	Detail_easyar_RecorderConfiguration_setOutputFile_mA522D96C033D7394EEB822C26E14A5A5F45ADF2C,
	Detail_easyar_RecorderConfiguration_setProfile_m217D189F131577EF27A05646124F94276DDA9E2F,
	Detail_easyar_RecorderConfiguration_setVideoSize_mFD2835BF0AC73927758FD9EF089D77554ED96058,
	Detail_easyar_RecorderConfiguration_setVideoBitrate_m91E055692D96998FAB1E637168160817383727BB,
	Detail_easyar_RecorderConfiguration_setChannelCount_m3B80E50D176A151028299D9BC40FEFF531DCAE55,
	Detail_easyar_RecorderConfiguration_setAudioSampleRate_m4CD99E54D4133D1BD5628F8A2074FC69126B9FCB,
	Detail_easyar_RecorderConfiguration_setAudioBitrate_m889B95DB08E6C109D17F3EEA66589C0E24536C03,
	Detail_easyar_RecorderConfiguration_setVideoOrientation_mCE238E2C49AC7BAB4E4B18855D2F95573DAB219D,
	Detail_easyar_RecorderConfiguration_setZoomMode_m7349296E56F4F14EB28A5964C57BF2D14D3F830E,
	Detail_easyar_RecorderConfiguration__dtor_mA9BBE3ED4BBDF40CB61A0E1594C4674F81FE4CD7,
	Detail_easyar_RecorderConfiguration__retain_mC3E60F419C9C5F9F024FCB0B2ED09F477F14ACC3,
	Detail_easyar_RecorderConfiguration__typeName_m09451E3F5FE2BEED1F0324F56A53DA65261A8A16,
	Detail_easyar_SparseSpatialMapResult_getMotionTrackingStatus_m813F2076B9E681A6E9D51493592D1D4C48EFA3D3,
	Detail_easyar_SparseSpatialMapResult_getVioPose_mDAFF6109B2FF8EF9934E0E85B1215CEC791CD181,
	Detail_easyar_SparseSpatialMapResult_getMapPose_m6311CEF9E66020A46570452D6A30695D3AEF96AC,
	Detail_easyar_SparseSpatialMapResult_getLocalizationStatus_mDB4EABA1E1A0AE880401408F51903ED155ACEB2A,
	Detail_easyar_SparseSpatialMapResult_getLocalizationMapID_m5950109E0BCF79E53B81BB5F30D8106B19F913AC,
	Detail_easyar_SparseSpatialMapResult__dtor_m3FB4DD378D7AAECEB06B1AB03ED64E7A1CA1C63E,
	Detail_easyar_SparseSpatialMapResult__retain_m6902EB74CB83188E98FDA0900B1F65A6FADBCABA,
	Detail_easyar_SparseSpatialMapResult__typeName_mF5394558BEDFC5935F72B0EB075C50C7DACE9DFA,
	Detail_easyar_castSparseSpatialMapResultToFrameFilterResult_m4C9FC0AECAD1F5E5F846D3A14913C965AFD04BF2,
	Detail_easyar_tryCastFrameFilterResultToSparseSpatialMapResult_mA8B38112B54B2131A5B425E706D7A73AD97D9C15,
	Detail_easyar_PlaneData__ctor_m325596FF63EBA7F8E7B62BA3D607B773BF285F81,
	Detail_easyar_PlaneData_getType_m3CD683FEFC482911E1067232CDBE514DCF20BA16,
	Detail_easyar_PlaneData_getPose_m543779FB85FD6B84822E4044943CB1707A405C1D,
	Detail_easyar_PlaneData_getExtentX_mD053831371BACDC93864C6625954A7B65FB0788F,
	Detail_easyar_PlaneData_getExtentZ_m1828227B7F77C462417F6A755295781E4C5D9E58,
	Detail_easyar_PlaneData__dtor_mD777D5B7C77646F4B8F87ED1537E04631EF402E7,
	Detail_easyar_PlaneData__retain_mE6F9CB15BE162EA8E7B052712B36359E9B55F0EA,
	Detail_easyar_PlaneData__typeName_mE0A5C4AE29D9BD2DC9445742C9C8F8F33065927F,
	Detail_easyar_SparseSpatialMapConfig__ctor_m8FE2EA52B927EA866348F8E455A7BD73287DE87C,
	Detail_easyar_SparseSpatialMapConfig_setLocalizationMode_m5ECCA9A568F3BCDF58045B3355B25EEC790184B6,
	Detail_easyar_SparseSpatialMapConfig_getLocalizationMode_m96582C5B09288BDC0BA847EEAD5485A85026B50F,
	Detail_easyar_SparseSpatialMapConfig__dtor_m4B6C9C47592C691DE16186CDC65D50F1BAC24A09,
	Detail_easyar_SparseSpatialMapConfig__retain_mE0E7C67A1B16ABE21C7FAEEC07421FC2C1904B7D,
	Detail_easyar_SparseSpatialMapConfig__typeName_mAB0AFB14DC85D65E8B0659D740CC714E55169A34,
	Detail_easyar_SparseSpatialMap_isAvailable_mBE88A9C77F56DAA56E0EBB0DC36108280B721BCA,
	Detail_easyar_SparseSpatialMap_inputFrameSink_m6123671A597EE737A8D60853D2D6AF1F9806D273,
	Detail_easyar_SparseSpatialMap_bufferRequirement_mF95D4C7CD1541ADE6C56005E9CEAA3DAC42EEAE7,
	Detail_easyar_SparseSpatialMap_outputFrameSource_mD95B0385931DA3FC32BA1CA433AAB872BD409714,
	Detail_easyar_SparseSpatialMap_create_m91EF53FA93EF300F21F9E7A342AB947B100B6EE9,
	Detail_easyar_SparseSpatialMap_start_mC6C209FC143F1D229E089C81573A5808C2EAD490,
	Detail_easyar_SparseSpatialMap_stop_m0D68D0162C4F58C1D9CEFD1238F5E5424DF6E44A,
	Detail_easyar_SparseSpatialMap_close_mA546298717C23B7869071093470324B26ED415F7,
	Detail_easyar_SparseSpatialMap_getPointCloudBuffer_m43AAB8D38806F7456A258C12D66EB39CD280B088,
	Detail_easyar_SparseSpatialMap_getMapPlanes_mF40833922861EE9E5FB294F28CCBE735D11875A7,
	Detail_easyar_SparseSpatialMap_hitTestAgainstPointCloud_mA92FA96AA8B906477E52836B66CC4E479D067045,
	Detail_easyar_SparseSpatialMap_hitTestAgainstPlanes_m4047B195AB754260E4E7D1374DE431AC684C2299,
	Detail_easyar_SparseSpatialMap_getMapVersion_mF4D1A3FBD50F7A5F54336C690C1D997F8D13833E,
	Detail_easyar_SparseSpatialMap_unloadMap_mB867BDDA2B2107FEE76E1E06415A375DB7AAFB96,
	Detail_easyar_SparseSpatialMap_setConfig_m690BDC6EFA97833958B4504331BDC61AEED086C9,
	Detail_easyar_SparseSpatialMap_getConfig_mBACFEEF54D61FA68039B813D5DC9C962D77C0704,
	Detail_easyar_SparseSpatialMap_startLocalization_mBF0DF641E51184A7883DCEC1B48358A1AE4B11AB,
	Detail_easyar_SparseSpatialMap_stopLocalization_mF7A0C967CA59B8D5B370B9BAE85C55DF87A4FBD4,
	Detail_easyar_SparseSpatialMap__dtor_mE8BA0180D02F0CBA277811E4885DDD0FB9930AAF,
	Detail_easyar_SparseSpatialMap__retain_m4AA707A843149A40F03DA6576268FB41D58702C1,
	Detail_easyar_SparseSpatialMap__typeName_m5AB42FBA95109D309FFC6F0A6156012BBF640BF1,
	Detail_easyar_SparseSpatialMapManager_isAvailable_mC81BE288E35E33F622EC716781CD0B7664F075C1,
	Detail_easyar_SparseSpatialMapManager_create_m37320AA1B24D95A35EE33F2A4286EF9AF0A61714,
	Detail_easyar_SparseSpatialMapManager_host_m05E49EB4E36BEFE9B4CCFD77E81A3219C5AFEEA3,
	Detail_easyar_SparseSpatialMapManager_load_m792234D212DB39489A6948AF849D378D64C7E455,
	Detail_easyar_SparseSpatialMapManager_clear_m867396C59E30B6AB407B928C591A677D2F11B051,
	Detail_easyar_SparseSpatialMapManager__dtor_mCE7EE8CECA084FD20604ED9DE960916B8CF3EB91,
	Detail_easyar_SparseSpatialMapManager__retain_m707EBC3ECC912051E6D2A4D60312B61959901468,
	Detail_easyar_SparseSpatialMapManager__typeName_m401C66DEFE58495C037B0985D2C1042C4FE504DC,
	Detail_easyar_Engine_schemaHash_m6F578A82F8311649E6F8CA600B3298928FCCC125,
	Detail_easyar_Engine_initialize_m2808284C3CCD0E5F10C71280241A8F8131EFF9E8,
	Detail_easyar_Engine_onPause_mFE24A1A0C398659CE6E89C79AE5E8140150E8B22,
	Detail_easyar_Engine_onResume_mB6F7F9B41F6B71CF8468A05222D01C36E66291E4,
	Detail_easyar_Engine_errorMessage_mC7F773F6F9EC6E986C59001EF96DB017E08F07B2,
	Detail_easyar_Engine_versionString_m9AFAB4AC12DB90CCEA9AC11F63CDBFAC6507AACC,
	Detail_easyar_Engine_name_m4FCAB81F099D97EF10AFF5D9CC100CA7246BF910,
	Detail_easyar_VideoPlayer__ctor_m7DEEF39BCDF82BFA1EBFD2C4947281C4487AA0AF,
	Detail_easyar_VideoPlayer_isAvailable_m247E99F08701BF16125C14E26BABBAADD68D40A3,
	Detail_easyar_VideoPlayer_setVideoType_m660F8600937071ED11FB5346ECFB78F6A37D839D,
	Detail_easyar_VideoPlayer_setRenderTexture_m916212678B69459C08516D2657FD603ED20CA5BB,
	Detail_easyar_VideoPlayer_open_m8BAA9EA44B7186C1915D2A528FFCB0FC1AAB5563,
	Detail_easyar_VideoPlayer_close_m5919A001327D2CBF0145D937C00D44F03810D1A8,
	Detail_easyar_VideoPlayer_play_m0CB1CCF485DFE61B7FC664CD0CF319AAA9B86413,
	Detail_easyar_VideoPlayer_stop_m0C39C35D78CF0DC2F7110E4099A1E1DF601FB00C,
	Detail_easyar_VideoPlayer_pause_m38F54B89E7A8E23C3FF4D871D94A69D06F5C0B08,
	Detail_easyar_VideoPlayer_isRenderTextureAvailable_m7C8682F7D18949CC4A07411654FAE41E32C53B86,
	Detail_easyar_VideoPlayer_updateFrame_mE170AB115613CD74D0E6A02FEDA45241E3AB181A,
	Detail_easyar_VideoPlayer_duration_m3B9168D0CA8DF29ECBECEC54AE46B671382A5233,
	Detail_easyar_VideoPlayer_currentPosition_mCD83B3C1F52EAD0E6BBC0197F6D687B30A287E2A,
	Detail_easyar_VideoPlayer_seek_mCBA37307527BF6BC029E24AEDE42199C9E739282,
	Detail_easyar_VideoPlayer_size_m54C1CF41F978BAEA1982376B75C1D5FA0CF4D11E,
	Detail_easyar_VideoPlayer_volume_m2843DFA0ADB258918A00816B6841C46FDB3D0739,
	Detail_easyar_VideoPlayer_setVolume_mB788E58E23D4A29AA23F04E3A1BA9BAA18E18587,
	Detail_easyar_VideoPlayer__dtor_m5E05634EBF902BA73867793D38626D286B57E3B2,
	Detail_easyar_VideoPlayer__retain_m142D6CAE8EDE88226A1DF0334C55DF6DDADEB839,
	Detail_easyar_VideoPlayer__typeName_mB82D9A777EBBDCFC0C53B086D3DF6023949B9B25,
	Detail_easyar_ImageHelper_decode_m54E1A1473FBE0A4EC3041C4AC90BBC85E94A124C,
	Detail_easyar_SignalSink_handle_mF8D4929F26413ABCF873201C154874B4F555EDED,
	Detail_easyar_SignalSink__dtor_m020A6AF4EF86B69E5F72A148DFC8ACB1938A6ECF,
	Detail_easyar_SignalSink__retain_mE20AE59D901078E06BF50CD565C86EAA869675D6,
	Detail_easyar_SignalSink__typeName_m93E13AF5901FC5258959BF9214B0E0BDD21F2C4C,
	Detail_easyar_SignalSource_setHandler_mC8EB8BAD3FB3B26B9CEFFF57CD880575E83AC9C6,
	Detail_easyar_SignalSource_connect_m39160F3C2489BE79586B0D129612B8867F43FF99,
	Detail_easyar_SignalSource_disconnect_m4062413F99631943BAB09A710DB5C501027C1306,
	Detail_easyar_SignalSource__dtor_m047A72AD7F71771DC751C576AEE2DD9A8BADD858,
	Detail_easyar_SignalSource__retain_mC2241D163FC6A0F84F35655B90F6F5176F4E75D6,
	Detail_easyar_SignalSource__typeName_m95F2FE9FD51259458BCCB4F3BA1CDCA9BBE517CC,
	Detail_easyar_InputFrameSink_handle_mB5540ACC221D25F534202E6F6AD094F33EC33F01,
	Detail_easyar_InputFrameSink__dtor_m66461159B78670B7CE8C115FAE87117B618E9540,
	Detail_easyar_InputFrameSink__retain_m0B598173F3D208579263B5FCFFC4350B88094744,
	Detail_easyar_InputFrameSink__typeName_m8D06AFC57143DBC4C855F355268B40EAC952B197,
	Detail_easyar_InputFrameSource_setHandler_m8BD6868FAF52B5DBC5D419E88CE3A750C305CDA0,
	Detail_easyar_InputFrameSource_connect_mB61B076815A78262766067931A7EEF5CED7B7DAD,
	Detail_easyar_InputFrameSource_disconnect_mB1F0E055E8D7165536ABFDDC428A6E74A7BD6D43,
	Detail_easyar_InputFrameSource__dtor_m99C011B4C4067C8041BAD206DD7AF0993AB98AD6,
	Detail_easyar_InputFrameSource__retain_m3C7F0D1558AC4C8386A092E822806158A98C4A52,
	Detail_easyar_InputFrameSource__typeName_m932250B2B9E1093F2136E872F9219DB88865767D,
	Detail_easyar_OutputFrameSink_handle_m005AF3CC6B5EE21AF3CB2B7BD4CF584B92C2426B,
	Detail_easyar_OutputFrameSink__dtor_mF9A5126192F570D39EF035665CCB31333DED5861,
	Detail_easyar_OutputFrameSink__retain_mE259890E77414BBE2B7E893AB9A00874524B48F1,
	Detail_easyar_OutputFrameSink__typeName_mE0B3F0F3A607684030AA07F0F0EA772BE1703801,
	Detail_easyar_OutputFrameSource_setHandler_mAE9C0083CB087AA6DCD29AEE2C8C611CD73315AD,
	Detail_easyar_OutputFrameSource_connect_mBB280A8EF6C0AC595B4D97FFFDFEA9E78C109279,
	Detail_easyar_OutputFrameSource_disconnect_m0A2096BB11941B1D91FBA9BD2A971CDB89775AA2,
	Detail_easyar_OutputFrameSource__dtor_mB5DA4B03955B3520667F5124B1D24C6CAE568600,
	Detail_easyar_OutputFrameSource__retain_m1A7E3E551ABAB77CC100C663C82055E0F31B18CE,
	Detail_easyar_OutputFrameSource__typeName_mD81C08884E8BFEB84A305779188878A9553DF1F5,
	Detail_easyar_FeedbackFrameSink_handle_m81FC5687C0994262F0BB630B6BEF372CC126BEBB,
	Detail_easyar_FeedbackFrameSink__dtor_m6DB49F4E319A89071E02BEED1E4F830434E83C92,
	Detail_easyar_FeedbackFrameSink__retain_m44875276040A45B9FB190DF42F7BEDEE1691846B,
	Detail_easyar_FeedbackFrameSink__typeName_m8C23AE43282A8C560924D93DEE50BBAF81D1DAF2,
	Detail_easyar_FeedbackFrameSource_setHandler_m22EE616EAF4B46CF8343612E37FB371CAAF638F6,
	Detail_easyar_FeedbackFrameSource_connect_m51709B65AFE9470F9B1D7BA5864A3F66C0F4EBF4,
	Detail_easyar_FeedbackFrameSource_disconnect_mF932D449C519B9A82972CED0C7080F745806ABA0,
	Detail_easyar_FeedbackFrameSource__dtor_m1072D69C201ECB0FF0E0A5B782B52D4C3C993B68,
	Detail_easyar_FeedbackFrameSource__retain_m677CA87D360C67BC97FFDC1BB5338D0347C4EE12,
	Detail_easyar_FeedbackFrameSource__typeName_m37E8388B4ECB95B903CD4689A96E6639207F2DF9,
	Detail_easyar_InputFrameFork_input_mB275D38AE4E9E874EEABFFE738826C7FEEEA3D0B,
	Detail_easyar_InputFrameFork_output_m7186A23030C142637B5CDBD28C14D4556D4F12AE,
	Detail_easyar_InputFrameFork_outputCount_mA8AC4E9200789FA962EB416DC41A31DFD8D7B554,
	Detail_easyar_InputFrameFork_create_mBFE6FE8B154267C078B2D20D7871BCAE801F43D3,
	Detail_easyar_InputFrameFork__dtor_mA54172D89163226A9B96EE4FF5D4715F90C7D2D4,
	Detail_easyar_InputFrameFork__retain_m78142EBFED0AF893F23A9E9D15E662FADE000C2D,
	Detail_easyar_InputFrameFork__typeName_mD0EA3EDE8D8AEA165DAFE3F26E12439191CCBE25,
	Detail_easyar_OutputFrameFork_input_mE2886D9845537FE7F158F19BB432BC8B37C3C4F2,
	Detail_easyar_OutputFrameFork_output_mA9E6E6FB1AE2F79457EBA2D19A9B21C39D144854,
	Detail_easyar_OutputFrameFork_outputCount_m6AD280BABA4F7D1F837467A14683931E018B3F26,
	Detail_easyar_OutputFrameFork_create_m589A7D9B8A8571B76A46A8EE31B7227CB09BD4C3,
	Detail_easyar_OutputFrameFork__dtor_m9F594F28100D68CAAFD9C22898B973517A246C23,
	Detail_easyar_OutputFrameFork__retain_m611B6B48C25064E8BFE1951023D4302FF080CCA2,
	Detail_easyar_OutputFrameFork__typeName_mAE7671463C5E8FD795CCCA7F989DE2C4BB29DCB3,
	Detail_easyar_OutputFrameJoin_input_mD6A852B1EFF87A699611A98E24F1C79DEF8CB16E,
	Detail_easyar_OutputFrameJoin_output_m83C4B14052CB595530A149985B91C53DE1B51D07,
	Detail_easyar_OutputFrameJoin_inputCount_mBDEAED819FE0AC103EC11A0D684F134C3A345C5A,
	Detail_easyar_OutputFrameJoin_create_mD364B93567339FBCE28ED01DE049E533DF296D52,
	Detail_easyar_OutputFrameJoin_createWithJoiner_m694281D772425C857E61A918557494BF6F2433F5,
	Detail_easyar_OutputFrameJoin__dtor_m23D06DE2B88604D03CAE6CB32F4D4091D59D146D,
	Detail_easyar_OutputFrameJoin__retain_m3C3B941E4F9CBB9F00B9F25F0FE9782ED943E02E,
	Detail_easyar_OutputFrameJoin__typeName_mDB1DB4C091499DD99B8EAD48B0FC5FBD0428EA12,
	Detail_easyar_FeedbackFrameFork_input_m57800AE3BC9398FFBDA0985A8470ED92D592D434,
	Detail_easyar_FeedbackFrameFork_output_m384CBE75EC3FF7369C42C78EE7536B530CFE3ABE,
	Detail_easyar_FeedbackFrameFork_outputCount_m6CE611293059CAB6FD246B6079876D64264F919A,
	Detail_easyar_FeedbackFrameFork_create_mCFC66E5F05A7216880F124FF9288D6915294A458,
	Detail_easyar_FeedbackFrameFork__dtor_mBDD3DFD7324105447F763F200716DF75DF9D3FB5,
	Detail_easyar_FeedbackFrameFork__retain_mC0209A5E29DE1EAA0AB8B3B8FF490B70B2BA576F,
	Detail_easyar_FeedbackFrameFork__typeName_m217B953453A42A68D3B61507DD1FF90D531A092A,
	Detail_easyar_InputFrameThrottler_input_m8D300B50DEE77924E128439ABCE022C6138502BB,
	Detail_easyar_InputFrameThrottler_bufferRequirement_mA0101746C556B8C97FB20C15AAA37935BD42AFA1,
	Detail_easyar_InputFrameThrottler_output_m0BD6AD005A874C8DF5C886D24C64800BB1E33C10,
	Detail_easyar_InputFrameThrottler_signalInput_m8C246FA720BEC088B27F552194D8C781B3465B6A,
	Detail_easyar_InputFrameThrottler_create_m695E13A5FE7F7FDCB3979E90A8A920D6340FF21D,
	Detail_easyar_InputFrameThrottler__dtor_m90E7A4D2DAD0A7DCED7A811AF805CA7659A6C048,
	Detail_easyar_InputFrameThrottler__retain_mF6C22242C3DC46ACB9AF59EE781346A76BC0A951,
	Detail_easyar_InputFrameThrottler__typeName_mA7436BD865EAD57DDF15F43BB100C7C21F18AE66,
	Detail_easyar_OutputFrameBuffer_input_m8C6FFF6312CB4607D96EB361E30B3A8980ED53F6,
	Detail_easyar_OutputFrameBuffer_bufferRequirement_m19C61150AFB4866A2C09975990F420F6ADD4E347,
	Detail_easyar_OutputFrameBuffer_signalOutput_m41F1607F7A9C8C84517252D68B7E144E7952E2B7,
	Detail_easyar_OutputFrameBuffer_peek_mC923F2ED92AB6D7F6D0BE5D7A146676667E451F0,
	Detail_easyar_OutputFrameBuffer_create_mC6D7699ACCCC386E9D1B34490B072252DA9B83DB,
	Detail_easyar_OutputFrameBuffer_pause_mCB67726A2498413D5502ED22428CACD9E150EEBA,
	Detail_easyar_OutputFrameBuffer_resume_mCDFFC6F51CFBDB27D38A4CC5CE96102A843C2569,
	Detail_easyar_OutputFrameBuffer__dtor_m9629F9FC4768C465EE0803331F581DC052E61B06,
	Detail_easyar_OutputFrameBuffer__retain_mA24BD34C823DF71D7B56D98348CD7A692EFA12B5,
	Detail_easyar_OutputFrameBuffer__typeName_m29257668A9C6A3AEA190163C467579E64241B60E,
	Detail_easyar_InputFrameToOutputFrameAdapter_input_mEC8DBCBC19E2B6554576AF226836D6E795D6A36A,
	Detail_easyar_InputFrameToOutputFrameAdapter_output_m43000B084DE6527B0819D1FF2C03D896F287D710,
	Detail_easyar_InputFrameToOutputFrameAdapter_create_mBF2C272141770A9D9BA8E6371A8598D9B927D55D,
	Detail_easyar_InputFrameToOutputFrameAdapter__dtor_mB47E5E792163583CFEF26FFFEDF268F393863806,
	Detail_easyar_InputFrameToOutputFrameAdapter__retain_m27F00C1EC372C61AFAD5F989253A1F49C77678C7,
	Detail_easyar_InputFrameToOutputFrameAdapter__typeName_mBD44555301188DC4EEA8C825B3380510831D6781,
	Detail_easyar_InputFrameToFeedbackFrameAdapter_input_m066255D89E5C2727E518BBD30E028E24523591E8,
	Detail_easyar_InputFrameToFeedbackFrameAdapter_bufferRequirement_mC96C762A823766EC31595EA401DD7EE8FBA25F3F,
	Detail_easyar_InputFrameToFeedbackFrameAdapter_sideInput_m533577CC9CCA1535D18BBD1648D356A89CDACD0E,
	Detail_easyar_InputFrameToFeedbackFrameAdapter_output_mF57204BBCA48F3CF1E47578EABAE94D285C3F60D,
	Detail_easyar_InputFrameToFeedbackFrameAdapter_create_mA9CF29FEF03B4BAD7B44421A96FBF1126458D048,
	Detail_easyar_InputFrameToFeedbackFrameAdapter__dtor_m1A6C640B2D12C1B2EC2E896C1A41BC65A818BE86,
	Detail_easyar_InputFrameToFeedbackFrameAdapter__retain_mA4723A50EA44182D2B7A0886BEA6E5BF2E1533E8,
	Detail_easyar_InputFrameToFeedbackFrameAdapter__typeName_mE6959B1246DD06AB0C7C69B4CC7101EB24689E6F,
	Detail_easyar_InputFrame_index_mEDB2856C9FF9A88E9920ECBFA50950087AC48969,
	Detail_easyar_InputFrame_image_m140EFB6262F340B9F0F3916E72141833D74160D0,
	Detail_easyar_InputFrame_hasCameraParameters_mD931E132D851F4DA7F88CB8EE5BCFC5C9C145BBA,
	Detail_easyar_InputFrame_cameraParameters_mA62697433558044B6FF1BD53681AC6AA9B25B328,
	Detail_easyar_InputFrame_hasTemporalInformation_m36F5B3CF5A939305D81930E233F1089EB521119E,
	Detail_easyar_InputFrame_timestamp_mF7C39F86224842E30C4B3173E090B9721E3DD552,
	Detail_easyar_InputFrame_hasSpatialInformation_mD3FB17E31F570987126A639EA8FF612D4C2A0244,
	Detail_easyar_InputFrame_cameraTransform_m6F3F30E489C5897C7ABDBF871120564FA6B330C9,
	Detail_easyar_InputFrame_trackingStatus_m09ECEACD869F2FA44DD3DAFC5059107BFEF23A02,
	Detail_easyar_InputFrame_create_m754F0F74A62561C8EFCD40841C7EC2F4FF8627DE,
	Detail_easyar_InputFrame_createWithImageAndCameraParametersAndTemporal_m5260B805F182EB356D16A83371827A68EB7A75BE,
	Detail_easyar_InputFrame_createWithImageAndCameraParameters_m141AF32142981C8D0B33190C555D3715482C377B,
	Detail_easyar_InputFrame_createWithImage_mDDEE39E781193DA245C0F447E5937E201653E875,
	Detail_easyar_InputFrame__dtor_mA41B0663599C7850BE0670167D8A51083454F19E,
	Detail_easyar_InputFrame__retain_m941C00204E986077F77F1B49AA1C6535DEF0F0EE,
	Detail_easyar_InputFrame__typeName_mD9811522E0609F824D15428806FE8B2E5A4573DA,
	Detail_easyar_FrameFilterResult__dtor_m9E3E4A0BCF1622F04F0C71BFB8C1EC3EAD31BBA1,
	Detail_easyar_FrameFilterResult__retain_mBFF0B4D846C559A0C1A40A107F17426482076967,
	Detail_easyar_FrameFilterResult__typeName_mF7F3C046209720735927948B7B26A27D83B8F6D0,
	Detail_easyar_OutputFrame__ctor_m658A62084DD2F33A00BDDC27E91E3B446565A7D7,
	Detail_easyar_OutputFrame_index_mE738B10BE5849CA075F81D71709BEAD6D99E5FD2,
	Detail_easyar_OutputFrame_inputFrame_m0E1A48E600F3D95869E56BC6E83FD954636C6E8D,
	Detail_easyar_OutputFrame_results_m241A08A345B5878CB51D282B81A6390E20C31024,
	Detail_easyar_OutputFrame__dtor_m9DB1463D81EE0A7E8865889925B8EE3AA5759630,
	Detail_easyar_OutputFrame__retain_m3F25965774DB1FD083E9BAECF5AD271E3E7EEB73,
	Detail_easyar_OutputFrame__typeName_mB17A156F808FAB28F3B8116BE77F24EC1A8F1D78,
	Detail_easyar_FeedbackFrame__ctor_mE8130EAE0CF0E3888A7885B0E7E6C642383D65CD,
	Detail_easyar_FeedbackFrame_inputFrame_mF682B084F7560A4B26AA6571FBB0214A07207645,
	Detail_easyar_FeedbackFrame_previousOutputFrame_m8DC07F8A8228C5B660D3B340004F8208F3568468,
	Detail_easyar_FeedbackFrame__dtor_mB30F6BF9135F890A874E5EAB99FCC3AB6D8DF5A2,
	Detail_easyar_FeedbackFrame__retain_m5CE500F436BFC3D2021CE70C61F1E9CCF18EC8C1,
	Detail_easyar_FeedbackFrame__typeName_m6D201590A998FCEF6F10222740DA6CD67A2AC9C7,
	Detail_easyar_Target_runtimeID_m8133CBAB8D8BC24930D60997CD9B363141388090,
	Detail_easyar_Target_uid_m64B63E65751B41F3C942BAE4BA0D435B69B7AFDE,
	Detail_easyar_Target_name_mBDE3F498CCF6ECAF3487C69FE68FA89B6150AB16,
	Detail_easyar_Target_setName_m2654EBF078FE6C6C0F83D963D8E33A8C25D07D39,
	Detail_easyar_Target_meta_m326FDC00B0E1D1C3F6D9F69EFCC66202664A584C,
	Detail_easyar_Target_setMeta_m5F31D654E462FAC79AA3D065C08763A9AB110066,
	Detail_easyar_Target__dtor_m7AB63DE8A08DB4D9C65F35719CC085877EFD3F19,
	Detail_easyar_Target__retain_m7237D732A5DABC4791C37561F6B275D63B68F9F4,
	Detail_easyar_Target__typeName_m395D719B2522F8518226EA93C1D2819652370C9B,
	Detail_easyar_TargetInstance__ctor_m0DD4ADBAF3D0252867A147A7CD4DE6F3738CE3CB,
	Detail_easyar_TargetInstance_status_m31F421A09DED7AF3990AC857B3D5CA98714C76ED,
	Detail_easyar_TargetInstance_target_m136FB8DFEE24063CCA22233F2E235F67C78FD9BA,
	Detail_easyar_TargetInstance_pose_m7686948E36D6B5F981FEA41DAB373AA97791BD7E,
	Detail_easyar_TargetInstance__dtor_mE4E998FD33C32B2BBE0F2261FC1A21E26BF2F052,
	Detail_easyar_TargetInstance__retain_m55AEDEADBCC847B0817482F5149AC885D6947D86,
	Detail_easyar_TargetInstance__typeName_m6029D2120AACBEB48D17BCE40BD28A881C04D05F,
	Detail_easyar_TargetTrackerResult_targetInstances_m3504052671989AE87C0FE4644B6988DF0DD0525B,
	Detail_easyar_TargetTrackerResult_setTargetInstances_m6DB34AB5A1E8DEE2C138994F722C0E4D84B5C036,
	Detail_easyar_TargetTrackerResult__dtor_m908866548574FE5DEE920577ED6A2062053834DF,
	Detail_easyar_TargetTrackerResult__retain_m670DE6D7F57400063547E12E3EF3C8790C9B543D,
	Detail_easyar_TargetTrackerResult__typeName_m6FBA8B4058BCFF5FD27F22A4A604809AB0253536,
	Detail_easyar_castTargetTrackerResultToFrameFilterResult_mCA3AB27FF8A530C05456D13A7B88147285B09328,
	Detail_easyar_tryCastFrameFilterResultToTargetTrackerResult_m456D60E618967C9C48D9F92FCA77CD0896CEAC9F,
	Detail_easyar_TextureId_getInt_m842A142D7F8201DED1A3E9DFE1B65A52AA30EF9D,
	Detail_easyar_TextureId_getPointer_mEBDBE484C534F23FB2678577D30384B58469B4B0,
	Detail_easyar_TextureId_fromInt_m48F5A7434E24DF8D0706FAF5A5D8804F2460DF45,
	Detail_easyar_TextureId_fromPointer_mA4A591B382CFCEFECF7A4CA8CBD219A969FD9E42,
	Detail_easyar_TextureId__dtor_m09C583139324ED5C363164493E952E6E48C53730,
	Detail_easyar_TextureId__retain_mD84406FFD9C8C110F33ABAD1C601301B8D224B10,
	Detail_easyar_TextureId__typeName_mCB452F6133A09F14F50FDFD0054A3A69145497F9,
	Detail_easyar_ListOfVec3F__ctor_m208AA39916D6306C7A1D13EFE918FF983F0017C4,
	Detail_easyar_ListOfVec3F__dtor_mD96C74E4AADDDC9C8546A071980D587A79AFCEB1,
	Detail_easyar_ListOfVec3F_copy_m0C5220942D553A6DA0B65B19EBE664BFB35475FB,
	Detail_easyar_ListOfVec3F_size_m5140DB61BD2C43887EAE7E336B573ADD335F9D5F,
	Detail_easyar_ListOfVec3F_at_m45853C0A7829D9B210287C182976A701FBDFB96F,
	Detail_easyar_ListOfTargetInstance__ctor_mE802AD303EC8450099963AA439FE2A186DFF679D,
	Detail_easyar_ListOfTargetInstance__dtor_m4E09F18CC8763D8439AE3033C73F722535B2864C,
	Detail_easyar_ListOfTargetInstance_copy_m1E804B6A7B63C091A09ED70548C3380CB36DD59A,
	Detail_easyar_ListOfTargetInstance_size_m455367BA9B4EA9033FE49BC85A06AD6F804C039C,
	Detail_easyar_ListOfTargetInstance_at_mA3F2F3CD0DC4A32B67AB28258580179BA4A31EDE,
	Detail_easyar_ListOfOptionalOfFrameFilterResult__ctor_mBF5DAD81C63BC8A1905F515A0A08F13D72949466,
	Detail_easyar_ListOfOptionalOfFrameFilterResult__dtor_m4060C70E10D3F52C23C1AB5165227CCD3E077CB9,
	Detail_easyar_ListOfOptionalOfFrameFilterResult_copy_mA399AE55ADFF1C3BA45E0903861E25D1FF1C1600,
	Detail_easyar_ListOfOptionalOfFrameFilterResult_size_m2608B475B5302D0DEEC0E172A45BC6F70D5E52BF,
	Detail_easyar_ListOfOptionalOfFrameFilterResult_at_m9FA024208C78175EA09194FADEC6B8D9F69FACC3,
	Detail_easyar_ListOfTarget__ctor_mADD03B4FB4AA5A1A3C54A9C3DD5B0D0F4F4BB7A2,
	Detail_easyar_ListOfTarget__dtor_mEC0C99C5EB317C5C55A29AB90DE3AE52545E7B5A,
	Detail_easyar_ListOfTarget_copy_mF20A09B8BC01C621246AFDCAD29B637699B1F3D4,
	Detail_easyar_ListOfTarget_size_m0A293FCD1184DC9A700EB9B47D8AD00F562B55C8,
	Detail_easyar_ListOfTarget_at_m5E034C9665D389CE13DC8D784470CA62FFBE9803,
	Detail_easyar_ListOfImage__ctor_m18F81AD3F3E783983E1CE031417B9055BAC17794,
	Detail_easyar_ListOfImage__dtor_m1325BED170B1F85FEEF02452FDCCA854E7131BAE,
	Detail_easyar_ListOfImage_copy_m4758F2547D592FA865733107E6CB4B2C84935115,
	Detail_easyar_ListOfImage_size_mA8BC0B77623E68B647E3D607A6E2ECC9ABB2CA40,
	Detail_easyar_ListOfImage_at_m9C02A5A9B5FFF80701D658034A9427F94AAD099D,
	Detail_easyar_ListOfBlockInfo__ctor_m7102CA58A465879D2C65993202F35D854DA6BDFC,
	Detail_easyar_ListOfBlockInfo__dtor_m9EFADB57FBA215F7AA169E160D9764BB3E041CD0,
	Detail_easyar_ListOfBlockInfo_copy_m7BEA45087689C73457B99CDEC423A8E263AFB751,
	Detail_easyar_ListOfBlockInfo_size_m0401FBBD16C69BB6855778FE317EFD5204F0B5BB,
	Detail_easyar_ListOfBlockInfo_at_m35F302F45F102C40FAC0E6843CDF07F1FAA90F51,
	Detail_easyar_ListOfPlaneData__ctor_m8D38E177592B910002CC0C8F663334E38530EFF7,
	Detail_easyar_ListOfPlaneData__dtor_mD085D3967B86ED76F284856BBA64CCC2BB22C790,
	Detail_easyar_ListOfPlaneData_copy_m24D45BD45728185CE0E2D05B88CF2E18FCDE2C81,
	Detail_easyar_ListOfPlaneData_size_m71083CD736D629DBD7F2B346C77948E64341E1B4,
	Detail_easyar_ListOfPlaneData_at_m668ADDB5A436FB8D02320150EF81680C501F4AC9,
	Detail_easyar_ListOfOutputFrame__ctor_mDB981943B148F4903059E8744E4B633AE972C2B3,
	Detail_easyar_ListOfOutputFrame__dtor_mC530268C5AC9EB0551810F7E0828C4AFCBF5D1BF,
	Detail_easyar_ListOfOutputFrame_copy_mA2045B036A943E3E174AD085C50E93930B165F87,
	Detail_easyar_ListOfOutputFrame_size_mFF9B7D1B62CBF8F0DF6BA856C2AA6A3D0E66B06E,
	Detail_easyar_ListOfOutputFrame_at_mADC5037975B76D149A95A42CE4AE249E23B2749A,
	Detail_String_to_c_m1E6A781AFCBC919C5A6A7FB3430C60DCE08FA04B,
	Detail_String_to_c_inner_m338944A05970542DD747E0DE3E99D53EB3C7CF83,
	Detail_String_from_c_m598C945D4B0987313DEB9CC0810324968E252552,
	Detail_String_from_cstring_m41461029B66E7B609106EEB30475D59BEE46DA75,
	NULL,
	NULL,
	Detail_FunctorOfVoid_func_m8233FE906FAE6473609404B722CF66412E5EFB48,
	Detail_FunctorOfVoid_destroy_m9A583576B55465A17C01F9B74E416B531702AB80,
	Detail_FunctorOfVoid_to_c_mC5613CA270E64E7C57C51C13A7CB36FB52996427,
	Detail_ListOfVec3F_to_c_mF8E4DB1661D8CE1ED5BC20DDED1FCD2B4D50FAA8,
	Detail_ListOfVec3F_from_c_m2D780A2EDE5838EA5755F1D6187AB78F420DED5B,
	Detail_ListOfTargetInstance_to_c_m7CC734436F98C3A7F02BC0E8EE232F0E015EF58B,
	Detail_ListOfTargetInstance_from_c_m2AF1CCF8D8F1A17C759520947A0FF7A7E5BE43A1,
	Detail_ListOfOptionalOfFrameFilterResult_to_c_mD353FDBE04412B0F818612BF3E95C8239CDF0F67,
	Detail_ListOfOptionalOfFrameFilterResult_from_c_mA2DA762569EFBE14680F8CB7ACAAEDDDE2C15AB4,
	Detail_FunctorOfVoidFromOutputFrame_func_m142DA320BDD782F0665F95929A1FAF518AAB0B80,
	Detail_FunctorOfVoidFromOutputFrame_destroy_m39E6790C6700E68EEB6EF662B32E84E8150EE773,
	Detail_FunctorOfVoidFromOutputFrame_to_c_m1FA7F0AE62C3404FDF4D9B79CB4A076A09F691FC,
	Detail_FunctorOfVoidFromTargetAndBool_func_m4F4E191A4A3E8C8B1C93A9A83F773843E947D3E0,
	Detail_FunctorOfVoidFromTargetAndBool_destroy_m407203B6D0165B9CBB93CDC394623D25D1394C3E,
	Detail_FunctorOfVoidFromTargetAndBool_to_c_mA09F2DBB0D1DA2A1B825161B8FD24FF34548DEAE,
	Detail_ListOfTarget_to_c_m0D5BFEE2B4FF3C1589D7F02C660542A9F41D75E3,
	Detail_ListOfTarget_from_c_m84398A60270F2855D753A0B2D48FFDE9E0E1F621,
	Detail_FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_func_m47D3C6B1C3FA746A6D370FBC7B1D517706FB3EFE,
	Detail_FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_destroy_m44D24357597F4A3861061A606C66CAC57CBDE0D8,
	Detail_FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_to_c_m37E8441A163CA939A0656B244F46D4874B5270DA,
	Detail_ListOfImage_to_c_m7DFB79A1964C7797E749B4D88CD57BBBA02D01DC,
	Detail_ListOfImage_from_c_m4AD4BB1192546842FF2B370A37D7A6EB77FBB60A,
	Detail_FunctorOfVoidFromCloudRecognizationResult_func_m631246C2873CA0786043DC8226B7A1F69BAF973D,
	Detail_FunctorOfVoidFromCloudRecognizationResult_destroy_mBF60D4AFDA72F76BFBE41B77BBAACB66D22FB251,
	Detail_FunctorOfVoidFromCloudRecognizationResult_to_c_mF394000F6512A843638766A3E872E063C9EABA8A,
	Detail_ListOfBlockInfo_to_c_m8AE703276F8D6E490385821E9F12A5C127EFD4C6,
	Detail_ListOfBlockInfo_from_c_mC33AB4B618886BC4852E2FDB4ED326F215CA7553,
	Detail_FunctorOfVoidFromInputFrame_func_mA15F0CAA1EFCDBBE96BA6D3A17F1AE49FFB6443F,
	Detail_FunctorOfVoidFromInputFrame_destroy_mBC40B788EC5FA01C081A8B8F29DD83BD7C04BBE0,
	Detail_FunctorOfVoidFromInputFrame_to_c_mE09D592C78FB2450897DBE82D3F71C797045EC57,
	Detail_FunctorOfVoidFromCameraState_func_mC7E9F14253350E76D81DD852C31479AE6FA97541,
	Detail_FunctorOfVoidFromCameraState_destroy_mFF1D0D74D6DDC00A03B9F68ABD23B93BBCB212B2,
	Detail_FunctorOfVoidFromCameraState_to_c_m48184C444F8707ED832ABB4A5958E4DBAEA2DE52,
	Detail_FunctorOfVoidFromPermissionStatusAndString_func_m8D9D32B6E616DCFD443DE289BEE2A1A9E10D63B3,
	Detail_FunctorOfVoidFromPermissionStatusAndString_destroy_m781B495D25D988C89184294BD62865D9E801969D,
	Detail_FunctorOfVoidFromPermissionStatusAndString_to_c_m090CF72FE3106914DE4F79C2B4406956AE9A680A,
	Detail_FunctorOfVoidFromLogLevelAndString_func_m9A78157C61E95CA37E54667EDB501049BE406D6A,
	Detail_FunctorOfVoidFromLogLevelAndString_destroy_mF22BD5934E396FB1042F8B0E27B54DBE318FCBFE,
	Detail_FunctorOfVoidFromLogLevelAndString_to_c_m69294E9D33E5CE6B6527652B4DBE3F5F697A1010,
	Detail_FunctorOfVoidFromRecordStatusAndString_func_mA45DA83CDE8ACEB8F46865D1B86A518290177EBA,
	Detail_FunctorOfVoidFromRecordStatusAndString_destroy_mA47A5359B41B4FD8321FA5CC075E76AD7ABBBCD1,
	Detail_FunctorOfVoidFromRecordStatusAndString_to_c_mFEEEB691A291F28EA0A514233B9C1878A35353AA,
	Detail_ListOfPlaneData_to_c_m956FFABFAC8E0F822137F46242637F3FA0BC161A,
	Detail_ListOfPlaneData_from_c_mABE69D1DF9912A0F5F41C2EBAE6E632C4E7FD1E1,
	Detail_FunctorOfVoidFromBool_func_m18B5057D02371A6A12534E44D51E79565365602A,
	Detail_FunctorOfVoidFromBool_destroy_m3D313F4C383413AAD2FCB52140036EA67F40B985,
	Detail_FunctorOfVoidFromBool_to_c_m3F8E9FD5F5A57F76366EF25F9995C785216EC7FA,
	Detail_FunctorOfVoidFromBoolAndStringAndString_func_m32AF4156D52F9B1AEA2A81D03E596F56609DF977,
	Detail_FunctorOfVoidFromBoolAndStringAndString_destroy_mA6C753EE55047102FBA63F1ABC45CE9D2B516010,
	Detail_FunctorOfVoidFromBoolAndStringAndString_to_c_mA32E423FE6EC406784F0CF931C765DD7316E0E91,
	Detail_FunctorOfVoidFromBoolAndString_func_m4786C9B24241EBD28AAF555E9CFCB97344EFB8CB,
	Detail_FunctorOfVoidFromBoolAndString_destroy_mDC79451BFE0F528760772F0A015FBC035E6E023E,
	Detail_FunctorOfVoidFromBoolAndString_to_c_mF617AB1474F656AD940D507F3D60A75B80D9C47E,
	Detail_FunctorOfVoidFromVideoStatus_func_mB7B7FD5B5858F436F3466A1919AF72453E7BE34F,
	Detail_FunctorOfVoidFromVideoStatus_destroy_mC318C3913E76C1CF3DC21561E52A312033C580E4,
	Detail_FunctorOfVoidFromVideoStatus_to_c_mDC754888D7AD6794FDDC572C48E824417E33329E,
	Detail_FunctorOfVoidFromFeedbackFrame_func_mF3A0BCA98C72331241FAC3A693309EBC3570DAD7,
	Detail_FunctorOfVoidFromFeedbackFrame_destroy_mE0489B5AF6DB4645A89B1AA2C5E728AA2085DA06,
	Detail_FunctorOfVoidFromFeedbackFrame_to_c_m4654D6C9873EA88DC10913ECE80F99077EEF36E7,
	Detail_FunctorOfOutputFrameFromListOfOutputFrame_func_m728DEA484FEF9A32AF0EA5C985684C3A923B6E93,
	Detail_FunctorOfOutputFrameFromListOfOutputFrame_destroy_m3213152A87BBAB55CB406BB9D08FB45301A33151,
	Detail_FunctorOfOutputFrameFromListOfOutputFrame_to_c_mCBD0AE3E1527F9689322688D72E736FFE864F19F,
	Detail_ListOfOutputFrame_to_c_m628E834221C655F32D06A3BBF65F958B1539C06C,
	Detail_ListOfOutputFrame_from_c_m3D19C2738649FBCCF73E2BDB8F4BA683E2199C22,
	Detail__cctor_m0D326FDA27F767F6166422FE775C92BACE73668B,
	AutoRelease_Add_m7A212059DFF41BE1EF6D05BEB95B52EF57C7B9F2,
	NULL,
	AutoRelease_Dispose_mE65C3AD323041685F52968097252346E245B058B,
	AutoRelease__ctor_m980DEE2E4F4111B44F7DB7EAD221C3F2F7CBB6B7,
	NULL,
	NULL,
	OptionalOfBuffer_get_has_value_mD6F26A29092013167861EE3E660476683A64529C,
	OptionalOfBuffer_set_has_value_mDC4547AE9C5D5CB894F23AB1AC16BB2B758D3CC4,
	FunctionDelegate__ctor_m1D45E2663E4A37D350606702178E972C22504488,
	FunctionDelegate_Invoke_m312678BEED2079103BF1FA550DCC3F140C93ABA2,
	FunctionDelegate_BeginInvoke_mA05E0CF1A0E17FFE7F7C261F84C87DF741268287,
	FunctionDelegate_EndInvoke_mC4D65D2A67724FCCEA48222E86707B798BDA5A86,
	DestroyDelegate__ctor_mEB511B64443B787AA6736EF3BEFC1FD6EE475DBE,
	DestroyDelegate_Invoke_m2D13CC8DFB145CE47D614842D9F878ED87F563CC,
	DestroyDelegate_BeginInvoke_m5020302360D3815B930AE1204E78072E0DE5F5AB,
	DestroyDelegate_EndInvoke_m700981CCA5D5A2D881CDF1885D9A55F8B3C3E716,
	OptionalOfObjectTarget_get_has_value_mF804B145E14437EA95BD0A5664AA16887425D034,
	OptionalOfObjectTarget_set_has_value_mF9205E39066CE8464514E8A917412EE576AAE862,
	OptionalOfTarget_get_has_value_m54BFBDD1FA4C03B32E0254203C95AB89471226C8,
	OptionalOfTarget_set_has_value_mD53D8270DE95BE1FE4DB151129C9654A6DAD4BCB,
	OptionalOfOutputFrame_get_has_value_m16750E6F8AC1CDD2105035942148EC3F17A2731C,
	OptionalOfOutputFrame_set_has_value_m0ED852A17431EA3157B7D32E3579B90F9DB11AA8,
	OptionalOfFrameFilterResult_get_has_value_m0B5B694A1819769F52A8B8B676DD8E7A504C4A1B,
	OptionalOfFrameFilterResult_set_has_value_mA6CDF1D438A978182E3D705DABCFB18A46835EC8,
	OptionalOfFunctorOfVoidFromOutputFrame_get_has_value_mAFC7BA70B0524FC0FFE85A533C0BD64F6C07E41A,
	OptionalOfFunctorOfVoidFromOutputFrame_set_has_value_mDAB3D368167065A00CA62736E0677FBC5A4D8540,
	FunctionDelegate__ctor_m66F61E3190D4E36FD0C3C0707BFD957529213AF8,
	FunctionDelegate_Invoke_mB7D8367BEC0AFE1316E79DC15A680A36FB82940B,
	FunctionDelegate_BeginInvoke_m569BE18FCE5C0B0AE5CB72A1062100AC6108E43E,
	FunctionDelegate_EndInvoke_m7CC0900AD87A0341B02A3482FEEF055973DA18C7,
	DestroyDelegate__ctor_mC13B534CE440DEE47C2E37906F8317A6B10343D4,
	DestroyDelegate_Invoke_m832126404B59A946EB12979D657AE9DC3BB1A833,
	DestroyDelegate_BeginInvoke_mAB9C3F695EB5221E67C04035619A538EBE8306A8,
	DestroyDelegate_EndInvoke_mFA27FD6FB8129F4FDD1C55E0845D13FAA66D7C0C,
	FunctionDelegate__ctor_mAB19B3CEB58FF908E87C397A326F2697E6726AD5,
	FunctionDelegate_Invoke_m5BD1865D5105B750654908BB965C68ACD6A6AFA0,
	FunctionDelegate_BeginInvoke_m1F486631168CC5C442EB408540E7D983BCBBC782,
	FunctionDelegate_EndInvoke_m0EA23CDF2296BAAC4D92D04E0BEC259851A48BDB,
	DestroyDelegate__ctor_m0C265E4B43D43EF11071BB6E13F5CB7D4C7FABFB,
	DestroyDelegate_Invoke_m36AED1864B294C2113E7CEEB15F7424A75EC67A3,
	DestroyDelegate_BeginInvoke_m98D795559000E47969925DBB8650B430A3BFBAFC,
	DestroyDelegate_EndInvoke_mB2DF1D58ADF47846B4D2886FC01F4610ED86AF28,
	FunctionDelegate__ctor_m4A872BF58089EA72EAF3DE53DAFDAC0BCD82FFA5,
	FunctionDelegate_Invoke_m166C49F173919D60AD282CBC1AF82956F9740473,
	FunctionDelegate_BeginInvoke_mEFE11253325F7453FDE38F52967CD32CAE5B2D21,
	FunctionDelegate_EndInvoke_m49F69DAB78FAB5E677C7D57D0B7EAA35C8378330,
	DestroyDelegate__ctor_m6ED8161195640DFFB8E46F5FA6289B3A05509D9A,
	DestroyDelegate_Invoke_m36D93B6620D6BED4DDFFEE8C6507C1368AAB8FC4,
	DestroyDelegate_BeginInvoke_m3A1D66B8E42C64B5085DC416A4EC45A5FFD46AD1,
	DestroyDelegate_EndInvoke_m1A8DA08D56D7A319184E7EDA702A3D7D9DBD9DFB,
	OptionalOfString_get_has_value_m9CD5A6C104367A1ED5B9E44785BA721056E0D4AE,
	OptionalOfString_set_has_value_m264C0ED811DA93486B2DE04A53B25EE094DE2C6A,
	OptionalOfImageTarget_get_has_value_m1EF7147FD931E8F917DEB6C46BD51A7563022D33,
	OptionalOfImageTarget_set_has_value_m8644B289D2E7A1195C693BB4E9843FE22F44C7A2,
	FunctionDelegate__ctor_m77BEF147432A3DB2F47F6A89266AF68957951242,
	FunctionDelegate_Invoke_m76DA49C11AB299733BD600054318972AA08A818A,
	FunctionDelegate_BeginInvoke_mA12817782D70B1FFBF1D42DF0292BD974131947D,
	FunctionDelegate_EndInvoke_m2B0EE98024E41E40D8B27CCFF1DFE3FDFD45EDA5,
	DestroyDelegate__ctor_m130EFA1D6131D28FEDE27440AF8FE8323ECD71A0,
	DestroyDelegate_Invoke_m1AFC4A439D8AD3B7975FE4853FBD353B35F0BB1C,
	DestroyDelegate_BeginInvoke_mCE7E52992BADEE8719A0F99C894AD9A2DFB0BD43,
	DestroyDelegate_EndInvoke_mED953CB4B4E72974B422853854EA3AA378C48AD8,
	OptionalOfFunctorOfVoidFromInputFrame_get_has_value_m1D155C98CBF075D904FF4031CAA79FA57AC1324C,
	OptionalOfFunctorOfVoidFromInputFrame_set_has_value_m9E8B4E0E2EDD811B56D788AA37AC28C6CDFC2D92,
	FunctionDelegate__ctor_m22228959FDD2D76D0494DB8BDE40DE8450114A74,
	FunctionDelegate_Invoke_mB73E30BD7031DC272CAEECFF9FD1DBA1229FACF8,
	FunctionDelegate_BeginInvoke_mAA32B7FEBDB4EDBBEE8845351F35B80D5B70BBB7,
	FunctionDelegate_EndInvoke_m0ED48B3290658AFC0C429CB25D098A501DD17533,
	DestroyDelegate__ctor_m637A5714DFD493412A57351034DF201AAD3CBA50,
	DestroyDelegate_Invoke_m79FE9F5D3B70D8BA4C970A2400EF2F512EDA1D81,
	DestroyDelegate_BeginInvoke_m142C7A0F1ED3FE2B0560D9640A131B4044D0A796,
	DestroyDelegate_EndInvoke_mC382376EED82A56614E777BA8963755CB008D049,
	OptionalOfFunctorOfVoidFromCameraState_get_has_value_m8B4EBF20C2AAD048E9D671047BEE42BF9D07308B,
	OptionalOfFunctorOfVoidFromCameraState_set_has_value_m1ED45DFE473C86A2641CF8823E93D759BBBFF7D1,
	FunctionDelegate__ctor_mEC46C2A6C3C08807C1562646AB98B8A199AB03D8,
	FunctionDelegate_Invoke_mF4E3F4769FEA4E1DD1B4E8B82D10C4913E05B451,
	FunctionDelegate_BeginInvoke_m281A3202DBF9A613647EB838A8FE26BFB7AB2F38,
	FunctionDelegate_EndInvoke_m3527CDAC3F38C1F78E6F8A01DFC6E07B02611072,
	DestroyDelegate__ctor_mC86175595B8866AECB3E95633F4024081810E8AB,
	DestroyDelegate_Invoke_mDFFBFC132610A0FD48CF58FAA6EA185C8B7FADE2,
	DestroyDelegate_BeginInvoke_m15447F2B57605F71E3D39ABC4CBF7E8E683B6E07,
	DestroyDelegate_EndInvoke_m11E070CD98E249582C1FAF058031B3914728EAE4,
	OptionalOfFunctorOfVoidFromPermissionStatusAndString_get_has_value_m81D80E41F5A8E24E6396A58596E192C4CF7DE650,
	OptionalOfFunctorOfVoidFromPermissionStatusAndString_set_has_value_m92FE8F26A4611C4D1D925EF3DB553A81F482D896,
	FunctionDelegate__ctor_m7CB4B98FD27C695EF880ED3DEDCCD68EC81B96D7,
	FunctionDelegate_Invoke_m331255F6103E2E85AD26F6F4AA18C4C7179D5B01,
	FunctionDelegate_BeginInvoke_mBA591F260EB29850D7FF2A05F84D4403002FBC0C,
	FunctionDelegate_EndInvoke_mF5C064004142278F5DF052DD64D81DE5B01001AE,
	DestroyDelegate__ctor_m9FFB97C3E475AE9C2B59D5CC3BBFCFF4A0A5D5E6,
	DestroyDelegate_Invoke_m67058F84DC237C252F0BA5045B4D6D8546507C5A,
	DestroyDelegate_BeginInvoke_mEB4027E06F88F4D9B381FB2C65756ED937134C48,
	DestroyDelegate_EndInvoke_m327A1294381F14BB2D8DE3CEFA64B6B634003D27,
	FunctionDelegate__ctor_m1ED621058775EBEDE6295EF2D208146A255975CC,
	FunctionDelegate_Invoke_m92881F9A36AF47BDFF0D94E1E97195AA6EE04EB2,
	FunctionDelegate_BeginInvoke_mCFCA59D7162BBB7E44AD21D453A2C78534B3F656,
	FunctionDelegate_EndInvoke_mF9FD7D3E095552529DE87711CA1C24C7C1BA0EA1,
	DestroyDelegate__ctor_mCF6A2241214BA897AC9500FF8FEBF386FAB4CB38,
	DestroyDelegate_Invoke_m7BE9BA22B468AABBA23017B98EA905B8B3562FA0,
	DestroyDelegate_BeginInvoke_m13D7228FB7BB78CBABD7D840A7A613F263E18A11,
	DestroyDelegate_EndInvoke_m7A2EF640AA2A19F9272D58FEDA1CFDD9CB1F32DE,
	OptionalOfFunctorOfVoidFromRecordStatusAndString_get_has_value_m40F62F2C9F50F336DB495B577C99D4AC9849FFD5,
	OptionalOfFunctorOfVoidFromRecordStatusAndString_set_has_value_mCC230A3F9E9860FC8BD386C044F043F177F7E852,
	FunctionDelegate__ctor_m7A83805BB9EA5670DC0648F8779AAB53AA8E88FC,
	FunctionDelegate_Invoke_m0640327DB6AF15A5E7453C71732763EDE32CB96B,
	FunctionDelegate_BeginInvoke_mDA2EBB0C5A782EE4ECB65F17E5C7C4C192DBEB30,
	FunctionDelegate_EndInvoke_m61EDA4A6FE2F6542395C6D0518ABE5C1A0F5A3E2,
	DestroyDelegate__ctor_m06D7090996066E6297C71939AFD1DE1B7660A740,
	DestroyDelegate_Invoke_m626B00C6EE55BAF2B51AF6D3440FAA26AF378D0B,
	DestroyDelegate_BeginInvoke_m07955E9D590DBD3CC52F0169EB9E91F711F91C5F,
	DestroyDelegate_EndInvoke_mC57A971B42AD810362C6C1AA5473EF7E050161E8,
	OptionalOfMatrix44F_get_has_value_m9C30778B7D9E982A11356624459B1D0A08A30AD0,
	OptionalOfMatrix44F_set_has_value_m68B97FAF4E20F2D0EE93221D7C51FE60F5EE9B55,
	OptionalOfFunctorOfVoidFromBool_get_has_value_m7D8DFE0840F0E53FA25E3ADEE84C54DDACCBE6BA,
	OptionalOfFunctorOfVoidFromBool_set_has_value_m077DB0E0292E386E9A1925D6161E580CB9A57B8B,
	FunctionDelegate__ctor_mE2C767B389B5CE333D13F751E5677121656BAF0F,
	FunctionDelegate_Invoke_m9E4218574D741E072D017696ADA9D89B0D2A1CEF,
	FunctionDelegate_BeginInvoke_m45CCBE1B2F615B6AB7CC51F5B5D73F60BEE22852,
	FunctionDelegate_EndInvoke_m2563E5B38C368F865DE17B913ED9B532C4FC44DD,
	DestroyDelegate__ctor_mB69AB348F0C70B3340B80D97F2129B839BF52A07,
	DestroyDelegate_Invoke_m3C7CFEB46A93EB9FE0BA5990F9474CFEFF16270C,
	DestroyDelegate_BeginInvoke_mD43363C968B9A3893D9801E8C087D68F67C39389,
	DestroyDelegate_EndInvoke_m06BC0CAA5DA44EA66C164D188E3142D773C691C2,
	OptionalOfImage_get_has_value_m7932A8718D733AE7A914D6726A3A8FEBBB4E132A,
	OptionalOfImage_set_has_value_mFA950AF9ADE589082DE916C46EAC5280D8C41CA4,
	FunctionDelegate__ctor_m9EA89B29E4B455B7EA0B3C16C3E972FC7D9BC2EB,
	FunctionDelegate_Invoke_m5A91A782F5BAC0D6B428F99B8F4736041BCC210B,
	FunctionDelegate_BeginInvoke_mA08550D10019D40D657518FA157FA543F03E69FD,
	FunctionDelegate_EndInvoke_m948F5CAB7D1CB208BA10DE7F57FE726F7FF63531,
	DestroyDelegate__ctor_m10EC24BC0FC7D6409DB1BAE2AA182520D7589B98,
	DestroyDelegate_Invoke_mA21998AA8EBB2D40C97D5ABA24D572D9A92C0C79,
	DestroyDelegate_BeginInvoke_m395A688BDA8B0C26AFF5AA5E19243539D6E4D95C,
	DestroyDelegate_EndInvoke_mF137E5C05472E83FCCA73EF9EA1149F51791E021,
	FunctionDelegate__ctor_m3B3E9DC14D9429470B8C395E0EA67316CEE44BDB,
	FunctionDelegate_Invoke_mACB6E1C587F056EBC17E0940A3BD3CBF202643F7,
	FunctionDelegate_BeginInvoke_m57772D05107C253E5FE982A575453FE4F7B39099,
	FunctionDelegate_EndInvoke_m14769A83B34A2B68B2F55E82623236CCD7171E16,
	DestroyDelegate__ctor_m3BB7638AB145B5134C072FDBF08F98AE74908569,
	DestroyDelegate_Invoke_mC4C2F2DCA035415FDD9DE260A4D8B3B90CCE480E,
	DestroyDelegate_BeginInvoke_mD2AA6C8AC71D45A301E958F2EC86A18381631DAD,
	DestroyDelegate_EndInvoke_m7E794503786446671BF6908DF893BA97E209EDA0,
	OptionalOfFunctorOfVoidFromVideoStatus_get_has_value_m4C5A5EB6B25090AC80D73CF3A3578AF663F68354,
	OptionalOfFunctorOfVoidFromVideoStatus_set_has_value_m798E54F282B4763352BFE839DB21AA648DF5BABB,
	FunctionDelegate__ctor_m8EA8F047D5B7E480FFAB83D7C2CA28A963E44EC5,
	FunctionDelegate_Invoke_mEE8CBEDB93E161FA7FD0705B35481329815CE5A5,
	FunctionDelegate_BeginInvoke_mEB80D100E9E57830F6B0651806DB3B551C49E482,
	FunctionDelegate_EndInvoke_mED27D12DC08E9A5465B5498887068B8290A8A714,
	DestroyDelegate__ctor_m21E28952A0A86A48CD2D2A0F9FBD1F562719E207,
	DestroyDelegate_Invoke_m0F3A0A1A6C02A77A8705661A34761DC4CBDDEBBA,
	DestroyDelegate_BeginInvoke_m765923CB6B874E36AE7032EEBA0A27D9F781DF69,
	DestroyDelegate_EndInvoke_m0E02869B92173A1BE64B79E7874FC603963552E7,
	OptionalOfFunctorOfVoid_get_has_value_m674E9E83E83F23CA50AAE7266ED19DFAC9EEA2ED,
	OptionalOfFunctorOfVoid_set_has_value_m074530599AED813FDFBE0893F9A8D84B30BBB57A,
	OptionalOfFunctorOfVoidFromFeedbackFrame_get_has_value_mD61A0B63DDB2445940E42029006B6E5220AACDF8,
	OptionalOfFunctorOfVoidFromFeedbackFrame_set_has_value_mC6C7579BB351079A6BA5EF5DD10B402EBD9E9B84,
	FunctionDelegate__ctor_m5A5FD6CB94180D93408F9D68ACCBFE4826A67145,
	FunctionDelegate_Invoke_mB60DAA936141A634D8050E1C4FFAA93CC2DBFD88,
	FunctionDelegate_BeginInvoke_mC8076C4E47EC2EDD343BC6C980060443E14C0B9C,
	FunctionDelegate_EndInvoke_m04BFD551DA0922500CA968ED3CA012A003C075A2,
	DestroyDelegate__ctor_m5E218AAD9779E8D53F5847A058AC701C213FEB66,
	DestroyDelegate_Invoke_m263EDC03F81A505C2B492775F6A28F3D6F6B2F39,
	DestroyDelegate_BeginInvoke_m25E177577EC66FFECF423A059A6E81F6ACE282F6,
	DestroyDelegate_EndInvoke_mF01233B1FC427FB1B42631CC077A8C6C98DABA2B,
	FunctionDelegate__ctor_m14AA29712CAC6C54514BE13932411F6B600E0AC0,
	FunctionDelegate_Invoke_m8A4197EF260E0810954C9F4C1DF1513F5EA8F47C,
	FunctionDelegate_BeginInvoke_m3D43A7D9A2DBC3643C8916BE473D594D78A58824,
	FunctionDelegate_EndInvoke_m3BA1C0C451E94B13FA24EA705B0EF99294D5DC7B,
	DestroyDelegate__ctor_mFCEB9949974004BA3352D77660C70D614B55F30F,
	DestroyDelegate_Invoke_m6A872C6D5937FE5764C7502E6067F1465230733A,
	DestroyDelegate_BeginInvoke_mCAFCF7983F4BE9DE4FC6EEA32D859A83D46410F9,
	DestroyDelegate_EndInvoke_m517B13FF95E4ACF5B3C01DD70B0B73DB3CEF685F,
	U3CU3Ec__cctor_m74A22B74D37503717109F6D8808AC60DEE890775,
	U3CU3Ec__ctor_mFCF65758C59A7BDB21C6952983D42B019C7294C4,
	U3CU3Ec_U3CListOfVec3F_to_cU3Eb__689_0_m0FC29BB3B1BE29805B61EC25B7AE61FA44DA8683,
	U3CU3Ec_U3CListOfTargetInstance_to_cU3Eb__691_0_mD08A8E4A39FC322C3F09787ABA8F703B6CAB62AA,
	U3CU3Ec_U3CListOfOptionalOfFrameFilterResult_to_cU3Eb__695_0_mD3BC3FD4F7D312E4E88F002EB853787839845467,
	U3CU3Ec_U3CListOfOptionalOfFrameFilterResult_to_cU3Eb__695_1_mB833AC25AD4E3CCCCFC0B67B5FD822028ED95EBC,
	U3CU3Ec_U3CListOfOptionalOfFrameFilterResult_from_cU3Eb__696_0_m71D204E7F65868D5773B6E8D4A4847CE2B633E77,
	U3CU3Ec_U3CListOfTarget_to_cU3Eb__707_0_m6D5A7A1706E369DFCC7D80F040BE76D27F0F9A3C,
	U3CU3Ec_U3CListOfImage_to_cU3Eb__715_0_m1850A414DB05BCE67375661B4BF9356B2DFF6493,
	U3CU3Ec_U3CListOfBlockInfo_to_cU3Eb__721_0_mF1DEA748299340E1B83F89405AB6DED771AF028C,
	U3CU3Ec_U3CListOfPlaneData_to_cU3Eb__748_0_mE4850FB026635DBF9878B9E258AC578D8C73C5EB,
	U3CU3Ec_U3CListOfOutputFrame_to_cU3Eb__779_0_m2CFCA8BDEE9B5A52C44D216802331C27ED9AC911,
	U3CU3Ec_U3C_cctorU3Eb__781_0_mE6BCB67756EFC6D753A67DDAED38C9C0F5A52503,
	U3CU3Ec_U3C_cctorU3Eb__781_1_m6B55BE835234AAC559C6A8539838DBA5E6D25AE0,
	U3CU3Ec_U3C_cctorU3Eb__781_2_m18FE0ED905FC020D609823EE57EB4EF49BAAA676,
	U3CU3Ec_U3C_cctorU3Eb__781_3_m4EFD8FB46F07A922A665192A8DD970FAAFD3089B,
	U3CU3Ec_U3C_cctorU3Eb__781_4_mFA86F98D192F00BA7FA0931CBBF142F59DCD0A6B,
	U3CU3Ec_U3C_cctorU3Eb__781_5_m887DCF951BEAECDAFDF76DD65696CF796B5F97EC,
	U3CU3Ec_U3C_cctorU3Eb__781_6_mC535A6C45C073D67DDFF17DFF5B92A2DD2424B43,
	U3CU3Ec_U3C_cctorU3Eb__781_7_mDB3B2C1BCF95BB42D4918DB18A470FC373355AE1,
	U3CU3Ec_U3C_cctorU3Eb__781_8_m857149615CABB88B4A3411642083E841759EC547,
	U3CU3Ec_U3C_cctorU3Eb__781_9_m0CF7F9F1250F43AB1DEF5BEFF3A006619254E2A3,
	U3CU3Ec_U3C_cctorU3Eb__781_10_m47438FDE3C8A0C3D09E41F6F27A1A0956CE5BB47,
	U3CU3Ec_U3C_cctorU3Eb__781_11_m1D606F1623D1FFFAB3FD27240A8527DA9FAB6F7D,
	U3CU3Ec_U3C_cctorU3Eb__781_12_m2EB6E1985B59AB6265B6C5B38F784A0E22CF01BC,
	U3CU3Ec_U3C_cctorU3Eb__781_13_m9D1393E1E91A563876381D29B4678D8843F9F5BF,
	U3CU3Ec_U3C_cctorU3Eb__781_14_m1E71C6E2297B08A5F8A5B65376ECE74503103EDE,
	U3CU3Ec_U3C_cctorU3Eb__781_15_mF6AA821EE6C7D2099CA71EB3070875C7EB7078CD,
	U3CU3Ec_U3C_cctorU3Eb__781_16_mF6AEE9E71185FF5809DD9658231B6196E0AF038B,
	U3CU3Ec_U3C_cctorU3Eb__781_17_mBE3B41641A7D5368BA0013A2E3648A8A0E922868,
	U3CU3Ec_U3C_cctorU3Eb__781_18_mB464257FD6BE4BC7D95940493DC2AF3ECC4711A4,
	U3CU3Ec_U3C_cctorU3Eb__781_19_m7F79EAD59280FABC734A3ECB11A0D18134BDD996,
	U3CU3Ec_U3C_cctorU3Eb__781_20_mFC4542077E13E3E38EE50CE8025FC75DC27C2CAD,
	U3CU3Ec_U3C_cctorU3Eb__781_21_m16B5446878F1D49A62AD63DA451878901F289E76,
	U3CU3Ec_U3C_cctorU3Eb__781_22_mB156A5D35AA2F28972DA5969AE8D46D9776E26A9,
	U3CU3Ec_U3C_cctorU3Eb__781_23_mDC8BBE4DA4BA38044331CDA1310C0D674DF474A9,
	U3CU3Ec_U3C_cctorU3Eb__781_24_m0FD1E42BFBF39DEF0A019CD2C90E5BFE33CA0C10,
	U3CU3Ec_U3C_cctorU3Eb__781_25_m6BF649B70FBD11DCC79A62AFF6B0AF183B5275C8,
	U3CU3Ec_U3C_cctorU3Eb__781_26_m119E5B5D8FE7B347AF65BBB43E84988BD93D719F,
	U3CU3Ec_U3C_cctorU3Eb__781_27_mA8321F6396BA2ABC41AAB98D0ABF82451630B175,
	U3CU3Ec_U3C_cctorU3Eb__781_28_mAA1BF5912922932B297C2ED4930F94EE953EB711,
	U3CU3Ec_U3C_cctorU3Eb__781_29_m7FBF0FBFE6588506B22247D6EBBC31B272F6635F,
	U3CU3Ec_U3C_cctorU3Eb__781_30_mD2953D2E79D277099385497EB90D5F1220D87957,
	U3CU3Ec_U3C_cctorU3Eb__781_31_m6BFC588AEF83717BC322B1F20E769C008B2BEA62,
	U3CU3Ec_U3C_cctorU3Eb__781_32_mE13AF98D98393FABF4EBAD6AAFC28B522C29632F,
	U3CU3Ec_U3C_cctorU3Eb__781_33_m21E4BE531D41C3F2448DA4623CCDB034FAAC7418,
	U3CU3Ec_U3C_cctorU3Eb__781_34_m0AEEB7744DF1C692F41D6D9429E612BC65819AA1,
	U3CU3Ec_U3C_cctorU3Eb__781_35_m361BD8BBECADC1D8BC65A190EF3016D75FA7340E,
	U3CU3Ec_U3C_cctorU3Eb__781_36_m039D91F8E1432F61A95E198223B7DF675C1CF5A1,
	U3CU3Ec_U3C_cctorU3Eb__781_37_mDDA91DEFD4FB5FF1A9A73E64543A09E0C19A8B41,
	U3CU3Ec_U3C_cctorU3Eb__781_38_m0D01FAA0D880BCE3574C5AA994C5F058EAC65A34,
	U3CU3Ec_U3C_cctorU3Eb__781_39_m5D92E3273F0B661AFE66993858B8CCFE9EAC05F4,
	U3CU3Ec_U3C_cctorU3Eb__781_40_mA371C7CE6B16F68448B6A1026B25444DE0831499,
	U3CU3Ec_U3C_cctorU3Eb__781_41_m360DF32D8028B52C19E668FB0E888DA9B811F044,
	U3CU3Ec_U3C_cctorU3Eb__781_42_m8802C6B4F2537B3E5B03F3947DB0F1210BDF0138,
	U3CU3Ec_U3C_cctorU3Eb__781_43_m842080962EBB563A84942546C381D0946457E0A3,
	U3CU3Ec_U3C_cctorU3Eb__781_44_m928C3B171D2C4B601F28592D4B7DED349CE7ED94,
	U3CU3Ec_U3C_cctorU3Eb__781_45_m6A920540D273BF152DEDEB43AF8AEE4F7A33DE0B,
	U3CU3Ec_U3C_cctorU3Eb__781_46_m149D77197A8F4067057E97BC7BE6BEA25A3476DC,
	U3CU3Ec_U3C_cctorU3Eb__781_47_m303A4CF48CA7956BA81C3822C243DADCC805EA4B,
	U3CU3Ec_U3C_cctorU3Eb__781_48_mABAB6D857AB480021AD8CA0C6BC4D5C9C08A3182,
	U3CU3Ec_U3C_cctorU3Eb__781_49_m4725A9DB8DEECB696835BA4817E80AA82CB94865,
	U3CU3Ec_U3C_cctorU3Eb__781_50_m30EC54B9BFE5FC0CB365ED6F8CDFEE0D26664A30,
	U3CU3Ec_U3C_cctorU3Eb__781_51_m67A6779679AF48EBB43FD4B2088F42AA5AA5AA1F,
	U3CU3Ec_U3C_cctorU3Eb__781_52_m240E379918EE89535EBB0CFCFC0CF757B9340D99,
	U3CU3Ec_U3C_cctorU3Eb__781_53_m52315F68B2B51E28331BFBCC86AC3B11FA824B0D,
	U3CU3Ec_U3C_cctorU3Eb__781_54_mC0830D51C49514239D760F3E76736291149326EA,
	U3CU3Ec_U3C_cctorU3Eb__781_55_m821AFCD44C5A335BCF73B65320C970312A483B6F,
	U3CU3Ec_U3C_cctorU3Eb__781_56_mC0AB756846BC9A4582FE4A201CFA16E4630C302B,
	U3CU3Ec_U3C_cctorU3Eb__781_57_mD89143A4F0A8C51DF3C1AE1C597900F0FD1E0DE5,
	U3CU3Ec_U3C_cctorU3Eb__781_58_m8E6AC562EA37F78D30751B1E0943CF30A549163A,
	U3CU3Ec_U3C_cctorU3Eb__781_59_mE4ED3483A3DAF625E816B04EB3C71181235A74A8,
	U3CU3Ec_U3C_cctorU3Eb__781_60_mB08EEDDFFE6E77C9A84D93927129F4C5B660680D,
	U3CU3Ec__DisplayClass700_0__ctor_m58ADB096FBD659F9A21C90257E7D8DC885D6DD8F,
	U3CU3Ec__DisplayClass700_0_U3CFunctorOfVoidFromOutputFrame_funcU3Eb__0_m35B0B7F6FF2DC6179D63C5157F514B502578CB39,
	U3CU3Ec__DisplayClass704_0__ctor_m7F92D06FD7ECAF3C25EF03F799B16429BC1AF613,
	U3CU3Ec__DisplayClass704_0_U3CFunctorOfVoidFromTargetAndBool_funcU3Eb__0_m1FA75065AE63D6D1A00569A3A5D14DF72364CAC3,
	U3CU3Ec__DisplayClass710_0__ctor_m94F770671944885F8471A6E5D586A8DF8D89E638,
	U3CU3Ec__DisplayClass710_0_U3CFunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_funcU3Eb__0_mD4D35F19A6180B2D9BE8B4750738E8CCC6D14644,
	U3CU3Ec__DisplayClass718_0__ctor_m55B3430BC64E712EFC5C5FF225B97D997783A8EE,
	U3CU3Ec__DisplayClass718_0_U3CFunctorOfVoidFromCloudRecognizationResult_funcU3Eb__0_m31EC853F846F08D9D99E9A8D983FEAE35DC6A107,
	U3CU3Ec__DisplayClass725_0__ctor_mC3161E17D4B12070F4830FF483B7F72C43A39240,
	U3CU3Ec__DisplayClass725_0_U3CFunctorOfVoidFromInputFrame_funcU3Eb__0_m4680E2F1E98DCE9D14175430427419D60987A51F,
	U3CU3Ec__DisplayClass772_0__ctor_mBCF774809B9D94886E30C3FE04C507C41CD94242,
	U3CU3Ec__DisplayClass772_0_U3CFunctorOfVoidFromFeedbackFrame_funcU3Eb__0_m53DDAFE7A244FF0FB38C3B320BCE53075CB60148,
	U3CU3Ec__DisplayClass776_0__ctor_m80CB201E31FADB519EB01849AE0A375D0193BC1F,
	U3CU3Ec__DisplayClass776_0_U3CFunctorOfOutputFrameFromListOfOutputFrame_funcU3Eb__0_m319C3913F62EE2A5AB8BC0290F0ADF4E0ABF4033,
	U3CU3Ec__DisplayClass776_1__ctor_mD712B4F6AB1E542AE081D5DBF432CFF2BB2FFA38,
	U3CU3Ec__DisplayClass776_1_U3CFunctorOfOutputFrameFromListOfOutputFrame_funcU3Eb__1_m1F62D9936816EDF7413F84D4DE99F657969A7EA2,
	RefBase__ctor_m7A260F9F32D29D8BA70A5673F69375938C959A03,
	RefBase_get_cdata_mB6578F201F1C3F2290BD531478E12F9C7957639A,
	RefBase_Finalize_m8AF2E59BBC04F9AB27B0E8CCE8DA9EDA843679F1,
	RefBase_Dispose_m09A89B3E9CFCF8387C122921A75DB9057C34B9CB,
	NULL,
	RefBase_Clone_mC01B22B577336F9A3FE79CBAB145EF07F757BB6B,
	Retainer__ctor_m306A515EBA144C8D5E370F4FDCA6C2BCD9E19494,
	Retainer_Invoke_m84D08D6C3E2BD7F95B90B21E30DCEEA885E81E19,
	Retainer_BeginInvoke_m3819FD3E451D8CBB2DFCE372F5A31B68E6B67B05,
	Retainer_EndInvoke_m49DF86569EE4A57DF039D26F9F9DD77D28346274,
	ObjectTargetParameters__ctor_m9669D686C9CF5D8C602EB00E73E9ADD0D0B9C609,
	ObjectTargetParameters_CloneObject_m52D6E16285E1AAD07F50BFE0EDB8774E7DDEDD2E,
	ObjectTargetParameters_Clone_m98EE44CC837D901E39B107E6B50297C1A40E81B2,
	ObjectTargetParameters__ctor_mDCEE6534FE4309ADF2E5C1F8852A177A5FA9C581,
	ObjectTargetParameters_bufferDictionary_mDABD4AF642361B07357E875F893BB92C7F5969C9,
	ObjectTargetParameters_setBufferDictionary_mD4C682FBF7F166492EAB351A489C6C03B81870A6,
	ObjectTargetParameters_objPath_m66930AD03179B466A592ABF89211E681009777FD,
	ObjectTargetParameters_setObjPath_mA943AF4181993AF422E4C6C3698EE2BB88B7E205,
	ObjectTargetParameters_name_mE8BE6FFF79E24D4123074842CE235FFA388FA615,
	ObjectTargetParameters_setName_m9621937A37E5A5C203E2EE6038E2C4A7A033F6F1,
	ObjectTargetParameters_uid_mD104B6834CF22CAAB1776D95DA631FCB599FFD40,
	ObjectTargetParameters_setUid_m2EF14E2DB2D12F019CE393B0EBE071E9A8D5456F,
	ObjectTargetParameters_meta_m2B8F6674970B96EC4F5D9B26ECE8097EDEF7A828,
	ObjectTargetParameters_setMeta_mC5E6CA52704E33D73A00430CBCDC468EB0A7176B,
	ObjectTargetParameters_scale_m565BA7669B0C42A6FF44A0D43ECD00F18FADFAB8,
	ObjectTargetParameters_setScale_m2B9A0EBC0ECE6405ACF82DA3C90608C89D4672CA,
	ObjectTarget__ctor_m68992F48171E218AF8531649572C6BBA4CA4B4DF,
	ObjectTarget_CloneObject_m5B5985636E1DB538A771B5D15BB9061572672D3B,
	ObjectTarget_Clone_m7F0766C9857E9807C6601895556DB3A2D0029324,
	ObjectTarget__ctor_m6A0F54F4CFE918B68B95386DEE0A396BF10A8DBB,
	ObjectTarget_createFromParameters_m9C04DC6A127FDEEE748C6D751DB5604209DE6665,
	ObjectTarget_createFromObjectFile_mBA708FA754BA5C0368C5DA7BCC6CAEC657A372FC,
	ObjectTarget_scale_m09B24B77852B8719DE926BC60DBBBCC641D05CD9,
	ObjectTarget_boundingBox_mD96A4B4D5AB63C5F200A007E7ACC504887A0FB18,
	ObjectTarget_setScale_m5D131415E3D6E79BECA6A09FF2B4FC2BB9F6F9F3,
	ObjectTarget_runtimeID_m1CE1BB1328D80FE9E2BA215D98F099B91A0DF421,
	ObjectTarget_uid_m04C010BA156AC4671D80DA47A9A6651B9956A441,
	ObjectTarget_name_m3CDBFE5595AB0167264CF08E219362BD349EC580,
	ObjectTarget_setName_m9435E85FB7961205A76CB389A036F9F0C9D2A213,
	ObjectTarget_meta_m16BABD79332153CC88486600A2D7405FF35B378D,
	ObjectTarget_setMeta_m85E56ACEDF13E5C587875552B20FB667F7D9B192,
	U3CU3Ec__cctor_m2449B1C4193B993B7B828B3DA6188ECF9A2F6D08,
	U3CU3Ec__ctor_mACF643E2FADC4F362E13889C73B44F31F0C78591,
	U3CU3Ec_U3CcreateFromParametersU3Eb__4_0_m708243164B6FF02B2A2C3C659F3C3AB70A97F0F0,
	U3CU3Ec_U3CcreateFromObjectFileU3Eb__5_0_mB6507D12B94221377FCE750D05AD47AD210E2ECA,
	ObjectTrackerResult__ctor_mEFE14AAAE06BDE7EFE6DA5A8F0F960A72F6F5399,
	ObjectTrackerResult_CloneObject_mC581232CCFEF3671C757E46AAB1574ADAAC65FFF,
	ObjectTrackerResult_Clone_m7B45868A7366FE8400AAB88DFBFD29F3B6D31DC8,
	ObjectTrackerResult_targetInstances_m1C59A3D86CED84EB2BD079E4FCFDAB35D070353A,
	ObjectTrackerResult_setTargetInstances_m7C8D9D96CF22EBA0CEE974E091CAE38DEEC16682,
	ObjectTracker__ctor_mC9AAFF5DAF891EDBCEEBAFF6D18D1FDCA103F610,
	ObjectTracker_CloneObject_mF854A05BC5751690BD291E1805265045E2FE58D9,
	ObjectTracker_Clone_m8C3C6381F47B13EC0E308FC683C78054C1742C25,
	ObjectTracker_isAvailable_mA0E12804D767C6224A3E85C5D8340455B68D6AA6,
	ObjectTracker_feedbackFrameSink_m22AF786EE2ED243911CEE1045BB86D497B134D2C,
	ObjectTracker_bufferRequirement_mAFE23598F1CB287131F494B263C9BA6AD51DFEFA,
	ObjectTracker_outputFrameSource_m70525490C852BA61617E160A348E7AED3B71AB58,
	ObjectTracker_create_mF19D20FC41A908A8E412EF86FBC668DCD2066E69,
	ObjectTracker_start_m33F86D8FD7E3BEED463AEA9D83084A9ED60E2AAB,
	ObjectTracker_stop_m5AA63DBCBD8DCFD713AC3EC35D30D465756CC72C,
	ObjectTracker_close_mA3FCF12B8710CF8ECC5D88146F68121248ABB005,
	ObjectTracker_loadTarget_m343C289BC990D2DD3DD6288FB44D89FD33B63CC4,
	ObjectTracker_unloadTarget_m85CFF1CD41835E53E1F15C039AB82AA1AA888BAF,
	ObjectTracker_targets_m2BBCABB0117FEB7F0D887CB7CEA1A521E6BD0008,
	ObjectTracker_setSimultaneousNum_mAB4E3B1EEAA501290676993A90411D1127B56B63,
	ObjectTracker_simultaneousNum_mCF70C03A178939D5A355BFF9CED73D0D4592D163,
	CalibrationDownloader__ctor_mCBD686C41DA88862DFA184064D237DAAFDABA326,
	CalibrationDownloader_CloneObject_mEC861D203AB61A4986F731A2B22C2735E6421209,
	CalibrationDownloader_Clone_mC43E57EF413B43F6B08339ADE806ABF5C851C47B,
	CalibrationDownloader__ctor_m385A7C9E24B4EBF37FACEC0A5BBF79432586FB36,
	CalibrationDownloader_download_m3C68F6381860DD1ED668A7928B1F7AB39981918A,
	CloudRecognizationResult__ctor_mC669149A29321DC58354C778438BA2913149ECAE,
	CloudRecognizationResult_CloneObject_m1812DE6EBB1AFB3A87BE4E59AF9D3779CB05CFBD,
	CloudRecognizationResult_Clone_m5BFBC0C5EDFFE14994D4EFED2159C2A2DD538940,
	CloudRecognizationResult_getStatus_m03896D43151D010F0A564CBCDD9AC36225707731,
	CloudRecognizationResult_getTarget_m6FAF44E37DEACC260AB11F9ADB4BA1EB79DF289B,
	CloudRecognizationResult_getUnknownErrorMessage_mC906B72E0E8255D0556C6985EA88C7B3AFB6DABE,
	U3CU3Ec__cctor_mC706284EAFCEB13EF8F05750A9F4B1800A635CDE,
	U3CU3Ec__ctor_m1238C3B1824BB665CB1874E52203FD33B2954334,
	U3CU3Ec_U3CgetTargetU3Eb__4_0_m11B2033894190021C713D88B7B2467FEB0F34039,
	U3CU3Ec__DisplayClass5_0__ctor_mD29F827AA6A906349C7BFC7AACEA6354FF64CE2B,
	U3CU3Ec__DisplayClass5_0_U3CgetUnknownErrorMessageU3Eb__0_mC3E207C843BF6FE15A1BEB78C85C6494AA742F12,
	CloudRecognizer__ctor_m549EC085B4AEA5F31E986060A9180A02A3EFE417,
	CloudRecognizer_CloneObject_m6150B1A145DB15194863FA671E6F9A8936E796C0,
	CloudRecognizer_Clone_mE1950F4117AF253174A743CD6886B3156C75D462,
	CloudRecognizer_isAvailable_m37098B8EB063BFA93CFC16985D27011228B42517,
	CloudRecognizer_create_m3F53BEB7A5732EBBE687120A53736AA858A249A1,
	CloudRecognizer_createByCloudSecret_m71E6387B3EB2E6848B9E40F13DBBDD949A53F54A,
	CloudRecognizer_resolve_m2BCF01C12B51ED1533D8CCF3F5606D49DA496CD1,
	CloudRecognizer_close_m4E6786F893C4A5B7620E2F250EA52CA7248BF508,
	Buffer__ctor_m15ADEF3BDB6E349FCE275463A209CDCCF8EA3465,
	Buffer_CloneObject_m346C1DDE9E8B6E7C1421AEABD52A717770B70487,
	Buffer_Clone_m5F3C17C6CCB50D9BEE76EEB7A9E128CD40D6E4F7,
	Buffer_wrap_m4C7E982C40E9E049D57EA1F551768A0BD4B52520,
	Buffer_create_mA4045366B79E8BB40127703CFA8C029245789B8A,
	Buffer_data_m889C1CA1DD900605C323F69DFDB7ADD1F3302662,
	Buffer_size_m7166BFEDC0284B4423E619AB184B053F773309B4,
	Buffer_memoryCopy_m05CE603F8CFE5330474FFC73C2B3BACD9B9BD9D7,
	Buffer_tryCopyFrom_m95FE694E61D7F1A94E22DDCE3BC01C7A5A2FA88A,
	Buffer_tryCopyTo_m587CA05561E493D7867708B89C21375DABC98469,
	Buffer_partition_mB867839C93BA3E330AC9C8CDDD02B63CD02FFF98,
	Buffer_wrapByteArray_mD81AFE8D8ABA7B59044BA60448F30B5233FF5D8B,
	Buffer_wrapByteArray_m627F2C23AE3CCA2B4A216C9BA0DD14A03FDDB6AE,
	Buffer_wrapByteArray_mEE6301024DE0CC550363B6E3A6620D43B7081FCB,
	Buffer_copyFromByteArray_m6F41156DC2DD490C222564708B1C37E535231837,
	Buffer_copyFromByteArray_m56B142C6DCFD152829CFF3465514EF36F75B0A66,
	Buffer_copyToByteArray_mAE5E07696CEBC141307CB2F9F36B606756F96664,
	Buffer_copyToByteArray_mBC783F00807578362CA594BB1611C00ECF4A6C06,
	U3CU3Ec__DisplayClass11_0__ctor_mDFFA4231ACEDA772E5728302D601A5B9FEB3B830,
	U3CU3Ec__DisplayClass11_0_U3CwrapByteArrayU3Eb__0_m9A8C109B13512123C7280DAE55BB2B11DB05E94A,
	U3CU3Ec__cctor_mFE7F01DBEDB32B327BB1A8B8A431053EE12331B9,
	U3CU3Ec__ctor_m846AD8143B92341281505FC87169252808836CEF,
	U3CU3Ec_U3CwrapByteArrayU3Eb__12_0_m695AE061FBD5320FB583FEC24F79E28351D12489,
	U3CU3Ec__DisplayClass13_0__ctor_mD633A27694742ACDF76E85A1360A364C57805213,
	U3CU3Ec__DisplayClass13_0_U3CwrapByteArrayU3Eb__0_mC8E3ADF8FF5DDA971C36B4AE593D9F1540F2910E,
	BufferDictionary__ctor_m05DAC6A1EBD39FF1D31B7C66D9648A2D6FF21127,
	BufferDictionary_CloneObject_m226827952B94424EC09E81CB8DE24ADD8A7AB073,
	BufferDictionary_Clone_m3D65AACF15B2058C38EA5C630A8A71E0C804B47D,
	BufferDictionary__ctor_m8B1D8159857DD9183FA7B1C36CD2CA74EA069041,
	BufferDictionary_count_m5D4F6415665F7A3E7D32C7DF5BD5A75B39C1DF38,
	BufferDictionary_contains_m47731845154BF9479676B88E49C3AE58B1B3BF07,
	BufferDictionary_tryGet_mF2C0B2C892BDA5714141763E78AB944AC0C87447,
	BufferDictionary_set_m174A99369588270A81FDB69E5F58F8D64BC78B58,
	BufferDictionary_remove_mA6E5F4646001810EA805E891F5537DF864EA9A34,
	BufferDictionary_clear_m4F33E2473C29CAA14EE2A49556885FC0AB8A6B7E,
	U3CU3Ec__cctor_m83631C33FE0F04211C0C208B13EA2D7480F1DC24,
	U3CU3Ec__ctor_m44B6C2E1AD3CBB5500FB27B81E30A2A0E275389D,
	U3CU3Ec_U3CtryGetU3Eb__6_0_mE5FD321D54AA7005B83049E3B648E965DEAA68BB,
	BufferPool__ctor_mCB97A4F9ECA2945A30B3E7515518D5D8CD0A166D,
	BufferPool_CloneObject_m2B1129C26B03DB5890F4C2A698D9B51EC3B1ABC7,
	BufferPool_Clone_m1CC7F95E98E2F0102F8A45653ECABEF459439A16,
	BufferPool__ctor_mD4C3F3D08507E4B84FB7EB3DFE2DE7514697BC0F,
	BufferPool_block_size_mFF35EA176E14727BB9A084832CCCD65F2F426CA9,
	BufferPool_capacity_mDE7A9D087B941FD53A1821D6D283121791CB9434,
	BufferPool_size_m247DE5CB827CDC3CBA15ECA263C9777F05C8E1B2,
	BufferPool_tryAcquire_m7FCDC71EF2175E1D1531DC31F2864D728272341C,
	U3CU3Ec__cctor_m527B3EBE09B6F7BE0A49CD5ABDE9082A34147F24,
	U3CU3Ec__ctor_m7EEA7C50E5C1795F5A827566F161F37B43C05E10,
	U3CU3Ec_U3CtryAcquireU3Eb__7_0_m7DD05F73A3E4763753BB2933C790CE2777768E28,
	CameraParameters__ctor_mF53D0258399082E9BBAD2D3E34C4CEB409B36E88,
	CameraParameters_CloneObject_m365759F654816E583AFEEDDD2DB72FB9CBC27926,
	CameraParameters_Clone_m8F39D18CD3B9DB63770AD273AA7B36E749031283,
	CameraParameters__ctor_mEC67E27AC76DF14C51421A495C56E092F8828B25,
	CameraParameters_size_m5A08D3EE034A68FB064DEE5654D748055799E85C,
	CameraParameters_focalLength_mE5A438ECF9352D53170C269AE41B9A834C41B00A,
	CameraParameters_principalPoint_mBA2D8F9CABF39378426F9488E1DFA6FDF23E70FA,
	CameraParameters_cameraDeviceType_m9B6F4FAEB296E769EB507BE9740754589ECC32CF,
	CameraParameters_cameraOrientation_m0B71366F65782C1D572874D28E963597B86EB9FE,
	CameraParameters_createWithDefaultIntrinsics_mDB2D0A438040A5F8D020B4DDFD1C6DA5FAF740D8,
	CameraParameters_getResized_mA7097D3A610D2617F9F6B1EF2947A0C4A941B32B,
	CameraParameters_imageOrientation_m81CB8939086AEFE115ABAB6AA5573A1EA44FDB29,
	CameraParameters_imageHorizontalFlip_mFC73FF33F2788DF324265F4C2C03F9297856DCBD,
	CameraParameters_projection_mA20C66EBCC9733301C8EA3E9A958CFF7D8AE20E5,
	CameraParameters_imageProjection_mA27D176CBD0FF41E6E34C2337DFC2B1D044AE05B,
	CameraParameters_screenCoordinatesFromImageCoordinates_mBEC8FC46278277DD00AAEDA7A88543C31C9DF396,
	CameraParameters_imageCoordinatesFromScreenCoordinates_m91AC0C1514097466C81A7E5B7FBD05FC3BCCC0FF,
	CameraParameters_equalsTo_mE3B1CB1DEF8B5E449E4E63F8DC83B3931724F087,
	Image__ctor_m3A17CA109931FB1778D0C13FC5D55D207272920E,
	Image_CloneObject_mB995191457185397E38780E59AFAC512D6DD97B0,
	Image_Clone_m3B57CEA8B825C8AA6408C266B8ED203BFD32DAB3,
	Image__ctor_mCBC1A7A24107EEB4E5B21860F46D2590333781DD,
	Image_buffer_m0B65E96E5DD0CAFB05510D452F82636051EDFE59,
	Image_format_m2F44AE1A06FA17797FB7D4CF2C59AEE46DAC9BA8,
	Image_width_m4DACAD96F77D6C3110FD681F9DEF88DB1550F7AB,
	Image_height_m946798A63C51B5546864C29CC88E2A58D01CDAEF,
	Matrix44F_get_data_m67C0A1BF271A6C366D67ED360291B61197CCA09F,
	Matrix44F_set_data_m7C3634F7119F87B60156682FA5A868E25791AB97,
	Matrix44F__ctor_m08360D8DD00F4951F4B3A7905BF5F08D3D865036,
	Matrix33F_get_data_m85D37D7D472AB79A7962EDCB3D01E70F85FDFBA4,
	Matrix33F_set_data_m501DC97575DC9045BCBEA35973CEE131C695A5A0,
	Matrix33F__ctor_m3683E7AB93523F0178BE0A9E5C8961B02A49F44E,
	Vec3D_get_data_m899EF2E47941ACA4FB20AC16FAD0EB6F52C2820C,
	Vec3D_set_data_mA768D5E64217EEE2119C35022EB2BBB34EBA91DA,
	Vec3D__ctor_mE118A85DCBF981E4D1FB28A10DB58FBCCCFBC3C0,
	Vec4F_get_data_mAB650AF267950B46EC508F2C8B93F26FCF8363D0,
	Vec4F_set_data_m62535D380DECDD9CD28F7A778EDCCEC0B26C5FA9,
	Vec4F__ctor_mD5ED30DAD10566BBAE3D3F24E5130A516D77269E,
	Vec3F_get_data_m6AB194630146D7297D96735F9E95AD83C044604B,
	Vec3F_set_data_mEECA60CD3B2A9829F912884D06CC385A63B5DDB8,
	Vec3F__ctor_m048B740E6E14E759A3AE6BA9EDC537B35D3F32C8,
	Vec2F_get_data_m2BFD2DF1F83590E815DBF7FD2F18F2A74251DE13,
	Vec2F_set_data_m511CE6F198ED6BF129F53080ACE5255A69BC7C44,
	Vec2F__ctor_m54CAE94E831DCCA2714C0B2C38581F238452933D,
	Vec4I_get_data_mA13BC5AC938DA168120751B0301BC644D7ADDD91,
	Vec4I_set_data_mE1291E4B6CF192491813EAC0EA8BB331B474EBA2,
	Vec4I__ctor_m9716FFE4C4B6410244BDBD51F1445186E25A6B08,
	Vec2I_get_data_m8C330A4BEF24736568B464D9A97FE12108A53218,
	Vec2I_set_data_mBD37D15E328F8F2746003C169F93976A499A342B,
	Vec2I__ctor_m79DAD76534551BB0FC8246F6948CE5BF9DF38045,
	DenseSpatialMap__ctor_m5BB2C39D4F330658465A57782CC7B2A28BA21CA8,
	DenseSpatialMap_CloneObject_m5AFB1E1D9CD91E5E0FC884CF2D55580C28E599B6,
	DenseSpatialMap_Clone_m5EB116673DE5BD9080F5B7803E1EC9722E8C2621,
	DenseSpatialMap_isAvailable_m6B6574FD47D4F2FD406E7ED29CED29D5D192B808,
	DenseSpatialMap_inputFrameSink_m41BFF5AEC85CD2B62B9D34C5DF6170D5E902B2FA,
	DenseSpatialMap_bufferRequirement_m12A7A57946D3953E8CEBC6E14F9847E338F47AC3,
	DenseSpatialMap_create_m035D3B83C875BFA994E7915E6A5ADF7F3E5C1520,
	DenseSpatialMap_start_mDA01BBC0029B5E5C4A6E6366662F9C4F42710F67,
	DenseSpatialMap_stop_m624752486F3FBFC8C0FF8827EE955CCE91E2D29C,
	DenseSpatialMap_close_mE781ADFA39C8FC16C5FA93A3F6FE3014EF29A1DB,
	DenseSpatialMap_getMesh_m2495EB5C19043082157D3003F7821CD63B556708,
	DenseSpatialMap_updateSceneMesh_m5874771E5E9D1367191AC73287363967B8175F67,
	BlockInfo__ctor_m56026AA781BF97B7CE29DF961342F00D1B57FB52,
	SceneMesh__ctor_mAC41970C535E5CFE31957B36C4ED44161AA480B8,
	SceneMesh_CloneObject_mF808CF8F328E919C18930BFF8057E5CC6B21249D,
	SceneMesh_Clone_mFD22EC5DEEDF03AFD384FC50BF4A4B1BA07F2797,
	SceneMesh_getNumOfVertexAll_m7DC63EAA58B0492CED10933128E9747D56DEB3F8,
	SceneMesh_getNumOfIndexAll_m6B193D9A8E8E581751506375B68FA8DE5CB0C64A,
	SceneMesh_getVerticesAll_m35A41676BF505C872EA9B352C30BB08F1D500E7D,
	SceneMesh_getNormalsAll_m2A1C7B1C1AD74CF95F1C95284072F2EF0ABD7010,
	SceneMesh_getIndicesAll_mEC6D584B9A11ACAB3B40991A90296370EC27D749,
	SceneMesh_getNumOfVertexIncremental_m82473C34926A654823CE08884E4EAAC0B53B71AC,
	SceneMesh_getNumOfIndexIncremental_mB309AC2B20E1107DAFA433FC40FAAC3CD6DE24B0,
	SceneMesh_getVerticesIncremental_m90A4B084DAB8ABF96DFEDE372680F46D1A5B7B0A,
	SceneMesh_getNormalsIncremental_m273A33F15DBCAFCEA33B90FE845A6F79937928F3,
	SceneMesh_getIndicesIncremental_m77276CBF70BD66A5B2575660C5B19FFACF621A29,
	SceneMesh_getBlocksInfoIncremental_m4434A60FF52661FCAE293AD6BC82FE0A6475CDF5,
	SceneMesh_getBlockDimensionInMeters_mD647782340C84AABE7C954F74E68ACE57B10B6F1,
	AccelerometerResult__ctor_m6B5CEB6914B368B1E58761FF3A309544749E2716,
	ARCoreCameraDevice__ctor_mAEDACEC9650C9F4A84D9AD2EA0E6A8579FEE1CFB,
	ARCoreCameraDevice_CloneObject_mB9A646252AE4F3EE73EF3D4CFC1B67AE7556B30E,
	ARCoreCameraDevice_Clone_m892747C253B9316A7463ED38FFBFAC6E3DB86E45,
	ARCoreCameraDevice__ctor_m154ED25404E862E35AD29E064342DA0F6EBD09D5,
	ARCoreCameraDevice_isAvailable_mC38AE95378C93C93CF3537961AB608CD9BC43110,
	ARCoreCameraDevice_bufferCapacity_m107E36E69C56A7041CA64AE1387758508AA6E51A,
	ARCoreCameraDevice_setBufferCapacity_mC7E39E57A21ADC3D71CD896FA3754385A54AE36A,
	ARCoreCameraDevice_inputFrameSource_m364B9618EB13A9ABBBB7AFBEDA562BA17CABA470,
	ARCoreCameraDevice_start_mF639D291680E787DB2790C06529900D73771A4AE,
	ARCoreCameraDevice_stop_mF9BFF73E2674A71D2AA87D0A29011B9528A44588,
	ARCoreCameraDevice_close_mC2EAFD194FC703248E574A79AB6978581C343017,
	ARKitCameraDevice__ctor_m707437B9257ADD75B3B7C3D3D330561782F8AC6C,
	ARKitCameraDevice_CloneObject_mEF03F52792010F9966C9AB72C4270F28C0592CC9,
	ARKitCameraDevice_Clone_m1E3D2158103B93B0CADDDF0205F136D4949B37B5,
	ARKitCameraDevice__ctor_m7FC6BE878E2A05951A09EF6664B54B8DF7B765F7,
	ARKitCameraDevice_isAvailable_mCC4AF95C7EBC02EA5B254EEBF50956E34D485D6D,
	ARKitCameraDevice_bufferCapacity_mFF0E9B37001CD204EBEC63CF319F6E6073266007,
	ARKitCameraDevice_setBufferCapacity_m21C8B38B98683576978C6723996B16E92948A474,
	ARKitCameraDevice_inputFrameSource_mE64BA2EC3E64FB2D5436F7676D436121E08BEC22,
	ARKitCameraDevice_start_m9C78E1885DB9F6AF821D04059F659D14E6A51378,
	ARKitCameraDevice_stop_mE89AF469B150603FDCB5742C1239866CA2035D6C,
	ARKitCameraDevice_close_m426689D5398519BA49A78D1A8271FA4DD52A2036,
	CameraDevice__ctor_mA4300807CC1F45E23E64580BD640ACEC88B400AF,
	CameraDevice_CloneObject_mC30B7D6E2FDB67F994249CCB19BA76E87BC82613,
	CameraDevice_Clone_m86FAD90C8A2E66528CB6A494BF8A56808F3B35E9,
	CameraDevice__ctor_mC79009D8FA04772615607AE2414194D744519EC5,
	CameraDevice_isAvailable_mC2DF7EC79FF272120FF31F0113E4732271D532B5,
	CameraDevice_androidCameraApiType_m8F819329DB6CEAE655C14E0699B726CD619FC2B6,
	CameraDevice_setAndroidCameraApiType_m0C0467F6FEC395BDB82EAF94061B3B930443FBC6,
	CameraDevice_bufferCapacity_m8D3CCA91D44C67422024236D2FC6706C3A6E04A8,
	CameraDevice_setBufferCapacity_m7CB10C72113692B3A221B5477949EDFDCD2C347F,
	CameraDevice_inputFrameSource_m4556B6355CCE3A0E3A10D468F7522B0B6191EB1B,
	CameraDevice_setStateChangedCallback_m79E20475BF8E1AC96A2647EDD2271BD2AD99D9EE,
	CameraDevice_requestPermissions_mE6DEB5F0EFE6DC377351CA1BC3028B5F83865372,
	CameraDevice_cameraCount_mBA8183A787FCEEE08F92773B60F2C649755A83E5,
	CameraDevice_openWithIndex_m5741C367F102F37054FF604D8FC5D51AD2CEEBD9,
	CameraDevice_openWithSpecificType_m18528F4CF8844CBCA093B75114980B64ECEBC3C8,
	CameraDevice_openWithPreferredType_mB46DF3D308E5D9C5493AF7985D3375EE5718DF3D,
	CameraDevice_start_m5F41109B9C1980B748AC594BEB7AC83FD9F4B5D4,
	CameraDevice_stop_m097BCE97FA5350AEC1F03E6D0D5AA89211BA905A,
	CameraDevice_close_mA16F3A0C432F99B32F573A3C2BFF9F01E4D0DCC2,
	CameraDevice_index_m1A2352F68C8EFB7F07A63936C724FC22506331A4,
	CameraDevice_type_mE077C41A6D61E80D93185B1752202DD5D1D30981,
	CameraDevice_cameraParameters_m27A63E80272CEDE038377CB93B2079210613AE7B,
	CameraDevice_setCameraParameters_mDB9FF3997B32F60E2CC2948434989675165BE762,
	CameraDevice_size_m2B5E07D077A823C96B3D0613F26101974A6457F0,
	CameraDevice_supportedSizeCount_mB86F2E42FE2A5B45AB19019B87C7AFD4DC0B6286,
	CameraDevice_supportedSize_mB034074836F2ACC09F0EBEA26A972455B086A9D0,
	CameraDevice_setSize_m62AE09B493AEADDD05D514897953808109979E53,
	CameraDevice_supportedFrameRateRangeCount_m95AB3C6964C40380D2CFCE1C5162E408F6CEAFD6,
	CameraDevice_supportedFrameRateRangeLower_m3CAAEF79A75991245552564D5CCE3BC9B0C8D012,
	CameraDevice_supportedFrameRateRangeUpper_m327F6F7BE1ABE59690A75D8EC5C9D83F401318E2,
	CameraDevice_frameRateRange_m2381B13ECC287979AFC0896AC773E7278C71FC14,
	CameraDevice_setFrameRateRange_mA5E58D39FA142E4CA04B5A06C5DC3573A52D9F5B,
	CameraDevice_setFlashTorchMode_m67989847859EE926D8115F9E8B3B415883AAED01,
	CameraDevice_setFocusMode_m0E810E1DF85A53658C3A52DBBBAEFD03A8CC131F,
	CameraDevice_autoFocus_m28FC50CFBE04C54E171C47ED6842AF024BFEC6AA,
	U3CU3Ec__cctor_m56DC48F1AA57BCD8E0C0A0928C29F7D0DDB9F5F6,
	U3CU3Ec__ctor_m95E32A3042FD542402DCF80B63AEBD99EC2B6903,
	U3CU3Ec_U3CsetStateChangedCallbackU3Eb__10_0_m594EE6FE03345C3592A0D3E38B51F56189DF1652,
	U3CU3Ec_U3CrequestPermissionsU3Eb__11_0_mAAFECC9D58FCA8A81F9AC6E23FC3A26551A00BD0,
	CameraDeviceSelector_getAndroidCameraApiType_m1D47715F5C4A9C82783CE1D1D3286BF286C363B6,
	CameraDeviceSelector_createCameraDevice_m976B4B095BE5DD8EF6B42966E8414EC50EEB901B,
	CameraDeviceSelector_getFocusMode_m2D8E298925AA0D5513C9BFCCC342C445E38929BA,
	CameraDeviceSelector__ctor_m005E4293B632B7C17388EBE791006A1A8B8715E8,
	MagnetometerResult__ctor_mD167F23B0247DBCF93D8EBA039DDA2822DD8E3ED,
	SurfaceTrackerResult__ctor_m7F947AEABC6F84ABFDF837C4AFF5C9184F50479D,
	SurfaceTrackerResult_CloneObject_m83D3A5DA96518D39DFBE63FB31912DB5EA7C7878,
	SurfaceTrackerResult_Clone_m326332F14411FA391812B705D8D8072721A8B692,
	SurfaceTrackerResult_transform_mAE43C2EECDCF3277A602CCBAE181567153C85D1B,
	SurfaceTracker__ctor_m39E346A1231B0AEB65B01018EE9B9F36EDE1223A,
	SurfaceTracker_CloneObject_m3F3F9446CE2DB92115BD6BFADCC6C8B9DE44891B,
	SurfaceTracker_Clone_mD376A8CFEB9537FC279EB49DE87A8F40E3647977,
	SurfaceTracker_isAvailable_mF38E81B97AD39BB7F6585F7E928922CE04E0D003,
	SurfaceTracker_inputFrameSink_m5364A7662F79B66A6138E04CB2E134A7D65386F7,
	SurfaceTracker_bufferRequirement_m007C8BC91C5978F0F24E0FDB8ED84DD015C921A2,
	SurfaceTracker_outputFrameSource_m77935476AF414D786F06AAB495841390A1088973,
	SurfaceTracker_create_mA471FE9DD43F301B5239A57B904512D2169A5559,
	SurfaceTracker_start_m31F700B9800709871C2B9704BF3AAC872D0337F4,
	SurfaceTracker_stop_mA51E429D1506FC349FAA1AD81FDB484F0AB21380,
	SurfaceTracker_close_m49B362D2279077EE69F9668080BBB9913DEBA4E9,
	SurfaceTracker_alignTargetToCameraImagePoint_mE76790785A7041778B722C3207D46F74A7ABB838,
	MotionTrackerCameraDevice__ctor_m3C17852987A8D09F55DD80DBFF5C02B7866EE023,
	MotionTrackerCameraDevice_CloneObject_mD72AEC7F6AABF7ECC3FB68385F926E6D54978703,
	MotionTrackerCameraDevice_Clone_m147C5193D30DC076C6D004FBBA54ABBC034C10B4,
	MotionTrackerCameraDevice__ctor_mF26DC562E6E4C4740D87354C1DDEC4A126D84C0F,
	MotionTrackerCameraDevice_isAvailable_m8BC93BECD04D2E3445696927404176B9D7066188,
	MotionTrackerCameraDevice_getQualityLevel_m4B6496FEFD8B0E3E3A6B8E877362114CDAFFEF6E,
	MotionTrackerCameraDevice_setFrameRateType_mF86259740166B8359AFE2652CE556A57E03E9F49,
	MotionTrackerCameraDevice_setFocusMode_mE11DB215BE7CBD8DF6AF4C638F0B7C8CC7F65B99,
	MotionTrackerCameraDevice_setFrameResolutionType_m92CF0535F26D8F58189E00A7332C46424CB67599,
	MotionTrackerCameraDevice_setTrackingMode_m9694E1D9A30B2CF569900CCCD07FDC73C151A9D1,
	MotionTrackerCameraDevice_setBufferCapacity_mE36E7B5E21B7213B192E5FCAC41CDFF49E5827F2,
	MotionTrackerCameraDevice_bufferCapacity_mAB3E322CEA29AF30A4A4B6A35E3A8547345C85AD,
	MotionTrackerCameraDevice_inputFrameSource_m71533CE3ECBB279E8FE1A02B84A5CF553BDDE9C8,
	MotionTrackerCameraDevice_start_m2DE73E8899A1D5EB0F3206AD1B2C8E46FBC79E6E,
	MotionTrackerCameraDevice_stop_m638E96EF3CC032B46424597BC4A07381515ED990,
	MotionTrackerCameraDevice_close_mF6D8E415DF82B25D17C36A2432886D587AAF8D63,
	MotionTrackerCameraDevice_hitTestAgainstPointCloud_mCA9CE1F287E0AD96DA629D7C4FE6471B383D2B2B,
	MotionTrackerCameraDevice_hitTestAgainstHorizontalPlane_mEDFDC32F066AEC4329DC361B0ADA4B3D78500CAC,
	MotionTrackerCameraDevice_getLocalPointsCloud_mAAB4C53D1F9A79F92D954E5181792CD9D2401605,
	InputFrameRecorder__ctor_mA84AF7039033030E16CA40897C574C5D4F333346,
	InputFrameRecorder_CloneObject_m639FCABAF42760CA99D3EAAC6D981E527E980095,
	InputFrameRecorder_Clone_mE8A60C3B0365DDA74C7B53769649AEF0EEC40F02,
	InputFrameRecorder_input_mEAF91E579CFE7DFA400E2F33C4B4F3E48375CBE9,
	InputFrameRecorder_bufferRequirement_m8BCDF510F87BEA9215A2F70D291C24924890043F,
	InputFrameRecorder_output_mDF1ED83C9137703917FAE94196096B62ADC01BB9,
	InputFrameRecorder_create_m2890A25B4B9A2C63EB6C7517AD39D50AE97737BF,
	InputFrameRecorder_start_m2DA0510E1EE5CC85BDE51C7A156BBEC2ED022551,
	InputFrameRecorder_stop_mCD1782C3989F3DF1F7A0777A54B43C23454E2AE0,
	InputFramePlayer__ctor_m686FF6D124EDE43E2255C0114E0B6D633163D00C,
	InputFramePlayer_CloneObject_mE7E06D95D4C6D4189C6B9524711F9C6E13A946F1,
	InputFramePlayer_Clone_mA1DAD872962E6DA942BA6A6BFDC1D6EDB441F522,
	InputFramePlayer_output_mF8418E49369189C5BBA0B69A66294BC7468B244C,
	InputFramePlayer_create_mF5E3DC126A7131C25633FE677A6D8EC9A6B63303,
	InputFramePlayer_start_m38A3BCBF6D4CF0CFA042CF4B721AE77FC4DC11C9,
	InputFramePlayer_stop_m1C3BE269E1356E65B93D8FDCAE3ED7AE30BB659F,
	InputFramePlayer_pause_m0053EA1F8FCBF581CB9684BB90453321E5980C7E,
	InputFramePlayer_resume_m4C82C6E4F5CC6C3437CD1AAAFC82CC94A202C7A4,
	InputFramePlayer_totalTime_m7854A0C11FB86D16CC86F99C3497C0C5107F7F1C,
	InputFramePlayer_currentTime_m009C2F432E987987C1C104507C101B0FE5B213EE,
	InputFramePlayer_initalScreenRotation_m55C52B6A1A7A158CB72DE898D9891C7578D48A22,
	InputFramePlayer_isCompleted_m1F9BD2B6469C2BDF5DD50401A8573956D7D11C86,
	CallbackScheduler__ctor_m1FF487731C76639AA70B1C01B0119C5CB3B0908D,
	CallbackScheduler_CloneObject_mF1D5B9116B68AE6E2345FA12B5256FE7EB51D6A3,
	CallbackScheduler_Clone_m68E7D2D53AE88A6C7F449D49B6372684F0D2C580,
	DelayedCallbackScheduler__ctor_m9C0D34522C1CEF32F46800BBD017908FDBBD9D74,
	DelayedCallbackScheduler_CloneObject_m080302D1CE9ECB6969262D245FF509C06A048D2D,
	DelayedCallbackScheduler_Clone_mCED824236552F2383F03BFB974E004BAA21CE96D,
	DelayedCallbackScheduler__ctor_m486B5C0EA07183C990EDA2477B400CB0BBE2887E,
	DelayedCallbackScheduler_runOne_m6B365DDFC8C9206F0C3D9A5C587D93713381384D,
	ImmediateCallbackScheduler__ctor_m5067F45CC54460DA2E20D12084221FA66BCC25EE,
	ImmediateCallbackScheduler_CloneObject_m7A70C79A5B2639469AD4BC804ABD4E5DF2222521,
	ImmediateCallbackScheduler_Clone_m73BA43F3EA7DA9C2D7F144E27995DC6423E8E6D5,
	ImmediateCallbackScheduler_getDefault_m07A4A8FC45D20C1F0AB07672BAC6B94BAAADC991,
	JniUtility_wrapByteArray_mF0E256B4A4D47CFA663A79016100590E05D99B61,
	JniUtility_wrapBuffer_m022A36615A6FA835400628E778B9DA0F07127514,
	JniUtility_getDirectBufferAddress_m23CE64B0247A8FF3586818E3D8E9E52D43D0C780,
	JniUtility__ctor_m8C3B9D38590032F89C79D21AD71ECD268FD544A5,
	Log_setLogFunc_m12F9A4378D2E7CC1248328F2A0C199107E6DE88C,
	Log_setLogFuncWithScheduler_m7C957E3FCF5BC4C19D1E8B6053A3C051F661BD06,
	Log_resetLogFunc_m4B495023D2A347F44CAC40BCC84E00774AF335AA,
	Log__ctor_mA320166FA0DEC6584B471E9CEB28467F0180151C,
	Storage_setAssetDirPath_mC6288D4C79ED1CDDEDA1A1B763127355CE987F07,
	Storage__ctor_mB027569BC3242F71B919CFCF552A432F9760BF12,
	ImageTargetParameters__ctor_mA55EEA0E4883ABD3C76328FD9C15D7B79F145717,
	ImageTargetParameters_CloneObject_m9B894A71ECD626E88FB5DEDAA03D84871B2B32F3,
	ImageTargetParameters_Clone_m41AA5B30437EC0DC6CD1CAB2975A50DD0533BD65,
	ImageTargetParameters__ctor_m9E5DBAB1C3F262FDED5A4E5E819B0470C18F010B,
	ImageTargetParameters_image_mE510BFC336E96EFD7DF7E3268FC843B194C6A377,
	ImageTargetParameters_setImage_m6D15510D01A51B211751FF63DCDE68E44BE3ACC2,
	ImageTargetParameters_name_mA88BFDB6924B7DB8AB6285B603F9BCF895D13C4C,
	ImageTargetParameters_setName_m93F24A91541C60D960AD625294741A238A002B60,
	ImageTargetParameters_uid_m9454559AA9296DF475F1D413A4CA3BA3A051BFC5,
	ImageTargetParameters_setUid_m05BF76E961F1ACD20B2B50589E42AD045745494D,
	ImageTargetParameters_meta_m9AD48070BF6A96D1716BE71DB20D0DE9F086A3BB,
	ImageTargetParameters_setMeta_m3CBD2DBFF7185C42280E23C9952E9E30CFF6671E,
	ImageTargetParameters_scale_m2E2275C606A059CB3ADDAA05E2594C5970583EE7,
	ImageTargetParameters_setScale_mE269C88716E931F1DA7F30798599F8FF2849B316,
	ImageTarget__ctor_m2217685673CA6131F64343C0CEF823B62C3C3F2D,
	ImageTarget_CloneObject_m5ABCB44EBFD7CE028F2196D6753042BB8D445253,
	ImageTarget_Clone_m4305CA89E131DD2BF8482C7078C4B8CA398CF7AB,
	ImageTarget__ctor_m3D3F65999ABB42321D7B50412F45A506609B07DF,
	ImageTarget_createFromParameters_m199C42E7B9137229B7B0B789F50130821780D42A,
	ImageTarget_createFromTargetFile_m8FAE255E8BF61EF3513E3CBB55E607A88801F104,
	ImageTarget_createFromTargetData_m2594953EAB807B9615FA3F05F583CF99933F5853,
	ImageTarget_save_m8EF425D415A5F1FAE50A71FFD6B1F648033E3B8E,
	ImageTarget_createFromImageFile_mE8390E9063BA3DED769B05D2B7B35E0C14F8BD09,
	ImageTarget_scale_m61AE3F3B2D8696CD447B30F85A4110BC7ACC5B33,
	ImageTarget_aspectRatio_mC1217BDCB829DB5D8655DD5F30E4A6C2293D8F2F,
	ImageTarget_setScale_mFB091536555855D4FC028660F151E457F24A6E08,
	ImageTarget_images_m01088CD8E8CABE4DBFCA5CF7E419B6468F664C6B,
	ImageTarget_runtimeID_m3F319AA0E88DA2C393FBEB259E165AD0D9BE8205,
	ImageTarget_uid_m09E8774F140F841AE508A25F3F39B3140DE708D5,
	ImageTarget_name_m36106C5603EE19B19B2BCCD581638F24DAC925C9,
	ImageTarget_setName_mE6A6CA6B01D37416BFF3E55832877857F1E89792,
	ImageTarget_meta_m9523E284F6781FE925DEAD5347CCEAEF7657CD68,
	ImageTarget_setMeta_m0E6B825C5B304E65F198017762CA658339627375,
	U3CU3Ec__cctor_m089E6D6AB5E9D5BBFC4389C9357DFD20971909A5,
	U3CU3Ec__ctor_m9D44AAA1557E053D041E97B325CC2198B171C173,
	U3CU3Ec_U3CcreateFromParametersU3Eb__4_0_m76F4FE5C5665725463645F063997A8EDB46340DF,
	U3CU3Ec_U3CcreateFromTargetFileU3Eb__5_0_mBCE48FF52FF8E57D67FE3CCDCDADDC41111B68D3,
	U3CU3Ec_U3CcreateFromTargetDataU3Eb__6_0_m9BC6A5468AE0C61FC170B4601577E64233B57301,
	U3CU3Ec_U3CcreateFromImageFileU3Eb__8_0_m6479FE02133FF8F3A609D1AFC59AA084CEDE167A,
	ImageTrackerResult__ctor_m7C7D0E1171C73A861A70E7002E7E4CF9FACA7AE7,
	ImageTrackerResult_CloneObject_mF40EF8E645C6430669A08D3FBB8700A97850FAC4,
	ImageTrackerResult_Clone_m83ECB2EB572667CAFA6CFA6B41B7471B0D3760B1,
	ImageTrackerResult_targetInstances_m5444C33C5B030365414C61C74F964FCE98EBF984,
	ImageTrackerResult_setTargetInstances_m1234E539086BA9B7ABFD81B528EE33B203C03233,
	ImageTracker__ctor_mDE19E84B1799570A3A3D3E107996375A4419263D,
	ImageTracker_CloneObject_m8A4E94C899DB11947C094C4DAF1E72299C2A91AC,
	ImageTracker_Clone_mF27D49FDEC92201EFBF99D035DE132174651E129,
	ImageTracker_isAvailable_mBDAE787913931938D137D6C4E92CD7C205692140,
	ImageTracker_feedbackFrameSink_m48496F65CBC19203A6A907592FA29B38FC84CD42,
	ImageTracker_bufferRequirement_mACD43009AD0ABC818D1C7BBE74EC1C45646B34B9,
	ImageTracker_outputFrameSource_m82E7B0A0CC7B58EA011942B8CD55E738C0F5CF5A,
	ImageTracker_create_m72A0371A03F453A7E99EED588E662A3DFEA67AD4,
	ImageTracker_createWithMode_m9F11D5B9160A87D3EEB36D1D0B7EEEB09DED97FB,
	ImageTracker_start_mA72E486A71352B79ED7529FA2A03A4E6CFDBA70E,
	ImageTracker_stop_m2F875E9550F84A07B410FD0AB5AB25D5987E437B,
	ImageTracker_close_mF07E3C927B7BF9F02632B80947FE9284E4699E43,
	ImageTracker_loadTarget_m62F0B49EC011BFF3EE90D858D16454239BEE3543,
	ImageTracker_unloadTarget_m1E900E5F81BCB68D380697EB279043CD96D81A8D,
	ImageTracker_targets_m83C149A25190AF37060E1BBB0305EB56430C1A1B,
	ImageTracker_setSimultaneousNum_mDB3AF18FC362ACDC862527E78137766FDBA01DEB,
	ImageTracker_simultaneousNum_m396E3AA7B974B7238D68C94DAE2FA2A99351B063,
	Recorder__ctor_mB8FA18780FEE4238CE35DCDD55D8ECFD7123D6FC,
	Recorder_CloneObject_m8066D619A8069DE343F1F5D8DC57E2F8E24DBF75,
	Recorder_Clone_mAF604210399647BB12965C656D247A68E82FFAB0,
	Recorder_isAvailable_m8A9F14E795C4909B248599572BFD147F38BA2233,
	Recorder_requestPermissions_m2B952A11E46AA65C4894989F30F8E9ED551FA384,
	Recorder_create_mAA356B8CFC814EE20C41D87863F685E57B74D10E,
	Recorder_start_mAE52606DC9DFCC66C091580E94EB1F7889C22ED9,
	Recorder_updateFrame_mA47F64B5CB29DD7E2E9D5863BA2AFB4CC7D91B90,
	Recorder_stop_mBD49F8860C1637BA0309286455BD6A4F5DC62B96,
	U3CU3Ec__cctor_m3BBFD3CF5307CF88D0EFED1880A703F16FC8D8E1,
	U3CU3Ec__ctor_m7E0CA6A9F9C716DCD2C1D53CE322FBFF7B78CC21,
	U3CU3Ec_U3CrequestPermissionsU3Eb__4_0_m97254790976C119CD7511930575DA958187B07CE,
	U3CU3Ec_U3CcreateU3Eb__5_0_mF263163369F5239315EF562CF5636C14305A7814,
	RecorderConfiguration__ctor_m584B949D8533DC0F343D4182786723B7D6B573BE,
	RecorderConfiguration_CloneObject_mD95D6E22A7E6300A5F105967157D7DF4B29FB186,
	RecorderConfiguration_Clone_m34DDF37F431614FA05C0E63BC5976A179723242E,
	RecorderConfiguration__ctor_mD311C62C0414FB918B805EF37385179F73DBFF53,
	RecorderConfiguration_setOutputFile_m637E20BF8EDCD3C0329D2FCE1836CE34675B9C8C,
	RecorderConfiguration_setProfile_m514341FB34D6914182E2F236C15EEEBE9B0865B2,
	RecorderConfiguration_setVideoSize_mF974BF5626E9304F93E2736A2A8AB451B2816AD5,
	RecorderConfiguration_setVideoBitrate_m0DE1D2EA5B409FC7C37F12271EA1D09D694AE0B9,
	RecorderConfiguration_setChannelCount_m92F3D880808D43E16C2F17A72CB0E851B2CDE440,
	RecorderConfiguration_setAudioSampleRate_mA9DE0FD62D1F1A19883A2F05B196A44FE2ACBB53,
	RecorderConfiguration_setAudioBitrate_m95DCACA8EFBDDD016A8C93730DA59E6719E1188D,
	RecorderConfiguration_setVideoOrientation_m893443D3761C40C1629659F72794BDAC9D37FEB5,
	RecorderConfiguration_setZoomMode_m9B78FF10A30C305C86B4416340860F2EC5A5799B,
	SparseSpatialMapResult__ctor_m991D626D81B33FCC1ABF82D876EE25B9395A95A7,
	SparseSpatialMapResult_CloneObject_m719357CA3B3E4B390407997ECAA0F3350DCB1C05,
	SparseSpatialMapResult_Clone_m74B505104160D3A211E0F0075D05F6A9AFA06047,
	SparseSpatialMapResult_getMotionTrackingStatus_m848EF55FAB9423233DDC160F6045A60D26096618,
	SparseSpatialMapResult_getVioPose_m866FF5FD369AED40194B89953E39BCB9995440FD,
	SparseSpatialMapResult_getMapPose_m68854FFB10A9706B7938A5FF93791A421B2479E1,
	SparseSpatialMapResult_getLocalizationStatus_m44740C95CAFF0A3A16CBBA97F166EC0A27528D99,
	SparseSpatialMapResult_getLocalizationMapID_mACB7191B5FA0F16D96B0F894B45658FADBF9CCCA,
	U3CU3Ec__cctor_m95FECA59D58CBCA0E9D0A6BE691DDBF8C3613F93,
	U3CU3Ec__ctor_m58DA267999A69D1854CB9E7438BA0B10F2EF3D4E,
	U3CU3Ec_U3CgetVioPoseU3Eb__4_0_m9EB790BD3F85F8CD44BFE74435ABB43F7A1378A5,
	U3CU3Ec_U3CgetMapPoseU3Eb__5_0_mA7B0D4F39D30AA1A4793B74F5617020066B75BC9,
	PlaneData__ctor_mBC847736FD9139351525C99BFF9449879CCECCAE,
	PlaneData_CloneObject_mC664AB789B7978EA87CAF563372635D3C86E800F,
	PlaneData_Clone_mA79E49A5E50860E28930F4DE27E15AA8E25B2051,
	PlaneData__ctor_mB20247057DA16BB4EEB8A52F3EAA91D82759733B,
	PlaneData_getType_m1678E832D9515F76F6139663011C50F983C6772E,
	PlaneData_getPose_mDD0ADF26432E586FD3B8816350768587209A5C38,
	PlaneData_getExtentX_m95A206F9E55AF3AC8A049B2D20293C83BA63A5F2,
	PlaneData_getExtentZ_m279DDCBBA38C56697932800C405731C6525A4203,
	SparseSpatialMapConfig__ctor_mDA8250C3AC98319D12B9CB2A81AD2480AD7EF857,
	SparseSpatialMapConfig_CloneObject_mF559EFE22921C6F82685C607A1252ED5A874F79A,
	SparseSpatialMapConfig_Clone_mE2DE77CDF5A5499B0F472D4A7368C0980545AF98,
	SparseSpatialMapConfig__ctor_m9C2C40A6EC187AA460CDF981D98A88DD43A64A43,
	SparseSpatialMapConfig_setLocalizationMode_m8F4D2CF90DC7C008AB8E5814DCFEB33219D55892,
	SparseSpatialMapConfig_getLocalizationMode_m6B0788FD02E6002097F792D32B5C1FB723B2BF17,
	SparseSpatialMap__ctor_mCB2F9FA6F8AA34415BED7426B1BE7A720AB6E46B,
	SparseSpatialMap_CloneObject_m6AEC0CCB8E3C92F6E6933EE1ADBAFF840CB007DA,
	SparseSpatialMap_Clone_m33C79259F025794B8C0FFC14F1198C0F73B9018B,
	SparseSpatialMap_isAvailable_m3A3DA49492871D1F7384155D290248F417A97A6E,
	SparseSpatialMap_inputFrameSink_m7B31A2789844D69C23AA442D79E4A4A960281562,
	SparseSpatialMap_bufferRequirement_mA8E26C7CC3C1E1EB79FAC893458E3EA9CCEEE5FA,
	SparseSpatialMap_outputFrameSource_m722F1B6FF8E2B856304972C1BF44C54E2BFAECD1,
	SparseSpatialMap_create_m2BF7A42F6A5920AA5B33038924252DD0820BFD3C,
	SparseSpatialMap_start_m11265C9AA5777E18B78DF52531E3383A1532FE98,
	SparseSpatialMap_stop_m0E1956586A1A760EEAD216949FC908E5A3BCB9E0,
	SparseSpatialMap_close_m83D072606653EE4D18FE0177F5B5926840DE6F70,
	SparseSpatialMap_getPointCloudBuffer_m0671798CEB40836C93EC9C5252FCB8BD470E408A,
	SparseSpatialMap_getMapPlanes_mBE5B56F55E2A8A6EA5E1F2FA30E7E632FC2FF6BD,
	SparseSpatialMap_hitTestAgainstPointCloud_mC3AF0ABC46ABB5607785F1E0104703498DE0D383,
	SparseSpatialMap_hitTestAgainstPlanes_m175C001A81D1EF29E992D92860AB295209AE8C57,
	SparseSpatialMap_getMapVersion_m72C8D223B5DF0A7C3ED5FFB784DCBCDA98FB8395,
	SparseSpatialMap_unloadMap_mE5A5615C9D082D6C5B930B624670F36595CD3408,
	SparseSpatialMap_setConfig_mFC0F9C6BB93A3131DA522593A9961BE04298556C,
	SparseSpatialMap_getConfig_m11D3B86A7668F20A982063F127BCF3CB6CFA082C,
	SparseSpatialMap_startLocalization_mF1B1840F0372DC12BB54F96BA4492BB035BFDE72,
	SparseSpatialMap_stopLocalization_m99461D40C9BCED8F809D649ED903564A0115914C,
	U3CU3Ec__cctor_m133C60B44966B28286298FCAA8442EAAE4348C81,
	U3CU3Ec__ctor_m6D9831E705A18F012C45E234DA1C8C700D237E73,
	U3CU3Ec_U3CunloadMapU3Eb__16_0_mE449AB43C94206323DD96EBF230E3A3639529207,
	SparseSpatialMapManager__ctor_m01D00DFB39A05436366E33D044EF65FB1BAE63CF,
	SparseSpatialMapManager_CloneObject_mF19C31CC27097D713A9BAC2B20F427DD36AA26BC,
	SparseSpatialMapManager_Clone_m23F9D87E0ED64E8FBF85329C4397321A096FC074,
	SparseSpatialMapManager_isAvailable_mD347B0F546077619E8C4E94DCAE5780D3561D461,
	SparseSpatialMapManager_create_m0C93C3656C659F332E1CE8E577C66C54AC1E58AB,
	SparseSpatialMapManager_host_m8125D9AE8B69601D4AF67A5AD013B810AD1E2624,
	SparseSpatialMapManager_load_m088C435DEA421C9881B0E1DB70F190CCEB705EA5,
	SparseSpatialMapManager_clear_m40E562AABB92A094D5516BF696814FF63545DD8C,
	U3CU3Ec__cctor_m8AF851FE9A4312AE045CA37DE70A50D94BBDE381,
	U3CU3Ec__ctor_mE8DB511B18D8EA1592726383A56EBEE8FFC8BB73,
	U3CU3Ec_U3ChostU3Eb__5_0_m6A7E0D0D9DCE907588DD5C3507F6202B4C6A7D31,
	Engine_schemaHash_mE29C355DA7FE433F156F0AD9F206C01E9D207D13,
	Engine_initialize_m11A436D24ACCBFCEA59056359E8AADCAF93264BC,
	Engine_onPause_mD1CB8E189883AF7D2DB9EB5561395165B029969C,
	Engine_onResume_m92D596C9F77D69647CD00F3B3062FEF2B72F763E,
	Engine_errorMessage_m79BE0BD4ECFD4CF5FB0A86C032FB3F6F7DC482C6,
	Engine_versionString_mBD0B9F6F029448E0A670FF25CCA313CDBAD98649,
	Engine_name_mBB673AB4504EBD510F636DE83285BBA5AD71AD9B,
	Engine__ctor_mA7E53F759C96FC1E785D79ECE02BED3E0291A4AB,
	VideoPlayer__ctor_mE63EC7FBE845F39BE13135497BD05CAF4D12F3AC,
	VideoPlayer_CloneObject_m539E8DE984620130ABAB7A50ED8F14355AEC45A1,
	VideoPlayer_Clone_mCC198F58E166E56504484888FB0F9CB631D72AB3,
	VideoPlayer__ctor_mADE34AF284005C6A1B8AB65A35915DE098F28390,
	VideoPlayer_isAvailable_m1EAD4D4980F19F33CF6515BBB025D689E1F73F67,
	VideoPlayer_setVideoType_m5A8A50779AA7D9CE0DEA7CD06C28BDEE15CD49DE,
	VideoPlayer_setRenderTexture_mEC891F4DFE78F90891E6F339253EF902FE6D9DE0,
	VideoPlayer_open_m620AF087A910AE5A81955FA2A863B006289F2F1E,
	VideoPlayer_close_m972383C93ACF5CF9C27D9670CDC38CE6B1C250F6,
	VideoPlayer_play_m220AF7A3898D95CD1E83F2BC278459D668BBB488,
	VideoPlayer_stop_mB5506354738E36120BFFB53D56D74344A21C5A50,
	VideoPlayer_pause_m0127DCC93E2761A5EED419EE3FD388827DAF711C,
	VideoPlayer_isRenderTextureAvailable_m03A1A75AD2B59DE330F29D19DFECC43117EBF67C,
	VideoPlayer_updateFrame_m2066792A001DD9CDF61ECD060D379BF33FDF6749,
	VideoPlayer_duration_mFDD1771DC1896253EEB75586A2EA86375C1D1D9B,
	VideoPlayer_currentPosition_mCD5EEA47EA030D603766EACAED4AB078A9E64F1D,
	VideoPlayer_seek_m28E4AA77A3E9258BF5F6859FEEFCF1F56EB5B77E,
	VideoPlayer_size_mE5ECC2479A815D956972478A2B9067BB5670C118,
	VideoPlayer_volume_mB0B2EC40637C399BCD97911BD071B24F487B9989,
	VideoPlayer_setVolume_mE22854258C4BA5B22672D286B67EFF1617FA4402,
	U3CU3Ec__cctor_m4816339B3F66B0F26F0B398F0F07DDCFB6BB3BA9,
	U3CU3Ec__ctor_mD4C07DF2E42B8793DBACA1F912D8267110FCF873,
	U3CU3Ec_U3CopenU3Eb__7_0_mA0B920BEBB24C1A44E0262C2A628D46388601A87,
	ImageHelper_decode_mD6E23B7CFB1B3A52E3F847AC5C656AD7D8D8B55A,
	ImageHelper__ctor_m9F3EBC666287C035D2E5919AEBC12FAFD0FF918F,
	U3CU3Ec__cctor_mAFA7C8FEA8A2CFEC22B832EC0FA9371D9E9FEADD,
	U3CU3Ec__ctor_mA62E6106E43A3D2FFC711C2B28C10824BB34EDA3,
	U3CU3Ec_U3CdecodeU3Eb__0_0_mC459140F49E041E56D1D3AEF0839B4044C748B3B,
	SignalSink__ctor_mF6AF7A95F070F4C83FEB971FF89806CD565B0FF8,
	SignalSink_CloneObject_mB31BBDEF20296F1E441B65CCC66E80423084CE2B,
	SignalSink_Clone_m2A70AE4EF7427E48BED0D4A383FE052C60151D3E,
	SignalSink_handle_m0D8833FDC2166DF77FDDBCB26CC224BFABF5C990,
	SignalSource__ctor_m39AFB9C077865268CB6459362326886625EB4B41,
	SignalSource_CloneObject_m100C35A7EA0D30E9429866AE3F98D7E1E75C0A01,
	SignalSource_Clone_m972442D5C977160332E8B12FEB5371A7F0A88A46,
	SignalSource_setHandler_m48FE5636FAB2A1307F5CA316DA41A51A9412865D,
	SignalSource_connect_mF691203C0003586CD8C4082BF7463D9E617371E8,
	SignalSource_disconnect_m6E0F810B8D69256FE3603B91AD1C708646003BE7,
	U3CU3Ec__cctor_mCDA8D804649F3793307B5B558543BF7EB04366B3,
	U3CU3Ec__ctor_m9C50CA93EFE31D8FFDBA0E8E37F18657A1BF816E,
	U3CU3Ec_U3CsetHandlerU3Eb__3_0_m089A90F360FA982EF63FF78484472203A5CE12DF,
	InputFrameSink__ctor_m3287F7C9257FFE56753ECCB5B5D93EFDC77FE0BA,
	InputFrameSink_CloneObject_m07B291D95CAE9B47CE34CD9D6EE42B0B704F4F05,
	InputFrameSink_Clone_mD990F744AE90A9AC6C99A1FFCDDD2CC54827D824,
	InputFrameSink_handle_mE6907122445B68170F796D3AD2DA52D7639B3C30,
	InputFrameSource__ctor_mBFC30B32C94A8C569B400E61DF7F74A63CD6CC8D,
	InputFrameSource_CloneObject_m3A904074EE0539A618596CBC3F02234C2C39A164,
	InputFrameSource_Clone_m60F5F350C4EB248516D8C81B47F4F4743BABEC74,
	InputFrameSource_setHandler_m67D138E16CB0325A82249FAFBBC2D3D67F51CE60,
	InputFrameSource_connect_mA010C74664C629D28A1FE86DFD5FB079DD6949F4,
	InputFrameSource_disconnect_m985D10F6191C8B6F4D7A7BA1136D2819956E28B8,
	U3CU3Ec__cctor_m5D4839887B41ECC90F25F7EC6A8B823D49261CA6,
	U3CU3Ec__ctor_m2D65CE67E8FBDC5A7BF08BD591DAD8C6E4B5845E,
	U3CU3Ec_U3CsetHandlerU3Eb__3_0_m6BB0C47808500F8D68DCC6E9C12137EC24C41AA2,
	OutputFrameSink__ctor_m59152189CA74683CCBB2E3E63A10BE2C202A9B6C,
	OutputFrameSink_CloneObject_mC81058D0E2D56C6FC97BCFA8EBB000A19BF9E6EB,
	OutputFrameSink_Clone_mE1DB31376C8C6E5B587BD31AEA942FA477BE571F,
	OutputFrameSink_handle_m2BC5F361F6450EFE5334185395F1B6F5E058BFED,
	OutputFrameSource__ctor_m7B1B093801B9830C6A10D40997125E261FE05695,
	OutputFrameSource_CloneObject_m7B0B3CE2602DE1381DC675321585B84E2C8E182D,
	OutputFrameSource_Clone_m79F86970969005956C2D90CF5DC799936F31F7F2,
	OutputFrameSource_setHandler_mE249CA5CA073F5A1425B04E45D784E7C9C1FA99B,
	OutputFrameSource_connect_m95CD69C3BDBE03CAC8B8352B4C8CF17A570178B9,
	OutputFrameSource_disconnect_mD824580F9CF482E579E3BBA0EF501EDE1ADBB2BB,
	U3CU3Ec__cctor_m1A728C179F576784D834E83D114F19717DA70CBA,
	U3CU3Ec__ctor_m1069BA0026EB506FFCC8779BAB0C0D018DA0D337,
	U3CU3Ec_U3CsetHandlerU3Eb__3_0_mB1F105F26E48763225CC384FFAD7B986B92D4A88,
	FeedbackFrameSink__ctor_m66EB61B466EE16DAF192525B3526EEFE71E00948,
	FeedbackFrameSink_CloneObject_m60167008FE8EDF83B67AF12279FDA5A25C9272D8,
	FeedbackFrameSink_Clone_mA60B9FE10D30448C55F9BC499862A1FFE59A2D2C,
	FeedbackFrameSink_handle_m87587A957222D425211EFE576BDC42C617A25AFB,
	FeedbackFrameSource__ctor_mC2E77C18906210037B4C1F91B5E692DB8022F966,
	FeedbackFrameSource_CloneObject_m1AC640C09FE6E4C0AE46500F40F751210E44CD08,
	FeedbackFrameSource_Clone_mD93996E1934B36D4B3E1758EFAE49A5ED20998B2,
	FeedbackFrameSource_setHandler_m3CE9B290717D7A8CE0C930CF95D44F85B3507C0D,
	FeedbackFrameSource_connect_mBB4835615A9C5E9B5656685256D1F137DD8996A2,
	FeedbackFrameSource_disconnect_m66BC4E4D809F6FD23F5247F669224955510BE020,
	U3CU3Ec__cctor_m9858EBDD939010A4CE3E3C611022123BE3F02A77,
	U3CU3Ec__ctor_m19A1D3C7D242B1662C7FF580DE996D690C44724C,
	U3CU3Ec_U3CsetHandlerU3Eb__3_0_mD514007AC7F366E44236D233F2FFA14B43B38C3B,
	InputFrameFork__ctor_m17118CE4AB9A5176001E3B8474DA9EAAB9A54839,
	InputFrameFork_CloneObject_m3DB48DED011601A2A1F6CC6E742D52A45E1E1876,
	InputFrameFork_Clone_mC5C061DBF80105A21FE988BF8DBC29CE99362652,
	InputFrameFork_input_mD04733942558F38A26DFE88A68E142F1D48E8A19,
	InputFrameFork_output_mDF50E3856CC5F778A1CC444CEAC523007E8B6A8E,
	InputFrameFork_outputCount_mDBEFE3BBF510D74C6710FE1F207D6A04AFA39C92,
	InputFrameFork_create_m999F1022EA13ABA8FD8925B0F606E3711FBD0232,
	OutputFrameFork__ctor_m6384B8744D108857AABC1ECA11258F3457039C09,
	OutputFrameFork_CloneObject_mB8A53AC9838CFB099B2AAF9152444608E2AA72CF,
	OutputFrameFork_Clone_m08972987705B87A777F5E4266A3E52BDD0FBBCBB,
	OutputFrameFork_input_m0686138B31FC557A39E3AF83BCE00CFCB872F372,
	OutputFrameFork_output_m32E1B3B2AD2E00CA2C9CDB55937C75BAF9A72F33,
	OutputFrameFork_outputCount_mB7C218FEE5ABD955E77D7D58194D1E353B3BDFC6,
	OutputFrameFork_create_m04CAC3B9F4E72C447A163438AB4CFFF2DE0E4032,
	OutputFrameJoin__ctor_m2D30ED77EE63BFF843D93B23B5C95A936F8F8AA5,
	OutputFrameJoin_CloneObject_mE7415677B8811DE3EA1F6C34EA6672F435A0D400,
	OutputFrameJoin_Clone_m32BE2C59D5C6BF7702E4A4552DEE532FB7C54E91,
	OutputFrameJoin_input_mA41E326C752DD455C0E371F0F399D7CAFC7181ED,
	OutputFrameJoin_output_mDBB48BDF3A421A24E702F2DEB97952CB64750610,
	OutputFrameJoin_inputCount_m8B0CFD17DA4D5A2E67D356545CCD3A2D949F58AA,
	OutputFrameJoin_create_m1883720ED8FEF4945E1859A4C0BACC92EDA18B78,
	OutputFrameJoin_createWithJoiner_mF2CC13FD592928B8555C0D05E82ED8350B492122,
	FeedbackFrameFork__ctor_m1DC2FEA03B57CE0FD44145413093C6BC93100DA8,
	FeedbackFrameFork_CloneObject_m10CAE68BCCAD36865C1C2A65914040E4FA8BA6FF,
	FeedbackFrameFork_Clone_m66431A91CE860DC56D1E5851D0DDB57D3301F915,
	FeedbackFrameFork_input_m83A845198F703166D153677154E9E8CBF26CD800,
	FeedbackFrameFork_output_m1FF4AF1F564A6959FB47EFA171BECB6DFA8F7A95,
	FeedbackFrameFork_outputCount_mCDDA337990E6F135792508008BDADB29241D544F,
	FeedbackFrameFork_create_m65592FF80794A1689DA76C173D6A754288983656,
	InputFrameThrottler__ctor_mC22472E10E7E9B877597F47F17077466959F1474,
	InputFrameThrottler_CloneObject_m447C2CDDD0B2EA349C85E1CAC313181D032FE3D1,
	InputFrameThrottler_Clone_m0739F6857D3955D3D669515363B18E89993FCA63,
	InputFrameThrottler_input_mF80B6D53658144F0C31D42B72F65BECCE01ADF2D,
	InputFrameThrottler_bufferRequirement_mC49D379EA541D8E48B41919BFCF20C481C65D9AD,
	InputFrameThrottler_output_m0F63EED956B251537AE2003FCBA17A5FAEDF3888,
	InputFrameThrottler_signalInput_mA9AE8B8F4E66474791454B26D6D57C6135626F98,
	InputFrameThrottler_create_mB75A540454EF3A2D92BCE4FED1B406CF0E755B50,
	OutputFrameBuffer__ctor_mB7491291402EF4BD50E11AB74F11E34F188A86CA,
	OutputFrameBuffer_CloneObject_m939D2A12BF7200FA934E95734014179DF0BBF196,
	OutputFrameBuffer_Clone_m6F7500202390D5EDB2CC22A5FD403A5C7B40389B,
	OutputFrameBuffer_input_mE624A9BBD675A37B31F4E35198BD48765B772A48,
	OutputFrameBuffer_bufferRequirement_mD7AC36795AE73AE223D0866B1B3295DD9F3F7D28,
	OutputFrameBuffer_signalOutput_mC2E7C1A10A70119446AF41BDFFE416A9492AEB41,
	OutputFrameBuffer_peek_m1F1F6547ECAA7AC886932F61E8E74AFDAC055B06,
	OutputFrameBuffer_create_m5BE4D97F5B4C7C5F2C176E60664F79E73056A6F4,
	OutputFrameBuffer_pause_mE8A667EE393FCFA3E4E5C9F0B34CA452F9377583,
	OutputFrameBuffer_resume_m284DB54061DE37B4936DB9427FE2A913608088DA,
	U3CU3Ec__cctor_mC830C71099D43EA6C411964DF571AC00A82DB7B5,
	U3CU3Ec__ctor_mFB9C6C8A3ADBC9F4629CBB7314AC6154F572C64E,
	U3CU3Ec_U3CpeekU3Eb__6_0_m53220CCE8E132C636E720A9E8C3536E34C0C4368,
	InputFrameToOutputFrameAdapter__ctor_mC608FBFDD92AF7B57CD1BEC4E1AF70D5F62CC637,
	InputFrameToOutputFrameAdapter_CloneObject_mC42CF32EC7FB384BD3C7E222CAAFB62E2DD376D0,
	InputFrameToOutputFrameAdapter_Clone_m86B7F0EBAFD742B7F4F90D92BD8451CEF67A65FF,
	InputFrameToOutputFrameAdapter_input_m0ADAA0D1B2017465D5F750C5733D0DE12608DA38,
	InputFrameToOutputFrameAdapter_output_mDC5D7D8D160264836C5D4E266259C30B439912FA,
	InputFrameToOutputFrameAdapter_create_m1BAFC1EE15A2ED9BE7BA524194B04A121555DCAC,
	InputFrameToFeedbackFrameAdapter__ctor_m4390C1F142C9F92EFA9E5084A2547F1321134F69,
	InputFrameToFeedbackFrameAdapter_CloneObject_m2DAE79D9AF7029A13FF5CA6B030633FBD275E4E1,
	InputFrameToFeedbackFrameAdapter_Clone_mB68AB89A79EE4D28007158F5A54A8F08D74EFA28,
	InputFrameToFeedbackFrameAdapter_input_mD15FDE6F26F3EB144D0B1E51935A9028E5B0533F,
	InputFrameToFeedbackFrameAdapter_bufferRequirement_m15FF3CF1B171BEBFD345EB4EFEB8C2B1B7F6FB07,
	InputFrameToFeedbackFrameAdapter_sideInput_mB98F4ED017BF05CF1842D92230C88025502D2363,
	InputFrameToFeedbackFrameAdapter_output_mF4AD1D647991F8B1B40CCABDFC910D5EC243199D,
	InputFrameToFeedbackFrameAdapter_create_m0B6CEC2D513888586480D8FA109D7469FBE853E5,
	InputFrame__ctor_m81DD5033A8A698CDACE58150C6E6BA620EA619CC,
	InputFrame_CloneObject_mBC899D9EC74733B024E36D30A5B2EF4D90A218D0,
	InputFrame_Clone_m8FA11D41A8F2F6055F990F73205187851B796809,
	InputFrame_index_m27F54C00B774FF87966F2457F824C2A7C2811B3E,
	InputFrame_image_m536F0E29192D23AF7E38CC51049C184FDEB967DE,
	InputFrame_hasCameraParameters_m0F90CE7C2ACA9D51A6FF7C555F9EF1FBB1ADB55E,
	InputFrame_cameraParameters_m9FC20524F32BAE442FD99BD4EB95D48EF419B7B7,
	InputFrame_hasTemporalInformation_mD4EB7502D896016121DF12AFC8553D4A52FDDACF,
	InputFrame_timestamp_m36ECBEA832F71C81E73059F623DB588C7F7DF9AC,
	InputFrame_hasSpatialInformation_mE5007CE57DB1751E90F92EA732EC335B9932EFC4,
	InputFrame_cameraTransform_mDA4DB985E2E5BB72AADFA417A9613E9D36D0B37A,
	InputFrame_trackingStatus_m4325CED81A205A8DE188C8CA7BBA761B4E662E6B,
	InputFrame_create_mCE51842BFFCE90F24821A6D89F518A9C8F12411D,
	InputFrame_createWithImageAndCameraParametersAndTemporal_m49D5A2847CB71BE635240ECF1C50112E0A1C5E8A,
	InputFrame_createWithImageAndCameraParameters_mC84C387CE178227D6BCD276081802F9442A9F0C4,
	InputFrame_createWithImage_m15D62333429079C139FC57B665A00EFF307BEB96,
	FrameFilterResult__ctor_m0FC7681961D80786AD40895C713546BCE0218B2C,
	FrameFilterResult_CloneObject_m9D8DF86977E41FF19F1A1733B03CFDF6D23CE2FD,
	FrameFilterResult_Clone_mAFE92B83CF6D168657A2C1F7767DCAC5D27D0713,
	OutputFrame__ctor_m0443F67E82979F438FDEE13055404E09489F57E1,
	OutputFrame_CloneObject_mF2D1C1F6091C6F0DCD3B47898189BB1AE534432C,
	OutputFrame_Clone_mBF41E503B01E569D5D43FDFD2C0AA8CAA90644E7,
	OutputFrame__ctor_m8998855452A69F094037351EC512202637D0BB9F,
	OutputFrame_index_mBAB93E8678567DC8EBB057F0C76C8A06A7C2417F,
	OutputFrame_inputFrame_m293C0B2D0DA5D7FFCD6B81EA941584F74947191A,
	OutputFrame_results_m761ADE9ADBB5AC1FB7F00BDBDC4A16CA039A124E,
	FeedbackFrame__ctor_mC3EC8158AECF3CC976968B6ACACED03C11DB4EC3,
	FeedbackFrame_CloneObject_mAC3D44752A000A8038E27974B8438C0B6BDA817D,
	FeedbackFrame_Clone_m1DE009912835532D9A66907AEDBC8B112EB4F437,
	FeedbackFrame__ctor_m640980DB624B1C99109140C58AE54599FF0048DC,
	FeedbackFrame_inputFrame_mB058D6B78748B6FE3775BAF2CB398CEB3443359D,
	FeedbackFrame_previousOutputFrame_m6914A0A65CCCAB23DBF21E9CBDEA1DCF8D674160,
	U3CU3Ec__cctor_m2506F497A721EC7F7456032A79B4F9E21C7751CC,
	U3CU3Ec__ctor_m822E5BA08E2770EB951977E4A218593C60E2E5E2,
	U3CU3Ec_U3C_ctorU3Eb__3_0_mFD92DDE4DEAA15AF0CC560F99AE3E021BFC345B5,
	U3CU3Ec_U3CpreviousOutputFrameU3Eb__5_0_m06C8AF5E3FD194455043062AD0F9E2F87D11AEAE,
	Target__ctor_m0F8C993DA2C6A0F4A4195AABBFE2CA1F3D36FE8B,
	Target_CloneObject_m5DAA36082516F47CBE4782EF1CFF0725F9A228D7,
	Target_Clone_mFF613F20D81D0FB57E892A0114DB8F2015AF7435,
	Target_runtimeID_m2141C7AD5E6034F57D5B58AA3F50D247F8568233,
	Target_uid_m6B84D7B0B65CB41A017957EF964C8B4C6B759A75,
	Target_name_m0CD7B1AE425F364629BB2F20F7DCA3358E878145,
	Target_setName_mE313CEF02B8D1182EBF3C0FBC0FD3D8542B2C8E8,
	Target_meta_mA66CD47AFC055BE46703360607370BD97C44A31D,
	Target_setMeta_mFB750089A0644D006BEAA60BBD3CBB58D1F53551,
	TargetInstance__ctor_mBAE8860180C7B7F414CBBD19F49ED4A2AC9C88EC,
	TargetInstance_CloneObject_m21C8DAC0824C77414FA815DE1FC1140B8D383FD0,
	TargetInstance_Clone_mD0EA63A05D60BC6268E26722089B45733FA4271E,
	TargetInstance__ctor_mDA3276A19EB368FED4C1E1F413FBD0799F67F23A,
	TargetInstance_status_mF64B473A947B8BFEB340824123163B7137791F77,
	TargetInstance_target_mE658848D1B612A20841A195EBE206DD384C460B6,
	TargetInstance_pose_m827C307640827DF3E224B3A3806CCEF370E9475E,
	U3CU3Ec__cctor_m31F943B8F265DE2DA8C5852063EAF0C48CBBFE2B,
	U3CU3Ec__ctor_mF61004C209C4C13F8C0441A3144F1FA8017148A5,
	U3CU3Ec_U3CtargetU3Eb__5_0_mC0DE2A81D0356AABFE643F12258AFC67A11DEDFF,
	TargetTrackerResult__ctor_m82E004D3CEB2D0AFEF65965F3366EC940D5A45F9,
	TargetTrackerResult_CloneObject_mD61B16D179823FAC6C70EEB1A73EC27CE66CBD11,
	TargetTrackerResult_Clone_mB63D4259A74C5479A6CB4C902C48FD5711B7020F,
	TargetTrackerResult_targetInstances_m32C1C586D9538A8074325E10A785BD8AC03D86A6,
	TargetTrackerResult_setTargetInstances_m45F8BDA021ACBDC84D180B0E4ACEB714ADADCD08,
	TextureId__ctor_m2226206AA94326DE9DD1023DF055FC7DB57DF8CB,
	TextureId_CloneObject_mFC5AB1E7855CF0A036AFB31E847955286AB0C6F9,
	TextureId_Clone_mDC6E21EE1197E2E103AF60F3CFD333C32395177E,
	TextureId_getInt_mB2FC4BA8235670FB1BD37D7C2A5FF7E0410E4027,
	TextureId_getPointer_m79CD499A5A756FD93E6B09DC176F599355B9581E,
	TextureId_fromInt_mA0500F164F6B0F0F529E4D9904CBDB7DB9BB990F,
	TextureId_fromPointer_m0940855F9DCDDBC505BD90E8CA38EA147BC19AF0,
	CameraImageMaterial__ctor_m891D7300151DF69FE6DEBB8906F55A7E4B93F523,
	CameraImageMaterial_Finalize_m05D1DE9C744A36E76A4ACE6CD58A6E4D265DFFB7,
	CameraImageMaterial_Dispose_m5518D807CB2F800B58FD34A39305425AAC66EB8D,
	CameraImageMaterial_UpdateByImage_mD68BDE3FF5CC6A31F32F30680F53EACB0E78F500,
	CameraImageMaterial_DisposeResources_m75B0E76CE09C46DD369BC518C30147AD5CA8C5F8,
	CameraImageRenderer_add_OnFrameRenderUpdate_m51A0C232CFF90B955C67688FDF8BB86EA34997D7,
	CameraImageRenderer_remove_OnFrameRenderUpdate_mC9D3AE9A0D3839A54144AF9F19A5CD119EB3DB34,
	CameraImageRenderer_add_TargetTextureChange_mAF7C7E5E3E3C695B331C152F9B904C8C3B7FD14F,
	CameraImageRenderer_remove_TargetTextureChange_m78E2A24B9CA80FDBE52FA01D1B9A6A551013A8A3,
	CameraImageRenderer_Awake_mC115975632F4D94D27E945871FA58A421776BA8D,
	CameraImageRenderer_OnEnable_mFE28229ED1A0278ECACAC21CFA9F0FFA3E56721C,
	CameraImageRenderer_OnDisable_mCBFA7D127848088EBFCF47D6460E3DDF8B39382C,
	CameraImageRenderer_OnDestroy_mC6CFEB935DB66F8D48FC0A483F22976B173E0CD8,
	CameraImageRenderer_RequestTargetTexture_m01D2F8CDDB119CECB1E4438F3C8A4B3573710F06,
	CameraImageRenderer_DropTargetTexture_m0BD2E03B5D4483F3CE929C9BA30DE6F950412DED,
	CameraImageRenderer_OnAssemble_mAC0ACF6E3253578B90C5F73933622C6B6B56A463,
	CameraImageRenderer_SetHFilp_m988D8ADB2EF66819A92044454D46EFEB6B96D85A,
	CameraImageRenderer_OnFrameChange_mFCCF3E5396F487D6FA26B8B71CB496A675D36114,
	CameraImageRenderer_OnFrameUpdate_mAF1315BB7EDB305911FA219C9CF4303A239A77E7,
	CameraImageRenderer_UpdateCommandBuffer_mF5ACA819D6E6FC5244FBBCA098107F193C5873A9,
	CameraImageRenderer_RemoveCommandBuffer_mA25A74A92D650A93EDE29FE5E58CEEB79DFDEBF2,
	CameraImageRenderer__ctor_mD5780AB6A458E50A4D973F98558926992610A4D1,
	UserRequest_Finalize_mB22508F92B12DBC3B0093B345D973E39F86A73D3,
	UserRequest_Dispose_m7C6AF56AF6AECE1ACB106AB6CA44C1FD31546D48,
	UserRequest_UpdateTexture_m3282FE8EB2D6CE8CD9D38684E1891DF21340156B,
	UserRequest_UpdateCommandBuffer_m1CC087B85E9211B0F93F022FB1BB5F0D35042EDA,
	UserRequest_RemoveCommandBuffer_m9D25DA2C0EDE47C8056DAE1093122E861C20D4B8,
	UserRequest__ctor_mE441F2A8FA332F749C37009ED40D52BB90CB1A4F,
	CameraImageShaders__ctor_m74134E120B7AB7B1DA8A08665FB98AE21D493B46,
	CameraSource_get_BufferCapacity_m479124B776E159091ABF4196E7C0C71E92A365D9,
	CameraSource_set_BufferCapacity_mDBC7729ABCB5C9B32C01A7B5C06967B497D40809,
	CameraSource_Start_m9CE8615E485AB424C7432D08446A13017FAAE8EC,
	CameraSource_OnDestroy_m4F320EE0941FBF33C8E406D61D58F6D269C8F592,
	NULL,
	NULL,
	CameraSource__ctor_m79F649183C3751B904BDDCD3BD349B28C0B33A9D,
	NULL,
	Display__ctor_mCB708C6F79C556534C00BD513B744B3262C7E8DA,
	Display_Finalize_m9A63397FF5AB80EF28262A82F2A1B0190A8F97E0,
	Display_get_Rotation_m184494F525116017620BD3493403CAC5AE66D54E,
	Display_Dispose_m2FA572504702BBDD96F38A17117B6853818888C2,
	Display_InitializeIOS_m7EC74EF42B7B1D941D046F1EFEEA6C22463CE625,
	Display_InitializeAndroid_m3595BB80B4C5BA8099475AEBE7107BAD159432A4,
	Display_DeleteAndroidJavaObjects_m46A6CEBA7D1708D79840F2B5E4D5DB29A4F4E232,
	DisplayEmulator_get_Rotation_mB9E15EBA1BFAE08CB03D0509C103E91E1BED84C0,
	DisplayEmulator_set_Rotation_m5626FF085412A9BF4F86CDC79D842FD29A94F305,
	DisplayEmulator_EmulateRotation_mA5A0F332F57E65F01412C3A1836C11F149609B27,
	DisplayEmulator__ctor_mC13A24C4AEB20EA0E333A31F41E2581374518D81,
	RenderCameraController_OnEnable_m7D4712EE9023B4769508F7FB5EB7CA7D1B67FBBD,
	RenderCameraController_OnDisable_m2EBC51E929E67810E6B5F2019F3B9D34FD6450EE,
	RenderCameraController_OnDestroy_mC6E7478523B725A031825072F335EB0F29414002,
	RenderCameraController_OnAssemble_m28826376DEAAEA8B5A6C9284587A22B4EAF794C5,
	RenderCameraController_SetProjectHFlip_m534466C602B0A3DB8A965D1B2018C05E3A09EF42,
	RenderCameraController_SetRenderImageHFilp_m4CAEA16AC2589DF87F27473DB889F7AD0EE97A57,
	RenderCameraController_OnFrameChange_m50C92530C5DBE72E424BB8853C26EA91AE6C170C,
	RenderCameraController_OnFrameUpdate_m15509507D757103F83B776C45405896EF1F98C71,
	RenderCameraController__ctor_m417EF82791B213B2CB05ED13E8C30B929606B945,
	RenderCameraController_U3COnFrameUpdateU3Eb__15_0_mB648F9A4567EEEE592C4E8366B39BB4C11C24152,
	RenderCameraController_U3COnFrameUpdateU3Eb__15_1_m2E3D602A08DF91717909CC18E505A4AB28907002,
	RenderCameraEventHandler_add_PreRender_m2B5CB28BCD7FD08991DA0C37A3FD8572B5B50FE8,
	RenderCameraEventHandler_remove_PreRender_m170313DE9016B8BE301148361DB4385E5B7102F1,
	RenderCameraEventHandler_add_PostRender_mAE8817443F0267F4799FF8FD04DC718B7799C2EC,
	RenderCameraEventHandler_remove_PostRender_mB83C13F707EB6CBA4786BD8C6AD3D580680004D3,
	RenderCameraEventHandler_OnPreRender_mC832C98837259BE583FC3CF87794818B5C5E6DAD,
	RenderCameraEventHandler_OnPostRender_m4B0563A488FA9B3DA6103CEA95FFD5B0A283AEAD,
	RenderCameraEventHandler__ctor_m6113923386E51F7322883A98BD9B733A0A260D39,
	RenderCameraParameters_Finalize_m26AE8B5D9789DB57830B48FAC8F8EAF331775D6A,
	RenderCameraParameters_get_Transform_m3E7E68A87D3D8F5EFCC7A209DCD42545BF62B0DD,
	RenderCameraParameters_set_Transform_m72CB53FC12350A7F3F15769ACE79F46F9F481708,
	RenderCameraParameters_get_Parameters_m0EDC1415649355013C64BBAABEEE13E84178A618,
	RenderCameraParameters_set_Parameters_m573A60BFD02F1DAF1316611AEAC9520681530AE7,
	RenderCameraParameters_Build_m4E10A9681C735D1FE207AAEE8608CAECFD232743,
	RenderCameraParameters_Dispose_mC430B7A598EE0F8949E065269D2238354D413352,
	RenderCameraParameters__ctor_m5BD82DF7FA740112DFB6154FB49936B4B4C7AE53,
	RenderCameraParameters__cctor_mC2C503329DD10E64C6336E4C78329CC845AB8DF5,
	VideoCameraDevice_get_Device_m926B3C984C4B5D61119460CA890229146FDE6355,
	VideoCameraDevice_set_Device_m79AD8296E6911D67EE9443AF8002911C250CD6AE,
	VideoCameraDevice_add_DeviceCreated_m3E212D2F198B64A65897F769F074B15BEBFFAA36,
	VideoCameraDevice_remove_DeviceCreated_m515E14F76CBAA5895B5E25B38A206DD27F0A5903,
	VideoCameraDevice_add_DeviceOpened_m77F110C7EC10229FA0081A405A144D24F9403DE1,
	VideoCameraDevice_remove_DeviceOpened_mB4CD73BD43608CCAE16B64BCC970E50C3F2C1B50,
	VideoCameraDevice_add_DeviceClosed_m3859CE363FA4A25C778697DF921AF7FEEA05BD33,
	VideoCameraDevice_remove_DeviceClosed_m65127FC471D2D556004BC3FCF95979A663D9FAC6,
	VideoCameraDevice_get_BufferCapacity_m8CAE82C03FAC65C466F4B1672E733A6B2F9B9D74,
	VideoCameraDevice_set_BufferCapacity_mC0ECB8987900A7A99D520FE6494BB1AF9ECF9A1D,
	VideoCameraDevice_get_HasSpatialInformation_m105F2ED4D48A6DA19370DA0A0807399618ED10AB,
	VideoCameraDevice_get_CameraPreference_m9060E303773933E9FC46E1E5F91CBD4CEEA855C3,
	VideoCameraDevice_set_CameraPreference_mFD218A17FEB6DACFD057AC006341FAF222B2B6CA,
	VideoCameraDevice_get_Parameters_mDC0A0FB9F550111DEC85835E8B44CF322371B314,
	VideoCameraDevice_set_Parameters_m85A3BBCBE499206C58889499C8ADA543713DDC64,
	VideoCameraDevice_OnEnable_m25EDBD1AC7230FBE57152BD4BD7514FF4876A93F,
	VideoCameraDevice_Start_mA825191047A6CBEACAF10A30B5F623E5DFE9D2FB,
	VideoCameraDevice_OnDisable_m8449FEE1BC9875B52537288897521DF432FA18E2,
	VideoCameraDevice_Open_mBA044207C8C73CE4C8B293FFF9056577BFEE6335,
	VideoCameraDevice_Close_m654B136E1E79F2EAB85390859CD06E09A9F03A15,
	VideoCameraDevice_Connect_mB1077D20F8B9B510A76BED74443F002092B4C672,
	VideoCameraDevice__ctor_m234E16844A3093CD04BE199560C41368186DE6BE,
	VideoCameraDevice_U3COpenU3Eb__36_0_m190320BF0CC8352DE99FFCD853BC7D15C35AAC9D,
	VideoRecorder_add_StatusUpdate_m063C7113C310771CDE2D387DC7F8BB834660BB86,
	VideoRecorder_remove_StatusUpdate_m2A7DD8BCF5AEAC7DD6065798C822E87B2FF420B1,
	VideoRecorder_get_IsReady_mEC576F4B82E4DDA3400B8EF8593EAC7429F2639C,
	VideoRecorder_set_IsReady_m45B7BD7D9955CF1B069513E7E670DED48E63FE5E,
	VideoRecorder_Start_m73AA2322830A08014A7C34950909F106052AE67C,
	VideoRecorder_OnDestroy_m0911AA05822C1F12C4A97AC5B63A44F433410914,
	VideoRecorder_StartRecording_m0E5464CACB87C8F71182071AD7CE082BA58FDEF8,
	VideoRecorder_StartRecording_mA47FFA0671EA3ACD7DBD0BBC38113AC162AF54C3,
	VideoRecorder_StopRecording_m5CB89ECDC7A32563CB35A6D0E1D2D18AA522C69E,
	VideoRecorder_RecordFrame_mEB4FA7E5EA4E4C018448BD346F3CC71FD32791D8,
	VideoRecorder__ctor_mCBD3F8E27220F67A10F5407D82EFC6AA980559E8,
	VideoRecorder_U3CStartU3Eb__14_0_m19D1B3C3E07883B5B2F14E30A2B681AAB83DBAB6,
	VideoRecorder_U3CStartRecordingU3Eb__17_0_m2BA06D3E64FB113F664EBC97423BCCA9FA5BDE1A,
	EasyARController_get_Instance_m4D9BEEAA229476C1639FC24DA46A94A5620A0D30,
	EasyARController_set_Instance_m0644DDA7B23B4DF33F6CA10E1EBBAF23EC11AAC5,
	EasyARController_get_Initialized_m0FCA276095BC052B4B970D3D1354FB1E6AA98C41,
	EasyARController_set_Initialized_mFD745ACB6C6E691DABA81F36CBC5525734185717,
	EasyARController_get_Scheduler_mC7D3A3E19D863258A8AEF0905F37C16AE54760E6,
	EasyARController_set_Scheduler_m5DB7C2DB70602A75751F494AB8DB5B86ABBFB15F,
	EasyARController_get_Settings_mBDF979F9B6F8A3D7585EE709A953D2610FB717BD,
	EasyARController_get_settingsPath_m879FAD5ADDEEE11E82DED4D955946D09980B7B41,
	EasyARController_get_Worker_m7EC07F0D41BF1A762F1B21D68779BA48B00B3E9E,
	EasyARController_set_Worker_mFB4F26C6EA7D7CA0AA5D741339D8E34538D1EC91,
	EasyARController_get_Display_mD3A3DC439FCED5221F7A096AF46AB2EB43D4A32E,
	EasyARController_set_Display_m8172B0A0B2853E66E36B9EEA28F929E6D08EC73D,
	EasyARController_GlobalInitialization_mB63A67532D901C55F5B3A086D12E443A395FB87F,
	EasyARController_Awake_m20721285FE4E5BE95DE758D9EAFA191F86EDE51C,
	EasyARController_Update_m4EB3B1167A1587D0571D342AB08CACD19AE987D6,
	EasyARController_OnApplicationPause_m155C86A2532D2A279161052A46B0024DA58AD38C,
	EasyARController_OnDestroy_m0C2A8C69599AAC81F21482374CE00479D307B33E,
	EasyARController_ShowErrorMessage_m1D87BEB50E1689DC937EDA8A0495A980DDBC072B,
	EasyARController__ctor_m52AD84F1B8E607D4D3932A146BFF5EEB9D080AF1,
	U3CU3Ec__cctor_m6EBCCA933137A54884E44EDCAB072CBD529A7D32,
	U3CU3Ec__ctor_mB9C9D7A9CCB189D9FAA2ACFDF4F9B3566C13D872,
	U3CU3Ec_U3CGlobalInitializationU3Eb__26_0_m9CDF7A53069A9F7678CF50899CABD21FBE87FDC7,
	U3CU3Ec_U3CGlobalInitializationU3Eb__26_1_m0BCD9FB4FF3FC073C8AAF36504B5B64399544FF6,
	EasyARSettings__ctor_m5374E04073BEBE36CF622A552961728E3261B496,
	TargetGizmoConfig__ctor_mBAB4814FAC6CA9C9153268D9A2377AA7F166E6E8,
	ImageTargetConfig__ctor_mE1D2417A07CBEFDB3E8D087170D349BD95F3A50C,
	ObjectTargetConfig__ctor_m840408CBA0C203A1982B4020B21FF3342CAA32F5,
	AndroidManifestPermission__ctor_mD2CDFFDB852784CBF5F0839B422455B2E14BF30D,
	CloudRecognizerFrameFilter_get_CloudRecognizer_m354F93CE6C51A14562194DE2BCEA51F27EE49D5F,
	CloudRecognizerFrameFilter_set_CloudRecognizer_m63F102BA1221D4CB3E75A63FB2E693A3B4B054F0,
	CloudRecognizerFrameFilter_get_BufferRequirement_m18153E863488C9038CAE9F67A6FE76FA9F96B6A3,
	CloudRecognizerFrameFilter_Start_m014FF15AC1CE4DF9F69449B59B64C0A63C2BE0D9,
	CloudRecognizerFrameFilter_OnDestroy_m1E778CF275B87416A85F20415CDAA433E017C807,
	CloudRecognizerFrameFilter_Resolve_mA0BA7D6AC33924752B0AF2A52500DC879CAFDE77,
	CloudRecognizerFrameFilter_OnAssemble_mC446E6A7D90669466A6042D439E408F395C41B97,
	CloudRecognizerFrameFilter_OnFrameUpdate_m60D7EAC06807B5D1851940D3BD366D4EB09812C6,
	CloudRecognizerFrameFilter_NotifyEmptyConfig_m4D40B0961815B84706B0AFC19BB61F6BA68A34F5,
	CloudRecognizerFrameFilter_NotifyEmptyPrivateConfig_m1E469C9CF3B114D611CA61A21EFE350C8F841946,
	CloudRecognizerFrameFilter__ctor_m973DD3E7D4A44A8C175949A8168C93BFEBAF2A7C,
	CloudRecognizerServiceConfig__ctor_mFBFFA6D0CC5FBDC497A8078E229F903918952AA5,
	PrivateCloudRecognizerServiceConfig__ctor_m88055E744DF478668D83D729C446444DC4365D38,
	Request__ctor_m518394CDF817588BDD9A9F9FBFA8CA766EE0BCA5,
	ImageTargetController_get_Target_m0F1C3C12D9FBF9ADC0AEB5320D86F30D19C0D811,
	ImageTargetController_set_Target_m691E8B52130CF50D8A749C600CF8E1A9DFC5442B,
	ImageTargetController_add_TargetAvailable_m9467BEDC472B4D21DEEC28741A0CEAFB29DFA820,
	ImageTargetController_remove_TargetAvailable_m8329089F509E9519EFF04A625CC1666E5A635B50,
	ImageTargetController_add_TargetLoad_m0D7180BCFF4AEAEB052DD4EA929097BE08EF188C,
	ImageTargetController_remove_TargetLoad_m02E72F6F67A3C78E8AFB012F713611088C6AF932,
	ImageTargetController_add_TargetUnload_mF3BD3D47A1BA72C2A2A9A5FBAEDD875AA78AC24B,
	ImageTargetController_remove_TargetUnload_mFC4F4CC13F66062F92A926CB7B71B135F11D9B4D,
	ImageTargetController_get_Tracker_mE03FC1F82F8E0C6FD3E3B92AB7CACA07AF49E19A,
	ImageTargetController_set_Tracker_mC89AB8C2EBA0BD373F609D9B9D62BCBF9926BBB2,
	ImageTargetController_get_Size_mFB5B22CC61808686226D884EACC483548F4C681A,
	ImageTargetController_set_Size_mA8B2E028DB76456BA724C9F7AB03A55F7284C7FC,
	ImageTargetController_Start_mB95572C8073F005F283E54B98C688316DD551BA0,
	ImageTargetController_Update_m2F7449CEA3063F93B13BC0E681C42C0D6F932739,
	ImageTargetController_OnDestroy_mC7D2FC8958B5982D68ACD59E88F97A07C7474C04,
	ImageTargetController_OnTracking_m1770BE57B5C0A7A79634F3227EB7DBF7ACF2992D,
	ImageTargetController_LoadImageFile_m54813DDF2569E837F90E668F2322117EB09ABE94,
	ImageTargetController_LoadTargetDataFile_m5FF7AC85BB633051C3374596A18744A9E4CB1FD9,
	ImageTargetController_LoadTarget_m753797B436A84D91896419D3D347C27249E1DD6C,
	ImageTargetController_LoadImageBuffer_mB52CC13907B585A80CD0E74F48F4AC4D04F9CF86,
	ImageTargetController_LoadTargetDataBuffer_mDCC3A373F4D20BBB75237CAA55538F03F15E976A,
	ImageTargetController_UpdateTargetInTracker_m1B4B7735ACB8DEED5757D4B829AE9DADABDEA775,
	ImageTargetController_UpdateScale_mD04D7FA8EC2FDBB1A2C26D689DC202C34C2C2883,
	ImageTargetController_CheckScale_m0F4D1461DB0C79333C54E75476BC76864548B700,
	ImageTargetController__ctor_mDD385F7B1202536BE4B6F67CCCF19A95C8BE8773,
	ImageTargetController_U3CLoadTargetDataFileU3Eb__35_0_m376DC236B4B665D74CB1642A180D1DF362DB8FA5,
	ImageTargetController_U3CUpdateTargetInTrackerU3Eb__39_0_mC5711419892B651ACFD0BF21371CB426D3C5C10A,
	ImageFileSourceData__ctor_mDEA8F58FDC88D9E5CAA030AC6A05B6AE1BBD01E2,
	TargetDataFileSourceData__ctor_m7F8546325EC90508CF9A14D3499EF6F48A72869B,
	U3CU3Ec__DisplayClass34_0__ctor_m03F77038B0975BF8B645BF622B5BB531D5DC9D77,
	U3CU3Ec__DisplayClass34_0_U3CLoadImageFileU3Eb__0_m9957693513E83EE4788DB8947D031C7F38B30319,
	U3CU3Ec__DisplayClass37_0__ctor_mBAC544DA80A8F17F86D93C88452F013642FDEEB3,
	U3CU3Ec__DisplayClass37_0_U3CLoadImageBufferU3Eb__0_mDDB31AC06D55CA990525A40B5E30EE838E189848,
	U3CLoadImageBufferU3Ed__37__ctor_m316EB6054CB5AECB04721EA523876CB3B8F7CFF4,
	U3CLoadImageBufferU3Ed__37_System_IDisposable_Dispose_m9549C468CA4876B3B335D784700FB6C36890511D,
	U3CLoadImageBufferU3Ed__37_MoveNext_m79CDDD968D06BF040B34605939751B570404232E,
	U3CLoadImageBufferU3Ed__37_U3CU3Em__Finally1_m1FA515671DFAE081EEC0F649834566FF740FD131,
	U3CLoadImageBufferU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA37C5AAEACB0AA727307F7BE6217A3E2B3EDD78B,
	U3CLoadImageBufferU3Ed__37_System_Collections_IEnumerator_Reset_mE9E76167788A27BAF0B470EBF78DE2E3BD92946F,
	U3CLoadImageBufferU3Ed__37_System_Collections_IEnumerator_get_Current_m0A268429535E1FFCB1091F7B5CE73BBB1AC6FD49,
	U3CU3Ec__DisplayClass38_0__ctor_m07EEDC11957897E2C5A11D6B644D24A7F9849A84,
	U3CU3Ec__DisplayClass38_0_U3CLoadTargetDataBufferU3Eb__0_m89E626FEEAD3E79B22ED80836208CB2751781783,
	U3CLoadTargetDataBufferU3Ed__38__ctor_m77E8BEB2D46B0FC1D095FFEB0CA65C72FC0C3C3C,
	U3CLoadTargetDataBufferU3Ed__38_System_IDisposable_Dispose_mED0D6ADF10935E5905B204001A364D6B3267C858,
	U3CLoadTargetDataBufferU3Ed__38_MoveNext_m068E451D3DD00141574A30F0D2903EC196B2146F,
	U3CLoadTargetDataBufferU3Ed__38_U3CU3Em__Finally1_mE8E170661AF53D113C6C42049C50779A7119F369,
	U3CLoadTargetDataBufferU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m965E6D287A33DAA0DA332F7023DC1E356F17F0F0,
	U3CLoadTargetDataBufferU3Ed__38_System_Collections_IEnumerator_Reset_m191C863FD554A6ABFCCBDDA70C1209F28B0B32D8,
	U3CLoadTargetDataBufferU3Ed__38_System_Collections_IEnumerator_get_Current_mC4E576DFA43B13AFBD8812DF07ECD41A9EC5B13A,
	U3CU3Ec__DisplayClass39_0__ctor_m13BC7F7ED4D79AFF70FE74CBE4327DE61090516E,
	U3CU3Ec__DisplayClass39_0_U3CUpdateTargetInTrackerU3Eb__1_m7448F405158C593AE316E4ED9CE17E6B25DCA78C,
	ImageTrackerFrameFilter_get_Tracker_m921C5DE4FA7D22775D7A0DBF9E39BB6D283E765F,
	ImageTrackerFrameFilter_set_Tracker_m0C3EA99FAF9A0B03278CE3C9127E3FEFC7CDBDB5,
	ImageTrackerFrameFilter_add_TargetLoad_mAC6919010D498254A50B080CD9E7D06BA4263FBD,
	ImageTrackerFrameFilter_remove_TargetLoad_m3578D6764A9429D55F53A2702D352A6157507745,
	ImageTrackerFrameFilter_add_TargetUnload_m9784EAE4580C2EA58AF655BBC2CDF523A78EA59F,
	ImageTrackerFrameFilter_remove_TargetUnload_m55826DC7A7DD08B92535FE77F01F5A9D03343DC2,
	ImageTrackerFrameFilter_add_SimultaneousNumChanged_mE16608F77817AA1C35B60C4C55F8CB86B88F1480,
	ImageTrackerFrameFilter_remove_SimultaneousNumChanged_m96B16C9C0818E2E2EB5916212E096CAE78BF02CA,
	ImageTrackerFrameFilter_get_BufferRequirement_m43C802AFEEB0B85ACC64A6213CDE516B870521E7,
	ImageTrackerFrameFilter_get_SimultaneousNum_m104FBA7D54E4F2908816EB921D74AE23A96D4BE4,
	ImageTrackerFrameFilter_set_SimultaneousNum_m340F710A6FEF0CDDDF99650BD232E593A4C44085,
	ImageTrackerFrameFilter_get_TargetControllers_mC0ADCA187166B49DE745682579B96ECE6BA77BEC,
	ImageTrackerFrameFilter_set_TargetControllers_mF46C7BB3F4488795C357A23535F4DBB1AA1B49AC,
	ImageTrackerFrameFilter_Awake_mBA9A9FACCE5A913D69691D0D1888FAA60666DABC,
	ImageTrackerFrameFilter_OnEnable_m1B76FC2657BB27222BDF7AC3300636037CAF4853,
	ImageTrackerFrameFilter_Start_m0BCEA9869AB4740C6245F69756D3CA6782C6D608,
	ImageTrackerFrameFilter_OnDisable_m27AF0890FFD39D1376CF4E4A06C3B8C2681888E8,
	ImageTrackerFrameFilter_OnDestroy_m3FAC3A81B751ED8425AFF30089A41A112CAAB9EF,
	ImageTrackerFrameFilter_LoadTarget_mAD2712D4BDBCAF841DD58D928E48397F9747F117,
	ImageTrackerFrameFilter_UnloadTarget_m9BDD5C1F5CC2608F175B4EEA74888DEA4D679ED1,
	ImageTrackerFrameFilter_FeedbackFrameSink_m093A1E7A28078D13C5DFDADAFB4A4A991A4F7DC9,
	ImageTrackerFrameFilter_OutputFrameSource_m947CC6F1A92D488C2F8158941F71C1E7825E075B,
	ImageTrackerFrameFilter_OnAssemble_m5A07A10C650CE77D1BFD0F0B1FA4E79165D9A3E9,
	ImageTrackerFrameFilter_OnResult_m0D22C00DC6B75DEB1860E38A433809F6193B8E53,
	ImageTrackerFrameFilter_LoadImageTarget_mB89085E278CF6C843A9880AE65A46F3FE06E6836,
	ImageTrackerFrameFilter_UnloadImageTarget_mA61853F37CD866227390890068ABA8B9EB6749BF,
	ImageTrackerFrameFilter_OnHFlipChange_m516A6D3B38FA0FCB17FAC1F235EFFFDA8490B84C,
	ImageTrackerFrameFilter_TryGetTargetController_m18DDCACF469F90C32765E53749A7A505827FE13C,
	ImageTrackerFrameFilter__ctor_m925ACEB7857F04DEC5F3D656408F95DEEE3F35D1,
	U3CU3Ec__DisplayClass37_0__ctor_m82DF8184A0F6CDDAD1E2BB5B50AD7E3D2B3BC034,
	U3CU3Ec__DisplayClass37_0_U3CLoadImageTargetU3Eb__0_mC76E6D0697AD4535DE2B039B61ACDEB6EBFF19B1,
	U3CU3Ec__DisplayClass38_0__ctor_m814F171A1C5EDEFB38153E3F33A7573BC4DC5743,
	U3CU3Ec__DisplayClass38_0_U3CUnloadImageTargetU3Eb__0_m12A42B10C360CD78CADE838AD9C35053944D18C4,
	ObjectTargetController_get_Target_m6A2FD3B44FC41DA411C2E3944988452E2733321B,
	ObjectTargetController_set_Target_mCE5C95E720E26090653430B9C1FBF667388A4BDC,
	ObjectTargetController_add_TargetAvailable_mCAC4FBCFA1A94B4CF3C6B5733E78246AE971B0AD,
	ObjectTargetController_remove_TargetAvailable_mB4F3B4DA1CE06FC264792F198ACDF17893EDB3F7,
	ObjectTargetController_add_TargetLoad_m8BC5700373C419E7FD833DFE691298443DE9BBE6,
	ObjectTargetController_remove_TargetLoad_mED573C2E2B49FBAE05701F23B2DFE9D8D884C54F,
	ObjectTargetController_add_TargetUnload_m754EF6ECA76B0D74D7FFABAE46697A4F4C9BA7B7,
	ObjectTargetController_remove_TargetUnload_m636650C9CA43F4DBA220C8DFE0134C1229C16907,
	ObjectTargetController_get_Tracker_mCC20560339BE0353742B6F44B5254226AE56B4A6,
	ObjectTargetController_set_Tracker_m6EB7D38EBFBF87A55953A4545D6C18A0D6F75EE7,
	ObjectTargetController_get_BoundingBox_m55C9156ECBE728F0C28820D32A32000614D8ABF6,
	ObjectTargetController_set_BoundingBox_mA3716CAAE726F3E1A264BB99573631049A1C6084,
	ObjectTargetController_Start_mF7469B78923DA4EDAD6D1A6AE9E2BA3A78A665B9,
	ObjectTargetController_Update_mC7A8C5FA613F7F49DBEEE1D7E70CA103E5948D94,
	ObjectTargetController_OnDestroy_m3D387D05568CD4D1F6AF87525986E0B5C0B2BC5D,
	ObjectTargetController_OnTracking_mE8D26AE98B0A8F01BCC267E20AEC2BD48A718DE6,
	ObjectTargetController_LoadObjFile_mAA8AD8DA11BFCD084B0BFFD8271B931C5F59CFA4,
	ObjectTargetController_LoadTarget_m6E48F3AAED98969C3BA058CBAD6E5442E3EE6028,
	ObjectTargetController_LoadObjFileFromSource_mD45D611FC2B2F282424481E62F8CD8A9852F9DCB,
	ObjectTargetController_UpdateTargetInTracker_m3F506478FBAB480BC40B0ADBC386E4BFC651E4F0,
	ObjectTargetController_UpdateScale_m03D18290AEC681A567B28FA5116379A33BA96BCB,
	ObjectTargetController_CheckScale_m09CCDC5F37B4FC47E92943788F7C3CB2E1A9E5D4,
	ObjectTargetController__ctor_m6E766FFAF06FFBA9B70B1872FB15D120CAEB0E45,
	ObjectTargetController_U3CUpdateTargetInTrackerU3Eb__36_0_m908DC12A830F0908B6ADFC0F462C6661446B896D,
	ObjFileSourceData__ctor_mB93AAF27E0FDC3474989B2219A4D9B453C7CFF4D,
	U3CU3Ec__DisplayClass35_0__ctor_m935ECE2E28A101A24DC5F7BFD1D2DF7729DF7685,
	U3CU3Ec__DisplayClass35_0_U3CLoadObjFileFromSourceU3Eb__0_m7CD6FCB9448321EF83D05676032BAD03FBBE007E,
	U3CU3Ec__DisplayClass35_1__ctor_m84F80C9CB69DF6B0B9ACA5C7963BA187D7557B18,
	U3CU3Ec__DisplayClass35_1_U3CLoadObjFileFromSourceU3Eb__1_mD0E5E14CF561D3CCEB463DAA1CBC94196102859A,
	U3CLoadObjFileFromSourceU3Ed__35__ctor_m7A6A05D9063B632855EC5CA6BA48A96F054B8BFE,
	U3CLoadObjFileFromSourceU3Ed__35_System_IDisposable_Dispose_m5A04B92ED61DC373ACAB6C6D5078C3FCFB50E888,
	U3CLoadObjFileFromSourceU3Ed__35_MoveNext_m7B46AE24F55220A6867AA0C52912BE79144D4BAD,
	U3CLoadObjFileFromSourceU3Ed__35_U3CU3Em__Finally1_m9711BC881ABB639C2ADCF04A52E9CE1553ED7AC1,
	U3CLoadObjFileFromSourceU3Ed__35_U3CU3Em__Finally2_mCC70FEDDF8F239EC933CC54C5ABB8520F1D15FD6,
	U3CLoadObjFileFromSourceU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m88255FE97B48643ACC9CE2B55EDD4FBCC117AABC,
	U3CLoadObjFileFromSourceU3Ed__35_System_Collections_IEnumerator_Reset_mC27E618A38368C2677E9B791ADDF7563C7000A66,
	U3CLoadObjFileFromSourceU3Ed__35_System_Collections_IEnumerator_get_Current_mA8A08EB1A756E18E934D954AA573F97B8EE915C3,
	U3CU3Ec__DisplayClass36_0__ctor_m913CE0503AE024BBD7879A925E77718F9C1978C0,
	U3CU3Ec__DisplayClass36_0_U3CUpdateTargetInTrackerU3Eb__1_m15EF9A811AF74EB7F8B5938CE8B3130201AFC3E7,
	ObjectTrackerFrameFilter_get_Tracker_mA297A04811AA9A4D809E939C83CFB45579E0B960,
	ObjectTrackerFrameFilter_set_Tracker_m3B1E1011F4FCBE741BA781359354DB7308564A2A,
	ObjectTrackerFrameFilter_add_TargetLoad_m873A78DB962501BC4DD7523EA6949F455BE30A24,
	ObjectTrackerFrameFilter_remove_TargetLoad_m82D9803297B69CF5AFEAB7AA0F12CF44AE7F52C4,
	ObjectTrackerFrameFilter_add_TargetUnload_mA1C4BB8E00F52698BE5A5A21B5103BC342D4EE17,
	ObjectTrackerFrameFilter_remove_TargetUnload_m091E7E675C61AC4B2D7E4785726A5F25BA57E33D,
	ObjectTrackerFrameFilter_add_SimultaneousNumChanged_m365A36A03D58A673DF10564813B2DB35C9306846,
	ObjectTrackerFrameFilter_remove_SimultaneousNumChanged_m4571D7E0F96DCD389C730718FDDEA39C507F7971,
	ObjectTrackerFrameFilter_get_BufferRequirement_m4498B9C4D0356B519BDC7C507E452E30FA8FE846,
	ObjectTrackerFrameFilter_get_SimultaneousNum_mA8828788A98B0E51E8FE9B3B9FAFE50B7A3F0830,
	ObjectTrackerFrameFilter_set_SimultaneousNum_mAC8B3B18397A774CA0956CA5BA87AC93CD21EE1F,
	ObjectTrackerFrameFilter_get_TargetControllers_m98076F1CABEC0673C73D9DF2C46D16C413C781E6,
	ObjectTrackerFrameFilter_set_TargetControllers_mD20434FDD01492103078E5A16E78EAF257CB8535,
	ObjectTrackerFrameFilter_Awake_m188B4C3E822EDE35D6101A5755D4E4205CCD3C23,
	ObjectTrackerFrameFilter_OnEnable_m5D9853B6FF844AA3C9F942DABD17FB5EECDBB527,
	ObjectTrackerFrameFilter_Start_m8B49466E2555E9625392932F2E4CD0322793A36E,
	ObjectTrackerFrameFilter_OnDisable_m784F3C885D7381DA26B89CD047412423D7D34015,
	ObjectTrackerFrameFilter_OnDestroy_mF1E6ED7C0E7A5AF822FE4C7D2BBBF222A2158364,
	ObjectTrackerFrameFilter_LoadTarget_m5FF3F323C026DF36C23B90D4FF79F674FF94C096,
	ObjectTrackerFrameFilter_UnloadTarget_m68EE25FF25C7C3584859088C0E5E9A277D28935B,
	ObjectTrackerFrameFilter_FeedbackFrameSink_m9B6A66C8F4D1FB0B4022E2410E9D33A3B5540733,
	ObjectTrackerFrameFilter_OutputFrameSource_m3E11417146D1480F6B43690E51D904D14AFA4FBD,
	ObjectTrackerFrameFilter_OnAssemble_mB8E0C73050D856ECF0FD265A57BD0B3C5A0879C5,
	ObjectTrackerFrameFilter_OnResult_mFDD30FED72D2598025C8B3F00DA36EFC8E3E5B0B,
	ObjectTrackerFrameFilter_LoadObjectTarget_m68788903472A4719107DBE18E06AD41F3B0C1A6D,
	ObjectTrackerFrameFilter_UnloadObjectTarget_mF0E970C536D4B9698A51D574D1F23374453EA0B1,
	ObjectTrackerFrameFilter_OnHFlipChange_m0536FE26C38BBB22C14486145F7E435264FC94C2,
	ObjectTrackerFrameFilter_TryGetTargetController_m1B4ADBC152722C759F690F539D7EF447BAEC0851,
	ObjectTrackerFrameFilter__ctor_m37771611EB65154C59C574FCA21E8ABE67492C9A,
	U3CU3Ec__DisplayClass36_0__ctor_mBEA30685FA373FDF4D82E18595112CD1E6CFF166,
	U3CU3Ec__DisplayClass36_0_U3CLoadObjectTargetU3Eb__0_m05BA5AA826D3B7452DBAC1D3229DF49A26A4EA9E,
	U3CU3Ec__DisplayClass37_0__ctor_mBC1C7AE887490DE87BBFE22974AB3CC25558377A,
	U3CU3Ec__DisplayClass37_0_U3CUnloadObjectTargetU3Eb__0_m514F3237CA127E51C944ACBE64ED37552F0DE50A,
	AliasAttribute__ctor_m4AECECEC85441A02B4FAB76842154B86687FB368,
	RecordAttribute__ctor_mB1E4F65426EE78E761977E7DF8232A49CC6AAC55,
	TaggedUnionAttribute__ctor_m50D336CC0A29803F16677978D8227E974BBF480E,
	TagAttribute__ctor_m44A1127B7D01D36A479963B3C559993271CDF1A4,
	TupleAttribute__ctor_m810F11BFA55CAA47E845A4FA7DF9E5B23B5535B4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	APIExtend_ToUnityMatrix_m206A3ACA63D8BB98326D5DFB039F3FF9DED64550,
	APIExtend_ToEasyARVector_mE2724A0CB62CD9CDEAE1046A8B79894F648985F1,
	APIExtend_ToEasyARVector_m9600E8DE018615DF428475EA76401EFC81B2A242,
	APIExtend_ToUnityVector_mF8D534B98643DC1F8A61AD769F2246627141661F,
	APIExtend_ToUnityVector_m091751C847170124879708574534A1AF98CE92CD,
	FileUtil_LoadFile_m88EBB2907ACDE55485823A4C26A2B9D7BFD403AD,
	FileUtil_LoadFile_m4AC9BF927CBA7B25789521DD69A1D0BC5CFC5919,
	FileUtil_PathToUrl_mC2490350AFE724942E8681D0F92A8D10C3B638D4,
	FileUtil_ImportSampleStreamingAssets_mA3FAD1235B9E311CEECCF77902310CDDFA9025C8,
	U3CU3Ec__DisplayClass1_0__ctor_m849D71F8C1DE1CDD64B657B89278D01AA11057BE,
	U3CU3Ec__DisplayClass1_0_U3CLoadFileU3Eb__0_m0B88454562B365A3172F4FABD19C6F7F24CD87F2,
	U3CLoadFileU3Ed__2__ctor_m7DAF075882D1195526359B33F1A62ED93E14812D,
	U3CLoadFileU3Ed__2_System_IDisposable_Dispose_mCC55578A4367C5E8FD2DB846C963A4C38CABBD79,
	U3CLoadFileU3Ed__2_MoveNext_m18AD8EDEBA7E999263C81C588A70FC92461D654C,
	U3CLoadFileU3Ed__2_U3CU3Em__Finally1_m7902C9828A9B4EDD334836FDF9927B36B10E1D8E,
	U3CLoadFileU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4177F95AB6FB19CBE473039096963BB7E9635A57,
	U3CLoadFileU3Ed__2_System_Collections_IEnumerator_Reset_mA8A4FE670BD8AFE9610995CDDB1CA59D0A3C447A,
	U3CLoadFileU3Ed__2_System_Collections_IEnumerator_get_Current_mCBCAEC97C03DA3D9F1F58642A8A252E93C8FE366,
	GUIPopup_Start_m45B63FE0D40EA02CB9FD1FEBA8AEFA3F6B4D78CB,
	GUIPopup_OnDestroy_mF5EB996234CB30ECBF0A4EC74CCEAC96D0BEE7C9,
	GUIPopup_EnqueueMessage_m1FF5C5310444511BAC722C9EC22C68FA62716CDE,
	GUIPopup_ShowMessage_mD81F2280B13C2F0CA60575456D4707531137D1F6,
	GUIPopup_OnGUI_m5DADAC429F40EA65BFFBFF907855934C588EA830,
	GUIPopup__ctor_mFDED8A24E14871FE680DE90F9B91F54971826B97,
	U3CShowMessageU3Ed__8__ctor_mAB6F5B533E4E6F9E8A40EBAF7760CE490A96C8C9,
	U3CShowMessageU3Ed__8_System_IDisposable_Dispose_mE9E1ED495359DED79D0F13CD157EBDE403127D68,
	U3CShowMessageU3Ed__8_MoveNext_m3C9BB8103AD7C6D794CE6746E11FE8F84171778D,
	U3CShowMessageU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4ACF3C5DA26FB7206A9E7D9F1716CF3DCF4C5B3B,
	U3CShowMessageU3Ed__8_System_Collections_IEnumerator_Reset_mD67A86850AFFE89CC4F85E8D08C43BDEDE9DA34F,
	U3CShowMessageU3Ed__8_System_Collections_IEnumerator_get_Current_mCA7C149643CD2D0E0987E92DD490202826DA5818,
	UIPopupException__ctor_m8C5AB7EFA4151FF4961EA444C19C5865F0702967,
	UIPopupException__ctor_m3F4A7CC28A89A468F68069B67F2CC83D79754338,
	ThreadWorker_Finalize_mBEFF3C1AB6727B72C5747A10847C86E850E49EEC,
	ThreadWorker_Dispose_m6860E0C54304EA3A8E95DC3B783A5822F17A2B0A,
	ThreadWorker_Run_m669EF0D58EFDF157F92FCBCE640CA2C17BAE5AAA,
	ThreadWorker_CreateThread_mBDA0B321F671AB0161E9D9E169927B9F3AD62EF0,
	ThreadWorker_Finish_m26C0F4D2140FC2E36CED8AC7C7CE385E9FA1377F,
	ThreadWorker__ctor_m8E1021EA4750802C61E540330F41FA3E933BF6FD,
	ThreadWorker_U3CCreateThreadU3Eb__6_0_m80AF9A8C9FD34024464AB15ADD3C185E5600DD6B,
	TransformUtil_SetCameraPoseOnCamera_m4B320D0AA8738A3499A7190C6E3E52F8B622BD32,
	TransformUtil_SetCameraPoseOnWorldRoot_m36BCF286E2AA7335D2CC5CA99F7B54254BAD06D6,
	TransformUtil_SetTargetPoseOnCamera_m110BF66AB55BF57E2B0AEBD9B9BBBC013E43F8EF,
	TransformUtil_SetTargetPoseOnTarget_m61EB662F2A768976ABD51E32FC1B56F95EA9C61E,
	TransformUtil_SetMatrixOnTransform_m94ED2BF7230D64F0F6F5AFA7AE32B01DB505F08F,
	TransformUtil_SetPoseOnTransform_m4616153C504051BBFA2DAE5DF9F38E3E13E71B2F,
	EasyARVersion__ctor_mE3A9AA4BF90D37A8D2F2D0AF490DEF78264FD4BB,
	ARAssembly_Finalize_m4E59E92137FEF5C9A09688C59057C387E7AF29FE,
	ARAssembly_get_Ready_mE0D7D9A363183508328A72FA7C587F88859F5A95,
	ARAssembly_set_Ready_m4628605213B7465AA5147702C0AAA558D806DADF,
	ARAssembly_get_RequireWorldCenter_m0C74C6CC8937D31FFE68E706A2F279E258C4DA2F,
	ARAssembly_set_RequireWorldCenter_m7C8C2237D1D06E9E9FEDCE171B6F758FEE8094D9,
	ARAssembly_get_OutputFrame_m3A32D230E7F77FC062789DF06BFB82CC094F1BDE,
	ARAssembly_get_ExtraBufferCapacity_m35B81FB31E8A3A0457388122576D92CC5BDF3C47,
	ARAssembly_set_ExtraBufferCapacity_mAFE9D14E0834C89BF192ACB5E64BEC872B25E502,
	ARAssembly_get_Display_mBC26348B8CEDF967753AC29C6CF134432C45267F,
	ARAssembly_Dispose_mE0840209F40AE98F84E433435C2A781A9E20292A,
	ARAssembly_Assemble_mBA4E331E478C136578F7818934FA8B38F2697D6E,
	ARAssembly_Break_m73276F4521C717870F2A786E5F17ADC16E2FC5B4,
	ARAssembly_Pause_m6662699E2BD483BAFBA4CBB62E928539B7CEE32A,
	ARAssembly_Resume_mDD31AAED79DA43547509D498F4299A026BF99836,
	ARAssembly_ResetBufferCapacity_m5D0A3FBE5FF894EC9E8FAFC6D15AFC0ACFECC053,
	ARAssembly_GetBufferRequirement_m565D94F7788098DFEB9B60CB7912BDA4C315E229,
	NULL,
	ARAssembly_Assemble_m03A0886758FE77CD2ED5CC31BB35C2641FCF0E89,
	ARAssembly_DisposeAll_m3D2063F105AB4054D9404E82F0B8CED238C892B0,
	ARAssembly__ctor_m4C7629E9D99085D2920460784ECC8A0CD02E70C1,
	ARSession_add_FrameChange_m77548873BBFE5CEE577894326237892B05F413E0,
	ARSession_remove_FrameChange_mC3AEE88DD4EF22F919C173CFA09EC594ECF29F2E,
	ARSession_add_FrameUpdate_m3CFB06B201EC6F834CA4ABF4F200695E4FACB08A,
	ARSession_remove_FrameUpdate_mDB1136F48109CC9DA0B48657022D986E0AC516D9,
	ARSession_add_WorldRootChanged_m7FD7A4B2571EB42293F054A5C23127CB960D6FDE,
	ARSession_remove_WorldRootChanged_m5AFE86652B914D379381B33B0C64DB303D8DE049,
	ARSession_get_FrameCameraParameters_m894DA7ABA5970C3B7EEE05BEA3FE8598C3D276B5,
	ARSession_set_FrameCameraParameters_mEA8C1C870A72E8AD0C7C1B6845FE332624923178,
	ARSession_Start_m89C7E9E648DA5641C34299E9E43A84211B2FB0AB,
	ARSession_Update_mB721CDA43C5CB02392EB29628A986CC0223A067C,
	ARSession_OnDestroy_m3C2B80B1956D6584AA68D4BEC48A0029806859B8,
	ARSession_ImageCoordinatesFromScreenCoordinates_m19D24287D63781FD9389B06367F276125B8355F6,
	ARSession_OnFrameUpdate_m8B02AE6174BD04F44EED93B50F1CEC02B7217812,
	ARSession_OnEmptyFrame_m394392E85E00A5B8D471015928FE1B809961CDD3,
	ARSession_DispatchResults_m5AD75B825155E77A5ED57B1F023760642964C420,
	ARSession_GetCompensation_m2DE268AE4B2D196DA4EBAFE647BD9B3BDEF96C82,
	ARSession__ctor_m781F584CFE3BC73219BEA343D9E5D288B6636DEF,
	FrameChangeAction__ctor_m3893ED7641A1E985C2C14E001E1604E2C2360DCD,
	FrameChangeAction_Invoke_m88B7FD21D43F0071691C8D75CBF1999C80747F10,
	FrameChangeAction_BeginInvoke_mC122423CDA618E1D7BABEFCF6DA04D1F421EC533,
	FrameChangeAction_EndInvoke_m6D6C0408DD895B92005EE8AC38FF14936606CFDC,
	NULL,
	FrameFilter_OnAssemble_mD9A848689E8E4CAF441B3BD366424D4B968C929B,
	FrameFilter_SetHFlip_m0A7568E1EE6E8F1006907D7B18A2E6B886D4AB4D,
	FrameFilter_OnHFlipChange_m59055D8C12A22CDE1275F7E0F4D6A7B886379559,
	FrameFilter__ctor_mD5A76B3475BAB035EF935F13EC901CCA8DC6DFA6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FramePlayer_get_IsCompleted_mD323DE4203F437E19C3D052737EC829A2E62831E,
	FramePlayer_get_Length_mBD1475F362608C9215478EEFA874C4A56D36CE2E,
	FramePlayer_get_Time_mABE46BAD8804FFF9D9AC3EEC454D78179B9FAF30,
	FramePlayer_get_HasSpatialInformation_mA13BBDA98504BF1AF1FA866459AA0BE1B9BA2FC8,
	FramePlayer_get_Display_mC71E926B807071F89F2EF0CFD1928EA35D0DD3DD,
	FramePlayer_Awake_mEB16B8D9DB0D86B92A5D2E5F12846E5D5C5AB705,
	FramePlayer_OnEnable_m112DA05A2E50A1E6902E7E9FC3C564945EB4D54C,
	FramePlayer_Start_m41F9FD32489F3705E496F939CB806226A07EBE7B,
	FramePlayer_OnDisable_m9B5FAD0E9698C07CB612809DE5F61FD66128107A,
	FramePlayer_OnDestroy_m3E455EE322BBD3451BA5AC15C907AF12091AB139,
	FramePlayer_Play_m8D693EE1E71F39400C5BB1BB28B1650C764A8E49,
	FramePlayer_Stop_mAE3B5E2358CFF4751FFB95D469F49569F9D357E4,
	FramePlayer_Pause_m95E8A405F632DA7B08A79FF62FDF370D8C4CC689,
	FramePlayer_Connect_m972AD0B00402DC4D23FBF0BD7D1073F7A43CC701,
	FramePlayer__ctor_m4D38929BECC90C9D3BEF4E25931C543164DDB37F,
	FrameRecorder_get_BufferRequirement_m4CF02FEA5B5260729441053A03387AE18EE963F6,
	FrameRecorder_Awake_mF7F2CEFDD245D9BE0CB4A2308210F29165E2CA47,
	FrameRecorder_OnEnable_m8A358D185F40B930618167B68346882D1769C0CD,
	FrameRecorder_Start_mCC7D9595A90A4452BF11B3F303442A441E24F9FC,
	FrameRecorder_OnDisable_m0FC86AFB6D4375B2B6BED75EE5D0815547A40F9C,
	FrameRecorder_OnDestroy_m384E2CAB3D791D1BEBEA509607F0D6F96522419F,
	FrameRecorder_OnAssemble_mE5ACB1466ED9768D130AD3A9ED5922E1688A90E8,
	FrameRecorder_Output_m5A55122086217EB4FF1540367F93A28037136B95,
	FrameRecorder_Input_mC803B50D44D0770B6DC271870E12FF085511141F,
	FrameRecorder__ctor_m1DF3BA697C5948E1E288372749E2D375BFF040A8,
	NULL,
	FrameSource_get_IsHMD_mE8BB50ADAFF835C5B04C582C205212C7DDB260D4,
	FrameSource_OnEnable_m7A12C3BF130A450A12F09E08F7A1FB108855FFF2,
	FrameSource_OnDisable_mBA86309F77ADC1C11D8A34B7DF8E9C9DE0B20AFC,
	FrameSource_Connect_m0E873AAE4D086BEA318B5201F01705CD68CCFB31,
	FrameSource_OnAssemble_m2E417FE61B5B4F8AFD74F97B8B24F7236241DB3C,
	FrameSource__ctor_m6E6038D6F73663C7F145AA71A58AC2D0BE97363F,
	TargetController_get_IsTracked_mCDCD168D5AE7E4FBB89191F60A086BB2159C02B9,
	TargetController_set_IsTracked_mEB0AC36E8C682FA8B2D00B02E65CAE450F40741F,
	TargetController_get_IsLoaded_m74BD4BCDAC8BDA2A60482DB96BEC34F944E38D51,
	TargetController_set_IsLoaded_m7C2707C8D99CAAFC51A250614A4790FD43D99472,
	TargetController_Start_m8A33886CADFA264C59035E4BC9ADC101B58FEE2C,
	TargetController_OnTracking_mF774DB9A49FD1A188D3AFE69BCCC65C623F4AE52,
	NULL,
	TargetController__ctor_m94341ECEBEE9443387A6BCC7FB1E9F2AA13A083B,
	WorldRootController_add_TrackingStatusChanged_mD2CE170147C8A715C42BBD4EF9951AB7C9977A51,
	WorldRootController_remove_TrackingStatusChanged_m0FF0A489070271B1B9B65CB749D396FE388D7508,
	WorldRootController_get_TrackingStatus_m93D0D79F1E994459E25C2692A96D498D1599EB5E,
	WorldRootController_set_TrackingStatus_m1573ED629C5CAA28C2907D85CE636576BBF05968,
	WorldRootController_Start_m7B383CD7B63F42CB1687EDF04532A7E7710ECD66,
	WorldRootController_OnTracking_m9B218A028C9082D84A377F24F0F410DD02B6B1BE,
	WorldRootController__ctor_mE8D1F5FEDB84931D35D2E5C92BF4C374F53E1505,
	DenseSpatialMapBlockController_get_Info_m9B5FAAE3691BEA4AAA516AF8BC75A971D265267F,
	DenseSpatialMapBlockController_set_Info_m13F0699BCA49F39EB168F49AE2199DD7CC7ABFFC,
	DenseSpatialMapBlockController_Awake_m54155F58835F151B71F86BA20E51246A2DBCB90B,
	DenseSpatialMapBlockController_OnDestroy_mE6F8A03B9653202DB9C80A8CF0BCCEB280D0459F,
	DenseSpatialMapBlockController_UpdateData_mD69B5EC20C0696EF9E1EFE19D7A4303B1ECB3260,
	DenseSpatialMapBlockController_UpdateMesh_m362AE28E9099A6783A11CD7EE325759FA9BED1EF,
	DenseSpatialMapBlockController_CopyMeshData_mCA2471B7D717A385962B8DFDFE45AD688189A75A,
	DenseSpatialMapBlockController__ctor_mD187E02BBA42E11DF1488C6083E79B0F5EDBA9DB,
	DenseSpatialMapBuilderFrameFilter_get_Builder_m758140881A3D5EB2FDD98AEF2DBE5412150F6ABD,
	DenseSpatialMapBuilderFrameFilter_set_Builder_m737FCF073162DE6634880B16969D488F26D0A7C6,
	DenseSpatialMapBuilderFrameFilter_add_MapCreate_m50792A04FFC1A0E331C87EBAD4429A612F55392D,
	DenseSpatialMapBuilderFrameFilter_remove_MapCreate_m672731B5112CA728EAC9F8C238D9B1BEEB4C13FB,
	DenseSpatialMapBuilderFrameFilter_add_MapUpdate_m2DED4A7DD8A17E497F480B12B1F438CDE6099BBC,
	DenseSpatialMapBuilderFrameFilter_remove_MapUpdate_m210F0D63D0E6A2AC2A3B88A55296E30729EEDA18,
	DenseSpatialMapBuilderFrameFilter_get_BufferRequirement_m314B7BFE417C2434BFD3C4695775D8E198DCF251,
	DenseSpatialMapBuilderFrameFilter_get_TrackingStatus_m3E17B76E936462D507DD2C5DDC6F6EA56BF48F03,
	DenseSpatialMapBuilderFrameFilter_set_TrackingStatus_m6FB6FEC5F485ECC8E81867E643274C679E6FB8E1,
	DenseSpatialMapBuilderFrameFilter_get_RenderMesh_m4457DCC80C7C4912AB9DBD853F7C4CC9241AF894,
	DenseSpatialMapBuilderFrameFilter_set_RenderMesh_m4872E7CB0A65DF3D1C1AC0870160E61E94DB99A5,
	DenseSpatialMapBuilderFrameFilter_get_MeshColor_m6A366E4F415167C1716E553BDF40525E88483C77,
	DenseSpatialMapBuilderFrameFilter_set_MeshColor_m625234A459EFEFAE7ED2CF12F2244401BB5CC404,
	DenseSpatialMapBuilderFrameFilter_get_MeshBlocks_mDD92DBC2AD9F3404C0B37AF4FEBDA82E3C6FA622,
	DenseSpatialMapBuilderFrameFilter_Awake_mA981315171723F0990D547B6934AA7CC4E029CC8,
	DenseSpatialMapBuilderFrameFilter_OnEnable_m2D80CD2DE5067F0D30BC1ACF2F13EB1B11F746C2,
	DenseSpatialMapBuilderFrameFilter_Start_mE36AAD3A756729CD440A5E55DA92BDBEC4C991DF,
	DenseSpatialMapBuilderFrameFilter_Update_m532A4E73A23BE40BC3F756F286EC6E55C1D56E0B,
	DenseSpatialMapBuilderFrameFilter_OnDisable_m77913A7DEA1D62AFBE1D354F0F8D047E625F6469,
	DenseSpatialMapBuilderFrameFilter_OnDestroy_m3D0C342D5AA66680219616DDA693B1C58B024625,
	DenseSpatialMapBuilderFrameFilter_InputFrameSink_m87AAF6A72DD8C272A5C1E35F35780BBE0A654637,
	DenseSpatialMapBuilderFrameFilter_OnTracking_mE134B8B290413E5294D80342E054E4EB9AE3542B,
	DenseSpatialMapBuilderFrameFilter_OnAssemble_m218526BA10CDFB35BA8B954E4804356EF7926C1B,
	DenseSpatialMapBuilderFrameFilter__ctor_mEE0F6D254AC8124935E87875DEDBE4142F20CA45,
	DenseSpatialMapBuilderFrameFilter_U3COnAssembleU3Eb__41_0_mB7091E6C9A2299026A8DBA9B6790D69D225AEB91,
	DenseSpatialMapDepthRenderer_get_RenderDepthCamera_m681A99424571E53FC47EE5FA06DA4662882FC1F2,
	DenseSpatialMapDepthRenderer_set_RenderDepthCamera_m492EF4A696EF204AD15D3E94CF170B8E9ABD6F45,
	DenseSpatialMapDepthRenderer_get_MapMeshMaterial_m0BBD1D7CD30168722CCD1758403766320276DF87,
	DenseSpatialMapDepthRenderer_set_MapMeshMaterial_mC72C0357A3454D90FE3AD3493D0ADE32C158EF36,
	DenseSpatialMapDepthRenderer_LateUpdate_mFF013734FA3C55BDFA17E066762ADDBE24C0D161,
	DenseSpatialMapDepthRenderer_OnDestroy_m3178E1B3E24B2BB94325319DFCEB1A670E9B0249,
	DenseSpatialMapDepthRenderer__ctor_m2DE033ADB30C69621A1C40F761CECEDC2584EFC5,
	SparseSpatialMapController_get_MapInfo_m9F7973AF582AB3D534F5A78C60A5B9EA4FF77A09,
	SparseSpatialMapController_set_MapInfo_m9A2E132F01CDA34B585D1393BDB53914392E6148,
	SparseSpatialMapController_add_MapInfoAvailable_m2862A31F3E802A743EB2EFD089685CAAEE2ABD56,
	SparseSpatialMapController_remove_MapInfoAvailable_mE56EBAB6010C146C210663CDF819CD10602D897F,
	SparseSpatialMapController_add_MapLocalized_m1E916CEF68A96614F5550D3ED1E13B5BAE9F64B3,
	SparseSpatialMapController_remove_MapLocalized_m9B100A0D840C4240F4EE3D07078C36B28E4CC18C,
	SparseSpatialMapController_add_MapStopLocalize_m47AD420BFD0385FEEFC65E3FE60316C2A3BA65F6,
	SparseSpatialMapController_remove_MapStopLocalize_mA18DA2D96E3D564B641366E2D1878197A572F88C,
	SparseSpatialMapController_add_MapLoad_m1EAD1D28F05893F5F24E830D895C07650119676E,
	SparseSpatialMapController_remove_MapLoad_m948B3F5628A733F6ED2E96668F4FFCFACD494333,
	SparseSpatialMapController_add_MapUnload_m2FB811A896D21DDF1CE0365F4672BF245B99F69F,
	SparseSpatialMapController_remove_MapUnload_m662469C6CE6B9C1ADCE099947BF8FB0AAAE3883B,
	SparseSpatialMapController_add_MapHost_mDD77B7155B17BBB8F9A4737B6C8F03CE408208BE,
	SparseSpatialMapController_remove_MapHost_m053C92797E2D6D610C67E10ECB87A00ADC7459B2,
	SparseSpatialMapController_get_MapWorker_mB62DF8B969EC1EFFF172B53578B16CD6501A37D0,
	SparseSpatialMapController_set_MapWorker_m0335162EA6291F30BE50023CBFFA82E0BB41F57F,
	SparseSpatialMapController_get_PointCloudParticleParameter_m669D5277F4CD0CE6847FAD89CB6BFC7B5931982F,
	SparseSpatialMapController_set_PointCloudParticleParameter_m546461FB2170E19D1C80C4D81E86AB10041E7F40,
	SparseSpatialMapController_get_PointCloud_m9E47A3C47C14727138FFB5EF22CFB19EED8DDC83,
	SparseSpatialMapController_set_PointCloud_m5741878569506D3FF8A9541478C658577DA831EF,
	SparseSpatialMapController_get_ShowPointCloud_mB5F6D574CBE38ADC064D67BB2A3867AFEFC7B21D,
	SparseSpatialMapController_set_ShowPointCloud_m5D28774E6B24850387C5986D315509A61AEA1BD5,
	SparseSpatialMapController_get_IsLocalizing_m18ECA11E492F62E020E84D70D19E52FAA5418A14,
	SparseSpatialMapController_set_IsLocalizing_m3953314E7CC78205D195598D224B1FA8C7F0885E,
	SparseSpatialMapController_Awake_m9ED6E1D46820FE5A085F20411262693A30EE5941,
	SparseSpatialMapController_Start_mE9783C356927904369A8C7B09974F54A0B5A5A60,
	SparseSpatialMapController_OnDestroy_m1170B19319F3589E968540CFE91CA1A295F68371,
	SparseSpatialMapController_HitTest_m5C0FB7B9A56193CFB2B79F0117F4B0C892AD5D6E,
	SparseSpatialMapController_Host_m60B0F3963441A28AFD59A9D43FD96E544F0903F3,
	SparseSpatialMapController_OnLocalization_m1913FDDB9CA77D2CB3A57A2540991873E6BEDB1C,
	SparseSpatialMapController_UpdatePointCloud_m848BCD707665213E68F268789CD6D17B6687D868,
	SparseSpatialMapController_UpdatePointCloud_mB15FB7D462ADDC401AB1FFE3A1DCB2BC3D4DDBF1,
	SparseSpatialMapController_LoadMapBuilderInfo_mCB3ED40C23F25BA1D568637F0EC96476C985658A,
	SparseSpatialMapController_LoadMapManagerInfo_mDDAE856BB30D0C66B8C47399147E40557A9E27A0,
	SparseSpatialMapController_LoadMapInfo_mB3B752B04D5C4B922DF3AF62E241AAB1CF827B17,
	SparseSpatialMapController_UpdateMapInLocalizer_m6C453C3768E0C64A06D39BFFAB245AE87DB8F6F0,
	SparseSpatialMapController__ctor_m1060907071F7E835CFC52DF13D20E638662F5F80,
	SparseSpatialMapController_U3CHostU3Eb__56_0_m3CAEB4EC3D350CCFA68C581013A003178E786564,
	SparseSpatialMapController_U3CUpdatePointCloudU3Eb__59_0_m02E8F1ACE41A94F38155BAB09A9EC88EED9B1202,
	SparseSpatialMapController_U3CUpdateMapInLocalizerU3Eb__63_0_mF3859885855B1D4DAE5DE4A90F9F337DB9E26733,
	SparseSpatialMapInfo__ctor_mCD732AB4A876F3155F62AFB7583B8C443DF656F1,
	MapManagerSourceData__ctor_mEC4C9781E8F8E872CE3D43FEF52B76F7E37C3D61,
	ParticleParameter__ctor_mF348917BAFD52AADB96F0E5649C604CFA3770739,
	U3CU3Ec__DisplayClass58_0__ctor_m9BF560C6D2F6042C8B3E8A19D94B21C8924DB2F5,
	U3CU3Ec__DisplayClass58_0_U3CUpdatePointCloudU3Eb__0_m2A25C9C0405219D6C784FD053A43A3317477DF74,
	U3CU3Ec__DisplayClass63_0__ctor_m19AE58683953A5C40AF6F2760B6FCC02266EE1DF,
	U3CU3Ec__DisplayClass63_0_U3CUpdateMapInLocalizerU3Eb__1_mBB4730BEC677A6AD74141F35CF25FCB5B7AB3FCC,
	SparseSpatialMapWorkerFrameFilter_get_Builder_mBDD7FD5C1D5874ECEA16C70E90EBB24B60A156D2,
	SparseSpatialMapWorkerFrameFilter_set_Builder_m8FB3708CF2035B1B9F3143EF73679478F41D533B,
	SparseSpatialMapWorkerFrameFilter_get_Localizer_m481D0F8E9147E444A72E4E5314E274A149015FFD,
	SparseSpatialMapWorkerFrameFilter_set_Localizer_mF300368DA1D996D55EE36DCDB612A2A19A1205FF,
	SparseSpatialMapWorkerFrameFilter_get_Manager_m3FEB082165C0C8373D273049674055AF80FEA82C,
	SparseSpatialMapWorkerFrameFilter_set_Manager_mD110FC2EDB5EF821F0FE1CB19AAACD3EB92927CD,
	SparseSpatialMapWorkerFrameFilter_add_MapLoad_m9761FF716AD4AB2E7B31B8B95F167D55A743EEDF,
	SparseSpatialMapWorkerFrameFilter_remove_MapLoad_mF1EA708E880E9F28E711B6189275A1549F55E704,
	SparseSpatialMapWorkerFrameFilter_add_MapUnload_mACB33B7581BAAAE760DDC69C6C91034978F21414,
	SparseSpatialMapWorkerFrameFilter_remove_MapUnload_m0AD3F4D12187B774221F0895C7DDC7B60831EC7E,
	SparseSpatialMapWorkerFrameFilter_add_MapHost_mB906F4AF336AFAB07D645A0EEAEFCE1516CE5727,
	SparseSpatialMapWorkerFrameFilter_remove_MapHost_mF4B8E4EEC4D44230DAD3148CB92E1A7744B279B7,
	SparseSpatialMapWorkerFrameFilter_get_BufferRequirement_m7845B242B6C5ED0227EAA1D157E28818FBDD4C88,
	SparseSpatialMapWorkerFrameFilter_get_TrackingStatus_mF01A0D9EADE683AA43A9A09567B9C29CA231F300,
	SparseSpatialMapWorkerFrameFilter_set_TrackingStatus_mFEA33C5312DB9C0A699EE011CF98CF4955465D9A,
	SparseSpatialMapWorkerFrameFilter_get_WorkingMode_mF8E9D3D97D5F1BC30860D7CAD2733F96EC97DE84,
	SparseSpatialMapWorkerFrameFilter_set_WorkingMode_m149AC1EE7A6BAEF8771908A6BD493F27AD0BF5CE,
	SparseSpatialMapWorkerFrameFilter_get_LocalizedMap_m58350A5B898D5E2D5437F76AD6B670CDC0BFC55F,
	SparseSpatialMapWorkerFrameFilter_set_LocalizedMap_m2006E253D0EE4E6D526491D2287EC70DBE4D6353,
	SparseSpatialMapWorkerFrameFilter_get_BuilderMapController_m3B422BADE125C970974568C6AEA40AA76B33988A,
	SparseSpatialMapWorkerFrameFilter_set_BuilderMapController_mF5A767EE02F7F72607BC17A0A188478C3BF3AF88,
	SparseSpatialMapWorkerFrameFilter_get_MapControllers_m7F54CCB59965E81B6E646442391498323B64B1E5,
	SparseSpatialMapWorkerFrameFilter_Awake_m25F8F48DB6D1F5E56DECA3E81B15336948C5416E,
	SparseSpatialMapWorkerFrameFilter_OnEnable_m9511A8AFB2D8973E375E8A4352C712F5F0F7B245,
	SparseSpatialMapWorkerFrameFilter_Start_mA8A0F7D64CA02DDCBF4E97A057343ED30D01EDB6,
	SparseSpatialMapWorkerFrameFilter_OnDisable_m99E4BB1020148384E0469EFCDB69D3948F68D619,
	SparseSpatialMapWorkerFrameFilter_OnDestroy_m1098AB5C3FC342DC9A8ED83E2D058F90B8A1F8F8,
	SparseSpatialMapWorkerFrameFilter_LoadMap_m06F7C2E177AE7E9BAFD3CB698DC334CE3F43ECC6,
	SparseSpatialMapWorkerFrameFilter_UnloadMap_mCBFAEDDEA7601B1BC0DD7BC0548C3FDA4651E7A3,
	SparseSpatialMapWorkerFrameFilter_HostMap_m2D6DBF8C93EDB848B1A5DD74BEC74B1780407571,
	SparseSpatialMapWorkerFrameFilter_InputFrameSink_m5F5BB68CCE1223BD89A6F6CFBAF3BAC2FC1FF230,
	SparseSpatialMapWorkerFrameFilter_OutputFrameSource_m5D80F2259FBD057FD2BF029B5E50F7EC65876FBE,
	SparseSpatialMapWorkerFrameFilter_OnTracking_m98D51644FF122D808823EC0878ED351E7F9E15D0,
	SparseSpatialMapWorkerFrameFilter_OnAssemble_mF947FBECB928CFF393F6BAA87BD9BE909C4998F4,
	SparseSpatialMapWorkerFrameFilter_OnResult_m394FA667CFD72EAD0B31CB6A0D19EB3A6824EE6B,
	SparseSpatialMapWorkerFrameFilter_LoadSparseSpatialMap_mB9F12BA3C6BC632D600B9A67F4D4E48CC5DD2843,
	SparseSpatialMapWorkerFrameFilter_UnloadSparseSpatialMap_m67A02DF6BE72011040F2358358ECE1DAA3CB96FD,
	SparseSpatialMapWorkerFrameFilter_HostSparseSpatialMap_m2771594249E5505ECA30A742F7E9B67EFB1DF646,
	SparseSpatialMapWorkerFrameFilter_LoadSparseSpatialMapBuild_m59759E14AA4A10642A76B1ABDABA4729932BE984,
	SparseSpatialMapWorkerFrameFilter_UnloadSparseSpatialMapBuild_m8B3CBCF56AC0BD1178D44C16D05778E6109AE02D,
	SparseSpatialMapWorkerFrameFilter_TryGetMapController_m1E5CB46E6EB44D12426290FEC8ACBD8C8656EA34,
	SparseSpatialMapWorkerFrameFilter_NotifyEmptyConfig_mAE65C3A3AAF64FC0BC741F9ECDC086EBC3815759,
	SparseSpatialMapWorkerFrameFilter__ctor_m1EFB5826D5C0EFE2723DE27E5DCA43E8B4B3CE84,
	SparseSpatialMapWorkerFrameFilter_U3COnAssembleU3Eb__62_0_m1C6E2C8CF69B82D1DF8CE6FBECC0F00C9EA2AA15,
	MapLocalizerConfig__ctor_m4FC8CD78E895F314865F830C33080C0C96A0F775,
	SpatialMapServiceConfig__ctor_mA4998B092B643B05664589BF6CACB5BD001B4DB1,
	U3CU3Ec__DisplayClass64_0__ctor_mEA69A6219847D01B58AEEA53C1A3C3D1D2745EC9,
	U3CU3Ec__DisplayClass64_0_U3CLoadSparseSpatialMapU3Eb__0_m1E4A235FD28AAA321B41363A3DE85D6D054D6127,
	U3CU3Ec__DisplayClass65_0__ctor_m3D4794B9D67A798D9C8EC7A48437F6E25BB028CE,
	U3CU3Ec__DisplayClass65_0_U3CUnloadSparseSpatialMapU3Eb__0_m4175627CA8C02EA9C439FE7F58E845E06851330F,
	U3CU3Ec__DisplayClass66_0__ctor_m12FC1D51B727584157542E9DEC9D57B3BD8AD77A,
	U3CU3Ec__DisplayClass66_0_U3CHostSparseSpatialMapU3Eb__0_m9182CA103270E33CA7D49BC825B645AA54B56113,
	SurfaceTrackerFrameFilter_get_Tracker_m3515EE44BB36EDE69EA10E7DFB43725F308B37E8,
	SurfaceTrackerFrameFilter_set_Tracker_mE4E2C8642D3A95893D082229CB198D89059ACCB8,
	SurfaceTrackerFrameFilter_get_BufferRequirement_mD8E622F9ABF6B7C6A177027B6E14ED848864BC78,
	SurfaceTrackerFrameFilter_Awake_m94269E304BDEDE5FB4034A25FD94636FFEDFD20A,
	SurfaceTrackerFrameFilter_OnEnable_m3CEFCB2BBBC576E03D8EE258D8C45C97B52AC1D4,
	SurfaceTrackerFrameFilter_Start_m3398EA0F512EB7EF15107E95BF754E5D1BC47E95,
	SurfaceTrackerFrameFilter_OnDisable_m2DA389874D3D534850F2A8BFC3F70706CC20EFE9,
	SurfaceTrackerFrameFilter_OnDestroy_mB5A68C415E73D1648BF5D85A253CD0768E581B4B,
	SurfaceTrackerFrameFilter_InputFrameSink_mCE559F0CF774BB125F1AF76AA777C0CC5B0EA6C5,
	SurfaceTrackerFrameFilter_OutputFrameSource_mE86E2ADE305407A9971A95C7BEB83D99970201AB,
	SurfaceTrackerFrameFilter_OnResult_mD11A18271B13CEEC49AFDFF9347B6DBEF3F8F79C,
	SurfaceTrackerFrameFilter__ctor_mA3A77E0400ADEEDF557453CED049CE43E0AF2696,
	VIOCameraDeviceUnion_get_Device_mFE46D8F19218FECD9FF165FE6E9759992BD707AA,
	VIOCameraDeviceUnion_set_Device_m9B7BA8186E8CA67F5674BBA422FC088F10786232,
	VIOCameraDeviceUnion_add_DeviceCreated_m74AD89492D2711324561C8E330BADB762414D47C,
	VIOCameraDeviceUnion_remove_DeviceCreated_m814DE270549834E858D6F5DA0DA2998774673B23,
	VIOCameraDeviceUnion_add_DeviceOpened_m19356CB413BC5584A315200A6C88CCDD82736F90,
	VIOCameraDeviceUnion_remove_DeviceOpened_m23CAF95355804DDDF8E0FABA8BC358D9638813E6,
	VIOCameraDeviceUnion_add_DeviceClosed_m5B573063D9E27DA34B996A6C0220A4F4579AE97B,
	VIOCameraDeviceUnion_remove_DeviceClosed_m29E68A81FFAEB479EE86B4F072F416A8CE65B1EA,
	VIOCameraDeviceUnion_get_BufferCapacity_mF92FB40A1AA7185C05055BC8BA1B258C63D425C4,
	VIOCameraDeviceUnion_set_BufferCapacity_m43DA832DE9AFD12060C1A5D5FF46025B1E4CA0BB,
	VIOCameraDeviceUnion_get_HasSpatialInformation_mFE45759DD9E710CCC60CE6E0EF382555CBF6F4C5,
	VIOCameraDeviceUnion_OnEnable_m1CD9C7E567F7916713973C710A34683660E00063,
	VIOCameraDeviceUnion_Start_m9A131FC6ECDF09499BB174DD6D77CC544C3D3FBA,
	VIOCameraDeviceUnion_OnDisable_m2FE3A6583CD9A17742AA50527BCF6570A080AE54,
	VIOCameraDeviceUnion_HitTestAgainstHorizontalPlane_m324F64200611FDE8B4BD937E0C7427B53D1F759E,
	VIOCameraDeviceUnion_HitTestAgainstPointCloud_mC7767E6028282E2C202074F6B0BDF6054A67D1A1,
	VIOCameraDeviceUnion_Open_m612581D070F3149183FAB906756EF4876A59F49E,
	VIOCameraDeviceUnion_Close_mB9E611FE58DE480C8DAAADA67FDA46D6475D38D8,
	VIOCameraDeviceUnion_Connect_m414A550851F691BE79005861A1FE3032A2A23DE2,
	VIOCameraDeviceUnion_CreateMotionTrackerCameraDevice_m0223C71EDA1D077DE1191B9CB6377665E48A9FBD,
	VIOCameraDeviceUnion_CreateARKitCameraDevice_m87901367060D75FB00E07F4C9919F01200032A7E,
	VIOCameraDeviceUnion_CreateARCoreCameraDevice_m4C8D7FF57E06C4FC676C2FBAE0F843CD2C51BE2F,
	VIOCameraDeviceUnion_CheckARCore_m35855E7EAFA4867DAC69FEC8DC90A53E888E78B7,
	VIOCameraDeviceUnion__ctor_mED699082D218F5A5E3B9EC7747CB1E9AC2129967,
	VIOCameraDeviceUnion_U3COpenU3Eb__35_0_m6EF254CCD5BFA74F72BCE71EA2E0C494780258A0,
	DeviceUnion__ctor_m42BE7827CFB2D9FF4E6253373351A8FC3562A937,
	DeviceUnion__ctor_mFE6386AF99318072092D638781A86280C9BFD81F,
	DeviceUnion__ctor_mFE048003E68E9B6BE5DAC1A4EF3C76E1A8DF0E3B,
	DeviceUnion_get_DeviceType_m3C4A65E1E009312D2B43DA6C9A89ADBDFC457536,
	DeviceUnion_set_DeviceType_m07F63903240ECF385A4C8E518FC8A7733BFF528C,
	DeviceUnion_get_MotionTrackerCameraDevice_m8B42F6AC6B41F0901184C306406564D1B48F9C77,
	DeviceUnion_set_MotionTrackerCameraDevice_mF15CA8EAB1010703DCC5361C73D1D1FF1C95DD1A,
	DeviceUnion_get_ARKitCameraDevice_m749823C8890CD578CE733E922602E55369A67597,
	DeviceUnion_set_ARKitCameraDevice_m8D56622C96FC6135148139F74BBC2BBC3FFB8511,
	DeviceUnion_get_ARCoreCameraDevice_mBFB7644ACC4238AE4A76C336CBAAAD435BBCDF36,
	DeviceUnion_set_ARCoreCameraDevice_mB09C9671ABD8F810CE339DBDFF767BFA42C8220B,
	DeviceUnion_op_Explicit_m097C87EB49833AFBA43BFDDA9212A814B639882B,
	DeviceUnion_op_Explicit_mF59DBDA51A7A0882EBDF6DBDBB2E988B4371FD79,
	DeviceUnion_op_Explicit_mB33D9559094F3DD71CD9A572888D53762FABC1D8,
	DeviceUnion_op_Implicit_m317B7F9378C62D68CBD7024124ADB1816A8BE2C3,
	DeviceUnion_op_Implicit_m897FB74DF520F9EE362080EA7F50E16B035512E0,
	DeviceUnion_op_Implicit_mA20D8DD8D1AEE404315C07C11F448DD04401B556,
	DeviceUnion_Type_m6524D087BBA8401EA8EF88FCB0F7BB77580B45AD,
	DeviceUnion_ToString_mF0EA7AC8B8DCDFD3A70E06CD19DD33B391E8D0C4,
	MotionTrackerCameraDeviceParameters__ctor_m34CDA873F01AEF2523CA2E9D8B4040CC46808C3E,
	U3CU3Ec__DisplayClass31_0__ctor_mD2E9CE71AE78A1396ED6577530F4853C47D242E1,
	U3CU3Ec__DisplayClass31_0_U3CStartU3Eb__0_mD753B72621EC50CA6FC0779D4A1B5536E33D8B1B,
	U3CU3Ec__DisplayClass38_0__ctor_mE10B400AC5B66BD025374C271F0FB1F15E6AE000,
	U3CU3Ec__DisplayClass38_0_U3CCreateMotionTrackerCameraDeviceU3Eb__0_m9C1E3A91D3151CE4A96E9C7F314D15D329917DB5,
	U3CU3Ec__DisplayClass38_0_U3CCreateMotionTrackerCameraDeviceU3Eb__1_m93D64D686BB2A1514134BA801D1F178F210D4E21,
	U3CU3Ec__DisplayClass38_0_U3CCreateMotionTrackerCameraDeviceU3Eb__2_m6A5523B26A148FA5ADE6117ED75857C612573F16,
	U3CU3Ec__DisplayClass38_0_U3CCreateMotionTrackerCameraDeviceU3Eb__3_m0B0F363DA396E8704E9F910B3922AAF6FF167ECA,
	U3CU3Ec__DisplayClass38_0_U3CCreateMotionTrackerCameraDeviceU3Eb__4_m4533E42DCA0BE1194634AAEE3387ED5D40140E84,
	U3CU3Ec__DisplayClass38_0_U3CCreateMotionTrackerCameraDeviceU3Eb__5_m23E0990A11F325669E4B63986E6EEF0E85BA4CF5,
	U3CU3Ec__DisplayClass39_0__ctor_mEB93E2798B4E3F271E37BD63A51C2C897EEAFC0B,
	U3CU3Ec__DisplayClass39_0_U3CCreateARKitCameraDeviceU3Eb__0_mE54857557C90DA065E65A151B7213B659ABE5B7D,
	U3CU3Ec__DisplayClass39_0_U3CCreateARKitCameraDeviceU3Eb__1_m132BC42564B6E6893A7CFB36CC162CB15A76F608,
	U3CU3Ec__DisplayClass39_0_U3CCreateARKitCameraDeviceU3Eb__2_m7D6102876579DE633CBB9C25DCA95EC6605A485A,
	U3CU3Ec__DisplayClass39_0_U3CCreateARKitCameraDeviceU3Eb__3_m74C6365AEF2BB9E23DA65E9489D787AAE3B299DB,
	U3CU3Ec__DisplayClass39_0_U3CCreateARKitCameraDeviceU3Eb__4_mD745157CE408F62396258C3AB7AB19C3F23D61A8,
	U3CU3Ec__DisplayClass39_0_U3CCreateARKitCameraDeviceU3Eb__5_mBB62A04157673A22F093EF85C050CB2BB6FDD444,
	U3CU3Ec__DisplayClass40_0__ctor_m3D36A66D351565CB02679989918F8BEFD97EF0A4,
	U3CU3Ec__DisplayClass40_0_U3CCreateARCoreCameraDeviceU3Eb__0_mE59BA8E0790239EB80BD815A7CF374158DB2EB2A,
	U3CU3Ec__DisplayClass40_0_U3CCreateARCoreCameraDeviceU3Eb__1_mBB2F2A9508CCC94BDABB5CE902D0B9E30DCCD5EF,
	U3CU3Ec__DisplayClass40_0_U3CCreateARCoreCameraDeviceU3Eb__2_m86D4C558C4B88BCB133288CDA72CF5DE991A815B,
	U3CU3Ec__DisplayClass40_0_U3CCreateARCoreCameraDeviceU3Eb__3_m722A845CC0BF1A95FD760C46570890F5FDB95865,
	U3CU3Ec__DisplayClass40_0_U3CCreateARCoreCameraDeviceU3Eb__4_m9F9086D23582BA66C5F0E3B62A2E08715E8B0AA2,
	U3CU3Ec__DisplayClass40_0_U3CCreateARCoreCameraDeviceU3Eb__5_m7710FE5A0C4DF48A3FF0EA2219CEFD63DBDFE069,
};
extern void OptionalOfBuffer_get_has_value_mD6F26A29092013167861EE3E660476683A64529C_AdjustorThunk (void);
extern void OptionalOfBuffer_set_has_value_mDC4547AE9C5D5CB894F23AB1AC16BB2B758D3CC4_AdjustorThunk (void);
extern void OptionalOfObjectTarget_get_has_value_mF804B145E14437EA95BD0A5664AA16887425D034_AdjustorThunk (void);
extern void OptionalOfObjectTarget_set_has_value_mF9205E39066CE8464514E8A917412EE576AAE862_AdjustorThunk (void);
extern void OptionalOfTarget_get_has_value_m54BFBDD1FA4C03B32E0254203C95AB89471226C8_AdjustorThunk (void);
extern void OptionalOfTarget_set_has_value_mD53D8270DE95BE1FE4DB151129C9654A6DAD4BCB_AdjustorThunk (void);
extern void OptionalOfOutputFrame_get_has_value_m16750E6F8AC1CDD2105035942148EC3F17A2731C_AdjustorThunk (void);
extern void OptionalOfOutputFrame_set_has_value_m0ED852A17431EA3157B7D32E3579B90F9DB11AA8_AdjustorThunk (void);
extern void OptionalOfFrameFilterResult_get_has_value_m0B5B694A1819769F52A8B8B676DD8E7A504C4A1B_AdjustorThunk (void);
extern void OptionalOfFrameFilterResult_set_has_value_mA6CDF1D438A978182E3D705DABCFB18A46835EC8_AdjustorThunk (void);
extern void OptionalOfFunctorOfVoidFromOutputFrame_get_has_value_mAFC7BA70B0524FC0FFE85A533C0BD64F6C07E41A_AdjustorThunk (void);
extern void OptionalOfFunctorOfVoidFromOutputFrame_set_has_value_mDAB3D368167065A00CA62736E0677FBC5A4D8540_AdjustorThunk (void);
extern void OptionalOfString_get_has_value_m9CD5A6C104367A1ED5B9E44785BA721056E0D4AE_AdjustorThunk (void);
extern void OptionalOfString_set_has_value_m264C0ED811DA93486B2DE04A53B25EE094DE2C6A_AdjustorThunk (void);
extern void OptionalOfImageTarget_get_has_value_m1EF7147FD931E8F917DEB6C46BD51A7563022D33_AdjustorThunk (void);
extern void OptionalOfImageTarget_set_has_value_m8644B289D2E7A1195C693BB4E9843FE22F44C7A2_AdjustorThunk (void);
extern void OptionalOfFunctorOfVoidFromInputFrame_get_has_value_m1D155C98CBF075D904FF4031CAA79FA57AC1324C_AdjustorThunk (void);
extern void OptionalOfFunctorOfVoidFromInputFrame_set_has_value_m9E8B4E0E2EDD811B56D788AA37AC28C6CDFC2D92_AdjustorThunk (void);
extern void OptionalOfFunctorOfVoidFromCameraState_get_has_value_m8B4EBF20C2AAD048E9D671047BEE42BF9D07308B_AdjustorThunk (void);
extern void OptionalOfFunctorOfVoidFromCameraState_set_has_value_m1ED45DFE473C86A2641CF8823E93D759BBBFF7D1_AdjustorThunk (void);
extern void OptionalOfFunctorOfVoidFromPermissionStatusAndString_get_has_value_m81D80E41F5A8E24E6396A58596E192C4CF7DE650_AdjustorThunk (void);
extern void OptionalOfFunctorOfVoidFromPermissionStatusAndString_set_has_value_m92FE8F26A4611C4D1D925EF3DB553A81F482D896_AdjustorThunk (void);
extern void OptionalOfFunctorOfVoidFromRecordStatusAndString_get_has_value_m40F62F2C9F50F336DB495B577C99D4AC9849FFD5_AdjustorThunk (void);
extern void OptionalOfFunctorOfVoidFromRecordStatusAndString_set_has_value_mCC230A3F9E9860FC8BD386C044F043F177F7E852_AdjustorThunk (void);
extern void OptionalOfMatrix44F_get_has_value_m9C30778B7D9E982A11356624459B1D0A08A30AD0_AdjustorThunk (void);
extern void OptionalOfMatrix44F_set_has_value_m68B97FAF4E20F2D0EE93221D7C51FE60F5EE9B55_AdjustorThunk (void);
extern void OptionalOfFunctorOfVoidFromBool_get_has_value_m7D8DFE0840F0E53FA25E3ADEE84C54DDACCBE6BA_AdjustorThunk (void);
extern void OptionalOfFunctorOfVoidFromBool_set_has_value_m077DB0E0292E386E9A1925D6161E580CB9A57B8B_AdjustorThunk (void);
extern void OptionalOfImage_get_has_value_m7932A8718D733AE7A914D6726A3A8FEBBB4E132A_AdjustorThunk (void);
extern void OptionalOfImage_set_has_value_mFA950AF9ADE589082DE916C46EAC5280D8C41CA4_AdjustorThunk (void);
extern void OptionalOfFunctorOfVoidFromVideoStatus_get_has_value_m4C5A5EB6B25090AC80D73CF3A3578AF663F68354_AdjustorThunk (void);
extern void OptionalOfFunctorOfVoidFromVideoStatus_set_has_value_m798E54F282B4763352BFE839DB21AA648DF5BABB_AdjustorThunk (void);
extern void OptionalOfFunctorOfVoid_get_has_value_m674E9E83E83F23CA50AAE7266ED19DFAC9EEA2ED_AdjustorThunk (void);
extern void OptionalOfFunctorOfVoid_set_has_value_m074530599AED813FDFBE0893F9A8D84B30BBB57A_AdjustorThunk (void);
extern void OptionalOfFunctorOfVoidFromFeedbackFrame_get_has_value_mD61A0B63DDB2445940E42029006B6E5220AACDF8_AdjustorThunk (void);
extern void OptionalOfFunctorOfVoidFromFeedbackFrame_set_has_value_mC6C7579BB351079A6BA5EF5DD10B402EBD9E9B84_AdjustorThunk (void);
extern void Matrix44F_get_data_m67C0A1BF271A6C366D67ED360291B61197CCA09F_AdjustorThunk (void);
extern void Matrix44F_set_data_m7C3634F7119F87B60156682FA5A868E25791AB97_AdjustorThunk (void);
extern void Matrix44F__ctor_m08360D8DD00F4951F4B3A7905BF5F08D3D865036_AdjustorThunk (void);
extern void Matrix33F_get_data_m85D37D7D472AB79A7962EDCB3D01E70F85FDFBA4_AdjustorThunk (void);
extern void Matrix33F_set_data_m501DC97575DC9045BCBEA35973CEE131C695A5A0_AdjustorThunk (void);
extern void Matrix33F__ctor_m3683E7AB93523F0178BE0A9E5C8961B02A49F44E_AdjustorThunk (void);
extern void Vec3D_get_data_m899EF2E47941ACA4FB20AC16FAD0EB6F52C2820C_AdjustorThunk (void);
extern void Vec3D_set_data_mA768D5E64217EEE2119C35022EB2BBB34EBA91DA_AdjustorThunk (void);
extern void Vec3D__ctor_mE118A85DCBF981E4D1FB28A10DB58FBCCCFBC3C0_AdjustorThunk (void);
extern void Vec4F_get_data_mAB650AF267950B46EC508F2C8B93F26FCF8363D0_AdjustorThunk (void);
extern void Vec4F_set_data_m62535D380DECDD9CD28F7A778EDCCEC0B26C5FA9_AdjustorThunk (void);
extern void Vec4F__ctor_mD5ED30DAD10566BBAE3D3F24E5130A516D77269E_AdjustorThunk (void);
extern void Vec3F_get_data_m6AB194630146D7297D96735F9E95AD83C044604B_AdjustorThunk (void);
extern void Vec3F_set_data_mEECA60CD3B2A9829F912884D06CC385A63B5DDB8_AdjustorThunk (void);
extern void Vec3F__ctor_m048B740E6E14E759A3AE6BA9EDC537B35D3F32C8_AdjustorThunk (void);
extern void Vec2F_get_data_m2BFD2DF1F83590E815DBF7FD2F18F2A74251DE13_AdjustorThunk (void);
extern void Vec2F_set_data_m511CE6F198ED6BF129F53080ACE5255A69BC7C44_AdjustorThunk (void);
extern void Vec2F__ctor_m54CAE94E831DCCA2714C0B2C38581F238452933D_AdjustorThunk (void);
extern void Vec4I_get_data_mA13BC5AC938DA168120751B0301BC644D7ADDD91_AdjustorThunk (void);
extern void Vec4I_set_data_mE1291E4B6CF192491813EAC0EA8BB331B474EBA2_AdjustorThunk (void);
extern void Vec4I__ctor_m9716FFE4C4B6410244BDBD51F1445186E25A6B08_AdjustorThunk (void);
extern void Vec2I_get_data_m8C330A4BEF24736568B464D9A97FE12108A53218_AdjustorThunk (void);
extern void Vec2I_set_data_mBD37D15E328F8F2746003C169F93976A499A342B_AdjustorThunk (void);
extern void Vec2I__ctor_m79DAD76534551BB0FC8246F6948CE5BF9DF38045_AdjustorThunk (void);
extern void BlockInfo__ctor_m56026AA781BF97B7CE29DF961342F00D1B57FB52_AdjustorThunk (void);
extern void AccelerometerResult__ctor_m6B5CEB6914B368B1E58761FF3A309544749E2716_AdjustorThunk (void);
extern void MagnetometerResult__ctor_mD167F23B0247DBCF93D8EBA039DDA2822DD8E3ED_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[63] = 
{
	{ 0x060003BC, OptionalOfBuffer_get_has_value_mD6F26A29092013167861EE3E660476683A64529C_AdjustorThunk },
	{ 0x060003BD, OptionalOfBuffer_set_has_value_mDC4547AE9C5D5CB894F23AB1AC16BB2B758D3CC4_AdjustorThunk },
	{ 0x060003C6, OptionalOfObjectTarget_get_has_value_mF804B145E14437EA95BD0A5664AA16887425D034_AdjustorThunk },
	{ 0x060003C7, OptionalOfObjectTarget_set_has_value_mF9205E39066CE8464514E8A917412EE576AAE862_AdjustorThunk },
	{ 0x060003C8, OptionalOfTarget_get_has_value_m54BFBDD1FA4C03B32E0254203C95AB89471226C8_AdjustorThunk },
	{ 0x060003C9, OptionalOfTarget_set_has_value_mD53D8270DE95BE1FE4DB151129C9654A6DAD4BCB_AdjustorThunk },
	{ 0x060003CA, OptionalOfOutputFrame_get_has_value_m16750E6F8AC1CDD2105035942148EC3F17A2731C_AdjustorThunk },
	{ 0x060003CB, OptionalOfOutputFrame_set_has_value_m0ED852A17431EA3157B7D32E3579B90F9DB11AA8_AdjustorThunk },
	{ 0x060003CC, OptionalOfFrameFilterResult_get_has_value_m0B5B694A1819769F52A8B8B676DD8E7A504C4A1B_AdjustorThunk },
	{ 0x060003CD, OptionalOfFrameFilterResult_set_has_value_mA6CDF1D438A978182E3D705DABCFB18A46835EC8_AdjustorThunk },
	{ 0x060003CE, OptionalOfFunctorOfVoidFromOutputFrame_get_has_value_mAFC7BA70B0524FC0FFE85A533C0BD64F6C07E41A_AdjustorThunk },
	{ 0x060003CF, OptionalOfFunctorOfVoidFromOutputFrame_set_has_value_mDAB3D368167065A00CA62736E0677FBC5A4D8540_AdjustorThunk },
	{ 0x060003E8, OptionalOfString_get_has_value_m9CD5A6C104367A1ED5B9E44785BA721056E0D4AE_AdjustorThunk },
	{ 0x060003E9, OptionalOfString_set_has_value_m264C0ED811DA93486B2DE04A53B25EE094DE2C6A_AdjustorThunk },
	{ 0x060003EA, OptionalOfImageTarget_get_has_value_m1EF7147FD931E8F917DEB6C46BD51A7563022D33_AdjustorThunk },
	{ 0x060003EB, OptionalOfImageTarget_set_has_value_m8644B289D2E7A1195C693BB4E9843FE22F44C7A2_AdjustorThunk },
	{ 0x060003F4, OptionalOfFunctorOfVoidFromInputFrame_get_has_value_m1D155C98CBF075D904FF4031CAA79FA57AC1324C_AdjustorThunk },
	{ 0x060003F5, OptionalOfFunctorOfVoidFromInputFrame_set_has_value_m9E8B4E0E2EDD811B56D788AA37AC28C6CDFC2D92_AdjustorThunk },
	{ 0x060003FE, OptionalOfFunctorOfVoidFromCameraState_get_has_value_m8B4EBF20C2AAD048E9D671047BEE42BF9D07308B_AdjustorThunk },
	{ 0x060003FF, OptionalOfFunctorOfVoidFromCameraState_set_has_value_m1ED45DFE473C86A2641CF8823E93D759BBBFF7D1_AdjustorThunk },
	{ 0x06000408, OptionalOfFunctorOfVoidFromPermissionStatusAndString_get_has_value_m81D80E41F5A8E24E6396A58596E192C4CF7DE650_AdjustorThunk },
	{ 0x06000409, OptionalOfFunctorOfVoidFromPermissionStatusAndString_set_has_value_m92FE8F26A4611C4D1D925EF3DB553A81F482D896_AdjustorThunk },
	{ 0x0600041A, OptionalOfFunctorOfVoidFromRecordStatusAndString_get_has_value_m40F62F2C9F50F336DB495B577C99D4AC9849FFD5_AdjustorThunk },
	{ 0x0600041B, OptionalOfFunctorOfVoidFromRecordStatusAndString_set_has_value_mCC230A3F9E9860FC8BD386C044F043F177F7E852_AdjustorThunk },
	{ 0x06000424, OptionalOfMatrix44F_get_has_value_m9C30778B7D9E982A11356624459B1D0A08A30AD0_AdjustorThunk },
	{ 0x06000425, OptionalOfMatrix44F_set_has_value_m68B97FAF4E20F2D0EE93221D7C51FE60F5EE9B55_AdjustorThunk },
	{ 0x06000426, OptionalOfFunctorOfVoidFromBool_get_has_value_m7D8DFE0840F0E53FA25E3ADEE84C54DDACCBE6BA_AdjustorThunk },
	{ 0x06000427, OptionalOfFunctorOfVoidFromBool_set_has_value_m077DB0E0292E386E9A1925D6161E580CB9A57B8B_AdjustorThunk },
	{ 0x06000430, OptionalOfImage_get_has_value_m7932A8718D733AE7A914D6726A3A8FEBBB4E132A_AdjustorThunk },
	{ 0x06000431, OptionalOfImage_set_has_value_mFA950AF9ADE589082DE916C46EAC5280D8C41CA4_AdjustorThunk },
	{ 0x06000442, OptionalOfFunctorOfVoidFromVideoStatus_get_has_value_m4C5A5EB6B25090AC80D73CF3A3578AF663F68354_AdjustorThunk },
	{ 0x06000443, OptionalOfFunctorOfVoidFromVideoStatus_set_has_value_m798E54F282B4763352BFE839DB21AA648DF5BABB_AdjustorThunk },
	{ 0x0600044C, OptionalOfFunctorOfVoid_get_has_value_m674E9E83E83F23CA50AAE7266ED19DFAC9EEA2ED_AdjustorThunk },
	{ 0x0600044D, OptionalOfFunctorOfVoid_set_has_value_m074530599AED813FDFBE0893F9A8D84B30BBB57A_AdjustorThunk },
	{ 0x0600044E, OptionalOfFunctorOfVoidFromFeedbackFrame_get_has_value_mD61A0B63DDB2445940E42029006B6E5220AACDF8_AdjustorThunk },
	{ 0x0600044F, OptionalOfFunctorOfVoidFromFeedbackFrame_set_has_value_mC6C7579BB351079A6BA5EF5DD10B402EBD9E9B84_AdjustorThunk },
	{ 0x0600055E, Matrix44F_get_data_m67C0A1BF271A6C366D67ED360291B61197CCA09F_AdjustorThunk },
	{ 0x0600055F, Matrix44F_set_data_m7C3634F7119F87B60156682FA5A868E25791AB97_AdjustorThunk },
	{ 0x06000560, Matrix44F__ctor_m08360D8DD00F4951F4B3A7905BF5F08D3D865036_AdjustorThunk },
	{ 0x06000561, Matrix33F_get_data_m85D37D7D472AB79A7962EDCB3D01E70F85FDFBA4_AdjustorThunk },
	{ 0x06000562, Matrix33F_set_data_m501DC97575DC9045BCBEA35973CEE131C695A5A0_AdjustorThunk },
	{ 0x06000563, Matrix33F__ctor_m3683E7AB93523F0178BE0A9E5C8961B02A49F44E_AdjustorThunk },
	{ 0x06000564, Vec3D_get_data_m899EF2E47941ACA4FB20AC16FAD0EB6F52C2820C_AdjustorThunk },
	{ 0x06000565, Vec3D_set_data_mA768D5E64217EEE2119C35022EB2BBB34EBA91DA_AdjustorThunk },
	{ 0x06000566, Vec3D__ctor_mE118A85DCBF981E4D1FB28A10DB58FBCCCFBC3C0_AdjustorThunk },
	{ 0x06000567, Vec4F_get_data_mAB650AF267950B46EC508F2C8B93F26FCF8363D0_AdjustorThunk },
	{ 0x06000568, Vec4F_set_data_m62535D380DECDD9CD28F7A778EDCCEC0B26C5FA9_AdjustorThunk },
	{ 0x06000569, Vec4F__ctor_mD5ED30DAD10566BBAE3D3F24E5130A516D77269E_AdjustorThunk },
	{ 0x0600056A, Vec3F_get_data_m6AB194630146D7297D96735F9E95AD83C044604B_AdjustorThunk },
	{ 0x0600056B, Vec3F_set_data_mEECA60CD3B2A9829F912884D06CC385A63B5DDB8_AdjustorThunk },
	{ 0x0600056C, Vec3F__ctor_m048B740E6E14E759A3AE6BA9EDC537B35D3F32C8_AdjustorThunk },
	{ 0x0600056D, Vec2F_get_data_m2BFD2DF1F83590E815DBF7FD2F18F2A74251DE13_AdjustorThunk },
	{ 0x0600056E, Vec2F_set_data_m511CE6F198ED6BF129F53080ACE5255A69BC7C44_AdjustorThunk },
	{ 0x0600056F, Vec2F__ctor_m54CAE94E831DCCA2714C0B2C38581F238452933D_AdjustorThunk },
	{ 0x06000570, Vec4I_get_data_mA13BC5AC938DA168120751B0301BC644D7ADDD91_AdjustorThunk },
	{ 0x06000571, Vec4I_set_data_mE1291E4B6CF192491813EAC0EA8BB331B474EBA2_AdjustorThunk },
	{ 0x06000572, Vec4I__ctor_m9716FFE4C4B6410244BDBD51F1445186E25A6B08_AdjustorThunk },
	{ 0x06000573, Vec2I_get_data_m8C330A4BEF24736568B464D9A97FE12108A53218_AdjustorThunk },
	{ 0x06000574, Vec2I_set_data_mBD37D15E328F8F2746003C169F93976A499A342B_AdjustorThunk },
	{ 0x06000575, Vec2I__ctor_m79DAD76534551BB0FC8246F6948CE5BF9DF38045_AdjustorThunk },
	{ 0x06000582, BlockInfo__ctor_m56026AA781BF97B7CE29DF961342F00D1B57FB52_AdjustorThunk },
	{ 0x06000592, AccelerometerResult__ctor_m6B5CEB6914B368B1E58761FF3A309544749E2716_AdjustorThunk },
	{ 0x060005D4, MagnetometerResult__ctor_mD167F23B0247DBCF93D8EBA039DDA2822DD8E3ED_AdjustorThunk },
};
static const int32_t s_InvokerIndices[2635] = 
{
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2189,
	2669,
	2669,
	2669,
	2669,
	2669,
	2189,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2170,
	2669,
	2189,
	2669,
	2619,
	2669,
	2155,
	2669,
	2641,
	2619,
	2669,
	2619,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2619,
	2669,
	2155,
	2669,
	2641,
	2619,
	2669,
	2619,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2619,
	2669,
	2155,
	2669,
	2641,
	2619,
	2669,
	2619,
	2669,
	2191,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2189,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	3436,
	3822,
	4015,
	4015,
	3822,
	4150,
	4142,
	3822,
	3824,
	3822,
	3824,
	3822,
	3824,
	3822,
	3824,
	3822,
	3824,
	4111,
	3826,
	4150,
	3822,
	4015,
	4142,
	3822,
	2760,
	4111,
	3822,
	3689,
	3995,
	3822,
	3822,
	3824,
	3822,
	3824,
	4150,
	3822,
	4015,
	3822,
	3822,
	3822,
	3824,
	4150,
	3822,
	4015,
	3822,
	3822,
	3822,
	3822,
	4215,
	3822,
	3995,
	3822,
	4142,
	4092,
	4150,
	4150,
	3150,
	3150,
	3822,
	3686,
	3995,
	4150,
	3822,
	4015,
	4142,
	3445,
	4150,
	3822,
	4015,
	3995,
	3822,
	3822,
	4150,
	3822,
	4015,
	4215,
	2961,
	3148,
	3149,
	4150,
	4150,
	3822,
	4015,
	3142,
	3813,
	4015,
	3995,
	3439,
	2906,
	2905,
	3139,
	4150,
	3822,
	4015,
	4142,
	3995,
	3687,
	3436,
	3441,
	3687,
	4150,
	4150,
	3822,
	4015,
	3421,
	3995,
	3995,
	3995,
	3822,
	4150,
	3822,
	4015,
	2818,
	4123,
	4121,
	4121,
	3995,
	3995,
	3172,
	3450,
	3546,
	3688,
	2746,
	2884,
	2803,
	2803,
	3687,
	4150,
	3822,
	4015,
	2959,
	3822,
	3995,
	3995,
	3995,
	4150,
	3822,
	4015,
	4215,
	3822,
	3995,
	4142,
	4092,
	4150,
	4150,
	3822,
	3688,
	4150,
	3822,
	4015,
	3995,
	3995,
	3822,
	3822,
	3822,
	3995,
	3995,
	3822,
	3822,
	3822,
	3822,
	4111,
	4150,
	3822,
	4015,
	4142,
	4215,
	3995,
	3823,
	3822,
	4092,
	4150,
	4150,
	4150,
	3822,
	4015,
	4142,
	4215,
	3995,
	3823,
	3822,
	4092,
	4150,
	4150,
	4150,
	3822,
	4015,
	4142,
	4215,
	3995,
	3823,
	3995,
	3823,
	3822,
	3446,
	3833,
	4201,
	3686,
	3686,
	3686,
	4092,
	4150,
	4150,
	3995,
	3995,
	3822,
	3824,
	4123,
	3995,
	3741,
	3690,
	3995,
	3719,
	3719,
	3995,
	3686,
	3688,
	3686,
	4092,
	4150,
	3822,
	4015,
	3993,
	3813,
	3993,
	4021,
	4150,
	3822,
	4015,
	3822,
	3822,
	4215,
	3822,
	3995,
	3822,
	4142,
	4092,
	4150,
	4150,
	3827,
	4150,
	3822,
	4015,
	4142,
	4215,
	4201,
	3686,
	3686,
	3686,
	3686,
	3823,
	3995,
	3822,
	4092,
	4150,
	4150,
	3449,
	3449,
	3822,
	4150,
	3822,
	4015,
	3822,
	3995,
	3822,
	4142,
	3307,
	4150,
	4150,
	3822,
	4015,
	3822,
	4142,
	3687,
	4150,
	4150,
	4092,
	3972,
	3972,
	3995,
	4092,
	4150,
	3822,
	4015,
	4150,
	3822,
	4015,
	4142,
	4092,
	4150,
	3822,
	4015,
	3822,
	3822,
	4142,
	4150,
	3822,
	4015,
	3822,
	3822,
	3156,
	3451,
	4015,
	4160,
	3828,
	4225,
	4150,
	4142,
	3822,
	3824,
	3822,
	3824,
	3822,
	3824,
	3822,
	3824,
	4111,
	3826,
	4150,
	3822,
	4015,
	4142,
	3822,
	3428,
	3822,
	3687,
	2760,
	4111,
	4111,
	3689,
	3822,
	3995,
	3822,
	3822,
	3824,
	3822,
	3824,
	4150,
	3822,
	4015,
	3822,
	3822,
	3822,
	3824,
	4150,
	3822,
	4015,
	3822,
	3822,
	3822,
	3822,
	4215,
	3822,
	3995,
	3822,
	4142,
	3813,
	4092,
	4150,
	4150,
	3150,
	3150,
	3822,
	3686,
	3995,
	4150,
	3822,
	4015,
	4215,
	3833,
	3153,
	4150,
	3147,
	4092,
	4150,
	3822,
	4015,
	4142,
	3824,
	3686,
	3823,
	3823,
	3823,
	3823,
	3823,
	3823,
	3823,
	4150,
	3822,
	4015,
	3995,
	4183,
	4183,
	4092,
	3822,
	4150,
	3822,
	4015,
	3822,
	3822,
	4142,
	3995,
	4021,
	4111,
	4111,
	4150,
	3822,
	4015,
	4142,
	3823,
	3995,
	4150,
	3822,
	4015,
	4215,
	3822,
	3995,
	3822,
	4142,
	4092,
	4150,
	4150,
	3822,
	3822,
	3449,
	3449,
	4142,
	3151,
	3824,
	3822,
	4092,
	4150,
	4150,
	3822,
	4015,
	4215,
	4142,
	2727,
	2740,
	4150,
	4150,
	3822,
	4015,
	4201,
	4092,
	4225,
	4225,
	4142,
	4142,
	4142,
	4142,
	4215,
	3823,
	3824,
	2960,
	4150,
	4092,
	4150,
	4150,
	4092,
	4150,
	3995,
	3995,
	3686,
	4123,
	4111,
	3689,
	4150,
	3822,
	4015,
	3822,
	4150,
	4150,
	3822,
	4015,
	3829,
	3824,
	4150,
	4150,
	3822,
	4015,
	3824,
	4150,
	3822,
	4015,
	3831,
	3824,
	4150,
	4150,
	3822,
	4015,
	3824,
	4150,
	3822,
	4015,
	3832,
	3824,
	4150,
	4150,
	3822,
	4015,
	3824,
	4150,
	3822,
	4015,
	3830,
	3824,
	4150,
	4150,
	3822,
	4015,
	3822,
	3428,
	3995,
	3813,
	4150,
	3822,
	4015,
	3822,
	3428,
	3995,
	3813,
	4150,
	3822,
	4015,
	3428,
	3822,
	3995,
	3813,
	3425,
	4150,
	3822,
	4015,
	3822,
	3428,
	3995,
	3813,
	4150,
	3822,
	4015,
	3822,
	3995,
	3822,
	3822,
	4142,
	4150,
	3822,
	4015,
	3822,
	3995,
	3822,
	3822,
	4142,
	4150,
	4150,
	4150,
	3822,
	4015,
	3822,
	3822,
	4142,
	4150,
	3822,
	4015,
	3822,
	3995,
	3822,
	3822,
	4142,
	4150,
	3822,
	4015,
	3995,
	3822,
	4092,
	3822,
	4092,
	3972,
	4092,
	4021,
	3995,
	2811,
	3146,
	3436,
	3822,
	4150,
	3822,
	4015,
	4150,
	3822,
	4015,
	3436,
	3995,
	3822,
	3822,
	4150,
	3822,
	4015,
	3452,
	3822,
	3822,
	4150,
	3822,
	4015,
	3995,
	3822,
	3822,
	3824,
	3822,
	3824,
	4150,
	3822,
	4015,
	4142,
	3995,
	3822,
	4021,
	4150,
	3822,
	4015,
	3822,
	3824,
	4150,
	3822,
	4015,
	3822,
	3822,
	3995,
	4015,
	3813,
	3822,
	4150,
	3822,
	4015,
	3436,
	4150,
	3822,
	3995,
	3742,
	3436,
	4150,
	3822,
	3995,
	3568,
	3436,
	4150,
	3822,
	3995,
	3875,
	3436,
	4150,
	3822,
	3995,
	3568,
	3436,
	4150,
	3822,
	3995,
	3568,
	3436,
	4150,
	3822,
	3995,
	3499,
	3436,
	4150,
	3822,
	3995,
	3568,
	3436,
	4150,
	3822,
	3995,
	3568,
	3574,
	4016,
	3602,
	4063,
	-1,
	-1,
	3822,
	4150,
	4168,
	3574,
	3602,
	3574,
	3602,
	3574,
	3602,
	3436,
	4150,
	4178,
	3152,
	4150,
	4181,
	3574,
	3602,
	3143,
	4150,
	4172,
	3574,
	3602,
	3436,
	4150,
	4174,
	3574,
	3602,
	3436,
	4150,
	4176,
	3428,
	4150,
	4173,
	3140,
	4150,
	4179,
	3140,
	4150,
	4177,
	3140,
	4150,
	4180,
	3574,
	3602,
	3448,
	4150,
	4169,
	2962,
	4150,
	4171,
	3155,
	4150,
	4170,
	3428,
	4150,
	4182,
	3436,
	4150,
	4175,
	3145,
	4150,
	4167,
	3574,
	3602,
	4225,
	2170,
	-1,
	2669,
	2669,
	-1,
	-1,
	2641,
	2189,
	1214,
	1194,
	347,
	1019,
	1214,
	2157,
	573,
	2170,
	2641,
	2189,
	2641,
	2189,
	2641,
	2189,
	2641,
	2189,
	2641,
	2189,
	1214,
	709,
	166,
	1019,
	1214,
	2157,
	573,
	2170,
	1214,
	449,
	97,
	1019,
	1214,
	2157,
	573,
	2170,
	1214,
	447,
	95,
	1019,
	1214,
	2157,
	573,
	2170,
	2641,
	2189,
	2641,
	2189,
	1214,
	709,
	166,
	1019,
	1214,
	2157,
	573,
	2170,
	2641,
	2189,
	1214,
	709,
	166,
	1019,
	1214,
	2157,
	573,
	2170,
	2641,
	2189,
	1214,
	708,
	165,
	1019,
	1214,
	2157,
	573,
	2170,
	2641,
	2189,
	1214,
	446,
	94,
	1019,
	1214,
	2157,
	573,
	2170,
	1214,
	446,
	94,
	1019,
	1214,
	2157,
	573,
	2170,
	2641,
	2189,
	1214,
	446,
	94,
	1019,
	1214,
	2157,
	573,
	2170,
	2641,
	2189,
	2641,
	2189,
	1214,
	713,
	167,
	1019,
	1214,
	2157,
	573,
	2170,
	2641,
	2189,
	1214,
	215,
	66,
	1019,
	1214,
	2157,
	573,
	2170,
	1214,
	450,
	98,
	1019,
	1214,
	2157,
	573,
	2170,
	2641,
	2189,
	1214,
	708,
	165,
	1019,
	1214,
	2157,
	573,
	2170,
	2641,
	2189,
	2641,
	2189,
	1214,
	709,
	166,
	1019,
	1214,
	2157,
	573,
	2170,
	1214,
	448,
	96,
	678,
	1214,
	2157,
	573,
	2170,
	4225,
	2669,
	1977,
	1564,
	2300,
	2300,
	1315,
	1564,
	1564,
	1363,
	1564,
	1564,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	1587,
	2669,
	2669,
	2669,
	2669,
	2669,
	1332,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2170,
	2669,
	2669,
	712,
	2604,
	2669,
	2669,
	2619,
	2619,
	1214,
	1194,
	347,
	1019,
	712,
	2619,
	2619,
	2669,
	2619,
	2170,
	2619,
	2170,
	2619,
	2170,
	2619,
	2170,
	2619,
	2170,
	2643,
	2191,
	712,
	2619,
	2619,
	2669,
	3927,
	2769,
	2643,
	2619,
	1861,
	2602,
	2619,
	2619,
	2170,
	2619,
	2170,
	4225,
	2669,
	1330,
	1330,
	712,
	2619,
	2619,
	2619,
	2170,
	712,
	2619,
	2619,
	4215,
	2619,
	2602,
	2619,
	4207,
	2641,
	2669,
	2669,
	733,
	733,
	2619,
	1821,
	2602,
	712,
	2619,
	2619,
	2669,
	1216,
	712,
	2619,
	2619,
	2602,
	2540,
	2545,
	4225,
	2669,
	1317,
	2669,
	1332,
	712,
	2619,
	2619,
	4215,
	3079,
	3281,
	733,
	2669,
	712,
	2619,
	2619,
	3265,
	4061,
	2604,
	2602,
	3439,
	403,
	402,
	876,
	4064,
	3273,
	3072,
	2170,
	455,
	2170,
	441,
	2669,
	2669,
	4225,
	2669,
	2669,
	2669,
	2669,
	712,
	2619,
	2619,
	2669,
	2602,
	1837,
	1313,
	1216,
	1837,
	2669,
	4225,
	2669,
	1314,
	712,
	2619,
	2619,
	1110,
	2602,
	2602,
	2602,
	2538,
	4225,
	2669,
	1314,
	712,
	2619,
	2619,
	240,
	2662,
	2661,
	2661,
	2602,
	2602,
	3292,
	1595,
	1480,
	1857,
	92,
	328,
	184,
	184,
	1837,
	712,
	2619,
	2619,
	455,
	2619,
	2602,
	2602,
	2602,
	2619,
	2170,
	3,
	2619,
	2170,
	32,
	2619,
	2170,
	683,
	2619,
	2170,
	497,
	2619,
	2170,
	759,
	2619,
	2170,
	1247,
	2619,
	2170,
	432,
	2619,
	2170,
	1110,
	712,
	2619,
	2619,
	4215,
	2619,
	2602,
	4207,
	2641,
	2669,
	2669,
	2619,
	1857,
	42,
	712,
	2619,
	2619,
	2602,
	2602,
	2619,
	2619,
	2619,
	2602,
	2602,
	2619,
	2619,
	2619,
	2619,
	2643,
	495,
	712,
	2619,
	2619,
	2669,
	4215,
	2602,
	2155,
	2619,
	2641,
	2669,
	2669,
	712,
	2619,
	2619,
	2669,
	4215,
	2602,
	2155,
	2619,
	2641,
	2669,
	2669,
	712,
	2619,
	2619,
	2669,
	4215,
	2602,
	2155,
	2602,
	2155,
	2619,
	1200,
	3834,
	4201,
	1821,
	1821,
	1821,
	2641,
	2669,
	2669,
	2602,
	2602,
	2619,
	2170,
	2662,
	2602,
	1974,
	1881,
	2602,
	1947,
	1947,
	2602,
	1821,
	1857,
	1821,
	2641,
	4225,
	2669,
	2310,
	2322,
	3993,
	4061,
	3993,
	2669,
	495,
	712,
	2619,
	2619,
	2614,
	712,
	2619,
	2619,
	4215,
	2619,
	2602,
	2619,
	4207,
	2641,
	2669,
	2669,
	2210,
	712,
	2619,
	2619,
	2669,
	4215,
	4201,
	1821,
	1821,
	1821,
	1821,
	2155,
	2602,
	2619,
	2641,
	2669,
	2669,
	1594,
	1594,
	2619,
	712,
	2619,
	2619,
	2619,
	2602,
	2619,
	4207,
	936,
	2669,
	712,
	2619,
	2619,
	2619,
	4207,
	1837,
	2669,
	2669,
	2641,
	2587,
	2587,
	2602,
	2641,
	712,
	2619,
	2619,
	712,
	2619,
	2619,
	2669,
	2641,
	712,
	2619,
	2619,
	4207,
	3268,
	3599,
	4015,
	2669,
	4152,
	3841,
	4225,
	2669,
	4152,
	2669,
	712,
	2619,
	2619,
	2669,
	2619,
	2170,
	2619,
	2170,
	2619,
	2170,
	2619,
	2170,
	2643,
	2191,
	712,
	2619,
	2619,
	2669,
	3923,
	3478,
	3923,
	1837,
	2768,
	2643,
	2643,
	1861,
	2619,
	2602,
	2619,
	2619,
	2170,
	2619,
	2170,
	4225,
	2669,
	1317,
	1317,
	1317,
	1317,
	712,
	2619,
	2619,
	2619,
	2170,
	712,
	2619,
	2619,
	4215,
	2619,
	2602,
	2619,
	4207,
	4061,
	2641,
	2669,
	2669,
	733,
	733,
	2619,
	1821,
	2602,
	712,
	2619,
	2619,
	4215,
	3834,
	3277,
	2669,
	720,
	2641,
	4225,
	2669,
	2322,
	2325,
	712,
	2619,
	2619,
	2669,
	2170,
	1821,
	2155,
	2155,
	2155,
	2155,
	2155,
	2155,
	2155,
	712,
	2619,
	2619,
	2602,
	2542,
	2542,
	2641,
	2619,
	4225,
	2669,
	1319,
	1319,
	712,
	2619,
	2619,
	2669,
	2602,
	2614,
	2643,
	2643,
	712,
	2619,
	2619,
	2669,
	2155,
	2602,
	712,
	2619,
	2619,
	4215,
	2619,
	2602,
	2619,
	4207,
	2641,
	2669,
	2669,
	2619,
	2619,
	1594,
	1594,
	4207,
	728,
	2170,
	2619,
	2641,
	2669,
	4225,
	2669,
	2307,
	712,
	2619,
	2619,
	4215,
	4207,
	54,
	83,
	2669,
	4225,
	2669,
	2331,
	4201,
	4093,
	4225,
	4225,
	4207,
	4207,
	4207,
	2669,
	712,
	2619,
	2619,
	2669,
	4215,
	2155,
	2170,
	456,
	2669,
	2641,
	2669,
	2669,
	2641,
	2669,
	2602,
	2602,
	1821,
	2662,
	2643,
	1861,
	4225,
	2669,
	2328,
	3922,
	2669,
	4225,
	2669,
	1316,
	712,
	2619,
	2619,
	2669,
	712,
	2619,
	2619,
	2073,
	2170,
	2669,
	4225,
	2669,
	2304,
	712,
	2619,
	2619,
	2170,
	712,
	2619,
	2619,
	2071,
	2170,
	2669,
	4225,
	2669,
	2316,
	712,
	2619,
	2619,
	2170,
	712,
	2619,
	2619,
	2072,
	2170,
	2669,
	4225,
	2669,
	2319,
	712,
	2619,
	2619,
	2170,
	712,
	2619,
	2619,
	2070,
	2170,
	2669,
	4225,
	2669,
	2313,
	712,
	2619,
	2619,
	2619,
	1585,
	2602,
	4061,
	712,
	2619,
	2619,
	2619,
	1585,
	2602,
	4061,
	712,
	2619,
	2619,
	1585,
	2619,
	2602,
	4061,
	3595,
	712,
	2619,
	2619,
	2619,
	1585,
	2602,
	4061,
	712,
	2619,
	2619,
	2619,
	2602,
	2619,
	2619,
	4207,
	712,
	2619,
	2619,
	2619,
	2602,
	2619,
	2544,
	4207,
	2669,
	2669,
	4225,
	2669,
	1331,
	712,
	2619,
	2619,
	2619,
	2619,
	4207,
	712,
	2619,
	2619,
	2619,
	2602,
	2619,
	2619,
	4207,
	712,
	2619,
	2619,
	2602,
	2619,
	2641,
	2619,
	2641,
	2587,
	2641,
	2614,
	2602,
	2894,
	3279,
	3604,
	4064,
	712,
	2619,
	2619,
	712,
	2619,
	2619,
	1216,
	2602,
	2619,
	2619,
	712,
	2619,
	2619,
	1202,
	2619,
	2544,
	4225,
	2669,
	2335,
	1331,
	712,
	2619,
	2619,
	2602,
	2619,
	2619,
	2170,
	2619,
	2170,
	712,
	2619,
	2619,
	2669,
	2602,
	2546,
	2614,
	4225,
	2669,
	1333,
	712,
	2619,
	2619,
	2619,
	2170,
	712,
	2619,
	2619,
	2602,
	2604,
	4061,
	4063,
	2669,
	2669,
	2669,
	1588,
	2669,
	2170,
	2170,
	2170,
	2170,
	2669,
	2669,
	2669,
	2669,
	2170,
	2170,
	2170,
	2189,
	1215,
	2170,
	1216,
	2170,
	2669,
	2669,
	2669,
	651,
	1216,
	2170,
	2669,
	2669,
	2602,
	2155,
	2669,
	2669,
	2669,
	2669,
	2669,
	2602,
	2669,
	2669,
	2602,
	2669,
	2669,
	2669,
	2669,
	2602,
	2155,
	2155,
	2669,
	2669,
	2669,
	2669,
	2170,
	2189,
	2189,
	1215,
	2170,
	2669,
	2669,
	2669,
	2170,
	2170,
	2170,
	2170,
	2669,
	2669,
	2669,
	2669,
	2615,
	2165,
	2619,
	2170,
	2170,
	2669,
	2669,
	4225,
	2619,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2602,
	2155,
	2641,
	2602,
	2155,
	2619,
	2170,
	2669,
	2669,
	2669,
	2669,
	2669,
	2170,
	2669,
	1123,
	2170,
	2170,
	2641,
	2189,
	2669,
	2669,
	2641,
	1837,
	2641,
	1837,
	2669,
	1123,
	1123,
	4207,
	4152,
	4215,
	4154,
	4207,
	4152,
	4207,
	4207,
	2619,
	2170,
	2619,
	2170,
	4225,
	2669,
	2669,
	2189,
	2669,
	2669,
	2669,
	4225,
	2669,
	1216,
	1216,
	2669,
	2669,
	2669,
	2669,
	2669,
	2619,
	2170,
	2602,
	2669,
	2669,
	1216,
	2170,
	2170,
	2170,
	2170,
	2669,
	2669,
	2669,
	2669,
	2619,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2619,
	2170,
	2664,
	2212,
	2669,
	2669,
	2669,
	2669,
	2170,
	2170,
	2170,
	883,
	1588,
	2669,
	2669,
	2669,
	2669,
	2170,
	1220,
	2669,
	2669,
	2669,
	2170,
	2669,
	2669,
	2155,
	2669,
	2641,
	2669,
	2619,
	2669,
	2619,
	2669,
	2669,
	2155,
	2669,
	2641,
	2669,
	2619,
	2669,
	2619,
	2669,
	1220,
	2619,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2602,
	2602,
	2155,
	2619,
	2170,
	2669,
	2669,
	2669,
	2669,
	2669,
	2170,
	2170,
	2619,
	2619,
	2170,
	1579,
	1216,
	1216,
	2189,
	1585,
	2669,
	2669,
	1220,
	2669,
	1220,
	2619,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2619,
	2170,
	2619,
	2170,
	2669,
	2669,
	2669,
	2669,
	2170,
	2170,
	1588,
	2669,
	2669,
	2669,
	2669,
	1220,
	2669,
	2669,
	2170,
	2669,
	2170,
	2155,
	2669,
	2641,
	2669,
	2669,
	2619,
	2669,
	2619,
	2669,
	1220,
	2619,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2602,
	2602,
	2155,
	2619,
	2170,
	2669,
	2669,
	2669,
	2669,
	2669,
	2170,
	2170,
	2619,
	2619,
	2170,
	1579,
	1216,
	1216,
	2189,
	1585,
	2669,
	2669,
	1220,
	2669,
	1220,
	2669,
	2669,
	2669,
	2669,
	2669,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4022,
	4122,
	4124,
	4128,
	4133,
	3274,
	3074,
	4064,
	4225,
	2669,
	2170,
	2155,
	2669,
	2641,
	2669,
	2619,
	2669,
	2619,
	2669,
	2669,
	3844,
	2619,
	2669,
	2669,
	2155,
	2669,
	2641,
	2619,
	2669,
	2619,
	1221,
	2170,
	2669,
	2669,
	2170,
	2669,
	2669,
	2669,
	2669,
	2972,
	2972,
	2972,
	2972,
	3462,
	2764,
	2669,
	2669,
	2641,
	2189,
	2641,
	2189,
	2544,
	2602,
	2155,
	2619,
	2669,
	2170,
	2669,
	2669,
	2669,
	2669,
	2602,
	-1,
	2669,
	2669,
	2669,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2539,
	2074,
	2669,
	2669,
	2669,
	1334,
	732,
	2669,
	866,
	1573,
	2669,
	1214,
	1215,
	360,
	2170,
	2602,
	2170,
	2189,
	2189,
	2669,
	2619,
	2619,
	1216,
	2619,
	1579,
	2602,
	2155,
	2641,
	2643,
	2643,
	2641,
	2619,
	2669,
	2669,
	2669,
	2669,
	2669,
	2641,
	2669,
	2669,
	2170,
	2669,
	2602,
	2669,
	2669,
	2669,
	2669,
	2669,
	2170,
	2619,
	2619,
	2669,
	2641,
	2641,
	2669,
	2669,
	2170,
	2170,
	2669,
	2641,
	2189,
	2641,
	2189,
	2669,
	2189,
	2669,
	2669,
	2170,
	2170,
	2602,
	2155,
	2669,
	2155,
	2669,
	2570,
	2116,
	2669,
	2669,
	1024,
	2669,
	2170,
	2669,
	2619,
	2170,
	2170,
	2170,
	2170,
	2170,
	2602,
	2602,
	2155,
	2641,
	2189,
	2575,
	2122,
	2619,
	2669,
	2669,
	2669,
	2669,
	2669,
	2669,
	2619,
	2155,
	2170,
	2669,
	2170,
	2619,
	2170,
	2619,
	2170,
	2669,
	2669,
	2669,
	2619,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2619,
	2170,
	2619,
	2170,
	2619,
	2170,
	2641,
	2189,
	2641,
	2189,
	2669,
	2669,
	2669,
	1596,
	1201,
	2189,
	2170,
	2669,
	2669,
	2170,
	2170,
	2669,
	2669,
	742,
	2341,
	742,
	2669,
	2669,
	2669,
	2669,
	1982,
	2669,
	742,
	2619,
	2170,
	2619,
	2170,
	2619,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2602,
	2602,
	2155,
	2602,
	2155,
	2619,
	2170,
	2619,
	2170,
	2619,
	2669,
	2669,
	2669,
	2669,
	2669,
	2170,
	2170,
	729,
	2619,
	2619,
	2155,
	2170,
	1579,
	1216,
	1216,
	462,
	2170,
	2170,
	1588,
	2170,
	2669,
	2170,
	2669,
	2669,
	2669,
	1240,
	2669,
	2189,
	2669,
	752,
	2619,
	2170,
	2602,
	2669,
	2669,
	2669,
	2669,
	2669,
	2619,
	2619,
	1579,
	2669,
	2619,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2170,
	2602,
	2155,
	2641,
	2669,
	2669,
	2669,
	1596,
	1596,
	2669,
	2669,
	2170,
	2669,
	2669,
	2669,
	2641,
	2669,
	1123,
	2170,
	2170,
	2170,
	2602,
	2155,
	2619,
	2170,
	2619,
	2170,
	2619,
	2170,
	4064,
	4064,
	4064,
	4064,
	4064,
	4064,
	2619,
	2619,
	2669,
	2669,
	1074,
	2669,
	2669,
	2669,
	2669,
	2155,
	2602,
	2170,
	2669,
	2669,
	2669,
	2669,
	2155,
	2602,
	2170,
	2669,
	2669,
	2669,
	2669,
	2155,
	2602,
	2170,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[32] = 
{
	{ 0x06000375, 32,  (void**)&Detail_FunctorOfVoid_func_m8233FE906FAE6473609404B722CF66412E5EFB48_RuntimeMethod_var, 0 },
	{ 0x06000376, 31,  (void**)&Detail_FunctorOfVoid_destroy_m9A583576B55465A17C01F9B74E416B531702AB80_RuntimeMethod_var, 0 },
	{ 0x0600037E, 22,  (void**)&Detail_FunctorOfVoidFromOutputFrame_func_m142DA320BDD782F0665F95929A1FAF518AAB0B80_RuntimeMethod_var, 0 },
	{ 0x0600037F, 21,  (void**)&Detail_FunctorOfVoidFromOutputFrame_destroy_m39E6790C6700E68EEB6EF662B32E84E8150EE773_RuntimeMethod_var, 0 },
	{ 0x06000381, 28,  (void**)&Detail_FunctorOfVoidFromTargetAndBool_func_m4F4E191A4A3E8C8B1C93A9A83F773843E947D3E0_RuntimeMethod_var, 0 },
	{ 0x06000382, 27,  (void**)&Detail_FunctorOfVoidFromTargetAndBool_destroy_m407203B6D0165B9CBB93CDC394623D25D1394C3E_RuntimeMethod_var, 0 },
	{ 0x06000386, 10,  (void**)&Detail_FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_func_m47D3C6B1C3FA746A6D370FBC7B1D517706FB3EFE_RuntimeMethod_var, 0 },
	{ 0x06000387, 9,  (void**)&Detail_FunctorOfVoidFromCalibrationDownloadStatusAndOptionalOfString_destroy_m44D24357597F4A3861061A606C66CAC57CBDE0D8_RuntimeMethod_var, 0 },
	{ 0x0600038B, 14,  (void**)&Detail_FunctorOfVoidFromCloudRecognizationResult_func_m631246C2873CA0786043DC8226B7A1F69BAF973D_RuntimeMethod_var, 0 },
	{ 0x0600038C, 13,  (void**)&Detail_FunctorOfVoidFromCloudRecognizationResult_destroy_mBF60D4AFDA72F76BFBE41B77BBAACB66D22FB251_RuntimeMethod_var, 0 },
	{ 0x06000390, 18,  (void**)&Detail_FunctorOfVoidFromInputFrame_func_mA15F0CAA1EFCDBBE96BA6D3A17F1AE49FFB6443F_RuntimeMethod_var, 0 },
	{ 0x06000391, 17,  (void**)&Detail_FunctorOfVoidFromInputFrame_destroy_mBC40B788EC5FA01C081A8B8F29DD83BD7C04BBE0_RuntimeMethod_var, 0 },
	{ 0x06000393, 12,  (void**)&Detail_FunctorOfVoidFromCameraState_func_mC7E9F14253350E76D81DD852C31479AE6FA97541_RuntimeMethod_var, 0 },
	{ 0x06000394, 11,  (void**)&Detail_FunctorOfVoidFromCameraState_destroy_mFF1D0D74D6DDC00A03B9F68ABD23B93BBCB212B2_RuntimeMethod_var, 0 },
	{ 0x06000396, 24,  (void**)&Detail_FunctorOfVoidFromPermissionStatusAndString_func_m8D9D32B6E616DCFD443DE289BEE2A1A9E10D63B3_RuntimeMethod_var, 0 },
	{ 0x06000397, 23,  (void**)&Detail_FunctorOfVoidFromPermissionStatusAndString_destroy_m781B495D25D988C89184294BD62865D9E801969D_RuntimeMethod_var, 0 },
	{ 0x06000399, 20,  (void**)&Detail_FunctorOfVoidFromLogLevelAndString_func_m9A78157C61E95CA37E54667EDB501049BE406D6A_RuntimeMethod_var, 0 },
	{ 0x0600039A, 19,  (void**)&Detail_FunctorOfVoidFromLogLevelAndString_destroy_mF22BD5934E396FB1042F8B0E27B54DBE318FCBFE_RuntimeMethod_var, 0 },
	{ 0x0600039C, 26,  (void**)&Detail_FunctorOfVoidFromRecordStatusAndString_func_mA45DA83CDE8ACEB8F46865D1B86A518290177EBA_RuntimeMethod_var, 0 },
	{ 0x0600039D, 25,  (void**)&Detail_FunctorOfVoidFromRecordStatusAndString_destroy_mA47A5359B41B4FD8321FA5CC075E76AD7ABBBCD1_RuntimeMethod_var, 0 },
	{ 0x060003A1, 8,  (void**)&Detail_FunctorOfVoidFromBool_func_m18B5057D02371A6A12534E44D51E79565365602A_RuntimeMethod_var, 0 },
	{ 0x060003A2, 7,  (void**)&Detail_FunctorOfVoidFromBool_destroy_m3D313F4C383413AAD2FCB52140036EA67F40B985_RuntimeMethod_var, 0 },
	{ 0x060003A4, 4,  (void**)&Detail_FunctorOfVoidFromBoolAndStringAndString_func_m32AF4156D52F9B1AEA2A81D03E596F56609DF977_RuntimeMethod_var, 0 },
	{ 0x060003A5, 3,  (void**)&Detail_FunctorOfVoidFromBoolAndStringAndString_destroy_mA6C753EE55047102FBA63F1ABC45CE9D2B516010_RuntimeMethod_var, 0 },
	{ 0x060003A7, 6,  (void**)&Detail_FunctorOfVoidFromBoolAndString_func_m4786C9B24241EBD28AAF555E9CFCB97344EFB8CB_RuntimeMethod_var, 0 },
	{ 0x060003A8, 5,  (void**)&Detail_FunctorOfVoidFromBoolAndString_destroy_mDC79451BFE0F528760772F0A015FBC035E6E023E_RuntimeMethod_var, 0 },
	{ 0x060003AA, 30,  (void**)&Detail_FunctorOfVoidFromVideoStatus_func_mB7B7FD5B5858F436F3466A1919AF72453E7BE34F_RuntimeMethod_var, 0 },
	{ 0x060003AB, 29,  (void**)&Detail_FunctorOfVoidFromVideoStatus_destroy_mC318C3913E76C1CF3DC21561E52A312033C580E4_RuntimeMethod_var, 0 },
	{ 0x060003AD, 16,  (void**)&Detail_FunctorOfVoidFromFeedbackFrame_func_mF3A0BCA98C72331241FAC3A693309EBC3570DAD7_RuntimeMethod_var, 0 },
	{ 0x060003AE, 15,  (void**)&Detail_FunctorOfVoidFromFeedbackFrame_destroy_mE0489B5AF6DB4645A89B1AA2C5E728AA2085DA06_RuntimeMethod_var, 0 },
	{ 0x060003B0, 2,  (void**)&Detail_FunctorOfOutputFrameFromListOfOutputFrame_func_m728DEA484FEF9A32AF0EA5C985684C3A923B6E93_RuntimeMethod_var, 0 },
	{ 0x060003B1, 1,  (void**)&Detail_FunctorOfOutputFrameFromListOfOutputFrame_destroy_m3213152A87BBAB55CB406BB9D08FB45301A33151_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[6] = 
{
	{ 0x02000034, { 6, 1 } },
	{ 0x0200013E, { 7, 14 } },
	{ 0x06000373, { 0, 1 } },
	{ 0x06000374, { 1, 1 } },
	{ 0x060003B7, { 2, 4 } },
	{ 0x0600091D, { 21, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[22] = 
{
	{ (Il2CppRGCTXDataType)2, 91 },
	{ (Il2CppRGCTXDataType)3, 5497 },
	{ (Il2CppRGCTXDataType)2, 583 },
	{ (Il2CppRGCTXDataType)3, 19 },
	{ (Il2CppRGCTXDataType)2, 224 },
	{ (Il2CppRGCTXDataType)3, 20 },
	{ (Il2CppRGCTXDataType)3, 721 },
	{ (Il2CppRGCTXDataType)3, 11331 },
	{ (Il2CppRGCTXDataType)2, 2354 },
	{ (Il2CppRGCTXDataType)2, 336 },
	{ (Il2CppRGCTXDataType)3, 11332 },
	{ (Il2CppRGCTXDataType)3, 11335 },
	{ (Il2CppRGCTXDataType)3, 11334 },
	{ (Il2CppRGCTXDataType)3, 11333 },
	{ (Il2CppRGCTXDataType)2, 2314 },
	{ (Il2CppRGCTXDataType)3, 10642 },
	{ (Il2CppRGCTXDataType)1, 2354 },
	{ (Il2CppRGCTXDataType)2, 2354 },
	{ (Il2CppRGCTXDataType)3, 10643 },
	{ (Il2CppRGCTXDataType)3, 10644 },
	{ (Il2CppRGCTXDataType)3, 11336 },
	{ (Il2CppRGCTXDataType)2, 0 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	2635,
	s_methodPointers,
	63,
	s_adjustorThunks,
	s_InvokerIndices,
	32,
	s_reversePInvokeIndices,
	6,
	s_rgctxIndices,
	22,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
