﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KembaliDariEasyAR : MonoBehaviour
{
    public GameObject AR, canvas,ini;
    public void kembaliDariEasyAR()
    {
        PlayerPrefs.SetInt("ManagementButton", 2);
    }

    public void aktifkanAR()
    {
        AR.SetActive(true);
        canvas.SetActive(true);
        ini.SetActive(false);
    }
}
