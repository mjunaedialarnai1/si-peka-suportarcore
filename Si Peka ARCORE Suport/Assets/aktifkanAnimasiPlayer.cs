﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aktifkanAnimasiPlayer : MonoBehaviour
{
    public managementAnimasiSimulasi managementAnimasi;
    public GameObject aktifkan, kotakDialog;

    public void PlayAnimasi()
    {
        managementAnimasi.aktifkanAnimasiIdle();
        aktifkan.SetActive(true);
        kotakDialog.SetActive(false);
    }

}
