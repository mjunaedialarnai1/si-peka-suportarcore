﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.XR;
using UnityEngine.XR.ARFoundation;

public class PortalPlacer : MonoBehaviour
{
    ARRaycastManager m_ARRaycastManager;
    List<ARRaycastHit> raycastHits = new List<ARRaycastHit>();
    

    //this is the prefabs to be instance
    public GameObject portalPrefabs;
    
    //this is the gameobject refrense instance after succesfull raycast intersection with a plane
    private GameObject spawnPortal;

    private void Awake()
    {
        m_ARRaycastManager = GetComponent<ARRaycastManager>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update() 
    {
        if (Input.touchCount > 0)
        {
            //getting touch input
            Touch touch = Input.GetTouch(0);
            if (m_ARRaycastManager.Raycast(touch.position, raycastHits, UnityEngine.XR.ARSubsystems.TrackableType.PlaneWithinPolygon))
            {
                //getting the pose of the touch
                Pose pose = raycastHits[0].pose;
                if (spawnPortal == null)
                {
                    spawnPortal = Instantiate(portalPrefabs,pose.position,Quaternion.Euler(0,0,0));

                    var rotationOfPortal = spawnPortal.transform.rotation.eulerAngles;
                    spawnPortal.transform.eulerAngles = new Vector3(rotationOfPortal.x,Camera.main.transform.rotation.eulerAngles.y,rotationOfPortal.z);
                }
                else
                {
                    spawnPortal.transform.position = pose.position;

                    var rotationOfPortal = spawnPortal.transform.rotation.eulerAngles;
                    spawnPortal.transform.eulerAngles = new Vector3(rotationOfPortal.x, Camera.main.transform.rotation.eulerAngles.y, rotationOfPortal.z);
                }
            }
        }
    }
}
