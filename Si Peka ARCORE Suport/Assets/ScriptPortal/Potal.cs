﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Experimental.XR;
using UnityEngine.XR.ARFoundation;
using UnityEngine.UI;

public class Potal : MonoBehaviour
{
    public Material[] materials;
    //public bool informasi;
    public GameObject informasiGameobject,tombolSesuaikan,gameObjectSimulasi;
    public Text buttonText;
    /* private void Awake()
     {
         informasiGameobject = GameObject.Find("kotak_Dialog");
         tombolSesuaikan = GameObject.Find("tombol_Sesuaikan");
     }*/
    ARPlaneManager m_ARPlaneManager;
    PortalPlacer m_portalPlacer;

    // Start is called before the first frame update
    void Start()
    {
        m_ARPlaneManager = GameObject.Find("AR Session Origin").GetComponent<ARPlaneManager>();
        m_portalPlacer = GameObject.Find("AR Session Origin").GetComponent<PortalPlacer>();
        tombolSesuaikan = GameObject.Find("tombol_Sesuaikan");
        buttonText = GameObject.Find("Text_TombolSesuaikan").GetComponent<Text>();
        gameObjectSimulasi.SetActive(false);
        foreach (var mat in materials)
        {
            mat.SetInt("stest", (int)CompareFunction.Equal);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerStay(Collider collider)
    {
        if(collider.tag!="MainCamera")
        {
            return;
        }

        Vector3 userPosition = Camera.main.transform.position+Camera.main.transform.forward*Camera.main.nearClipPlane;
        Vector3 relativePosition = transform.InverseTransformPoint(userPosition);
        //Outside
        if (relativePosition.z<0)
        {
            gameObjectSimulasi.SetActive(false);
            foreach (var mat in materials)
            {
                mat.SetInt("stest",(int)CompareFunction.Equal);
                informasiGameobject.SetActive(false);
                tombolSesuaikan.SetActive(true);
                //m_ARPlaneManager.enabled=true;
                //m_portalPlacer.enabled = true;
                TogglePlaneDetectionAndVisibility();
            }
        }
        //Inside
        else
        {
            gameObjectSimulasi.SetActive(true);
            foreach (var mat in materials)
            {
                mat.SetInt("stest", (int)CompareFunction.NotEqual);
                informasiGameobject.SetActive(true);
                tombolSesuaikan.SetActive(false);
                
                
              
                    m_ARPlaneManager.enabled = false;
                    m_portalPlacer.enabled = false;
                
                //TogglePlaneDetectionAndVisibility();
            }
        }
    }
    public void TogglePlaneDetectionAndVisibility()
    {
        m_ARPlaneManager.enabled = !m_ARPlaneManager.enabled;
        if (m_ARPlaneManager.enabled)
        {
            setAllPlaneActiveOrDeactive(true);
            GameObject.Find("AR Session Origin").GetComponent<PortalPlacer>().enabled = true;
            buttonText.text = "Kunci Posisi portal";

        }
        else
        {
            setAllPlaneActiveOrDeactive(false);
            GameObject.Find("AR Session Origin").GetComponent<PortalPlacer>().enabled = false;
            buttonText.text = "ubah posisi portal";
        }
    }


    void setAllPlaneActiveOrDeactive(bool value)
    {
        foreach (var plane in m_ARPlaneManager.trackables)
        {
            plane.gameObject.SetActive(value);
        }
    }
}
