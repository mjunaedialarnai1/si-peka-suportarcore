﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    public GameObject objectToSpawn;
    private PlacementIndikator placementIndikator;
    private void Start()
    {
        placementIndikator = FindObjectOfType<PlacementIndikator>();

    }
    private void Update()
    {
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            GameObject obj = Instantiate(objectToSpawn,
                                         placementIndikator.transform.position,
                                         placementIndikator.transform.rotation);
        }
    }
}
