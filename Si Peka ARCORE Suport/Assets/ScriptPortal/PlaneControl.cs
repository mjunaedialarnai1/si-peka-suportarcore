﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.UI;

public class PlaneControl : MonoBehaviour
{
    public ARPlaneManager m_ARPlaneManager;
    public Text buttonText;
    private void Awake()
    {
        //m_ARPlaneManager = GetComponent<ARPlaneManager>();
        m_ARPlaneManager = GameObject.Find("AR Session Origin").GetComponent<ARPlaneManager>();
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TogglePlaneDetectionAndVisibility()
    {
        m_ARPlaneManager.enabled = !m_ARPlaneManager.enabled;
        if (m_ARPlaneManager.enabled)
        {
            setAllPlaneActiveOrDeactive(true);
            GetComponent<PortalPlacer>().enabled = true;
            buttonText.text = "Kunci Posisi portal";
        }
        else
        {
            setAllPlaneActiveOrDeactive(false);
            GetComponent<PortalPlacer>().enabled = false;
            buttonText.text = "ubah posisi portal";
        }
    }


    void setAllPlaneActiveOrDeactive(bool value)
    {
        foreach (var plane in m_ARPlaneManager.trackables)
        {
            plane.gameObject.SetActive(value);
        }
    }
}
