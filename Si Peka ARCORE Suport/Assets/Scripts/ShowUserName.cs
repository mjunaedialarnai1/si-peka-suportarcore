﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowUserName : MonoBehaviour
{
    public Text teksUserName;
    // Start is called before the first frame update
    void Start()
    {
        teksUserName.text= PlayerPrefs.GetString("UserName");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
