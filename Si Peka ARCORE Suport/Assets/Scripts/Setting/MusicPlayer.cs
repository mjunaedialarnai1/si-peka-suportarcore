﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicPlayer : MonoBehaviour
{
    public AudioSource audioSource;
    
    public Slider slider;
    public bool bukanARFOUNDATION=false,mainMenu=false;


    //PlayerPrefes = Volume
  
    void Start()
    {
        if (bukanARFOUNDATION)
        {
            float a = audioSource.volume = PlayerPrefs.GetFloat("Volume");
            audioSource.volume = a / 3;

        }
        else
        {
            audioSource.volume = PlayerPrefs.GetFloat("Volume");
        }
        if (mainMenu)
        {
            slider.value = PlayerPrefs.GetFloat("Volume");
        }
        
    }

    // Update is called once per frame
   

    public void UpdateVolume(float volumMusic)
    {
        PlayerPrefs.SetFloat("Volume", volumMusic);
        
        if (bukanARFOUNDATION)
        {
            float a = audioSource.volume = PlayerPrefs.GetFloat("Volume");
            audioSource.volume = a / 2;
            
        }
        else
        {
            audioSource.volume = PlayerPrefs.GetFloat("Volume");
        }
    }
}
