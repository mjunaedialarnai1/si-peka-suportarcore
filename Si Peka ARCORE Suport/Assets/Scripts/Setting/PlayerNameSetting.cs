﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerNameSetting : MonoBehaviour
{
    public InputField inputUserName;
    // Start is called before the first frame update
    void Start()
    {
        inputUserName.text = PlayerPrefs.GetString("UserName");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void setUserName()
    {
        string playername = inputUserName.text;
        if (!string.IsNullOrEmpty(playername))
        {
            PlayerPrefs.SetString("UserName", playername);

        }
    }

    public void ResetUserName()
    {
        //PlayerPrefs.DeleteKey("UserName");
        SceneManager.LoadScene(0);
        PlayerPrefs.DeleteAll();
    }
}
