﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ARCoreSuport : MonoBehaviour
{
    public int suport;
    [SerializeField] ARSession m_Session;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        
            if ((ARSession.state == ARSessionState.None) ||
                (ARSession.state == ARSessionState.CheckingAvailability))
            {
                yield return ARSession.CheckAvailability();
            }

            if (ARSession.state == ARSessionState.Unsupported)
            {
            // Start some fallback experience for unsupported devices
            suport = 0;
            }
            else
            {
                // Start the AR session
                m_Session.enabled = true;
            suport = 1;
            }
        Debug.Log(suport);
        PlayerPrefs.SetInt("ARCORESUPORT", suport);
        
    }


}
