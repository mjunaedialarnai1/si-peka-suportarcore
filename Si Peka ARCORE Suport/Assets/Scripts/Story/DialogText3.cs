﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogText3 : MonoBehaviour
{
    public Text textDialog,tanyaKabar;
    // Start is called before the first frame update
    void Start()
    {
        textDialog.text = "Hai, " + PlayerPrefs.GetString("UserName") + " ini adalah hari pertamamu bekerja, apa kabarmu hari ini?";
    }
    public void sangatBaikFunction()
    {
        tanyaKabar.text = "Kamu keren sekali " + PlayerPrefs.GetString("UserName") + " ,sangat bersemangat sekali kamu kawan. ";
    }
    public void kurangBaikFunction()
    {
        tanyaKabar.text = "Tidak usah khawatir " + PlayerPrefs.GetString("UserName") + " ,hampir semua orang mengalami apa yang kamu rasakan dihari pertama kerjanya. ";
    }

}
