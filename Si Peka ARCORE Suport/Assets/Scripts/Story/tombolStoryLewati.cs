﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tombolStoryLewati : MonoBehaviour
{
    public GameObject tombolLewati;
    // Start is called before the first frame update
    void Start()
    {
        string userName = PlayerPrefs.GetString("UserName");
        if (!string.IsNullOrEmpty(userName))
        {
            tombolLewati.SetActive(true);
        }
        else
        {
            tombolLewati.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
