﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowUserNameInStory : MonoBehaviour
{
    public Text textUserName;
    // Start is called before the first frame update
    void Start()
    {
        textUserName.text = PlayerPrefs.GetString("UserName");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
