﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Collider))]
public class bukaSpawner : MonoBehaviour
{
    public UnityEvent upEvent, downEvent;
    public Animator spawner,karakter;

    public GameObject gameobjectTextInformasi, gameobjectTextMaafKamuSudahMemakai, kotakDialog;
    public Text textInformasi, TextMaafKamuSudahMemakai;
    public Animator animasiKamuSudah;
    int nomorTunggu=9;
    

    public void karakterAnimPlay()
    {
        karakter.Play("Play");
    }

    private void Start()
    {
        

    }

    private void OnMouseDown()
    {
        Debug.Log("CLICK!");
        downEvent?.Invoke();
        //spawner.Play("Play");
        int a = PlayerPrefs.GetInt("flowMenggunakanAPD");
        if (a < nomorTunggu)
        {
            gameobjectTextInformasi.SetActive(false);
            gameobjectTextMaafKamuSudahMemakai.SetActive(true);
            TextMaafKamuSudahMemakai.text = "Kamu Harus Memakai peralatan K3 Secara lengkap dahulu";
            animasiKamuSudah.Play("textOn");
            
            
        }
        else if (a >= nomorTunggu)
        {
            
            spawner.Play("Scene");
        }
        



    }


    /* private void OnMouseUp()
     {
         Debug.Log("UP!");
         downEvent?.Invoke();
     }*/


}
