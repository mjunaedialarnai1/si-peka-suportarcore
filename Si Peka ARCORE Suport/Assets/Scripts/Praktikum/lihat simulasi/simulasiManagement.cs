﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class simulasiManagement : MonoBehaviour
{
    public GameObject btnLihatSimulasi, btnBackToPortal,animasi3DSimulasi,aset3DPortal,kotakDialog;

   /* private void Awake()
    {
        animasi3DSimulasi = GameObject.Find("Simulasi");
        aset3DPortal = GameObject.Find("ROOM");
        kotakDialog = GameObject.Find("kotak_Dialog");
    }*/
    private void Start()
    {
        
        Debug.Log(PlayerPrefs.GetInt("flowMenggunakanAPD"));
        //animasi3DSimulasi.SetActive(false);
    }
    public void btnSimulasiOnCLick()
    {
        btnLihatSimulasi.SetActive(false);
        btnBackToPortal.SetActive(true);
        animasi3DSimulasi.SetActive(true);
        aset3DPortal.SetActive(false);
        kotakDialog.SetActive(false);
    }
    public void btnBackToPortalClicked()
    {
        btnLihatSimulasi.SetActive(true);
        btnBackToPortal.SetActive(false);
        animasi3DSimulasi.SetActive(false);
        aset3DPortal.SetActive(true);
        kotakDialog.SetActive(true);
    }


}
