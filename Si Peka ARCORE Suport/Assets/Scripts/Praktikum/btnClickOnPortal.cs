﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Collider))]
public class btnClickOnPortal : MonoBehaviour
{
    public UnityEvent upEvent, downEvent;
    public GameObject objectIni;
    public int nomorTunggu;//nomor ini digunakan untuk mengatur urutan

    public GameObject gameobjectTextInformasi, gameobjectTextMaafKamuSudahMemakai,kotakDialog;
    public Text textInformasi, TextMaafKamuSudahMemakai;
    public string namaBarang;
    public Animator animasiKamuSudah;

   
    private void Start()
    {
        gameobjectTextMaafKamuSudahMemakai.SetActive(false);
        kotakDialog.SetActive(false);
        

    }

    private void OnMouseDown()
    {
        Debug.Log("CLICK!");
        downEvent?.Invoke();
        int a = PlayerPrefs.GetInt("flowMenggunakanAPD");

        if (a <nomorTunggu)
        {
            gameobjectTextInformasi.SetActive(false);
            gameobjectTextMaafKamuSudahMemakai.SetActive(true);
            TextMaafKamuSudahMemakai.text = "Kamu harus memakai peralatan K3 secara urut. tolong ikuti petunjuk yang ada";
            animasiKamuSudah.Play("textOn");
        }else if (a == nomorTunggu)
        {
            objectIni.SetActive(false);
            a++;
            PlayerPrefs.SetInt("flowMenggunakanAPD", a);
        }else if (a > nomorTunggu)
        {
            gameobjectTextInformasi.SetActive(false);
            gameobjectTextMaafKamuSudahMemakai.SetActive(true);
            TextMaafKamuSudahMemakai.text = "Kamu Sudah Memakai " + namaBarang;
            animasiKamuSudah.Play("textOn");
        }
    }
   /* private void OnMouseUp()
    {
        Debug.Log("UP!");
        downEvent?.Invoke();
    }*/

    
}
