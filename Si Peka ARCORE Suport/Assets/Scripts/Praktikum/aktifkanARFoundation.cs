﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aktifkanARFoundation : MonoBehaviour
{
    public GameObject menuIni,cameraa, ARSesion, ARSesionOrigin,planeControler;

    public void aktifkan()
    {
        menuIni.SetActive(false);
        cameraa.SetActive(false);
        ARSesion.SetActive(true);
        ARSesionOrigin.SetActive(true);
        planeControler.SetActive(true);
    }
}
