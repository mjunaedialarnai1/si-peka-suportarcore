﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Navigasi : MonoBehaviour
{
    public GameObject NonActivateMenu, ActivateMenu;
    public int open;
    public void OpenScene()
    {
        SceneManager.LoadScene(open);
    }
    public void MenuActivation()
    {
        NonActivateMenu.SetActive(false);
        ActivateMenu.SetActive(true);
    }
    public void exit()
    {
        Application.Quit();
    }
    public void Aktifkan()
    {
        ActivateMenu.SetActive(true);
    }
}
