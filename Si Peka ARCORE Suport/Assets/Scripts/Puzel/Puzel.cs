﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puzel : MonoBehaviour
{
    //abc yang didrag
    public GameObject a, b, c, d, e, f, g, h, i, aBlack, bBlack, cBlack, dBlack, eBlack, fBlack, gBlack, hBlack, iBlack,ajawab,bjawab,cjawab, djawab, ejawab, fjawab, gjawab, hjawab, ijawab, yewwMenang, soal, Next;

    Vector2 aInitialPos, bInitialPos, cInitialPos, dInitialPos, eInitialPos, fInitialPos, gInitialPos, hInitialPos, iInitialPos;

    float penghitunganmundur = 60;

    //public AudioSource source;
    //public AudioClip[] correct;
    //public AudioClip incorrect;
    int JumlahBenar = 0;

    public Text timerUI;

    //bool acorrect, bcorrect, ccorrect, dcorrect, ecorrect = false;


    void Start()
    {
        aInitialPos = a.transform.position;
        bInitialPos = b.transform.position;
        cInitialPos = c.transform.position;
        dInitialPos = d.transform.position;
        eInitialPos = e.transform.position;
        fInitialPos = f.transform.position;
        gInitialPos = g.transform.position;
        hInitialPos = h.transform.position;
        iInitialPos = i.transform.position;
        
        PlayerPrefs.SetInt("JumlahBenar", 0);
    }

    public void DragA()
    {
        a.transform.position = Input.mousePosition;
    }

    public void DragB()
    {
        b.transform.position = Input.mousePosition;
    }

    public void DragC()
    {
        c.transform.position = Input.mousePosition;
    }

    public void DragD()
    {
        d.transform.position = Input.mousePosition;
    }

    public void DragE()
    {
        e.transform.position = Input.mousePosition;
    }

    public void DragF()
    {
        f.transform.position = Input.mousePosition;
    }
    public void DragG()
    {
        g.transform.position = Input.mousePosition;
    }
    public void DragH()
    {
        h.transform.position = Input.mousePosition;
    }
    public void DragI()
    {
        i.transform.position = Input.mousePosition;
    }
    

    //DROPPPPPP


    public void DropA()
    {
        float Distance = Vector3.Distance(a.transform.position, aBlack.transform.position);

        if (Distance < 100)
        {
            a.transform.position = aBlack.transform.position;
            //source.clip = correct[Random.Range(0, correct.Length)];
            //source.Play();
            //acorrect = true;
            JumlahBenar = PlayerPrefs.GetInt("JumlahBenar") + 1;
            PlayerPrefs.SetInt("JumlahBenar", JumlahBenar);
            aBlack.SetActive(false);
            ajawab.SetActive(true);
            a.SetActive(false);
        }
        else
        {
            a.transform.position = aInitialPos;
            //source.clip = incorrect;
            //source.Play();
        }

    }

    public void DropB()
    {
        float Distance = Vector3.Distance(b.transform.position, bBlack.transform.position);

        if (Distance < 50)
        {
            b.transform.position = bBlack.transform.position;
            //source.clip = correct[Random.Range(0, correct.Length)];
            //source.Play();
            //bcorrect = true;
            JumlahBenar = PlayerPrefs.GetInt("JumlahBenar") + 1;
            PlayerPrefs.SetInt("JumlahBenar", JumlahBenar);
            bBlack.SetActive(false);
            bjawab.SetActive(true);
            b.SetActive(false);
        }
        else
        {
            b.transform.position = bInitialPos;
            //source.clip = incorrect;
            //source.Play();
        }

    }

    public void DropC()
    {
        float Distance = Vector3.Distance(c.transform.position, cBlack.transform.position);

        if (Distance < 50)
        {
            c.transform.position = cBlack.transform.position;
            //source.clip = correct[Random.Range(0, correct.Length)];
            //source.Play();
            //   ccorrect = true;
            JumlahBenar = PlayerPrefs.GetInt("JumlahBenar") + 1;
            PlayerPrefs.SetInt("JumlahBenar", JumlahBenar);
            cBlack.SetActive(false);
            cjawab.SetActive(true);
            c.SetActive(false);
        }
        else
        {
            c.transform.position = cInitialPos;
            // source.clip = incorrect;
            //source.Play();
        }

    }

    public void DropD()
    {
        float Distance = Vector3.Distance(d.transform.position, dBlack.transform.position);

        if (Distance < 50)
        {
            d.transform.position = dBlack.transform.position;
            //source.clip = correct[Random.Range(0, correct.Length)];
            //source.Play();
            //    dcorrect = true;
            JumlahBenar = PlayerPrefs.GetInt("JumlahBenar") + 1;
            PlayerPrefs.SetInt("JumlahBenar", JumlahBenar);
            dBlack.SetActive(false);
            djawab.SetActive(true);
            d.SetActive(false);
        }
        else
        {
            d.transform.position = dInitialPos;
            //source.clip = incorrect;
            // source.Play();
        }

    }

    public void DropE()
    {
        float Distance = Vector3.Distance(e.transform.position, eBlack.transform.position);

        if (Distance < 50)
        {
            e.transform.position = eBlack.transform.position;
            //source.clip = correct[Random.Range(0, correct.Length)];
            //source.Play();
            //     ecorrect = true;
            JumlahBenar = PlayerPrefs.GetInt("JumlahBenar") + 1;
            PlayerPrefs.SetInt("JumlahBenar", JumlahBenar);
            eBlack.SetActive(false);
            ejawab.SetActive(true);
            e.SetActive(false);
        }
        else
        {
            e.transform.position = eInitialPos;
            //source.clip = incorrect;
            // source.Play();
        }



    }

    public void DropF()
    {
        float Distance = Vector3.Distance(f.transform.position, fBlack.transform.position);

        if (Distance < 100)
        {
            f.transform.position = fBlack.transform.position;
            //source.clip = correct[Random.Range(0, correct.Length)];
            //source.Play();
            //acorrect = true;
            JumlahBenar = PlayerPrefs.GetInt("JumlahBenar") + 1;
            PlayerPrefs.SetInt("JumlahBenar", JumlahBenar);
            fBlack.SetActive(false);
            fjawab.SetActive(true);
            f.SetActive(false);
        }
        else
        {
            f.transform.position = fInitialPos;
            //source.clip = incorrect;
           // source.Play();
        }

    }

    public void DropG()
    {
        float Distance = Vector3.Distance(g.transform.position, gBlack.transform.position);

        if (Distance < 100)
        {
            g.transform.position = gBlack.transform.position;
            //source.clip = correct[Random.Range(0, correct.Length)];
            //source.Play();
            //acorrect = true;
            JumlahBenar = PlayerPrefs.GetInt("JumlahBenar") + 1;
            PlayerPrefs.SetInt("JumlahBenar", JumlahBenar);
            gBlack.SetActive(false);
            gjawab.SetActive(true);
            g.SetActive(false);

        }
        else
        {

            g.transform.position = gInitialPos;
           // source.clip = incorrect;
           // source.Play();
        }

    }

    public void DropH()
    {
        float Distance = Vector3.Distance(h.transform.position, hBlack.transform.position);

        if (Distance < 100)
        {
            h.transform.position = hBlack.transform.position;
            //source.clip = correct[Random.Range(0, correct.Length)];
            //source.Play();
            //acorrect = true;
            JumlahBenar = PlayerPrefs.GetInt("JumlahBenar") + 1;
            PlayerPrefs.SetInt("JumlahBenar", JumlahBenar);
            hBlack.SetActive(false);
            hjawab.SetActive(true);
            h.SetActive(false);
        }
        else
        {
            h.transform.position = hInitialPos;
            //source.clip = incorrect;
            //source.Play();
        }

    }

    public void DropI()
    {
        float Distance = Vector3.Distance(i.transform.position, iBlack.transform.position);

        if (Distance < 100)
        {
            i.transform.position = iBlack.transform.position;
            //source.clip = correct[Random.Range(0, correct.Length)];
           // source.Play();
            //acorrect = true;
            JumlahBenar = PlayerPrefs.GetInt("JumlahBenar") + 1;
            PlayerPrefs.SetInt("JumlahBenar", JumlahBenar);
            iBlack.SetActive(false);
            ijawab.SetActive(true);
            i.SetActive(false);
        }
        else
        {
            i.transform.position = iInitialPos;
           // source.clip = incorrect;
            //source.Play();
        }

    }

    

    public void ResetNilai()
    {
        PlayerPrefs.SetInt("JumlahBenar", 0);
        penghitunganmundur = 60;
        a.transform.position = aInitialPos;
        b.transform.position = bInitialPos;
        c.transform.position = cInitialPos;
        d.transform.position = dInitialPos;
        e.transform.position = eInitialPos;
        f.transform.position = fInitialPos;
        g.transform.position = gInitialPos;

        aBlack.SetActive(false);
        bBlack.SetActive(false);
        cBlack.SetActive(false);
        dBlack.SetActive(false);
        eBlack.SetActive(false);
        fBlack.SetActive(false);
        gBlack.SetActive(false);
        hBlack.SetActive(false);
        iBlack.SetActive(false);

        ajawab.SetActive(true);
        bjawab.SetActive(true);
        cjawab.SetActive(true);
        djawab.SetActive(true);
        ejawab.SetActive(true);
        fjawab.SetActive(true);
        gjawab.SetActive(true);
        hjawab.SetActive(true);
        ijawab.SetActive(true);

        a.SetActive(true);
        b.SetActive(true);
        c.SetActive(true);
        d.SetActive(true);
        e.SetActive(true);
        f.SetActive(true);
        g.SetActive(true);
        h.SetActive(true);
        i.SetActive(true);




    }

    void Update()
    {
        //Nnilai.text = PlayerPrefs.GetInt("JumlahBenar").ToString();
        if (penghitunganmundur > 0)
        {
            timerUI.text = "Puzel | Waktu Tersisa " + penghitunganmundur.ToString("F0")+" Detik";
            //PlayerPrefs.SetInt("Timer",CountDownStartValue)
            //CountDownStartValue--;

            //Next.SetActive(false);
        }
        else
        {
            //SceneManager.LoadScene(SceneIndex);
            //timerUI.text = "GameOver";
            soal.SetActive(false);
            Next.SetActive(true);
            // ResetNilai();
            //penghitunganmundur = 60;
            //CounterDownTimer();


            //soal.SetActive(false);


        }

        penghitunganmundur -= Time.deltaTime;



        if (PlayerPrefs.GetInt("JumlahBenar") == 9)
        {
            yewwMenang.SetActive(true);
            soal.SetActive(false);
        }
    }
}
