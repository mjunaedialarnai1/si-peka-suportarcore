﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class hasilPuzel: MonoBehaviour
{
    public Text text_Ucapan_Selamat;
    public GameObject sertifikat, selamat;
 
    // Start is called before the first frame update
    void Start()
    {
            PlayerPrefs.SetInt("CekPuzel", 1);
            //PlayerPrefs.SetInt("ManagementButton",3) ubah 5 menjadi parameter untuk membuka tombol sertifikat
            if (PlayerPrefs.GetInt("CekQuis") == 1)
            {
                text_Ucapan_Selamat.text = "Selamat " + PlayerPrefs.GetString("UserName") + ", Kamu telah lulus mengerjakan QUis K3Training. Penghargaan telah terbuka, silahkan cek didalam pengaturan.";
                PlayerPrefs.SetInt("ManagementButton", 3);
                sertifikat.SetActive(true);
                selamat.SetActive(false);
            }
            else
            {
                //Selamat Kamu telah lulus mengerjakan QUis K3Training. silahkan menyelesaikan gamePuzel untuk membuka sertifikatmu
                text_Ucapan_Selamat.text = "Selamat " + PlayerPrefs.GetString("UserName") + ", Kamu telah lulus mengerjakan QUis K3Training. silahkan menyelesaikan Game Puzel untuk membuka semua pencapaian.";
                sertifikat.SetActive(false);
                selamat.SetActive(true);
            }

    }

    // Update is called once per frame
    void Update()
    {

    }
}
