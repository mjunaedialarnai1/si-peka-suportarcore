﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HasilQuis : MonoBehaviour
{
    public Text text_Ucapan_Selamat,text_Maaf;
    public GameObject lolos, cobalagi,sertifikat;
    // Start is called before the first frame update
    void Start()
    {
        //PlayerPrefs.SetInt("skor", 75);
        if (PlayerPrefs.GetInt("Skor") >= 75)
        {
            
            PlayerPrefs.SetInt("CekQuis", 1);
            //PlayerPrefs.SetInt("ManagementButton",3) ubah 5 menjadi parameter untuk membuka tombol sertifikat
            if (PlayerPrefs.GetInt("CekPuzel") == 1)
            {
                text_Ucapan_Selamat.text = "Selamat " + PlayerPrefs.GetString("UserName") + ", Kamu telah lulus mengerjakan QUis K3Training dengan nilai "+PlayerPrefs.GetInt("Skor")+" .";
                PlayerPrefs.SetInt("ManagementButton", 3);
                lolos.SetActive(true);
                cobalagi.SetActive(false);
                //sertifikat.SetActive(true);
            }
            else
            {
                lolos.SetActive(true);
                cobalagi.SetActive(false);
                //Selamat Kamu telah lulus mengerjakan QUis K3Training. silahkan menyelesaikan gamePuzel untuk membuka sertifikatmu
                text_Ucapan_Selamat.text = "Selamat " + PlayerPrefs.GetString("UserName") + ", Kamu telah lulus mengerjakan QUis K3Training dengan nilai " + PlayerPrefs.GetInt("Skor") + " . silahkan menyelesaikan game Puzel untuk membuka semua pencapaian..";

            }
        }
        else
        {
            cobalagi.SetActive(true);
            lolos.SetActive(false);
            text_Maaf.text = "Kamu mendapatkan nilai " + PlayerPrefs.GetInt("Skor") + ", Maaf kamu belum lulus mengerjakan Quis k3Training. silahkan mengerjakan ulang agar sertifikat mu terbuka";
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
}
