﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class openingQuis : MonoBehaviour
{
    public Text text;
    public GameObject quis;
    // Start is called before the first frame update
    void Start()
    {
        text.text = "Hai "+PlayerPrefs.GetString("UserName")+", Akhirnya kita kembali bertemu di quis k3Training. kamu diberikan waktu 30 detik untuk menjawab pertanyaan. perlu diingat, untuk mendapatkan sertifikat minimal kamu harus mendapatkan nilai 75 dalam quis ini.";
    }

    public void SceneQuisOpen()
    {
        this.gameObject.SetActive(false);
        quis.SetActive(true);
    }
}
