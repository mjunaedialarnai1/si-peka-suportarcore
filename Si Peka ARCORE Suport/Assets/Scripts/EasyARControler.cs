﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EasyARControler : MonoBehaviour
{
    public Animator animCharacter;
    int urut = 0;
    public int jumlahAnimasi;
    public GameObject[] materi;
    public Text info;

    private void Start()
    {
        animCOntrol();
    }
    public void Next()
    {
        urut++;
        if (urut > jumlahAnimasi-1)
        {
            urut = 0;
        }
        animCOntrol();
    }
    public void prev()
    {
        urut--;
        if (urut < 0)
        {
            urut = jumlahAnimasi-1;
        }
        animCOntrol();
    }

   

    public void PlayAnimasi()
    {
        if (urut == 0)
        {
            info.text = "WearPack dan Rompi SAfety";
            animCharacter.Play("idle(werpackDanRompi");
        }
        else if (urut == 1)
        {
            info.text = "Masker";
            animCharacter.Play("masker");
        }
        else if (urut == 2)
        {
            info.text = "earmuffs Dan kacamata";
            animCharacter.Play("earmuffsDankacamata");
        }
        else if (urut == 3)
        {
            info.text = "sarung tangan";
            animCharacter.Play("sarungtangan");
        }
        else if (urut == 4)
        {
            info.text = "Helm";
            animCharacter.Play("Helm");
        }
        else if (urut == 5)
        {
            info.text = "tali keamanan";
            animCharacter.Play("tali keamanan");
        }
        else if (urut == 6)
        {
            info.text = "boots";
            animCharacter.Play("boots");
        }
    }
    public void animCOntrol()
    {
        foreach (GameObject a in materi)
        {
            a.SetActive(false);
        }
        materi[urut].SetActive(true);
        PlayAnimasi();

    }

}
