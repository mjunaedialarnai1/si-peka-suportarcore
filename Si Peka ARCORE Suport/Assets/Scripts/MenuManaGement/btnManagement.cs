﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class btnManagement : MonoBehaviour
{
    public GameObject tombolPraktikAsli, tombolGameAsli, tombolSertifikatAsli, tombolPraktikBayangan, tombolGameBayangan, tombolSertifikatBayangan,tombolEasyAR,tombolEasyARBayangan;

    int ArCoreCheckSuport;
    // Start is called before the first frame update
    private void Start()
    {
        Debug.Log(PlayerPrefs.GetInt("ManagementButton"));
        //PlayerPrefs.SetInt("ManagementButton",0);
        ArCoreCheckSuport = PlayerPrefs.GetInt("ARCORESUPORT");
    }
    void cek()
    {
        if (PlayerPrefs.GetInt("ManagementButton") == 0)
        {
            if (ArCoreCheckSuport == 1)
            {
                tombolPraktikAsli.SetActive(false);
                tombolPraktikBayangan.SetActive(true);
                tombolEasyAR.SetActive(false);
                tombolEasyARBayangan.SetActive(false);
            }
            else
            {
                tombolEasyAR.SetActive(false);
                tombolEasyARBayangan.SetActive(true);
                tombolPraktikAsli.SetActive(false);
                tombolPraktikBayangan.SetActive(false);
            }

   
            tombolGameAsli.SetActive(false);
            tombolGameBayangan.SetActive(true);
            tombolSertifikatAsli.SetActive(false);
            tombolSertifikatBayangan.SetActive(true);
        }else if (PlayerPrefs.GetInt("ManagementButton") == 1)
        {
            if (ArCoreCheckSuport == 1)
            {
                tombolPraktikAsli.SetActive(true);
                tombolPraktikBayangan.SetActive(false);
                tombolEasyAR.SetActive(false);
                tombolEasyARBayangan.SetActive(false);
            }
            else
            {
                tombolEasyAR.SetActive(true);
                tombolEasyARBayangan.SetActive(false);
                tombolPraktikAsli.SetActive(false);
                tombolPraktikBayangan.SetActive(false);
            }

            
            tombolGameAsli.SetActive(false);
            tombolGameBayangan.SetActive(true);
            tombolSertifikatAsli.SetActive(false);
            tombolSertifikatBayangan.SetActive(true);
        }
        else if (PlayerPrefs.GetInt("ManagementButton") == 2)
        {
            if (ArCoreCheckSuport == 1)
            {
                tombolPraktikAsli.SetActive(true);
                tombolPraktikBayangan.SetActive(false);
                tombolEasyAR.SetActive(false);
                tombolEasyARBayangan.SetActive(false);
            }
            else
            {
                tombolEasyAR.SetActive(true);
                tombolEasyARBayangan.SetActive(false);
                tombolPraktikAsli.SetActive(false);
                tombolPraktikBayangan.SetActive(false);
            }


            tombolGameAsli.SetActive(true);
            tombolGameBayangan.SetActive(false);
            tombolSertifikatAsli.SetActive(false);
            tombolSertifikatBayangan.SetActive(true);
        }
        else if (PlayerPrefs.GetInt("ManagementButton") == 3)
        {
            if (ArCoreCheckSuport == 1)
            {
                tombolPraktikAsli.SetActive(true);
                tombolPraktikBayangan.SetActive(false);
                tombolEasyAR.SetActive(false);
                tombolEasyARBayangan.SetActive(false);
            }
            else
            {
                tombolEasyAR.SetActive(true);
                tombolEasyARBayangan.SetActive(false);
                tombolPraktikAsli.SetActive(false);
                tombolPraktikBayangan.SetActive(false);
            }


            tombolGameAsli.SetActive(true);
            tombolGameBayangan.SetActive(false);
            tombolSertifikatAsli.SetActive(true);
            tombolSertifikatBayangan.SetActive(false);
        }
    }

    private void Update()
    {
        cek(); 
    }


}
